class AddNpcNamesToNations < ActiveRecord::Migration
  def self.up
    add_column :nations , :npc_names , :text
    for nation in Nation.find(:all)
      nation.npc_names = ''
      nation.save
    end
  end

  def self.down
    remove_column :nations, :npc_names
  end
end
