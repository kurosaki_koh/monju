class CreateCommandResourceDefinition < ActiveRecord::Migration
  class ObjectDefinition < ActiveRecord::Base
  end

  def change
    create_table :command_resource_definitions do |t|
      t.string :name
      t.integer :power
      t.string :tag
      t.integer :object_definition_id
    end

    add_index :command_resource_definitions , :name
    add_index :command_resource_definitions , :tag

    add_column :object_definitions , :type ,  :string
    add_column :command_resources , :command_resource_definition_id , :integer
    ObjectDefinition.update_all(type: 'CommonDefinition')
  end
end
