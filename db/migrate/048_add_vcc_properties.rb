require 'property.rb'

class AddVccProperties < ActiveRecord::Migration

  def self.up
     vcc_url = Property.new
     vcc_url.name = 'vcc_url'
     vcc_url.value = 'http://www.i-cot.sakura.ne.jp/vis/idress/combat/1.4.1/cal4.html'
     vcc_url.save

     vcc_ver = Property.new
     vcc_ver.name = 'vcc_version'
     vcc_ver.value = '1.4.1'
     vcc_ver.save
  end

  def self.down
    vcc_url = Property.find_by_name('vcc_url')
    vcc_url.destroy
    vcc_ver = Property.find_by_name('vcc_version')
    vcc_ver.destroy
  end
end
