class CreateMileAccounts < ActiveRecord::Migration
  def self.up
    create_table "mile_accounts", :force => true do |t|
      t.integer  "character_id"
      t.integer  "nation_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
  def self.down
    drop_table :mile_accounts      
  end
end
