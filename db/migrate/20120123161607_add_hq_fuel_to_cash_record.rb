class AddHqFuelToCashRecord < ActiveRecord::Migration
  def self.up
    add_column :cash_records , :hqfuel , :integer 
    add_column :latest_cash_records , :hqfuel , :integer
  end

  def self.down
    remove_column :cash_records , :hqfuel  rescue nil
    remove_column :latest_cash_records , :hqfuel rescue nil
  end
end
