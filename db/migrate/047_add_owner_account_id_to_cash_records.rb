class AddOwnerAccountIdToCashRecords < ActiveRecord::Migration
  class CashRecord <ActiveRecord::Base
    belongs_to :nation
    belongs_to :owner_account
  end
  def self.up
    add_column :cash_records , :owner_account_id , :integer
    CashRecord.transaction do
      for record in CashRecord.find(:all)
        record.owner_account = record.nation.owner_account
        record.save
      end   
    end
  end
  
  def self.down
    remove_column :cash_records , :owner_account_id
  end
end
