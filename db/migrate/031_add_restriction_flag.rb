class AddRestrictionFlag < ActiveRecord::Migration
  def self.up
    add_column :nations , :restriction , :boolean
  end

  def self.down
    remove_column :nations , :restriction
  end
end
