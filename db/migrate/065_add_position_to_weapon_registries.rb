class AddPositionToWeaponRegistries < ActiveRecord::Migration
  def self.up
    add_column :weapon_registries , :position , :integer rescue nil
    
    accounts = OwnerAccount.find(:all)
    for acc in accounts
      i = 1
      for wr in acc.weapon_registries.sort_by{|w| w[:id]}
        wr.position = i
        wr.save
        i += 1
      end
    end
  end

  def self.down
    remove_column :weapon_registries, :position
  end
end
