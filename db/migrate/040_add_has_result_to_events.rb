class AddHasResultToEvents < ActiveRecord::Migration
  def self.up
    add_column :events , :has_result , :boolean , :default => false
  end

  def self.down
    remove_column :events , :has_result
  end
end
