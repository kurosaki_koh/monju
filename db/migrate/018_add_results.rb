class AddResults < ActiveRecord::Migration
  def self.up
    create_table :results do |t|
      t.column "originator",    :integer, :default => 0
      t.column "note", :text
      t.column "magic_item" , :text
      t.column "url", :text
      t.column "character_id",  :integer
      t.column "event_id",      :integer
    end
    
    create_table :events do |t|
      t.column "name", :text
      t.column "results_url", :text
    end
  end

  def self.down
    drop_table :results
    drop_table :events
  end
end
