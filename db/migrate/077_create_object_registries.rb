# -*- encoding: utf-8 -*-
class CreateObjectRegistries < ActiveRecord::Migration
  def self.up
    create_table :object_registries do |t|
      t.string :name
      t.integer :number
      t.text :note
      t.integer :position
      t.text :hq
      
      t.integer :owner_account_id
      t.integer :object_definition_id
      
      t.timestamps
    end
    
    role = Role.create(:name => 'item_officer' , :jname => 'アイテム図鑑')
  end

  def self.down
    drop_table :object_registries
    Role.find_by_jname('アイテム図鑑').destroy rescue nil
  end
end
