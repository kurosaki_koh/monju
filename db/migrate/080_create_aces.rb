class CreateAces < ActiveRecord::Migration
  def self.up
    add_column :object_definitions , :object_type , :string rescue nil
    create_table :aces do |t|
      t.string :name
      t.string :hurigana
      t.integer :object_definition_id
      t.timestamp
    end rescue nil
  end

  def self.down
    remove_column :object_definitions , :object_type
    #drop_table :aces
  end
end
