class AlterIObjectColumnName < ActiveRecord::Migration
  def self.up
    rename_column(:i_objects , :specification_id , :object_definition_id)
  end

  def self.down
    rename_column(:i_objects , :object_definition_id  , :specification_id)
  end
end
