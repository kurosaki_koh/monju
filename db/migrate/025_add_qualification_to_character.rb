class AddQualificationToCharacter < ActiveRecord::Migration
  def self.up
    add_column :characters , :qualification , :text
    add_column :characters , :phy , :integer
    add_column :characters , :str , :integer 
    add_column :characters , :dur , :integer 
    add_column :characters , :chr , :integer 
    add_column :characters , :agi , :integer 
    add_column :characters , :dex , :integer 
    add_column :characters , :sen , :integer 
    add_column :characters , :edu , :integer 
    add_column :characters , :luc , :integer 
  end

  def self.down
    remove_column :characters , :qualification 
    remove_column :characters , :phy
    remove_column :characters , :str 
    remove_column :characters , :dur 
    remove_column :characters , :chr 
    remove_column :characters , :agi 
    remove_column :characters , :dex 
    remove_column :characters , :sen 
    remove_column :characters , :edu 
    remove_column :characters , :luc 
  end
end
