require 'rubygems'
require 'open-uri'
require 'hpricot'
require 'kconv'

class AddOptionToObjectDefinition < ActiveRecord::Migration
  class ObjectDefinition < ActiveRecord::Base
    serialize :addition
    def zukan_definition_url=(value)
      self.addition= {} if self.addition.nil?
      self.addition[:zukan_definition_url]=value
    end

    def zukan_definition_url
      self.addition= {} if self.addition.nil?
      self.addition[:zukan_definition_url]
    end

  end

  ZUKAN_URL = 'http://www35.atwiki.jp/marsdaybreaker/pages/698.html'
  def self.import_ace_definition_urls_from_zukan
    definition_index =  Hpricot(open(ZUKAN_URL).read.toutf8)
    links = definition_index / '/html/body/div[11]/div/div[2]/div/div/div/table/tr/td/a'
    for link in links
      ace_name = link.inner_html
      url = link[:href]

      target = ObjectDefinition.find_by_name(ace_name)
      if target
        target.zukan_definition_url = url
        target.save
      end
    end
  end

  def self.up
    add_column :object_definitions ,:addition, :text rescue nil
    ObjectDefinition.find(:all).each{|o|
      o.addition = {:zukan_definition_url => ''}
      o.save
    }
    import_ace_definition_urls_from_zukan
  end

  def self.down
    remove_column :object_definitions , :addition
  end
end
