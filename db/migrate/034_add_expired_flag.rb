class AddExpiredFlag < ActiveRecord::Migration
  def self.up
    add_column :information, :expired , :boolean
  end

  def self.down
    remove_column :information, :expired
  end
end
