class AddUserNameColumn2 < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end

  def self.up
    add_column :users , :name , :string
    users = User.find(:all)
    for user in users
      user.name = user.login
      user.save
    end
  end

  def self.down
    remove_column :users, :name
  end
end
