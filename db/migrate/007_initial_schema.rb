class InitialSchema < ActiveRecord::Migration
  def self.up
    create_table "characters", :force => true do |t|
      t.column "name",          :text,                   :null => false
      t.column "originator",    :integer, :default => 0
      t.column "nation_id",     :integer
      t.column "job_idress_id", :integer
      t.column "character_no",  :text
    end
  
    create_table "yggdrasills", :force => true do |t|
      t.column "name", :text
      t.column "phy",  :integer
      t.column "str",  :integer
      t.column "dur",  :integer
      t.column "chr",  :integer
      t.column "agi",  :integer
      t.column "dex",  :integer
      t.column "sen",  :integer
      t.column "edu",  :integer
      t.column "luc",  :integer
      t.column "note", :text
    end

    create_table "job_idresses", :force => true do |t|
      t.column "name",      :text
      t.column "phy",       :integer
      t.column "str",       :integer
      t.column "dur",       :integer
      t.column "chr",       :integer
      t.column "agi",       :integer
      t.column "dex",       :integer
      t.column "sen",       :integer
      t.column "edu",       :integer
      t.column "luc",       :integer
      t.column "note",      :text
      t.column "race_id",   :integer
      t.column "job1_id",   :integer
      t.column "job2_id",   :integer
      t.column "job3_id",   :integer
      t.column "job4_id",   :integer
      t.column "nation_id", :integer
    end
  
#    add_index "job_idresses", ["race_id"], :name => "fki_race"
  
    create_table "nations", :force => true do |t|
      t.column "name",         :text
      t.column "homepage",     :text
      t.column "king",         :text
      t.column "regent",       :text
      t.column "contact",      :text
      t.column "idress_list",  :text
      t.column "fund",         :integer
      t.column "resource",     :integer
      t.column "food",         :integer
      t.column "fuel",         :integer
      t.column "short_name",   :text
      t.column "contact_note", :text
      t.column "belong",       :text
      t.column "race_id",      :integer
    end
  
    create_table "races", :force => true do |t|
      t.column "name", :text
      t.column "phy",  :integer
      t.column "str",  :integer
      t.column "dur",  :integer
      t.column "chr",  :integer
      t.column "agi",  :integer
      t.column "dex",  :integer
      t.column "sen",  :integer
      t.column "edu",  :integer
      t.column "luc",  :integer
      t.column "note", :text
    end
  
    create_table "skills", :force => true do |t|
      t.column "name",          :text
      t.column "phy",           :integer
      t.column "str",           :integer
      t.column "dur",           :integer
      t.column "chr",           :integer
      t.column "agi",           :integer
      t.column "dex",           :integer
      t.column "sen",           :integer
      t.column "edu",           :integer
      t.column "luc",           :integer
      t.column "zero_attack",   :integer
      t.column "to_5m",         :integer
      t.column "to_50m",        :integer
      t.column "to_300m",       :integer
      t.column "to_500m",       :integer
      t.column "defence",       :integer
      t.column "yggdrasill_id", :integer, :null => false
    end
  
  end

  def self.down
  end
end
