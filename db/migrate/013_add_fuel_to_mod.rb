class AddFuelToMod < ActiveRecord::Migration
  def self.up
    add_column :modifications , :fuel , :integer
  end

  def self.down
    remove_column :modifications , :fuel
  end
end
