class AddPIdressToCharacter < ActiveRecord::Migration
  def self.up
     add_column :characters, :personal_idress_id , :integer
   end

  def self.down
     remove_column :characters, :personal_idress_id
  end
end
