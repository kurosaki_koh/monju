class AddArToCharacters < ActiveRecord::Migration
  def self.up
    add_column :characters , :ar , :integer , :default => nil
  end

  def self.down
    remove_column :characters , :ar
  end
end
