class AddWarningsToIDefinition < ActiveRecord::Migration
  def self.up
    add_column :i_definitions , :warnings , :text
  end

  def self.down
    remove_column :i_definitions , :warnings
  end
end
