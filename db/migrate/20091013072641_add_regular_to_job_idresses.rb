class AddRegularToJobIdresses < ActiveRecord::Migration
  def self.up
    add_column(:job_idresses, :regular , :boolean , :default => true)
    JobIdress.update_all('regular = true')
  end

  def self.down
    remove_column :job_idresses, :regular
  end
end
