class AddTaigenChangelogUrlToProperties < ActiveRecord::Migration
  PROP_KEY = 'taigen_changelog_url'
  PROP_KEY2 = 'echizen_support_url'
  def self.up
    prop = Property.new
    prop.name = PROP_KEY
    prop.value = 'http://bb2.atbb.jp/echizenrnd/viewforum.php?f=6'
    prop.save

    prop = Property.new
    prop.name = PROP_KEY2
    prop.value = 'http://bb2.atbb.jp/echizenrnd/index.php'
    prop.save
  end

  def self.down
    props = []
    props << Property.find_by_name(PROP_KEY)
    props << Property.find_by_name(PROP_KEY2)
    for prop in props.compact
      prop.destroy
      prop.save
    end
  end
end
