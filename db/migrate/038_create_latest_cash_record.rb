class CreateLatestCashRecord < ActiveRecord::Migration
  class LatestCashRecords < ActiveRecord::Base
  end

  def self.up
    create_table :latest_cash_records do |t|
      t.column :url , :string
      t.column :xpath , :string
      t.column :keyword , :string
      t.column :row_index , :integer
      t.column :col_begin , :integer
      t.column :col_end , :integer
      t.column :parsed , :text
      t.column :updated , :datetime
      t.column :last_modified  , :string
      t.column :fund , :integer
      t.column :resource , :integer
      t.column :food , :integer
      t.column :fuel , :integer
      t.column :entertainment , :integer
    end
    
    for n in Nation.find(:all)    
      rec = LatestCashRecords.new
      rec.id = n.id
      rec.save
    end
  end

  def self.down
    drop_table :latest_cash_records
  end
end
