class ChangeMileAccountToOwnerAccount < ActiveRecord::Migration
  
  class MileAccount < ActiveRecord::Base 
  end
  
  class OwnerAccount < ActiveRecord::Base 
    belongs_to :owner , :polymorphic => true
    belongs_to :character
    belongs_to :nation
  end
  
  def self.up
    rename_table :mile_accounts , :owner_accounts
    rename_column :mile_changes , :mile_account_id , :owner_account_id
    add_column :owner_accounts , :owner_id , :integer
    add_column :owner_accounts , :owner_type , :string
    
    OwnerAccount.transaction do
      for owner_account in OwnerAccount.find(:all)
        if owner_account.character
          owner_account.owner = owner_account.character
          #        owner_account[:owner_type]='Character'
        elsif owner_account[:nation_id]
          owner_account.owner = owner_account.nation
          
          #       owner_account[:owner_type]='Nation'
        end
        owner_account.save
      end
    end 
    remove_column :owner_accounts , :character_id
    remove_column :owner_accounts , :nation_id
  end
  
  def self.down
    add_column :owner_accounts,:character_id , :integer
    add_column :owner_accounts , :nation_id , :integer
    
    OwnerAccount.transaction do
      for owner_account in OwnerAccount.find(:all)
        case owner_account.owner.class
        when Character
          owner_account.character_id = owner_account.owner_id
        when Nation
          owner_account.nation_id = owner_account.owner_id
        end
        owner_account.save
      end
    end 
    remove_column :owner_accounts , :owner_id
    remove_column :owner_accounts , :owner_type 
    
    rename_column :mile_changes , :owner_account_id , :mile_account_id
    rename_table :owner_accounts , :mile_accounts
  end
end
