class AlterEventNoTypeToString < ActiveRecord::Migration
  def self.up
    change_column(:cash_records , :event_no , :string)
  end

  def self.down
    change_column(:cash_records , :event_no , :integer)
  end
end
