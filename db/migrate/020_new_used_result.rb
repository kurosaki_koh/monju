class NewUsedResult < ActiveRecord::Migration
  def self.up
    create_table :used_results do |t|
      t.column "pc_name",    :text
      t.column "initial",    :integer, :default => 0
      t.column "result",    :integer, :default => 0
      t.column "note",    :text
      t.column "url",     :text
      t.column "character_id", :integer
    end
  end

  def self.down
    drop_table :used_results
  end
end
