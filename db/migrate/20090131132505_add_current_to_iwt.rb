class AddCurrentToIwt < ActiveRecord::Migration
  def self.up
    add_column :idress_wearing_tables , :is_current , :boolean , :default => false
    IdressWearingTable.update_all('is_current = false')
  end

  def self.down
    remove_column :idress_wearing_tables , :is_current 
  end
end
