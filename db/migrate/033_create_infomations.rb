# -*- encoding: utf-8 -*-
class CreateInfomations < ActiveRecord::Migration
  def self.up
    create_table :information do |t|
      t.column(:title,:string,:default => "お知らせ")
      t.column(:content , :text)
      t.column(:created_at , :datetime)
      t.column(:updated_at , :datetime)
      t.column(:url , :string)
    end
  end

  def self.down
    drop_table :information
  end
end
