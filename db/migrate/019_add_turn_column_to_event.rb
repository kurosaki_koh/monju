class AddTurnColumnToEvent < ActiveRecord::Migration
  def self.up
    add_column :events, :turn , :integer , :default => 0
    add_column :events, :order_no , :integer , :default => 0
  end

  def self.down
    remove_column :events, :turn
    remove_column :events, :order
  end
end
