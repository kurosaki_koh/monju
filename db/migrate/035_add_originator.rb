class AddOriginator < ActiveRecord::Migration
  def self.up
    add_column :yggdrasills , :originator , :integer , :default => 0
    Yggdrasill.update_all("originator = 0")
  end

  def self.down
    remove_column :yggdrasills, :originator
  end
end
