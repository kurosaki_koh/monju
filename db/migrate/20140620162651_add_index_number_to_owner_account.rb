class AddIndexNumberToOwnerAccount < ActiveRecord::Migration
  def change
    add_index :owner_accounts , :number

    add_column :unit_registries , :type , :string
    rename_column :unit_registries , :convert_definition_id , :object_definition_id
  end
end
