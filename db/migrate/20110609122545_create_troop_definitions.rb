class CreateTroopDefinitions < ActiveRecord::Migration
  def self.up
    create_table :troop_definitions do |t|
      t.string :name
      t.integer :turn
      t.integer :nation_id
      t.string :source_url
      t.text :taigen_def
      t.text :evaluations
      t.text :evaluations_data
      t.datetime :source_updated_at
      t.datetime :taigen_def_updated_at

      t.timestamps
    end
  end

  def self.down
    drop_table :troop_definitions
  end
end
