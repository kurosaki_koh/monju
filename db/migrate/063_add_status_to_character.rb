# -*- encoding: utf-8 -*-
class AddStatusToCharacter < ActiveRecord::Migration
  def self.up
    add_column :characters , :status , :string
    Character.update_all("status = '通常'")
  end

  def self.down
    remove_column :characters , :status
  end
end

