class InsertWeponRegistries < ActiveRecord::Migration
  def self.up
    nations = Nation.find(:all)
    for account in nations.map{|n|n.owner_account}
      weapons = account.weapons.find_all{|w|w.specification == nil}
      weapons.group_by(&:name).each do |name , weapons_group|
        spec = Specification.find_by_name(name)
        WeaponRegistry.register(account ,spec , weapons_group.size)
      end
      weapons.each{|w|w.destroy}
    end
  end

  def self.down
    WeaponRegistry.delete_all
  end
end
