class AddNotYet < ActiveRecord::Migration
  def self.up
    add_column :results , :not_yet , :boolean , :default => false
    Result.update_all("not_yet = false")
  end

  def self.down
    remove_colum :results, :not_yet
  end
end
