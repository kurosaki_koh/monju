class AlterColumnName2 < ActiveRecord::Migration
  def up
    rename_column :command_resources , :yggdrasill_id , :source_id
  end

  def down
    rename_column :command_resources , :source_id , :yggdrasill_id
  end
end
