class AddKanaToObjectRegistry < ActiveRecord::Migration
  def self.up
    add_column :object_definitions, :hurigana , :string
  end

  def self.down
    remove_column :object_definitions, :hurigana
  end
end
