class CreateWeaponRegistry < ActiveRecord::Migration
  def self.up
    create_table :weapon_registries do |t|
      t.string :name
      t.integer :specification_id
      t.integer :number
      t.text :note
      t.string :reference_url
      t.integer :owner_account_id
    end

    create_table :specifications do |t|
      t.string :name
      t.string :type_no
      t.text :i_spec
      t.string :original_url
    end
    
    add_column :i_objects , :specification_id , :integer
  end

  def self.down
    drop_table :weapon_registries
    drop_table :specifications
    remove_column :i_objects , :specification_id 
  end
end
