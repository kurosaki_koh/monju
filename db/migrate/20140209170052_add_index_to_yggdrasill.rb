class AddIndexToYggdrasill < ActiveRecord::Migration
  def self.up
    add_index :yggdrasills , :rgt
    add_index :yggdrasills , :lft
    add_index :yggdrasills , :parent_id
    add_column :yggdrasills , :depth , :integer
  end

  def self.down
    remove_index :yggdrasills , :rgt
    remove_index :yggdrasills , :lft
    remove_index :yggdrasills , :parent_id
    remove_column :yggdrasills , :depth
  end
end
