class AddIntentionToCharacter < ActiveRecord::Migration
  def self.up
    add_column :characters , :intention , :text
  end

  def self.down
    remove_column :characters , :intention
  end
end
