class AddModesMode < ActiveRecord::Migration
  def self.up
    add_column :modifications , :modes , :text
    add_column :modifications , :mode , :integer
  end

  def self.down
    remove_column :modifications , :modes
    remove_column :modifications , :mode
  end
end
