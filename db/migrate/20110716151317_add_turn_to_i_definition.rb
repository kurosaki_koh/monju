class AddTurnToIDefinition < ActiveRecord::Migration
  def self.up
    add_column :i_definitions , :revision , :integer
    remove_index :i_definitions , :name
    add_index :i_definitions , [:name , :revision]
  end

  def self.down
    remove_index :i_definitions
    remove_column :i_definitions , :revision
    add_index :i_definitions , :name
  end
end
