class CreateYggdrasillNodes < ActiveRecord::Migration
  def self.up
    rename_table(:yggdrasills , :jobs)
    create_table :yggdrasills do |t|
      t.string :idress_name
      t.string :name
      t.integer :owner_account_id

      t.string :page_url
      t.string :basis_url
      t.string :hq
      t.string :hq_basis_url

      t.text :i_definition

      t.integer :parent_id
      t.integer :lft
      t.integer :rgt

      t.timestamps
    end
  end

  def self.down
    drop_table :yggdrasills
    rename_table(:jobs , :yggdrasills)
  end
end
