class AddAvailableToCommandResources < ActiveRecord::Migration
  def up
    add_column :command_resources , :available , :boolean , default: true
    remove_column :command_resources , :name
    remove_column :command_resources , :power
    remove_column :command_resources , :tag
  end

  def down
    remove_column :command_resources , :available
   add_column :command_resources , :name , :string
   add_column :command_resources , :power , :integer
   add_column :command_resources , :tag , :string
  end

end
