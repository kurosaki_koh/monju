class AddJobIdressEtcHq < ActiveRecord::Migration
  def self.up
    JobIdress.transaction do
      for ji in JobIdress.find(:all)
        ji.bonuses[:etc]={:quality => '0'}
        ji.save
      end
    end
    add_column :job_idresses , :regular_no , :integer
  end

  def self.down
    JobIdress.transaction do
      for ji in JobIdress.find(:all)
        ji.bonuses.delete(:etc)
        ji.save
      end
    end
    remove_column :job_idress , :regular_no rescue nil
  end
end
