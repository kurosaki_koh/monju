class CreateIdressAcquirements < ActiveRecord::Migration
  def self.up
    create_table :idress_acquirements do |t|
      t.integer :character_id
      t.integer :job_idress_id
      t.text :note
      t.timestamps
    end
  end

  def self.down
    drop_table :idress_acquirements
  end
end
