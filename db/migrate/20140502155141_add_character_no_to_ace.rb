class AddCharacterNoToAce < ActiveRecord::Migration
  def change
    add_column :aces , :character_no , :string
    add_index :aces , :character_no
    remove_index :command_resources , name: 'index_command_resources_on_name_and_owner_account_id'
  end
end
