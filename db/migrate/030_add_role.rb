require 'db/create_admin_user.rb'
class AddRole < ActiveRecord::Migration
  def self.up
    add_column :users , :role , :text
    create_admin
  end

  def self.down
    remove_column :users, :role
  end
end
