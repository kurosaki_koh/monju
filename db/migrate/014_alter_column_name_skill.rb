class AlterColumnNameSkill < ActiveRecord::Migration
  def self.up
    rename_column(:skills , :yggdrasill_id , :job_idress_id)
    add_column(:skills , :fuel , :integer)
    add_column(:skills , :note , :text)
  end

  def self.down
    rename_column(:skills , :job_idress_id , :yggdrasill_id )
    remove_column(:skills, :fuel , :integer)
    remove_column(:skills, :note , :text)
  end
end
