class AddIndexToYggAndCommandResources < ActiveRecord::Migration
  def change
    add_index :command_resources , [:name , :owner_account_id] , unique: true
  end
end
