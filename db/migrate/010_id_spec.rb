class IdSpec < ActiveRecord::Migration
  def self.up
    create_table(:id_specs ) do |t|
      t.column :name, :string
      t.column :phy , :integer
      t.column :str , :integer
      t.column :dur , :integer
      t.column :chr , :integer
      t.column :agi , :integer
      t.column :dex , :integer
      t.column :sen , :integer
      t.column :edu , :integer
      t.column :luc , :integer
      t.column :zero_attack , :integer
      t.column :to_5m , :integer
      t.column :to_50m , :integer
      t.column :to_300m , :integer
      t.column :to_500m , :integer
      t.column :defense , :integer

      t.column :required_fuel , :integer
      t.column :required_resource , :integer
      t.column :num_of_pilots , :integer
      t.column :num_of_copilots , :integer
      t.column :ar,:integer
      t.column :note , :text
      t.column :troop_id ,:integer
    end

    add_column :troops , :acts_as , :string
  end


  def self.down
    drop_table :id_specs
    remove_column :trooops , :acts_as
  end
end
