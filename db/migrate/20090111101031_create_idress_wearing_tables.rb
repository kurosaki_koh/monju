class CreateIdressWearingTables < ActiveRecord::Migration
  def self.up
    create_table :idress_wearing_tables do |t|
      t.integer :nation_id
      t.string :turn
      t.string :title
      t.text :idress_wearings
      t.text :deltas
      t.text :note
      t.string :crypted_password
      t.string :salt

      t.timestamps
    end
  end

  def self.down
    drop_table :idress_wearing_tables
  end
end
