class CreateProperties < ActiveRecord::Migration
  def self.up
    create_table :properties do |t|
      t.column :name,:string
      t.column :value , :string
    end
    execute <<-SQL
      insert into properties (name , value)
      values ('current_turn' , '8');
    SQL
  end

  def self.down
    drop_table :properties 
  end
end
