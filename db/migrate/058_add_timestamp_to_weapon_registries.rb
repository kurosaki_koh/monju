class AddTimestampToWeaponRegistries < ActiveRecord::Migration
  def self.up
    add_column :weapon_registries , :created_at , :datetime
    add_column :weapon_registries , :updated_at , :datetime
  end

  def self.down
    remove_column :weapon_registries , :created_at 
    remove_column :weapon_registries , :updated_at 
  end
end
