class AddNationIdForSoldiers < ActiveRecord::Migration
  def self.up
    add_column :operation_nodes , :nation_id , :integer
  end

  def self.down
    remove_column :operation_nodes , :nation_id
  end
end
