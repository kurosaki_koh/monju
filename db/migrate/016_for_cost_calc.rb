class ForCostCalc < ActiveRecord::Migration
  def self.up
    add_column :job_idresses, :fund_cost , :integer , :default => 0
    add_column :job_idresses, :food_cost , :integer , :default => 1
  end

  def self.down
    remove_column :job_idresses, :fund_cost
    remove_column :job_idresses, :food_cost
  end
end
