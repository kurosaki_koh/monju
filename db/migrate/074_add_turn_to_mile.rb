class AddTurnToMile < ActiveRecord::Migration
  def self.up
    add_column :mile_changes , :turn , :integer rescue nil
    MileChange.transaction do
      MileChange.find(:all).each{|m| m.turn = 11 ; m.save}
    end
  end

  def self.down
    remove_column :mile_changes , :turn
  end
end
