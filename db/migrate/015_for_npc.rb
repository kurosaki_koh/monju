class ForNpc < ActiveRecord::Migration
  def self.up
    add_column(:nations, :num_npc , :integer)
    add_column(:operation_nodes, :is_npc , :boolean)
    add_column(:job_idresses, :pilot , :boolean)
    add_column(:job_idresses, :copilot , :boolean)
  end

  def self.down
    remove_column(:nations, :num_npc)
    remove_column(:soldier, :is_npc)
    remove_column(:job_idresses, :pilot)
    remove_column(:job_idresses, :copilot)
  end
end
