class CreateObjectDefinitions < ActiveRecord::Migration
  def self.up
    create_table :object_definitions do |t|
      t.string :name
      t.text :definition
      t.text :specialty_definition
      t.timestamps
    end
  end

  def self.down
    drop_table :object_definitions
  end
end
