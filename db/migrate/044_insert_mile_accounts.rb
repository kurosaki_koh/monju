class InsertMileAccounts < ActiveRecord::Migration
  class MileAccount < ActiveRecord::Base 
    belongs_to :character
    belongs_to :nation
  end
  
  class Nation< ActiveRecord::Base 
    has_one :mile_account
  end
  
  class Character< ActiveRecord::Base 
    has_one :mile_account
  end
  def self.up
    MileAccount.transaction do
      for nation in Nation.find(:all).sort_by{|n| n[:id]}
        unless nation.mile_account
          account = MileAccount.new
          account.nation = nation
          account.save
        end
      end
    end
    
    MileAccount.transaction do
      for character in Character.find(:all).sort_by{|c| c[:id]}
        unless character.mile_account
          account = MileAccount.new
          account.character = character
          account.save
        end
      end
    end
    
  end
  
  def self.down
    MileAccount.delete_all
  end
end
