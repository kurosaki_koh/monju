class CreateBonds < ActiveRecord::Migration
  def self.up
    create_table :bonds do |t|
      t.string :bond_type
      t.integer :character_id
      t.integer :ace_id
      t.string :old_character_name
      t.string :character_name_changing_url
      t.integer :character_ace_id
      t.string :old_ace_name
      t.string :ace_name_changing_url
      t.text :date_note
      t.text :note

      t.timestamps
    end
  end

  def self.down
    drop_table :bonds
  end
end
