class AddEventNameToResults < ActiveRecord::Migration
  def self.up
     add_column :results, :event_name , :text
   end

  def self.down
     remove_column :results, :event_name
  end
end
