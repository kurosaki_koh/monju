# -*- encoding: utf-8 -*-
class AddRaceTypeToCharacter < ActiveRecord::Migration
  class Character < ActiveRecord::Base 
  end
  def self.up
    add_column :characters , :race_type , :string
    add_column :characters , :note , :text
    add_column :characters , :gender  , :text
    
    Character.update_all("race_type = '人' , gender = 'undefined'")
    
    add_column :job_idresses , :race_type , :string
    JobIdress.update_all("race_type = '人'")
  end

  def self.down
    remove_column :characters ,:race_type
    remove_column :characters ,:note
    remove_column :characters ,:gender

    remove_column :job_idresses , :race_type
  end
end
