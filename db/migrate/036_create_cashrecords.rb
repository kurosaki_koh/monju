class CreateCashrecords < ActiveRecord::Migration
  def self.up
    create_table :cash_records do |t|
      t.column :event_no , :integer
      t.column :event_name , :text
#      t.column :details_url , :text
      t.column :entry_url ,:text
      t.column :result_url , :text
      t.column :reference_url, :text
      t.column :fund , :integer
      t.column :resource , :integer
      t.column :food , :integer
      t.column :fuel , :integer
      t.column :amusement , :integer
      t.column :num_npc , :integer
      t.column :num_id , :integer
      t.column :note , :text
      t.column :turn , :integer
      t.column :position, :integer
      t.column :nation_id , :integer
    end
    
    create_table :cash_record_details do |t|
      t.column :detail_type , :text
      t.column :character_no , :text
      t.column :pc_name , :text
      t.column :job_type , :text
      t.column :fund , :integer
      t.column :resource , :integer
      t.column :food , :integer
      t.column :fuel , :integer
      t.column :amusement , :integer
      t.column :num_npc , :integer
      t.column :num_id , :integer
      t.column :fund , :integer
      t.column :resource , :integer
      t.column :food , :integer
      t.column :fuel , :integer
      t.column :amusement , :integer
      t.column :num_npc , :integer
      t.column :cash_record_id , :integer
    end

  end

  def self.down
    drop_table :cash_records
    drop_table :cash_record_details
  end
end
