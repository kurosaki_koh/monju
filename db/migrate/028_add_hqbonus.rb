class AddHqbonus < ActiveRecord::Migration
  def self.up
    add_column :job_idresses , :hq , :text
    JobIdress.update_all ["hq = ? ",({}.to_yaml)]
  end

  def self.down
    remove_column :job_idresses , :hq
  end
end
