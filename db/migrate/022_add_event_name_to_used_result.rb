class AddEventNameToUsedResult < ActiveRecord::Migration
  def self.up
    add_column :used_results, :event_name , :text
  end

  def self.down
    remove_column :used_results, :event_name 
  end
end
