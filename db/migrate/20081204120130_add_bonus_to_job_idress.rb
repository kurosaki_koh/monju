class AddBonusToJobIdress < ActiveRecord::Migration
  def self.up
    add_column :job_idresses , :bonuses, :text
    add_column :job_idresses , :acquired_by_mile , :boolean
    JobIdress.update_all(['bonuses = ? , acquired_by_mile = false',
      {:hq0=>{:quality=>'0'},:hq1=>{:quality=>'0'},:hq2=>{:quality=>'0'},:hq3=>{:quality=>'0'}}.to_yaml])
  end

  def self.down
    remove_column :job_idresses , :bonuses
    remove_column :job_idresses , :acquired_by_mile
  end
end
