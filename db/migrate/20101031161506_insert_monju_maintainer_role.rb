# -*- encoding: utf-8 -*-
class InsertMonjuMaintainerRole < ActiveRecord::Migration
  def self.up
    unless Role.find_by_jname('文殊公社運用部')
      Role.create(:name => 'monju_maintainer' , :jname => '文殊公社運用部')
    end
  end

  def self.down
    for role in Role.find_all_by_jname('文殊公社運用部')
      role.destroy rescue nil
    end
  end
end
