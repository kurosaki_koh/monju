class AddMilesToOwnerAccount < ActiveRecord::Migration
  def self.up
    add_column :owner_accounts , :miles, :integer 
    OwnerAccount.find(:all){|oc| 
      oc.update_miles
    }
  end

  def self.down
    remove_column :owner_accounts , :miles 
  end
end
