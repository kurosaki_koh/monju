# -*- encoding: utf-8 -*-
class CreateRoles2 < ActiveRecord::Migration
  class Role < ActiveRecord::Base
  end
  def self.up
    create_table :roles  do |t|
      t.string :name
      t.string :jname
      t.text :description

      t.timestamps
    end rescue nil

    create_table :roles_users ,:id => false ,:force => true do |t|
      t.integer :role_id
      t.integer :user_id
    end rescue nil
    role = Role.create(:name => 'admin' , :jname => '管理者')
    role = Role.create(:name => 'herald' , :jname => '紋章官')
    role = Role.create(:name => 'accountant' , :jname => '金庫番')
  end

  
  def self.down
    drop_table :roles rescue nil
    drop_table :roles_users rescue nil
  end
end
