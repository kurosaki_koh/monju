class CreateCommandResouces < ActiveRecord::Migration

  def change
    create_table :command_resources do |t|
      t.string :name
      t.integer :power
      t.string :tag
      t.string :category
      t.string :authority_url
      t.integer :owner_account_id
      t.integer :yggdrasill_id
      t.text :note
      t.timestamps
    end

    add_index :command_resources , :tag
  end

end
