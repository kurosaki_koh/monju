class AddColumnsToJobIdresses < ActiveRecord::Migration
  def self.up
    add_column :job_idresses , :owner_account_id , :integer
    add_column :job_idresses , :idress_type , :string
    for ji in JobIdress.find(:all)
      ji.owner_account_id = ji.nation.owner_account.id
      ji.idress_type = 'JobIdress'
      ji.save
    end
  end

  def self.down
    remove_column :job_idresses , :owner_account_id
    remove_column :job_idresses , :idress_type
  end
end
