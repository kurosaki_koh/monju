class CreateTroops < ActiveRecord::Migration
  def self.up
    create_table(:troops ) do |t|
      t.column :name, :string
      t.column :enable , :boolean
      t.column :note , :text
      t.column :operation_id , :integer
      t.column :parent_id , :integer
      t.column :type, :string ##for single table inheritance
      
      ## for Soldier
      t.column :character_id , :integer

      ## for IDressGear and JobIdress
      t.column :archtype_id , :integer
    end
    
    execute(<<-SQL)
      ALTER TABLE troops
        ADD CONSTRAINT parent_id FOREIGN KEY (id)
        REFERENCES troops (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE CASCADE;
    SQL
    
    create_table(:modifications ) do |t|
      t.column :name, :string
      t.column :enable , :boolean
      t.column :phy , :integer
      t.column :str , :integer
      t.column :dur , :integer
      t.column :chr , :integer
      t.column :agi , :integer
      t.column :dex , :integer
      t.column :sen , :integer
      t.column :edu , :integer
      t.column :luc , :integer
      t.column :zero_attack , :integer
      t.column :to_5m , :integer
      t.column :to_50m , :integer
      t.column :to_300m , :integer
      t.column :to_500m , :integer
      t.column :defense , :integer
      t.column :note , :text
      t.column :troop_id ,:integer
    end
    
    execute(<<-SQL)
      ALTER TABLE modifications
        ADD CONSTRAINT troop_id FOREIGN KEY (troop_id)
        REFERENCES troops (id) MATCH SIMPLE
        ON UPDATE CASCADE ON DELETE CASCADE;
    SQL
  end

  def self.down
    drop_table :modifications
    drop_table :troops
    
  end
end
