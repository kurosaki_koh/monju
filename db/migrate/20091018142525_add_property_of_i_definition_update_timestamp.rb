class AddPropertyOfIDefinitionUpdateTimestamp < ActiveRecord::Migration
  class Property < ActiveRecord::Base
  end

  def self.up
    prop = Property.new
    prop.name = 'i_definition_updated_at'
    prop.value = Time.new.strftime('%Y/%m/%d %H:%M:%S')
    prop.save
  end

  def self.down
    prop = Property.find_by_name('i_definition_updated_at')
    if prop
      prop.destroy
      prop.save
    end
  end
end
