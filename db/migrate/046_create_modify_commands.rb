class CreateModifyCommands < ActiveRecord::Migration
  def self.up
    create_table :modify_commands do |t|
      t.text :original
      t.text :change
      t.text :modify_type
      t.integer :owner_account_id
      t.integer :target_id
      t.string :target_type
      t.timestamps
      t.integer :user_id
    end
  end

  def self.down
    drop_table :modify_commands
  end
end
