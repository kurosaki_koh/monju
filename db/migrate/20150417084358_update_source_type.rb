class UpdateSourceType < ActiveRecord::Migration
  def up
    CommandResource.
        joins(command_resource_definition: :object_definition).
        joins('INNER JOIN `unit_registries` ON `unit_registries`.`object_definition_id` = `object_definitions`.`id`').
        select('*').
        where("object_definitions.type" => 'ConvertDefinition').
        update_all('`command_resources`.`source_id` = `unit_registries`.`id` , `command_resources`.`source_type` = \'ConvertSource\' ')


  end

  def down
    CommandResource.
        joins(command_resource_definition: :object_definition).
        joins('INNER JOIN `unit_registries` ON `unit_registries`.`object_definition_id` = `object_definitions`.`id`').
        select('*').
        where("object_definitions.type" => 'ConvertDefinition').
        update_all('`command_resources`.`source_id` = NULL , `command_resources`.`source_type` = NULL ')
  end
end
