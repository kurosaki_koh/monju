class AlterName < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE troops RENAME TO operation_nodes"
    execute "ALTER TABLE modifications RENAME COLUMN troop_id TO operation_node_id"
  end

  def self.down
    execute "ALTER TABLE operation_nodes RENAME TO troops;"
    execute "ALTER TABLE modifications RENAME COLUMN operation_node_id TO troop_id"
  end
end
