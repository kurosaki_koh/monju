class AddPlayerName < ActiveRecord::Migration
  def self.up
    add_column :characters , :player_name , :text
  end

  def self.down
    remove_column :characters, :player_name
  end
end
