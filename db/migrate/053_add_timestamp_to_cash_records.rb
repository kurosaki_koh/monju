class AddTimestampToCashRecords < ActiveRecord::Migration
  def self.up
    add_column :cash_records , :created_at , :datetime
    add_column :cash_records , :updated_at , :datetime
  end

  def self.down
    remove_column :cash_records , :created_at
    remove_column :cash_records , :updated_at
  end
end
