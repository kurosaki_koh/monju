class AddNoteToOrganizations < ActiveRecord::Migration
  def self.up
    add_column :organizations , :note , :text
    add_column :organizations , :parent_organization , :string
  end

  def self.down
    remove_column :organizations , :note
    remove_column :organizations , :parent_organization 
  end
end
