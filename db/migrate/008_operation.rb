class Operation < ActiveRecord::Migration
  def self.up
    create_table(:operations ) do |t|
      t.column :name, :string
      t.column :created, :date
      t.column :modified, :date
      t.column :limit, :date
      t.column :content, :text
      t.column :note , :text
    end
  end

  def self.down
    drop_table(:oparations)
  end
end
