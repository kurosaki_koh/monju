class CreateIDefinition < ActiveRecord::Migration
  def self.up
    create_table :i_definitions do |t|
      t.string :name
      t.string :object_type
      t.text :definition
      t.text :compiled_hash
      t.timestamps
    end
  end

  def self.down
    drop_table :i_definitions
  end
end
