# -*- encoding: utf-8 -*-
class AddPositionToSpecification < ActiveRecord::Migration
  class MetaSpec < ActiveRecord::Base
  end
  class Specification < ActiveRecord::Base
    belongs_to :meta_spec
  end
  def self.up
    create_table :meta_specs do |t|
      t.string :name
      t.timestamp
    end rescue nil
    
    equip = MetaSpec.new(:name => '装備')
    equip.save
    equip.reload
    add_column :specifications , :position , :integer rescue nil
    add_column :specifications , :meta_spec_id , :integer rescue nil
    
    i = 1
    specs = Specification.find(:all).sort_by{|s| s.type_no.to_i}
    for spec in specs
       spec.position = i
       spec.meta_spec = equip
       spec.save
       i += 1
    end
  end

  def self.down
    remove_column :specifications , :position rescue nil
    remove_column :specifications , :meta_spec rescue nil
    drop_table :meta_specs rescue nil
  end
end
