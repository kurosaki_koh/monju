class AddPropertyOfTaigenDocumentsUrl < ActiveRecord::Migration
  class Property < ActiveRecord::Base
  end

  def self.up
    prop = Property.new
    prop.name = 'taigen_documents_url'
    prop.value = 'http://bb2.atbb.jp/echizenrnd/viewforum.php?f=7'
    prop.save
  end

  def self.down
    prop = Property.find_by_name('taigen_documents_url')
    if prop
      prop.destroy
      prop.save
    end
  end
end
