class AddNameAndNumberToOwnerAccount < ActiveRecord::Migration
  class Character < ActiveRecord::Base
  end
  class Nation < ActiveRecord::Base
  end
  class OwnerAccount < ActiveRecord::Base
    belongs_to :owner, :polymorphic => true
  end

  def up
    add_column(:owner_accounts, :name, :string)
    add_column(:owner_accounts, :number, :string)

    OwnerAccount.transaction do
      OwnerAccount.all.each { |acc|
#        puts acc
        next if acc.owner.nil?
        acc.name = acc.owner.name
        acc.number = case acc.owner_type
                       when 'Character'
                         acc.owner.character_no
                       when 'Nation'
                         acc.owner[:id]
                       else
                         nil
                     end
        acc.save
        puts acc.inspect
      }
    end
  end

  def down
    remove_column(:owner_accounts , :name)
    remove_column(:owner_accounts , :number)
  end
end
