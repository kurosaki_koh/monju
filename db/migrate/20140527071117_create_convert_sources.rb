class CreateConvertSources < ActiveRecord::Migration
  def change
    create_table :unit_registries do |t|
      t.string :name
      t.text :note
      t.string :authority_url
      t.integer :owner_account_id
      t.integer :convert_definition_id
      t.boolean :available , default: false
      t.timestamps
    end
  end
end
