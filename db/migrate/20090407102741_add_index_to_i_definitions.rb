class AddIndexToIDefinitions < ActiveRecord::Migration
  def self.up
    add_index(:i_definitions , :name)
  end

  def self.down
    remove_index(:i_definitions , :name)
  end
end
