class AlterColumnName < ActiveRecord::Migration
  def up
    add_column :command_resources , :source_type , :string
    CommandResource.where('yggdrasill_id is not ?' , nil).update_all(source_type: 'Yggdrasill')
  end

  def down
    remove_column :command_resources , :source_type
  end
end
