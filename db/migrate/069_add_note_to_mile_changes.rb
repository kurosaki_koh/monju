class AddNoteToMileChanges < ActiveRecord::Migration
  def self.up
    add_column :mile_changes , :note , :text
  end

  def self.down
    remove_column :mile_changes , :note
  end
end
