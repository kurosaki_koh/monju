class AddColumnsToYggdrasill < ActiveRecord::Migration
  def change
    add_column :yggdrasills , :node_type ,:string
    add_column :yggdrasills , :note , :text
    add_column :yggdrasills , :extra , :text
  end
end
