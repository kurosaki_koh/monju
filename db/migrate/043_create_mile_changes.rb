class CreateMileChanges < ActiveRecord::Migration
  def self.up
    create_table :mile_changes do |t|
      t.column :mile , :integer
      t.column :reason , :text
      t.column :application_url  , :string
      t.column :ground_url , :string
      t.column :mile_account_id , :integer
      t.timestamps
    end
  end

  def self.down
    drop_table :mile_changes
  end
end
