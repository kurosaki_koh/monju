class CreateIObjects < ActiveRecord::Migration
  def self.up
    create_table :i_objects do |t|
      t.string :object_id
      t.string :name
      t.string :type
      t.integer :owner_account_id
      
      t.timestamps
    end
  end

  def self.down
    drop_table :i_objects
  end
end
