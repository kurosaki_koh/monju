class AddDefinition < ActiveRecord::Migration
  def self.up
    add_column :yggdrasills , :definition , :text rescue nil
  end

  def self.down
    remove_column :yggdrasills, :definition
  end
end
