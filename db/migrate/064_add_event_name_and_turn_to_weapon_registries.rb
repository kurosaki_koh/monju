class AddEventNameAndTurnToWeaponRegistries < ActiveRecord::Migration
  def self.up
    add_column :weapon_registries ,:event_no ,  :string 
    add_column :weapon_registries ,:event_name ,  :string 
    add_column :weapon_registries , :turn , :string 
    WeaponRegistry.update_all("turn = '10'")
  end

  def self.down
    remove_column :weapon_registries , :event_no
    remove_column :weapon_registries , :event_name
    remove_column :weapon_registries , :turn
  end
end
