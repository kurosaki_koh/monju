# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150417084358) do

  create_table "aces", :force => true do |t|
    t.string  "name"
    t.string  "hurigana"
    t.integer "object_definition_id"
    t.string  "character_no"
  end

  add_index "aces", ["character_no"], :name => "index_aces_on_character_no"

  create_table "bonds", :force => true do |t|
    t.string   "bond_type"
    t.integer  "character_id"
    t.integer  "ace_id"
    t.string   "old_character_name"
    t.string   "character_name_changing_url"
    t.integer  "character_ace_id"
    t.string   "old_ace_name"
    t.string   "ace_name_changing_url"
    t.text     "date_note"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cash_record_details", :force => true do |t|
    t.text    "detail_type"
    t.text    "character_no"
    t.text    "pc_name"
    t.text    "job_type"
    t.integer "fund"
    t.integer "resource"
    t.integer "food"
    t.integer "fuel"
    t.integer "amusement"
    t.integer "num_npc"
    t.integer "num_id"
    t.integer "cash_record_id"
  end

  create_table "cash_records", :force => true do |t|
    t.string   "event_no"
    t.text     "event_name"
    t.text     "entry_url"
    t.text     "result_url"
    t.text     "reference_url"
    t.integer  "fund"
    t.integer  "resource"
    t.integer  "food"
    t.integer  "fuel"
    t.integer  "amusement"
    t.integer  "num_npc"
    t.integer  "num_id"
    t.text     "note"
    t.integer  "turn"
    t.integer  "position"
    t.integer  "nation_id"
    t.integer  "owner_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "hqfuel"
  end

  create_table "characters", :force => true do |t|
    t.text    "name",                              :null => false
    t.integer "originator",         :default => 0
    t.integer "nation_id"
    t.integer "job_idress_id"
    t.text    "character_no"
    t.integer "personal_idress_id"
    t.text    "qualification"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.text    "intention"
    t.text    "player_name"
    t.integer "ar"
    t.string  "status"
    t.string  "race_type"
    t.text    "note"
    t.text    "gender"
  end

  create_table "command_resource_definitions", :force => true do |t|
    t.string  "name"
    t.integer "power"
    t.string  "tag"
    t.integer "object_definition_id"
  end

  add_index "command_resource_definitions", ["name"], :name => "index_command_resource_definitions_on_name"
  add_index "command_resource_definitions", ["tag"], :name => "index_command_resource_definitions_on_tag"

  create_table "command_resources", :force => true do |t|
    t.string   "category"
    t.string   "authority_url"
    t.integer  "owner_account_id"
    t.integer  "source_id"
    t.text     "note"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.integer  "command_resource_definition_id"
    t.boolean  "available",                      :default => true
    t.string   "source_type"
  end

  create_table "events", :force => true do |t|
    t.text    "name"
    t.text    "results_url"
    t.integer "turn",        :default => 0
    t.integer "order_no",    :default => 0
    t.boolean "has_result",  :default => false
  end

  create_table "i_definitions", :force => true do |t|
    t.string   "name"
    t.string   "object_type"
    t.text     "definition"
    t.text     "compiled_hash"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "warnings"
    t.integer  "revision"
  end

  add_index "i_definitions", ["name", "revision"], :name => "index_i_definitions_on_name_and_revision"

  create_table "i_objects", :force => true do |t|
    t.string   "object_id"
    t.string   "name"
    t.string   "type"
    t.integer  "owner_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "specification_id"
  end

  create_table "id_specs", :force => true do |t|
    t.string  "name"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.integer "zero_attack"
    t.integer "to_5m"
    t.integer "to_50m"
    t.integer "to_300m"
    t.integer "to_500m"
    t.integer "defense"
    t.integer "required_fuel"
    t.integer "required_resource"
    t.integer "num_of_pilots"
    t.integer "num_of_copilots"
    t.integer "ar"
    t.text    "note"
    t.integer "troop_id"
  end

  create_table "idress_acquirements", :force => true do |t|
    t.integer  "character_id"
    t.integer  "job_idress_id"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "idress_wearing_tables", :force => true do |t|
    t.integer  "nation_id"
    t.string   "turn"
    t.string   "title"
    t.text     "idress_wearings"
    t.text     "deltas"
    t.text     "note"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_current",       :default => false
  end

  create_table "information", :force => true do |t|
    t.string   "title",      :default => "お知らせ"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.boolean  "expired"
  end

  create_table "job_idresses", :force => true do |t|
    t.text    "name"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.text    "note"
    t.integer "race_id"
    t.integer "job1_id"
    t.integer "job2_id"
    t.integer "job3_id"
    t.integer "job4_id"
    t.integer "nation_id"
    t.boolean "pilot"
    t.boolean "copilot"
    t.integer "fund_cost",        :default => 0
    t.integer "food_cost",        :default => 1
    t.text    "hq"
    t.integer "owner_account_id"
    t.string  "idress_type"
    t.text    "bonuses"
    t.boolean "acquired_by_mile"
    t.boolean "regular",          :default => true
    t.integer "regular_no"
    t.string  "race_type"
  end

  add_index "job_idresses", ["race_id"], :name => "fki_race"

  create_table "jobs", :force => true do |t|
    t.text    "name"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.text    "note"
    t.integer "originator", :default => 0
    t.text    "definition"
  end

  create_table "latest_cash_records", :force => true do |t|
    t.string   "url"
    t.string   "xpath"
    t.string   "keyword"
    t.integer  "row_index"
    t.integer  "col_begin"
    t.integer  "col_end"
    t.text     "parsed"
    t.datetime "updated"
    t.string   "last_modified"
    t.integer  "fund"
    t.integer  "resource"
    t.integer  "food"
    t.integer  "fuel"
    t.integer  "entertainment"
    t.integer  "hqfuel"
  end

  create_table "meta_specs", :force => true do |t|
    t.string "name"
  end

  create_table "mile_changes", :force => true do |t|
    t.integer  "mile"
    t.text     "reason"
    t.string   "application_url"
    t.string   "ground_url"
    t.integer  "owner_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "note"
    t.integer  "turn"
  end

  create_table "modifications", :force => true do |t|
    t.string  "name"
    t.boolean "enable"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.integer "zero_attack"
    t.integer "to_5m"
    t.integer "to_50m"
    t.integer "to_300m"
    t.integer "to_500m"
    t.integer "defense"
    t.text    "note"
    t.integer "operation_node_id"
    t.text    "modes"
    t.integer "mode"
    t.integer "fuel"
  end

  create_table "modify_commands", :force => true do |t|
    t.text     "original"
    t.text     "change"
    t.text     "modify_type"
    t.integer  "owner_account_id"
    t.integer  "target_id"
    t.string   "target_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "nations", :force => true do |t|
    t.text    "name"
    t.text    "homepage"
    t.text    "king"
    t.text    "regent"
    t.text    "contact"
    t.text    "idress_list"
    t.integer "fund"
    t.integer "resource"
    t.integer "food"
    t.integer "fuel"
    t.text    "short_name"
    t.text    "contact_note"
    t.text    "belong"
    t.integer "race_id"
    t.integer "num_npc"
    t.boolean "restriction"
    t.text    "npc_names"
  end

  create_table "object_definitions", :force => true do |t|
    t.string   "name"
    t.text     "definition"
    t.text     "specialty_definition"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "hurigana"
    t.string   "object_type"
    t.text     "addition"
    t.string   "type"
  end

  create_table "object_registries", :force => true do |t|
    t.string   "name"
    t.integer  "number"
    t.text     "note"
    t.integer  "position"
    t.text     "hq"
    t.integer  "owner_account_id"
    t.integer  "object_definition_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "open_id_authentication_associations", :force => true do |t|
    t.integer "issued"
    t.integer "lifetime"
    t.string  "handle"
    t.string  "assoc_type"
    t.binary  "server_url"
    t.binary  "secret"
  end

  create_table "open_id_authentication_nonces", :force => true do |t|
    t.integer "timestamp",  :null => false
    t.string  "server_url"
    t.string  "salt",       :null => false
  end

  create_table "operation_nodes", :force => true do |t|
    t.string  "name"
    t.boolean "enable"
    t.text    "note"
    t.integer "operation_id"
    t.integer "parent_id"
    t.string  "type"
    t.integer "character_id"
    t.integer "archtype_id"
    t.string  "acts_as"
    t.boolean "is_npc"
    t.integer "nation_id"
  end

  create_table "operations", :force => true do |t|
    t.string "name"
    t.date   "created"
    t.date   "modified"
    t.date   "limit"
    t.text   "content"
    t.text   "note"
  end

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.string   "org_no"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "note"
    t.string   "parent_organization"
  end

  create_table "owner_accounts", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "miles"
    t.string   "name"
    t.string   "number"
  end

  add_index "owner_accounts", ["number"], :name => "index_owner_accounts_on_number"

  create_table "properties", :force => true do |t|
    t.string "name"
    t.string "value"
  end

  create_table "races", :force => true do |t|
    t.text    "name"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.text    "note"
  end

  create_table "results", :force => true do |t|
    t.integer "originator",   :default => 0
    t.text    "note"
    t.text    "magic_item"
    t.text    "url"
    t.integer "character_id"
    t.integer "event_id"
    t.text    "event_name"
    t.boolean "not_yet",      :default => false
    t.integer "position"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.string   "jname"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "skills", :force => true do |t|
    t.text    "name"
    t.integer "phy"
    t.integer "str"
    t.integer "dur"
    t.integer "chr"
    t.integer "agi"
    t.integer "dex"
    t.integer "sen"
    t.integer "edu"
    t.integer "luc"
    t.integer "zero_attack"
    t.integer "to_5m"
    t.integer "to_50m"
    t.integer "to_300m"
    t.integer "to_500m"
    t.integer "defence"
    t.integer "job_idress_id", :null => false
    t.integer "fuel"
    t.text    "note"
  end

  create_table "specifications", :force => true do |t|
    t.string  "name"
    t.string  "type_no"
    t.text    "i_spec"
    t.string  "original_url"
    t.integer "position"
    t.integer "meta_spec_id"
  end

  create_table "troop_definitions", :force => true do |t|
    t.string   "name"
    t.integer  "turn"
    t.integer  "nation_id"
    t.string   "source_url"
    t.text     "taigen_def"
    t.text     "evaluations"
    t.text     "evaluations_data"
    t.datetime "source_updated_at"
    t.datetime "taigen_def_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unit_registries", :force => true do |t|
    t.string   "name"
    t.text     "note"
    t.string   "authority_url"
    t.integer  "owner_account_id"
    t.integer  "object_definition_id"
    t.boolean  "available",            :default => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "type"
  end

  create_table "used_results", :force => true do |t|
    t.text    "pc_name"
    t.integer "initial",      :default => 0
    t.integer "result",       :default => 0
    t.text    "note"
    t.text    "url"
    t.integer "character_id"
    t.text    "event_name"
  end

  create_table "users", :force => true do |t|
    t.string   "login"
    t.string   "email"
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
    t.datetime "remember_token_expires_at"
    t.text     "role"
    t.string   "name"
    t.string   "identity_url"
    t.string   "player_no"
  end

  create_table "weapon_registries", :force => true do |t|
    t.string   "name"
    t.integer  "specification_id"
    t.integer  "number"
    t.text     "note"
    t.string   "reference_url"
    t.integer  "owner_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "event_no"
    t.string   "event_name"
    t.string   "turn"
    t.integer  "position"
  end

  create_table "yggdrasills", :force => true do |t|
    t.string   "idress_name"
    t.string   "name"
    t.integer  "owner_account_id"
    t.string   "page_url"
    t.string   "basis_url"
    t.string   "hq"
    t.string   "hq_basis_url"
    t.text     "i_definition"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "node_type"
    t.text     "note"
    t.text     "extra"
    t.text     "definition"
  end

  add_index "yggdrasills", ["lft"], :name => "index_yggdrasills_on_lft"
  add_index "yggdrasills", ["parent_id"], :name => "index_yggdrasills_on_parent_id"
  add_index "yggdrasills", ["rgt"], :name => "index_yggdrasills_on_rgt"

end
