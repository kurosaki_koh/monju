--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'Standard public schema';


SET search_path = public, pg_catalog;

--
-- Name: hs_each; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE hs_each AS (
	"key" text,
	value text
);


--
-- Name: xpath_list(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION xpath_list(text, text) RETURNS text
    AS $_$SELECT xpath_list($1,$2,',')$_$
    LANGUAGE sql IMMUTABLE STRICT;


--
-- Name: xpath_nodeset(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION xpath_nodeset(text, text) RETURNS text
    AS $_$SELECT xpath_nodeset($1,$2,'','')$_$
    LANGUAGE sql IMMUTABLE STRICT;


--
-- Name: xpath_nodeset(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION xpath_nodeset(text, text, text) RETURNS text
    AS $_$SELECT xpath_nodeset($1,$2,'',$3)$_$
    LANGUAGE sql IMMUTABLE STRICT;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cash_record_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cash_record_details (
    id integer NOT NULL,
    detail_type text,
    character_no text,
    pc_name text,
    job_type text,
    fund integer,
    resource integer,
    food integer,
    fuel integer,
    amusement integer,
    num_npc integer,
    num_id integer,
    cash_record_id integer
);


--
-- Name: cash_record_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cash_record_details_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: cash_record_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cash_record_details_id_seq OWNED BY cash_record_details.id;


--
-- Name: cash_records; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cash_records (
    id integer NOT NULL,
    event_no integer,
    event_name text,
    entry_url text,
    result_url text,
    reference_url text,
    fund integer,
    resource integer,
    food integer,
    fuel integer,
    amusement integer,
    num_npc integer,
    num_id integer,
    note text,
    turn integer,
    "position" integer,
    nation_id integer
);


--
-- Name: cash_records_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cash_records_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: cash_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cash_records_id_seq OWNED BY cash_records.id;


--
-- Name: characters_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE characters_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: cash_record_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cash_record_details (
    id integer NOT NULL,
    detail_type text,
    character_no text,
    pc_name text,
    job_type text,
    fund integer,
    resource integer,
    food integer,
    fuel integer,
    amusement integer,
    num_npc integer,
    num_id integer,
    cash_record_id integer
);


--
-- Name: cash_record_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cash_record_details_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: cash_record_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cash_record_details_id_seq OWNED BY cash_record_details.id;


--
-- Name: cash_records; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cash_records (
    id integer NOT NULL,
    event_no integer,
    event_name text,
    entry_url text,
    result_url text,
    reference_url text,
    fund integer,
    resource integer,
    food integer,
    fuel integer,
    amusement integer,
    num_npc integer,
    num_id integer,
    note text,
    turn integer,
    "position" integer,
    nation_id integer
);


--
-- Name: cash_records_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cash_records_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: cash_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cash_records_id_seq OWNED BY cash_records.id;


--
-- Name: characters; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE characters (
    id integer DEFAULT nextval('characters_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    originator integer DEFAULT 0,
    nation_id integer,
    job_idress_id integer,
    character_no text,
    personal_idress_id integer,
    qualification text,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    intention text,
    player_name text
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: events; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE events (
    id integer DEFAULT nextval('events_id_seq'::regclass) NOT NULL,
    name text,
    results_url text,
    turn integer DEFAULT 0,
    order_no integer DEFAULT 0
);


--
-- Name: id_specs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE id_specs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: id_specs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE id_specs (
    id integer DEFAULT nextval('id_specs_id_seq'::regclass) NOT NULL,
    name character varying(255),
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    zero_attack integer,
    to_5m integer,
    to_50m integer,
    to_300m integer,
    to_500m integer,
    defense integer,
    required_fuel integer,
    required_resource integer,
    num_of_pilots integer,
    num_of_copilots integer,
    ar integer,
    note text,
    troop_id integer
);


--
-- Name: information_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE information_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: information; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE information (
    id integer DEFAULT nextval('information_id_seq'::regclass) NOT NULL,
    title character varying(255) DEFAULT 'お知らせ'::character varying,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    url character varying(255),
    expired boolean
);


--
-- Name: information; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE information (
    id integer NOT NULL,
    title character varying(255) DEFAULT 'お知らせ'::character varying,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    url character varying(255),
    expired boolean
);


--
-- Name: information_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE information_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: information_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE information_id_seq OWNED BY information.id;


--
-- Name: job_idresses; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE job_idresses (
    id integer DEFAULT nextval('job_idresses_id_seq'::regclass) NOT NULL,
    name text,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    note text,
    race_id integer,
    job1_id integer,
    job2_id integer,
    job3_id integer,
    job4_id integer,
    nation_id integer,
    pilot boolean,
    copilot boolean,
    fund_cost integer DEFAULT 0,
    food_cost integer DEFAULT 1,
    hq text
);


--
-- Name: jobidress_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE jobidress_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: modifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE modifications_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: modifications; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE modifications (
    id integer DEFAULT nextval('modifications_id_seq'::regclass) NOT NULL,
    name character varying(255),
    "enable" boolean,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    zero_attack integer,
    to_5m integer,
    to_50m integer,
    to_300m integer,
    to_500m integer,
    defense integer,
    note text,
    operation_node_id integer,
    modes text,
    "mode" integer,
    fuel integer
);


--
-- Name: nations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nations_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: nations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nations (
    id integer DEFAULT nextval('nations_id_seq'::regclass) NOT NULL,
    name text,
    homepage text,
    king text,
    regent text,
    contact text,
    idress_list text,
    fund integer,
    resource integer,
    food integer,
    fuel integer,
    short_name text,
    contact_note text,
    belong text,
    race_id integer,
    num_npc integer,
    restriction boolean
);


--
-- Name: operation_nodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE operation_nodes_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: operation_nodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE operation_nodes (
    id integer DEFAULT nextval('operation_nodes_id_seq'::regclass) NOT NULL,
    name character varying(255),
    "enable" boolean,
    note text,
    operation_id integer,
    parent_id integer,
    "type" character varying(255),
    character_id integer,
    archtype_id integer,
    acts_as character varying(255),
    is_npc boolean,
    nation_id integer
);


--
-- Name: operations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE operations_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: operations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE operations (
    id integer DEFAULT nextval('operations_id_seq'::regclass) NOT NULL,
    name character varying(255),
    created date,
    modified date,
    "limit" date,
    content text,
    note text
);


--
-- Name: properties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE properties (
    id integer NOT NULL,
    name character varying(255),
    value character varying(255)
);


--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE properties_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE properties_id_seq OWNED BY properties.id;


--
-- Name: properties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE properties (
    id integer NOT NULL,
    name character varying(255),
    value character varying(255)
);


--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE properties_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE properties_id_seq OWNED BY properties.id;


--
-- Name: races; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE races (
    id integer DEFAULT nextval('races_id_seq'::regclass) NOT NULL,
    name text,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    note text
);


--
-- Name: results_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE results_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: results; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE results (
    id integer DEFAULT nextval('results_id_seq'::regclass) NOT NULL,
    originator integer DEFAULT 0,
    note text,
    magic_item text,
    url text,
    character_id integer,
    event_id integer,
    event_name text,
    not_yet boolean DEFAULT false
);


--
-- Name: schema_info; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_info (
    version integer
);


--
-- Name: skills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE skills_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: skills; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE skills (
    id integer DEFAULT nextval('skills_id_seq'::regclass) NOT NULL,
    name text,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    zero_attack integer,
    to_5m integer,
    to_50m integer,
    to_300m integer,
    to_500m integer,
    defence integer,
    job_idress_id integer NOT NULL,
    fuel integer,
    note text
);


--
-- Name: troops_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE troops_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: used_results_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE used_results_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: troops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE troops_id_seq OWNED BY operation_nodes.id;


--
-- Name: used_results; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE used_results (
    id integer DEFAULT nextval('used_results_id_seq'::regclass) NOT NULL,
    pc_name text,
    initial integer DEFAULT 0,
    result integer DEFAULT 0,
    note text,
    url text,
    character_id integer,
    event_name text
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    "login" character varying(255),
    email character varying(255),
    crypted_password character varying(40),
    salt character varying(40),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    remember_token character varying(255),
    remember_token_expires_at timestamp without time zone,
    "role" text
);


--
-- Name: yggdrasills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE yggdrasills_id_seq
    START WITH 169
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: yggdrasills; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE yggdrasills (
    id integer DEFAULT nextval('yggdrasills_id_seq'::regclass) NOT NULL,
    name text,
    phy integer,
    str integer,
    dur integer,
    chr integer,
    agi integer,
    dex integer,
    sen integer,
    edu integer,
    luc integer,
    note text,
    originator integer DEFAULT 0
);


--
-- Name: COLUMN yggdrasills.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.name IS '名称';


--
-- Name: COLUMN yggdrasills.phy; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.phy IS '体格';


--
-- Name: COLUMN yggdrasills.str; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.str IS '筋力';


--
-- Name: COLUMN yggdrasills.dur; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.dur IS '耐久力';


--
-- Name: COLUMN yggdrasills.chr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.chr IS '外見';


--
-- Name: COLUMN yggdrasills.agi; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.agi IS '敏捷';


--
-- Name: COLUMN yggdrasills.dex; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.dex IS '器用';


--
-- Name: COLUMN yggdrasills.sen; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.sen IS '感覚';


--
-- Name: COLUMN yggdrasills.edu; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.edu IS '知識';


--
-- Name: COLUMN yggdrasills.luc; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN yggdrasills.luc IS '幸運';


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE cash_record_details ALTER COLUMN id SET DEFAULT nextval('cash_record_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE cash_records ALTER COLUMN id SET DEFAULT nextval('cash_records_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE characters ALTER COLUMN id SET DEFAULT nextval('characters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE cash_records ALTER COLUMN id SET DEFAULT nextval('cash_records_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE properties ALTER COLUMN id SET DEFAULT nextval('properties_id_seq'::regclass);


--
-- Name: cash_record_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE information ALTER COLUMN id SET DEFAULT nextval('information_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE job_idresses ALTER COLUMN id SET DEFAULT nextval('job_idresses_id_seq'::regclass);


--
-- Name: cash_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cash_records
    ADD CONSTRAINT cash_records_pkey PRIMARY KEY (id);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE nations ALTER COLUMN id SET DEFAULT nextval('nations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE operation_nodes ALTER COLUMN id SET DEFAULT nextval('operation_nodes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE operations ALTER COLUMN id SET DEFAULT nextval('operations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE properties ALTER COLUMN id SET DEFAULT nextval('properties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE races ALTER COLUMN id SET DEFAULT nextval('races_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE results ALTER COLUMN id SET DEFAULT nextval('results_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE skills ALTER COLUMN id SET DEFAULT nextval('skills_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE used_results ALTER COLUMN id SET DEFAULT nextval('used_results_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE yggdrasills ALTER COLUMN id SET DEFAULT nextval('yggdrasills_id_seq'::regclass);


--
-- Name: cash_record_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cash_record_details
    ADD CONSTRAINT cash_record_details_pkey PRIMARY KEY (id);


--
-- Name: cash_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cash_records
    ADD CONSTRAINT cash_records_pkey PRIMARY KEY (id);


--
-- Name: characters_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY characters
    ADD CONSTRAINT characters_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: id_specs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY id_specs
    ADD CONSTRAINT id_specs_pkey PRIMARY KEY (id);


--
-- Name: information_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY information
    ADD CONSTRAINT information_pkey PRIMARY KEY (id);


--
-- Name: job_idresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT job_idresses_pkey PRIMARY KEY (id);


--
-- Name: modifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY modifications
    ADD CONSTRAINT modifications_pkey PRIMARY KEY (id);


--
-- Name: nations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nations
    ADD CONSTRAINT nations_pkey PRIMARY KEY (id);


--
-- Name: operations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY operations
    ADD CONSTRAINT operations_pkey PRIMARY KEY (id);


--
-- Name: properties_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id);


--
-- Name: races_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY races
    ADD CONSTRAINT races_pkey PRIMARY KEY (id);


--
-- Name: results_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_pkey PRIMARY KEY (id);


--
-- Name: skills_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- Name: troops_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY operation_nodes
    ADD CONSTRAINT troops_pkey PRIMARY KEY (id);


--
-- Name: used_results_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY used_results
    ADD CONSTRAINT used_results_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: yggdrasills_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY yggdrasills
    ADD CONSTRAINT yggdrasills_pkey PRIMARY KEY (id);


--
-- Name: fki_race; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_race ON job_idresses USING btree (race_id);


--
-- Name: job1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT job1 FOREIGN KEY (job1_id) REFERENCES yggdrasills(id);


--
-- Name: job2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT job2 FOREIGN KEY (job2_id) REFERENCES yggdrasills(id);


--
-- Name: job3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT job3 FOREIGN KEY (job3_id) REFERENCES yggdrasills(id);


--
-- Name: job4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT job4 FOREIGN KEY (job4_id) REFERENCES yggdrasills(id);


--
-- Name: nation_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT nation_id FOREIGN KEY (nation_id) REFERENCES nations(id);


--
-- Name: parent_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY operation_nodes
    ADD CONSTRAINT parent_id FOREIGN KEY (id) REFERENCES operation_nodes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: race; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_idresses
    ADD CONSTRAINT race FOREIGN KEY (race_id) REFERENCES races(id);


--
-- Name: troop_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY modifications
    ADD CONSTRAINT troop_id FOREIGN KEY (operation_node_id) REFERENCES operation_nodes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

INSERT INTO schema_info (version) VALUES (37)
