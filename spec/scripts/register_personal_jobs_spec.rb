# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'
require 'script/tools/register_personal_jobs.rb'

describe PersonalJobRegister do
  fixtures :races , :yggdrasills,:characters,:job_idresses , :owner_accounts

  before(:each) do
    @pjr = PersonalJobRegister.new
  end

  it '入力文字列を正しく解釈できる' do
    @pjr.source = <<EOT
01-00001-01:個人職業なし:ただの人＋謎１＋謎２＋謎３
EOT
    @pjr.parse_all

    parsed = @pjr.source_data[0]
    parsed[:character_no].should == '01-00001-01'
    parsed[:name].should == '個人職業なし'
    parsed[:race].should == 'ただの人'
    parsed[:jobs].should == ['謎１','謎２','謎３']
  end

  it "該当するキャラクターが存在しない" do
    @pjr.source = <<EOT
00-00000-00:該当なし:ただの人＋謎＋謎＋謎
EOT
    @pjr.parse_all

    result = @pjr.check(@pjr.source_data[0])
    result.should be_nil
  end

  it "該当するキャラクターが存在し、登録すべき個人職業アイドレスが存在しない。" do
    @pjr.source = <<EOT
01-00001-01:個人職業なし:森国人＋マッドサイエンティスト＋医師＋名医
01-00002-01:個人職業なし:森国人＋マッドサイエンティスト＋名医＋名医
EOT
    @pjr.parse_all

    result = @pjr.check(@pjr.source_data[0])
    result.should == true
    result = @pjr.check(@pjr.source_data[1])
    result.should == true


    @pjr.register_all

    ch1 = Character.find_by_character_no('01-00001-01')
    ch1.owner_account.job_idresses[0].inspect_idresses.should == '森国人＋マッドサイエンティスト＋医師＋名医'

    ch2 = Character.find_by_character_no('01-00002-01')
    ch2.owner_account.job_idresses[1].inspect_idresses.should == '森国人＋マッドサイエンティスト＋名医＋名医'
  end

  it "該当するキャラクターが存在し、登録すべき個人職業アイドレスが既に存在する。" do
    @pjr.source = <<EOT
01-00002-01:個人職業ありし:森国人＋マッドサイエンティスト＋医師＋名医
EOT
    @pjr.parse_all

    result = @pjr.check(@pjr.source_data[0])
    result.should == false
  end

  it '任意の個人職業アイドレスを登録できる。' do
    @pjr.source = <<EOT
01-00001-01:個人職業なし:森国人＋マッドサイエンティスト＋医師＋名医
01-00002-01:個人職業なし:森国人＋マッドサイエンティスト＋名医＋名医
EOT
    @pjr.parse_all

    result = @pjr.register(@pjr.source_data[0])
    result.should_not be_nil

    ch1 = Character.find_by_character_no('01-00001-01')
    ch1.owner_account.job_idresses[0].inspect_idresses.should == '森国人＋マッドサイエンティスト＋医師＋名医'

  end
  it '存在するアイドレス名称かを事前に検証する。' do
    param = {:race => 'かぎりなく人に近いなにか',:jobs => ['謎１','謎２','謎３']}
    @pjr.validate(param).should == false

    param = {:race => '森国人',:jobs =>  ['マッドサイエンティスト','謎２','謎３']}
    @pjr.validate(param).should == false

    param = {:race => '森国人',:jobs => ['マッドサイエンティスト','名医','医師']}
    @pjr.validate(param).should == true
  end
end

