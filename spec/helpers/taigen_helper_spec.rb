# -*- encoding: utf-8 -*-
#ENV["RAILS_ENV"] = "test"
#require File.expand_path(File.dirname(__FILE__) + "/../../config/environment")

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
require 'i_language/calculator'

DatabaseCconfig = YAML.load_file(File.join(Rails.root,'config/database.yml') )
class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end

describe TaigenHelper do
  include ApplicationHelper
  include TaigenHelper
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::JavaScriptHelper
  include ActionView::Helpers::CaptureHelper

  def params
    @params
  end
  before(:each) do
    @params={}
  end

  it "should desc" do
    pending
    source =<<EOT
_define_I=D: {
チップ２_国有：チップボール,
（Ｐ）18-00426-01_タルク：詩歌の民＋犬妖精＋銃士隊＋竜士隊＋補給士官：敏捷+1*知識+1*幸運+1
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00426-01%A1%A7%A5%BF%A5%EB%A5%AF [^]
}；
EOT
    context = Context.new
    troop = context.parse_troop(source)

    chip = troop.children[0]
    result = division_tab_content(troop)
    puts result
    result = inspect_troop_cost(troop)
    result.should_not be_nil
    puts result
  end

  it 'test' do
    pending
    source = <<EOT
＃！有効条件：契約した王と距離１０ｍ以内で一緒に行動する間の
＃！有効条件：盾によって

＃！個別付与：大型ＰＣ，全ユニット
＃！除外：大型ＰＣ，｛32-00644-01_鴻屋　心太，ザ・グレート・エビゾー｝

＃！個別付与：藩王の指揮，全ユニット
＃！除外：藩王の指揮，32-00622-01_セントラル越前

＃！有効条件：＜藩王の指揮＞に関する，｛本隊，越前情報部（降車），藩王分隊｝

32-00622-01_セントラル越前：東国人＋藩王＋ギーク＋ネットワークエンジニア：知識+1*幸運+1;
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00622-01%A1%A7%A5%BB%A5%F3%A5%C8%A5%E9%A5%EB%B1%DB%C1%B0

32-00644-01_鴻屋　心太：東国人＋サイボーグ＋大剣士＋王：敏捷+1*知識+1*幸運+1;

_define_I=D:{
ザ・グレート・エビゾー：チップボール：体格+4/* Ｔ１５巨大工廠生産品につき */,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋大剣士＋剣：敏捷+1*知識+1*幸運+1;
－カトラス：個人所有：白兵距離戦闘行為，歩兵，条件発動，（白兵距離での）｛攻撃，防御，移動｝、評価＋２。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00643-01%A1%A7%CC%EB%C6%E5%C5%F6%CB%E3
－王の契約による強化：契約した王と距離１０ｍ以内で一緒に行動する間の）全判定、評価＋３。
－鴻屋王の特産品による強化：（＜王の特産品による強化の特殊能力＞で選ばれた）全判定、評価＋２。＃「白兵戦の全判定に＋２」とする。
};

＃！特殊無効：カトラスの白兵距離戦闘行為補正，ザ・グレート・エビゾー


＃元々の「王の特産品による強化」特殊に基づき、太元で白兵戦補正が付くよう、専用の定義パッチを作成。
Ｌ：鴻屋王の特産品による強化　＝　｛
　ｔ：名称　＝　鴻屋王の契約による強化（定義）
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊鴻屋王の特産品による強化の定義カテゴリ　＝　，，，判定補正。
　　＊鴻屋王の特産品による強化の特殊能力　＝　，，，魂の故郷の特産品に由来する力を契約した剣に与えることができ、その力に応じた判定を選ぶことができる。
　　＊鴻屋王の特産品による強化の特殊補正　＝　，，条件発動，（白兵距離での）全判定、評価＋２。
　　＊鴻屋王の特産品による強化の効果範囲　＝　，，，契約した王と距離１０ｍ以内で一緒に行動する間。
　｝
｝

_define_division:{越前情報部（降車）：電子妖精軍,
32-00622-01_セントラル越前，
32-00644-01_鴻屋　心太,
32-00643-01_夜薙当麻
};

_define_division:{越前情報部・剣王分隊,
32-00644-01_鴻屋　心太,
ザ・グレート・エビゾー
};

_define_division:{越前情報部・剣王分隊（降車）,
32-00644-01_鴻屋　心太,
32-00643-01_夜薙当麻
};
EOT
    context = Context.new
    troop = context.parse_troop(source)
    puts result = troop.evals_str
    result.should_not be_nil

    for dv in troop.divisions
      puts dv.evals_str
    end

    dvken = context.units['越前情報部・剣王分隊']
    ebizo = dvken.children[1]
    melee = Evaluator['白兵戦']
#    ebizo.children[0].evaluate('知識').should == -1
    melee.evaluate(ebizo).should == 27
  end

  it 'test2' do
    source = <<EOT
＃！有効条件：｛艦船，宇宙艦船，航空機，Ｉ＝Ｄ｝にコパイロットとして搭乗している場合での
_define_vessel:{
１番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）42-00438-02_深織志岐：北国人＋テストパイロット＋帝國軍歩兵＋整備士２：知識+1*幸運+1 ；
（ＣＰ）18-xx026-xx_リトルリトルボイコット：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx011-xx_戸隠　一葉(とがくし　かずは)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
_define_vessel:{
２番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx013-xx_うーた：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx019-xx_郁宮(いく)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx020-xx_羽月(はつき)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
３番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx014-xx_リン：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx027-xx_双樹　七都(ふたき　なつ)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx028-xx_仁村　八雲(にむら　やくも)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
EOT
    context = Context.new
    troop = context.parse_troop(source)

    result = division_tab_content(troop)
    puts result
    result.should_not be_nil
  end

  it 'test3' do
    pending
    source = <<EOT
Ｌ：船乗り　＝　｛
　ｔ：名称　＝　船乗り（職業）
　ｔ：要点　＝　髪を隠す帽子か布，艦剣，膝までのズボン
　ｔ：周辺環境　＝　帆船
　ｔ：評価　＝　体格１，筋力２，耐久力０，外見２，敏捷２，器用３，感覚３，知識１，幸運０
　ｔ：特殊　＝　｛
　　＊船乗りの職業カテゴリ　＝　，，，派生職業アイドレス。
　　＊船乗りの位置づけ　＝　，，，｛パイロット系，コパイロット系｝。
　　＊船乗りのパイロット資格　＝　，，，搭乗可能（艦船系）。
　　＊船乗りのコパイロット資格　＝　，，，搭乗可能（艦船系）。
　　＊船乗りの緊急輸送能力　＝　，，，ボートで２万ｔの物資を緊急輸送することができる。
＃　　＊船乗りの搭乗補正　＝　，搭乗，条件発動，（位置づけ（艦船系）の乗り物に搭乗している場合での）全判定、評価＋２。
　　＊船乗りの艦上補正　＝　，搭乗，条件発動，（位置づけ（艦船系）の乗り物の艦上で活動する場合での）全能力、評価＋２。
　｝
　ｔ：→次のアイドレス　＝　赤鮭（ＡＣＥ），青カモメ（ＡＣＥ），海の魔女（職業），海賊（職業），冒険家（職業）
｝

Ｌ：海賊　＝　｛
　ｔ：名称　＝　海賊（職業）
　ｔ：要点　＝　頭布，日焼け，鍛えた体
　ｔ：周辺環境　＝　海
　ｔ：評価　＝　体格２，筋力３，耐久力２，外見０，敏捷３，器用３，感覚３，知識１，幸運０
　ｔ：特殊　＝　｛
　　＊海賊の職業カテゴリ　＝　，，，派生職業アイドレス。
　　＊海賊の位置づけ　＝　，，，｛パイロット系，コパイロット系｝。
　　＊海賊のパイロット資格　＝　，，，搭乗可能（艦船系）。
　　＊海賊のコパイロット資格　＝　，，，搭乗可能（艦船系）。
　　＊海賊の緊急輸送能力　＝　，，任意発動，ボートで２万ｔの物資を緊急輸送することができる。
　　＊海賊の搭乗補正　＝　，搭乗，条件発動，（位置づけ（艦船系）の乗り物に搭乗している場合での）全判定、評価＋２。
　　＊海賊の艦上補正　＝　，搭乗，条件発動，（位置づけ（艦船系）の乗り物の艦上で活動する場合での）全能力、評価＋１。
　｝
　ｔ：→次のアイドレス　＝　赤鮭?（ＡＣＥ），富良野?（ＡＣＥ），海の魔女?（職業），船長?（職業），水上艦船・私略船の建造（イベント）
｝

＃！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
_define_vessel:{
フリゲート：ムーン級パトロールフリゲート,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
};
EOT

    context = Context.new
    troop = context.parse_troop(source)

    result = division_tab_content(troop)
    puts result
    result.should_not be_nil
  end

  it '付与系能力がある場合、その特殊を選別して表示する。' do
    pending
    source = <<EOT
king:藩王＋名整備士；
－高機能ハンドヘルド
EOT

    context = Context.new
    troop = context.parse_troop(source)
    all_sp = collect_all_sp(troop)
    specifications_content = select_specification(all_sp)
    specifications_content.should include_text( '＊高機能ハンドヘルドの形状　＝　，，，コンピュータ。')
    etc_content = sp_content( select_etc_sp(all_sp) )
    etc_content.should_not include_text( '＊高機能ハンドヘルドの形状　＝　，，，コンピュータ。')
    etc_content.should_not include_text( '＊藩王の指揮能力　＝　，，，藩王自身及び藩王搭乗機を除く藩王の指揮を受ける部隊に＜藩王の指揮＞を付与することができる。')
    action_content = sp_content(select_action_sp(all_sp))
    action_content.should_not include_text('＊名整備士のチューニング行為')

    enhancement_content = sp_content(select_enhancement_sp(all_sp))
    enhancement_content.should include_text('＊藩王の指揮能力')
    enhancement_content.should include_text('＊名整備士のチューニング行為')
  end

  it 'geek' do
    source = <<EOT
soldier:歩兵+ギーク;
EOT
    context = Context.new
    troop = context.parse_troop(source)
    result = division_tab_content(troop)
    result.should_not be_nil
  end

  it 'ACE搭乗編成' do
    source = <<EOT
＃！有効条件：＜銀内ユウ＞を帯同しない場合での
_define_I=D:{
ユーフォー２：多国籍要撃機”ユーフォー”,
－多国籍要撃機”ユーフォー”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）
（Ｐ）05-XX009-xx_資源猫士：南国人＋学生＋パイロット
（ＣＰ）銀内優斗３：銀内優斗３;
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00141-01%A1%A7%B6%E4%C6%E2%A1%A1%A5%E6%A5%A6
};
EOT
    context = Context.new
    troop = context.parse_troop(source)
    result = division_tab_content(troop)
    result.should_not be_nil

    source2 = <<EOT
_define_I=D:{
バンバンジーバイク_玄霧弦耶所有：バンバンジー・バイク,
(P)11-00230-01_玄霧弦耶：玄霧+ＨＱ：,
－アンデットバスター：個人所有：対アンデット攻撃＋５（着用型／両手武器／両手）
－蛇の指輪２：個人所有：大神官の聖別により、耐久＋１（着用型／指輪／右手先）
－玄霧の指輪：個人所有：死期を一日延ばす（着用型／結婚指輪／左手先）
－英雄達のオルゴール：愛を思い出したとき、熱血反応と超辛反応を起こす。勝利の暗号が隠されている。
－残念賞メダル：ダイスを振った際、出目を±１０の範囲で変更できる。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki /index.php?00230-01%A1%A7%B8%BC%CC%B8%B8%B9%CC%ED
}；

＃！評価無効：白兵戦
＃！評価無効：近距離戦
＃！評価無効：遠距離戦

＃！状況：山岳・森林
＃！有効条件：｛山岳，森林｝での
EOT

    context = Context.new
    troop = context.parse_troop(source2)
    result = division_tab_content(troop)
    result.should_not be_nil
  end
  it '評価式定義' do
    source = <<EOT
Ｌ：評価定義（中距離戦合計）　＝　｛
　ｌ：式　＝　3+4
　ｌ：行為分類　＝　中距離戦闘行為
｝

soldier１:歩兵；
soldier２：帝國軍歩兵；
EOT

    context = Context.new
    troop = context.parse_troop(source)
    result = division_tab_content(troop)
    result.should_not be_nil

    ev = troop.find_evaluator('中距離戦合計')
    calc = ev.calculate(troop)
    inspector = calc.create_inspectors
    result = inspector.actions_details_text
    result.should match(/3 \+ 4 = 7/)
    puts result

    source = <<EOT
Ｌ：評価定義（中距離戦合計）　＝　｛
　ｌ：式　＝　3+4
｝

soldier１:歩兵；
soldier２：帝國軍歩兵；
EOT
    context = Context.new
    troop = context.parse_troop(source)
    result = division_tab_content(troop)
    result.should_not be_nil
  end


  it 'リソース消費修正' do
    source = <<EOT
38-00444-02：ヴィザ：暁の民＋ドラゴンスレイヤー＋王＋ライオンハート＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00435-02：枚方　弐式：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+3*幸運+3；
－ドラグンバスター：国有：白兵攻撃＋６、白兵攻撃の燃料を消費しない（着用型／その他）
EOT

    context = Context.new
    troop = context.parse_troop(source)
    troop.action_evals_str

    maikata = troop.children[1]
    sp=maikata.find_spec('大剣士の白兵距離戦闘補正')
    result1=inspect_resource_modification(maikata,sp)
    puts result1
    result1.should =~ /燃料 \* 0/
    result2 = inspect_sp_cost(troop)
    puts result2
    result2.should =~ /燃料 \* 0/
  end

  it '部隊補正付きのドラゴンシンパシー' do
    source = <<EOT
ｌ：＊ラビプラスの全能力補正　＝　乗り物，，（着用したＩ＝Ｄの）全能力、評価＋４。
ｌ：＊フライトユニットの器用補正　＝　，，器用、評価－１０。
ｌ：＊フライトユニットの感覚補正　＝　乗り物，，感覚、評価＋２。

＃！有効条件：着用したＩ＝Ｄの
＃！個別付与：教導パイロットの指導，【本隊】

_define_I=D:{うさ：ｈｉｎ－にゅーうささん，
－ラビプラス
－増加燃料装備
－フライトユニット
(p)pilot:名パイロット＋摂政＋ドラゴンシンパシー；
｝；
EOT
    context = Context.new
    troop = context.parse_troop(source)
    troop.action_evals_str

    calc = Evaluator['ドラゴンシンパシー'].calculate(troop)
    inspectors = calc.create_inspectors
    puts inspectors.actions_details_text
#    result = division_tab_content(troop)
#    result.should_not be_nil
  end

  it '随行ユニットのみの評価' do
    source = <<EOT
Ｌ：電子妖精（レンジャー版）の情報防壁　＝　｛
　ｔ：名称　＝　電子妖精（レンジャー版）の情報防壁（ＮＰＣ）
　ｔ：評価　＝　情報戦防御１５
　ｔ：特殊　＝　｛
　　＊電子妖精（レンジャー版）の情報防壁のＮＰＣカテゴリ　＝　，，逗留ＮＰＣ。
　｝
｝

Ｌ：評価定義（情報戦防御）　＝　｛
　ｌ：引数　＝情報戦防御
　ｌ：補正対象　＝　情報戦闘
　ｌ：補正使用条件　＝　なし
　ｌ：評価分類　＝　情報戦防御
｝

ｌ：随行ユニットが提出可能な評価分類　←　｛'装甲'，'情報戦防御'｝

＃！部隊補正：電子妖精（レンジャー版）

hacker1:愛の民+猫妖精２+ハッカー+スターファイター
hacker2:愛の民+猫妖精２+ハッカー+スターファイター

（随行）firewall:電子妖精（レンジャー版）の情報防壁;
EOT
    context = Context.new
    troop = context.parse_troop(source)

    result = division_tab_content(troop)
    puts result
    result.should_not be_nil
  end
  it 'バンドユニットのみの評価' do
    source = <<EOT
Ｌ：スカールド　＝　｛
　ｔ：名称　＝　スカールド（乗り物）
　ｔ：要点　＝　いかにも安そう，展開式の楯，鈍重な
　ｔ：周辺環境　＝　地下
　ｔ：評価　＝　体格３５，筋力２２，耐久力３５，外見２，敏捷９，器用７，感覚４，知識５，幸運１５
　ｔ：特殊　＝　｛
　　＊スカールドの乗り物カテゴリ　＝　，，Ｉ＝Ｄ。
　　＊スカールドのイベント時燃料消費　＝　，，（戦闘イベント参加時）燃料－５万ｔ。
　　＊スカールドのイベント時資源消費　＝　，，（戦闘イベント参加時）資源－２万ｔ。
　　＊スカールドの必要パイロット数　＝　，，パイロット０名。＃無人機
　　＊スカールドの局地活動能力　＝　，，宇宙。
　　＊スカールドの防御補正　＝　，条件発動，防御、評価＋１６。
　　＊スカールドの人機数　＝　，，１０人機。
　　＊スカールドのアタックランク　＝　，，ＡＲ１５。
　　＊スカールドの出撃制限　＝　，，出撃の際にバンドしなければならず、以後、そのバンド先についていく。分割はできない。
　　＊スカールドの防御特性　＝　，，スカールドは大型の楯であり、防御判定時にはバンド対象の部隊を覆うように展開される。
　　＊スカールドの防御性能　＝　，，スカールドは使い捨ての盾として設計されており、防御判定に１回失敗したとしても本体が破損するだけでバンド対象の部隊は無傷である。
　｝
　ｔ：→次のアイドレス　＝　Ｉ＝Ｄ・ソードマンの開発（イベント）
｝

soldier1:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；
soldier2:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；

（バンド）スカールド01：スカールド；
EOT
    context = Context.new
    troop = context.parse_troop(source)

    result = division_tab_content(troop)
    puts result
    result.should_not be_nil
    result = inspect_troop_cost(troop)
    result.should_not be_nil
    puts result
  end

  it 'オプション装備' do
    source = <<EOT
_define_I=D:{
一号機：ラビプラス,
－ラビプラスのブースター
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
EOT
    context = Context.new
    troop = context.parse_troop(source)

    result = division_tab_content(troop)
    puts result

  end

  it '無人艦船' do
    source = <<EOT
＃！評価派生：反撃での，対空戦闘
＃！有効条件：反撃する場合

_define_vessel:{
０１ツバキ_組織所有：フラワー級宇宙駆逐艦+HQ装甲，
};
EOT
    context = Context.new
    troop = context.parse_troop(source)

    result = nil

    proc{result = division_tab_content(troop)}.should_not raise_error
    puts result
  end
end

