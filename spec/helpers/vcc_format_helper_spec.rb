# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'
include ApplicationHelper

class IDefinition  #TODO ターン別対応
  establish_connection :development
end
describe VccFormatHelper do
  fixtures :characters , :races , :yggdrasills , :job_idresses , :owner_accounts , :nations,:object_registries

  it 'format character line' do
    ch  = Character.find_by_character_no('01-00002-01')
    result = format_soldier(:number => ch.character_no , :name => ch.name ,
      :job_idress => '森国人+医師+名医+マッドサイエンティスト' , :personal_idress => nil,
      :belongs => '藩国部隊')
    puts result
  end

  it 'format item lines' do
    ch  = Character.find_by_character_no('01-00002-01')
    result = format_item_mod_str(ch)
    result.should match(/耐久力\+1/)
    result.should match(/筋力\+2/)
    result.split('*').size.should == 2

    ch2 = Character.find_by_character_no('01-00001-01')
    format_item_mod_str(ch2).should == ''
  end
end
