# encoding : utf-8

FactoryGirl.define do
  factory :nation do
    id 1
    name 'テスト藩国'
    after(:create) do |n|
      create(:evasion_master , owner_account: n.owner_account)
      create(:mantle , owner_account: n.owner_account)
    end

  end

  factory :zero_nation do


  end
end