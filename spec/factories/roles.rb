# encoding : utf-8

FactoryGirl.define do
  factory :admin_role , class: Role do
    name 'admin'
    jname '管理者'
  end

  factory :herald_role, class: Role do
    name 'herald'
    jname '紋章官'
  end

  factory :accountant_role , class: Role do
    name 'accountant'
    jname '金庫番'
  end
  factory :item_officer_role, class: Role do
    name 'item_officer'
    jname 'アイテム図鑑'
  end
  factory :monju_maintainer_role , class: Role do
    name 'monju_maintainer'
    jname '文殊公社運用部'
  end

end