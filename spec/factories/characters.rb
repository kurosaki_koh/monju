# encoding : utf-8

FactoryGirl.define do
  factory :character do
    character_no '01-00000-00'
    name 'test PC'
    nation_id 1
    after(:create) do |ch|
      create(:yggdrasill, owner_account: ch.owner_account)
    end

    factory :king do
      character_no '01-00010-01'
      name 'king'
      nation_id 1
      qualification '藩王１級'
    end
  end



end
