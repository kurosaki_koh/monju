# encoding : utf-8

FactoryGirl.define do
  factory :ace do
    character_no '00-00000-00'
    name '蒼の忠孝'
    hurigana 'あおのただたか'
  end
end
