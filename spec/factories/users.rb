# encoding : utf-8

FactoryGirl.define do
  factory :user do
    login 'test'
    email 'test@test'
    role ''
    name 'test'

    password 'test'
    password_confirmation 'test'

    player_no '00000'
  end

end