# encoding : utf-8

FactoryGirl.define do
  factory :owner_account do
    owner_type 'Character'
    miles 0
    number '00-00000-00'
    name 'test PC'
    character
  end

  factory :owner_account2 , class: OwnerAccount do
    id 2
    owner_type 'Character'
    miles 0
    number '00-00002-00'
    name 'test2'

  end

  factory :king_account , class: OwnerAccount do
    id 3
    owner_type 'Character'
    miles 0
    number '00-00010-01'
    name 'はんおう'

  end
end

