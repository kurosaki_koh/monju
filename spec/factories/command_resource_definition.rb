# encoding : utf-8

FactoryGirl.define do
  factory :command_resource_definition do
    name 'シオネの小像'
  end

  factory :included_command_resource_definition , :class => :command_resource_definition do
    included_def
    sequence(:name) {|n| "コマンドリソース＃#{n}"}
    sequence(:power){|n| n}
    tag '装甲'
  end

end
