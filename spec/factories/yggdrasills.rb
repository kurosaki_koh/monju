# encoding : utf-8

FactoryGirl.define do
  factory :yggdrasill do
    name '竜牙の使い手'
    idress_name '竜牙の使い手'
    basis_url 'http://dummy.url/for/test'
    owner_account
  end



  factory :evasion_master , class: Yggdrasill do
    name '回避の達人'
    idress_name '回避の達人'
  end

  factory :ochitsuke , class: Yggdrasill do
    name 'まあまて落ち着け'
    idress_name 'まあまて落ち着け'
  end



  factory :pocket_pique , :class => Yggdrasill do
    name 'ポケットピケ'
    idress_name  'ポケットピケ'
    owner_account
  end

  factory :mantle , :class => Yggdrasill do
    name '風切りのマント'
    idress_name  '風切りのマント'
    owner_account
  end

  factory :include_test , :class => Yggdrasill do
    name 'インクルードテスト用アイドレス'
    idress_name 'インクルードテスト用アイドレス'
    owner_account
  end


end