# encoding : utf-8

FactoryGirl.define do
  factory :object_registry do
    name 'ポケットピケ'
    number 1
    object_definition
  end

  factory :statue_of_sione do
    name 'シオネの小像'
    number 1
    association :object_definition , factory: :statue_of_sione_def
  end

  factory :mantle_reg do
    name '風切りのマント'
    number 1
    association :object_definition , factory: :mantle_def
  end

end