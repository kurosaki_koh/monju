# encoding : utf-8

FactoryGirl.define do
  factory :small_field_def , class: CommonDefinition do
    name '小さい畑'
    definition '小さい畑：１０：＃食料'
  end

  factory :dragon_tooth_warrior , class: CommonDefinition do
    name '竜牙の使い手'
    object_type '職業'
    definition <<EOT
竜牙の使い手（職業）：１５：＃白兵
要点
・竜牙の骸骨兵：２０：＃装甲
・骸骨兵労働：１５：＃陣地
・牙のせんじ薬：１０：＃耐久
・集中攻撃命令：１５：＃白兵
エクストラ
・盾にして逃げる：１５：＃敏捷
次のアイドレス
・竜牙の達人：１５：＃白兵
・ネクロマンサー：１５：＃詠唱
EOT
  end

  factory :evasion_master_def , class: CommonDefinition do
    name '回避の達人'
    object_type '職業'
    definition <<EOT
回避の達人（職業）：１５：＃装甲
要点
・素早い動き：１０：＃敏捷
・潜り抜け：１５：＃装甲
・ジャンプ：１０：＃敏捷
・捨てて身軽に：１０：＃敏捷
エクストラ
・デンプシーロール：１５：＃白兵
次のアイドレス
・ボクサー：１０：＃白兵
・よけティーチャー：１５：＃装甲
EOT
  end
  factory :object_definition do
    name 'ポケットピケ'
    definition <<EOT
ポケットピケ（アイテム）：３０：＃敏捷
・ドライブ：２５：＃航路移動
・たたんで盾に：２５：＃装甲
・脱出：２５：＃装甲
・最大出力：２５：＃飛行
EOT
    object_type 'アイテム'
  end

  factory :pocket_pique_def  , class: CommonDefinition do
    name 'ポケットピケ'
    definition <<EOT
ポケットピケ（アイテム）：３０：＃敏捷
・ドライブ：２５：＃航路移動
・たたんで盾に：２５：＃装甲
・脱出：２５：＃装甲
・最大出力：２５：＃飛行
EOT
    object_type 'アイテム'
  end

  factory :mantle_def , class: CommonDefinition do
    name '風切りのマント'
    definition <<EOT
風切のマント（アイテム）：１０：＃敏捷
・幻惑迷彩：１０：＃隠蔽
・跳躍：１０：＃敏捷
・マント受け：１０：＃装甲
・揺れる影：１０：＃外見
EOT
    object_type 'アイテム'
  end

  factory :statue_of_sione_def , class: CommonDefinition do
    name 'シオネの小像'
    definition 'シオネの小像（アイテム）：５０：＃希望'
  end

  factory :cat  , class: CommonDefinition do
    name '個人用猫士'
    definition '個人用猫士'
    object_type 'アイテム'
  end

  factory :included_def , class: CommonDefinition do
    name 'インクルードテスト用アイドレス'
    definition <<EOT
インクルードテスト用定義：１０：＃テスト
・コマンドリソース1：１：＃装甲
・コマンドリソース2：２：＃装甲
・コマンドリソース3：３：＃装甲
・コマンドリソース4：４：＃装甲
・コマンドリソース5：５：＃装甲
・コマンドリソース6：６：＃装甲
・コマンドリソース7：７：＃装甲
・コマンドリソース8：８：＃装甲
・コマンドリソース9：９：＃装甲
EOT
    object_type 'アイテム'
  end
end