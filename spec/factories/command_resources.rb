# encoding : utf-8

FactoryGirl.define do
  factory :command_resource do
    include_test
    owner_account
    included_command_resource_definition
  end
end
