# -*- encoding: utf-8 -*-

require 'spec_helper'

DatabaseCconfig = YAML.load_file(File.join(Rails.root.join('config','database.yml') ) )
class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end
describe UnitRegistriesController ,  :type => :controller do
  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    @admin_role = create :admin_role

    @character = create(:character)
    @king = create(:king)
    controller.stub(:current_user).and_return(user)
    @owner_account2 = create(:owner_account2)
  end

  context 'ConvertSource' do
    let(:params){
      {
          unit_registry: {
              name: '迷宮FIX',
              note: '備考テスト',
              authority_url: 'http://cwtg.jp/bbs3/nisetre.cgi?no=5397' ,
              type: 'ConvertSource' ,
              owner_account_id: owner_account_id,
          },

          source: <<-EOT ,
          ＃！部分有効条件：位置づけ（情報系）の職業
                  ＃越前藩の電子妖精軍ＨＱを反映
                  ＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
                  ＃本来は部隊単位で与える大型PC・超大型PCの補正を、黒埼一名に与える。
                  ＃＃！個別付与：大型ＰＣの計算資源＋超大型ＰＣの計算資源，全ユニット
                  32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ネットワークエンジニア+法の司：敏捷+1*知識+4*幸運+1;
                  －大型ＰＣ：黒埼紘所有：情報戦＋５。（元から情報戦ができる職業の場合）さらに＋３。部隊内の情報戦行為可能要員に効果あり
                  －超大型ＰＣ：黒埼紘所有：。（元から情報戦ができる職業の場合）情報戦＋８。部隊内の情報戦行為可能要員に効果あり
                  －水流銃：個人所有：歩兵武装，，近距離戦闘行為が可能。 , 歩兵武装，条件発動，（射撃（銃）、近距離での）攻撃、評価＋２。属性（水）。片手持ち。
                  －猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
                  －法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。
                  ＃－電子妖精軍：藩国装備
                  －受付：リザルトで取得
                  －まあまて落ち着け：リザルトで取得
                  －個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9"
          EOT
          owner_account_id: owner_account_id
      }
    }
    shared_examples_for 'success to create a unit registry' do
      it 'succeed' do
        expect {
          post :create, params
        }.to change(UnitRegistry, :count).by(1)
      end
    end

    context '正常系' do
      let(:user){create(:user , roles: [@admin_role])}
      let(:owner_account_id) {@character.owner_account.id}

      it_should_behave_like 'success to create a unit registry'
      it 'create' do
        puts params.inspect
        post :create, params
#        @character.owner_account.unit_registries.reload
        puts @character.owner_account.unit_registries.first.to_yaml
        expect(@character.owner_account.unit_registries.first).to be_truthy
        expect(@character.owner_account.unit_registries.first.convert_definition).to be_truthy
        expect(@character.owner_account.unit_registries.first.convert_definition.name).to eq  '迷宮FIX'
        expect(@character.owner_account.unit_registries.first.convert_definition.object_type).to eq  'コンバート定義'
      end
    end

    describe '権限チェック' do
      context 'ログインなし' do
        let(:owner_account_id) {2}
        let(:user){:false}

        it '失敗' do
          expect {
            post :create, params
          }.to change(UnitRegistry, :count).by(0)
        end
      end

      context '権限なしでログイン' do
        let(:user){create(:user )}
        let(:owner_account_id) {2}

        it '失敗' do
          expect {
            post :create, params
          }.to change(UnitRegistry, :count).by(0)
        end
      end

      context '本人' do
        it_should_behave_like 'success to create a unit registry' do
          let(:user){create(:user )}
          let(:owner_account_id) {@character.owner_account.id}
        end
      end

      context '当該国華族' do
        let(:user){create(:user,player_no: '00010')}
        let(:owner_account_id) {@character.owner_account.id}
        it_should_behave_like 'success to create a unit registry'

        it 'is noble' do
          puts user.characters.first.inspect
          puts user.characters.first.qualification =~ /藩王.級/
          expect(user.characters.first.is_noble?).to be_truthy
        end
      end

      context '管理者' do
        let(:user){create(:user  , roles: [@admin_role])}
        let(:owner_account_id) {@character.owner_account.id}
        it_should_behave_like 'success to create a unit registry'
      end

    end
  end

  context 'IncludeRegistry' do
    before :each do
      @include_test = create :included_def
      @owner_account = @character.owner_account
      @owner_account.add_object_definition(@include_test)
    end
    let(:user){create(:user , roles: [@admin_role])}
    let(:owner_account_id) {@character.owner_account.id}
    let(:params){
      {
          owner_account_id: owner_account_id ,
          unit_registry: {
              name: 'インクルードテスト',
              note: 'テスト',
              authority_url: 'http://cwtg.jp/bbs3/nisetre.cgi?no=5397' ,
              type: 'IncludeRegistry' ,
              owner_account_id: @owner_account.id,
          },

          source:  {
              main_member: {
                  character_no: @owner_account.number ,
                  name: '黒埼紘'
              },
              members: {
                  character_no: '32-00622-01' ,
                  name: 'セントラル越前'
              },
              includes: [
                  {
                      command_resource: {
                          name: 'test' ,
                          power: @owner_account.command_resources.sum(&:power) ,
                          tag: '装甲'
                      } ,
                      sources: @owner_account.command_resources.pluck(:id)[0..8]
                  }
              ]
          }.to_json
      }
    }
    describe 'register IncludeRegistry' do
      it 'succeed' do
        expect {
          post :create, params
        }.to change(UnitRegistry, :count).by(1)
        puts @character.owner_account.unit_registries.inspect
        puts @character.owner_account.unit_registries.first.object_definition.definition
        expect(@character.owner_account.unit_registries.first).to be_truthy
        expect(@character.owner_account.unit_registries.first.object_definition).to be_truthy
        expect(@character.owner_account.unit_registries.first.object_definition.name).to eq  'インクルードテスト'
        expect(@character.owner_account.unit_registries.first.object_definition.object_type).to eq  'インクルード定義'
      end
    end
  end

end