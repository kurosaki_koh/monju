require File.dirname(__FILE__) + '/../spec_helper'

describe OrganizationsController do
  describe "handling GET /organizations" do

    before(:each) do
      @organization = mock_model(Organization)
      Organization.stub!(:find).and_return([@organization])
    end
  
    def do_get
      get :index
    end
  
    it "should be successful" do
      do_get
      response.should be_success
    end

    it "should render index template" do
      do_get
      response.should render_template('index')
    end
  
    it "should find all organizations" do
      Organization.should_receive(:find).with(:all).and_return([@organization])
      do_get
    end
  
    it "should assign the found organizations for the view" do
      do_get
      assigns[:organizations].should == [@organization]
    end
  end

  describe "handling GET /organizations.xml" do

    before(:each) do
      @organization = mock_model(Organization, :to_xml => "XML")
      Organization.stub!(:find).and_return(@organization)
    end
  
    def do_get
      @request.env["HTTP_ACCEPT"] = "application/xml"
      get :index
    end
  
    it "should be successful" do
      do_get
      response.should be_success
    end

    it "should find all organizations" do
      Organization.should_receive(:find).with(:all).and_return([@organization])
      do_get
    end
  
    it "should render the found organizations as xml" do
      @organization.should_receive(:to_xml).and_return("XML")
      do_get
      response.body.should == "XML"
    end
  end

  describe "handling GET /organizations/1" do

    before(:each) do
      @organization = mock_model(Organization)
      Organization.stub!(:find).and_return(@organization)
    end
  
    def do_get
      get :show, :id => "1"
    end

    it "should be successful" do
      do_get
      response.should be_success
    end
  
    it "should render show template" do
      do_get
      response.should render_template('show')
    end
  
    it "should find the organization requested" do
      Organization.should_receive(:find).with("1").and_return(@organization)
      do_get
    end
  
    it "should assign the found organization for the view" do
      do_get
      assigns[:organization].should equal(@organization)
    end
  end

  describe "handling GET /organizations/1.xml" do

    before(:each) do
      @organization = mock_model(Organization, :to_xml => "XML")
      Organization.stub!(:find).and_return(@organization)
    end
  
    def do_get
      @request.env["HTTP_ACCEPT"] = "application/xml"
      get :show, :id => "1"
    end

    it "should be successful" do
      do_get
      response.should be_success
    end
  
    it "should find the organization requested" do
      Organization.should_receive(:find).with("1").and_return(@organization)
      do_get
    end
  
    it "should render the found organization as xml" do
      @organization.should_receive(:to_xml).and_return("XML")
      do_get
      response.body.should == "XML"
    end
  end

  describe "handling GET /organizations/new" do

    before(:each) do
      @organization = mock_model(Organization)
      Organization.stub!(:new).and_return(@organization)
    end
  
    def do_get
      get :new
    end

    it "should be successful" do
      do_get
      response.should be_success
    end
  
    it "should render new template" do
      do_get
      response.should render_template('new')
    end
  
    it "should create an new organization" do
      Organization.should_receive(:new).and_return(@organization)
      do_get
    end
  
    it "should not save the new organization" do
      @organization.should_not_receive(:save)
      do_get
    end
  
    it "should assign the new organization for the view" do
      do_get
      assigns[:organization].should equal(@organization)
    end
  end

  describe "handling GET /organizations/1/edit" do

    before(:each) do
      @organization = mock_model(Organization)
      Organization.stub!(:find).and_return(@organization)
    end
  
    def do_get
      get :edit, :id => "1"
    end

    it "should be successful" do
      do_get
      response.should be_success
    end
  
    it "should render edit template" do
      do_get
      response.should render_template('edit')
    end
  
    it "should find the organization requested" do
      Organization.should_receive(:find).and_return(@organization)
      do_get
    end
  
    it "should assign the found Organization for the view" do
      do_get
      assigns[:organization].should equal(@organization)
    end
  end

  describe "handling POST /organizations" do

    before(:each) do
      @organization = mock_model(Organization, :to_param => "1")
      Organization.stub!(:new).and_return(@organization)
    end
    
    describe "with successful save" do
  
      def do_post
        @organization.should_receive(:save).and_return(true)
        post :create, :organization => {}
      end
  
      it "should create a new organization" do
        Organization.should_receive(:new).with({}).and_return(@organization)
        do_post
      end

      it "should redirect to the new organization" do
        do_post
        response.should redirect_to(organization_url("1"))
      end
      
    end
    
    describe "with failed save" do

      def do_post
        @organization.should_receive(:save).and_return(false)
        post :create, :organization => {}
      end
  
      it "should re-render 'new'" do
        do_post
        response.should render_template('new')
      end
      
    end
  end

  describe "handling PUT /organizations/1" do

    before(:each) do
      @organization = mock_model(Organization, :to_param => "1")
      Organization.stub!(:find).and_return(@organization)
    end
    
    describe "with successful update" do

      def do_put
        @organization.should_receive(:update_attributes).and_return(true)
        put :update, :id => "1"
      end

      it "should find the organization requested" do
        Organization.should_receive(:find).with("1").and_return(@organization)
        do_put
      end

      it "should update the found organization" do
        do_put
        assigns(:organization).should equal(@organization)
      end

      it "should assign the found organization for the view" do
        do_put
        assigns(:organization).should equal(@organization)
      end

      it "should redirect to the organization" do
        do_put
        response.should redirect_to(organization_url("1"))
      end

    end
    
    describe "with failed update" do

      def do_put
        @organization.should_receive(:update_attributes).and_return(false)
        put :update, :id => "1"
      end

      it "should re-render 'edit'" do
        do_put
        response.should render_template('edit')
      end

    end
  end

  describe "handling DELETE /organizations/1" do

    before(:each) do
      @organization = mock_model(Organization, :destroy => true)
      Organization.stub!(:find).and_return(@organization)
    end
  
    def do_delete
      delete :destroy, :id => "1"
    end

    it "should find the organization requested" do
      Organization.should_receive(:find).with("1").and_return(@organization)
      do_delete
    end
  
    it "should call destroy on the found organization" do
      @organization.should_receive(:destroy)
      do_delete
    end
  
    it "should redirect to the organizations list" do
      do_delete
      response.should redirect_to(organizations_url)
    end
  end
end