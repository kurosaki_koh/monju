require File.dirname(__FILE__) + '/../spec_helper'

describe MileChangesController do
  fixtures :mile_changes , :owner_accounts , :users

  before(:each) do
    session[:user]='1'
  end

  
  #Delete these examples and add some real ones
  it "should use MileChangesController" do
    controller.should be_an_instance_of(MileChangesController)
  end


  describe "GET 'create'" do
    it "should be redirect" do
      count = ModifyCommand.count
      count.should == 0
      get 'create' , :owner_account_id => 1 , :mile_change => {:mile => '5'}
      ModifyCommand.count.should == 1
      command = ModifyCommand.find(:first , :order => 'id desc')
      p command
      response.should be_redirect
    end
  end
end
