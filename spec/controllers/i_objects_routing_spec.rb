require File.dirname(__FILE__) + '/../spec_helper'

describe IObjectsController do
  describe "route generation" do

    it "should map { :controller => 'i_objects', :action => 'index' } to /i_objects" do
      route_for(:controller => "i_objects", :action => "index").should == "/i_objects"
    end
  
    it "should map { :controller => 'i_objects', :action => 'new' } to /i_objects/new" do
      route_for(:controller => "i_objects", :action => "new").should == "/i_objects/new"
    end
  
    it "should map { :controller => 'i_objects', :action => 'show', :id => 1 } to /i_objects/1" do
      route_for(:controller => "i_objects", :action => "show", :id => 1).should == "/i_objects/1"
    end
  
    it "should map { :controller => 'i_objects', :action => 'edit', :id => 1 } to /i_objects/1/edit" do
      route_for(:controller => "i_objects", :action => "edit", :id => 1).should == "/i_objects/1/edit"
    end
  
    it "should map { :controller => 'i_objects', :action => 'update', :id => 1} to /i_objects/1" do
      route_for(:controller => "i_objects", :action => "update", :id => 1).should == "/i_objects/1"
    end
  
    it "should map { :controller => 'i_objects', :action => 'destroy', :id => 1} to /i_objects/1" do
      route_for(:controller => "i_objects", :action => "destroy", :id => 1).should == "/i_objects/1"
    end
  end

  describe "route recognition" do

    it "should generate params { :controller => 'i_objects', action => 'index' } from GET /i_objects" do
      params_from(:get, "/i_objects").should == {:controller => "i_objects", :action => "index"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'new' } from GET /i_objects/new" do
      params_from(:get, "/i_objects/new").should == {:controller => "i_objects", :action => "new"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'create' } from POST /i_objects" do
      params_from(:post, "/i_objects").should == {:controller => "i_objects", :action => "create"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'show', id => '1' } from GET /i_objects/1" do
      params_from(:get, "/i_objects/1").should == {:controller => "i_objects", :action => "show", :id => "1"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'edit', id => '1' } from GET /i_objects/1;edit" do
      params_from(:get, "/i_objects/1/edit").should == {:controller => "i_objects", :action => "edit", :id => "1"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'update', id => '1' } from PUT /i_objects/1" do
      params_from(:put, "/i_objects/1").should == {:controller => "i_objects", :action => "update", :id => "1"}
    end
  
    it "should generate params { :controller => 'i_objects', action => 'destroy', id => '1' } from DELETE /i_objects/1" do
      params_from(:delete, "/i_objects/1").should == {:controller => "i_objects", :action => "destroy", :id => "1"}
    end
  end
end