# -*- encoding: utf-8 -*-

require 'spec_helper'

describe PackagedRegisterController do
  before(:each) do
    prepare_session
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    request.stub(:request_uri).and_return('http://maki.wanwan-empire.net/packaged_register/register_result')
    controller.stub(:login_required).and_return(true)
    @character = create(:character)
    @params = {results: {
        '1' => {
            character_no: '00-00000-00',
            name: 'test PC',
            idress: '小さい畑',
            note: 'BW01に参加',
            url: 'http://trpg-2maho.sakura.ne.jp/dva1/wforum/wforum.cgi?no=3708&reno=3707&oya=3535&mode=msgview&page=0'
        }
    }}
  end
  context '単体コマンドリソース' do

    before(:each) do
      @small_field = create(:small_field_def)
    end

    it 'register_result' do
      expect {
        post :register_result, @params
      }.to change(CommandResource, :count).by(1)
      expect(@character.owner_account.command_resources.first.available?).to eq true
    end

    it '同名コマンドリソースの重複はしない' do
      @character.owner_account.add_object_definition(@small_field)

      expect {
        post :register_result, @params
      }.to change(CommandResource, :count).by(0)
    end
  end

  shared_examples '非可算アイドレスのリザルト' do
    it 'イグドラシルのみ追加。' do
      expect(@character.owner_account.yggdrasills.size).to eq 2
    end
  end

  shared_examples 'コマンドリソースが増えない。' do
    it 'コマンドリソースは変化なし' do
      expect(@character.owner_account.command_resources.size).to eq 0
    end
  end

  shared_examples '取得済みイグドラシルを得た場合' do |idress_name|
    it '同名登録はイグドラシル追加を行わない。' do
      @params[:results]['1'][:idress] = idress_name
      expect{post :register_result, @params}.to change(Yggdrasill, :count).by(0)
    end
  end

  shared_examples '可算アイドレスのリザルト' do |idress_name|
    it 'イグドラシルとObjectRegistryに追加' do
      expect(@character.owner_account.yggdrasills.size).to eq 2
      expect(@character.owner_account.num_of_property(idress_name )).to eq 1
    end
  end

  context 'アイドレス２定義のリザルト' do
    before(:each) do
    end

    describe '技術アイドレスなら' do
      before(:each) do
        create :ochitsuke_def
        @params[:results]['1'][:idress] = 'まあまて落ち着け'
        post :register_result, @params
      end

      it_behaves_like '非可算アイドレスのリザルト'
      it_behaves_like 'コマンドリソースが増えない。'
      it_behaves_like '取得済みイグドラシルを得た場合' , 'まあまて落ち着け'

    end
    describe 'アイテムアイドレスなら' do
      before(:each) do
        create :chance_ball_def
        @params[:results]['1'][:idress] = 'チャンスボール'
        post :register_result, @params
      end

      it_behaves_like '可算アイドレスのリザルト'  , 'チャンスボール'
      it_behaves_like 'コマンドリソースが増えない。'
      it_behaves_like '取得済みイグドラシルを得た場合' , 'チャンスボール'

    end
  end

  context 'アイドレス３定義のリザルト' do
    describe '職業アイドレスなら' do

      before(:each) do
        create :evasion_master_def
        @params[:results]['1'][:idress] = '回避の達人'
        post :register_result, @params
      end

      it_behaves_like '非可算アイドレスのリザルト'

      it 'コマンドリソース＋８' do
        @character.owner_account.reload
        expect(@character.owner_account.command_resources.size).to eq 8
      end
    end
    describe 'アイテムアイドレスなら' do
      before(:each) do
        create :pocket_pique_def
        @params[:results]['1'][:idress] = 'ポケットピケ'
        post :register_result, @params
      end

      it_behaves_like '可算アイドレスのリザルト' , 'ポケットピケ'
      it 'コマンドリソース＋５' do
        @character.owner_account.reload
        expect(@character.owner_account.command_resources.size).to eq 5
      end
    end
  end

end

