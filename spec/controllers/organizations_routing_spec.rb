require File.dirname(__FILE__) + '/../spec_helper'

describe OrganizationsController do
  describe "route generation" do

    it "should map { :controller => 'organizations', :action => 'index' } to /organizations" do
      route_for(:controller => "organizations", :action => "index").should == "/organizations"
    end
  
    it "should map { :controller => 'organizations', :action => 'new' } to /organizations/new" do
      route_for(:controller => "organizations", :action => "new").should == "/organizations/new"
    end
  
    it "should map { :controller => 'organizations', :action => 'show', :id => 1 } to /organizations/1" do
      route_for(:controller => "organizations", :action => "show", :id => 1).should == "/organizations/1"
    end
  
    it "should map { :controller => 'organizations', :action => 'edit', :id => 1 } to /organizations/1/edit" do
      route_for(:controller => "organizations", :action => "edit", :id => 1).should == "/organizations/1/edit"
    end
  
    it "should map { :controller => 'organizations', :action => 'update', :id => 1} to /organizations/1" do
      route_for(:controller => "organizations", :action => "update", :id => 1).should == "/organizations/1"
    end
  
    it "should map { :controller => 'organizations', :action => 'destroy', :id => 1} to /organizations/1" do
      route_for(:controller => "organizations", :action => "destroy", :id => 1).should == "/organizations/1"
    end
  end

  describe "route recognition" do

    it "should generate params { :controller => 'organizations', action => 'index' } from GET /organizations" do
      params_from(:get, "/organizations").should == {:controller => "organizations", :action => "index"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'new' } from GET /organizations/new" do
      params_from(:get, "/organizations/new").should == {:controller => "organizations", :action => "new"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'create' } from POST /organizations" do
      params_from(:post, "/organizations").should == {:controller => "organizations", :action => "create"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'show', id => '1' } from GET /organizations/1" do
      params_from(:get, "/organizations/1").should == {:controller => "organizations", :action => "show", :id => "1"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'edit', id => '1' } from GET /organizations/1;edit" do
      params_from(:get, "/organizations/1/edit").should == {:controller => "organizations", :action => "edit", :id => "1"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'update', id => '1' } from PUT /organizations/1" do
      params_from(:put, "/organizations/1").should == {:controller => "organizations", :action => "update", :id => "1"}
    end
  
    it "should generate params { :controller => 'organizations', action => 'destroy', id => '1' } from DELETE /organizations/1" do
      params_from(:delete, "/organizations/1").should == {:controller => "organizations", :action => "destroy", :id => "1"}
    end
  end
end