# -*- encoding: utf-8 -*-
require 'spec_helper'

describe TaigenController ,:type => :controller do
#  controller_name :taigen
  before(:each) do
    @controller = TaigenController.new
  end

  it "should desc" do
    source = <<EOT
soldier:歩兵+ギーク;

EOT
    post(:update , :source => source)
  end


  it 'バンドユニットのみの評価' do
    source = <<EOT
Ｌ：スカールド　＝　｛
　ｔ：名称　＝　スカールド（乗り物）
　ｔ：要点　＝　いかにも安そう，展開式の楯，鈍重な
　ｔ：周辺環境　＝　地下
　ｔ：評価　＝　体格３５，筋力２２，耐久力３５，外見２，敏捷９，器用７，感覚４，知識５，幸運１５
　ｔ：特殊　＝　｛
　　＊スカールドの乗り物カテゴリ　＝　，，Ｉ＝Ｄ。
　　＊スカールドのイベント時燃料消費　＝　，，（戦闘イベント参加時）燃料－５万ｔ。
　　＊スカールドのイベント時資源消費　＝　，，（戦闘イベント参加時）資源－２万ｔ。
　　＊スカールドの必要パイロット数　＝　，，パイロット０名。＃無人機
　　＊スカールドの局地活動能力　＝　，，宇宙。
　　＊スカールドの防御補正　＝　，条件発動，防御、評価＋１６。
　　＊スカールドの人機数　＝　，，１０人機。
　　＊スカールドのアタックランク　＝　，，ＡＲ１５。
　　＊スカールドの出撃制限　＝　，，出撃の際にバンドしなければならず、以後、そのバンド先についていく。分割はできない。
　　＊スカールドの防御特性　＝　，，スカールドは大型の楯であり、防御判定時にはバンド対象の部隊を覆うように展開される。
　　＊スカールドの防御性能　＝　，，スカールドは使い捨ての盾として設計されており、防御判定に１回失敗したとしても本体が破損するだけでバンド対象の部隊は無傷である。
　｝
　ｔ：→次のアイドレス　＝　Ｉ＝Ｄ・ソードマンの開発（イベント）
｝

soldier1:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；
soldier2:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；

（バンド）スカールド01：スカールド；
EOT

    post(:update , :source => source)
  end

  it '太元API' do
    source = 'soldier:大剣士；'
    post(:compile , :source => source)

    p response
    response.should be_success
  end
end

