# -*- encoding: utf-8 -*-
require 'spec_helper'

describe SearchController do

  before(:each) do
    @controller = SearchController.new
  end
  
#  it '根源力指定でのキャラクター検索' do
#    get 'result' , :keyword => '根源力：500000以上'
#
#    response.should be_success
#    puts assigns[:results]
#  end

  it '根源力指定でのキャラクター検索2' do
    results = @controller.search_by_originator('根源力：400000以上')
    puts results.inspect
    text =  results.collect{|ch| "#{ch.character_no}:#{ch.name}:#{ch.sum_originator}"}
    puts text
  end

end
