require File.dirname(__FILE__) + '/../../spec_helper'

describe "/organizations/show.html.erb" do
  include OrganizationsHelper
  
  before(:each) do
    @organization = mock_model(Organization)
    @organization.stub!(:name).and_return("MyString")

    assigns[:organization] = @organization
  end

  it "should render attributes in <p>" do
    render "/organizations/show.html.erb"
    response.should have_text(/MyString/)
  end
end

