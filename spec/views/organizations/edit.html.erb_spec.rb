require File.dirname(__FILE__) + '/../../spec_helper'

describe "/organizations/edit.html.erb" do
  include OrganizationsHelper
  
  before do
    @organization = mock_model(Organization)
    @organization.stub!(:name).and_return("MyString")
    assigns[:organization] = @organization
  end

  it "should render edit form" do
    render "/organizations/edit.html.erb"
    
    response.should have_tag("form[action=#{organization_path(@organization)}][method=post]") do
      with_tag('input#organization_name[name=?]', "organization[name]")
    end
  end
end


