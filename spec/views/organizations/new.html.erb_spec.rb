require File.dirname(__FILE__) + '/../../spec_helper'

describe "/organizations/new.html.erb" do
  include OrganizationsHelper
  
  before(:each) do
    @organization = mock_model(Organization)
    @organization.stub!(:new_record?).and_return(true)
    @organization.stub!(:name).and_return("MyString")
    assigns[:organization] = @organization
  end

  it "should render new form" do
    render "/organizations/new.html.erb"
    
    response.should have_tag("form[action=?][method=post]", organizations_path) do
      with_tag("input#organization_name[name=?]", "organization[name]")
    end
  end
end


