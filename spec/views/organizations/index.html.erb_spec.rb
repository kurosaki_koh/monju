require File.dirname(__FILE__) + '/../../spec_helper'

describe "/organizations/index.html.erb" do
  include OrganizationsHelper
  
  before(:each) do
    organization_98 = mock_model(Organization)
    organization_98.should_receive(:name).and_return("MyString")
    organization_99 = mock_model(Organization)
    organization_99.should_receive(:name).and_return("MyString")

    assigns[:organizations] = [organization_98, organization_99]
  end

  it "should render list of organizations" do
    render "/organizations/index.html.erb"
    response.should have_tag("tr>td", "MyString", 2)
  end
end

