require File.dirname(__FILE__) + '/../../spec_helper'

describe "/i_objects/index.html.erb" do
  include IObjectsHelper
  
  before(:each) do
    i_objects_98 = mock_model(IObjects)
    i_objects_98.should_receive(:object_id).and_return("MyString")
    i_objects_98.should_receive(:name).and_return("MyString")
    i_objects_98.should_receive(:type).and_return("MyString")
    i_objects_99 = mock_model(IObjects)
    i_objects_99.should_receive(:object_id).and_return("MyString")
    i_objects_99.should_receive(:name).and_return("MyString")
    i_objects_99.should_receive(:type).and_return("MyString")

    assigns[:i_objects] = [i_objects_98, i_objects_99]
  end

  it "should render list of i_objects" do
    render "/i_objects/index.html.erb"
    response.should have_tag("tr>td", "MyString", 2)
    response.should have_tag("tr>td", "MyString", 2)
  end
end

