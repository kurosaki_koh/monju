require File.dirname(__FILE__) + '/../../spec_helper'

describe "/i_objects/new.html.erb" do
  include IObjectsHelper
  
  before(:each) do
    @i_objects = mock_model(IObjects)
    @i_objects.stub!(:new_record?).and_return(true)
    @i_objects.stub!(:object_id).and_return("MyString")
    @i_objects.stub!(:name).and_return("MyString")
    @i_objects.stub!(:type).and_return("MyString")
    assigns[:i_objects] = @i_objects
  end

  it "should render new form" do
    render "/i_objects/new.html.erb"
    
    response.should have_tag("form[action=?][method=post]", i_objects_path) do
      with_tag("input#i_objects_name[name=?]", "i_objects[name]")
      with_tag("input#i_objects_type[name=?]", "i_objects[type]")
    end
  end
end


