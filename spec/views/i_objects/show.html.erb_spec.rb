require File.dirname(__FILE__) + '/../../spec_helper'

describe "/i_objects/show.html.erb" do
  include IObjectsHelper
  
  before(:each) do
    @i_objects = mock_model(IObjects)
    @i_objects.stub!(:object_id).and_return("MyString")
    @i_objects.stub!(:name).and_return("MyString")
    @i_objects.stub!(:type).and_return("MyString")

    assigns[:i_objects] = @i_objects
  end

  it "should render attributes in <p>" do
    render "/i_objects/show.html.erb"
    response.should have_text(/MyString/)
    response.should have_text(/MyString/)
  end
end

