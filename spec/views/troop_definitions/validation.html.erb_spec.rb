# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../../spec_helper'

describe "troop_definitions/validation.html.erb" do
  include TroopDefinitionsHelper
#  render_views
#  before(:each) do
##    render 'troop_definitions/validation'
#    @troop_definition = mock_model(TroopDefinition)
#    @results = {}
#    assign[:results] = @results
#    assign[:troop_definition] = @troop_definition
#  end

  it "should render edit form" do
    source = <<EOT
＃太元登録までの措置
Ｌ：アルファン　＝　｛
　ｔ：名称　＝　アルファン（乗り物）
　ｔ：要点　＝　機体，足一体型，複数機
　ｔ：周辺環境　＝　宇宙
　ｔ：評価　＝　装甲１５，遠距離戦闘１５
　ｔ：特殊　＝　｛
　　＊アルファンの乗り物カテゴリ　＝　，，｛Ｉ＝Ｄ，人形｝。
　　＊アルファンの必要パイロット数　＝　，，パイロット０名。
　　＊アルファンの人機数　＝　，，２人機。
　　＊アルファンのアタックランク　＝　，，なし。
　　＊アルファンの活動制限　＝　，，｛宇宙、水中｝。
　　＊アルファンの遠距離戦闘行為　＝　，，遠距離戦闘行為が可能。＃遠距離戦闘評価：可能：（敏捷＋感覚）÷２
　　＊アルファンの自動迎撃能力　＝　，条件発動，敵が射程に入ると自動で１回の遠距離戦闘行為ができる。１００％制限。
　｝
　ｔ：→次のアイドレス　＝　小型無人機?（乗り物），群体制御ＡＩの開発?（イベント），デコイ運用戦術?（技術），Ｉ＝Ｄ用デコイばらまき装備?（アイテム）
｝

Ａｌ１_藩国所有：アルファン/*宰相府大規模工廠T17製品装甲+4評価*/：装甲+4；
Ａｌ２_藩国所有：アルファン/*宰相府大規模工廠T17製品装甲+4評価*/：装甲+4；
EOT
    td = TroopDefinition.new
    td.taigen_def = source
#    render "/troop_definitions/1/validation.html.erb"

    assign(:troop_definition,td)
    assign(:t_result, td.taigen_check)
    assign(:r_result, td.rule_check)
    
    
    response.should have_tag("form[action=#{i_objects_path(@i_objects)}][method=post]") do
      with_tag('input#i_objects_name[name=?]', "i_objects[name]")
      with_tag('input#i_objects_type[name=?]', "i_objects[type]")
    end
  end
end


