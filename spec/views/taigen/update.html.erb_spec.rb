# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../../spec_helper'
require 'i_language/calculator'

DatabaseCconfig = YAML.load_file(File.join(Rails.root,'config/database.yml') )
class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end

describe "/taigen/edit.html.erb" do
  include TaigenHelper
  
  before do
    @result = []
    @warnings = []
    @source_digest = 'dummy'
    @result_digest = 'dummy'
#    @i_objects = mock_model(IObjects)
#    @i_objects.stub!(:object_id).and_return("MyString")
#    @i_objects.stub!(:name).and_return("MyString")
#    @i_objects.stub!(:type).and_return("MyString")
#    assigns[:i_objects] = @i_objects
  end

  it "should render edit form" do
    context = Context.new
    @source = <<EOT
Ｌ：スカールド　＝　｛
　ｔ：名称　＝　スカールド（乗り物）
　ｔ：要点　＝　いかにも安そう，展開式の楯，鈍重な
　ｔ：周辺環境　＝　地下
　ｔ：評価　＝　体格３５，筋力２２，耐久力３５，外見２，敏捷９，器用７，感覚４，知識５，幸運１５
　ｔ：特殊　＝　｛
　　＊スカールドの乗り物カテゴリ　＝　，，Ｉ＝Ｄ。
　　＊スカールドのイベント時燃料消費　＝　，，（戦闘イベント参加時）燃料－５万ｔ。
　　＊スカールドのイベント時資源消費　＝　，，（戦闘イベント参加時）資源－２万ｔ。
　　＊スカールドの必要パイロット数　＝　，，パイロット０名。＃無人機
　　＊スカールドの局地活動能力　＝　，，宇宙。
　　＊スカールドの防御補正　＝　，条件発動，防御、評価＋１６。
　　＊スカールドの人機数　＝　，，１０人機。
　　＊スカールドのアタックランク　＝　，，ＡＲ１５。
　　＊スカールドの出撃制限　＝　，，出撃の際にバンドしなければならず、以後、そのバンド先についていく。分割はできない。
　　＊スカールドの防御特性　＝　，，スカールドは大型の楯であり、防御判定時にはバンド対象の部隊を覆うように展開される。
　　＊スカールドの防御性能　＝　，，スカールドは使い捨ての盾として設計されており、防御判定に１回失敗したとしても本体が破損するだけでバンド対象の部隊は無傷である。
　｝
　ｔ：→次のアイドレス　＝　Ｉ＝Ｄ・ソードマンの開発（イベント）
｝

soldier1:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；
soldier2:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；

（バンド）スカールド01：スカールド；
    
EOT
    @evals_str = ''
    begin
      @troop = context.parse_troop(@source)
      @evals_str = @troop.evals_str
    end
    render "/i_objects/edit.html.erb"

    response.should_not be_nil
#    response.should have_tag("form[action=#{i_objects_path(@i_objects)}][method=post]") do
#      with_tag('input#i_objects_name[name=?]', "i_objects[name]")
#      with_tag('input#i_objects_type[name=?]', "i_objects[type]")
#    end
  end
end


