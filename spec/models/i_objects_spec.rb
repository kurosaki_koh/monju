# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe IObject ,"Idress オブジェクトを扱った場合" do
  fixtures :owner_accounts , :i_objects

  describe IObject ,'兵器生産時' do
    before(:each) do
      @account = owner_accounts(:one)
      @pre_weapons = @account.weapons.dup
    end
    it '生産数に指定した数だけIObjectレコードが増える' do 
=begin
  ｆ：兵器を生産した場合＝｛
    引数←｛兵器を生産・追加した対象のOwnerAccount、兵器の名称、生産数｝
  　事前条件←｛
      兵器を所有する対象のOwenerAccount が存在する。
      生産数＞０である。    
    ｝
  　事後条件←｛
        同じ兵器の名称を持つIObjectレコードが生産数だけ生成される。
　　　　生成されたレコードは全て、引数に与えられたOwnerAccountと関連付けされる。
        生成されたレコードの name は、全て、引数に与えられた「兵器の名称」と等しくなる。
        生成されたレコードの type は、全て、':weapon'となる。
    ｝
  ｝
=end
      Weapon.register(@account , 'フェイクトモエリバー' , 3)

      post_weapons = @account.weapons 
      @pre_weapons.each{|w| post_weapons.delete(w)}
      post_weapons.size.should == 3
    
      for pw in post_weapons
        pw.name.should == 'フェイクトモエリバー'
        pw.class.should == Weapon
      end
    
    end

=begin
  ｆ：兵器生産数に０以下を指定された場合＝｛
    引数←｛兵器を生産・追加した対象のOwnerAccount、兵器の名称、生産数｝
  　事前条件←｛
      兵器を所有する対象のOwenerAccount が存在する。
      生産数＜０である。    
    ｝
  　事後条件←｛
      バリデーションエラーを返す。
    ｝
  ｝
=end
    it '兵器生産数に０以下を指定された場合' do
      proc{Weapon.register(@account , 'フェイクトモエリバー' , 0)}.should raise_error(ArgumentError)
    end
  end


  describe '兵器を破壊・市場に売却等で除籍した場合' do
    before(:each) do
      @account = owner_accounts(:one)
    end
=begin
  ｆ：兵器を破壊・市場に売却等で除籍しようとして、指定した数以下の保持数しかなかった場合＝｛
    引数←｛兵器の除籍を行う対象のOwnerAccountのID、兵器の名称、除籍数｝
  　事前条件←｛
           兵器を所有する対象のOwenerAccount が存在する。
           type == :weapon ,   name == 「兵器の名称」、owner_account_id == 「対象のOwnerAccountのID」であるレコードの数が、「除籍数」の数未満だけ存在する。｝
    事後条件←｛
          バリデーションエラーを返す。
      ｝
    不変条件←｛
         IObjectは何も変化しない。
         OwnerAccount は何も変化しない。
    ｝
  ｝
=end  
    it '兵器を破壊・市場に売却等で除籍しようとして、指定した数以下の保持数しかなかった場合' do
      proc{Weapon.unregister(@account , 'トモエリバー',10)}.should raise_error(RuntimeError)
    end
    
    #    pending('作業途中につき')

=begin
  ｆ：兵器を破壊・市場に売却等で除籍した場合＝｛
    引数←｛兵器の除籍を行う対象のOwnerAccountのID、兵器の名称、除籍数｝
  　事前条件←｛
           兵器を所有する対象のOwenerAccount が存在する。
           type == :weapon ,   name == 「兵器の名称」、owner_account_id == 「対象のOwnerAccountのID」であるレコードの数が、「除籍数」の数以上存在する。｝
    事後条件←｛
           type == :weapon ,   name == 「兵器の名称」、owner_account_id == 「対象のOwnerAccountのID」であるレコードの数が、「除籍数」の数だけ削除される。｝
      ｝
  ｝
=end  
    it '通常ケース' do
      p @account.weapons
      tomoes = @account.weapons.find_all_by_name('トモエリバー').dup
      proc{Weapon.unregister(@account , 'トモエリバー',2)}.should_not raise_error 
      post_tomoes = @account.weapons.find_all_by_name('トモエリバー').dup
      post_tomoes.size.should == (tomoes.size - 2 )
    end
  end
  #      proc{IObject::register_weapons(@account , 'フェイクトモエリバー' , 0)}.should raise_error(ArgumentError)

  describe '兵器の所属を移動した場合' do
    before(:each) do
      @account1 = owner_accounts(:one)
      @account2 = owner_accounts(:two)
    end

    it '兵器の所属を移動しようとして、移動元の兵器の数が「移動数」未満だった場合' do
=begin
  ｆ：兵器の所属を移動した場合＝｛
    引数←｛兵器の移動元の対象OwnerAccount.id 、兵器の移動先の対象OwnerAccount.id 、兵器の名称、移動数｝
　　事前条件←｛
      移動元のOwnerAccountが存在する。
      移動先のOwnerAccountが存在する。｝
      type == :weapon ,   name == 「兵器の名称」、owner_account_id == 「対象のOwnerAccountのID」であるレコードの数が、「除籍数」の数未満だけ存在する。
    ｝

    事後条件←｛
      バリデーションエラーを返す。
    ｝
  ｝
=end
      proc{Weapon.move(@account1, @account2 , 'トモエリバー',10)}.should raise_error(RuntimeError)
    end

    it '兵器の所属を移動した場合' do
=begin
  ｆ：兵器の所属を移動した場合＝｛
    引数←｛兵器の移動元の対象OwnerAccount.id 、兵器の移動先の対象OwnerAccount.id 、兵器の名称、移動数｝
　　事前条件←｛
      移動元のOwnerAccountが存在する。
      移動先のOwnerAccountが存在する。｝
      type == :weapon ,   name == 「兵器の名称」、owner_account_id == 「対象のOwnerAccountのID」であるレコードの数が、「除籍数」の数以上存在する。
    ｝

    事後条件←｛
      移動元のOwnerAccountに関連付けられたIObject のうち、name==「兵器の名称」であるレコードの総数が、事前より「移動数」だけ減少する。
      移動先のOwnerAccountに関連付けられたIObject のうち、name==「兵器の名称」であるレコードの総数が、事前より「移動数」だけ追加される。
    ｝
  ｝
=end
      pre_count1 = @account1.weapons.find_all_by_name('トモエリバー').size
      pre_count2 = @account2.weapons.find_all_by_name('トモエリバー').size

      Weapon.move(@account1, @account2 , 'トモエリバー',2)

      post_count1 = @account1.weapons.find_all_by_name('トモエリバー').size
      post_count2 = @account2.weapons.find_all_by_name('トモエリバー').size
      post_count1.should == (pre_count1 - 2)
      post_count2.should == (pre_count2 + 2)
    end
  end

end
