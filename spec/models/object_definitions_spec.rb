# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe ObjectDefinition ,"ObjectDefinition " do
  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
  end

  it 'test_data' do
    def1 = ObjectDefinition.new(:definition => <<EOT)
古い聖銃に似せたブラスター（アイテム）：３０：＃近距離
・ＦＯ：２５：＃遠距離
・是空の真似：２５：＃隠蔽
・サトルの真似：２５：＃遠距離
・増幅カートリッジ：２５：＃中距離
EOT
    def1.is_idress3?.should == true

    def1.parsed_command_resources.size.should == 5
    def1.parsed_command_resources[0][:name].should == '古い聖銃に似せたブラスター'
    def1.parsed_command_resources[0][:power].should == 30
    def1.parsed_command_resources[0][:tag].should == '近距離'

    def1.parsed_command_resources[1][:name].should == 'ＦＯ'
    def1.parsed_command_resources[1][:power].should == 25

  end

  it 'nekorisu' do
    nekorisu = CommonDefinition.new(:definition => <<EOT)
おともネコリス（ＮＰＣ）：２０：＃基本評価
要点
・げっ歯類？：２０：＃外見
・猫？：３０：＃感覚
・零体：８０：＃耐久
・樹を渡る：３０：＃敏捷

エクストラ
・コパイ：３０：＃コパイ
・世界移動：３０：＃世界移動
・空間計算：４０：＃知識
・器用な前足：３０：＃器用
・力を貸す：５０：＃装甲

次のアイドレス
・お話を食べる：３０：＃知識
・背中をおしてくれる：５０：＃勇気


EOT
    nekorisu.is_idress3?.should == true

    nekorisu.parsed_command_resources.size.should == 12
    nekorisu.parsed_command_resources[1][:name].should == 'げっ歯類？'
    nekorisu.save

    expect(nekorisu.is_idress3_format?).to eq true
    expect(nekorisu.command_resource_definitions.size).to eq 12
    #def1.command_resources[0][:power].should == 30
    #def1.command_resources[0][:tag].should == '近距離'
    #
    #def1.command_resources[1][:name].should == 'ＦＯ'
    #def1.command_resources[1][:power].should == 25

  end

  it 'enfeeblement' do
    enfeeblement = CommonDefinition.new(:definition =>  <<EOT     )
弱体化！（技術）：５０：＃弱体化！
・S＊15
名前：弱体化！
要求タグ：＃弱体化！
E＊戦力：不定（緊急度に応じて変化する）
必要勝利数：１０
フレーバー：力を制御しなければ。
特別ルール：
・＃弱体化！を持つものは任意のブランチとして起こせる。ただし、ブランチが許可されていなければならない。
・他のキャラクターと戦力を合算することは出来ない。
・ダイス出目は固定で、４、３、２、１、１、１、１、１、１、１である。
・このブランチに成功すると裏返ったりオーマにならない程度に力を抑制できる。シーンが終わるまで全戦力は半分になる。
大勝利時の効果：３勝利を得る。
勝利時の効果：１勝利を得る。
引き分け時の効果：なし
敗北時の効果：なし
惨敗時の効果：なし
EOT

    enfeeblement.is_idress3?.should == false
    enfeeblement.is_command_resource?.should == true

    enfeeblement.parsed_command_resources.size.should == 1
    enfeeblement.parsed_command_resources[0][:name].should == '弱体化！'

    enfeeblement.save
    expect(enfeeblement.command_resource_definitions.size).to eq 1

  end



  it 'not idress3' do
    odef = ObjectDefinition.new(:definition => <<EOT)
Ｌ：高機能ハンドヘルド　＝　｛
　ｔ：名称　＝　高機能ハンドヘルド（マジックアイテム）
　ｔ：要点　＝　腕につける，ＰＣ，格好いい
　ｔ：周辺環境　＝　手
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊高機能ハンドヘルドのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊高機能ハンドヘルドの位置づけ　＝　，，｛マジックアイテム，武器，兵器｝。
　　＊高機能ハンドヘルドの着用箇所　＝　，，片手持ち。
　　＊高機能ハンドヘルドの形状　＝　，，コンピュータ。
　　＊高機能ハンドヘルドの情報戦闘行為　＝　歩兵武装，，情報戦闘行為が可能。
　　＊高機能ハンドヘルドの情報戦闘補正　＝　歩兵武装，条件発動，情報戦闘、評価＋３。
　　＊高機能ハンドヘルドの情報系職業補正　＝　歩兵武装，条件発動，（位置づけ（情報系）の職業による）情報戦闘、評価＋１。
　｝
　ｔ：→次のアイドレス　＝　大型ＰＣ（アイテム），ハッキング（イベント），小さい電子妖精（アイテム），車載化ＡＩ（イベント）
｝
EOT
    odef.is_idress3?.should == false
    odef.is_command_resource?.should == false

    odef.parsed_command_resources.should == []
    odef.save

    expect(odef.command_resource_definitions.size).to eq 0
  end

  it 'アイテム・兵器等であれば、countable? == true' do
    odef = ObjectDefinition.new(:definition => <<EOT)
Ｌ：高機能ハンドヘルド　＝　｛
　ｔ：名称　＝　高機能ハンドヘルド（マジックアイテム）
　ｔ：要点　＝　腕につける，ＰＣ，格好いい
　ｔ：周辺環境　＝　手
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊高機能ハンドヘルドのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊高機能ハンドヘルドの位置づけ　＝　，，｛マジックアイテム，武器，兵器｝。
　　＊高機能ハンドヘルドの着用箇所　＝　，，片手持ち。
　　＊高機能ハンドヘルドの形状　＝　，，コンピュータ。
　　＊高機能ハンドヘルドの情報戦闘行為　＝　歩兵武装，，情報戦闘行為が可能。
　　＊高機能ハンドヘルドの情報戦闘補正　＝　歩兵武装，条件発動，情報戦闘、評価＋３。
　　＊高機能ハンドヘルドの情報系職業補正　＝　歩兵武装，条件発動，（位置づけ（情報系）の職業による）情報戦闘、評価＋１。
　｝
　ｔ：→次のアイドレス　＝　大型ＰＣ（アイテム），ハッキング（イベント），小さい電子妖精（アイテム），車載化ＡＩ（イベント）
｝
EOT
    odef.object_type = 'マジックアイテム'
    expect(odef.countable?).to eq true
  end

  it 'アイテム・兵器以外であれば、is_countable? == false' do
    odef = ObjectDefinition.new(:definition => '小さい畑：１０：＃食料')
    expect(odef.countable?).to eq false
  end


  it 'insert 時より先に登録されていた同名のYggdrasill があった場合、コマンドリソースを追加する。' do
    @character = create(:character)

    expect(@character.owner_account.command_resources.size).to eq 0

    create :dragon_tooth_warrior

    @character.owner_account.command_resources.reload
    expect(@character.owner_account.command_resources.size).to eq 8

    end
end
