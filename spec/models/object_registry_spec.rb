# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe ObjectRegistry ,"ObjectRegistry " do
#  fixtures :object_definitions

  before(:each) do
  end
  it 'create' do
    pending
    objr = ObjectRegistry.new
    objr.name='レトロライフ'
    objr.save
    objr.object_definition.should_not be_nil
    objr.object_definition.name.should == 'レトロライフ'
  end


  describe 'アイテムである単体コマンドリソースの個数が変動した場合' do
    before :each do
      ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
      create(:object_definition)
      cr_def = create(:statue_of_sione_def)
      @character = create(:character)
      create(:command_resource ,
             owner_account: @character.owner_account ,
             command_resource_definition: cr_def.command_resource_definitions[0]
      )
      create(:object_registry , number: 1 , owner_account: @character.owner_account , name: 'シオネの小像')
      @character.owner_account
    end
    it '1 > 0個' do
      cr = @character.owner_account.command_resources.where('command_resource_definitions.name' => 'シオネの小像').first
      expect(cr.available?).to eq true
      create(:object_registry , number: -1 , owner_account: @character.owner_account , name: 'シオネの小像')
      puts @character.owner_account.num_of_property('シオネの小像')

      cr = @character.owner_account.command_resources.where('command_resource_definitions.name' => 'シオネの小像').first
      expect(cr.available?).to eq false
    end
    it '0 > 1個' do
      create(:object_registry , number: -1 , owner_account: @character.owner_account , name: 'シオネの小像')
      create(:object_registry , number: 1 , owner_account: @character.owner_account , name: 'シオネの小像')
      cr = @character.owner_account.command_resources.where('command_resource_definitions.name' => 'シオネの小像').first
      expect(cr.available?).to eq true
    end
  end
end
