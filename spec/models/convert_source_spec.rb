# -*- encoding: utf-8 -*-

require 'spec_helper'

DatabaseCconfig = YAML.load_file(File.join(Rails.root.join('config','database.yml') ) )
class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end

describe ConvertSource do
    before(:each) do
      stub_session
      @character = create(:character)

      @params = {
          unit_registry: {
              name: '迷宮FIX',
              note: '備考テスト',
              authority_url: 'http://cwtg.jp/bbs3/nisetre.cgi?no=5397'
          },
          source: <<-EOT ,
＃！部分有効条件：位置づけ（情報系）の職業
＃越前藩の電子妖精軍ＨＱを反映
＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
＃本来は部隊単位で与える大型PC・超大型PCの補正を、黒埼一名に与える。
＃＃！個別付与：大型ＰＣの計算資源＋超大型ＰＣの計算資源，全ユニット
32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ネットワークエンジニア+法の司：敏捷+1*知識+4*幸運+1;
－大型ＰＣ：黒埼紘所有：情報戦＋５。（元から情報戦ができる職業の場合）さらに＋３。部隊内の情報戦行為可能要員に効果あり
－超大型ＰＣ：黒埼紘所有：。（元から情報戦ができる職業の場合）情報戦＋８。部隊内の情報戦行為可能要員に効果あり
－水流銃：個人所有：歩兵武装，，近距離戦闘行為が可能。 , 歩兵武装，条件発動，（射撃（銃）、近距離での）攻撃、評価＋２。属性（水）。片手持ち。
－猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。
＃－電子妖精軍：藩国装備
－受付：リザルトで取得
－まあまて落ち着け：リザルトで取得
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9"
          EOT
          owner_account_id: @character.owner_account.id
      }
    end

    context 'create' do

      before :each do
        @cs = ConvertSource.new(@params[:unit_registry])
        @cs.owner_account = @character.owner_account
      end
      it 'create' do
        @cs.source = @params[:source]
        @cs.save

        expect(@cs = @character.owner_account.unit_registries.first).to be_truthy
        expect(@cs.available).to be false
        expect(@cs.convert_definition).to be_truthy
        expect(@cs.convert_definition.name).to eq  '迷宮FIX'
        expect(@cs.convert_definition.object_type).to eq  'コンバート定義'
        expect(@cs.convert_definition.addition['divisions'][0]['evaluations'].size).to eq 28
      end

      it '文法エラーな太元書式は受け付けない' do

        @cs.source = <<EOT
  Ｌ：＝
EOT
        expect(@cs.save).to be_falsy
      end

      it 'register' do
        @cs.source =  @params[:source]
        @cs.save

        @cs.rebuild_command_resources

        expect(@character.owner_account.command_resources.size).to eq 25
        expect(@cs.available).to be_truthy

        expect(@character.owner_account.command_resources.first.name).to eq '黒埼紘の攻撃機会'
      end
    end

    describe 'ConvertSource の切り替え' do
      before :each do
        @former_cs = ConvertSource.new(@params[:unit_registry])
        @former_cs.owner_account = @character.owner_account
        @former_cs.source =  @params[:source]
        @former_cs.save

        @params[:unit_registry][:name] = 'コンバート'
        @former_cs.rebuild_command_resources

        @new_cs = ConvertSource.new(@params[:unit_registry])
        @new_cs.owner_account = @character.owner_account
        @new_cs.source = <<EOT
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋大剣士＋剣：敏捷+1*知識+1*幸運+1;
EOT
        @new_cs.save
        @new_cs.rebuild_command_resources
        @former_cs.reload
      end
      # it '元のConvertSourceは無効化される' do
      #   expect(@former_cs.available).to be_falsy
      #   expect(@former_cs.convert_definition).to be_truthy
      #   expect(@former_cs.convert_definition.command_resource_definitions.size).to eq 0
      #
      # end
      it '新しいConvertSourceが有効化される。' do
        expect(@new_cs.available).to be_truthy
        expect(@new_cs.convert_definition).to be_truthy
        expect(@new_cs.convert_definition.command_resource_definitions.size).to eq 23
        expect(@character.owner_account.command_resources.size).to eq 48


      end
    end

  describe 'PLACE のコンバート' do
    before :each do
      @params[:source] = <<EOT
＃＃！有効条件：法執行時での
00-00652-02_猫野和錆(ＰＬＡＣＥ)：和錆、国境なき医者＋HQ＋SHQ
－法の執行者の紋章：個人所有：（着用型／手先）法執行時、全判定＋２
－カトラス：個人所有：（着用型／片手持ち武器）白兵戦行為可能・，（白兵（剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋２
＃－恩寵の短剣：個人所有：（着用型／片手持ち武器）白兵戦行為可能・，（白兵（短剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋１
－綺麗な結婚指輪：個人所有：（着用型／手先）結婚していることを思い出す
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00652-02%A1%A7%C7%AD%CC%EE%CF%C2%BB%AC
EOT

      cs = ConvertSource.new(@params[:unit_registry])
      cs.owner_account =  @character.owner_account
      cs.source =  @params[:source]
      cs.save
      cs.rebuild_command_resources

    end

    # it 'コマンドリソース名称は”【ＡＣＥ定義名】（ＡＣＥ）”となる' do
    #   expect(@character.owner_account.unit_registries.first.name).to eq '和錆、国境なき医者（ＡＣＥ）の攻撃機会'
    # end

  end

  describe 'with resourcify syntax' do
    before :each do
      @source = <<EOT
32-00635-01：hacker:ハッカー；
－大型ＰＣの計算資源

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：６：＃情報戦
　ｆ：＊大型ＰＣの計算資源の情報戦闘補正　＝　大型ＰＣの計算資源の情報戦闘補正：１２：＃情報戦
｝
EOT
      cs = ConvertSource.new(@params[:unit_registry])
      cs.owner_account = @character.owner_account
      cs.source =  @source
      cs.save
      cs.rebuild_command_resources
    end

    it '指定されたコマンドリソースが生成される。' do
      cr =   @character.owner_account.command_resources.last
      expect(cr.name).to eq '大型ＰＣの計算資源の情報戦闘補正'
      expect(cr.power).to eq 12
      expect(cr.tag).to eq '情報戦'
    end

  end

end