# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

require 'rubygems'
require 'treetop'

#require File.join(File.dirname(__FILE__),'../calculator/evcc_defs.rb')
#
#require File.join(File.dirname(__FILE__),'../calculator/unit.rb')
#require File.join(File.dirname(__FILE__),'../calculator/evaluator.rb')
require 'i_language/calculator' # ILanguage::Calculator
require 'jiken/database/i_definition'
require 'jcode'
$KCODE='u'
include ILanguage::Calculator
include TaigenHelper

class IDefinition
  establish_connection :development
end

Definitions.set_hq

describe Unit do
  before(:each) do
    @parser = EVCCParser.new
    @unit = Unit.new
  end
  Abilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']


  def inspect_evals(troop)
    troop_evals_str(troop) + "\n"+defense_evals_str(troop) + action_evals_str(troop)
  end
  
  it "judge basic_bonus? tenyo" do
#    tenyo = Definitions['天陽']
    soldier = Soldier.new
    soldier.items = ['天陽']
    bonus_sp = soldier.bonuses.find{|b|b['定義'] =~ /＊天陽の全能力補正/}

    soldier.basic_bonus?(bonus_sp,'耐久力').should == true
    evals = soldier.evals
    for col in Abilities
      evals[col].should == 2
      soldier.evaluate(col).should == 2
    end

    armor = Evaluator['防御']
    armor.evaluate(soldier)
    result = soldier.accept_evaluator(armor)
    result.should == 2
  end

  it "judge basic_bonus? cyborg" do
#    tenyo = Definitions['天陽']
    soldier = Soldier.new
    soldier.items = ['サイボーグ']
    bonus_sp = soldier.all_bonuses.find{|b|b['定義'] =~ /＊サイボーグの筋力・耐久力補正/}
    bonus_sp.should_not be_nil
    soldier.basic_bonus?(bonus_sp,'耐久力').should == true
    evals = soldier.evals
    evals['筋力'].should == 1
    evals['耐久力'].should == 1
    soldier.evaluate('筋力').should == 3
    soldier.evaluate('耐久力').should == 3
    armor = Evaluator['防御']
    soldier.accept_evaluator(armor).should == 2
  end

  it "02：ａｋｉｈａｒｕ藩国(T14)編成検証 " do
    source = <<EOT
//有効条件：変身することで
//有効条件：パイロットとして搭乗している場合での
02-00027-01_涼原秋春：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*幸運+1；
02-00031-01_鴨瀬高次：南国人＋医師＋風紀委員会＋生徒会役員＋法官：敏捷+1*知識+1*幸運+2；
02-00032-01_忌闇装介：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+2；
02-00034-01_阪明日見：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+6；
02-00036-01_鈴木：南国人＋医師＋風紀委員会＋生徒会役員：幸運+1；
02-00037-01_田中申：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+1；
02-00038-01_和志：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：筋力+6*敏捷+1*知識+1*幸運+1；
02-00039-01_リバーウィンド：南国人＋医師＋風紀委員会＋生徒会役員＋護民官：敏捷+1*器用+1*知識+1*幸運+1；
02-00841-01_ゆり花：南国人＋医師＋風紀委員会＋生徒会役員；
02-00842-01_勇作：南国人＋医師＋風紀委員会＋生徒会役員；

_define_I=D: {
デカショー１_国有：デカショー,
（Ｐ）02-00028-01_４４４：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*感覚+5*知識+1*外見+2*幸運+2
};

_define_I=D: {
デカショー２_国有：デカショー,
（Ｐ）02-00030-01_橘：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット+ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+2
};
EOT
#    root = @parser.parse(source)
#    troop = create_troop(root)
#    result = inspect_evals(troop)

#    result.should == <<EOT
#体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運
#25：28：28：24：25：29：40：31：34
#防御：防御（白兵距離）：防御（近距離）：防御（中距離）：防御（遠距離）
#26：26：26：26：26
#白兵距離戦闘行為：29
#偵察：40
#EOT
  end

end

