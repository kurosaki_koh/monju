# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'


DatabaseCconfig = YAML.load_file(File.join(Rails.root.join('config','database.yml') ) )
class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end

describe IncludeRegistry do
  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    @owner_account = create(:character).owner_account
    @include_test = create :included_def
    @owner_account.add_object_definition(@include_test)
  end



  def register_include(data)
    @include_registry = IncludeRegistry.new
    @include_registry.name = 'テスト'
    @include_registry.source = data #.to_yaml
    @include_registry.owner_account = @owner_account
    @include_registry.save
    @include_registry.rebuild_command_resources
  end

  shared_context 'インクルードの登録' do

    let(:include_data) do
      {
          main_member: {
              character_no: '32-00635-01' ,
              name: '黒埼紘'
          },
          members: [{
                        character_no: '32-00622-01' ,
                        name: 'セントラル越前'
                    }],
          includes: [
              {
                  command_resource: {
                      name: 'test' ,
                      power: @owner_account.command_resources.sum(&:power) ,
                      tag: '装甲'
                  } ,
                  sources: @owner_account.command_resources.pluck(:id) #map{|cr|cr.id}
              }
          ]
      }
    end

    before :each do
      register_include(include_data)
    end



  end

  describe 'インクルードのみの登録' do
    include_context 'インクルードの登録'

    it 'ハッシュからインクルードを登録できる。' do
      expect(@include_registry.object_definition.definition).to eq include_data.to_yaml
      expect(@include_registry.name).to eq "テスト"
      expect(@include_registry.object_definition.name).to eq "テスト"
      expect(@include_registry.object_definition.type).to eq 'IncludeDefinition'
      expect(@include_registry.owner_account).to eq @owner_account

    end
  end

  context 'priority' do
    before :each do
      cs = ConvertSource.new(
        name: '黒埼紘',
        note: 'test note' ,
        authority_url: 'test url'
      )
      cs.source = <<EOT
＃！部分有効条件：位置づけ（情報系）の職業
＃越前藩の電子妖精軍ＨＱを反映
＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
＃本来は部隊単位で与える大型PC・超大型PCの補正を、黒埼一名に与える。
＃＃！個別付与：大型ＰＣの計算資源＋超大型ＰＣの計算資源，全ユニット
32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ネットワークエンジニア+法の司：敏捷+1*知識+4*幸運+1;
－大型ＰＣ：黒埼紘所有：情報戦＋５。（元から情報戦ができる職業の場合）さらに＋３。部隊内の情報戦行為可能要員に効果あり
－超大型ＰＣ：黒埼紘所有：。（元から情報戦ができる職業の場合）情報戦＋８。部隊内の情報戦行為可能要員に効果あり
－水流銃：個人所有：歩兵武装，，近距離戦闘行為が可能。 , 歩兵武装，条件発動，（射撃（銃）、近距離での）攻撃、評価＋２。属性（水）。片手持ち。
－猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。
＃－電子妖精軍：藩国装備
－受付：リザルトで取得
－まあまて落ち着け：リザルトで取得
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9
EOT
      cs.owner_account = @owner_account
      cs.save
      cs.rebuild_command_resources
    end

    it 'ConvertSource のみ登録されているなら、これを返す' do
      result = @owner_account.unit_registries.by_name('黒埼紘').first
      puts result.to_yaml
      expect(result.type).to eq 'ConvertSource'
      expect(result.command_resources.size).to eq 25

    end

    context 'IncludeRegistryも登録されている' do
      include_context 'インクルードの登録'

      it 'IncludeRegistryを優先する。' do
        result = @owner_account.unit_registries.by_name('黒埼紘').first
        expect(result.type).to eq 'IncludeRegistry'
        expect(result.command_resources.size).to eq 1
      end
    end


  end



end