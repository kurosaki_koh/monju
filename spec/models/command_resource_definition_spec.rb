# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe ObjectDefinition ,"ObjectDefinition " do
  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
  end

  describe '自動成功' do
    before :each  do
      @command_resource_definition = CommandResourceDefinition.new(name:'test' , power:'自動成功' , tag:'test')
      @command_resource_definition.save
      @command_resource_definition.reload
      @command_resource_definition
    end

    it '読み込んだレコードは”自動成功"を返す"' do
      expect(@command_resource_definition.power).to eq '自動成功'
    end
  end
end
