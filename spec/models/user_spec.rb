require File.dirname(__FILE__) + '/../spec_helper'

describe User do
  before :each do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    @owner_account = create(:character).owner_account
    @user = create(:user)
  end

  it 'has many characters' do
    ch = @user.characters
    puts ch.inspect
    expect(ch.first).to eq @owner_account.owner
  end

  it 'has many owner_account' do
    ch = @user.owner_accounts
    puts ch.inspect
    expect(ch.first).to eq @owner_account
  end
end