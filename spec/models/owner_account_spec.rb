# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'
include IdressSearcher

describe OwnerAccount do
#  fixtures :owner_accounts , :i_objects , :specifications

  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    @owner_account = create(:character).owner_account
    # @params = {}
    # @params[:type_no]='1'
    # @params[:number]='2'
    # @spec = create :specification
  end

  shared_examples '非可算アイドレスのリザルト' do
    it 'イグドラシルのみ追加。' do
      expect(@owner_account.yggdrasills.size).to eq 2
    end
  end

  shared_examples 'コマンドリソースが増えない。' do
    it 'コマンドリソースは変化なし' do
      expect(@owner_account.command_resources.size).to eq 0
    end
  end

  shared_examples '取得済みイグドラシルを得た場合' do |idress_name|
    it '同名登録はイグドラシル追加を行わない。' do
      idress = search_idress(idress_name)
      expect { @owner_account.add_object_definition(idress)}.to change(Yggdrasill, :count).by(0)
    end
  end

  shared_examples '可算アイドレスのリザルト' do |idress_name|
    it 'イグドラシルとObjectRegistryに追加' do
      expect(@owner_account.yggdrasills.size).to eq 2
      expect(@owner_account.num_of_property(idress_name)).to eq 1
    end
  end

  shared_examples 'url と note の反映' do |proc , params|
    it 'url と note　が正しく反映されている。' do
      object = proc.call(@owner_account)
      puts object.inspect
      expect(object.url).to eq params[:url]
      expect(object.note).to eq params[:note]
    end
  end

  context 'アイドレス２定義のリザルト' do
    before(:each) do
    end

    describe '技術アイドレスなら' do
      before(:each) do
        @ochitsuke = create :ochitsuke_def
        @owner_account.add_object_definition(@ochitsuke)
      end

      it_behaves_like '非可算アイドレスのリザルト'
      it_behaves_like 'コマンドリソースが増えない。'
      it_behaves_like '取得済みイグドラシルを得た場合', 'まあまて落ち着け'

    end
    describe 'アイテムアイドレスなら' do
      before(:each) do
        @chance_ball = create :chance_ball_def
        @owner_account.add_object_definition(@chance_ball)
      end

      it_behaves_like '可算アイドレスのリザルト', 'チャンスボール'
      it_behaves_like 'コマンドリソースが増えない。'
      it_behaves_like '取得済みイグドラシルを得た場合', 'チャンスボール'

    end
  end

  context 'アイドレス３定義のリザルト' do
    describe '職業アイドレスなら' do

      before(:each) do
        @odef = create :evasion_master_def
        @owner_account.add_object_definition(@odef , url: 'test url' , note: 'idress3 note')
      end

      it_behaves_like '非可算アイドレスのリザルト'

      it 'コマンドリソース＋８' do
        @owner_account.reload
        expect(@owner_account.command_resources.count).to eq 8
      end

      it_behaves_like 'url と note の反映' , ->(o){
        result = o.yggdrasills.last
        def result.url
          self.basis_url
        end
        result
      },   {url: 'test url' , note: 'idress3 note'}
    end
    describe 'アイテムアイドレスなら' do
      before(:each) do
        @odef = create :pocket_pique_def
        @owner_account.add_object_definition(@odef)
      end

      it_behaves_like '可算アイドレスのリザルト', 'ポケットピケ'
      it 'コマンドリソース＋５' do
        @owner_account.reload
        expect(@owner_account.command_resources.count).to eq 5
      end
    end
  end

  context '非アイドレスアイテムのリザルト' do
    describe 'アイテムアイドレスなら' do
      before(:each) do
        @odef = create :cat
        @owner_account.add_object_definition(@odef)
      end

      it 'コマンドリソース＋0、イグドラシル＋0、ObectRegistry+1' do
        @owner_account.reload
        expect(@owner_account.command_resources.count).to eq 0
        expect(@owner_account.yggdrasills.count).to eq 1
        expect(@owner_account.num_of_property('個人用猫士')).to eq 1
      end
    end

  end

  describe 'OwnerAccount#add_command_resource' do
    before(:each) do
      @small_field = create :small_field_def
      @owner_account.add_object_definition(@small_field , url: 'test url' , note: 'idress3 note')
    end

    it 'コマンドリソースの追加ができる' do
      expect(@owner_account.command_resources.count ).to eq 1

    end

    it '同名のコマンドリソース追加はできない' do
      expect {
        @owner_account.add_object_definition(@small_field)
        @owner_account.reload
      }.to change { @owner_account.command_resources.count }.by(0)
    end

    it 'イグドラシルには変化がない。' do
      expect(@owner_account.yggdrasills.count ).to eq 1
    end

    it_behaves_like 'url と note の反映' , ->(o){
       result = o.command_resources.last
      def result.url
        self.authority_url
      end
      result
    },   {url: 'test url' , note: 'idress3 note'}
  end

  describe 'OwnerAccount#inherited_command_resources' do


  end
end
