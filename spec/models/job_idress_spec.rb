# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe JobIdress do
  fixtures :races , :yggdrasills
  before(:each) do
    @job_idress = JobIdress.new
  end

  it "生成時に能力値を自動計算する" do
    @job_idress.race = races(:high_wooden_folk)
    @job_idress.job1 = yggdrasills(:mad_scientist)
    @job_idress.job2 = yggdrasills(:doctor)
    @job_idress.job3 = yggdrasills(:veteran_doctor)
    @job_idress.save
    p @job_idress
    Job::Abilities.find{|c| @job_idress[c] != 0 }.should_not == nil
  end
  it "構成を変えても能力値を自動計算する" do
    @job_idress.race = races(:high_wooden_folk)
    @job_idress.job1 = yggdrasills(:mad_scientist)
    @job_idress.job2 = yggdrasills(:doctor)
    @job_idress.job3 = yggdrasills(:veteran_doctor)
    @job_idress.save
    @job_idress.race = races(:wooden_folk)
    @job_idress.save
    p @job_idress
    Job::Abilities.find{|c| @job_idress[c] != 0 }.should_not == nil
  end

  it 'アイドレス構成の一致を正確に判定できる' do
    @job_idress.race = races(:wooden_folk)
    @job_idress.job1 = yggdrasills(:doctor)
    @job_idress.job2 = yggdrasills(:doctor)
    @job_idress.job3 = yggdrasills(:veteran_doctor)
    @job_idress.save

    @job_idress.is_this?(['森国人','医師','医師','名医']).should == true
    @job_idress.is_this?(['森国人','医師','医師']).should == false
  end
end
