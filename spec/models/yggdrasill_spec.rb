# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe Yggdrasill do
#  fixtures :yggdrasills , :job_idresses , :races

  before(:each) do
    ModifySweeper.any_instance.stub(:controller).and_return(double('session', :session => {}))
    @yggdrasill = Yggdrasill.new
    @definitions = {}
    [:small_field_def , :dragon_tooth_warrior , :evasion_master_def , :pocket_pique_def , :mantle_def].each do |odef|
      @definitions[odef] = create odef
    end

    create :nation
  end

  xit 'can parse command resource' do
    pending
    @yggdrasill.i_definition = <<EOT
古い聖銃に似せたブラスター（アイテム）：３０：＃近距離
・ＦＯ：２５：＃遠距離
・是空の真似：２５：＃隠蔽
・サトルの真似：２５：＃遠距離
・増幅カートリッジ：２５：＃中距離
EOT

    @yggdrasill.should respond_to :command_resources
    @yggdrasill.parsed_command_resources.size.should == 5

  end


  it 'イグドラシルを追加すると、コマンドリソースが増える' do

    @character = create(:character)
    puts @character.owner_account.command_resources.inspect
    @character.owner_account.command_resources.reload
    @character.owner_account.command_resources.size.should == 8
    @character.owner_account.command_resources[0].name.should =='竜牙の使い手'
    @character.owner_account.command_resources[0].authority_url.should =='http://dummy.url/for/test'
    @character.owner_account.command_resources[1].authority_url.should =='http://dummy.url/for/test'

  end

  it 'イグドラシルを削除すると、関連していたコマンドリソースも削除される。' do
#    ModifySweeper.any_instance.stub(:controller).and_return(double('session',:session => {}) )

    @character = create(:character)

    @character.owner_account.yggdrasills[0].destroy
    @character.owner_account.command_resources.size.should == 0
  end

  it 'ユニーク定義でないイグドラシルを追加すると、コマンドリソースが追加される。' do
#    ModifySweeper.any_instance.stub(:controller).and_return(double('session',:session => {}) )

    @character = create(:character)
    puts "#owner_account:" +@character.owner_account.inspect

    create(:object_registry , owner_account: @character.owner_account)
    @character.owner_account.reload
    puts ObjectRegistry.all.inspect
    puts "#object registry:" +@character.owner_account.object_registries.size.to_s
    puts "#object registry:" +@character.owner_account.object_registries.owned_objects.inspect
    ygg = Yggdrasill.new
    ygg.name = 'ポケットピケ'
    ygg.idress_name = 'ポケットピケ'
    ygg.owner_account = @character.owner_account
    ygg.save



    @character.owner_account.command_resources.size.should == 13
    @character.owner_account.command_resources[8].name.should == 'ポケットピケ'
    @character.owner_account.command_resources[8].tag.should == '敏捷'
  end

  it 'idress_name を変更した場合、コマンドリソースがすげ替えられる' do
    @character = create(:character)
    create(:object_registry , owner_account: @character.owner_account)
    @character.owner_account.reload

    ygg = @character.owner_account.yggdrasills[0]
    puts ygg.definition
    ygg.name = 'ポケットピケ'
    ygg.idress_name = 'ポケットピケ'
    ygg.i_definition = ''
    ygg.save
    puts ygg.definition

    puts ygg.parsed_command_resources.map{|c| c[:name]}
    puts
    ygg.parsed_command_resources.size.should == 5
    puts @character.owner_account.command_resources.map{|c| c.name}
    @character.owner_account.command_resources.size.should == 5
    @character.owner_account.command_resources[0].name.should == "ポケットピケ"


  end

  # it 'Yggdrasill#is_available' do
  #   @character = create(:character)
  #   create(:object_definition)
  #
  # end

  describe 'command resource extending' do

    before :each do
      @character = create(:character)
      @character.nation.owner_account.add_object_definition(@definitions[:pocket_pique_def])
    end

    # it 'Character は藩国のイグドラシルが持つコマンドリソースも利用できる' do
    #   pending '可算アイドレスと非可算アイドレスでの波及に有無があるので没？'
    #   expect(@character.inherited_command_resources.size).to eq @character.nation.owner_account.command_resources.size
    #   expect(@character.inherited_command_resources.all?{|cr|cr[:from] == 'テスト藩国'}).to be_truthy
    #
    #   puts @character.inherited_command_resources.inspect
    # end

    it '藩国のアイドレスで非可算アイドレス由来のコマンドリソースは波及する' do
      expect(@character.extended_command_resources.map{|r|r.name}).to include('回避の達人')

    end
    it '藩国のアイドレスで可算アイドレス由来のコマンドリソースは波及しない' do
      puts @character.extended_command_resources.map{|r|r.to_form}

      expect(@character.extended_command_resources.map{|r|r.name}).not_to include('風切のマント')
    end

    it '藩国所有の単体コマンドリソースは波及する。' do

    end
  end

  it 'アイドレス名が定義と一致しない場合、invalid扱い' do
    pending #Yggdrasill では定義を持たなくしたので
    @character = create(:character)
    ygg = @character.owner_account.yggdrasills[0]
    ygg.idress_name =  'ポケットピケ'
    ygg.has_definition?
    ygg.errors.size.should == 1
    puts ygg.errors.map{|e|e.inspect}
  end


  describe 'アイテムアイドレスが持つコマンドリソース' do
    before(:each) do
      @character = create(:character)
      @owner_account = @character.owner_account
    end


    it 'アイテムが０個＞１個になり、かつイグドラシル登録があれば、コマンドリソースを追加する。' do
      ygg = create(:pocket_pique , owner_account: @character.owner_account)

      ygg.is_available?.should eq false
      @character.owner_account.command_resources.size.should == 8

      create(:object_registry , owner_account: @character.owner_account)
      @character.owner_account.reload

      ygg.definition_type.should == :i3
      @character.owner_account.num_of_property('ポケットピケ').should == 1
      ygg.reload
      expect(ygg.is_available?).to eq true

      @character.owner_account.command_resources.size.should == 13
    end

    it 'アイテムが１個＞０個になり、かつイグドラシル登録があれば、コマンドリソースを論理削除する。' do
      create(:object_registry , owner_account: @character.owner_account)
      ygg = create(:pocket_pique , owner_account: @character.owner_account)

      expect(ygg.is_available?).to eq true
      @character.owner_account.command_resources.size.should == 13
      expect(ygg.command_resources.all?{|cr|cr.available? == true}).to eq true
      @owner_account.reload

      create(:object_registry , number: -1 , owner_account: @character.owner_account)

      @owner_account.reload
      ygg.reload
      expect(ygg.is_available?).to  eq false
      @owner_account.command_resources.size.should == 13
      expect(ygg.command_resources.all?{|cr|cr.available? == false}).to eq true
    end

    it 'アイテムが１個＞０個になったあと、また１個になれば、コマンドリソースを論理削除から復活する。' do
      create(:object_registry , owner_account: @character.owner_account)
      ygg = create(:pocket_pique , owner_account: @character.owner_account)

      expect(ygg.is_available?).to eq true
      @character.owner_account.command_resources.size.should == 13
      expect(ygg.command_resources.all?{|cr|cr.available? == true}).to eq true
      @character.owner_account.reload

      create(:object_registry , number: -1 , owner_account: @character.owner_account)

      @character.owner_account.reload
      ygg.reload
      expect(ygg.is_available?).to  eq false
      @character.owner_account.command_resources.size.should == 13
      expect(ygg.command_resources.all?{|cr|cr.available? == false}).to eq true

      create(:object_registry , number: 1 , owner_account: @character.owner_account)

      ygg.reload
      expect(ygg.is_available?).to  eq true
      @character.owner_account.command_resources.size.should == 13
      expect(ygg.command_resources.all?{|cr|cr.available? == true}).to eq true
    end
  end

end
