# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe Nation ,'新たに藩国を登録した場合' do
  before(:each) do
    @nation = Nation.new
  end

  it "対応するOwnerAccount も自動で生成される。" do
    @nation.save
    @nation.owner_account.should_not == nil
  end
  
  it "属性にシンボル':test' を代入して、後でクエリで参照できる。" do
    @nation.belong = :test
    @nation.save
    nation = Nation.find_by_belong(:test)
    nation.belong.should == :test
  end
  
end
