# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'


describe ModifyCommand,"MileChange を監視した場合"  do
  fixtures :mile_changes
  before(:each) do
#    @mile_change
#    @modify_command_filter = ModifyCommandFilter.new(MileChange)
#    @controller = ControllerStub.new
#    @controller.params[:action]=:create
  end
  
  it 'create したら、:insert なModifyCommand が登録されること。' do
    count = ModifyCommand.count
    count.should == 0
    new_mile_change = MileChange.new()
    Mile = 5
    Reason = 'create_test'
    ApplicationUrl = 'n/a'
    new_mile_change[:mile]=Mile
    new_mile_change[:reason]=Reason
    new_mile_change[:application_url]=ApplicationUrl
    new_mile_change.save
    
    (count+1).should == ModifyCommand.count 
    new_modify = ModifyCommand.find(:first , :order => 'id desc')

    new_modify[:original].should == nil
    new_modify[:change]["mile"].should == Mile
    new_modify[:change]["reason"].should == Reason
    new_modify[:change]["application_url"].should == ApplicationUrl
    new_modify[:modify_type].should == :insert
  end
  
  it 'updateしたら:updateな新規のModifyCommandを登録すること。' do
    count = ModifyCommand.count
    mile_change = MileChange.find(1)

    #    @controller.params[:id]=2
    new_record = {}
    mile_change[:mile]= Mile
    mile_change[:reason]=update_text ='update_text'
    mile_change[:application_url] = update_url = 'update application url'
    mile_change.save
    
    count2 = ModifyCommand.count
    (count+1).should == count2
    
    new_command = ModifyCommand.find(:first,:order =>'id desc')
    new_command[:original]["mile"].should == nil
    new_command[:original]["reason"].should == nil
    new_command[:original]["application_url"].should == nil
    new_command[:change]["mile"].should == Mile
    new_command[:change]["reason"].should == update_text
    new_command[:change]["application_url"].should == update_url
    new_command[:modify_type].should == :update
  end

  it 'deletes したら:delete なModifyCommandが登録されること' do
    count = ModifyCommand.count
    mile_change = MileChange.find(1)
    mile_change.destroy

    ModifyCommand.count.should == count + 1
    new_command = ModifyCommand.find(:first,:order =>'id desc')
    new_command[:modify_type].should == :delete
  end

end
