# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../spec_helper'

describe Ace do
  before :each do
    @ace = create :ace
  end

  it 'OnwerAccount#number とAce#character_noが同値である' do
    expect(@ace.character_no).to eq(@ace.owner_account.number)
  end

  it 'OnwerAccount#number とAce#character_noが同値である' do
    @ace.character_no = '44-00097-a1'
    @ace.save
    @ace.reload
    expect(@ace.character_no).to eq(@ace.owner_account.number)
  end
end
