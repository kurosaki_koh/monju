#encoding: utf-8

Monju::Application.configure do
# Settings specified here will take precedence over those in config/environment.rb


# The test environment is used exclusively to run your application's
# test suite.  You never need to work with it otherwise.  Remember that
# your test database is "scratch space" for the test suite and is wiped
# and recreated between test runs.  Don't rely on the data there!
  config.cache_classes = true

# Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

# Show full error reports and disable caching
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false

# Tell ActionMailer not to deliver emails to the real world.
# The :test delivery method accumulates sent emails in the
# ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

# Configure static asset server for tests with Cache-Control for performance
  config.serve_static_assets = true
  config.static_cache_control = "public, max-age=3600"

# Allow pass debug_assets=true as a query parameter to load pages with unpackaged assets
  config.assets.allow_debugging = true

#config.gem 'rspec',       :version => '>= 1.2.9', :lib => false unless File.directory?(File.join(Rails.root, 'vendor/plugins/rspec'))
#config.gem 'rspec-rails', :version => '>= 1.2.9', :lib => false unless File.directory?(File.join(Rails.root, 'vendor/plugins/rspec-rails'))
end