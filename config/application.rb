# Put this in config/application.rb
require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require *Rails.groups(:assets => %w(development test))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

FastGettext.add_text_domain 'monju', :path => 'locale'
FastGettext.default_available_locales = ['en','jp'] #all you want to allow
FastGettext.default_text_domain = 'monju'

module Monju
  class Application < Rails::Application
    config.autoload_paths += [config.root.join('lib'),config.root.join('app','models','concerns') ,
                              config.root.join('app','controllers','concerns')]
    config.encoding = 'utf-8'

# Enable the asset pipeline
    config.assets.enabled = true

# Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    # See Rails::Configuration for more options
    config.session_store :cookie_store , session_key: 'monju_session', secret:  'UgJ1ZyccaxPiP2IeA3FTdO5DwRZ227W7wIe1ZpzpWm5BP9iT'
    config.middleware.use Rack::Deflater

    config.action_view.javascript_expansions[:defaults] = %w(application )
  end
end
