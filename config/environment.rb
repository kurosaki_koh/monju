## Be sure to restart your web server when you modify this file.
#
#Encoding.default_external = 'UTF-8'
#
## Uncomment below to force Rails into production mode when
## you don't control web/app server and can't set it the proper way
#ENV['RAILS_ENV'] ||= 'development'
#
## Specifies gem version of Rails to use when vendor/rails is not present
##RAILS_GEM_VERSION = '2.3.14' unless defined? RAILS_GEM_VERSION
#RAILS_GEM_VERSION = '3.0.6' unless defined? RAILS_GEM_VERSION
#
## Bootstrap the Rails environment, frameworks, and default configuration
#require File.join(File.dirname(__FILE__), 'boot')
#
#Rails::Initializer.run do |config|
## Settings in config/environments/* take precedence over those specified here
#
#  # Skip frameworks you're not going to use (only works if using vendor/rails)
#  # config.frameworks -= [ :action_web_service, :action_mailer ]
#
#  # Only load the plugins named here, by default all plugins in vendor/plugins are loaded
#  # config.plugins = %W( exception_notification ssl_requirement )
#
#  # Add additional load paths for your own custom dirs
#  # config.load_paths += %W( #{RAILS_ROOT}/extras )
#
#  # Force all environments to use the same logger level
#  # (by default production uses :info, the others :debug)
#  # config.log_level = :debug
#
#  # Use the database for sessions instead of the file system
#  # (create the session table with 'rake db:sessions:create')
##   config.action_controller.session_store = {
##     :session_key => 'monju_sessyon',
##     :secret => 'UgJ1ZyccaxPiP2IeA3FTdO5DwRZ227W7wIe1ZpzpWm5BP9iT'
##   }
#
#
#  # Use SQL instead of Active Record's schema dumper when creating the test database.
#  # This is necessary if your schema can't be completely dumped by the schema dumper,
#  # like if you have constraints or database-specific column types
#  # config.active_record.schema_format = :sql
#
#  # Activate observers that should always be running
#  # config.active_record.observers = :monju_observer
#
#  # Make Active Record use UTC-base instead of local time
#  # config.active_record.default_timezone = :utc
#
#  # See Rails::Configuration for more options
#  config.action_controller.session ={
#     :session_key => 'monju_session',
#     :secret => 'UgJ1ZyccaxPiP2IeA3FTdO5DwRZ227W7wIe1ZpzpWm5BP9iT'
#   }
#  config.middleware.use Rack::Deflater
#end
#
## Add new inflection rules using the following format
## (all these examples are active by default):
## Inflector.inflections do |inflect|
##   inflect.plural /^(ox)$/i, '\1en'
##   inflect.singular /^(ox)en/i, '\1'
##   inflect.irregular 'person', 'people'
##   inflect.uncountable %w( fish sheep )
## end
#
## Add new mime types for use in respond_to blocks:
## Mime::Type.register "text/richtext", :rtf
## Mime::Type.register "application/x-mobile", :mobile
#
## Include your application configuration below
##require 'gettext/rails'
#require 'iconv'
#
#MONJU_VERSION = "1.3.0"
#
#ActionController::Base.perform_caching = true
#
ENV['TZ'] = 'Asia/Tokyo'
#ActiveSupport::CoreExtensions::Date::Conversions::DATE_FORMATS.update(:default => '%Y/%m/%d', :db => '%Y-%m-%d')


# Load the rails application
require File.expand_path('../application', __FILE__)

MONJU_VERSION = "1.6.1"
# Initialize the rails application

Monju::Application.initialize!
