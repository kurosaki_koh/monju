Monju::Application.routes.draw do
  resources :troop_definitions do

    member do
      get :validation
      get :parse
      get :value_check_list
      get :diff_form
    end

  end

  resources :npc_name_list
  resources :object_definitions
  resources :common_definitions , controller: :object_definitions , type: 'CommonDefinition'

  resources :specifications do
    collection do
      post :move_position
    end


  end

  resources :organizations
  resources :knights
  resources :adventurers_guild
  resources :advance_bases
  resources :individual_knights
  resources :corporations
  resources :other_orgs
  resources :aces do
    collection do
      get :items
      get :items_for_wiki
    end


  end

  resources :roles
  resources :i_definitions

  resources :nations do
    collection do
      get :top
      get :associated   , :action => :associated_nations_list
    end
    member do
      get :miles
    end
    resources :characters
    resources :collection
    resources :idress_wearing_tables do

      member do
        post :edit
        get :copy
        get :restriction_check
        get :vcc_format
      end

    end
  end

#  resources :mile_changes
  resources :owner_accounts do
#    get :facade
    collection do
      get :facade
      get :update_form_parts
      get :confirm
    end
    member do
      get :update_owner_account_list
      post :process_edit
    end

    resources :cash_records do
      collection do
        get :import_form
        post :import_select_table
        post :move_position
      end
    end

    resources :weapon_registries do
      collection do
        post :move_position
      end
    end

    resources :object_registries do
      collection do
        post :move_position
        get :wiki_output
      end
    end

    resources :job_idresses
    resources :mile_changes
    resources :yggdrasills

    resources :command_resources

    resources :unit_registries do
      member do
        get :rebuild
        get :disable
      end
    end
  end

  resources :unit_registries

  resources :yggdrasills do
    member do
      post :move
    end
  end

  resources :characters do
    collection do
      get :bonus_list
      post :move_position
    end
    member do
      get :acquired_idresses
      post :append_acquired_idress
      delete :remove_acquired_idress
      get :edit_note_for_acquirement
      put :update_note_for_acquirement
    end
    resources :bonds
  end

  resources :modify_commands

  match 'owner_accounts/:owner_account_id/weapons' => 'weapons#index' , :as => :weapons
  match 'owner_accounts/:owner_account_id/modify_commands' => 'modify_commands#index' , :as => :owner_account_modify_commands

  match 'taigen/compile.:format' => 'taigen#compile', :defaults => {:format => 'js'}

  match 'search/command_resources.:format' => 'search#command_resources' , defaults: {format: :js}
  match ':controller/service.wsdl' => '#wsdl'
  match '/:controller(/:action(/:id))'

end
