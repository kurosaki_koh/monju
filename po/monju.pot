msgid ""
msgstr ""
"Project-Id-Version: monju 0.30\n"
"POT-Creation-Date: 2008-04-08 00:29+0900\n"
"PO-Revision-Date: 2008-04-08 00:29+0900\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

msgid "insert"
msgstr ""

msgid "update"
msgstr ""

msgid "delete"
msgstr ""

#: app/models/cash_record.rb:-
msgid "cash record"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Event no"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Event name"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Entry url"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Result url"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Reference url"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Fund"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Resource"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Food"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Fuel"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Amusement"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Num npc"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Num"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Note"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Turn"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Position"
msgstr ""

#: app/models/cash_record.rb:-
msgid "CashRecord|Nation"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "cash record detail"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Detail type"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Character no"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Pc name"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Job type"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Fund"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Resource"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Food"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Fuel"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Amusement"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Num npc"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Num"
msgstr ""

#: app/models/cash_record_detail.rb:-
msgid "CashRecordDetail|Cash record"
msgstr ""

#: app/models/character.rb:-
msgid "character"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Name"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Originator"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Nation"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Job idress"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Character no"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Personal idress"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Qualification"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Phy"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Str"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Dur"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Chr"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Agi"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Dex"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Sen"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Edu"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Luc"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Intention"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Player name"
msgstr ""

#: app/models/character.rb:-
msgid "Character|Ar"
msgstr ""

#: app/models/event.rb:-
msgid "event"
msgstr ""

#: app/models/event.rb:-
msgid "Event|Name"
msgstr ""

#: app/models/event.rb:-
msgid "Event|Results url"
msgstr ""

#: app/models/event.rb:-
msgid "Event|Turn"
msgstr ""

#: app/models/event.rb:-
msgid "Event|Order no"
msgstr ""

#: app/models/event.rb:-
msgid "Event|Has result"
msgstr ""

#: app/models/information.rb:-
msgid "information"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Title"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Content"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Created at"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Updated at"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Url"
msgstr ""

#: app/models/information.rb:-
msgid "Information|Expired"
msgstr ""

#: app/models/job_idress.rb:-
msgid "job idress"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Name"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Phy"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Str"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Dur"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Chr"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Agi"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Dex"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Sen"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Edu"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Luc"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Note"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Race"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Job1"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Job2"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Job3"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Job4"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Nation"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Pilot"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Copilot"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Fund cost"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Food cost"
msgstr ""

#: app/models/job_idress.rb:-
msgid "JobIdress|Hq"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "latest cash record"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Url"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Xpath"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Keyword"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Row index"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Col begin"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Col end"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Parsed"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Updated"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Last modified"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Fund"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Resource"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Food"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Fuel"
msgstr ""

#: app/models/latest_cash_record.rb:-
msgid "LatestCashRecord|Entertainment"
msgstr ""

#: app/models/mile_change.rb:-
msgid "mile change"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Mile"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Reason"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Application url"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Ground url"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Owner account"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Created at"
msgstr ""

#: app/models/mile_change.rb:-
msgid "MileChange|Updated at"
msgstr ""

#: app/models/modification.rb:-
msgid "modification"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Name"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Enable"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Phy"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Str"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Dur"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Chr"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Agi"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Dex"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Sen"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Edu"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Luc"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Zero attack"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|To 5m"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|To 50m"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|To 300m"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|To 500m"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Defense"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Note"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Operation node"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Modes"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Mode"
msgstr ""

#: app/models/modification.rb:-
msgid "Modification|Fuel"
msgstr ""

#: app/models/modify_command.rb:-
msgid "modify command"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Original"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Change"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Modify type"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Owner account"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Target"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Target type"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Created at"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|Updated at"
msgstr ""

#: app/models/modify_command.rb:-
msgid "ModifyCommand|User"
msgstr ""

#: app/models/nation.rb:-
msgid "nation"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Name"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Homepage"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|King"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Regent"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Contact"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Idress list"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Fund"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Resource"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Food"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Fuel"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Short name"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Contact note"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Belong"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Race"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Num npc"
msgstr ""

#: app/models/nation.rb:-
msgid "Nation|Restriction"
msgstr ""

#: app/models/operation.rb:-
msgid "operation"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Name"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Created"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Modified"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Limit"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Content"
msgstr ""

#: app/models/operation.rb:-
msgid "Operation|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "operation node"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Name"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Enable"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Operation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Parent"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Type"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Character"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Archtype"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Acts as"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Is npc"
msgstr ""

#: app/models/operation_node.rb:-
msgid "OperationNode|Nation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "troop"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Name"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Enable"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Operation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Parent"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Type"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Character"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Archtype"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Acts as"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Is npc"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Troop|Nation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "npcsoldier"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Name"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Enable"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Operation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Parent"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Type"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Character"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Archtype"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Acts as"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Is npc"
msgstr ""

#: app/models/operation_node.rb:-
msgid "NpcSoldier|Nation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "soldier"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Name"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Enable"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Operation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Parent"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Type"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Character"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Archtype"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Acts as"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Is npc"
msgstr ""

#: app/models/operation_node.rb:-
msgid "Soldier|Nation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "idgear"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Name"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Enable"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Note"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Operation"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Parent"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Type"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Character"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Archtype"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Acts as"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Is npc"
msgstr ""

#: app/models/operation_node.rb:-
msgid "IDGear|Nation"
msgstr ""

#: app/models/owner_account.rb:-
msgid "owner account"
msgstr ""

#: app/models/owner_account.rb:-
msgid "OwnerAccount|Created at"
msgstr ""

#: app/models/owner_account.rb:-
msgid "OwnerAccount|Updated at"
msgstr ""

#: app/models/owner_account.rb:-
msgid "OwnerAccount|Owner"
msgstr ""

#: app/models/owner_account.rb:-
msgid "OwnerAccount|Owner type"
msgstr ""

#: app/models/property.rb:-
msgid "property"
msgstr ""

#: app/models/property.rb:-
msgid "Property|Name"
msgstr ""

#: app/models/property.rb:-
msgid "Property|Value"
msgstr ""

#: app/models/race.rb:-
msgid "race"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Name"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Phy"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Str"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Dur"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Chr"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Agi"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Dex"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Sen"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Edu"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Luc"
msgstr ""

#: app/models/race.rb:-
msgid "Race|Note"
msgstr ""

#: app/models/result.rb:-
msgid "result"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Originator"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Note"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Magic item"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Url"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Character"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Event"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Event name"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Not yet"
msgstr ""

#: app/models/result.rb:-
msgid "Result|Position"
msgstr ""

#: app/models/skill.rb:-
msgid "skill"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Name"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Phy"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Str"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Dur"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Chr"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Agi"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Dex"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Sen"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Edu"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Luc"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Zero attack"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|To 5m"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|To 50m"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|To 300m"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|To 500m"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Defence"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Job idress"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Fuel"
msgstr ""

#: app/models/skill.rb:-
msgid "Skill|Note"
msgstr ""

#: app/models/used_result.rb:-
msgid "used result"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Pc name"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Initial"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Result"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Note"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Url"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Character"
msgstr ""

#: app/models/used_result.rb:-
msgid "UsedResult|Event name"
msgstr ""

#: app/models/user.rb:-
msgid "user"
msgstr ""

#: app/models/user.rb:-
msgid "User|Login"
msgstr ""

#: app/models/user.rb:-
msgid "User|Email"
msgstr ""

#: app/models/user.rb:-
msgid "User|Crypted password"
msgstr ""

#: app/models/user.rb:-
msgid "User|Salt"
msgstr ""

#: app/models/user.rb:-
msgid "User|Created at"
msgstr ""

#: app/models/user.rb:-
msgid "User|Updated at"
msgstr ""

#: app/models/user.rb:-
msgid "User|Remember token"
msgstr ""

#: app/models/user.rb:-
msgid "User|Remember token expires at"
msgstr ""

#: app/models/user.rb:-
msgid "User|Role"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "yggdrasill"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Name"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Phy"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Str"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Dur"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Chr"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Agi"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Dex"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Sen"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Edu"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Luc"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Note"
msgstr ""

#: app/models/yggdrasill.rb:-
msgid "Yggdrasill|Originator"
msgstr ""
