module UtenHelper
  def sum_miles(nation)
    sum = nation.owner_account.miles
    sum += nation.characters.map{|c|c.owner_account.miles}.sum
    sum
  end
  
end
