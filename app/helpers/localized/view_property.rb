class Localized::ViewProperty
  attr_reader :options, :model, :master_class

  def initialize (model, hash)
    hash ||= {}

    @model        = model
    @yaml_data    = hash
    @options      = hash[:options] || {}
    @master_array = []
    @master_hash  = {}
    @master_class = nil

    parse_master
    @include_blank = @master_array.first.first.to_s.empty? rescue false
  end

  protected
    def parse_master
      master = @yaml_data[:masters]
      @master_array = []
      @master_hash  = {}
      @master_class = nil

      case master
      when NilClass
      when String
        model = Localized::Model[master]
        @master_class = model.active_record
        @master_array = model.masters
        @master_array.each do |key, val|
          @master_hash[key] = val
        end
      when Array
        master.each do |hash|
          key, val = hash.to_a.first
          @master_array << [key, val]
          @master_hash[key] = val
        end
      else
        raise Localized::Model::ConfigurationError,
          "Cannot accept '%s' as master. It should be an Array or :belongs_to or a String(ActiveRecord class name). Check %s." % [master.class, @model.yaml_path]
      end
    end

  public
    def [] (key)
      @yaml_data[key]
    end

    def masters
      @master_array
    end

    def master (value)
      @master_hash[value]
    end

    def reload_master
      parse_master
      return masters
    end

    def has_master?
      not masters.empty?
    end

    def include_blank?
      @include_blank
    end

    def has_time_format?
      self[:time_format]
    end

    def has_format? (postfix = nil)
      self["format#{postfix}".intern] || self[:format]
    end

    def has_column_type?
      self[:column_type].is_a?(Symbol)
    end

    def column_type
      (has_column_type? && self[:column_type]) || (has_master? && :master) || nil
    end

    def human_value (value, controller)
      controller = controller.is_a?(ActionController::Base) ? controller : controller.controller
      if has_master?
        master(value)
      elsif has_time_format?
        value.strftime(self[:time_format])
      elsif format = has_format?("_" + controller.action_name)
        format % ERB::Util.html_escape(value)
      else
        ERB::Util.html_escape(value)
      end
    end

    def human_edit (singular_name, column_name, view)
      case column_type
      when :acts_as_tree
        value  = view.instance_eval("@#{singular_name}").send(column_name)
        record = master_class.find(value) rescue nil
        html   = view.acts_as_tree_field(singular_name, column_name, master_class, record)
      when :checkbox, :check_box
        html = view.check_box(singular_name, column_name, options)
      when :radio, :radio_button
        separater = "&nbsp;"
        delimiter = "&nbsp;&nbsp;"
        html = masters.map{|key,val|
          [view.radio_button(singular_name, column_name, key, options), val] * separater
        } * delimiter
      when :master
        html = view.collection_select(singular_name, column_name, masters, :first, :last, options)
      when NilClass
        html = view.input(singular_name, column_name, options)
      else
        tag = ActionView::Helpers::InstanceTag.new(singular_name, column_name, self)
        tag.instance_eval("def column_type; :%s; end" % self[:column_type])
        html = tag.to_tag(options)
      end

      if format = has_format?("_" + view.controller.action_name)
        html = format % html
      end

      return html
    end
end



