module AssociatedActionHelper
  def default_model_class
    (dependencies_on(:model).to_a.first || self.to_s.sub(/Controller$/, '').sub(/.*::/, '')).to_s.classify.constantize
  end

  def model_class (value = nil)
    if value
      @model_class = value.is_a?(Class) ? value : value.to_s.classify.constantize
    else
      @model_class ||= default_model_class
    end
  end

  def has_many (*names)
    register_reflection(names)
  end

  def has_one (*names)
    register_reflection(names)
  end

  def register_reflection (*names)
    names.each do |name|
      reflection = model_class.reflections[name.to_s.intern]
      reflections << reflection if reflection
    end
  end

  def associated_action_prefix (reflection)
    "%s_%s" % [reflection.active_record.to_s.gsub(/::/, '_').underscore, reflection.name]
  end

  def associated_url (reflection, record, action_format = "%s", options = {})
    {
      :controller => reflection.class_name.to_s.gsub(/::/, '_').underscore,
      :action     => action_format % associated_action_prefix(reflection),
      :id         => record.is_a?(ActiveRecord::Base) ? record.id : record,
    }.merge(options)
  end

  def belongs_to (*names)
    associations = model_class.reflect_on_all_associations(:belongs_to).select{|i| names.include?(i.name)}
    owners       = associations.collect{|reflection| reflection.class_name}.uniq
    owners.each do |owner_class_name|
      owner_class_name.constantize.reflections.values.each do |reflection|
        next unless reflection.class_name == model_class.to_s
        next unless [:has_many, :has_one].include?(reflection.macro)
        associated_action_for(reflection)
      end
    end
  end

  def associated_action_for (reflection)
    nested_prefix = self.to_s.sub(/[^:]+\Z/, '')
    include "#{nested_prefix}#{reflection.active_record}#{reflection.macro.to_s.camelize}#{reflection.name.to_s.camelize}".constantize
  rescue NameError
    message = "Association Error: '%s' expects '%s' contains '%s'. Add proper rule to '%s' class, or remove 'belongs_to :%s' line from your controller." % [self, model_class, reflection.macro, model_class, reflection.klass]
    ActiveRecord::Base.logger.debug(message)
  end

  def reflections
    @reflections ||= []
  end

  def each_reflection
    reflections.each {|ref| yield(ref) }
  end
end
