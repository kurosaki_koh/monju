module IpeHelper
  class ValueHookedInstanceTag < ::ActionView::Helpers::InstanceTag
    def initialize (object_name, method_name, template_object, local_binding = nil, object = nil, &block)
      super(object_name, method_name, template_object, local_binding, object)
      @value_hook = block
    end

    def value
      val = super
      val = @value_hook.call(val) || val if @value_hook
      return val
    end
  end

  def in_place_collection_editor(field_id, options = {})
    function =  "new Ajax.InPlaceCollectionEditor("
    function << "'#{field_id}', "
    function << "'#{url_for(options[:url])}'"

    js_options = {}
    js_options['cancelText'] = %('#{options[:cancel_text]}') if options[:cancel_text]
    js_options['okText'] = %('#{options[:save_text]}') if options[:save_text]
    js_options['rows'] = options[:rows] if options[:rows]
    js_options['externalControl'] = options[:external_control] if options[:external_control]
    js_options['ajaxOptions'] = options[:options] if options[:options]
    js_options['callback']   = "function(form) { return #{options[:with]} }" if options[:with]
    js_options['model'] = (options[:model] || '').inspect
    js_options['collection'] = options[:collection].inspect
    function << (', ' + options_for_javascript(js_options)) unless js_options.empty?
        
    function << ')'

    javascript_tag(function)
  end

  def in_place_collection_editor_field(object, method, tag_options = {}, in_place_editor_options = {})
    collection   = in_place_editor_options[:collection]
    value_method = in_place_editor_options[:collection_value_method] || :first
    text_method  = in_place_editor_options[:collection_text_method]  || :last
    tag = ValueHookedInstanceTag.new(object, method, self) {|value|
      collection.find{|master| value == master.send(*value_method)}.send(*text_method) rescue value
    }

    tag_options = {:tag => "div", :id => "#{object}_#{method}_#{tag.object.id}_in_place_editor", :class => "in_place_editor_field"
    }.merge!(tag_options)
    in_place_editor_options[:url]        ||= url_for({ :action => "set_#{object}_#{method}", :id => tag.object.id })
    in_place_editor_options[:model]      ||= instance_eval("@#{object}").send(method) || ''
    in_place_editor_options[:collection] = @controller.instance_eval(collection.to_s) if collection.is_a?(Symbol)

    tag.to_content_tag(tag_options.delete(:tag), tag_options) +
      in_place_collection_editor(tag_options[:id], in_place_editor_options)
  end

  def human_attribute_ipe (singular_name, column_name, options={}, ipe_options={})
    singular_name = singular_name.to_s
    property      = view_property(singular_name, column_name)
    collection    = ipe_options[:collection] || (property.has_master? ? property.masters : nil) rescue nil
    ipe_options   = (property[:ipe_options] || {}).merge(ipe_options) rescue {}

    if collection
      ipe_options = ipe_options.merge(:collection=>collection)
      in_place_collection_editor_field(singular_name, column_name, options, ipe_options)
    else
      options = options.merge(:tag=>"div") # to click empty field
      in_place_editor_field(singular_name, column_name, options, ipe_options)
    end
  end
end
