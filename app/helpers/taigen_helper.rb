# -*- encoding: utf-8 -*-
require_dependency 'i_language/calculator'
module TaigenHelper
  #  include ILanguage::Calculator
  include TaigenInspector
  include TaigenInspector::Helper

  Abilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']

  def tab_box(tab_id)
#    javascript_tag(%Q! new TabBox("##{name}");!)
    javascript_tag(%Q! $(function() { $("##{tab_id}").tabs(); } ) !)
  end

  def troop_evals(unit)
    unit.evals_str.gsub("\n","<br/>\n")
  end

  def defense_evals(unit)
    unit.defense_evals_str.gsub("\n","<br/>\n")
  end

  def action_evals(unit)
    unit.action_evals_str.gsub(/\n/,"<br/>\n")
  end

  def derived_evals(unit)
    unit.derived_evals_str.gsub("\n","<br/>\n")
  end

  def common_evals(unit)
    unit.common_abilities_evals_str.gsub("\n","<br/>\n")
  end

  def fleet_evals(fleet)
    result = ''
    manipulation = Evaluator['操縦（個別）']
    #    calc_set = manipulation.calculate(fleet)
    for c in fleet.children
      man_calc = manipulation.calculate(c)
      unless man_calc.is_na?
        result << <<-EOT
        <p>
        ●#{c.name}<br/>
        #{c.evals_str.gsub("\n","<br/>\n")}<br/>
        操縦：#{ man_calc.total }<br/><br/>
        #{inspect_leader(c)}<br/>
        </p>
        EOT
      end
    end
    result.html_safe
  end

  def format_row(name,hash,cols = Abilities)
    return '' if hash.nil?
    result = "
      <tr >
        <th aiign=left>
           #{name}
        </th>

        <td  align=right>"
    if hash.class <= BonusSpec
      result << cols.collect{|c|
        hash.value(c)
      }.join('</td> <td  align=right>')
    else
      result << cols.collect{|c|
        hash[c].to_s if hash.has_key?(c) || hash.has_key?('全能力')
      }.join('</td> <td  align=right>')
    end
    result << "
      </td>
      </tr>"
    if name == '個人修正' && hash.keys.include?('全能力')
      all_hash = Abilities.inject({}){|h,c| h[c]=hash['全能力'] ; h}
      result << format_row('個人補正(全能力)',all_hash,cols)
    end
    raw result
  end

  def inspect_pilot_evals_table(unit,sums)
    result = ''

    #非ACEパイロットと機体評価の合算
    pilot_cols = [nil,nil,nil,nil,nil,'器用','感覚','知識','幸運']
    pilot_items = unit.pilot_items
    for data in pilot_items
      result << format_row(h(data.name),data,pilot_cols)
    end
    max_values = ['器用','感覚','知識','幸運'].inject({}){|hash,col|
      hash[col] = pilot_items.collect{|data|data.value(col)}.max
      sums[col] += hash[col].to_i
      hash
    }
    result << format_row('最大値',max_values , pilot_cols)

    #ACEパイロットと機体評価のうち最大値を抽出

    if unit.aces.size > 0
      result << format_row('小計' , sums , StandardAbilities)
      ace_items = unit.ace_items
      max_values = {}
      for data in ace_items
        result << format_row(h(data.name) + '(ACE)' , data , StandardAbilities)
      end
      max_values = StandardAbilities.inject({}){|hash,col|
        values = ace_items.collect{|ace_item|ace_item.value(col)}
        values << sums[col]
        hash[col] = values.max
        hash
      }
      result << format_row('ACE及び機体での最大値',max_values , StandardAbilities)
      sums.update(max_values)
    end
    raw result
  end

  def inspect_career_evals_table(unit,sums)
    pilot_cols = [nil,nil,nil,nil,nil,'器用','感覚','知識','幸運']
    if unit.evaluation_mode == :selection
      inspect_pilot_evals_table(unit,sums)
    else
      rd_sum_rows(unit.children , pilot_cols  , sums)
    end
  end


  def rd_sum_rows(units , columns  , sums)
    result = ''
    sum_rd = {}
    for unit in units
      hash = {}
      for col in columns
        if col.nil?
          hash[col] = ''
          next
        end
        value = unit.evaluate(col)
        rd = to_rd(value)
        hash[col] = sprintf("%d : %.1f" , value , rd)
        sum_rd[col] ||= 0.0
        sum_rd[col] += rd
      end
      eval_data = EvalData.new(unit.name , hash)
      result << format_row(h(unit.name) , eval_data , columns)
    end
    result << format_row('乗員RD合計' , sum_rd , columns)

    sum_eval = columns.compact.inject({}){|hash,col|
      if sum_rd[col]
        hash[col] = to_eval(sum_rd[col])
        sums[col] += hash[col]
      end
      hash
    }
    result << format_row('乗員一般評価' , sum_eval , columns)
    return result.html_safe
  end

  def inspect_passenger_evals_table(unit , sums)
    rd_sum_rows(unit.crew_division.children , StandardAbilities , sums)
  end

  def inspect_evals_table(child,division)
    sums = Hash.new{|h,k|h[k]=0}
    optional_keys = child.evals.keys.select{|k|!Abilities.include?(k) }.inject([]){|array,item|
      array << item unless array.include?(item)
      array
    }
    columns = Abilities + optional_keys
    result = '<br/>・'
    result +=  "・" unless child.is_troop_member?
    result += link_to_toggle('評価値詳細','evals',division,division.context.current_situation,child)
    result << "
<br/>
<div id='evals_#{division.hash}_#{division.context.current_situation.hash}_#{child.hash}'  style='overflow: visible; display: none;'>
<table border=1>
<tr>
  <th></th>
  <th>#{columns.join('</th><th>')}</th>
</tr>
    "

    for data in child.evals_items
      name = data.name
      result << format_row(h(name) ,data,columns)
      sum_rows(sums,data,columns)
    end

    case child
    when Career
      result << inspect_career_evals_table(child,sums)
    when Vehicle
      result << inspect_pilot_evals_table(child,sums)
    when Vessel
      result << inspect_passenger_evals_table(child,sums)
    end

    for data in child.evaluate_items
      name = data.name
      if data.class == BonusSpec && (data.owner != data.target)
        name = "( #{data.owner.name} )<br/>" + data.name
      end
      current_columns = if child.class <= Vessel && data.class  <= BonusSpec
        if data.is_judge_bonus? || !data.is_target?(child, '体格')
          ([nil] * StandardAbilities.size + optional_keys)
        else
          StandardAbilities.dup
        end
      else
        columns
      end
      result << format_row(name ,data,current_columns)

      #      result << format_row(name ,data,columns)
      sum_rows(sums,data,current_columns)
    end
    result << format_row('合計',sums,columns)
    result << "</table>"
    result << "</div>\n"
    result << child.children.collect{|c|h(c.name) + '　' +  inspect_evals_table(c,division) + inspect_unit_sp(c,division)}.join
    result.html_safe
  end
  def sum_rows(sums,data,keys = data.keys)
    return if data.nil?
    for col in keys
      if data.class == BonusSpec
        sums[col] += data.value(col).to_i
      else
        next unless (data.has_key?(col) || data.has_key?('全能力'))
        data = data.to_hash if data.class != Hash
        sums[col]+= data[col].to_i
      end
    end
  end

  def restriction_check(child)
    return unless child.is_registered_character?
    character = child.registered_character

    idefs = child.idresses.uniq.collect{|i|IDefinition.find_by_name(i)}.compact
    specs = idefs.collect{|idef|idef.compiled_hash}.flatten.compact
    restrictions = specs.select{|sp| sp.spec_type == '使用制限'}
    result =  if character
      result = "<br/>　　#{child.name}:根源力 #{character.sum_originator}:#{child.idresses.join('+')}<br/>"
      result << restrictions.collect{|r|r.source}.join("<br/>\n)")
      result
    else
      nil
    end
    result.html_safe
  end

  def inspect_children(division)
    result = "\n"
    for child in division.children
      optional_keys = child.evals.keys.select{|k| !Abilities.include?(k)}
      keys = child.evals.keys
      result << "・" unless child.is_troop_member?
      result << "#{child.name}："
      result << (Abilities+optional_keys).select{|c| keys.include?(c)}.collect{|c| "#{c}:#{v = child.evaluate(c)} "}.join
      result << " ＡＲ：#{child.ar}"
      result << link_to('[個人詳細]' , character_path(child.registered_character) ,:target => 'monju') if child.is_registered_character?
      result << inspect_evals_table(child,division)
      result << inspect_unit_sp(child , division)
      result << "\n<br/>"
    end
    result << "\n<br/>\n"
    result.html_safe
  end

  def inspect_child_sp_list(unit, division)
    result = unit.is_troop_member? ? '' : '・'

    raw (result + inspect_unit_sp(unit , division) )
  end


  def rd_table_line_tag(unit,columns , optional_keys)
    result = ''
    result << "
<tr>
  <td> #{unit.name}</td>"

    result << (columns + optional_keys).collect{|c|
      col_str = unit.evals.has_key?(c) ? "#{v = unit.evaluate(c)} : #{to_rd(v)}" : ''
      "<td> #{col_str}</td>\n"
    }.join
    result << "</tr>\n"
    result.html_safe
  end

  def rd_table_tag(troop,title = '')
    sum_hash = {}
    keys = troop.evals.keys
    initial_value = troop.num_of_members == 0 ? 1 : 0
    keys.each{|c|sum_hash[c] = initial_value}
    optional_keys = keys.select{|k|!Abilities.include?(k)}
    if troop.formation == :fleet
      columns = optional_keys
      title = '艦船'
    else
      columns = Abilities + optional_keys
    end
    evals_items = troop.evals_items
    evaluate_items = troop.evaluate_items
    link_str = "#{title}RD合算#{'及び部隊補正' if troop.personal_bonuses || (evals_items.size + evaluate_items.size )>0}："

    result = '<p/>■'+link_to_toggle(link_str , 'rd_table' , troop , troop.context.current_situation)
    result << "
<br/>
<div id='rd_table_#{troop.hash}_#{troop.context.current_situation.hash}'  style='overflow: visible; display: none;'>
<table border=1>
<tr>
  <th></th>
  <th>#{columns.join('</th><th>')}</th>
</tr>"

    members = troop.children
    for child in members
      result << rd_table_line_tag(child,columns , [])
      for c in columns
        sum_hash[c] += to_rd(child.evaluate(c)) if child.evals.has_key?(c)
      end
    end
    total_row = columns.collect{|col|
      if ((val=sum_hash[col]) == 0) || val.nil?
        "<td> - : -"
      else
        "<td> #{to_eval(val)} : #{val}"
      end
    }.join("\n")
    evals_items = troop.evals_items

    result << "
<tr>
<th>#{evals_items.size > 0 ? '小計：': '合計：' }</th>
#{total_row}
</tr>"

    if evals_items.size + evaluate_items.size > 0
      sum_hash2 = {}
      for item in evals_items + evaluate_items
        row = "
<tr>
<th> #{item.name}
</th>"

        row << columns.collect{|c|
          sum_hash2[c] ||= (sum_hash[c] == 0) ? 0 : to_eval(sum_hash[c])
          if item.value(c)
            sum_hash2[c] += item.value(c) if item.value(c)
            "<td> #{item.value(c)} </td>\n"
          else
            "<td></td>"
          end
        }.join("\n")
        row << "</tr>\n"
        result << row
      end
      total_row = columns.collect{|c|"<td> #{sum_hash2[c] } </td>\n"}.join("\n")
      result << "
<tr>
<th>合計：</th>
#{total_row}
</tr>"
    end
    result << "\n</tbody></table>\n</div>\n"
    result.html_safe
  end


  def action_detail_header(unit,ev)
    text = ''
    action_name = ev.action_name
    calc = ev.calculate(unit)

    eval_str = (ev.is_undefined? || ev.is_action_bonus? || calc.is_na? ) ? '-' : calc.total
    #表題
    text << "<h3>■#{ev.display_name}"

    text << " 評価：#{eval_str}" unless ev.is_individualy?
    text << "</h3>\n"
    return text if ev.is_undefined?
    #メンバ内訳
    text << link_to_toggle('内訳：',"ev" , unit , unit.context.current_situation , ev)
    text << "<div id=\"ev_#{unit.hash}_#{unit.context.current_situation.hash}_#{ev.hash}\" ,  style='overflow: visible; display: none;padding-left: 10px;'>\n"
    unless (action_name.blank? || action_name =~ /防御/ || ['偵察'].include?(action_name)) || unit.num_of_members == 0
      text << "行為可能:#{num_owners = unit.num_of_action_owner(action_name)} / 人数:#{num_member = unit.num_of_members} = #{(num_owners.to_f / num_member*100).to_i}% <br/>"
    end
    text << "<br/>\n"
    raw text
  end

  [ILanguage::Calculator::Calculation ,ILanguage::Calculator::LeaderCalculation].each do |klass|
    klass.class_eval do
      def is_simple_calculation?
        true
      end

      def create_inspectors
        EvalInspector.new(self.evaluator,self.unit,self.unit.troop,self)
      end
    end
  end

  ILanguage::Calculator::TeamCalculation.class_eval do
    def is_simple_calculation?
      false
    end

    def create_inspectors
      inspectors = self.children.collect{|c|
        c.create_inspectors
      }
      InspectorArray.new(self.unit,self.evaluator,inspectors , self)
    end
  end
  ILanguage::Calculator::CalculationSet.class_eval do
    def is_simple_calculation?
      false
    end

    def create_inspectors
      inspectors = self.children.collect{|c|
        c.create_inspectors
      }
      CalculationSetInspector.new(self.unit,self.evaluator,inspectors , self)
    end
  end

  ILanguage::FCalc::FunctorCalculation.class_eval do
    def is_simple_calculation?
      true
    end

    def create_inspectors
      ExpressionInspector.new(self)
    end
  end

  ILanguage::Calculator::Evaluator.class_eval do
    def is_simple_calcuration?(troop)
      self.calculate(troop).class == Calculation
    end

    def create_inspectors(unit)
      inspectors = if is_simple_calcuration?(unit)
        [EvalInspector.new(self,nil,unit)]
      else
        calc = self.calculate(unit)
        calc.children.collect{|c|
          EvalInspector.new(c.evaluator,c.unit,unit , c)
        }
      end
      InspectorArray.new(unit,self,inspectors)
    end
  end

  ILanguage::Calculator::ControlEvaluator.class_eval do
    def create_inspectors(unit)
      if self.is_individualy?
        calc = self.calculate(unit)
        inspectors = calc.children.collect{|sub_team_calc|
          sub_inspectors = sub_team_calc.children.collect{|c|EvalInspector.new(c.evaluator,c.unit,c.unit , c)}
          InspectorArray.new(sub_team_calc.unit ,sub_team_calc.evaluator , sub_inspectors,sub_team_calc)
        }
        InspectorArray.new(unit,self,inspectors)
      else
        super(unit)
      end
    end
  end

  ILanguage::Calculator::SynchronizeEvaluator.class_eval do
    def is_simple_calcuration?(troop)
      false
    end

    def create_inspectors(unit)
      details = self.details(unit)
      members = details.all_leaves
      results = members.collect{|c|EvalInspector.new(self,c.unit , unit)}
      array = InspectorArray.new(unit , self , results , details)
      array
    end
  end

  module NamedScholer
    def to_s
      case self.bonus
      when Integer
        sprintf("%+d%s" , self.bonus.to_i , self.name ? "（#{self.name}）":'')
      when String
        sprintf("+%s%s" , self.bonus , self.name ? "（#{self.name}）":'')
      end
    end
  end

  [ILanguage::Calculator::BonusSpec , ILanguage::Calculator::EvalData].each do |klass|
    klass.class_eval do
      include NamedScholer
    end
  end



  def actions_details_text(troop,ev)
    result = action_detail_header(troop , ev)
    return result if ev.is_undefined?
    #    inspectors = ev.create_inspectors(troop)
    calc = ev.calculate(troop)
    inspectors = calc.create_inspectors
    #    def initialize(unit,evaluator,inspectors , details = nil)
    inspectors = InspectorArray.new(troop,ev , [inspectors] , calc) if  inspectors.class == EvalInspector

    result << inspectors.actions_details_text.html_safe
    result << "</div>\n".html_safe

    result.html_safe
  end


  def expr_tab(troop)
    result = "<h3>■数式：</h3>"
    expr_calc = ILanguage::FCalc::FunctorCalculation.new
    begin
      expr_calc.result = troop.expr.evaluate(troop , :calculation => expr_calc)
    rescue RuntimeError => e
      expr_calc.result = nil
      expr_calc.error = e
      #do_nothing
    end
    result << expr_calc.create_inspectors.actions_details_text
    result.html_safe
  end

  def actions_tab_content(troop)
    result = ''
    for ev in troop.evaluators
      next unless ev || ev.is_implicitly?
      result <<  actions_details_text(troop , ev)
    end

    result << expr_tab(troop) if troop.expr

    return result if troop.children.size == 0
    sync = Evaluator['同調']
    result << actions_details_text(troop , sync)

    manipulation = Evaluator['操縦（個別）']
    sync_vessel = Evaluator['同調（機内）']

    if troop.formation == :fleet
      for vessel in troop.children
        result << "<h3>■個別評価:#{vessel.name}</h3>\n"
        result << actions_details_text(vessel , manipulation)
        result << actions_details_text(vessel , sync_vessel)
      end
    end

    if troop.non_action_bonus_tqrs.size > 0
      children_num = troop.num_of_members
      result << "<h3>■行為以外の７５％制限能力 </h3>\n<ul>\n"
      for tqr_name in troop.non_action_bonus_tqrs
        num_of_tqr_owners = 0
        troop.children.each{|c| num_of_tqr_owners += 1 if !c.is_attendant? && c.non_action_bonus_tqrs.include?(tqr_name)}
        result << "  <li>#{tqr_name}： #{num_of_tqr_owners} / #{children_num} = #{(num_of_tqr_owners.to_f / children_num*100).to_i}% </li> \n"
      end
      result << "</ul>\n"
    end
    result.html_safe
  end

  def select_specification(all_sp)
    specs = all_sp.select{|sp|
      !['行為','行為補正','補正','その他定義'].include?(sp.spec_type) ||
        (sp.spec_type == 'その他定義' && (sp.name =~ /の形状$/o))
    }
    specs.collect{|sp| sp.source}.join("<br/>\n").html_safe
  end

  def select_etc_sp(all_sp)
     select_sp(all_sp,'その他定義'){|sp|sp['付与'].nil? && sp.name !~ /の形状$/o}
  end

  def select_action_sp(all_sp)
    select_sp(all_sp,'行為'){|sp| sp['付与'].nil?}
  end

  def select_enhancement_sp(all_sp)
    select_sp(all_sp,'行為','その他定義'){|sp| sp['付与']}
  end

  def list_items_tag(items)
    items.collect{|item| "<li>" + item + "<li>\n"}.to_s.html_safe
  end

  def select_sp(all_sp,*sp_types , &block )
    specs = all_sp.select{|sp| sp_types.include?(sp.spec_type)}
    if block
      specs = specs.select(&block)
    end
    specs
  end

  def sp_content(specs)
    if specs.size == 0
      return "　　（なし）<br/>\n".html_safe
    end

    defines = specs.collect do |sp|
      ( block_given? ? yield(sp) :sp.source) + (sp.is_disabled? ? content_tag(:span , style: 'color: red'){ '＃特殊無効' }  : '')
    end
    (defines.join(tag(:br) + "\n")  + tag(:br)).to_s.html_safe
  end

  def condition_enabler_tag(sp)
    head = ''
    if sp.class <= BonusSpec && !sp.is_used?
      cond = sp.condition
      if cond != ''
        cond_str = cond.sub(/^（/,'').sub(/）$/,'')
        link_str = link_to_function('[有効条件に追加]' , "insert_condition_enabler_directive('#{cond_str}');" )
      else
        link_str = ''
      end
      head = "<font color=#ff8800><strong>（未使用）</strong></font>#{link_str}"
    end
    head.html_safe
  end


  def inspect_unit_sp(unit,division = unit.troop)
    result = ''

    link_str = (unit.is_troop_member? ? '' : '・') +"・#{unit.name} 特殊一覧"
    if unit.class <= Troop
      link_str = "■全特殊一覧"
    end
    all_sp = unit.collect_all_sp_for_view
    result = link_to_toggle_part("#{link_str}：","sp_list" , division ,division.context.current_situation, unit){
      specs_part = link_to_toggle_part("・諸元一覧：","spcification_list", division ,division.context.current_situation, unit){
        select_specification(all_sp)
      } << link_to_toggle_part("・その他特殊一覧：","further_list" , division ,division.context.current_situation, unit){
        sp_content( select_etc_sp(all_sp) )
      } << link_to_toggle_part("・他者補正能力一覧：","enhancements_list" , division ,division.context.current_situation, unit){
        sp_content( select_enhancement_sp(all_sp) ){|sp|
          enhancement = unit.context.definitions[sp['付与']['付与アイドレス']]
          next if enhancement.nil?

          idress_part = "＜#{sp['付与']['付与アイドレス']}＞"
          source = h(sp.source)
          enhancement_defs = enhancement['原文'].each_line.select{|line|
            line.include?('補正　＝　')
          }.collect{|line| line.gsub(/^　　＊/,'＊')}.join("\n'&#10;'")

          source.gsub(idress_part , content_tag(:span , idress_part , :title => enhancement_defs) )
        }
      } << link_to_toggle_part("・行為一覧：","actions_list" , division ,division.context.current_situation, unit){
        sp_content( select_sp(all_sp,'行為') )
      }

      bonuses = all_sp.select{|sp|sp.class <= BonusSpec}
      specs_part << link_to_toggle_part("・行為補正・補正一覧（使用済み）：","used_list" , division ,division.context.current_situation, unit){
        used_bonuses = bonuses.select{|sp|
          flag = if unit.class <= Troop
            division.is_sp_used?(sp)
          else
            sp.is_used?
          end

          if division.formation == :fleet
            flag |= division.crew_division.is_sp_used?(sp) | division.crew_division_getted_off.is_sp_used?(sp)
          end
          sp.bonus if flag
          flag
        }

        sp_content( select_sp(used_bonuses,'行為補正','補正')){|sp|
          (unit.class <= ContainerUnit ? "(#{sp.owner.name})<br/>" : '') + h(sp.source)
        }
      }

      specs_part << link_to_toggle_part("・行為補正・補正一覧（未使用）：","unused_bonus_list" , division,division.context.current_situation , unit){
        unused_bonuses = bonuses.select{|sp|
          if unit.class <= Troop
            !division.is_sp_used?(sp)
          else
            !sp.is_used?
          end
        }

        sp_content( select_sp(unused_bonuses,'行為補正','補正')){|sp|
          (unit.class <= ContainerUnit ? "(#{sp.owner.name})<br/>" : '') + condition_enabler_tag(sp) + h(sp.source)
        }
      }


      specs_part << inspect_command_resources(division , unit) if division.is_convertible? && !unit.is_attendant?

      specs_part
    } << tag(:br)

  end

  def inspect_command_resources(division , unit)
    crs = division.context.parse_command_resources
    converted_crs = division.context.converted_specialties.values
    inspection_converted = crs.reject{|cr|converted_crs.include?(cr)}.collect{|cr|
       [cr[:name] , cr[:power] , cr[:tag]].join("：")
    }

    inspection_resourcified = division.context.converted_specialties.collect{|sp_name , cr|
      [cr[:name] , cr[:power] , cr[:tag]].join("：") + '　＃＊' + sp_name
    }

    link_to_toggle_part("・コマンドリソース：" , 'converted_sp_list' , division,division.context.current_situation , unit){
      (inspection_converted + inspection_resourcified).join(tag(:br)).to_s.html_safe
    }
  end

  def inspect_troop_cost(troop)

    sps = "<div id='cost_list'   style='overflow: visible; display: none;'>\n"
    cost = troop.total_cost{|u,sp|
      line = h(u.name) + ':' + h(sp.source)
      line += "＃ × #{troop.hops}航路" if sp.is_consumed_by_hops?
      line += "<br/>\n"
      sps << line
    }
    result = ''
    result << inspect_sp_cost_values(cost,true)
    result << inspect_sp_cost_values(cost,false)

    result << link_to_toggle('内訳：','cost_list') + tag(:br) + "\n" + raw(sps) + raw("\n</div>\n")
    result.html_safe
  end


  def inspect_sp_cost_values(cost,light = true)
    result = "特殊消費（#{light ? '軽' : '重'}編成時）：<br/>\n"

    for key in %w[資源 食料 燃料]
      result += "#{key}：#{(cost[key] || 0).floor.abs * (light ? 1 : 3)}万ｔ<br/>\n"
    end

    result += "生物資源：#{(cost['生物資源'] || 0).floor.abs * (light ? 1 : 3)}万ｔ<br/>\n" if cost.has_key?('生物資源')
    result += "<br/>\n"
  end

  def inspect_resource_modification(u,sp)
    cost = sp.cost
    rms = cost.keys.inject({}){|hash , key|
      rm = sp.owner.resource_reduction_coefft(sp,key)
      hash[key] = rm.to_s if rm != 1.0
      mutiplier = sp.owner.resource_multiplier(sp,key)
      if mutiplier != 1.0
        if hash[key]
          hash[key] += " * #{mutiplier}"
        else
          hash[key] = mutiplier.to_s
        end
      end
      hash
    }
    rms_str = ''
    rms_str += " * #{u.troop.hops}航路。　" if sp.is_consumed_by_hops?
    rms_str += rms.keys.collect{|key|"#{key} * #{rms[key]}倍"}.join(' , ')
    raw( (rms_str == '') ? '' : " ＃" + rms_str )
  end

  def inspect_event_cost(troop)
    sps = raw "<div id='event_cost_list'   style='overflow: visible; display: none; margin-left:10pt;'>\n"
    costs = troop.event_cost{|u,sp|
      sps << h(u.character_no) + ':' if u.character_no
      sps << h(u.name) + ':'
      sps << h(sp.source)
      sps << '（猫・犬士につき半減）' if u.class <= Soldier && u.is_npc? && sp.lib.is_race?
      sps << inspect_resource_modification(u,sp)
      sps << "<br/>\n"
    }
    result = troop.event_cost_str.gsub("\n","<br/>\n")

    result << link_to_toggle(raw('<br/>内訳：'),'event_cost_list') + tag(:br) + "\n" + raw(sps) + raw("\n</div>\n")

    costs.each_pair{|k,v| costs[k] = costs[k].floor}
    raw result
  end

  def inspect_sp_cost(troop)
    list = []
    costs = {}
    result = troop.sp_cost_str{|u,sp|
      if sp.cost && sp.cost.keys.size > 0
        line = h(sp.owner.name) + ':' + h(sp.source)
        line << inspect_resource_modification(u,sp)
        list << line
      end
      sp.cost.keys.each{|key|
        costs[key] ||= 0
        cost_val =  sp.cost[key]
        cost_val *= troop.hops if sp.is_consumed_by_hops?
        costs[key] += cost_val
      }
    }.gsub("\n",tag(:br) + "\n")
    list.sort!

    result.gsub!(/特殊消費/,"特殊消費（重編成の場合は３倍） ")

    sps = raw "<div id='cost_list'   style='overflow: visible; display: none; margin-left:10pt;'>\n"
    sp_list = sps + raw(list.join(tag(:br) + "\n"))
    result += link_to_toggle('内訳：','cost_list') + tag(:br) + "\n" + sp_list
    cost_str = costs.keys.collect{|key| "#{key} #{costs[key]}"}.join(",")
    result << <<EOT
<br/>
（消費合計：#{cost_str }）
EOT
    result += raw "<br/>\n</div>\n"
    raw result
  end

  def instpect_costs_apply_details(troop)
    cost_detail = ["資源","燃料","食料","生物資源","資金"].collect{|r_type|
      rmd = ResourceModificationDetail.new(troop , r_type)
      rmd.print_details
    }.flatten
    cost_detail_str = cost_detail.join("\n").gsub("\n","<br/>\n")
    result = if cost_detail_str.size > 0
      "<br/><p>(E-3.5) 特殊消費（重編成の場合は３倍）および消費修正<br/>\n" + cost_detail_str + "</p><br/>"
    else
      ''
    end
    raw result
  end

  def inspect_total_cost(troop)
    raw troop.organization_cost_str.gsub("\n","<br/>\n")
  end

  def inspect_total_cost_values(troop , e_cost , sp_cost , light = true)
    result = "（#{light ? '軽': '重'}編成時）<br/>"

    keys = %w[資源 食料 燃料]
    %w[生物資源 資金].each do |kind|
      keys << kind if [e_cost , sp_cost].any?{|costs|costs.has_key?(kind)}
    end

    for key in keys
      source = "#{e_cost[key].floor.abs} + ( #{sp_cost[key].floor.abs} * #{light ? 1 : 3} )"
      if var = troop.variable("#{key}コスト倍率")
        source = "( #{source} ) * #{(var.evaluate(troop) * 100).floor.to_f / 100}"
      end
      begin
        expr = troop.context.fcalc_parser.parse(source).interpret
        calc = expr.calculate(troop)
      rescue
        raise source
      end

      result += "#{key}：#{calc.total.floor}万ｔ　　　＃#{source} = #{calc.total}<br/>\n"
    end
    raw (result + "<br/>\n")
  end

  def format_leaves(calc)
    result = ''
    calc.all_leaves.each do |leader|
      result += "#{leader.unit.character_no + '_' if leader.unit.character_no}#{leader.unit.name}：#{calc.total}<br/>\n"
    end
    raw result
  end
  def inspect_leader(unit)
    raw unit.leaders_str.gsub("\n","<br/>\n")
  end

  def division_id(division)
    "division_#{division.hash}"
  end

  def troop_tag(troop)
    if  troop.divisions.size == 0
      division_tab(troop)
    else
      content_tag(:div , :id => 'troop_tab' ) {
        content = content_tag(:ul){
          troop.all_divisions.
              collect{|div|tab_item_tag(div.name , division_id(div))}.
              inject{|c ,  c2| c << c2}
        }

        troop.all_divisions.each{|div|
          content << division_tab(div)
        }
        content
      }<<  tab_box('troop_tab')
    end
  end


  def division_tab(division , name=division.name)
    content_tag(:div , :id => division_id(division)){
      if division.all_situation_names.size == 1
        division.context.change_to_default_situation(division)
        division.change_to_default_situation(division)
        division_tab_content(division)
      else
        situation_tab(division)
      end
    }
  end


  def situation_id(division , s_name = '')
    "situations_#{division.hash}_#{s_name}"
  end

  def tab_item_tag(tab_name , id)
    content_tag(:li){content_tag(:a , tab_name ,  href: '#'+ id) }
  end

  def situation_tab( division)
      content_tag(:div , :id => situation_id(division) ) {
        content = content_tag(:ul){
          division.all_situation_names.
              collect{|s_name|tab_item_tag(s_name , situation_id(division , s_name))}.
              inject{|c ,  c2| c << c2}
        }

        division.all_situation_names.each{|s_name|
          content << situation_tab_content(s_name , division)
        }
        content
      }<<  tab_box(situation_id(division))
  end


  def situation_tab_content(situation,division)
    division.context.change_situation_to(situation,false)
    division.change_situation_to(situation)
    division.reset_evals
    content_tag(:div , :id => situation_id(division ,situation ) ) {
      division_tab_content(division )
      #result = h(situation)
      #result << division_tab_content(division)
      #raw result
    }
  end

  def division_tab_content(division)
    tab_id = "div_#{division.hash}_#{division.context.current_situation.hash}"
    flag =  division.children.size > 0 && division.class == Troop && division.context.current_situation_name == '（基本）'
     content_tag(:div, :id => tab_id) {

       content =content_tag(:ul){
        tabs = content_tag(:li){ content_tag(:a ,'評価まとめ '  , href: '#'+evaluation_tab_id(division)) }
        tabs << content_tag(:li){ content_tag(:a ,'部隊員詳細', href: '#'+members_tab_id(division) ) } if division.children.size > 0
        tabs << content_tag(:li){ content_tag(:a ,'判定評価詳細', href: '#'+actions_tab_id(division) ) }
        tabs << content_tag(:li){ content_tag(:a ,'コスト', href: '#cost_tab_content' ) } if flag
        tabs
      }
      content <<  evaluation_tab(division)
      content << members_tab(division) if division.children.size > 0
      content << actions_tab(division)
      content << costs_tab(division) if flag
      content
    } <<  tab_box(tab_id)
  end

  def evaluation_tab_id(division)
    "evaluation_tab_#{division.hash}_#{division.context.current_situation.hash}"
  end
  def evaluation_tab(division)
    s_name = division.context.current_situation_name
    content_tag(:div , id: evaluation_tab_id(division)) {
        common_str = common_evals(division)
        action_str = action_evals(division)
        derived_evals_str = derived_evals(division)

        if params[:do_break]
          common_str.gsub!(/ \/ (<br\/>\n・)?（/mo,"<br/>・（")
          common_str.gsub!(/ \/ (<br\/>\n・)?/mo,"<br/>")
          action_str.gsub!(/ \/ (<br\/>\n・)?（/mo,"<br/>・（")
          action_str.gsub!(/ \/ (<br\/>\n・)?/mo,"<br/>")
        end
        result = if division.children.size == 0
          ''
        else
          leaders_str = inspect_leader(division)
          result = "
          <p>
            <b>
＜#{division.name}＞#{'（' + s_name + '）' if s_name != '（基本）'}<br/>
              ＡＲ：#{division.ar} <p/>
              #{ division.evals_str.gsub("\n","<br/>\n")} <br/>
            </b>
            <b>
              #{common_str}
            </b>
            <b>
              #{action_str}
            </b>
          </p>
          <p>
            <b>
              #{leaders_str}
            </b>
          </p>
          <p>
            <b>
              #{derived_evals_str}
            </b>
         </p>
          <p>
          部隊の可能行為<br/>
          <ul>
            #{division.all_action_names.collect{|item|'<li>'+ item.to_s + "</li>\n" }.join("\n")}
          </ul>"

          if division.non_action_bonus_tqrs.size > 0
            items = division.non_action_bonus_tqrs.collect{|item|'<li>'+ item + "</li>\n" }
            result << "
          部隊が満たすその他７５％制限<br/>
          <ul>
            #{items}
          </ul>"
          end
          result + '</p>'
        end

        if division.formation == :fleet # && division.children.size > 1
          result << "\n#{fleet_evals(division)}\n"
        end

        result << "<b>数式：</b>" + division.expression_evals_str + "<br/>"  if division.expr

        result << "
          </p>
          <p>
          作成：#{Time.new.strftime("%Y/%m/%d %H:%M:%S")}<br />
          太元Version： #{TaigenController::VERSION} <br />
          ｉ言語定義最終更新日時： #{Property[:i_definition_updated_at]}<br />
          編成データダイジェスト値： #{@source_digest} <br/>
          出力結果ダイジェスト値： #{@result_digest} <br/>
          </p>"
        raw result
    }
  end

  def members_tab_id(division)
    "members_tab_#{division.hash}_#{division.context.current_situation.hash}"
  end

  def members_tab(division)
       content_tag(:div , id: members_tab_id(division)) {
        result = members_detail(division)
        default_calc = division.context.default_abilities_evaluator.calculate(division) if division.context.abilities_evaluators.size > 0
        for aev in division.context.abilities_evaluators
          sub_calc = aev.calculate(division)
          next if default_calc == sub_calc
          result += "<br/>"
          content_tag(:p) {
            s_name = division.context.current_situation
            result += link_to_toggle("■（#{aev.particular_situation_name}）",'evals',division.context.current_situation,aev.particular_situation_name)
            aev.particular_situation(division){
              result += <<EOT
<br/>
<div id="evals_#{s_name.hash}_#{aev.particular_situation_name.hash}" ,  style='overflow: visible; display: none;padding-left: 10px;'>
  #{members_detail(division)}
</div>
EOT
            }

          }
        end
        raw result
      }
  end

  def members_detail(division)
    raw <<EOT
            ■部隊内訳：<br />
             #{inspect_children(division)}
             #{rd_table_tag(division)}
             #{rd_table_tag(division.crew_division , '乗員') if division.formation == :fleet}
             #{inspect_unit_sp(division,division)}
EOT
  end

  def actions_tab_id(division)
    "actions_tab_#{division.hash}_#{division.context.current_situation.hash}"
  end
  def actions_tab(division)
    content_tag(:div , id: actions_tab_id(division)) {
      content_tag(:p , actions_tab_content(division) )
    }
  end

  def costs_tab(troop)
    content_tag(:div , '' , :id => 'cost_tab_content')
  end

  def costs_tab_content(troop)
    troop.context.change_to_default_situation(troop)
    troop.change_to_default_situation(troop)

    result = <<EOT
          <p>
            (E-1)#{inspect_event_cost(troop)}
          </p>
          <p>
            (E-2) 編成種別 <br/>
            #{troop.context.variables['編成種別']}
          <p>
            (E-3)#{inspect_sp_cost(troop)}
          </p>
            #{instpect_costs_apply_details(troop) }
          <p>
            (E-5)#{inspect_total_cost(troop)}
          </p>

EOT
    result.html_safe
  end
end
