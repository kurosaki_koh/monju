module InformationHelper
  def latest_information
    Information.find(:first ,:conditions => ["expired = ? " ,false] ,  :order => 'created_at DESC', :limit=>1)
  end
end
