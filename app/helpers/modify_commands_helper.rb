module ModifyCommandsHelper
  
  def dic(model_class , field=nil)
    model_class = model_class.name if model_class.class == Class
    return nil if model_class.nil?
    if field.nil?
      class_name = model_class.underscore.gsub('_',' ')
      _(class_name)
    else
      _(model_class + '|'+field.to_s.humanize)
    end
  end
  
  def diff_humanize(model_class_str  ,hash)
    return '&nbsp' unless hash
    result = []
    for key in model_class_str.constantize.columns.map{|m| m.name}
      next if ['id', 'owner_account_id' , 'updated_at' , 'created_at' , 'position','character_id' , 'event_id'].include?(key) #key == 'id' || key == 'owner_account_id' || key == 'updated_at'
      next unless hash.has_key?(key)
      str = dic(model_class_str , key)+":"
      str += if hash[key].class == Time
         hash[key].strftime('%y/%m/%d %H:%M')
      elsif key =~ /job\d_id/ && hash[key]
        ygg = Job.find(hash[key])
        ygg.nil? ? hash[key].to_s : ygg.name
      else
        hash[key].to_s
      end
      result << str
    end
    result.join(' , ')
  end
  
  def humanize_original(model)
    diff_humanize(model.target_type , model[:original])
  end
  
  def humanize_change(model)
    diff_humanize(model.target_type , model[:change])
  end
end
