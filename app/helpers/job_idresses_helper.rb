# -*- encoding: utf-8 -*-
module JobIdressesHelper
  include IncrementalJobSearch

  def hq_tags(index,bonuses)
    result = "Ｑチェック結果："
    result += select_tag("job_idress[bonuses][#{index}][quality]",
      options_for_select([
          ['通常','0'],['ＨＱ（＋１）','1'],['ＳＨＱ（＋２）','2'],['ＵＨＱ（＋３）','3']
        ],bonuses[index][:quality]) ,
        :onChange => "update_by_quality('#{index.to_s}')"
      )

    hq_targets = Job::Abilities.collect{|c| [Job::AbilityKanji[c],c.to_s]}
    hq_targets << ['その他：','']
    result += " ＨＱ対象："
    result += select_tag("job_idress[bonuses][#{index}][hq_target]",
      options_for_select(hq_targets,bonuses[index][:hq_target].to_s),
        :onChange => "update_by_target('#{index.to_s}')" ,
        :disabled => (bonuses[index][:quality] == '0' )
    )
    result += ' ' + text_field_tag("job_idress[bonuses][#{index}][option]",bonuses[index][:option],
    :disabled => (bonuses[index][:quality] == '0' || bonuses[index][:hq_target] != "" ))
    return raw result
  end
end
