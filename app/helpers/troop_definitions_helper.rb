# -*- encoding: utf-8 -*-

module TroopDefinitionsHelper

  def licences_tag(result , index , type = :pilot)
     type_key = type == :pilot ? :pilot_licences : :copilot_licences
     licences_tags = result.info[type_key][index].collect{|l|
       if l == 'すべて' || result.info[:vehicle_categories].include?(l)
         valid_tag(l)
       else
         l
       end
     }
     raw( licences_tags.join(' , ') )
  end

  def valid_tag(text)
    content_tag( :span , "【#{text}】", style: 'font-weight:bold;color:green')
  end

  def invalid_tag(text)
    content_tag(:span ,  "【#{text}】" , style: 'font-weight:bold;color:red')
  end

  DIRECTIVES = {
    :ConditionDef => {:id => :cond , :name => '＃！有効条件'} ,
    :PartialConditionDef => {:id => :pcond , :name => '＃！部分有効条件'},
    :IdressAppendantDef => {:id => :iad , :name => '＃！個別付与'} ,
    :TroopBonusDef => {:id => :tb , :name => '＃！部隊補正'} ,
    :ResourceModificationHQDef => {:id => :rmhq , :name => '＃！消費削減ＨＱ'} ,
    :SituationDef => {:id => :situation , :name => '＃！状況'} ,
    :ParticularSituationDef => {:id => :psituation , :name => '＃！評価派生'} ,
    :DisableDef => {:id => :disable , :name => '＃！特殊無効'} ,
  }

  DIRECTIVE_DIC = DIRECTIVES.keys.inject({}){|result,key|
    result[ DIRECTIVES[key][:name] ] = key.to_s
    result
  }

  def directive_em_tag(line)
    result =  if line =~ /^((＃|#)(！|!)([^：:]+))(：|:)/ && DIRECTIVE_DIC.has_key?($1)
    <<EOT
<span class="#{ DIRECTIVE_DIC[$1]}" >#{h line}</span>
EOT
    else
      h line
    end
    raw result
  end

  def directives_check_tag(class_name)
    result = if @directives.has_key?(class_name) && DIRECTIVES[class_name]
    <<EOT
<label for="#{DIRECTIVES[class_name][:id]}">
  <input id="#{DIRECTIVES[class_name][:id]}" name="d_type" type="checkbox" value="#{class_name}"  onClick="change_color(this)"/>
  #{DIRECTIVES[class_name][:name]}
</label>
EOT
    else
      nil
    end
    raw result
  end

  def print_diff(div1 , div2 , s_name)
    header = "■#{div1.name}/#{s_name}<br/>\n"
    [div1 , div2].each do | d|
       d.context.change_situation_to(s_name,false)
       d.change_situation_to(s_name)
    end
    diff = Difference.between(div1 , div2)


    result = diff.print.gsub("\n" , '<br/>')
    if result.size > 0
      header + result + "<br/>\n"
    else
      ''
    end
  end

  def print_all_diffs(troop1 , troop2)
    result = ''
    troop1.all_divisions.each { |div1|
      div2 = troop2.all_divisions.find { |div2| div2.name == div1.name }
      next if div2.nil?
      for s_name in div1.all_situation_names
        next unless div2.all_situation_names.find { |name| name = s_name }
        result += print_diff(div1, div2, s_name) + "<br/>\n"
      end
    }
    result
  end


  def topic_path_base
    link_to('T'+@troop_definition.turn.to_s , troop_definitions_path(:turn => @troop_definition.turn)) +
  " > " + link_to(@troop_definition.name , @troop_definition )
  end
end
