# -*- encoding: utf-8 -*-
module SearchHelper 

  include NationsHelper

  def format_result(target)
    case target
    when Character
      link_to target.character_no + ':' + target.name + '＠' + target.nation.name +
        (@results[:characters] ? ':' + target.originator.to_s : '') , target

    when OwnerAccount
      link_to "#{target.number+'：' if target.number}#{target.name}" , target.owner
    end
  end

  def owner_type_string(owner_account)
    case owner_account.owner_type
    when 'Nation'
      case owner_account.owner.belong
      when :republic
        '共和国'
      when :empire
        '帝國'
      when :associated
        '準藩国'
      else
        ''
      end
    when 'Organization'
      _( owner_account.owner.type.to_s )
    else
      ''
    end
  end
end
