module LocalizeHelper
  def localize (group_name, action_name, debug = nil)
    localized = Localized::Message.instance[group_name, action_name]
  rescue Localized::Message::EntryNotFound
    raise
  rescue => err
    ActiveRecord::Base.logger.debug("Localize Error: #{err} in ('#{group_name}'=>'#{action_name}'")
  ensure
    return localized || Inflector::humanize(action_name)
  end

  def localized_data (klass, group_name, attr_name = nil)
    Localized::Model[klass][group_name, attr_name]
  end

  def localized_column_name (klass, column_name)
    localized_data(klass, 'column_names', column_name.to_s) || Inflector::humanize(column_name.to_s)
  end

  def localized_class_name (klass)
    localized_data(klass, 'names', :class) || klass.to_s.humanize
  end

  def localized_instance_name (record_or_class)
    case record_or_class
    when Class
      localized_data(record_or_class, 'names', :instance)
    when ActiveRecord::Base
      Localized::Model[record_or_class].localize_instance(record_or_class)
    when NilClass
      return nil
    else
      raise ArgumentError, "got %s, expected ActiveRecord::Base subclass or its instance" % [record_or_class.class]
    end
  end

  def localized_error_messages_for(object, options = {})
    options = options.symbolize_keys
    object  = instance_variable_get("@#{object}") unless object.is_a?(ActiveRecord::Base)
    if object.nil? or object.errors.empty?
      return nil
    else
      header_tag = options[:header_tag]    || localize(:error_messages_for, :options_header_tag)
      summary    = object.errors.count.to_s + localize(:error_messages_for, :errors_occurred, :debug)
      content_tag("div",
                  content_tag(header_tag, summary) +
                  content_tag("p", localize(:error_messages_for, :there_were_problems)) +
                  content_tag("ul", object.errors.collect { |attr, msg| content_tag("li", "%s%s" %
                    [attr == 'base' ? '' : localized_column_name(object.class, attr), localize(:error_messages, msg)])}),
                  "id" => options[:id] || "errorExplanation", "class" => options[:class] || "errorExplanation"
                  )
    end
  end

  def view_property (singular_name_or_klass, column_name)
    Localized::Model[singular_name_or_klass].view_property(column_name)
  end

  def human_attribute_name (record, column_name)
    klass = record.is_a?(Class) ? record : record.class
    localized_column_name(klass, column_name)
  end

  def human_attribute_value (record, column_name)
    value    = record.send(column_name)
    property = view_property(record.class, column_name)
    property.human_value(value, self)  # self is a Controller or a View
  rescue
    ERB::Util.html_escape(value)
  end

  def human_attribute_edit (singular_name, column_name)
    singular_name = singular_name.to_s
    property      = view_property(singular_name, column_name)
#raise [singular_name, column_name, property].inspect
    case property
    when Localized::ViewProperty
      property.human_edit(singular_name, column_name, self) # self should be a View
    else
      input(singular_name.to_s, column_name)
    end
  end

  def localized_label_class_on (record, column_name)
    if record.errors.on(column_name)
      Localized::Message.instance[:error_messages_for, :label_error_class] || 'labelWithErrors'
    else
      Localized::Message.instance[:error_messages_for, :label_class] || 'label'
    end
  end

  module_function :localize
  module_function :localized_data
  module_function :localized_column_name
  module_function :localized_class_name
  module_function :localized_instance_name
  module_function :view_property
end
