# -*- encoding: utf-8 -*-
module IncrementalJobSearch
  def include_incremental_search_code
  javascript_tag :defer => 'defer ' do <<EOT

var event_source = null;

function update_list(target , value){
  list = $(target)
  reg = new RegExp('^' + value + '.*')
  enabled_line = $A(list.options).find(function(opt){
    return (reg.match(opt.text));
  });

  if (enabled_line) {
    enabled_line.selected = true;
  }
  return enabled_line;
}

function incremental_select(target){
  keyword = $F(target + '_inclemental');
  list_name = 'job_idress_'+target+'_id';
  job = update_list(list_name , keyword);

  if (job){
    update_job_idress_name();
  }
}

function selected_text(target){
  return $(target).options[$(target).selectedIndex].text;
}
function update_job_idress_name(){
  if ($F('job_idress_idress_type_jobidress')=='JobIdress'){
    $('job_idress_combo').update( selected_text('job_idress_race_id') + '+' +
      selected_text('job_idress_job1_id') + '+' + 
      selected_text('job_idress_job2_id') + '+' + 
      selected_text('job_idress_job3_id')
      );
  }else{
    $('job_idress_combo').update( selected_text('job_idress_job4_id') );
  }
}
function update_form(){
    ['race_id' , 'job1_id' , 'job2_id','job3_id'].each(function(name){
        if ($F('job_idress_idress_type_jobidress') == 'JobIdress'){
          $('job_idress_' + name).enable();
        }else{
          $('job_idress_' + name).disable();
        }
      });
    if ($F('job_idress_idress_type_jobidress') == 'JobIdress'){
        $('job_idress_job4_id').disable();
    }else{
        $('job_idress_job4_id').enable();
    }
    update_job_idress_name();
}

function update_regular_no(){
  if ($('job_idress_type_regular').checked ){
    $('job_idress_regular_no').enable();
  }else{
    $('job_idress_regular_no').disable();
  }
}

function incremental_select_all(){
  idress_name = $F('job_idress_combo_increment');
  idress_name = idress_name.replace(/＋/g,'+');
  idresses = idress_name.split('+');
  flag = false;
  race = update_list('job_idress_race_id' , idresses[0]);
  if (race){
    flag = true;
  }
  idresses.shift();

  id_names = ['job1_id' , 'job2_id','job3_id']
  idx = 0

  idresses.each(function(name){
//    alert(id_names[idx] + ':' + name);
    job = update_list('job_idress_' + id_names[idx] , name);
    idx ++;
  });
  if (flag==true) {
    update_job_idress_name();
  }
};
EOT
   end
  end
  
  def incremental_select(target,job_idress ,opts)
    render :partial => 'shared/incremental_select' , :locals => {:target => target , :job_idress => job_idress , :opts => opts}
  end
end
