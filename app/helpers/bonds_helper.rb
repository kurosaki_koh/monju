module BondsHelper

  def aces_select_tag
    select(:bond , :ace_id ,Ace.all(:order => 'hurigana').collect{|p| [ p.name, p.id ] }, { :include_blank => true }).html_safe
  end

end
