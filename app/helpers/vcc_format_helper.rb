# -*- encoding: utf-8 -*-
require_dependency 'i_definition'

module VccFormatHelper
  def hq_format_str(ji)
    result = ''
    for lv in [:hq0,:hq1,:hq2,:hq3]
      if ji.bonuses[lv][:quality] != '0' && ji.bonuses[lv][:hq_target] != ''
        result += '+'
        result += 'Ｓ' if ji.bonuses[lv][:quality] == '2'
        result += 'ＨＱ' + Job::AbilityKanji[ji.bonuses[lv][:hq_target].to_sym]
      end
    end
    return result
  end

  def format_soldier(data ,nation_id) #character_no,name,idresses , personal_idress , belong)
    row = data[:number] + '_' + data[:name] + '：' + data[:job_idress]
    character = Character.find_by_character_no(data[:number])
    if character
      row += format_job_idress_hq(character , nation_id,data[:job_idress])
      row += format_personal_idress(character , data[:personal_idress])
      items = select_items(character)
      mod_str = format_character_mod_str(character,items)
      row += mod_str
      row += ";"
      items_output = format_items(items,character)
      if items_output.blank?
        row += "\n"
      else
        row += "\n" + items_output 
      end
      row
    else
      row += format_job_idress_hq(nil , nation_id,data[:job_idress])
      row += ";\n"
    end
    raw row
  end

  def format_job_idress_hq(character , nation_id , idresses)
    job_idresses = []
    if character
      job_idresses += character.owner_account.job_idresses
      nation = character.nation
    else
      nation = Nation.find(nation_id)
    end

    job_idresses += nation.owner_account.job_idresses
    target = job_idresses.find{|j| j.inspect_idresses == idresses}
    target ? hq_format_str(target) : ''
  end

  def format_personal_idress(character,personal_idress)
    unless personal_idress.blank?
      result = ("+"+personal_idress)
      j4_target = character.acquired_idresses.detect{|ai| ai.name == personal_idress}
      result += hq_format_str(j4_target) if j4_target
    else
      result = ''
    end
    result
  end

  def format_character_mod_str(character,items)
    mod_str = character_mod_str(character,"+","*")
    item_str = format_item_mod_str(character,items)
    unless item_str.blank?
      item_str = '*'  + item_str unless mod_str.blank?
      mod_str += '<span class="item_bonus">' + item_str
    end
    unless mod_str.blank?
      return "："+mod_str
    else
      return ''
    end
  end

  def select_items(character)
    names = character.owner_account.object_registries.owned_objects.select{|obj|
      obj[:number]>0}.collect{|obj|
      obj[:name]
    }
    item_defs = names.collect{|name|IDefinition.find_from_name_and_rev(name , nil)}.compact
    item_defs.select{|item_def|
      def_hash = item_def.compiled_hash
      defs = def_hash['特殊定義']
      !defs.any?{|sp| sp['特殊種別'] == '位置づけ' && sp['位置づけ'].include?('家具')} &&
        defs.any?{|sp|sp['特殊種別'] =~ /補正/ || sp['特殊種別'] == '行為'}
    }
  end

  def format_item_mod_str(character,items = nil)
    items = select_items(character) if items.nil?
    all_sp = items.collect{|item|
      c_hash = item.compiled_hash.dup
      c_hash['Ｌ：名'] = item.name unless c_hash['Ｌ：名']
      c_hash
    }.collect{|idefs|
      hash = idefs['特殊定義']
      hash.each{|sp| sp['Ｌ：名'] = idefs['Ｌ：名']}
      hash
    }.flatten
    bonus_sp = all_sp.select{|sp|sp['特殊種別']=='補正' && sp['補正使用条件'].blank? &&
          sp['補正対象'].all?{|target| Job::AbilityKanji.values.include?(target)}
    }
    result = bonus_sp.collect{|item| item['補正対象'].collect{|target|
        "#{target}#{sprintf("%+d",item['補正'].to_i)}<span class='item_name'>/*#{item['Ｌ：名']}*/</span>"
    }.join('*')}.join('*')
    result = result+'</span>' unless result.blank?
    raw result
  end

  def format_items(items,character)
    items_result = items.collect{|item|
      head = "－#{item.name}：個人所有："
      note_sps = item.compiled_hash['特殊定義'].select{|sp|sp['特殊種別'] =~ /補正/ || sp['特殊種別'] == '行為'}
      note = note_sps.collect{|sp|
        sp['定義'] =~ /　＝　(.+)/
        $1
      }.join(' , ')
      e_point_sp = item.compiled_hash['特殊定義'].find{|sp|sp['特殊種別'] == '着用箇所'}
      e_point = e_point_sp ? e_point_sp['着用箇所'].sub('するもの' , '') : ''
      head + note + e_point
    }.join("\n")
    if items_result.blank?
      return ''
    else
      '<span class="items">' + items_result + "\n－個人取得ＨＱ根拠ＵＲＬ：#{idress_wiki_personal_yggdrasill_page_url(character)}\n</span>"
    end
  end

  def idress_wiki_personal_yggdrasill_page_url(character)
    number = character.character_no.sub(/^\d{2}\-/,'')
    encoded_name = URI.escape(('：'+character.name).toeuc)
    url = 'http://farem.s101.xrea.com/idresswiki/index.php?' + number + encoded_name
    auto_link(url)
  end
end