# -*- encoding: utf-8 -*-
module OwnerAccountsHelper
  IObject # for require Weapon
  def owner_path(account)
    if account.character
      url_for(:controller => :characters , :action => :show , :id => account.character)
    else
      url_for(:controller => :nation , :action => :show , :id => account.nation)
    end
  end
  
  def set_disable_for_facade(flag)
    fields_for_nation = [  # :edit_params_weapons_name ,
      :weapons_number , 
      :weapons_app_type_alter , :weapons_app_type_move , :weapons_move_dest ,
      :weapons_event_no , :weapons_event_name , :weapons_turn , 
      :weapons_note , :weapons_reference_url , :weapons_weapon_id , 
      :caches_event_no , :caches_event_name , :caches_entry_url , 
      :caches_result_url , :caches_reference_url , :caches_fund , 
      :caches_resource , :caches_food , :caches_fuel, 
      :caches_amusement , :caches_num_npc , :caches_note , 
      :caches_turn ,
    ]
#    fields_for_nation.each{|field| page[field].disabled = flag}
    raw fields_for_nation.collect{|field|
      "$('#"+field.to_s + "').attr('disabled' , "+flag.to_s + ");\n"
    }.join
  end
  
  def update_tables_in_facade(owner_account)
    
    if owner_account && owner_account.owner_type == 'Character'
#      page[:weapons_table].hide
      raw "$('#weapons_table').attr('visible' , false);\n"
    else
      #page[:weapons_table].show
      #page[:weapons_table].replace_html :partial => '/weapons/index' , :locals => {:weapons => owner_account.weapon_registries}
      raw <<-EOT
       $("#weapons_table").show();
       $("#weapons_table").html("#{escape_javascript(render partial: '/weapons/index' , locals: {weapons: owner_account.weapon_registries})} ");
      EOT
    end
  end
  def yell_weapon_select_list
    select( :weapons , 'weapon_id' , yell_weapon_list.map{|i| [i[1] , i[0]]} ,{},{ :disabled => true})
  end

  def update_links(owner_account)
    return if owner_account.nil?
    owner_type = owner_account.owner_type
    if owner_type != 'Character'
      page[:link_to_cache_records].replace(page.context.link_to( '[財務表]' , 
        page.context.cash_records_path(owner_account),:turn => Property[:current_turn]  , :target => :cache ,:id=>'link_to_cache_records' ))
      page[:link_to_weapon_list].replace(page.context.link_to( '[兵器一覧]' , 
        page.context.owner_account_weapons_path(owner_account) , :target => :weapon , :id=>'link_to_weapon_list' ))
      page[:link_to_weapon_registry].replace(page.context.link_to( '[兵器履歴]' , 
        page.context.owner_account_weapon_registries_path(owner_account) ,
        :target => :weapon ,:id=>'link_to_weapon_registry' ))
    end
#    raise owner_account.to_yaml
     page[:link_to_mile_changes].replace(page.context.link_to( '[マイル]' , page.context.mile_changes_path(owner_account,:turn => 'last') , :target => :mile ,:id=>'link_to_mile_changes' ))
  end
  
  def context
    page.instance_variable_get("@context").instance_variable_get("@template")
  end

end
