module IpeForHelper
  def ipe_for (object, attribute = nil, collection = nil, value_method = :first, text_method = :last)
    if attribute
      class_name = object.to_s.underscore
      define_method("set_#{class_name}_#{attribute}") do
        klass = object.to_s.classify.constantize
        value = klass.update(params[:id], attribute => params[:value]).send(attribute)

        if collection
          collection = instance_eval(collection.to_s) if collection.is_a?(Symbol)
          value      = collection.find{|master| value == master.send(*value_method)}.send(*text_method) rescue value
        end
        render :text =>value.to_s
      end
    else
      klass = object.to_s.classify.constantize
      klass.content_columns.each do |column|
        collection = LocalizeHelper::view_property(object.to_s, column.name).masters rescue nil
        ipe_for(object, column.name, collection)
      end
    end
  end
end
