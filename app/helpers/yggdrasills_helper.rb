module YggdrasillsHelper

  def tree_node_tag(node ,&view_method)
    result = "<li id=#{node[:id]} #{!node.leaf? ? 'class=\'expanded\'' : ''} >"
    result << (view_method ?
        view_method.call(node) :
        (link_to(node.name , owner_account_yggdrasill_path(@owner_account , node) ) || "")       )
    if !node.leaf?
      result << "\n<ul>\n"
      node.children.each{|n| result << tree_node_tag(n) + "\n"}
      result << "\n</ul>\n"
    end
    result << "</li>"
    result.html_safe
  end
end
