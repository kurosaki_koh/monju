# -*- encoding: utf-8 -*-
require 'delegate'

module TaigenInspector
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::JavaScriptHelper


  module Helper
    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::JavaScriptHelper
    include ApplicationHelper
    include ActionView::Context

    def link_to_toggle(word,link_id_key,*objs)
      link_id = link_id_key
      link_id += '_' + objs.collect{|obj|obj.hash.to_s}.join('_') if objs.size > 0
      link_to_function(word ,"$('##{link_id}').slideToggle();")
#      link_to_function(word ,"new Effect.toggle('#{link_id}' , 'slide' , {duration:0.3})")
    end

    def toggle_div_tag(link_id_key , *objs)
      id_str = [link_id_key , objs.collect{|obj|obj.hash}].flatten.join('_')
      result = "<div id='#{id_str}'  style='overflow: visible; display: none;'>".html_safe
      if block_given?
        result += yield.html_safe
        result += "</div>".html_safe
      end
      result.html_safe
    end
  end

  class EvalInspector

    attr :evalor , true
    attr :rds , true
    attr :child , true
    attr :division , true
    attr_reader :action_name
    attr_reader :calculation
    
    include Helper

    def initialize(ev, child,division , calc = nil)
      init(ev,child,division , calc)
    end

    def init(ev, child,division,calc = nil)
      @evalor = ev
      @action_name = @evalor.action_name
      @rds = {}
      @child = child
      @division = division
      @filtered_sp = nil
      @calculation = calc || @evalor.calculate((child || division))
      @is_simple = @evalor.is_standard && division.no_sp?(@evalor)
    end

    def evaluator
      @evalor
    end

    def is_simple?
      @is_simple
    end

    def filtered_sp
      @filtered_sp ||= if !@is_simple && @evalor.bonus_filter
        @filtered_sp = child.sub_evals_items(@evalor)
      else
        @filtered_sp = []
      end
    end

    def header
      content_tag :span , "#{ (@child ? @child.name : @division.name)}：#{  @evalor.name}："
    end

    def inspect_arg(arg)
      value = 0
      args_txt = ''
      bonus = 0
      count = 0
      calc = @calculation.arguments[arg]
      if calc.nil? || calc.is_na?
        return " #{arg}:N/A"
      end
      
      if arg
        value = calc.base_value
        args_txt = " #{arg}:#{value}"
      end

      bonuses = @calculation.arguments[arg].operands_no_dup
      
      if bonuses.size > 0
        for sp in bonuses
          if sp.is_string?
            bonus = sp.bonus
            break
          end
          if sp.value(arg)
            args_txt += "+#{sp.bonus}"
            bonus += sp.bonus
            count += 1
          end
        end
      end

      #合計値
      if arg
        if bonus.class == String
          args_txt = bonus
        else
          rd = @calculation.arguments[arg].rd
          args_txt += "=#{value + bonus}" if count > 0 && rd != 0
          unless @is_simple
            args_txt += "(RD:#{rd}) "
          end
        end
      end

      args_txt
    end

    def inspect_spec_defs
      operands = @calculation.arguments.values.inject(Set.new) do |set , value|
        set |= value.operands_no_dup
      end

      if operands.size > 0

        link_to_toggle_part('補正内訳' , 'bonus_specs' , self){
          operands.collect{|sp|
            content_tag(:span){
              if sp.class == EvalData
                '　　('+ sp.name.html_safe + ')'
              else
                (sp.owner != sp.target ? sp.owner.name : '' ) + sp.source
              end
            } << tag( :br)
          }.push(tag :br).inject{|a ,b| a << b}
        }
      else
        ''
      end

    end

    def inspect_eval
      result = header()
      if @evalor.args.size > 0 && !@evalor.is_action_bonus?
        arg_inspections = @calculation.arg_names.collect{|arg| inspect_arg(arg)}
        if arg_inspections[0] =~ /自動/o
          result << arg_inspections[0]
        else
          result << arg_inspections.join
        end
      else
        result << inspect_arg(nil)
      end
      result << tag(:br)
      result << inspect_spec_defs if @child && @evalor.are_applicable_between(@child)
      result
    end

    def build_inspection
      inspect_eval
    end
  end

  class InspectorArray < DelegateClass(Array)
    attr :inspectors , false
    attr :details , false
    attr :evaluator , false
    attr :unit , false
    attr_reader :calculation

    include Helper
    
    def initialize(unit,evaluator,inspectors , details = nil)
      super(inspectors)
      @inspectors = inspectors
      @details = details
      @unit = unit
      @evaluator = evaluator
      @calculation = details || evaluator.calculate(unit)
      @operands = nil
    end

    def operands(key)
      
    end

    #メンバ毎の内訳表示
    def inspect_members
      self.compact.collect{|i| i.build_inspection}.join
    end

    def build_inspection
      result = ''
      result << "・#{@unit.name}：<div style='margin-left:2em'>\n"
      result << actions_details_text
      result << "</div>\n"
      result.html_safe
    end

    def calcurate_rd
      if @evaluator.class == SynchronizeEvaluator
        rds = [self[0].rds]
      else
        rds = self.collect{|i|i.rds}
      end

      #RD合算処理
      sum_rd = Hash.new(0)
      self[0].evaluator.args(@unit){|a| sum_rd[a] = 0}
      rds.each{|rd_hash|
        rd_hash.each{|key,value|
          sum_rd[key] ||= 0
          sum_rd[key] += value
        }
      }
      @sum_rd = sum_rd
    end

    #引数ごとのRD合計値＞評価変換表示
    def inspect_parameter_line
      return '' if @calculation.is_na?
      @calculation.arg_names.collect{|k|
        v = @calculation.rd_total(k)
        part_str = ''
        rd_part = v ? "RD:#{v}" : ''
        part_str += "#{k} #{rd_part} 評価#{@calculation.arguments[k].base_value}"
        @calculation.arguments[k].operands_no_dup.each do |e|
          part_str += e.to_s
        end
        part_str
      }.join(' , ')
    end

    #引数平均および部隊補正加算の表示
    def inspect_total_line
      result = ''
      args = @calculation.evaluator.args

      argsize = args.size
      if argsize > 0 && !@calculation.is_na?
        calcs = args.collect{|arg|@calculation.arguments[arg]}
        argument_totals = calcs.select{|calc|!calc.is_na?}.collect{|calc|calc.total}
        if argument_totals.size > 1
          result_calc_str = "<br/>\n("
          result_calc_str += argument_totals.collect{|arg|arg.to_s}.join('+')
          result_calc_str += ')'
          result_calc_str += "/#{argsize}"
          result_calc_str += "=#{@calculation.arguments_avg }"
          result << " #{result_calc_str}"
        end
        arg_avg = @calculation.arguments_avg
        class_name = @calculation.evaluator.class_name
        if @calculation.arg_names.include?(class_name)
          val = @calculation.arguments[class_name].total
          if val
            if val.class == String
              result += "#{val}<br/>\n"
            else
              if arg_avg && arg_avg.class != String
                avg_rd = to_rd(@calculation.arguments_avg)
                result += "(RD:#{avg_rd}) , "
              else
                avg_rd = 0
              end
              class_rd = to_rd(val)
              result += sprintf("%d(RD:%.1f)<br/>\n",val,class_rd)
              result += sprintf("(%.1f + %.1f)=%.1f 評価:%d",avg_rd , class_rd ,rd_sum =  avg_rd + class_rd , to_eval(rd_sum))
            end
          end
        end
        troop_bonus_elements = @calculation.operands_no_dup
        
        auto = troop_bonus_elements.find{|tb|tb.bonus}
        sp_bonus = auto ? auto.bonus : troop_bonus_elements.collect{|tb|tb.bonus}.sum

        result << "<br/>\n結果：#{@calculation.base_value}"
        if sp_bonus.class == String || troop_bonus_elements.size > 0
          troop_bonus_sp =  troop_bonus_elements.collect{|tb| ERB::Util.h(tb.to_s) }
          result << troop_bonus_sp.join
          result << " = #{@calculation.total }"
        end
        result << "<br/>\n"
      end
      result
    end
    
    def inspect_distinct_operands
      return '' if @calculation.class == Calculation
      operands = @calculation.distinct_operands
      return '' if operands.size == 0
      
      link_to_toggle_part('補正一覧' , 'operands' , self){
        content_tag(:ul){operands.collect{|op| '<li>' + op.source + "</li>\n"}.join.html_safe }
      }
    end

    def actions_details_text
#      result = ''
      result = self.inspect_members

      if  (!@evaluator.is_individualy? && !@evaluator.is_action_bonus?) || @calculation.class == SynchronizeCalculation
        #評価値への変換
        result += self.inspect_parameter_line  unless @evaluator.is_simple_calcuration?(@unit) 
        result += self.inspect_total_line
        result += self.inspect_distinct_operands
      end
      result << tag(:br) + "\n"
      result.html_safe
    end

  end

  class CalculationSetInspector < InspectorArray
    #メンバ毎の内訳表示
    def inspect_members
      self.compact.collect{|i|
        result =  i.build_inspection
        if i.calculation.class == Calculation
          result += "結果：#{i.calculation.base_value}"
          if i.calculation.operands_no_dup.size > 0
            result += i.calculation.operands_no_dup.collect{|sp|"#{sp.to_s}"}.join(' + ')
            result += " = #{i.calculation.total}"
          end
        end
        result << tag(:br) + "\n"
        result
      }.join
    end

  end

  class ExpressionInspector
    def initialize(calc)
      @calculation = calc
    end
    def actions_details_text
      if @calculation.result
        result = @calculation.to_s.gsub(/\n/ , "<br/>\n")
        result << "<br/>\n"
        result
      elsif @calculation.error
        @calculation.error.to_s
      else
        ''
      end
    end
  end
end