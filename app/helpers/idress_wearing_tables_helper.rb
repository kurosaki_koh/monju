# -*- encoding: utf-8 -*-
module IdressWearingTablesHelper
  include VccFormatHelper
  def idress_wearing_table_path(iwt)
    nation_idress_wearing_table_path(params[:nation_id],iwt)
  end
  def idress_wearing_tables_path
    nation_idress_wearing_tables_path(params[:nation_id])
  end
  def find_all_originator_restriction(wearing)
    jobs = wearing[:job_idress].gsub('+','＋').split('＋')
    jobs <<  wearing[:personal_idress] unless wearing[:personal_idress].blank?
    @notice_hash ||= {}
    @idef_hash ||= {}
    wearing[:number].strip =~ /\d{2}-(.{5}\-.{2})/

    jobs = wearing[:job_idress].gsub('+','＋').split('＋')
    jobs <<  wearing[:personal_idress] unless wearing[:personal_idress].blank?
    limits = []
    for name in jobs
      next if @notice_hash.has_key? name
      idef = @idef_hash[name] ||= IDefinition.find(:first , :conditions => ["name = ? and revision is NULL" , name])
      if idef.nil?
        @notice_hash[name]="文殊に職業アイドレス定義'#{name}'の登録がありません。（管理人にご連絡下さい）"
        next
      end
      if idef.compiled_hash.nil? || idef.compiled_hash['エラー']
        @notice_hash[name]="文殊の職業アイドレス定義'#{name}'の登録がなんだかおかしいです。（管理人にご連絡下さい……）"
        next
      end
      originator_limit =  idef.compiled_hash['特殊定義'].find{|sp|
        sp['特殊種別']=='使用制限' &&  sp['使用制限'] =~ /根源力/
      }
      limits << originator_limit['定義'] if originator_limit
    end
    limits
  end
end
