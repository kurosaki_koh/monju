# -*- encoding: utf-8 -*-
# Methods added to this helper will be available to all templates in the application.
#require 'yggdrasill.rb'
module ApplicationHelper
  AbilityNames = {"phy" => "体格", "str" => "筋力", "dur" => "耐久力", "chr" => "外見", "agi" => "敏捷",
                  "dex" => "器用", "sen" => "感覚", "edu" => "知識", "luc" => "幸運"}

  def character_mod_str(character, plus = "＋", separator=" ")
    result = ""
    for col in ['phy', "str", "dur", "chr", "agi", "dex", "sen", "edu", "luc"]
      val = character.attributes[col]
      if val
        result += sprintf("%s%s%d%s", AbilityNames[col], plus, val, separator)
      end
    end
    result.chop
    #    character.attributes.inspect
  end


  def job_idress_select_list(nation_id=nil)
    cond = nation_id ? "nation_id = #{nation_id}" : ""
    @result = JobIdress.all(:conditions => cond).collect { |job| [job.inspect_idresses, job.id] }
    @result
  end

  def personal_idress_list
    p_idresses = Job.all(:conditions => "id < 100").sort do |a, b|
      a.id <=> b.id
    end
    list = [["なし", nil]] + p_idresses.collect { |pi| [pi[:name], pi[:id]] }
    list
  end

  def prepare_ids #(params)
    case
      when params[:nation_id]
        @nation_id = params[:nation_id]
      when @character
        #      @character = Character.find(params[:character])
        @nation_id = @character.nation_id
    end
    if params[:owner_account_id]
      @owner_account = OwnerAccount.find(params[:owner_account_id]) unless @owner_account
      case @owner_account.owner_type
        when 'Nation'
          @nation_id = @owner_account.owner.id
          @character = nil
        when 'Character'
          @character = @owner_account.owner
          @nation_id = @owner_account.owner.nation_id
      end
    end
    if controller.class == OwnerAccountsController && params[:id]
      case @owner_account.owner_type
        when 'Nation'
          @nation_id = @owner_account.owner.id
        when 'Character'
          @character = @owner_account.owner
          @nation_id = @owner_account.owner.nation.id
      end
    end
    return @nation_id, @character
  end

  def current_user_name
    current_user != :false ? current_user.login : ""
  end

  def jst(d = Time.now)
    d.utc + (3600 * 9)
  end

  def is_admin?
    current_user != :false && current_user.role == "admin"
  end

  def link_as_return_to(owner_account)
    case owner_account.owner
      when Organization
        link_to '[ 組織一覧に戻る ]', associated_nations_path
      else
        link_to '[ 戻る ]', owner_account.owner
    end
  end

  def lf2br(str)
    return '' unless str
    str.gsub("\n", tag(:br) + "\n")
  end

  def url2link(str, name=nil)
    return '' if str.nil?
    uris = URI.extract(str, ['http', 'ttp'])
    raw uris.inject(str.dup) { |result, uri|
          uri_target = uri =~ /^ttp\:/ ? uri.sub(/^ttp\:/, 'http:') : uri
          uri = p_to_nisetre(uri)
          replace_str = link_to(name ? name : uri, uri_target)
          result.gsub!(uri, replace_str)
        }
  end

  # R.I.P. od-san
  def p_to_nisetre(url)
    if url =~ /p\.ag\.etr\.ac\/cwtg\.jp\/(.+?)\/(\d+)/
      url = "http://cwtg.jp/#{$1}/nisetre.cgi?no=#{$2}"
    end
    url
  end

  def url2link_for_wiki(str, name='[参照]')
    return '' if str.nil?
    uris = URI.extract(str, ['http', 'ttp'])

    results = []
    note = str.dup
    for uri in uris
      uri = p_to_nisetre(uri)
      index = note =~ /#{Regexp.escape(uri)}/
      if index
        result = note[0...index]
        result = name if result.nil? || result.empty?
        results << "&link(#{result}){#{uri}}"
        note = note[index..-1]
        note.sub!(uri, '')
      end
    end
    results << note unless note.nil? || note.empty?
    results.join('&br()')
  end

  def to_note_str(str, name=nil)
    return '' if str.nil?
    raw lf2br(url2link(str, name))
  end

  def move_button(dir)
    case dir
      when :up
        button_to_function '▲', 'call_move_row("up");', :id => 'up_button', :disabled => 'true', :class => 'up_button'
      when :down
        button_to_function '▼', 'call_move_row("down");', :id => 'down_button', :disabled => 'true', :class => 'down_button'
    end
  end

  def turn_link(turn=params[:turn])
    current_turn = Property['current_turn'].to_i
    if turn
      t = turn.to_i
    else
      t= current_turn
    end
    result = ''
    for i in ([t-2, 1].max)..([t+2, current_turn].min)
      result += if !turn.nil? && i == t
                  "<span style='font-weight:bold'> #{i} </span>"
                else
                  ' ' + link_to(" #{i} ", :turn => i) + ' '
                end
    end
    result += ' ' + (turn.nil? && params[:name].nil? ? '全て' : link_to('全て'))
    result += tag(:br)
    raw result
  end

  def offset_link(count, turn, limit = 100)
    index = 1
    result = ''
    limit = 100 if limit.nil?
    while index < count
      max = [index + limit - 1, count].min
      turn_string = sprintf("%d-%d", index, max)
      result += if (params[:offset].nil? || params[:offset].to_i != (index -1))
                  link_to(turn_string, :offset => index-1, :limit => limit, :turn => turn)
                else
                  "<span style='font-weight:bold'> #{turn_string} </span>"
                end + ' '
      index = max + 1
    end

    result += if (!params[:offset].nil? && params[:offset].to_i != (count - limit) || params[:limit] == 'all')
                link_to("末尾#{limit}件", :limit => limit, :turn => turn, :offset => count - limit)
              else
                "<span style='font-weight:bold'> 末尾#{limit}件 </span> "
              end + ' '

    result += if params[:limit] != 'all'
                link_to("T#{turn}全て", :limit => 'all', :turn => turn)
              else
                "<span style='font-weight:bold'> T#{turn}全て </span> "
              end
    raw result
  end

  def has_role?(*roles)
    controller.has_role?(*roles)
  end

  def idress_wiki_url(name)
    link_to name, 'http://farem.s101.xrea.com/idresswiki/index.php?'+URI.escape(name.toeuc)
  end

  def make_route_link(owner_account)
    result = []
    case owner_account.owner
      when Character
        character = owner_account.owner
        result << link_to(character.nation.name, character.nation)
        result << link_to('国民一覧', nation_characters_path(character.nation))
        result << link_to(character.character_no + ':' + character.name, character)
      when Ace
        result << link_to('ACE管理', aces_path)
        result << link_to(owner_account.owner.name, owner_account.owner)
      else
        return ' > ' + link_to(owner_account.owner.name, owner_account.owner)
    end
    return ' > ' + result.join(' > ')
  end

  def options_for_owned_idress_by_nation(nation, race_type)
    [['普通の人', 0]]+JobIdress::select_list(nation.owner_account, race_type)
  end

  def options_for_owned_idress(owner_account)
    return [] if owner_account.nil?
    owner_account.job_idresses.select { |ji| ji.idress_type == 'JobIdress' }.map { |ji| ["#{ji.inspect_idresses}#{'(個人)' if owner_account.owner_type == 'Character'}", ji.id] }
  end

  def options_for_acquired_idress(character)
    [['なし', nil]] + character.acquired_idresses.map { |ai| [ai.name + "(#{ai.owner_account.owner.name})", ai.id] }
  end


  def idress_wiki_definition_url(name)
    'http://farem.s101.xrea.com/idresswiki/index.php?'+URI.escape(name.toeuc)
  end

  def zukan_url(name)
    name = name.name if name.class <= ObjectDefinition
    'http://www35.atwiki.jp/marsdaybreaker/?page=' + URI.escape(name)
  end

  def paginate_links_by_week(klass, where, args)
    records = klass.find(:all, :conditions => where)
    hash = {}
    records.each { |rec|
      beginning = rec.created_at.to_date.beginning_of_week
      end_of_week = beginning.next_week - 1
      key = sprintf("%02d/%s ～ %02d/%s", beginning.year % 100, beginning.strftime('%m/%d'), end_of_week.year % 100, end_of_week.strftime('%m/%d'))
      hash[key] = {:year => beginning.cwyear, :cweek => beginning.cweek}
    }
    result = []
    for key in hash.keys.sort
      result << link_to(key, args.update(hash[key]))
    end
    result << where

  end

  #部分一致による入力候補選択
  #select_id : 対象カラム名(foo_id)
  #select_tag : select タグの内容
  #container_id : 選択候補コンポーネントのコンテナに使われるid属性
  #input_id : キーワード入力部に使われるid属性
  #suggest_id : 選択候補リストに使われるid属性
  #
  #もしくは
  #selection_suggest_input_tag(:name => 'foo' , select_tag) : この場合、上から順に'foo_select_id' , (select_tag), 'foo_cointainer','foo_input','foo_suggestion'とする
  #もしくは
  #selection_suggest_input_tag(nil,select_tag):この場合、'suggestion_select_id' , 'suggestion_container' , 'keyword_input' , 'suggestion'となる。
  def selection_suggest_input_tag(select_id, select_tag, container_id=nil, input_id=nil, suggest_id=nil)
    if select_id.class == Hash
      if !select_id[:name].blank?
        name = select_id[:name].to_s
        select_id = name
        container_id = name + '_container'
        input_id = name + '_input'
        suggest_id = name + '_suggestion'
      else
        select_id = nil
      end
    end

    if select_id.nil?
      select_id = 'suggestion_select_id'
      container_id = 'suggestion_container'
      input_id = 'keyword_input_id'
      suggest_id = 'suggestion'
    end

    tags = <<-"EOT"
      <div id="#{container_id}" class="suggest-container" style="display: inline;">
        #{select_tag}<div id="#{suggest_id}" class="suggest"></div>
        <input id="#{input_id}" type="text" autocomplete="off" value="" onfocus="this.select();" />
      </div>
    EOT
    tags += javascript_tag <<-"EOT"
      (function () { 

        var opts = $A($('#{select_id}').childNodes).map(
          function (e, i) {
            return e.innerHTML;
          }
        ).compact();

        function afterCompleted(completedValue) {
          $A($('#{select_id}').childNodes).find(
            function (e, i) {
              if(e.innerHTML == completedValue) {
                return true;
              }
              return false;
            }
          ).selected = true;
          $('#{select_id}').focus();
        }

        function startSuggest() {

          var width = $('#{select_id}').getWidth() + 'px';
          var height = $('#{select_id}').getHeight() + 'px';
          $('#{suggest_id}').style.width = width;
          $('#{suggest_id}').style.left  = width;
          $('#{suggest_id}').style.top   = height;

          new Suggest.LocalWithAfterHook(
          // new Suggest.Local(
            "#{input_id}",    // 入力のエレメントID
            "#{suggest_id}",  // 補完候補を表示するエリアのID
            opts,             // 補完候補の検索対象となる配列
            {                 // オプション
              dispMax: 50,
              interval: 250,
              hookAfterComplete: afterCompleted
            }
          );
        }

        window.addEventListener ?
          window.addEventListener('load', startSuggest, false) :
          window.attachEvent('onload', startSuggest);
      })();
    EOT
    tags
  end

  def negative_warned_tag(number, positive_color = 'black', negative_color = 'red')
    content_tag(:span, number, :style => "color:  #{number.to_i >= 0 ? positive_color : negative_color}")
  end

  def link_to_toggle_part(word, link_id_key, *objs)
    link_id = link_id_key
    link_id += '_' + objs.collect { |obj| obj.hash.to_s }.join('_') if objs.size > 0

    link_to_function(word, "$('##{link_id}').slideToggle('fast');") <<
        tag(:br) <<
        content_tag(:div, :id => link_id, :style => 'overflow: visible; display: none;padding-left: 10px') { yield } << "\n"
  end

  def source_link_to(command_resource)
    case command_resource.source_type
      when 'ConvertSource'
        link_to 'コンバート：' + command_resource.source.name,
                owner_account_unit_registry_path(command_resource.owner_account, command_resource.source)
      when 'IncludeRegistry'
        link_to 'インクルード：' + command_resource.source.name,
                owner_account_unit_registry_path(command_resource.owner_account, command_resource.source)
      when 'Yggdrasill'
        link_to 'イグドラシル：' + command_resource.source.name ,
            owner_account_yggdrasill_path(command_resource.owner_account, command_resource.source) , title: command_resource.source.definition
      when nil
        link_to 'コマンドリソース(単体）', owner_account_command_resource_path(command_resource.owner_account, command_resource)

    end
  end

end
