module ObjectDefinitionsHelper
  include NationsHelper
  
  def sort_by_nations_and_aces(definition)
   regs = definition.object_registries 
   owners = regs.group_by(&:owner_account)
   nations = owners.keys.group_by(&:nation)
   nations_keys = nations.keys.sort_by{|nation| nation.nil? ? 99999 : nation.id}
   aces = nations_keys.find_all{|n|n === Ace}.sort_by{|a|a.hurigana}
   if aces.size > 0
     nations_keys = nations_keys.delete_if{|n| n  === Ace}.sort_by(&:id)
     nations_keys << aces[0]
     nations[aces[0]]=aces.sort_by(&:hurigana).map{|a|a.owner_account}
   end
   return nations , nations_keys , owners
  end
end
