# -*- encoding: utf-8 -*-

module NationsHelper
  class Helper
    include Singleton
    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::FormHelper 
  end
  include VccFormatHelper
  def help
    Helper.instance
  end

  def kind_of_table(table)
    case controller.auto_detect(table)
    when :used_results
      return "転用リザルト"
    when :results
      return "イベントリザルト"
    else
      return "不明"
    end
  end
  
  def checksum(table,ch)
    result = "<span style='color: green'>解析による合計："
    case controller.auto_detect(table)
    when :used_results
       result += "初期根源力計(#{ch.sum_initials})+リザルト計(#{ch.sum_used_results})=#{ch.sum_initials + ch.sum_used_results}</span>"
       return result
    when :results
      result += "根源力計(#{ch.sum_results})</span>"
      return result
    else
      return ""
    end
  end
  
  def characters_select_tag(tag_name , nation_id,include_null_flag = true)
    characters = Character.find(:all,:conditions => ["nation_id = ?",nation_id],:order => "character_no")
    select_table = characters.collect{|c| ["#{c.character_no}:#{c.name}" , c.character_no] }
    select_table =  [["該当なし","NULL"]] + select_table if include_null_flag
    select_tag tag_name , options_for_select(select_table)
  end

  def originator_str(ch)
    result = ch.sum_proper_originator.to_s
    pi = ch.personal_idress
    result += "(+#{pi.job4.originator})" if  pi && (pi.job4.originator > 0)
    result
  end


  def ellipsify(link_fmt ,  registries,link_id )
    return '' if registries.nil?
    objs = registries[0].owner_account if registries[0]
    result =  ''

    if registries.size >= 3
      ellipsified = registries[0..(registries.size - 3)]
      link_str = sprintf(link_fmt,ellipsified.collect{|r|r.number.to_i}.sum.abs)
      result << link_to_toggle_part(link_str , link_id , objs) {
        raw registry_notes(ellipsified)
      }
    end
    raw result
  end

  def registry_notes(registries )
    raw registries.collect{|r|
      result = controller.has_role?(:item_officer) ? link_to('[編]',edit_owner_account_object_registry_path(r.owner_account , r)) : ''
      result += raw lf2br(url2link(r.note,'[参照]'))
      result
    }.join(tag 'br')
  end

  def ellipsified_registry(registries)
    if registries.size >= 3
      registries[0..(registries.size - 3)]
    else
      nil
    end
  end

  def left_registry(registries)
    if registries.size >= 3
      registries[-2..-1]
    else
      registries
    end
  end

  def select_got_items(registries)
    registries.select{|r| r.number.to_i > 0}.sort_by{|r|
      r.note =~ /^アイドレス開始/o ? '00' : r.note
    }
  end

  def select_used_items(registries)
    registries.select{|r| r.number.to_i <= 0}.sort_by{|r|
      r.note =~ /^アイドレス開始/o ? '00' : r.note
    }
  end

  def got_items(registries)
    items = select_got_items(registries)
    result = ellipsify("[ここまでの入手合計：%d]" , items , 'got_items')
    result << registry_notes(left_registry(items))
    raw result
  end

  def used_items(registries)
    items = select_used_items(registries)
    result = ''
    result << ellipsify("[ここまでの消費合計：%d]" ,items , 'used_items')
    result << registry_notes(left_registry(items))
    raw result
  end

  def items_for_wiki(items,ellipsed_fmt)
    result = []
    e_items = ellipsified_registry(items)
    if e_items
      oo_url = owner_account_object_registries_url(e_items[0].owner_account)
      link_str = sprintf(ellipsed_fmt , e_items.collect{|r|r.number.to_i}.sum.abs)
      
      result << "&link(#{link_str}){#{oo_url}}"
    end
    result += left_registry(items).collect{|r|url2link_for_wiki(r.note).gsub(/(\n|\r)/,'')}
    result.join("&br()")
  end
  
  def got_items_for_wiki(registries)
    items = select_got_items(registries)
    items_for_wiki(items,'[ここまでの入手合計：%d]')
  end
  
  def used_items_for_wiki(registries)
    items = select_used_items(registries)
    items_for_wiki(items,'[ここまでの消費合計：%d]')
  end

  def links(model)
    at = CashRecord.arel_table
    cols = [ at[:fund].sum.as('fund') , at[:resource].sum.as('resource') , at[:food].sum.as('food') , at[:hqfuel].sum.as('hqfuel') , at[:amusement].sum.as('amusement')]
    sums = CashRecord.where(owner_account_id: model.owner_account).select(cols).first

    return raw <<-EOT
        <tr>
          <td> #{h model.name}</td>
          <td align=right> #{ sums[:fund].to_i } </td>
          <td align=right> #{ sums[ :resource].to_i } </td>
          <td align=right> #{ sums[:food ].to_i } </td>
          <td align=right> #{ sums[ :fuel].to_i  } </td>
          <td align=right> #{ sums[ :hqfuel].to_i  } </td>
          <td align=right> #{ sums[ :amusement].to_i } </td>
          <td align=right> #{ model.owner_account.miles.to_i } </td>
          <td> #{ link_to '[財務表]', owner_account_cash_records_path(model.owner_account)}</td>
          <td> #{ link_to '[マイル口座]' , owner_account_mile_changes_path(model.owner_account,:turn => 'last') } </td>
          <td> #{ link_to '[保有兵器表]' , weapons_path(model.owner_account) }</td>
          <td> #{ link_to '[兵器登録履歴]' , owner_account_weapon_registries_path(model.owner_account) , :action => :list , :nation_id => @nation } </td>
          <td> #{ link_to '[アイテム登録履歴]' , owner_account_object_registries_path(model.owner_account) , :action => :list , :nation_id => @nation } </td>
          <td> #{ link_to '[編集履歴]' , owner_account_modify_commands_path(model.owner_account) } </td>
        </tr>
    EOT
  end

  def table_header
    raw "<tr><th width=250> 名称 </th> <th> 資金 </th><th> 資源 </th><th> 食料 </th><th> 燃料 </th><th> 高性能燃料 </th><th> 生物資源 </th><th> マイル </th></tr>"
  end
end
