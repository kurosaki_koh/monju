module CharactersHelper

  def originator_disp(ch)
    result = h ch.sum_originator 
    if ch.personal_idress && ch.personal_idress.originator.to_i > 0 
      result += sprintf("( %d + %d )",ch.sum_initials + ch.sum_used_results + ch.sum_results , ch.personal_idress.originator.to_i)
    end
    result
  end

end
