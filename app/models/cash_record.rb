class CashRecord < ActiveRecord::Base
  attr_accessible :event_no , :event_name , :entry_url , :result_url , :reference_url , :fund ,
                  :resource , :food , :fuel , :amusement , :num_npc , :num_id , :note , :turn ,
                  :position , :nation_id , :owner_account  , :hqfuel

  has_many :cash_record_details , :dependent => :destroy
  has_many :modify_commands , :as => :target
  belongs_to :owner_account
  acts_as_list :scope => :owner_account_id
  
  validates_presence_of :event_name

  after_save :update_latest_cash_record
  after_destroy :update_latest_cash_record

  def update_latest_cash_record
    if self.owner_account.owner_type == 'Nation'
      self.owner_account.owner.latest_cash_record.update_values if self.owner_account.owner.latest_cash_record
    end
  end
  
  def CashRecord.settlement_by_turn(owner_account_id , target_turn)
    cols = [:fund , :resource , :food , :fuel , :hqfuel , :amusement , :num_npc ]
    sql = cols.map{|c| "sum(#{c}) as #{c} "}.join(',')
    sql = "select " + sql + " from cash_records where turn < ? and owner_account_id = ?"
    CashRecord.find_by_sql([sql,  target_turn,owner_account_id ])[0]
  end
end
