# -*- encoding: utf-8 -*-
class ObjectRegistry < ActiveRecord::Base
  attr_accessible :name , :number , :note , :position , :hq , :owner_account_id , :object_definition_id

  belongs_to :owner_account
  belongs_to :object_definition

  acts_as_list :scope => :owner_account_id
  
  before_save :update_definition_ref
  before_validation :normalize_colon

  validates_associated :owner_account

  def update_definition_ref
    self.object_definition = ObjectDefinition.find_by_name(self.name.strip)
  end
  
  def normalize_colon
    self.hq.gsub!('：',':') if hq
    self.hq.gsub!(/:\s*/,': ') if hq
  end
  
  def validate
    begin
      result = hq_hash
      if result != nil && result.class != Hash
        errors.add(:hq,'HQの書式が正しくありません。”修正対象：修正値”の形で各行に列記してください。'+result.inspect)
      end
    rescue
      errors.add(:hq, 'HQの書式が正しくありません。”修正対象：修正値”の形で各行に列記してください。'+result.inspect)
    end
  end
  
  def hq_hash
    return nil if hq.nil?
#    YAML.load('{' + self.hq + '}')
    result = YAML.load(self.hq.strip )
    result = nil if result == false
    result
  end


  private
  def num_of_current
    self.owner_account.num_of_property(self.name)
  end

  around_save do |record , block|
    before_flag = num_of_current == 0
    block.call
    record.owner_account.reload
    after_flag = num_of_current == 0

    if (before_flag != after_flag)
      if  (ygg = record.owner_account.yggdrasills.find_by_name(record.name))
        ygg.rebuild_command_resources
      elsif (crd = CommandResourceDefinition.find_by_name(record.name))
        if (cr = record.owner_account.command_resources.where('command_resource_definitions.name = ?' , record.name ).first )
          cr.available = (num_of_current > 0 )
          cr.save
        end
      end
    end
    true
  end
end
