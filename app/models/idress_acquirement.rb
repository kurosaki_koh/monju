class IdressAcquirement < ActiveRecord::Base
  attr_accessible :character_id , :job_idress_id , :note

  belongs_to :character
  belongs_to :acquired_idress , 
             :class_name => 'JobIdress' , 
             :foreign_key => :job_idress_id
end
