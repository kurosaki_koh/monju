# -*- encoding: utf-8 -*-
require_dependency 'jiken/database/i_definition'

class IDefinition
  attr_accessible :name , :object_type , :definition , :compiled_hash , :revision


  def convert_to_record(record , node)
      record.name = self.name
      evaluation = node.evaluation
      for key in Job::AbilityKanji.keys
        record[key] = evaluation[Job::AbilityKanji[key]]
      end
      record.definition = definition  if record.respond_to?(:definition=)
      record.note = definition
      record
  end
  def to_record
    node = lib.node
    case node.object_type
    when /職業/
      record = Job.find_by_name(self.name)
      record = Job.new unless record
    when /(種族|人)$/
      record = Race.find_by_name(self.name)
      record = Race.new unless record
    else
      return nil
    end
    record = convert_to_record(record,node)
    return record
  end
  
  def IDefinition.find_from_name_and_rev(name , rev)
    if rev.nil?
      self.find(:first , :conditions => ["name = ? and revision is null" , name])
    else
      self.find(:first , :conditions => ["name = ? and revision = turn" , name , rev])
    end
  end

  def is_idress3?
    false
  end

  def derivable?
    true
  end

  def is_command_resource?
    false
  end

  def countable?
    !!(lib.node.object_type =~ /(オプション|騎跨装備|兵器|乗り物|ウォードレス|艦船|アイテム)$/)
  end

end