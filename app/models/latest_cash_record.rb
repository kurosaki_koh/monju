class LatestCashRecord < ActiveRecord::Base
  attr_accessible :url , :fund , :resource , :food , :fuel , :hqfuel , :entertainment
  belongs_to :nation , :foreign_key => :id

  scope :republic , ->{order(:id).joins(:nation).where("belong = ?" , :republic.to_yaml)}
  scope :empire , ->{order(:id).joins(:nation).where("belong = ?" , :empire.to_yaml)}

  def update_values
    acc = self.nation.owner_account
    for col in [:fund,:resource,:food,:fuel , :hqfuel]
      self[col] = acc.sum(col)
    end
    self[:entertainment] = acc.sum(:amusement)
    self.save
  end
end
