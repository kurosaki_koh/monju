class Modification < ActiveRecord::Base
  belongs_to :operation_node
  belongs_to :soldier , 
             :class_name => "SoldierNode" ,
             :foreign_key => "operation_node_id"
  
  serialize :modes
  
  def set_values(val)
    if val.class.to_s == Skill.to_s
      set_values_from_skill(val)
    else
      set_values_from_yggdrasill_skill(val)
    end
    self[:modes] = @modes_yaml
    for key in Job::AllAbilities
      temp_val = @modes_yaml[0]
      mode_val = temp_val[key]
      self[key] = mode_val if mode_val && mode_val != "0"
    end
    self[:mode] = 0
    self[:name] = @modes_yaml[0][:name]
    self[:enable] = false
    self[:fuel] = @modes_yaml[0][:fuel] ? @modes_yaml[0][:fuel] : 0
    
  end
  
  def set_values_from_yggdrasill_skill(ys)
    @modes_yaml = ys.modes
  end
  
  def set_values_from_skill(skill)
    hash = {}
    for key in Job::AllAbilities + [:name , :fuel]
       hash[key] = skill[key]
    end
    @modes_yaml = [hash]
  end
end
