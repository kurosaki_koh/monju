class Specification < ActiveRecord::Base
  belongs_to :meta_spec
  acts_as_list :scope => :meta_spec_id
end
