# -*- encoding: utf-8 -*-
require 'digest/sha1'
require 'set'
class User < ActiveRecord::Base
  attr_accessible :email , :remember_token , :role , :name  , :login , :password , :password_confirmation


  # Virtual attribute for the unencrypted password
  attr_accessor :password

  validates_presence_of     :login #, :email
  validates_presence_of     :password,                   :if => :password_required?
  validates_presence_of     :password_confirmation,      :if => :password_required?
  validates_length_of       :password, :within => 4..40, :if => :password_required?
  validates_confirmation_of :password,                   :if => :password_required?
  validates_length_of       :login,    :within => 3..40
#  validates_length_of       :email,    :within => 3..100
#  validates_uniqueness_of   :login, :email, :case_sensitive => false
  validates_uniqueness_of   :login , :case_sensitive => false
  before_save :encrypt_password

  has_and_belongs_to_many :roles

  has_many :characters , class_name: 'Character' , :finder_sql => Proc.new{%Q(
    SELECT characters.*
    FROM characters
    where character_no like '%#{player_no}%'
  )}

  has_many :owner_accounts , class_name: 'OwnerAccount' , :finder_sql => Proc.new{%Q(
    SELECT owner_accounts.*
    FROM owner_accounts
    where number like '%#{player_no}%'
  )}

  def nations
    self.characters.map{|c|c.character_no[0..1].to_i}
  end

  def validate
    begin
      OpenIdAuthentication::normalize_url(self.identity_url) if self.identity_url && !self.identity_url.empty?
    rescue OpenIdAuthentication::InvalidOpenId
      errors.add(:identity_url , '不正なOpenIDです。')
    end
  end
  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  def self.authenticate(login, password)
    u = find_by_login(login) # need to get the salt
    u && u.authenticated?(password) ? u : nil
  end

  # Encrypts some data with the salt.
  def self.encrypt(password, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    self.class.encrypt(password, salt)
  end

  def authenticated?(password)
    crypted_password == encrypt(password)
  end

  def remember_token?
    remember_token_expires_at && Time.now.utc < remember_token_expires_at 
  end

  # These create and unset the fields required for remembering users between browser closes
  def remember_me
    self.remember_token_expires_at = 2.weeks.from_now.utc
    self.remember_token            = encrypt("#{email}--#{remember_token_expires_at}")
    save(false)
  end

  def forget_me
    self.remember_token_expires_at = nil
    self.remember_token            = nil
    save(:validate => false)
  end
  
  def has_role?(roles )
    return true if roles == []
    return false if self.roles.count == 0
    roles = [roles] unless roles.class == Array
    roles = roles.select{|r|r.class == String }

    my_roles = Set.new(self.roles.collect{|r|r.name})
    required_roles = Set.new(roles.collect{|r|r.to_s})

    intersect = my_roles & required_roles

    return intersect.size > 0
  end


  protected
    # before filter 
    def encrypt_password
      if self.identity_url && !self.identity_url.empty?
        self.identity_url = URI.parse(self.identity_url).normalize.to_s
      end
      return if password.blank?
      self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{login}--") if new_record?
      self.crypted_password = encrypt(password)
    end
    
    def password_required?
      crypted_password.blank? || !password.blank?
    end
end
