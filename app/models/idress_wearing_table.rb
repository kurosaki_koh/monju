# -*- encoding: utf-8 -*-
require 'digest/sha1'
class IdressWearingTable < ActiveRecord::Base
  attr_accessor :turn , :deltas , :note
  belongs_to :nation
  serialize :idress_wearings
  serialize :deltas
  before_create :encrypt_password

  class IdressWearing
    attr_accessor :number
    attr_accessor :name
    attr_accessor :job_idress
    attr_accessor :personal_idress
    attr_accessor :belongs
    attr_accessor :same_as_prev
    attr_accessor :location

    def initialize(hash=nil)
      return if hash.nil?
      for key in hash.keys
        self[key]=hash[key]
      end
    end

    def [](sym)
      self.send(sym)
    end

    def []=(sym,val)
      self.send(sym.to_s + '=' , val)
    end

    def <=>(opponent)
      self.number <=> opponent.number
    end

    def delta_keys
      [:number , :name , :job_idress , :personal_idress , :belongs]
    end

    def attributes
      result = {}
      for k in keys
        result[k]= self[k]
      end
      result
    end
  end

  class Delta
    attr_accessor :old_data
    attr_accessor :new_data
    attr_accessor :updated_at
    attr_accessor :source
    attr_accessor :index

    def initialize

      @old_data = {}
      @new_data = {}
      @updated_at = Time.new
    end

    def Delta.diff(old_rec , new_rec)
      delta = Delta.new
      if old_rec.nil? && new_rec
        new_rec.delta_keys.collect{|k| delta.new_data[k]=new_rec[k]}
        delta.old_data = nil
        return delta
      end
      if new_rec.nil? && old_rec
        old_rec.delta_keys.collect{|k| delta.old_data[k]=old_rec[k]}
        delta.new_data = nil
        return delta
      end
      flag = false
      for key in old_rec.delta_keys
        if old_rec[key] != new_rec[key]
          delta.old_data[key] = old_rec[key]
          delta.new_data[key] = new_rec[key]
          flag = true
        end
      end
      delta.source = old_rec if flag
      return flag ? delta : nil
    end
  end



  def create_characters_wearings
    wearings = []
    nation = self.nation #|| Nation.find(self.nation_id)
#    raise nation.characters.to_yaml
    characters = nation.characters.sort_by{|a| a.character_no }
    kings_name = nation.king
    king = characters.detect{|c|c.name == kings_name}
    if king
      characters.delete(king)
      characters.unshift(king)
    end
    for c in characters
      wearing = IdressWearing.new
      wearing.number = c.character_no
      wearing.name = c.name
      wearing.job_idress = 'なし'
#      wearing.job_idress = c.job_idress.inspect_idresses
#      if wearing.job_idress == '普通の人'
#        wearing.job_idress =  'なし'
#      else
#        wearing.personal_idress = c.personal_idress.name  if c.personal_idress
#      end
      wearings << wearing
    end
    return wearings
  end

  def reset_character_wearings
    self.idress_wearings.delete_if do |record|
        record[:number] !~ /xx/
    end
    npc_records = self.idress_wearings
    characters_wearings = self.create_characters_wearings()
    self.idress_wearings = characters_wearings + npc_records
  end

  def init_idress_wearing_table
    wearings = create_characters_wearings()

    self.deltas = []
    self.idress_wearings = wearings
  end

  def add_deltas(new_hash,wearings)
    delta = Delta.new
    result = [delta]
    if !new_hash.nil?
      flag = false
      for col in delta_keys
        if self[col] != new_hash[col]
          if self[col] == false
            delta.old_data[col] = false
            delta.new_data[col] = true
          else
            delta.old_data[col] = self[col]
            delta.new_data[col] = new_hash[col]
          end
          flag = true
        end
      end
      result = [] unless flag
    end
    max_size = [wearings.size , self.idress_wearings.size].max
    result += (0...max_size).collect{|i|
      d = Delta.diff(self.idress_wearings[i],wearings[i])
      d.index = i if d
      d
    }.compact
    self.deltas.unshift(result) if result.size > 0
  end

  def delta_keys
    [:turn, :title , :note,:is_current]
  end

  def to_s
    nation_idress_wearing_table_path(nation_id , self.id)
  end

  # Encrypts some data with the salt.
  def self.encrypt(password, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    self.class.encrypt(password, salt)
  end

  def authenticated?(password)
    crypted_password == encrypt(password)
  end

  def IdressWearingTable.find_current_table(nation_id)
    result = IdressWearingTable.find_nation_id_and_is_current(nation_id , true)
    if result.nil?
      result = IdressWearingTable.find_by_nation_id(nation_id,
        :order => 'updated_at desc'
      )
    end
    return result
  end

  protected
  def encrypt_password
     self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{nation.name}--") if new_record?
     self.crypted_password = encrypt(self.password)
  end

  def after_save
    if self.is_current
      IdressWearingTable.update_all('is_current = false',
        ['nation_id = ? and not id = ?' , self.nation_id , self.id])
    end
  end

end
