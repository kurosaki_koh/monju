# -*- encoding: utf-8 -*-

class IncludeDefinition < ObjectDefinition
  # def register_command_resource_definitions
  #   CommandResourceDefinition.transaction do
  #     for crd in self.parsed_command_resources
  #       cr = CommandResourceDefinition.new(crd)
  #       cr.object_definition = self
  #       cr.save
  #     end
  #   end
  # end


  def parsed_command_resources
    definition_cache[:includes].map{|crd|crd[:command_resource]}
  end

  private
  def definition_cache
    @_cache ||= YAML.load(self.definition).with_indifferent_access
  end
end
