class Result < ActiveRecord::Base
  attr_accessible :originator , :note , :magic_item , :url , :event_id , :event_name , :not_yet , :position

  belongs_to :character
  belongs_to :event

  acts_as_list :scope => :character_id 
  
  def originator
    return self[:originator].to_i
  end

  def owner_account
    self.character.owner_account
  end
  
  def self.event_names(nation_id = nil)
    sql = "SELECT DISTINCT results.id , results.event_name , results.event_id FROM results INNER JOIN characters ON results.character_id = characters.id "
    sql += " WHERE characters.nation_id = #{nation_id.to_i} " if (nation_id != 'admin' )&& nation_id.to_i
    sql += " ORDER BY results.id"
    event_names = find_by_sql(sql)
    
    names = {}
    results = []
    for r in event_names
      unless names.has_key?(r.event_name)
        names[r.event_name]=r
        results << r
      end
    end
    results.sort_by{|r| r.event_name ? r.event_name : "__"}
  end
  
  def self.list_by_event_name(eid , nid = nil)
    return [] unless eid
    result = Result.find(eid)

    if nid == 'admin'
      cond = [ "results.event_name = ?", result.event_name ]
    else
      nid = result.character.nation_id unless nid
      cond = [ "results.event_name = ? and characters.nation_id = ?" , result.event_name , nid]
    end
    results = Result.find( :all, :conditions => cond , 
                           :include => :character,
                           :order => "characters.character_no")
  end
end
