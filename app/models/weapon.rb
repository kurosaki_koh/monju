class Weapon < IObject
  
  def self.register(account , spec , num)
#    raise [account , spec , num].inspect
    raise ArgumentError ,  'num must be more zero' if num <= 0
    Weapon.transaction do 
      num.times do
        iobj = Weapon.new
        iobj.name = spec.name
        iobj.specification = spec
        iobj.object_id = iobj.id
        account.weapons << iobj
        iobj.save
      end
    end
  end
  
  def self.unregister(account , spec , num)
    targets = find_and_validate_num_of_weapons(account , spec , num)
    num.times{ targets.pop.destroy }
  end
  
  def self.move(source ,dest ,  spec , num)
    targets = find_and_validate_num_of_weapons(source , spec , num)
    self.transaction do
      num.times{ 
        target = targets.pop
        target.owner_account = dest
        target.save
      }
    end
  end

  private
  def self.find_and_validate_num_of_weapons(account , spec , num)
    targets = account.weapons.find_all_by_specification_id(spec.id,:order => 'object_id' )
    num_target = targets.nil? ? 0 : targets.size
    raise "Target weapons are too few.Excpect #{num} or more , but #{num_target}" if num_target < num
    targets
  end
  
end