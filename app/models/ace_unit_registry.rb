# -*- encoding: utf-8 -*-

class AceUnitRegistry < UnitRegistry
# attr_accessible :authority_url, :object_definition_id, :name, :note, :owner_account_id

  def source=(source)
    self.object_definition ||= AceUnitDefinition.new(object_type: 'ＡＣＥユニット定義')

    self.object_definition.definition = source
  end



end
