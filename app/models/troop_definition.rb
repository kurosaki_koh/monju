require_dependency 'i_language/calculator'
class TroopDefinition < ActiveRecord::Base
  attr_accessible :name , :turn , :nation_id , :source_url , :taigen_def ,
                  :source_updated_at , :taigen_def_updated_at

  include ILanguage::EVCC
  include ILanguage::Calculator

  serialize :evaluations_data , Hash

  belongs_to :nation

  def nation_formated
    sprintf("%02d:%s" , nation_id , nation.name)
  end


  def parse_troop
    context = Context.new(self.turn.to_i)
    self.taigen_def.gsub!("\r\n","\n")
    self.taigen_def.gsub!(/ +\n/,"\n")
    self.taigen_def.strip!

    context.parse_troop(self.taigen_def)
  end

  def taigen_check
    check(ILanguage::Calculator::TaigenValidations )
  end

  def rule_check
    check(ILanguage::Calculator::RuleValidations )
  end

  def check(validations)
    @troop ||= self.parse_trop
    result = {}
    for validator in validations.sorted_list
      validation = {}
      validation[:validator] = validator
      validation[:result] = validator.do_check(@troop)
      result[validator.no] = validation
    end
    result
  end
  
  def update_evaluations
    begin
      troop = parse_troop
      self.evaluations_data = troop.make_result
      self.evaluations = troop.all_result_str
    rescue => e
      self.evaluations_data = nil
      self.evaluations = e.to_s
    end
  end

  before_create  :update_evaluations

  def before_update
    update_evaluations if self.taigen_def_changed? || self.turn_changed?
  end
end
