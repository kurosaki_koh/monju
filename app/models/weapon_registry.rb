class WeaponRegistry < ActiveRecord::Base
  attr_accessible :name , :number , :note , :reference_url , :owner_account_id , :event_no , :event_name ,
                  :turn , :position

  belongs_to :specification
  belongs_to :owner_account

  acts_as_list :scope => :owner_account_id

  belongs_to :destination , 
             :foreign_key => :destination_id ,
             :class_name => 'WeaponRegistry'

  IObject #for load class 'Weapon'
  
  def self.register(account,spec,num, note='' , ref_url='')
    record = self.new
    record.specification = spec
    record.name = spec.name
    record.owner_account = account
    record.number = num
    record.note = note
    record.reference_url = ref_url
    record.save
    if num.to_i > 0
      Weapon.register(account,spec,num)
    else num.to_i < 0
      Weapon.unregister(account,spec,num)
    end
  end
  
  def self.move(account,dest , spec,num, note='' , ref_url='')
    record = self.new
    record.specification = spec
    record.owner_account = account
    record.number = num.abs * -1
    record.name = spec.name
    record.note = note
    record.reference_url = ref_url
    record.save
    
    record_dest = self.new
    record_dest.specification = spec
    record_dest.owner_account = dest
    record_dest.number = num.abs
    record_dest.note = note
    record_dest.reference_url = ref_url
    record_dest.name = spec.name
    record_dest.save
    
    Weapon.move(account , dest , spec , num)
  end
  
  def has_destination?
    !self.destination.nil?
  end
end
