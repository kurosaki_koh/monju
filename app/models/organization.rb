class Organization < ActiveRecord::Base
  attr_accessible :name , :org_no , :type , :note , :parent_organization

  has_one :owner_account , :as => :owner , :dependent => :destroy
  owner_factory = OwnerAccountFactory.new(:org_no)
  after_create owner_factory
  after_update owner_factory
  
  def belong
    :empire #組織の財務表は所属問わず高性能燃料の欄を含むものとする。
  end
#  after_update OwnerAccountFactory
end

class Knights < Organization
end

class AdventurersGuild < Organization
end

class AdvanceBase < Organization
end

class IndividualKnights < Organization
end

class Corporation < Organization
end

class OtherOrg < Organization
end