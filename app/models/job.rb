# -*- encoding: utf-8 -*-
class Job < ActiveRecord::Base
  attr_accessible :phy, :str, :dur, :chr, :agi, :dex, :sen, :edu, :luc ,
                  :name , :note , :originator , :definition
  Abilities = [:phy, :str, :dur, :chr, :agi, :dex, :sen, :edu, :luc]
  AbilityKanji = {:phy => '体格' , :str =>'筋力' , :dur => '耐久力' , 
    :agi => '敏捷' , :dex=>'器用' , :chr => '外見' ,
    :sen => '感覚' , :edu => '知識' , :luc => '幸運'}
  BattleAbilities = [:zero_attack,:to_5m, :to_50m, :to_300m, :to_500m, :defense]
  AllAbilities = Abilities + BattleAbilities

  validates_uniqueness_of :name
  
  def skills
    Skills[self.name]
  end
  
  def after_update
    related_idresses = JobIdress.find(:all , :conditions => ['job1_id = ? or job2_id = ? or job3_id = ?' , self[:id], self[:id], self[:id]])
    
    JobIdress.transaction do
      for related in related_idresses
        related.culc_abilities
        related.save
      end
    end
  end
  
  def before_destroy
    related_idresses = JobIdress.find(:all , :conditions => ['job1_id = ? or job2_id = ? or job3_id = ?' , self[:id], self[:id], self[:id]])
    JobIdress.transaction do
      for related in related_idresses
        flag = false
        [:job1_id , :job2_id , :job3_id].each{|col| 
          if related[col] == self[:id]
            related[col] = nil
            flag = true
          end
          if flag
            related.culc_abilities
            related.save
          end
        }
      end
    end
  end
end

class JobSkill
  attr :modes,true
  attr :name,true
  
  def initialize(parsed)
    @modes = []
    for mode in parsed
      @modes << mode  
    end
    @name = @modes[0][:name]
    @modes.shift if @modes.size > 1
  end
  
end

SkillsYaml = <<EOT
---
"剣士": 
- - :name: "0m戦闘修正+２"
    :zero_attack: 2
    :fuel: 1
- - :name: "〜5m戦闘修正+2"
    :to_5m: 1
    :fuel: 1
- - :name: "防御修正+2"
    :defencse: 2
    :fuel: 1
"理力使い": 
- - :name: "〜300m戦闘修正+２"
    :to_300m: 2
    :fuel: 1
- - :name: "〜500m戦闘修正+1"
    :to_500m: 1
    :fuel: 1
"忍者": 
- - :name: "0m戦闘修正+"
    :zero_attack: 1
    :fuel: 1
- - :name: "〜5m戦闘修正+1"
    :to_5m: 1
    :fuel: 1
"サイボーグ": 
- - :name: "宇宙戦行為"
    :fuel: 1
- - :name: "ナショナルネット接続行為"
    :fuel: 1
- - :name: "筋力/耐久力修正+2"
    :str: 2
    :dur: 2
    :fuel: 1
"ドラッガー": 
- - :name: "薬物による強化行為"
  - :name: "体格+1"
    :phy: 1
    :fuel: 1
  - :name: "筋力+1"
    :dur: 1
    :fuel: 1
  - :name: "耐久+1"
    :dur: 1
    :fuel: 1
  - :name: "外見+1"
    :chr: 1
    :fuel: 1
  - :name: "敏捷+1"
    :agi: 1
    :fuel: 1
  - :name: "器用+1"
    :dex: 1
    :fuel: 1
  - :name: "感覚+1"
    :sen: 1
    :fuel: 1
  - :name: "知識+1"
    :edu: 1
    :fuel: 1
  - :name: "幸運+1"
    :luc: 1
    :fuel: 1
- - :fuel: 1
    :name: "予知夢行為"
"歩兵": 
- - :name: "50m戦闘修正+1"
    :to_50m: 1
    :fuel: 1
- - :name: "〜300m戦闘修正+2"
    :to_300m: 2
    :fuel: 1
- - :name: "〜500m戦闘修正+1"
    :to_300m: 1
    :fuel: 1
"整備士": 
- - :name: "整備修正+3"
    :dex: 3
    :fuel: 1
"医師": 
- - :name: "治療修正+3"
    :dex: 3
    :edu: 3
    :luc: 3
    :fuel: 1
"学生": 
- - :name: "連戦時+2"
    :phy: 2
    :str: 2
    :dur: 2
    :chr: 2
    :agi: 2
    :dex: 2
    :sen: 2
    :edu: 2
    :luc: 2
    :fuel: 1
"銃士隊": 
- - :name: "中距離戦闘+２"
    :to_50m: 2
    :to_300m: 2
    :fuel: 1
"名パイロット": 
- - :name: "I=D戦闘"
  - :name: "I=D戦闘体格+１"
    :phy: 1
    :fuel: 1
  - :name: "I=D戦闘筋力+1"
    :dur: 1
    :fuel: 1
  - :name: "I=D戦闘耐久+1"
    :dur: 1
    :fuel: 1
  - :name: "I=D戦闘外見+1"
    :chr: 1
    :fuel: 1
  - :name: "I=D戦闘敏捷+1"
    :agi: 1
    :fuel: 1
    :sen: 2
  - :name: "I=D戦闘器用+1"
    :dex: 1
    :fuel: 1
  - :name: "I=D戦闘感覚+1"
    :sen: 1
    :fuel: 1
  - :name: "I=D戦闘知識+1"
    :edu: 1
    :fuel: 1
  - :name: "I=D戦闘幸運+1"
    :luc: 1
    :fuel: 1
"偵察兵": 
- - :name: "〜50m戦闘+２"
    :to_50m: 2
    :fuel: 1
- - :name: "偵察+3"
    :sen: 3
    :fuel: 1
"名整備士": 
- - :name: "整備+3"
    :dex: 3
    :fuel: 1
- - :name: "I=Dカスタマイズ+1"
    :fuel: 1
"名医": 
- - :name: "治療+3"
    :dex: 3
    :edu: 3
    :luc: 3
    :fuel: 1
- - :name: "奇跡の幸運+1"
    :luc: 1
    :fuel: 1
"舞踏子": 
- - :name: "ヤガミ・ドランジ・アキ+3"
    :fuel: 1
"理力建築士": 
- - :name: "陣地:自分+２人の戦闘+2"
    :zero_attack: 2
    :to_5m: 2
    :to_50m: 2
    :to_300m: 2
    :to_500m: 2
    :defense: 2
    :fuel: 1
"幻影使い": 
- - :name: "相手の感覚-3"
    :fuel: 1
"ドラッグマジシャン": 
- - :name: "薬物強化(改)"
  - :name: "体格+2"
    :phy: 2
    :fuel: 1
  - :name: "筋力+2"
    :dur: 2
    :fuel: 1
  - :name: "耐久+2"
    :dur: 2
    :fuel: 1
  - :name: "外見+2"
    :chr: 2
    :fuel: 1
  - :name: "敏捷+2"
    :agi: 2
    :fuel: 1
  - :name: "器用+2"
    :dex: 2
    :fuel: 1
  - :name: "感覚+2"
    :sen: 2
    :fuel: 1
  - :name: "知識+2"
    :edu: 2
    :fuel: 1
  - :name: "幸運+2"
    :luc: 2
    :fuel: 1
"泥棒猫": 
- - :name: "侵入+3"
    :agi: 3
    :fuel: 1
"世界忍者": 
- - :name: "侵入+3"
    :agi: 3
    :fuel: 1
"猫": 
- - :name: "侵入+3"
    :agi: 3
    :fuel: 1
"学兵": 
- - :name: "適応能力"
  - :name: "体格+１"
    :phy: 1
    :fuel: 1
  - :name: "筋力+1"
    :dur: 1
    :fuel: 1
  - :name: "耐久+1"
    :dur: 1
    :fuel: 1
  - :name: "外見+1"
    :chr: 1
    :fuel: 1
  - :name: "敏捷+1"
    :agi: 1
    :fuel: 1
  - :name: "器用+1"
    :dex: 1
    :fuel: 1
  - :name: "感覚+1"
    :sen: 1
    :fuel: 1
  - :name: "知識+1"
    :edu: 1
    :fuel: 1
  - :name: "幸運+1"
    :luc: 1
    :fuel: 1
"戦車兵": 
- - :name: "乗車時感覚/知識+2"
    :sen: 2
    :edu: 2
    :fuel: 1
"騎士": 
- - :name: "近距離+2"
    :zero_attack: 2
    :to_5m: 2
    :fuel: 1
- - :name: "防御+3"
    :defense: 3
    :fuel: 1
- - :name: "長距離移動"
    :fuel: 1
"宇宙軍": 
- - :name: "筋力/耐久力+2"
    :str: 2
    :dur: 2
    :fuel: 1
"工兵": 
- - :name: "〜300m戦闘+2"
    :to_300m: 2
    :fuel: 1
- - :name: "建築物破壊"
    :fuel: 1
- - :name: "塹壕:自分+2人に戦闘+1"
    :zero_attack: 1
    :to_5m: 1
    :to_50m: 1
    :to_300m: 1
    :to_500m: 1
    :defense: 1
    :fuel: 1
"魔法使い": 
- - :name: "自虐ネタ:非アラダ感覚ｰ４"
    :fuel: 1
EOT

SkillsData = YAML::load(SkillsYaml)
Skills = {}
for job_name in SkillsData.keys
  job_skills = SkillsData[job_name]
  Skills[job_name] = [] 
  for job_skill in job_skills
    #    job_skill.unshift({:name => "なし"})
    for mode in job_skill
      for col in [:phy,:str,:dur,:chr,:agi,:dex,:sen,:edu,:luc,
        :zero_attack,:to_5m,:to_50m,:to_300m,:to_500m,:defense,:fuel]
        mode[col] = 0 unless mode[col]
      end
    end
    Skills[job_name] << JobSkill.new(job_skill)
  end
end
