class Property < ActiveRecord::Base

  def to_i
    self.value.to_i
  end
  
  def to_s
    self.value.to_s
  end
  
  def self.[](key)
    key = key.to_s if key.class == Symbol
    prop = Property.find(:first , :conditions => ["name = ?" , key])
    prop && prop.value
  end
end
