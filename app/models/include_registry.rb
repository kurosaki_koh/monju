# -*- encoding: utf-8 -*-

class IncludeRegistry < UnitRegistry
# attr_accessible :authority_url, :object_definition_id, :name, :note, :owner_account_id

  def source=(source)
    self.object_definition ||= IncludeDefinition.new(object_type: 'インクルード定義')
    @definition = nil
    self.object_definition.definition = case source
                                          when String
                                            source
                                          when Hash
                                            source.to_yaml
                                          else
                                            source
                                        end

  end

  def definition
    @definition ||= YAML.load(self.source).with_indifferent_access
  end

  def command_resource_definitions
    self.object_definition.parsed_command_resources
  end

end
