class Nation < ActiveRecord::Base
  attr_accessible :name , :homepage , :king , :regent , :contact , :idress_list ,
                  :short_name , :contact_note , :belong , :race_id , :restriction , :npc_names
  has_many :job_idresses
  has_many :characters
  has_many :cash_records ,:order => "turn , position"
  
  belongs_to :race 
  has_one :owner_account , :as => :owner, :dependent => :destroy
  has_one :latest_cash_record , :foreign_key => :id
  
  serialize :belong
  owner_factory = OwnerAccountFactory.new(:id)
  after_create owner_factory
  after_update owner_factory

  def Nation::select_list
    @nations = []
    for nation in Nation.order('id')
      @nations << ["#{nation.id}:#{nation.name}",nation.id.to_i]
    end
    
    @nations
  end
  
  def create_owner_account
    unless owner_account 
      account = OwnerAccount.new
      account.nation = self
      account.save
    end
  end
  
  def sum(col,options={})
    val = 0
    for row in cash_records(:all , options)
      val += row[col]
    end
    val
  end

  def num_npc(turn=Property['current_turn'] )
    num_npc = 0
    cr = CashRecord.settlement_by_turn(self.owner_account.id,turn.to_i )
    num_npc = cr ? cr.num_npc : 15
    num_npc ||= 0

    crs = CashRecord.find_all_by_owner_account_id_and_turn(self.owner_account.id , turn)
    for cr in crs
      num_npc += cr.num_npc.to_i if cr.num_npc.to_i > 0
    end
    return num_npc
  end
  
  def npc_names
    list = self[:npc_names].strip.split("\n")
    list.collect{|name|
      if name =~ /^(\d{2}-.....-..)/
        number = $1
        name.gsub!(number,'')
        name.strip!
      end
      name
    }
  end

  def npc_numbers
    list = self[:npc_names].strip.split("\n")
    list.collect{|name|
      if name =~ /^(\d{2}-.....-..)/
        $1.strip
      else
        nil
      end
    }
  end

  def nation
    self
  end

  def extended_command_resources
    []
  end
end
