# -*- encoding: utf-8 -*-
class ObjectDefinition < ActiveRecord::Base
  attr_accessible :name , :definition , :specialty_definition , :hurigana , :object_type , :addition

  has_many :object_registries
  has_many :owner_account , :through => :object_registries
  serialize :addition , Hash

  has_many :command_resource_definitions

  include ::CommandResourceParser

  CountableTypes = %w"オプション 騎跨装備 兵器 乗り物 ウォードレス 艦船 アイテム マジックアイテム"
  UnitDefinitionTypes = %w"コンバート定義 ＡＣＥユニット定義"

  scope :countable , -> { where(self.arel_table[:object_type].in(CountableTypes) ) }

  scope :extendable , -> { where(self.arel_table[:object_type].not_in(CountableTypes + UnitDefinitionTypes)  ) }

  def zukan_definition_url=(value)
    self.addition= {} if self.addition.nil?
    self.addition[:zukan_definition_url]=value  
  end
  
  def zukan_definition_url
    self.addition= {} if self.addition.nil?
    self.addition[:zukan_definition_url]
  end
  
  class Specialty
    attr :action_name,true
    attr :branch , true
    attr :condition, true
    attr :effects, true
    
    class Effect
      attr :effect_type , true
      attr :target , true
      attr :value , true

      def parse(definition)
        
      end
    end
    def initialize(definition)
      @effects = []
      parse(definition)
    end
    
    def parse(definition)
#      str = definition.gsub(/[，、]/,',')
      @action_name , @branch , @condition , @effects = begin
        eval(definition) 
      rescue SyntaxError
        [nil,nil,nil,[]]
      end
    end
    
    def bonus(action_name , branch , cond ,target)
      return 0 unless @effects
      matched = @effects.select{|ef| ef[:target] == target ||  ef[:target] == '全評価'}
      return matched.inject(0){|sum,eff| sum+=eff[:value]}
    end
  end
  
#  def initialize
#    @specialities = nil
#  end
  
  def specialties
    init_specialties
    @specialties
  end
  
  def init_specialties
    unless @specialties
      @specialties = []
      return if  specialty_definition.nil? || specialty_definition.empty?
      for line in self.specialty_definition
        @specialties << Specialty.new(line)
      end
    end
  end
  
  def bonus(action_name , branch , cond ,target)
    return specialties.inject(0){|sum,s|s.bonus(action_name,branch,cond,target)}
  end

  def countable?
    !!(CountableTypes.include?(self.object_type) )
  end



  def register_command_resource_definitions
    CommandResourceDefinition.transaction do
      for crd in self.parsed_command_resources
        cr = CommandResourceDefinition.new(crd)
        cr.object_definition = self
        cr.save
      end
    end
  end
  protected
  before_save {
    self.addition = {} if self.addition.nil?
  }


end

class CommonDefinition < ObjectDefinition
  validates_uniqueness_of :name

  after_create {
    ObjectRegistry.update_all("object_definition_id = #{self.id}" , ["name = ?",self.name])
    register_command_resource_definitions if is_idress3_format?

    for ygg in Yggdrasill.where(idress_name: self.name)
      ygg.register_command_resources
    end
  }

  after_update {
    ObjectRegistry.transaction do
      ObjectRegistry.update_all("object_definition_id = #{self.id}" , ["name = ?",self.name])
      ObjectRegistry.update_all("name = '#{self.name}'" , ["object_definition_id = ?",self.id])
    end
  }

end

