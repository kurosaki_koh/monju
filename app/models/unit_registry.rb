# -*- encoding: utf-8 -*-

class UnitRegistry < ActiveRecord::Base
  attr_accessible :authority_url, :object_definition_id, :name, :note, :owner_account_id
  belongs_to :owner_account
  belongs_to :object_definition , include: :command_resource_definitions , dependent: :destroy

  has_many :command_resources, dependent: :delete_all  , as: :source

  scope :single , ->{
    t = UnitRegistry.arel_table[:type]
    where(t.eq('ConvertSource').or t.eq('AceUnitDefinition') )
  }

  scope :by_name , ->(name){
    select(<<SQL).order(:priority)
* , CASE type
WHEN 'IncludeRegistry' then 1
WHEN 'ConvertSource' then 2
else 3
end priority
SQL
  }

  def source
    self.object_definition.definition
  end

  before_save do
    self.object_definition.name = self.name if self.object_definition.name != self.name
    self.object_definition.save
  end

  def rebuild_command_resources
    self.object_definition.command_resource_definitions.delete_all
    self.object_definition.register_command_resource_definitions
    self.object_definition.reload
    self.register_command_resources
  end

  def to_format
    "#{self.owner_account.number}:#{self.name}"
  end

  def register_command_resources
    CommandResource.transaction do
      for crd in self.object_definition.command_resource_definitions
        command_resource = CommandResource.new
        command_resource.note = "#{self.owner_account.name}のコマンドリソース"
        command_resource.command_resource_definition= crd
        command_resource.owner_account= self.owner_account
        command_resource.authority_url= self.authority_url
        command_resource.available= true
        command_resource.source = self
        command_resource.source_type = self.class.name
        command_resource.save
        puts crd.inspect
      end
    end

    self.available=true
    self.save
  end

  def disable_command_resources
    CommandResource.transaction do

      query = <<EOT
DELETE command_resources FROM command_resources
INNER JOIN command_resource_definitions AS crds on (command_resources.command_resource_definition_id = crds.id)
WHERE crds.object_definition_id = #{self.object_definition_id} and command_resources.owner_account_id = #{self.owner_account_id}
EOT

      ActiveRecord::Base.connection.execute(query)
      self.available=false
      self.save
    end
  end

  def available_command_resources
    (self.owner_account.available_command_resources || []) + (self.command_resources || [] ) + (self.owner_account.extended_command_resources || [])
  end


  def command_resource_definitions
    self.object_definition.parsed_command_resources
  end
end