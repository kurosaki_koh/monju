class Race < ActiveRecord::Base
  attr_accessible :name , :phy, :str, :dur, :chr, :agi, :dex, :sen, :edu, :luc ,:note

  def Race::select_list
    @result = []
    for r in Race.find(:all)
      @result << [r.name,r.id]
    end
    @result
  end

  def to_s
    self.name
  end
end
