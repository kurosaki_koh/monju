class IObject < ActiveRecord::Base
  attr_accessible :object_id , :name , :type , :owner_account_id , :object_definition_id

  belongs_to :owner_account
  belongs_to :object_registry


end
