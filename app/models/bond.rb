# -*- encoding: utf-8 -*-
class Bond < ActiveRecord::Base

  attr_accessible :bond_type , :character_id , :ace_id ,:old_character_name ,  :character_name_changing_url , :character_ace_id , :old_ace_name ,
                  :ace_name_changing_url , :date_note , :note
  belongs_to :character
  belongs_to :ace
  belongs_to :character_ace , :class_name => 'ObjectDefinition' , :foreign_key => 'character_ace_id'

  validates_associated :ace , :message => 'が選択されていません。'
end
