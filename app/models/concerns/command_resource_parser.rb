# encoding : utf-8

module CommandResourceParser
  FirstLineFormat = /^(.+?)(（.+?）)：(.+?)：＃(.*)/
  CommandResourceFormat = /^・?(.+?)(（.+?）)?：(.+?)：＃(.*)/

  def is_idress3_format?(text = self.definition)
    return false if text.blank?
    !!(text.each_line.to_a[0] =~ CommandResourceFormat)
  end
  def is_idress3?(text = self.definition)
    if  is_idress3_format?(text)
      resources = self.parsed_command_resources(text)
      return resources.size >= 2
    end
    false
  end

  alias :derivable? :is_idress3?

  def is_command_resource?(text = self.definition)
    return false if text.blank?
    if text.each_line.to_a[0] =~ CommandResourceFormat
      resources = self.parsed_command_resources
      return resources.size == 1
    end
    false
  end

  def parsed_command_resources(text = self.definition)
    result = []
    return result unless self.is_idress3_format?
    for line in text.each_line
      line.strip!
      if match_data = CommandResourceFormat.match( line)
        power = match_data[3]
        if power =~ /^[０１２３４５６７８９]+$/
          power = JcodeUtil.parse_int(power)
        end
        result << {:name => match_data[1] , :power => power, :tag => match_data[4]}
      end
    end
    return result
  end
end