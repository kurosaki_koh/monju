require 'object_definition'
module IdressSearcher
  def search_idress(name)
    return nil if name.blank?
    IDefinition.find_by_name(name ) || CommonDefinition.find_by_name(name)
  end
end