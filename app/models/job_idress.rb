# -*- encoding: utf-8 -*-
class JobIdress  < ActiveRecord::Base
  attr_accessible :name , :phy, :str, :dur, :chr, :agi, :dex, :sen, :edu, :luc ,
                  :note , :race_id , :job1_id , :job2_id , :job3_id , :job4_id ,
                  :nation_id ,:hq , :owner_account_id , :idress_type , :acquired_by_mile ,
                  :regular , :regular_no , :race_type

  # include Calcuratable
  serialize :hq
  serialize :bonuses
  
  belongs_to :race, 
    :class_name => "Race",
    :foreign_key => "race_id"

  belongs_to :job1, 
    :class_name => "Job",
    :foreign_key => "job1_id"

  belongs_to :job2, 
    :class_name => "Job",
    :foreign_key => "job2_id"

  belongs_to :job3, 
    :class_name => "Job",
    :foreign_key => "job3_id"

  belongs_to :job4, 
    :class_name => "Job",
    :foreign_key => "job4_id"

  belongs_to :nation
  belongs_to :owner_account
  
  has_one :skill

  has_many :modify_commands , :as => :target

  HQ_LEVELS = [:hq0, :hq1, :hq2 , :hq3 , :etc]
  def initialize
    super
    self.hq = {}
    self.bonuses = HQ_LEVELS.inject({}){|hash,lv| hash[lv]={:quality => '0'};hash}
  end
  
  def ability(column)  ##Override from Culcuratable
    if Job::Abilities.include? column
      self.attributes[column]
    else
      nil
    end
  end
  #
  #  def sum_ability(column)
  #    sum = race[column] + job1[column] + job2[column]
  #    sum += job3[column] if job3
  #    sum += job4[column] if job4
  #    sum += self.attributes[col.to_s] if self.attributes[col.to_s]
  #    sum
  #  end
  
  def JobIdress::select_list(owner_account=nil , race_type = nil ,belong = nil)
    jobs = JobIdress.find_all_by_owner_account_id_and_idress_type(owner_account , 'JobIdress').
      select{|j|j.regular_no && j.regular? && (race_type.nil? || (j.race_type.match(/#{race_type}/)))}
    
    jobs = JobIdress::sort_by_race(jobs , belong) if belong
    @result = jobs.map{|j| [j.inspect_idresses , j.id]}
    @result
  end
  
  def JobIdress::sort_by_race_lambda(belong)
    race_order = (belong && belong == :republic) ? %w!人 人１ 人２ 猫 犬 フェアリー メタルボディ 蜘蛛型舞踏体! : %w!人 人１ 人２ 犬 猫 フェアリー メタルボディ 蜘蛛型舞踏体	!
    lambda {|j|
      [race_order.index(j.race_type) || 999, j.regular_no || 999 , j.inspect_idresses]
    }
  end
  
  def JobIdress::sort_by_race(job_idresses , belong = nil)
    job_idresses.sort_by(&JobIdress::sort_by_race_lambda(belong))
  end
  
  def is_this?(targets)
    return false if targets[0] != self.race.name
    yggs = jobs
    for i in 1..(targets.size-1)
      ygg = yggs.find{|y| 
        y.name == targets[i]
      }
      if ygg
        yggs.delete_at(yggs.index(ygg))
      else
        return false
      end
    end
    
    return yggs.size == 1
  end
  
  def jobs
    [race,job1,job2,job3,job4].compact
  end
  
  def inspect_idresses
    jobs.collect{|y|y.name}.join("＋")
  end

  def skills
    ret = []
    jobs.each{|y| ret += y.skills if y.skills }
    ret
  end
  
  def culc_abilities
    self.idress_type = 'JobIdress' if self.idress_type.nil?
    if self.idress_type == 'JobIdress'
      self.job4 = nil
    else
      self.race = self.job1 = self.job2 = self.job3 = nil
    end
    for col in [:phy , :dur , :str , :chr , :agi , :dex , :sen , :edu , :luc]
      val = 0
      val = jobs.inject(0){|v,y|v + y[col].to_i }
      self.hq = {} if self.hq.nil?
      for lv in HQ_LEVELS
        next unless self.bonuses[lv]
        lv_hash = self.bonuses[lv]
        bonus = lv_hash ? lv_hash[:quality].to_i : 0
        val += bonus.to_i if bonus && self.bonuses[lv][:hq_target].to_s == col.to_s
      end
#      val += hq[col] if hq[col]
      self[col]= val
    end
  end
  
  alias :name  :inspect_idresses


  before_update :culc_abilities
  before_create :culc_abilities

end
  