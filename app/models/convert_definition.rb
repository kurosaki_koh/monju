# -*- encoding: utf-8 -*-

class ConvertDefinition < ObjectDefinition

  has_one :convert_source , foreign_key: :object_definition_id
  validate :compile_and_set_addition

  include ILanguage::Calculator::Convertible

  def compile
    @context = Context.new
    begin
      @troop = @context.parse_troop(self.definition)
      raise 'コンバート定義に含まれるユニットが複数存在します。' unless @troop.children.select { |c| !c.is_attendant? }.size == 1
    rescue => e
      errors.add(:definition, '太元書式エラー：' + e.to_s)
      return false
    end
    @context
  end

  def compile_and_set_addition
    if self.definition_changed?
      if self.compile
        self.addition = @context.hash_for_api
      else
        return false
      end
    end
    return true
  end

  def is_idress3_format?(dummy=nil)
    true
  end

  def troop_hash
    self.addition
  end

  def troop
    self.compile.troop
  end

  def parsed_command_resources(test = self.definition)
    parse_command_resources
  end

  # def owner_name
  #   self.compile
  #   unit = @troop.children.first
  #   if self.convert_source.owner_account.owner_type == 'Character' && unit.is_ace?
  #     unit.idresses.first + '（ＡＣＥ）'
  #   else
  #     unit.name
  #   end
  # end
  #
  # def parsed_command_resources(text = self.definition)
  #   result = []
  #
  #   hash = Hash[*self.addition['divisions'].first['evaluations'].flatten(1)]
  #   name = self.owner_name
  #
  #   %w"追跡 装甲 陣地構築 侵入 同調".each { |key| select_max_value(hash, key) }
  #
  #   select_max_value(hash, '治療', /治療$/)
  #
  #
  #   for key in hash.keys
  #     result << {
  #         name: "#{name}の#{key}",
  #         power: hash[key],
  #         tag: key
  #     }
  #   end
  #
  #   command_resources = self.addition['divisions'].first['command_resources']
  #   result += command_resources if command_resources
  #
  #   return result
  # end
  #
  # private
  # def select_max_value(hash, keyword, re = nil)
  #   re = /^#{keyword}.*/ unless re
  #   keys = hash.keys.select { |key| re =~ key }
  #   max_value = keys.collect { |key| hash[key] }.max_by{|val| val.class == String ? 99999 : val}
  #   keys.each { |k| hash.delete(k) }
  #   hash[keyword] = max_value if max_value
  #   max_value
  # end
end