class Ace < ActiveRecord::Base
  attr_accessible :name , :hurigana , :object_definition_id  , :character_no

  belongs_to :object_definition
  has_one :owner_account  , :as => :owner

  has_many :bonds , :dependent => :destroy

  owner_factory = OwnerAccountFactory.new(:character_no)
  after_create owner_factory
  after_update owner_factory

  def fellow
    bonds.first.character
  end

  def related_character
    return nil if bonds.size == 0
    return bonds[0].character
  end

  def extended_command_resources
    ch_id = self.fellow.owner_account.id
    n_id = self.fellow.nation.owner_account.id

    CommandResource.where(CommandResource.arel_table[:owner_account_id].in([ch_id , n_id])).merge(CommandResource.extendable).collect{|cr|cr[:from] = cr.owner_account.name ; cr}
  end
end
