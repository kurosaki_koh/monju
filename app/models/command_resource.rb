# encoding : utf-8

class CommandResource < ActiveRecord::Base
  belongs_to :owner_account
  belongs_to :source , polymorphic: true
#  belongs_to :yggdrasill , polymorphic

  belongs_to :command_resource_definition

#  attr_accessible :name , :power , :tag , :category , :authority_url , :note
  attr_accessible  :category , :authority_url , :note

  scope :nested_availabilities , lambda {|id1 ,id2| where('owner_account_id = :id1 or owner_account_id = :id2' , id1: id1 , id2: id2)}
  validates_uniqueness_of :command_resource_definition_id , scope: :owner_account_id

  scope :extendable , -> {joins(:command_resource_definition => :object_definition).merge(ObjectDefinition.extendable)}

  scope :defined_by , ->(object_definition_id){
    crd = CommandResourceDefinition.arel_table
     joins(:command_resource_definition).
         where(crd[:object_definition_id].
                   eq(object_definition_id))
  }

  scope :delete_by_definition , ->(object_definition_id){
    crd = CommandResourceDefinition.arel_table
    where("id in (#{select("`command_resources`.`id`").
              joins("as p1 inner join command_resource_definitions as p2 on p1.command_resource_definition_id = p2.id").where("p2.object_definition_id" => object_definition_id  ).to_sql}) " ).as("p3")
  }

  def to_form
    self.command_resource_definition.to_form
    # result = self.name.dup
    # result += '（' + self.category + '）' unless self.category.blank?
    # result += '：'+ self.power.to_s + '：＃' + self.tag
    # result
  end

  def update_available
    odef = self.command_resource_definition.object_definition
    self.available = !odef.countable? || self.owner_account.num_of_property(odef.name) > 0
  end

  def name
    self.command_resource_definition.try(:name)
  end

  def power
    self.command_resource_definition.try(:power)
  end

  def tag
    self.command_resource_definition.try(:tag)
  end

  def to_hash
    result = [:name , :power , :tag , :id , :source_id , :source_type].inject({}){|h,k|h[k] = self.send(k);h}
    [:from , :from_id].each{|k|result.update({from: self[k] }) if self[k] }
    result
  end

  def from_id
    self[:from_id]
  end

  # def category
  #   self.command_resource_definition.category
  # end

end
