module Calcuratable

  def [](col)
    if Job::Abilities.include? col
      return self.ability(col)
    elsif Job::BattleAbilities.include? col
      return culc_ability(col)
    else
      super(col)
    end
  end

  def ability(col)
    return self.read_attribute(col)
  end
  
  def culc_ability(column)
    case column
    when :zero_attack
       (ability(:phy) + ability(:str)).to_f / 2
    when :to_5m
       (ability(:phy) + ability(:str)).to_f / 2
    when :to_50m
       (ability(:sen) + ability(:edu)).to_f / 2
    when :to_300m
       (ability(:sen) + ability(:edu)).to_f / 2
    when :to_500m
       (ability(:sen) + ability(:agi)).to_f / 2
    when :defense
       (ability(:phy) + ability(:dur)).to_f / 2
    else
       nil
    end
  end

  def rd(column)
    val = ability(column)
    return  val  ? (1.5 ** val) : 0
  end
end