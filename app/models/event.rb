class Event < ActiveRecord::Base
  attr_accessible :name , :results_url , :turn , :order_no , :has_result

  has_many :results
end
