class Operation < ActiveRecord::Base
  serialize :content
  has_many :operation_nodes,:dependent => :destroy
  include Calcuratable
  
  def rd(col)
    val = 0
    if Job::Abilities.include? col
      for c in troops
         val += c.rd(col)
      end
      return val
    elsif Job::BattleAbilities.include? col
      return nil
    end
  end
  
  def ability(col)
    if Job::Abilities.include? col
      rd_val = self.rd(col)
      return (Math::log(rd_val) / Math::log(1.5)).round
    elsif Job::BattleAbilities.include? col
      culc_ability(col).round
    end
  end
  
  def init_content()
    hash = {}
    for nation in Nation.find(:all)
      sub_hash = {}
      [:fund,:resource,:food, :fuel, :num_npc].each{|key| sub_hash[key] = nation[key]}
      hash[nation.id] = sub_hash
    end
    self[:content] = hash
  end
end
