class MileChange < ActiveRecord::Base
  attr_accessible :mile , :reason , :application_url ,:ground_url , :owner_account_id,
                  :note , :turn

  belongs_to :owner_account
  has_many :modify_commands , :as => :target
  
  validates_presence_of :mile
  validates_numericality_of :mile

#  include ModifyRecordable

  after_save :update_owner_miles
  before_destroy :refuge_owner_account
  after_destroy :update_owner_miles
  def update_owner_miles
    owner = @owner_account_obj || self.owner_account
    owner && owner.update_miles
  end
  
  def refuge_owner_account
    @owner_account_obj = self.owner_account
  end
  
  #def after_destroy
  #  @owner_account_obj.update_miles if @owner_account_obj
  #end
end
