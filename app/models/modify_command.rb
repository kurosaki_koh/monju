class ModifyCommand < ActiveRecord::Base
  belongs_to :owner_account
  belongs_to :target , :polymorphic => true
  belongs_to :user
  serialize(:original , Hash)
  serialize( :change , Hash)

  #class SymbolWrapper
  #  def self.load(string)
  #    string.to_sym
  #  end
  #
  #  def self.dump(symbol)
  #    symbol.to_s
  #  end
  #end
  #
  serialize :modify_type

  def ModifyCommand::insert_command(new_rec,user = nil)
    com = ModifyCommand.new
    com.modify_type = :insert
    com.original = nil
    com.change = new_rec.attributes
    com.target = new_rec
    com.owner_account = new_rec.owner_account
    com.user_id = user
    com.save
    com
  end
  
  def ModifyCommand::update_command(orig_rec , mod_rec , user = nil)
    return if orig_rec.class == OwnerAccount
    orig = {}
    mod = {}
    for col in orig_rec.attributes.keys
      if orig_rec[col] != mod_rec[col]
        orig[col]=orig_rec[col]
        mod[col]=mod_rec[col]
      end
    end
    if orig.keys == []
      return true
    end
    com = ModifyCommand.new
    com.original = orig
    com.change = mod
    com.modify_type = :update
    com.target=orig_rec
    com.owner_account = orig_rec.owner_account
    com.user_id = user
    #    com.owner = orig_rec.owner
    com.save
    com
  end
  
  def ModifyCommand::delete_command(target_rec , user = nil)
    com = ModifyCommand.new
    com.original = target_rec.attributes
    com.change = nil
    com.modify_type = :delete
    com.target = target_rec
    com.owner_account = target_rec.owner_account
    com.user_id = user
    com.save!
  end

  def owner_name
    return '' if self.owner_account.nil? || self.owner_account.owner.nil?

    case self.owner_account.owner_type
    when 'Character'
      self.owner_account.owner.character_no + ':' + self.owner_account.owner.name
    when 'Nation'
      self.owner_account.owner.id.to_s + ':' + self.owner_account.owner.name
    else
      self.owner_account.owner.name
    end
  end
  
  def do_revert
    case modify_type
    when :insert
      com.change.destroy
    when :update
      com.target.update_attributes(com.original)
    when :delete
      target = target_type.constantize.new
      target.update_attributes(original)
      target.id= target_id
      target.save
    end
    com.target.save
  end
end
