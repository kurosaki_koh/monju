class OwnerAccountFactory
  def initialize(number_method)
    @number_method = number_method
  end

  def after_create(model)
    oc = model.owner_account = OwnerAccount.new if model.owner_account.nil?
    oc.name = model.name
    oc.number = model[@number_method]
    oc.miles = 0
    oc.save
  end
  
  def after_update(model)
    oc = model.owner_account
    if model.respond_to?(:type) && model.type != model.owner_account.owner_type
      model.owner_account.owner_type = model.type
    end
    oc.name = model.name if model.name_changed?
    if model.send(@number_method.to_s + '_changed?')
      oc.number = model.send(@number_method)
    end
    oc.save
  end
end