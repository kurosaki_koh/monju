# -*- encoding: utf-8 -*-

class ConvertSource < UnitRegistry
  belongs_to :convert_definition , include: :command_resource_definitions , foreign_key: 'object_definition_id' , dependent: :destroy

  alias :object_definition :convert_definition

  def source=(source)
    self.convert_definition ||= ConvertDefinition.new(object_type: 'コンバート定義')

    self.convert_definition.definition = source
  end

end
