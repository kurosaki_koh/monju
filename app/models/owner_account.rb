require 'i_object'

class OwnerAccount < ActiveRecord::Base
  attr_accessible :owner_id , :owner_type

  has_many :mile_changes
  has_many :cash_records , :order => :position
  has_many :i_objects
  has_many :weapons , :dependent => :destroy
  has_many :weapon_registries , :order => :position do
    def by_spec(spec_id)
      WeaponRegistry.find_all_by_owner_account_id_and_specification_id(proxy_association.owner.id , spec_id)
    end
  end
  has_many :job_idresses
  has_many :yggdrasills
  #todo : Rails 4.0 では lambda で渡す
  has_many :command_resources , include: :command_resource_definition
  has_many :unit_registries , include: :object_definition



  has_many :object_registries , :order => :position do
    def owned_objects
      results = []
      proxy_association.owner.object_registries.group_by(&:name).each do |name , object_group|
        hq_defined = object_group.find{|og|og.hq_hash}
        hq_hash = hq_defined.hq_hash if hq_defined
        owned_object = {:name => name , 
                        :number => object_group.sum{|o| o.number.to_i} ,
                        :owner_account => proxy_association.owner ,
                        :hq => hq_hash ,
                        :definition => object_group[0].object_definition ,
                        :hurigana => object_group[0].object_definition ? object_group[0].object_definition.hurigana.to_s : '',
                        :created_at => object_group.min{|a,b| a.created_at <=> b.created_at}.created_at ,
                        :registries => object_group #.sort_by{|item| [item.created_at , item.name]}
                        }
        results << owned_object
      end
      return results.sort_by{|obj| obj[:definition] ? obj[:definition].hurigana.to_s : ''}
    end
  end

  def num_of_property(name)
    group = self.object_registries.owned_objects.select{|ob|ob[:name]==name}[0]
    group ? group[:number] : 0
  end
  belongs_to :owner , :polymorphic => true
  
  def sum(column ,options = {})
    case column
    when :mile
      return mile_changes(true).map{|m|m.mile.to_i}.sum
    when :fund , :food , :resource , :fuel , :amusement , :num_npc , :hqfuel
      return cash_records(true).sum(column).to_i
    end
  end
  
  def nation
    case self.owner_type
    when 'Character'
      self.owner.nation
    when 'Nation'
      self.owner
    else
      self.owner
    end
  end
  
  def register_weapons(spec , num)
    Weapon.register(self, spec ,num)
  end
  
  def unregister_weapons(spec , num)
    Weapon.unregister(self, spec , num)
  end
  
#  def alter_weapons(name , num)
  def alter_weapons(params)
    if params[:number].nil? || (number = params[:number].to_i) == 0
      raise ArgumentError , 'arg \'num\' must not be zero or nil'
    end
    spec = Specification.find(params[:weapon_id])
#    if number > 0
#      register_weapons(spec , number)
#    else
#      unregister_weapons(spec , - number)
#    end
    wr = prepare_weapon_registry(spec,params)
    wr.save
  end
  
#  def move_weapons(dest , name , num)
  def move_weapons(params)
    dest = OwnerAccount.find(params[:move_dest])
    spec = Specification.find(params[:weapon_id])
    number = params[:number].to_i.abs
#    Weapon.move(self , dest , spec , number)
    wr = prepare_weapon_registry(spec,params, number  * -1)
    wr.save
    wr = dest.prepare_weapon_registry(spec,params , number )
    wr.save
  end
  
  def update_miles
    self.miles = sum(:mile)
    self.save
  end
                                         0
  after_create :update_miles

  def add_object_definition(odef , opts = {url: '' , note: ''})
    return unless odef

    if odef.is_command_resource?
      cr_attr  = odef.command_resource_definitions.first
      unless self.command_resources.where('command_resource_definitions.name'=> cr_attr[:name]).first
        cr = self.command_resources.build
        cr.command_resource_definition = cr_attr
        cr.authority_url = opts[:url]
        cr.update_available()
        cr.note = opts[:note]
        cr.save
      end
    elsif (odef.derivable?) && !self.yggdrasills.find_by_name(odef.name)
      root = self.create_yggdrasill_root
      ygg = self.yggdrasills.build
      ygg.name = odef.name
      ygg.idress_name = odef.name
      ygg.basis_url = opts[:url]
      ygg.note = opts[:note]
      ygg.save
      ygg.move_to_child_of(root)
    end

    if odef.countable?
      oreg = self.object_registries.build
      oreg.name = odef.name
      oreg.number = 1
      oreg.note = opts[:note] + "\n" + opts[:url]
      oreg.save
    end

  end

  def available_command_resources
    self.command_resources.select do |cr|
      not %w"ConvertDefinition AceUnitDefinition IncludeDefinition".include?(cr.command_resource_definition.object_definition.type)
    end
  end

  def query_converted_resources(unit_name = nil)
    query =  self.unit_registries
    query = query.where(name: unit_name) if unit_name
    unit_registries = query.where(available: true).to_a

    if unit_registries.size == 0 && unit_name
      return nil
    end

    self.command_resources.joins(command_resource_definition: :object_definition).where(ObjectDefinition.arel_table[:type].eq('ConvertDefinition'))
  end

  def converted_resources(unit_name = nil)
    if (query = query_converted_resources(unit_name))
      crs = query.to_a
    else
      return nil
    end

    unit_registries.collect{|cs|

      cs.object_definition.command_resource_definitions.collect{|crd|
        cr = crs.find{|cr| cr.command_resource_definition_id == crd.id}
        if cr
          crs.delete(cr)
          cr[:from] = cs.name
          cr[:from_id] = cs.id
          cr
        else
          nil
        end
      }.compact
    }.flatten
  end

  def extended_command_resources
    self.owner.extended_command_resources
  end

  protected

  def create_yggdrasill_root
    self.yggdrasills.root || begin
      root_node = Yggdrasill.create!(name: 'root', idress_name: 'root', owner_account_id: self.id)
      root_node.reload
      root_node
    end
  end


  def prepare_weapon_registry(spec,params , number = nil)
    number = number ? number : params[:number].to_i
    wr = WeaponRegistry.new
    wr.owner_account = self
    wr.specification = spec
    wr.note = params[:note]
    wr.reference_url = params[:reference_url]
    wr.event_no = params[:event_no]
    wr.event_name = params[:event_name]
    wr.turn = params[:turn]
    wr.name = spec.name
    wr.number = number
    wr
  end
end
