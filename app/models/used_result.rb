class UsedResult < ActiveRecord::Base
  attr_accessible :pc_name , :initial , :result , :note , :url , :character_id , :event_name
  belongs_to :character
  def initial
    return self[:initial].to_i
  end
  def result
    return self[:result].to_i
  end
  def owner_account
    self.character.owner_account
  end
  
end
