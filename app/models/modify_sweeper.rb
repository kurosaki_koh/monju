class ModifySweeper < ActionController::Caching::Sweeper
  observe MileChange , CashRecord , WeaponRegistry , ObjectRegistry , ::Result , UsedResult, Character , JobIdress , OwnerAccount

  def after_create(model)
    ModifyCommand::insert_command(model , controller.session[:user])  unless model.class == OwnerAccount
  end
  
  def before_update(model)
    return if controller.nil?
    ModifyCommand::update_command(model.class.find(model.id) , model , controller.session[:user])
  end
  
  def after_destroy(model)
    ModifyCommand::delete_command(model , controller.session[:user])
  end
  
end