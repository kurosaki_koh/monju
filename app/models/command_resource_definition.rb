# encoding : utf-8

class CommandResourceDefinition < ActiveRecord::Base
  belongs_to :object_definition
  has_many :command_resources , dependent: :delete_all
  has_many :owner_accounts , through: :command_resources , conditions: "command_resources.available = true"

  INFINITY = 2147483647 #max value of MySQL integer
  def to_form
    result = self.name.dup
    result += '：'+ self.power.to_s + '：＃' + self.tag
    result
  end

  def initialize(params)
    super
    if params[:power] == '自動成功'
      self.power  = INFINITY
    end
  end
  before_save do
    self.power = INFINITY if self.power == '自動成功'
  end


  def power
    (val = read_attribute(:power)) == INFINITY ? '自動成功' : val
  end
end