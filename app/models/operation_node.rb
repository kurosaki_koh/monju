class OperationNode < ActiveRecord::Base
  belongs_to :operation
  has_many :modifications ,
           :dependent => :destroy , 
           :order => :id ,
           :foreign_key => "operation_node_id"

  def fuel
    result = 0
    for c in children + modifications
      next unless c.enable
      val = c.fuel
      result += (val ? val : 0 )
    end
    result
  end
  
  def cost(kind)
    result = 0
    children.each{|c| result += c.cost(kind)}
    result
  end
end
 
class ::TroopNode < OperationNode
  include Calcuratable
  
  def rd(col)
    val = 0
    if Job::Abilities.include? col
      for c in self.children
        val += c.rd(col)
      end
      return val
    elsif Job::BattleAbilities.include? col
      return nil
    end
  end

  def ability(col)
    if Job::Abilities.include? col
      (Math::log(self.rd(col)) / Math::log(1.5)).round
    else
      culc_ability(col).round
    end
  end
  
  
end

class ::SoldierNode < OperationNode
  belongs_to :character
  belongs_to :nation
    
  belongs_to :job_idress ,
             :class_name => "JobIdress" , 
             :foreign_key => "archtype_id"

  include Calcuratable
  
  KindsOfActsAs = [:soldier , :dog_soldier , :pilot , :copilot ,:dog_copilot]
  
  def ability(col)
    return nil if job_idress.nil?
    val = 0
    if Job::Abilities.include? col
       val = self.job_idress.ability(col)
       for c in modifications
         val += c[col] if c.enable
       end
       return val
    else
      return culc_ability(col)
    end
  end
  
  def cost(kind)
    case kind
    when :food
      job_idress.food_cost
    when :fund
      job_idress.fund_cost
    else
      0
    end  
  end
end

class NpcSoldier < SoldierNode
  def cost(kind)
    result = case kind
    when :food
      if job_idress.nil?
        0.5
      else
        0.5 * job_idress.food_cost 
      end
    else
      super
    end
  end
end

class IDGear < OperationNode
  belongs_to :spec ,
             :class_name => "IdSpec" ,
             :foreign_key => "archtype_id"
  
  belongs_to :nation
  include Calcuratable

  def ability(col)
    val = 0
    if Job::Abilities.include? col
       val = self.spec.ability(col)
       val += children.collect{|c| c[col]}.max
       return val
    else
      culc_ability(col) + self.spec.ability(col)
    end
  end
  
end
