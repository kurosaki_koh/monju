# encoding : utf-8

class Yggdrasill < ActiveRecord::Base
  attr_accessible :idress_name, :name, :owner_account_id, :page_url, :basis_url, :hq, :hq_basis_url,
                  :i_definition, :parent_id, :lft, :rgt, :depth, :node_type, :note, :extra
  acts_as_nested_set

  belongs_to :owner_account
  has_many :command_resources, dependent: :delete_all  , as: :source
  include ::CommandResourceParser

  serialize :hq
  serialize :extra

  validates_presence_of :name
  validates_presence_of :idress_name
#  validate :has_definition?
#  validates_uniqueness_of :idress_name , scope: :owner_account_id  #ToDo: 現状のデータではおそらくムリ。データの正規化が必要。

  before_validation :set_idress_name_from_definition

  def definition
    get_definition
  end

  def has_definition?
    if self.name != 'root'
      errors.add(:i_definition, "アイドレス定義が無効です。") if self.get_definition.blank?
      # if self.is_idress3? && self.parsed_command_resources[0][:name] != self.idress_name
      #   errors.add(:i_definition, "アイドレス名が定義と一致しません。")
      # end
    end

  end

  def set_idress_name_from_definition
    check_change
    if self.idress_name.blank? && self.definition_type == :unique
      self.idress_name = parsed_command_resources[0][:name]
    end
    true
  end

  protected :set_idress_name_from_definition

  def check_change
    if self.i_definition_changed? || self.idress_name_changed?
      @definition = nil
      @definition_type = nil
      @_odef = nil
    end
  end

  def get_definition
    check_change
    @definition ||= if !self.i_definition.blank? #todo : ユニーク定義もObjectDefinition に持ってく
                      @definition_type ||= :unique
                      self.i_definition
                    elsif (idef = IDefinition.find_by_name(self.idress_name))
                      @definition_type ||= :i2
                      idef.definition
                    elsif (@_odef = ObjectDefinition.find_by_name(self.idress_name))
                      @definition_type ||= @_odef.is_idress3? ? :i3 : :i2
                      @_odef.definition
                    else
                      @definition_type ||= nil
                      nil
                    end
  end

  def definition_type
    check_change
    unless @definition_type
      self.get_definition
    end
    @definition_type
  end

  def register_command_resources
    check_change
    if self.is_available?
      for crd in @_odef.command_resource_definitions
        if (cr = self.command_resources.size > 0 )
          self.command_resources.update_all(available: true)
        else
          command_resource = CommandResource.new
          command_resource.command_resource_definition = crd
          command_resource.owner_account = self.owner_account
          command_resource.source = self
          command_resource.note = "＜#{self.name}＞のコマンドリソース\n"
          command_resource.authority_url = self.basis_url
          command_resource.available = true
          command_resource.save
        end
      end
    else
      self.command_resources.update_all(available: false)
    end
  end

  def rebuild_command_resources
    CommandResource.transaction do
#      self.command_resources.delete_all
      self.register_command_resources
    end
  end

  def is_available?
    case self.definition_type
      when :unique
        parsed_command_resources.size > 0
      when :i3
        if @_odef.countable?
          self.owner_account.num_of_property(@_odef.name) > 0
        else
          true
        end
      else
        false
    end
  end

  after_create do
    if self.is_available?
      CommandResource.transaction do
        self.register_command_resources
      end
    end
  end

  before_update if: :idress_name_changed? do
    self.command_resources.delete_all
  end
  after_update do
    self.rebuild_command_resources if self.is_available?
  end


end
