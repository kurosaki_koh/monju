# -*- encoding: utf-8 -*-

class Character < ActiveRecord::Base
  attr_accessible :name , :originator , :nation_id , :job_idress_id , :character_no , :personal_idress_id ,
                  :qualification , :phy , :str , :dur , :chr , :agi , :dex  , :sen , :edu , :luc ,
                  :intention , :player_name , :ar , :status , :race_type , :note , :gender


  include Calcuratable
  belongs_to :nation
  belongs_to :job_idress
  
  belongs_to :personal_idress, 
  :class_name => "JobIdress",
  :foreign_key => "personal_idress_id"
  
  has_many :results , :order => "position"
  has_many :used_results ,:order => "id"
  has_many :idress_acquirements
  has_many :acquired_idresses , :through => :idress_acquirements

  has_one :owner_account , :as => :owner
  has_many :bonds

  owner_factory = OwnerAccountFactory.new(:character_no)
  after_create owner_factory
  after_update owner_factory
  
  def ability(col) ## override from Culcuratable
    if Job::Abilities.include? col
      val = self.job_idress[col]
      val +=self.personal_idress[col] if self.personal_idress
      val
    else
      nil
    end
  end
  
  def sum_originator
    result = sum_proper_originator
#    result += personal_idress.originator.to_i if personal_idress
    result
  end
  
  def sum_proper_originator
    sum_results + sum_initials + sum_used_results
  end
  
  def sum_results
    result = 0
    for r in results
      result += r.originator.to_i unless r.not_yet
    end
    result
  end
  
  def sum_initials
    result = 0
    for ur in used_results
      result += ur.initial.to_i
    end
    result
  end
  
  def sum_used_results
    result = 0
    for ur in used_results
      result +=  ur.result.to_i
    end
    result
  end
  
  def sum_ability(col)
    if Job::Abilities.include? col
      val = 0
      val = self.job_idress[col] if self.job_idress
      val +=self.personal_idress[col] if self.personal_idress
      val += self.attributes[col.to_s] if self.attributes[col.to_s]
      val
    else
      nil
    end
  end
   before_update :set_nil_if_zero
  
  def set_nil_if_zero
    [:phy , :str , :dur , :chr , :agi , :dex , :sen , :edu , :luc , :ar].each{|a|
      self[a]= nil if self.send(a.to_s)==0
    }
    return true
  end
  
  def create_owner_account
    unless owner_account 
      account = OwnerAccount.new
      account.character = self
      account.save
    end
  end
  
  def inspect_idresses
    self.job_idress ? self.job_idress.inspect_idresses : ''
  end

  def available_command_resources
    CommandResource.nested_availabilities(self.owner_account.id , self.nation.owner_account.id)
  end

  def extended_command_resources
    self.nation.owner_account.command_resources.extendable.collect{|cr|cr[:from] = self.nation.name ; cr}
  end

  def after_update
    owner_account.name = self.name self.owner_account.name != self.name
    owner_account.number = self.character_no if self.owner_account.number != self.character_no
    owner_account.save
  end

  def is_noble?
    !! (self.qualification =~ /藩王.級/)
  end
end
