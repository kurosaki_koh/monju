$ ->
  CommandResourcePattern = /・?(.+?)(（(.+?)）)?：(.+?)：＃(.+)/
  parseMultibyteToInt = (str) ->
    str.replace /[０１２３４５６７８９]/g , (s) ->
      b = "０１２３４５６７８９".indexOf(s)
      if(b!=-1)
        b
      else
        s


  $('#definition').bind 'keyup' ,  ->
    text =$(this).val()
    if(text == '' )
      return
    else if CommandResourcePattern.test text
      $(this).css('background-color' , "#ffffff")
      result = text.match(CommandResourcePattern)
      $('#command_resource_name').val(result[1])
      $('#command_resource_tag').val(result[5])
      $('#command_resource_power').val parseMultibyteToInt(result[4])
      $('#command_resource_category').val(result[3])

    else
      $(this).css('background-color' , "#ffcccc")
      return