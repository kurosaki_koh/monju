Effect.SlideRightIntoView=function(_1){
_1=$(_1);
Element.cleanWhitespace(_1);
var _2=_1.firstChild.style.right;
var _3=Element.getDimensions(_1);
return new Effect.Scale(_1,100,Object.extend({scaleContent:false,scaleY:false,scaleFrom:0,scaleMode:{originalHeight:_3.height,originalWidth:_3.width},restoreAfterFinish:true,afterSetup:function(_4){
Element.makePositioned(_4.element.firstChild);
if(window.opera){
_4.element.firstChild.style.left="";
}
Element.makeClipping(_4.element);
_1.style.width="0";
Element.show(_1);
},afterUpdateInternal:function(_5){
_5.element.firstChild.style.right=(_5.dims[1]-_5.element.clientWidth)+"px";
},afterFinishInternal:function(_6){
Element.undoClipping(_6.element);
Element.undoPositioned(_6.element.firstChild);
_6.element.firstChild.style.right=_2;
}},arguments[1]||{}));
};
Effect.SlideRightOutOfView=function(_7){
_7=$(_7);
Element.cleanWhitespace(_7);
var _8=_7.firstChild.style.right;
return new Effect.Scale(_7,0,Object.extend({scaleContent:false,scaleY:false,scaleMode:"box",scaleFrom:100,restoreAfterFinish:true,beforeStartInternal:function(_9){
Element.makePositioned(_9.element.firstChild);
if(window.opera){
_9.element.firstChild.style.left="";
}
Element.makeClipping(_9.element);
Element.show(_7);
},afterUpdateInternal:function(_a){
_a.element.firstChild.style.right=(_a.dims[1]-_a.element.clientWidth)+"px";
},afterFinishInternal:function(_b){
Element.hide(_b.element);
Element.undoClipping(_b.element);
Element.undoPositioned(_b.element.firstChild);
_b.element.firstChild.style.right=_8;
}},arguments[1]||{}));
};

