var SelectableTable=Class.create();
SelectableTable.classNames={table:"selectableTable_table",tr:"selectableTable_tr",trHover:"selectableTable_trHover",trSelected:"selectableTable_trSelected"};
SelectableTable.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
var _2={arrayDefaultData:[],flagAllowUnselect:true,flagInitialAllowMultiple:false,flagKeypressAvailable:false,flagKeypressDeleteAvailable:false,flagKeypressInsertAvailable:false,functionPostAdd:Prototype.emptyFunction,functionPostDelete:Prototype.emptyFunction,functionPostPressLeft:Prototype.emptyFunction,functionPostPressRight:Prototype.emptyFunction,functionPostSelect:Prototype.emptyFunction,functionPostUnselect:Prototype.emptyFunction,functionPreAdd:function(){
return true;
},functionPreDelete:function(){
return true;
},functionSubmit:Prototype.emptyFunction,initialSelected:null,prefixTrId:"selectable_table_",prefixCSS:"custom_"};
this.options=Object.extend(_2,arguments[1]||{});
this.classNames=new CssUtil([SelectableTable.classNames,CssUtil.appendPrefix(this.options.prefixCSS,SelectableTable.classNames)]);
this.documentListener=this.eventKeypress.bindAsEventListener(this);
this.flagAllowMultiple=this.options.flagInitialAllowMultiple;
this.flagAvailable=true;
this.focused=null;
this.lastSelected=null;
this.newNumber=1;
this.selected=new Object();
this.build();
if(arguments[2]){
this.selectEffect(this.buildTrId(arguments[2]));
}
Element.setStyle(this.element,{visibility:"visible"});
},add:function(){
if(!this.flagAvailable){
return;
}
if(!this.options.functionPreAdd(this)){
return;
}
if(arguments[0]==null){
arguments[0]=this.options.arrayDefaultData;
}
if(typeof (arguments[0])!="string"){
arguments=arguments[0];
}
if(arguments[0]==null){
return;
}
var _3,_4;
_3=document.createElement("tr");
_3.id="new_"+this.newNumber;
this.buildTr(_3);
for(var i=0;i<arguments.length;i++){
_4=document.createElement("td");
_4.innerHTML=arguments[i];
_3.appendChild(_4);
}
this.element.tBodies[0].appendChild(_3);
this.newNumber++;
this.options.functionPostAdd(this);
},build:function(){
var _6=this.element.tBodies[0].rows;
this.classNames.addClassNames(this.element,"table");
Event.observe(document,"keypress",this.documentListener);
for(var i=0;i<_6.length;i++){
this.buildTr(_6[i]);
}
var _8=this.options.initialSelected;
if(_8){
this.selectEffect(this.buildTrId(_8));
}
},buildTr:function(_9){
_9.id=this.buildTrId(_9.id);
this.classNames.addClassNames(_9,"tr");
Event.observe(_9,"click",this.eventClick.bindAsEventListener(this));
Event.observe(_9,"dblclick",this.eventDoubleClick.bindAsEventListener(this));
Event.observe(_9,"mouseout",this.eventFocusOut.bindAsEventListener(this));
Event.observe(_9,"mouseover",this.eventFocusOver.bindAsEventListener(this));
},buildTrId:function(_a){
return this.options.prefixTrId+_a;
},deleteAll:function(){
if(!this.flagAvailable){
return;
}
if(!this.options.functionPreDelete(this)){
return;
}
for(var _b in this.selected){
this.element.tBodies[0].removeChild($(_b));
delete this.selected[_b];
}
this.focused=null;
this.options.functionPostDelete(this);
},eventClick:function(_c){
if(!this.flagAvailable){
return;
}
if(_c.shiftKey){
this.selectOrUnselectRange(Event.findElement(_c,"tr").id);
}else{
this.selectOrUnselect(Event.findElement(_c,"tr").id,_c.ctrlKey);
}
},eventDoubleClick:function(_d){
if(!this.flagAvailable){
return;
}
if(this.flagAllowMultiple){
this.select(Event.findElement(_d,"tr").id,false);
this.submit();
}
},eventFocusOut:function(_e){
if(!this.flagAvailable){
return;
}
this.focusOff();
},eventFocusOver:function(_f){
if(!this.flagAvailable){
return;
}
this.focusOn(Event.findElement(_f,"tr").id);
Event.findElement(_f,"tr").focus();
},eventKeypress:function(_10){
if(!this.flagAvailable){
return;
}
if(!this.options.flagKeypressAvailable){
return;
}
switch(_10.keyCode){
case 13:
if(_10.shiftKey){
this.selectOrUnselectRange(this.focused);
}else{
this.selectOrUnselect(this.focused,_10.ctrlKey);
}
break;
case 37:
this.options.functionPostPressLeft(this);
break;
case 38:
this.focusMove("up");
break;
case 39:
this.options.functionPostPressRight(this);
break;
case 40:
this.focusMove("down");
break;
case 45:
if(this.options.flagKeypressInsertAvailable){
this.add();
}
break;
case 46:
if(this.options.flagKeypressDeleteAvailable){
this.deleteAll();
}
break;
}
},focusMove:function(_11){
if(!this.flagAvailable){
return;
}
if(this.focused==null){
this.focusOn(this.element.tBodies[0].rows[0].id);
}else{
var _12=$(this.focused).rowIndex;
var _13,_14;
switch(_11){
case "down":
_13=1;
_14=this.isBottom(_12);
break;
case "up":
_13=-1;
_14=this.isTop(_12);
break;
}
if(!_14){
this.focusOn(this.element.rows[_12+_13].id);
}
}
},focusOff:function(){
if(!this.flagAvailable){
return;
}
if(this.focused!=null){
var _15=$(this.focused);
this.classNames.removeClassNames(_15,"trHover");
this.focused=null;
}
},focusOn:function(_16){
if(!this.flagAvailable){
return;
}
if($(_16)!=null){
this.focusOff();
this.classNames.addClassNames($(_16),"trHover");
this.focused=_16;
}
},getSelected:function(){
var _17=new Array();
for(var _18 in this.selected){
_17.push(_18.replace(this.options.prefixTrId,""));
}
return _17;
},getSelectedElement:function(id){
var _1a=this.options.prefixTrId+id;
return $(_1a);
},isBottom:function(_1b){
return (_1b==this.element.rows.length-1)?true:false;
},isTop:function(_1c){
return (_1c==this.element.tBodies[0].rows[0].rowIndex)?true:false;
},makeAvailable:function(){
this.flagAvailable=true;
},makeMultiple:function(){
this.flagAllowMultiple=true;
},makeSingular:function(){
this.flagAllowMultiple=false;
this.unselectAll();
},makeUnavailable:function(){
this.flagAvailable=false;
},removeEventFromDocument:function(){
Event.stopObserving(document,"keypress",this.documentListener);
},select:function(_1d,_1e){
if(!this.flagAvailable){
return;
}
this.selectEffect(_1d,_1e);
this.lastSelected=_1d;
this.options.functionPostSelect(this);
if(!this.flagAllowMultiple){
this.submit();
}
},selectAll:function(){
if(!this.flagAvailable){
return;
}
if(!this.flagAllowMultiple){
return;
}
this.selected=new Object();
var _1f=this.element.tBodies[0].rows;
for(var i=0;i<_1f.length;i++){
this.select(_1f[i].id,true);
}
},selectEffect:function(_21,_22){
if($(_21)){
if(!this.flagAllowMultiple||!_22){
this.unselectAll();
}
this.classNames.addClassNames($(_21),"trSelected");
this.selected[_21]=true;
}
},selectOrUnselect:function(_23,_24){
if(!this.flagAvailable){
return;
}
if(_23==null){
return;
}
if(_24&&this.selected[_23]){
if(!this.flagAllowMultiple&&!this.options.flagAllowUnselect){
return;
}
this.unselect(_23);
}else{
this.select(_23,_24);
}
},selectOrUnselectRange:function(_25){
if(!this.flagAvailable){
return;
}
if(_25==null){
return;
}
if(this.lastSelected==null||this.lastSelected==_25){
this.selectOrUnselect(_25);
return;
}
var _26=false;
var _27=this.element.tBodies[0].rows;
var _28=this.lastSelected;
for(var i=0;i<_27.length;i++){
if(_27[i].id==_25||_27[i].id==_28){
_26=(_26)?false:true;
}else{
if(!_26){
continue;
}
}
if(this.selected[_28]){
this.select(_27[i].id,true);
}else{
this.unselect(_27[i].id);
}
}
},submit:function(_2a){
if(!this.flagAvailable){
return;
}
var _2b=this.getSelected();
this.options.functionSubmit(_2b[0]);
},unselect:function(_2c){
if(!this.flagAvailable){
return;
}
this.classNames.removeClassNames($(_2c),"trSelected");
delete this.selected[_2c];
this.lastSelected=_2c;
this.options.functionPostUnselect(this);
},unselectAll:function(){
if(!this.flagAvailable){
return;
}
var _2d=this.element.tBodies[0].rows;
for(var i=0;i<_2d.length;i++){
this.unselect(_2d[i].id);
}
}};
var SelectableTableManager=Class.create();
SelectableTableManager.prototype={initialize:function(){
this.active=null,this.list={};
},activate:function(key){
this.stop();
if(this.list[key]){
this.list[key].makeAvailable();
this.active=this.list[key];
}else{
this.active=null;
}
},push:function(key,_31){
this.list[key]=_31;
},start:function(){
if(this.active){
this.active.makeAvailable();
}
},stop:function(){
$H(this.list).each(function(el){
if(el[1]){
el[1].makeUnavailable();
}
});
}};

