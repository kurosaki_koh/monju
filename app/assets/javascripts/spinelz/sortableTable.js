var SortableTable=Class.create();
SortableTable.classNames={header:"sortableTable_header",title:"sortableTable_title",empty:"sortableTable_empty",down:"sortableTable_down",up:"sortableTable_up",mark:"sortableTable_mark",thead:"sortableTable_thead",tbody:"sortableTable_tbody"};
SortableTable.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
var _2=Object.extend({sortType:false,cssPrefix:"custom_"},arguments[1]||{});
var _3=CssUtil.appendPrefix(_2.cssPrefix,SortableTable.classNames);
this.classNames=new CssUtil([SortableTable.classNames,_3]);
this.sortType=_2.sortType;
this.currentOrder="default";
this.defaultOrder=new Array();
for(var i=1;i<this.element.rows.length;i++){
this.defaultOrder[i-1]=this.element.rows[i];
}
this.build();
Element.setStyle(this.element,{visibility:"visible"});
},build:function(){
thead=this.element.tHead;
this.classNames.addClassNames(thead,"thead");
tbody=thead.nextSibling;
while((tbody.nodeType!=1)||(tbody.tagName.toLowerCase()!="tbody")){
tbody=tbody.nextSibling;
}
this.classNames.addClassNames(tbody,"tbody");
var _5=this.element.rows[0];
if(!_5){
return;
}
for(var i=0;i<_5.cells.length;i++){
var _7=_5.cells[i];
_7.style.cursor="pointer";
Element.cleanWhitespace(_7);
var _8=Builder.node("DIV",$A(_7.childNodes));
this.classNames.addClassNames(_8,"title");
var _9=Builder.node("DIV");
this.classNames.addClassNames(_9,"mark");
this.classNames.addClassNames(_9,"empty");
var _a=Builder.node("DIV",[_8,_9]);
this.classNames.addClassNames(_a,"header");
_7.appendChild(_a);
var _b=_8.offsetWidth;
var _c=_9.offsetWidth;
_8.style.width=(_b+_c)+"px";
Event.observe(_5.cells[i],"click",this.sortTable.bindAsEventListener(this));
}
},sortTable:function(_d){
var _e=Event.element(_d);
if(_e.tagName.toUpperCase()!="TD"&&_e.tagName.toUpperCase()!="TH"){
_e=Element.getParentByTagName(["TD","TH"],_e);
}
var _f=_e.cellIndex;
if(this.targetColumn!=_f){
this.currentOrder="default";
}
this.targetColumn=_f;
var _10=new Array();
for(var i=1;i<this.element.rows.length;i++){
_10[i-1]=this.element.rows[i];
}
if(_10.length<1){
return;
}
if(this.currentOrder=="default"){
_10.sort(this.getSortFunc());
this.currentOrder="asc";
}else{
if(this.currentOrder=="asc"){
_10=_10.reverse();
this.currentOrder="desc";
}else{
if(this.currentOrder=="desc"){
_10=this.defaultOrder;
this.currentOrder="default";
}
}
}
for(var i=0;i<_10.length;i++){
this.element.tBodies[0].appendChild(_10[i]);
}
this.mark(_e);
},mark:function(_12){
var _13=document.getElementsByClassName(SortableTable.classNames.mark,this.element);
var _14=document.getElementsByClassName(SortableTable.classNames.mark,_12)[0];
for(var i=0;i<_13.length;i++){
var _16=_13[i].parentNode;
var _17=document.getElementsByClassName(SortableTable.classNames.title,_16)[0];
var _18=_17.offsetWidth;
if(_14==_13[i]){
var _19=_14.offsetWidth;
if(this.currentOrder=="asc"){
this.classNames.addClassNames(_14,"down");
this.classNames.removeClassNames(_14,"empty");
if(!document.all){
_17.style.width=(_18-_19)+"px";
}
}else{
if(this.currentOrder=="desc"){
this.classNames.addClassNames(_14,"up");
this.classNames.removeClassNames(_14,"down");
}else{
if(this.currentOrder=="default"){
this.classNames.addClassNames(_14,"empty");
this.classNames.removeClassNames(_14,"up");
if(!document.all){
_17.style.width=(_18+_19)+"px";
}
}
}
}
}else{
if(Element.hasClassName(_13[i],SortableTable.classNames.empty)){
continue;
}else{
if(Element.hasClassName(_13[i],SortableTable.classNames.down)){
this.classNames.removeClassNames(_13[i],"down");
}else{
if(Element.hasClassName(_13[i],SortableTable.classNames.up)){
this.classNames.removeClassNames(_13[i],"up");
}
}
}
var _19=_14.offsetWidth;
this.classNames.addClassNames(_13[i],"empty");
if(!document.all){
_17.style.width=(_18+_19)+"px";
}
}
}
},getSortFunc:function(){
if(!this.sortType||!this.sortType[this.targetColumn]){
return SortFunction.string(this);
}
var _1a=this.getSortType();
if(!this.sortType||!_1a){
return SortFunction.string(this);
}else{
if(_1a==SortFunction.numeric){
return SortFunction.number(this);
}
}
return SortFunction.date(this);
},getSortType:function(){
return this.sortType[this.targetColumn];
}};
var SortFunction=Class.create();
SortFunction={string:"string",numeric:"numeric",mmddyyyy:"mmddyyyy",mmddyy:"mmddyy",yyyymmdd:"yyyymmdd",yymmdd:"yymmdd",ddmmyyyy:"ddmmyyyy",ddmmyy:"ddmmyy",date:function(_1b){
return function(fst,snd){
var _1e=Element.collectTextNodes(fst.cells[_1b.targetColumn]);
var _1f=Element.collectTextNodes(snd.cells[_1b.targetColumn]);
var _20,_21;
var _20=SortFunction.getDateString(_1e,_1b.getSortType());
var _21=SortFunction.getDateString(_1f,_1b.getSortType());
if(_20==_21){
return 0;
}
if(_20<_21){
return -1;
}
return 1;
};
},number:function(_22){
return function(fst,snd){
var _25=parseFloat(Element.collectTextNodes(fst.cells[_22.targetColumn]));
if(isNaN(_25)){
_25=0;
}
var _26=parseFloat(Element.collectTextNodes(snd.cells[_22.targetColumn]));
if(isNaN(_26)){
_26=0;
}
return _25-_26;
};
},string:function(_27){
return function(fst,snd){
var _2a=Element.collectTextNodes(fst.cells[_27.targetColumn]);
var _2b=Element.collectTextNodes(snd.cells[_27.targetColumn]);
if(_2a==_2b){
return 0;
}
if(_2a<_2b){
return -1;
}
return 1;
};
},getDateString:function(_2c,_2d){
var _2e=_2c.split("/");
if((_2d==SortFunction.mmddyyyy)||(_2d==SortFunction.mmddyy)){
var _2f=new Array();
_2f.push(_2e[2]);
_2f.push(_2e[0]);
_2f.push(_2e[1]);
}else{
if((_2d==SortFunction.ddmmyyyy)||(_2d==SortFunction.ddmmyy)){
var _2f=new Array();
_2f.push(_2e[2]);
_2f.push(_2e[1]);
_2f.push(_2e[0]);
}else{
_2f=_2e;
}
}
return _2f.join();
}};

