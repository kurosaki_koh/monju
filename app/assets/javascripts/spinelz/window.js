var Window=Class.create();
Window.className={window:"window",header:"window_header",headerLeft:"window_headerLeft",headerMiddle:"window_headerMiddle",headerRight:"window_headerRight",buttonHolder:"window_buttonHolder",closeButton:"window_closeButton",maxButton:"window_maxButton",minButton:"window_minButton",body:"window_body",bodyLeft:"window_bodyLeft",bodyMiddle:"window_bodyMiddle",bodyRight:"window_bodyRight",bottom:"window_bottom",bottomLeft:"window_bottomLeft",bottomMiddle:"window_bottomMiddle",bottomRight:"window_bottomRight"};
Window.prototype={initialize:function(_1){
var _2=Object.extend({className:Window.className.window,width:300,height:300,minWidth:200,minHeight:40,drag:true,resize:true,resizeX:true,resizeY:true,modal:false,closeButton:true,maxButton:true,minButton:true,cssPrefix:"custom_",restriction:false,endDrag:Prototype.emptyFunction,endResize:Prototype.emptyFunction,addButton:Prototype.emptyFunction,preMaximize:function(){
return true;
},preMinimize:function(){
return true;
},preRevertMaximize:function(){
return true;
},preRevertMinimize:function(){
return true;
},preClose:function(){
return true;
},endMaximize:Prototype.emptyFunction,endMinimize:Prototype.emptyFunction,endRevertMaximize:Prototype.emptyFunction,endRevertMinimize:Prototype.emptyFunction,endClose:Prototype.emptyFunction,dragOptions:{},appendToBody:false},arguments[1]||{});
var _3=CssUtil.appendPrefix(_2.cssPrefix,Window.className);
this.classNames=new CssUtil([Window.className,_3]);
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
this.options=_2;
this.element.className=this.options.className;
this.header=null;
this.windowBody=null;
this.bottom=null;
this.elementId=this.element.id;
this.dragHandleId=this.elementId+"_dragHandle";
this.maxZindex=-1;
this.minFlag=false;
this.maxFlag=false;
this.currentPos=[0,0];
this.currentSize=[0,0];
this.buildWindow();
this.cover=new IECover(this.element,{padding:10});
Element.makePositioned(_1);
Element.hide(this.element);
Element.setStyle(this.element,{visibility:"visible"});
if(this.options.appendToBody){
this.appendToBody.callAfterLoading(this);
}
},buildWindow:function(){
Element.cleanWhitespace(this.element);
with(this.element.style){
width=this.options.width+"px";
height=this.options.height+"px";
}
var _4=this.element.childNodes[0];
var _5=this.element.childNodes[1];
this.buildHeader(_4);
this.buildBody(_5);
this.buildBottom();
var _6={height:this.options.height};
this.setBodyHeight(_6);
if(this.options.drag){
this.createDraggble();
}
if(this.options.resize){
this.enableResizing();
}
},buildHeader:function(_7){
var _8=Builder.node("div");
this.classNames.addClassNames(_8,"headerLeft");
var _9=Builder.node("div",{id:this.dragHandleId});
this.classNames.addClassNames(_9,"headerMiddle");
var _a=Builder.node("div");
this.classNames.addClassNames(_a,"headerRight");
var _b=Builder.node("div");
this.classNames.addClassNames(_b,"buttonHolder");
_9.appendChild(_7);
var _c=[_8,_9,_b,_a];
this.header=Builder.node("div",_c);
this.classNames.addClassNames(this.header,"header");
this.element.appendChild(this.header);
if(this.options.closeButton){
var _d=Builder.node("div",{id:this.element.id.appendSuffix("closeButton")});
this.classNames.addClassNames(_d,"closeButton");
_b.appendChild(_d);
Event.observe(_d,"click",this.close.bindAsEventListener(this));
}
if(this.options.maxButton){
var _e=Builder.node("div",{id:this.element.id.appendSuffix("maxButton")});
this.classNames.addClassNames(_e,"maxButton");
_b.appendChild(_e);
Event.observe(_e,"click",this.maximize.bindAsEventListener(this));
}
if(this.options.minButton){
var _f=Builder.node("div",{id:this.element.id.appendSuffix("minButton")});
this.classNames.addClassNames(_f,"minButton");
_b.appendChild(_f);
Event.observe(_f,"click",this.minimize.bindAsEventListener(this));
}
if(this.options.addButton){
var _10=this.options.addButton;
if(_10.constructor==Function){
_10(_b);
}else{
if(_10.constructor==Array){
var _11=this;
var _12=_b.firstChild;
_10.each(function(b){
var _14=Builder.node("div",{id:b.id,className:b.className});
Event.observe(_14,"click",b.onclick.bindAsEventListener(_11));
if(b.first&&_12){
_b.insertBefore(_14,_12);
}else{
_b.appendChild(_14);
}
});
}
}
}
},buildBody:function(_15){
var _16=Builder.node("div",{className:Window.className.bodyLeft});
this.classNames.addClassNames(_16,"bodyLeft");
var _17=Builder.node("div");
this.classNames.addClassNames(_17,"bodyMiddle");
_17.appendChild(_15);
var _18=Builder.node("div");
this.classNames.addClassNames(_18,"bodyRight");
var _19=[_18,_16,_17];
this.windowBody=Builder.node("div",_19);
this.classNames.addClassNames(this.windowBody,"body");
this.element.appendChild(this.windowBody);
},buildBottom:function(){
var _1a=Builder.node("div");
this.classNames.addClassNames(_1a,"bottomLeft");
var _1b=Builder.node("div");
this.classNames.addClassNames(_1b,"bottomMiddle");
var _1c=Builder.node("div");
this.classNames.addClassNames(_1c,"bottomRight");
var _1d=[_1a,_1b,_1c];
this.bottom=Builder.node("div",_1d);
this.classNames.addClassNames(this.bottom,"bottom");
this.element.appendChild(this.bottom);
},createDraggble:function(){
var _1e=this;
var _1f=Object.extend({handle:this.dragHandleId,starteffect:Prototype.emptyFunction,endeffect:Prototype.emptyFunction,endDrag:this.options.endDrag,scroll:window},this.options.dragOptions);
if(this.options.restriction){
_1f.snap=function(x,y){
function constrain(n,_23,_24){
if(n>_24){
return _24;
}else{
if(n<_23){
return _23;
}else{
return n;
}
}
}
var _25=Element.getDimensions(_1e.element);
var _26=Element.getDimensions(_1e.element.parentNode);
if(Element.getStyle(_1e.element.parentNode,"position")=="static"){
var _27=Position.positionedOffset(_1e.element.parentNode);
var _28=_27[0];
var _29=_27[1];
return [constrain(x,_28,_28+_26.width-_25.width),constrain(y,_29,_29+_26.height-_25.height)];
}else{
return [constrain(x,0,_26.width-_25.width),constrain(y,0,_26.height-_25.height)];
}
};
}else{
var p=Position.cumulativeOffset(Position.offsetParent(this.element));
_1f.snap=function(x,y){
return [((x+p[0])>=0)?x:0-p[0],((y+p[1])>=0)?y:0-p[1]];
};
}
new DraggableWindowEx(this.element,_1f);
},setWindowZindex:function(_2d){
_2d=this.getZindex(_2d);
this.element.style.zIndex=_2d;
},getZindex:function(_2e){
return ZindexManager.getIndex(_2e);
},open:function(_2f){
this.opening=true;
if(this.options.modal){
Modal.mask(this.element,{zIndex:_2f});
}else{
this.setWindowZindex(_2f);
}
Element.show(this.element);
this.cover.resetSize();
this.opening=false;
if(this.shouldClose){
this.close();
this.shouldClose=false;
}
},close:function(){
if(this.opening){
this.shouldClose=true;
}
if(!this.options.preClose(this)){
return;
}
this.element.style.zIndex=-1;
this.maxZindex=-1;
try{
Element.hide(this.element);
}
catch(e){
}
if(this.options.modal){
Modal.unmask();
}
this.options.endClose(this);
if(this.opening){
this.shouldClose=true;
}
},minimize:function(_30){
if(this.minFlag){
if(!this.options.preRevertMinimize(this)){
return;
}
Element.toggle(this.windowBody);
if(this.maxFlag){
this.minFlag=false;
this.setMax();
}else{
var _31={height:this.currentSize[1]};
this.setBodyHeight(_31);
this.element.style.width=this.currentSize[0];
this.element.style.height=this.currentSize[1];
this.element.style.left=this.currentPos[0];
this.element.style.top=this.currentPos[1];
this.maxFlag=false;
this.minFlag=false;
this.options.endRevertMinimize(this);
}
}else{
if(!this.options.preMinimize(this)){
return;
}
Element.toggle(this.windowBody);
if(!this.maxFlag){
this.currentPos=[Element.getStyle(this.element,"left"),Element.getStyle(this.element,"top")];
this.currentSize=[Element.getStyle(this.element,"width"),Element.getStyle(this.element,"height")];
}
this.setMin();
this.minFlag=true;
this.options.endMinimize(this);
}
this.cover.resetSize();
},maximize:function(_32){
if(this.maxFlag){
if(this.minFlag){
Element.toggle(this.windowBody);
this.minFlag=false;
this.setMax();
}else{
if(!this.options.preRevertMaximize(this)){
return;
}
var _33={height:parseInt(this.currentSize[1])};
this.setBodyHeight(_33);
this.element.style.width=this.currentSize[0];
this.element.style.height=this.currentSize[1];
this.element.style.left=this.currentPos[0];
this.element.style.top=this.currentPos[1];
this.maxFlag=false;
this.minFlag=false;
document.body.style.overflow="";
this.element.style.position=this.position;
if(this.parent){
if(this.nextElement){
this.parent.insertBefore(this.element,this.nextElement);
}else{
this.parent.appendChild(this.element);
}
}
this.options.endRevertMaximize(this);
}
}else{
if(!this.options.preMaximize(this)){
return;
}
if(!this.minFlag){
this.currentPos=[Element.getStyle(this.element,"left"),Element.getStyle(this.element,"top")];
this.currentSize=[Element.getStyle(this.element,"width"),Element.getStyle(this.element,"height")];
}else{
Element.toggle(this.windowBody);
this.minFlag=false;
}
this.parent=this.element.parentNode;
this.nextElement=Element.next(this.element,0);
this.position=Element.getStyle(this.element,"position");
document.body.style.overflow="hidden";
document.body.appendChild(this.element);
this.element.style.position="absolute";
this.setMax();
this.maxFlag=true;
this.options.endMaximize(this);
}
this.cover.resetSize();
},setMin:function(){
var _34=this.header.offsetHeight+this.bottom.offsetHeight;
var _35=this.options.minWidth;
this.element.style.height=_34+"px";
this.element.style.width=_35+"px";
},setMax:function(_36){
var _37=Element.getWindowWidth();
var _38=Element.getWindowHeight();
var _39={height:_38};
with(this.element.style){
width=_37+"px";
height=_38+"px";
left="0px";
top="0px";
}
this.setBodyHeight(_39);
this.setWindowZindex(_36);
},_getParentWidth:function(_3a){
if(_3a&&_3a.style){
var _3b=_3a.style.width;
var _3c=0;
if(_3b){
if((_3c=_3b.indexOf("px",0))>0){
return parseInt(_3b);
}else{
if((_3c=_3b.indexOf("%",0))>0){
var pw=this._getParentWidth(_3a.parentNode);
var par=parseInt(_3b);
return pw*par/100;
}else{
if(!_3b.isNaN){
return parseInt(_3b);
}
}
}
}else{
if(_3a==document.body){
return Element.getWindowWidth();
}
}
}
},setHeight:function(_3f){
_3f={height:_3f};
Element.setStyle(this.element,_3f);
this.setBodyHeight(_3f);
},setBodyHeight:function(_40){
var _41=parseInt(_40.height);
if(_41>this.options.minHeight){
var _42=(_41-this.header.offsetHeight-this.bottom.offsetHeight)+"px";
this.windowBody.childNodes[0].style.height=_42;
this.windowBody.childNodes[1].style.height=_42;
this.windowBody.childNodes[2].style.height=_42;
this.windowBody.style.height=_42;
}
if(this.cover){
this.cover.resetSize();
}
},center:function(){
var w=parseInt(Element.getStyle(this.element,"width"));
var h=parseInt(Element.getStyle(this.element,"height"));
var _45=Position.cumulativeOffset(Position.offsetParent(this.element));
var _46=(Element.getWindowWidth()-w)/2;
var top=(Element.getWindowHeight()-h)/2;
var _48=(document.documentElement.scrollTop||document.body.scrollTop);
var _49=(document.documentElement.scrollLeft||document.body.scrollLeft);
top+=_48-_45[1];
_46+=_49-_45[0];
top=((top+_45[1])>=0)?top:0-_45[1];
_46=((_46+_45[0])>=0)?_46:0-_45[0];
Element.setStyle(this.element,{left:_46+"px",top:top+"px"});
},enableResizing:function(){
var _4a=this.options.resizeY?6:0;
var _4b=this.options.resizeY?6:0;
var _4c=this.options.resizeX?6:0;
var _4d=this.options.resizeX?6:0;
this.resizeable=new ResizeableWindowEx(this.element,{top:_4a,bottom:_4b,left:_4c,right:_4d,minWidth:this.options.minWidth,minHeight:this.options.minHeight,draw:this.setBodyHeight.bind(this),resize:this.options.endResize,restriction:this.options.restriction,zindex:2000});
},disableResizing:function(){
this.resizeable.destroy();
},appendToBody:function(){
this.removeFromBody(this.element.id);
document.body.appendChild(this.element);
},removeFromBody:function(_4e){
$A(document.body.childNodes).each(function(_4f){
if(_4f.id==_4e){
_4f.remove();
}
});
}};
var DraggableWindowEx=Class.create();
Object.extend(DraggableWindowEx.prototype,Draggable.prototype);
Object.extend(DraggableWindowEx.prototype,{initDrag:function(_50){
if(Event.isLeftClick(_50)){
var src=Event.element(_50);
if(src.tagName&&(src.tagName=="INPUT"||src.tagName=="SELECT"||src.tagName=="OPTION"||src.tagName=="BUTTON"||src.tagName=="TEXTAREA")){
return;
}
if(this.element._revert){
this.element._revert.cancel();
this.element._revert=null;
}
var _52=[Event.pointerX(_50),Event.pointerY(_50)];
var pos=Position.cumulativeOffset(this.element);
this.offset=[0,1].map(function(i){
return (_52[i]-pos[i]);
});
var _55=ZindexManager.getIndex();
this.originalZ=_55;
this.options.zindex=_55;
Element.setStyle(this.element,{zIndex:_55});
Draggables.activate(this);
Event.stop(_50);
}
},endDrag:function(_56){
if(!this.dragging){
return;
}
this.stopScrolling();
this.finishDrag(_56,true);
this.options.endDrag();
Event.stop(_56);
}});

