Accordion=Class.create();
Accordion.className={accordion:"accordion",panel:"accordion_panel",tab:"accordion_tab",tabLeftInactive:"accordion_tabLeftInactive",tabLeftActive:"accordion_tabLeftActive",tabMiddleInactive:"accordion_tabMiddleInactive",tabMiddleActive:"accordion_tabMiddleActive",tabRightInactive:"accordion_tabRightInactive",tabRightActive:"accordion_tabRightActive"};
Accordion.prototype={initialize:function(_1){
var _2=Object.extend({cssPrefix:"custom_",selected:1,duration:0.5},arguments[1]||{});
this.options=_2;
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
var _3=CssUtil.appendPrefix(this.options.cssPrefix,Accordion.className);
this.classNames=new CssUtil([Accordion.className,_3]);
this.classNames.addClassNames(this.element,"accordion");
this.selected=(this.options.selected>0)?this.options.selected-1:0;
this.start();
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
this.effecting=false;
},start:function(){
this.tabs=[];
this.panels=[];
this.panelList=[];
this.tabId=this.element.id+"_tab";
this.tabLeftId=this.tabId+"_left";
this.tabMiddleId=this.tabId+"_middle";
this.tabRightId=this.tabId+"_right";
this.panelId=this.element.id+"_panel";
this.build();
},build:function(){
Element.cleanWhitespace(this.element);
this.panelList=this.element.childNodes;
for(var i=0;i<this.panelList.length;i++){
if(this.panelList[i].nodeType!=1){
Element.remove(this.panelList[i]);
i--;
continue;
}
Element.cleanWhitespace(this.panelList[i]);
var _5=this.panelList[i].childNodes;
this.buildTab(_5[0],i);
this.buildPanel(_5[0],i);
}
this.selectTab();
},buildTab:function(_6,i){
var _8=Builder.node("div",{id:this.tabId+i});
this.classNames.addClassNames(_8,"tab");
var _9=Builder.node("div",{id:this.tabLeftId+i});
var _a=Builder.node("div",{id:this.tabMiddleId+i});
_a.appendChild(_6);
var _b=Builder.node("div",{id:this.tabRightId+i});
_8.appendChild(_9);
_8.appendChild(_a);
_8.appendChild(_b);
Event.observe(_8,"click",this.selectTab.bindAsEventListener(this));
this.tabs[i]=_8;
this.setTabInactive(_8);
this.panelList[i].appendChild(_8);
},buildPanel:function(_c,i){
var _e=Builder.node("div",{id:this.panelId+i});
this.classNames.addClassNames(_e,"panel");
_e.appendChild(_c);
Element.hide(_e);
this.panels[i]=_e;
this.panelList[i].appendChild(_e);
},selectTab:function(e){
if(this.effecting){
return;
}
if(!e){
if(!this.panels[this.selected]){
this.selected=0;
}
Element.show(this.panels[this.selected]);
this.setTabActive(this.tabs[this.selected]);
return;
}
var _10=Event.element(e);
var _11=this.getTargetIndex(_10);
if(_11==this.selected){
return;
}
var _12=this.panels[this.selected];
var _13=this.panels[_11];
this.setTabInactive(this.tabs[this.selected]);
this.setTabActive(this.tabs[_11]);
this.effecting=true;
new Effect.Parallel([new Effect.BlindUp(_12,{sync:true}),new Effect.BlindDown(_13,{sync:true})],{duration:this.options.duration,beforeStart:function(){
this.effecting=true;
}.bind(this),afterFinish:function(){
this.effecting=false;
}.bind(this)});
this.selected=_11;
},setTabActive:function(tab){
var _15=tab.childNodes;
this.classNames.refreshClassNames(_15[0],"tabLeftActive");
this.classNames.refreshClassNames(_15[1],"tabMiddleActive");
this.classNames.refreshClassNames(_15[2],"tabRightActive");
},setTabInactive:function(tab){
var _17=tab.childNodes;
this.classNames.refreshClassNames(_17[0],"tabLeftInactive");
this.classNames.refreshClassNames(_17[1],"tabMiddleInactive");
this.classNames.refreshClassNames(_17[2],"tabRightInactive");
},getTargetIndex:function(_18){
while(_18){
if(_18.id&&_18.id.indexOf(this.tabId,0)>=0){
var _19=_18.id.substring(this.tabId.length);
if(!isNaN(_19)){
return _19;
}
}
_18=_18.parentNode;
}
}};

