var ToolBar=Class.create();
ToolBar.className={container:"toolbar_container",containerLeft:"toolbar_containerLeft",containerMiddle:"toolbar_containerMiddle",containerRight:"toolbar_containerRight",toolbarItem:"toolbar_item",toolbarItemHover:"toolbar_itemHov",toolbarItemPres:"toolbar_itemPres",toolbarContent:"toolbar_content",toolbarContentPres:"toolbar_contentPres"};
ToolBar.prototype={initialize:function(_1){
var _2=Object.extend({cssPrefix:"custom_"},arguments[1]||{});
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.options=_2;
var _3=CssUtil.appendPrefix(this.options.cssPrefix,ToolBar.className);
this.classNames=new CssUtil([ToolBar.className,_3]);
this.build();
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
},build:function(){
this.classNames.addClassNames(this.element,"container");
var _4=this.element.childNodes;
var _5=Builder.node("div");
this.classNames.addClassNames(_5,"containerLeft");
this.containerMiddle=Builder.node("div");
this.classNames.addClassNames(this.containerMiddle,"containerMiddle");
var _6=Builder.node("div");
this.classNames.addClassNames(_6,"containerRight");
var _7=[];
var _8=this;
$A(_4).each(function(i){
if(i.nodeType!=1){
throw $continue;
}
_8.buildIcon(i);
});
this.element.appendChild(_5);
this.element.appendChild(this.containerMiddle);
this.element.appendChild(_6);
},buildIcon:function(_a){
var _b=Builder.node("div");
this.classNames.addClassNames(_b,"toolbarItem");
var _c=Builder.node("div");
this.classNames.addClassNames(_c,"toolbarContent");
_c.appendChild(_a);
_b.appendChild(_c);
this.containerMiddle.appendChild(_b);
this.setHovEvent(_b);
this.setPresEvent(_b);
},addIcon:function(_d){
var _e=Object.extend({id:"newIcon",src:"url",alt:"icon",width:15,height:15},arguments[0]||{});
if(!$(_e.id)){
var _f=Builder.node("img",{id:_e.id,src:_e.src,alt:_e.alt,style:"width: "+_e.width+"px; height: "+_e.height+"px;"});
this.buildIcon(_f);
}
},removeIcon:function(_10){
var _11=$(_10);
if(_11){
var _12=_11.parentNode.parentNode;
Element.remove(_12);
}
},addEvent:function(_13,_14,_15){
var _16=$(_13);
if(_16){
var _17=_16.parentNode.parentNode;
Event.observe(_17,_14,_15);
}
},removeEvent:function(_18,_19,_1a){
var _1b=$(_18);
if(_1b){
var _1c=_1b.parentNode.parentNode;
Event.stopObserving(_1c,_19,_1a);
}
},setHovEvent:function(_1d){
Event.observe(_1d,"mouseout",this.toggleItemClass(_1d,"toolbarItem").bindAsEventListener(this));
Event.observe(_1d,"mouseover",this.toggleItemClass(_1d,"toolbarItemHover").bindAsEventListener(this));
Event.observe(_1d,"mouseout",this.toggleItemClass(_1d.childNodes[0],"toolbarContent").bindAsEventListener(this));
},setPresEvent:function(_1e){
Event.observe(_1e,"mousedown",this.toggleItemClass(_1e,"toolbarItemPres").bindAsEventListener(this));
Event.observe(_1e,"mouseup",this.toggleItemClass(_1e,"toolbarItem").bindAsEventListener(this));
Event.observe(_1e,"mousedown",this.toggleItemClass(_1e.childNodes[0],"toolbarContentPres").bindAsEventListener(this));
Event.observe(_1e,"mouseup",this.toggleItemClass(_1e.childNodes[0],"toolbarContent").bindAsEventListener(this));
},toggleItemClass:function(_1f,_20){
return function(){
this.classNames.refreshClassNames(_1f,_20);
};
}};

