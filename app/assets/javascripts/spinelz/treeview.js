var TreeView=Class.create();
TreeView.className={top:"treeview",dir:"treeview_dir",dirBody:"treeview_dirBody",dirBodyText:"treeview_dirBodyText",dirBodyTextActive:"treeview_dirBodyTextActive",dirContainer:"treeview_dirContainer",dirContainerHover:"treeview_dirContainerHover",file:"treeview_file",fileBody:"treeview_fileBody",fileBodyText:"treeview_fileBodyText",fileBodyTextActive:"treeview_fileBodyTextActive",state_open:"treeview_stateOpen",state_close:"treeview_stateClose",state_empty:"treeview_stateEmpty",dirIcon:"treeview_dirIcon",fileIcon:"treeview_fileIcon",handle:"treeview_handle"};
TreeView.iconId="treeview_icon";
TreeView.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.options=Object.extend({dirSymbol:"dir",fileSymbol:"file",cssPrefix:"custom_",open:true,callBackFunctions:false,dirSelect:true,fileSelect:true,noSelectedInsert:true,iconIdPrefix:TreeView.iconId,move:false,unselected:Prototype.emptyFunction,enableUnselected:true,sortOptions:{},openDir:Prototype.emptyFunction,closeDir:Prototype.emptyFunction,emptyImg:false,initialSelected:null},arguments[1]||{});
this.customCss=CssUtil.appendPrefix(this.options.cssPrefix,TreeView.className);
this.classNames=new CssUtil([TreeView.className,this.customCss]);
this.changeClassNameDirAndFile(this.element);
var _2=this.element.childNodes;
for(var i=0;i<_2.length;i++){
this.build(_2[i]);
}
this.classNames.addClassNames(this.element,"top");
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
if(this.options.initialSelected){
this.selectEffect(this.options.initialSelected);
}
if(this.options.move){
this.setSortable();
}
},addChildById:function(_4,_5,_6){
_4=$(_4);
_5=$(_5);
var _7=null;
if(!_4||!_5){
return;
}else{
if(Element.hasClassName(_5,TreeView.className.dir)){
_7=this.getChildDirContainer(_5);
}else{
if(Element.hasClassName(_5,TreeView.className.top)){
_7=_5;
}else{
return;
}
}
}
this.build(_4);
if(isNaN(_6)){
_7.appendChild(_4);
}else{
var _8=this.getDirectoryContents(_7);
if(_8[_6]){
_7.insertBefore(_4,_8[_6]);
}else{
_7.appendChild(_4);
}
}
this.refreshStateImg(_5);
if(this.options.dragAdrop){
this.setSortable();
}
},addChildByPath:function(_9,_a){
_9=$(_9);
if(_9){
this.build(_9);
}else{
return;
}
var _b=_a.split("/").findAll(function(_c){
return (_c!="");
});
var _d=_b.pop();
var _e=this.search(_b.join("/"));
var _f=this.getDirectoryContents(_e);
if(_f[_d]){
_e.insertBefore(_9,_f[_d]);
}else{
_e.appendChild(_9);
}
this.refreshStateImg(_e.parentNode);
if(this.options.dragAdrop){
this.setSortable();
}
},addChildBySelected:function(_10,_11){
if(!this.selected&&!this.options.noSelectedInsert){
return;
}
if(this.selected){
this.addChildById(_10,this.selected,_11);
}else{
this.addChildById(_10,this.element,_11);
}
},addSelectItemCallback:function(_12){
if(!this.options.callBackFunctions){
this.options.callBackFunctions=new Array();
}
this.options.callBackFunctions.push(_12);
},build:function(_13){
if(_13.nodeType!=1){
return;
}
Element.cleanWhitespace(_13);
this.changeClassNameDirAndFile(_13);
if(Element.hasClassName(_13,TreeView.className.dir)){
var _14=this.createDirectoryContainer(_13);
var _15;
if(this.hasContents(_14)){
_15=this.createDirectoryBody(_13,false);
}else{
_15=this.createDirectoryBody(_13,true);
}
_13.appendChild(_15);
_13.appendChild(_14);
var _16=_14.childNodes;
for(var i=0;i<_16.length;i++){
this.build(_16[i]);
}
}else{
if(Element.hasClassName(_13,TreeView.className.file)){
var _18=this.createFileBody(_13);
_13.appendChild(_18);
}
}
},changeClassName:function(_19,to,_1b){
var _1c=document.getElementsByClassName(_1b,_19);
var _1d=this.classNames.joinClassNames(to);
_1c.each(function(n){
n.className=n.className.replace(new RegExp(_1b),_1d);
});
if(Element.hasClassName(_19,_1b)){
_19.className=_19.className.replace(new RegExp(_1b),_1d);
}
},changeClassNameDirAndFile:function(_1f){
this.changeClassName(_1f,"dir",this.options.dirSymbol);
this.changeClassName(_1f,"file",this.options.fileSymbol);
},convertJSON:function(){
return JSON.stringify(this.parse());
},createDirectoryBody:function(_20,_21){
var _22=null;
var _23=this.classNames.joinClassNames("dir");
if(_20.className!=_23){
_22=_20.className.replace(new RegExp(_23+" "),"");
_20.className=_23;
}
var _24=new Array();
var _25;
if(_21&&!this.options.emptyImg){
_25="state_empty";
}else{
if(this.options.open){
_25="state_open";
}else{
_25="state_close";
}
}
var id=this.options.iconIdPrefix.appendSuffix(_20.id);
var _27=Builder.node("DIV",{id:id.appendSuffix("stateImg")});
this.classNames.addClassNames(_27,_25);
Event.observe(_27,"click",this.toggle.bindAsEventListener(this));
var _28=Builder.node("DIV",{id:id});
this.classNames.addClassNames(_28,"dirIcon");
if(_22){
Element.addClassName(_28,_22);
}
this.classNames.addClassNames(_28,"handle");
var _29=Builder.node("SPAN",this.getDirectoryText(_20));
this.classNames.addClassNames(_29,"dirBodyText");
_24.push(_27);
_24.push(_28);
_24.push(_29);
var _2a=Builder.node("DIV",_24);
this.classNames.addClassNames(_2a,"dirBody");
if(this.options.dirSelect){
Event.observe(_28,"click",this.selectDirItem.bindAsEventListener(this));
Event.observe(_29,"click",this.selectDirItem.bindAsEventListener(this));
}
return _2a;
},createDirectoryContainer:function(_2b){
var _2c=_2b.getElementsByTagName("ul")[0];
if(!_2c){
_2c=Builder.node("UL");
}
this.classNames.addClassNames(_2c,"dirContainer");
if(!this.options.open){
Element.hide(_2c);
}
return _2c;
},createFileBody:function(_2d){
var _2e=null;
var _2f=this.classNames.joinClassNames("file");
if(_2d.className!=_2f){
_2e=_2d.className.replace(new RegExp(_2f+" "),"");
_2d.className=_2f;
}
var id=this.options.iconIdPrefix.appendSuffix(_2d.id);
var _31=Builder.node("DIV",{id:id});
this.classNames.addClassNames(_31,"fileIcon");
if(_2e){
Element.addClassName(_31,_2e);
}
this.classNames.addClassNames(_31,"handle");
var _32=Builder.node("SPAN",$A(_2d.childNodes));
this.classNames.addClassNames(_32,"fileBodyText");
var _33=new Array();
_33.push(_31);
_33.push(_32);
var _34=Builder.node("DIV",_33);
this.classNames.addClassNames(_34,"fileBody");
if(this.options.fileSelect){
Event.observe(_31,"click",this.selectFileItem.bindAsEventListener(this));
Event.observe(_32,"click",this.selectFileItem.bindAsEventListener(this));
}
return _34;
},getChildBody:function(_35){
var _36=[TreeView.className.fileBody,TreeView.className.dirBody];
return Element.getFirstElementByClassNames(_35,_36);
},getChildBodyText:function(_37){
var _38=[TreeView.className.fileBodyText,TreeView.className.fileBodyTextActive,TreeView.className.dirBodyText,TreeView.className.dirBodyTextActive];
return Element.getFirstElementByClassNames(_37,_38);
},getChildBodyTextNode:function(_39){
var _3a=this.getChildBody(_39);
var _3b=this.getChildBodyText(_3a);
return this.searchTextNode(_3b);
},getChildDir:function(_3c){
return document.getElementsByClassName(TreeView.className.dir,_3c);
},getChildDirBody:function(_3d){
return document.getElementsByClassName(TreeView.className.dirBody,_3d)[0];
},getChildDirContainer:function(_3e){
return document.getElementsByClassName(TreeView.className.dirContainer,_3e)[0];
},getChildStateImg:function(_3f){
var _40=this.getChildDirBody(_3f);
var _41=[TreeView.className.state_close,TreeView.className.state_open,TreeView.className.state_empty];
return Element.getFirstElementByClassNames(_40,_41);
},getChildren:function(_42,_43,_44){
var _45;
var _46=new Array();
if(_42){
_45=$(_42).getElementsByTagName("ul")[0];
}else{
_45=this.element;
}
$A(Element.getTagNodes(_45)).each(function(_47){
if(!_43&&Element.hasClassName(_47,TreeView.className.dir)){
_46.push(_47);
}
if(!_44&&Element.hasClassName(_47,TreeView.className.file)){
_46.push(_47);
}
});
return _46;
},getDirectoryContents:function(_48){
return $A(_48.childNodes).findAll(function(_49){
if((_49.nodeType!=1)){
return false;
}
if(_49.tagName.toLowerCase()=="li"){
return true;
}
return false;
});
},getDirectoryText:function(_4a){
return $A(_4a.childNodes).findAll(function(_4b){
if((_4b.nodeType!=1)){
return true;
}else{
if(_4b.tagName.toLowerCase()!="ul"){
return true;
}
}
return false;
});
},getHierarchyNumber:function(){
if(!this.selected){
return;
}
var _4c=this.selected;
var i=0;
while(true){
if(this.element==_4c){
return i;
}else{
_4c=this.getParentDir(_4c,true);
if(!_4c){
return;
}
i++;
}
}
},getParentDir:function(_4e,top){
var _50=Element.getParentByClassName(TreeView.className.dir,_4e);
if(!_50&&top){
_50=Element.getParentByClassName(TreeView.className.top,_4e);
}
return _50;
},hasContents:function(_51){
if(_51){
if(!Element.hasClassName(_51,TreeView.className.dirContainer)&&!Element.hasClassName(_51,TreeView.className.top)){
return false;
}
var _52=_51.childNodes;
for(var i=0;i<_52.length;i++){
if(_52[i].nodeType==1){
if(Element.hasClassName(_52[i],TreeView.className.dir)||Element.hasClassName(_52[i],TreeView.className.file)){
return true;
}
}
}
}
return false;
},parse:function(_54){
if(!_54){
_54=this.element;
}
var _55=[];
var _56=this.getDirectoryContents(_54);
for(var i=0;i<_56.length;i++){
var _58=_56[i];
var _59=this.getChildBody(_58);
var _5a=this.getChildBodyText(_59);
var _5b={};
_5b.id=_58.id;
_5b.name=Element.collectTextNodes(_5a).replace(/\n/,"");
if(Element.hasClassName(_58,TreeView.className.dir)){
_5b.type=this.options.dirSymbol;
_5b.contents=this.parse(this.getChildDirContainer(_58));
}else{
_5b.type=this.options.fileSymbol;
}
_55.push(_5b);
}
return _55;
},refreshStateImg:function(_5c){
if(!Element.hasClassName(_5c,TreeView.className.dir)){
return;
}
var _5d=this.getChildDirContainer(_5c);
var img=this.getChildStateImg(_5c);
if(!this.hasContents(_5d)&&!this.options.emptyImg){
this.classNames.refreshClassNames(img,"state_empty");
}else{
if(Element.visible(_5d)){
this.classNames.refreshClassNames(img,"state_open");
}else{
this.classNames.refreshClassNames(img,"state_close");
}
}
},removeById:function(_5f){
_5f=$(_5f);
if(_5f){
var _60=_5f.parentNode.parentNode;
Element.remove(_5f);
this.refreshStateImg(_60);
}
},removeByPath:function(_61){
var _62=_61.split("/").findAll(function(elm){
return (elm!="");
});
var _64=_62.pop();
var _65=this.search(_62.join("/"));
var _66=this.getDirectoryContents(_65)[_64];
if(_66){
this.removeById(_66);
}
},removeBySelected:function(){
if(!this.selected){
return;
}
this.removeById(this.selected);
this.selected=false;
},renameById:function(_67,_68){
_68=$(_68);
if(!Element.hasClassName(_68,TreeView.className.dir)&&!Element.hasClassName(_68,TreeView.className.file)){
return;
}
var _69=this.getChildBodyTextNode(_68);
_69.nodeValue=_67;
},renameByPath:function(_6a,_6b){
var _6c=_6b.split("/").findAll(function(elm){
return (elm!="");
});
var _6e=_6c.pop();
var _6f=this.search(_6c.join("/"));
var _70=this.getDirectoryContents(_6f)[_6e];
if(_70){
this.renameById(_6a,_70);
}
},renameBySelected:function(_71){
if(!this.selected){
return;
}
this.renameById(_71,this.selected);
},search:function(_72){
var _73=_72.split("/").findAll(function(elm){
return (elm!="");
});
var _75=this.element;
for(var i=0;i<_73.length;i++){
var num=_73[i];
var _78=this.getDirectoryContents(_75);
if(_78[num]&&Element.hasClassName(_78[num],TreeView.className.dir)){
_75=this.getChildDirContainer(_78[num]);
}else{
return false;
}
}
return _75;
},searchTextNode:function(_79){
var _7a=null;
var _7b=_79.childNodes;
for(var i=0;i<_7b.length;i++){
if(_7b[i].nodeType==3){
_7a=_7b[i];
break;
}else{
if(_7b[i].nodeType==1){
var tmp=this.searchTextNode(_7b[i]);
if(tmp){
_7a=tmp;
break;
}
}
}
}
return _7a;
},selectDirItem:function(_7e){
var _7f=Element.getParentByClassName(TreeView.className.dirBody,Event.element(_7e));
this.selectItem(_7f);
},selectEffect:function(_80){
_80=$(_80);
if(_80){
var _81=_80.firstChild;
if(this.selectItemUnselect(_81,false)){
return;
}
this.selectItemSelect(_81,false);
}
},selectFileItem:function(_82){
var _83=Element.getParentByClassName(TreeView.className.fileBody,Event.element(_82));
this.selectItem(_83);
},selectItem:function(_84){
if(this.selectItemUnselect(_84,true)){
return;
}
this.selectItemSelect(_84,true);
},selectItemSelect:function(_85,_86){
this.selected=_85.parentNode;
var _87=this.getChildBodyText(_85);
if(Element.hasClassName(_87,TreeView.className.dirBodyText)){
this.classNames.refreshClassNames(_87,"dirBodyTextActive");
this.defaultCss="dirBodyText";
}else{
if(Element.hasClassName(_87,TreeView.className.fileBodyText)){
this.classNames.refreshClassNames(_87,"fileBodyTextActive");
this.defaultCss="fileBodyText";
}
}
if(_86){
if(this.options.callBackFunctions){
for(var i=0;i<this.options.callBackFunctions.length;i++){
this.options.callBackFunctions[i](_85.parentNode);
}
}
}
},selectItemUnselect:function(_89,_8a){
if(this.selected){
var _8b=this.getChildBody(this.selected);
var _8c=this.getChildBodyText(_8b);
this.classNames.refreshClassNames(_8c,this.defaultCss);
if(this.selected==_89.parentNode&&this.options.enableUnselected){
this.selected=false;
this.defaultCss=false;
if(_8a){
this.options.unselected();
}
return true;
}
}
return false;
},setSortable:function(){
var _8d=Object.extend({dropOnEmpty:true,tree:true,hoverclass:"treeview_dirContainerHover",scroll:window,ghosting:true},this.options.sortOptions);
Sortable.create(this.element,_8d);
},toggle:function(_8e){
Event.stop(_8e);
var src=Event.element(_8e);
var _90=this.getParentDir(src);
var _91=this.getChildDirContainer(_90);
if(!this.hasContents(_91)&&!this.options.emptyImg){
return;
}
Element.toggle(_91);
this.refreshStateImg(_90);
if(!this.hasContents(_91)&&!this.options.emptyImg){
this.options.openDir(_90,_91);
}else{
if(Element.visible(_91)){
this.options.openDir(_90,_91);
}else{
this.options.closeDir(_90,_91);
}
}
}};

