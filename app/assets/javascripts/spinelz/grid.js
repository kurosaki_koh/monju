Grid=Class.create();
Grid.className={container:"grid_container",baseTable:"grid_baseTable",baseRow:"grid_baseRow",baseCell:"grid_baseCell",headerTable:"grid_headerTable",headerRow:"grid_headerRow",headerCell:"grid_headerCell",headerCellDrag:"grid_headerCellDrag",headerCellSort:"grid_headerCellVal",idTable:"grid_idTable",idRow:"grid_idRow",idCell:"grid_idCell",idCellVal:"grid_idCellVal",cellTable:"grid_cellTable",cellTbody:"grid_cellTbody",cellRow:"grid_cellRow",cell:"grid_cell",cellVal:"grid_cellVal",cellSelected:"grid_cellSelected",state:"grid_state",stateEmpty:"grid_stateEmpty",stateOpen:"grid_stateOpen",stateClose:"grid_stateClose",inplaceEditor:"grid_inplaceEditor"};
Grid.scrollTop=0;
Grid.scrollLeft=0;
Grid.options={};
Grid.options.baseTable={border:1,frame:"border",cellSpacing:0,cellPadding:0};
Grid.options.headerTable={border:1,frame:"border",cellSpacing:0,cellPadding:0};
Grid.options.idTable={border:1,frame:"border",cellSpacing:0,cellPadding:0};
Grid.options.cellTable={border:1,frame:"border",cellSpacing:0,cellPadding:0};
Grid.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
var _2=Object.extend({cssPrefix:"custum_",cellMinWidth:10,cellMinHeight:10,cellDefaultWidth:72,cellDefaultHeight:25,defaultRowLength:10,baseWidth:40,baseHeight:25,baseTop:0,baseLeft:0,cellEditUrl:"",updateGridUrl:"",updateGridReceiver:"",hierarchy:false,hierarchyCol:false,hierarchyIndent:20,sortOptions:{}},arguments[1]||{});
this.options=_2;
this.custumCss=CssUtil.appendPrefix(_2.cssPrefix,Grid.className);
this.cssUtil=new CssUtil([Grid.className,this.custumCss]);
this.cssUtil.addClassNames(this.element,"container");
this.hierarchyCol=this.options.hierarchyCol?$(this.options.hierarchyCol):false;
this.hierarchyColIndex=this.hierarchyCol?this.hierarchyCol.cellIndex:0;
Element.makePositioned(this.element);
Position.includeScrollOffsets=true;
this.stateDivWidth=parseInt(CssUtil.getCssRuleBySelectorText("."+Grid.className.state).style.width,10);
this.marginSize=this.options.marginSize?this.options.marginSize:4;
this.stateIndent=15;
this.rowIdBase=this.element.id+"_row_";
this.topLevelList=new Array();
this.removeList=new Array();
this.build();
this.removeList.each(function(r){
r.parentNode.removeChild(r);
});
var _4={relate:this.idTable,handle:this.headerTable,callBack:this.finishSort.bind(this)};
_4=$H(_4).merge(this.options.sortOptions);
this.sortTable=new SortableTableGridEx(this.cellTable,_4);
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
},build:function(){
Element.cleanWhitespace(this.element);
this.cellTable=this.element.childNodes[0];
Element.cleanWhitespace(this.cellTable);
this.colLength=this.cellTable.tHead.rows[0].cells.length;
this.rowLength=this.cellTable.tBodies[0].rows.length;
if(this.rowLength==0){
this.rowLength=this.options.defaultRowLength;
}
this.buildBaseTable();
this.buildHeaderTable();
this.buildIdTable();
this.buildCellTable();
Event.observe(this.element,"scroll",this.fixTablePosition.bindAsEventListener(this));
},buildBaseTable:function(){
this.baseTable=Builder.node("table",Grid.options.baseTable);
this.cssUtil.addClassNames(this.baseTable,"baseTable");
with(this.baseTable.style){
width=this.options.baseWidth+"px";
height=this.options.baseHeight+"px";
position="absolute";
top=this.options.baseTop+"px";
left=this.options.baseLeft+"px";
}
var _5=this.baseTable.insertRow(0);
var _6=_5.insertCell(0);
this.cssUtil.addClassNames(_5,"baseRow");
this.cssUtil.addClassNames(_6,"baseCell");
this.element.appendChild(this.baseTable);
},buildHeaderTable:function(){
this.headerTable=Builder.node("table",Grid.options.headerTable);
this.cssUtil.addClassNames(this.headerTable,"headerTable");
var _7=this.cellTable.tHead;
var _8=_7.rows[0];
_8.id=this.element.id+"_headerTable_row";
var _9=_8.cells;
Element.cleanWhitespace(_7);
Element.cleanWhitespace(_8);
this.cssUtil.addClassNames(_8,"headerRow");
for(var i=0;i<_9.length;i++){
var _b=_9[i];
var _c=_b.firstChild;
var _d=_b.innerHTML;
this.buildHeaderCell(_b,_d,i);
this.removeList.push(_c);
}
this.headerTable.appendChild(_7);
with(this.headerTable.style){
width=this.options.cellDefaultWidth*this.colLength+"px";
height=this.baseTable.style.height;
position="absolute";
top=Element.getStyle(this.baseTable,"top");
left=parseInt(Element.getStyle(this.baseTable,"left"))+parseInt(Element.getStyle(this.baseTable,"width"))+"px";
}
this.element.appendChild(this.headerTable);
Sortable.create(_8,{tag:"td",handle:Grid.className.headerCellDrag,constraint:true,overlap:"horizontal",scroll:this.element,onUpdate:this.updateCellLine.bind(this)});
},buildHeaderCell:function(_e,_f,_10){
_e.id=this.element.id+"_header_col_"+_10;
var _11=Builder.node("div");
var _12=Builder.node("div");
this.cssUtil.addClassNames(_e,"headerCell");
this.cssUtil.addClassNames(_11,"headerCellDrag");
this.cssUtil.addClassNames(_12,"headerCellSort");
_e.style.width=this.options.cellDefaultWidth+"px";
var _13=parseInt(Element.getStyle(_e,"width"))-(this.marginSize*2);
var _14=_13-(this.marginSize*2);
var _15=this.options.baseHeight-(Grid.options.headerTable.border*4);
with(_11.style){
width=_13+"px";
height=_15+"px";
marginLeft=this.marginSize+"px";
marginRight=this.marginSize+"px";
}
with(_12.style){
width=_14+"px";
height=_15+"px";
marginLeft=this.marginSize+"px";
marginRight=this.marginSize+"px";
}
_12.innerHTML=_f;
_11.appendChild(_12);
_e.appendChild(_11);
new ResizeableGridEx(_e,{minWidth:this.options.cellMinWidth,top:0,right:2,bottom:0,left:0,draw:this.updateCellWidth.bind(this)});
Event.observe(_e,"mousedown",this.setSelectedColumn.bindAsEventListener(this));
Event.observe(_11,"mousedown",this.setSelectedColumn.bindAsEventListener(this));
Event.observe(_12,"mousedown",this.setSelectedColumn.bindAsEventListener(this));
},buildIdTable:function(){
this.idTable=Builder.node("table",Grid.options.idTable);
this.cssUtil.addClassNames(this.idTable,"idTable");
for(var i=0;i<this.rowLength;i++){
var row=this.idTable.insertRow(i);
this.buildIdRow(row,i);
}
with(this.idTable.style){
width=this.options.baseWidth+"px";
position="absolute";
top=parseInt(Element.getStyle(this.baseTable,"top"))+parseInt(Element.getStyle(this.baseTable,"height"))+"px";
left=Element.getStyle(this.baseTable,"left");
}
this.element.appendChild(this.idTable);
var _18=this.idTable.tBodies[0];
_18.id=this.element.id+"_idTable_tbody";
Sortable.create(_18,{tag:"tr",handle:Grid.className.idCellVal,scroll:this.element,constraint:true,overlap:"vertical",onUpdate:this.updateRowLine.bind(this)});
},buildIdRow:function(row,_1a){
row.id=this.rowIdBase+"_id_"+_1a;
var _1b=row.insertCell(0);
var _1c=Builder.node("div");
this.cssUtil.addClassNames(row,"idRow");
this.cssUtil.addClassNames(_1b,"idCell");
this.cssUtil.addClassNames(_1c,"idCellVal");
with(_1b.style){
width=this.options.baseWidth+"px";
height=this.options.cellDefaultHeight+"px";
}
with(_1c.style){
marginTop=this.marginSize+"px";
marginBottom=this.marginSize+"px";
width=this.options.baseWidth-(Grid.options.idTable.border*4)+"px";
height=this.options.cellDefaultHeight-(this.marginSize*2)+"px";
}
_1c.innerHTML=_1a+1;
_1b.appendChild(_1c);
new ResizeableGridEx(_1b,{minHeight:this.options.cellMinHeight,top:0,right:0,bottom:2,left:0,draw:this.updateCellHeight.bind(this)});
Event.observe(row,"mousedown",this.setSelectedRow.bindAsEventListener(this));
Event.observe(_1b,"mousedown",this.setSelectedRow.bindAsEventListener(this));
Event.observe(_1c,"mousedown",this.setSelectedRow.bindAsEventListener(this));
},buildCellTable:function(){
var _1d=this.cellTable.tBodies[0];
Element.cleanWhitespace(_1d);
with(this.cellTable){
border=Grid.options.cellTable.border;
cellSpacing=Grid.options.cellTable.cellSpacing;
cellPadding=Grid.options.cellTable.cellPadding;
}
this.cssUtil.addClassNames(this.cellTable,"cellTable");
this.cssUtil.addClassNames(_1d,"cellTbody");
this.element.appendChild(this.cellTable);
var _1e=this.cellTable.rows;
if(!_1e||_1e.length==0){
for(var i=0;i<this.rowLength;i++){
var _20=this.cellTable.insertRow(i);
_20.id=this.rowIdBase+i;
this.cssUtil.addClassNames(_20,"cellRow");
for(var j=0;j<this.colLength;j++){
var _22=_20.insertCell(j);
this.buildCell(_22,j,"");
}
if(this.options.hierarchy){
this.setHierarchyRow(_20);
}
}
}else{
for(var i=0;i<this.rowLength;i++){
var row=_1e[i];
Element.cleanWhitespace(row);
this.cssUtil.addClassNames(row,"cellRow");
row.id=this.rowIdBase+i;
var _24=row.cells;
for(var j=0;j<_24.length;j++){
var _25=_24[j];
Element.cleanWhitespace(_25);
this.buildCell(_25,j,Element.collectTextNodes(_25));
this.removeList.push(_25.firstChild);
}
if(this.options.hierarchy){
this.setHierarchyRow(row);
}
}
}
with(this.cellTable.style){
width=this.options.cellDefaultWidth*this.colLength+"px";
position="absolute";
top=parseInt(Element.getStyle(this.baseTable,"top"))+parseInt(Element.getStyle(this.baseTable,"height"))+"px";
left=parseInt(Element.getStyle(this.baseTable,"left"))+parseInt(Element.getStyle(this.baseTable,"width"))+"px";
}
this.cellTable.getIdRow=this.getIdRow.bind(this);
},buildCell:function(_26,_27,_28){
var _29=Builder.node("div");
_29.innerHTML=_28;
_26.appendChild(_29);
_26.id=_26.parentNode.id+"_col_"+_27;
this.cssUtil.addClassNames(_26,"cell");
this.cssUtil.addClassNames(_29,"cellVal");
with(_26.style){
width=Element.getStyle(this.getHeaderCell(_26),"width");
height=Element.getStyle(this.getIdRow(_26.parentNode).cells[0],"height");
}
with(_29.style){
width=_26.style.width;
height=_26.style.height;
marginTop="0px";
marginBottom="0px";
}
Event.observe(_26,"click",this.setSelectedCell.bindAsEventListener(this));
var _2a=new Ajax.InPlaceEditor(_29,this.options.cellEditUrl,{formClassName:this.cssUtil.joinClassNames("inplaceEditor"),rows:2,cols:12,okButton:false,cancelLink:false,submitOnBlur:true,hoverClassName:"cellHover",highlightcolor:"#becfeb",highlightendcolor:"#becfeb",onComplete:this.showStateDiv.bind(this),callback:this.createInplaceEditorParams.bind(this),formId:_26.id+"_form"});
_29.ajax=_2a;
Event.stopObserving(_29,"click",_2a.onclickListener);
Event.stopObserving(_29,"mouseover",_2a.mouseoverListener);
Event.stopObserving(_29,"mouseout",_2a.mouseoutListener);
Event.observe(_29,"dblclick",this.setTextAreaSize.bindAsEventListener(this));
if(this.options.hierarchy&&_26.cellIndex==this.hierarchyColIndex){
Event.observe(_29,"dblclick",this.hideStateDiv.bindAsEventListener(this));
}
Event.observe(_29,"dblclick",_2a.onclickListener);
},addColumn:function(_2b,_2c,_2d){
var _2e=this.headerTable.rows[0];
var _2f=(!isNaN(_2b))?_2b:this.colLength;
var _30=this.colLength;
var _31=_2e.insertCell(_2f);
this.buildHeaderCell(_31,_2c,_2f);
var _32=this.cellTable.rows;
var _33=this.idTable.rows;
for(var i=0;i<_32.length;i++){
var _35=_32[i].insertCell(_2f);
var _36="";
if(_2d&&_2d[i]){
_36=_2d[i];
}
this.buildCell(_35,_30,_36);
}
this.headerTable.style.width=parseInt(Element.getStyle(this.headerTable,"width"))+this.options.cellDefaultWidth+"px";
this.cellTable.style.width=this.headerTable.style.width;
var _37=Sortable.options(_2e);
_37.draggables.push(new Draggable(_31,{revert:true,constraint:true,scroll:this.element,handle:Element.getElementsByClassName(_31,Grid.className.headerCellDrag)[0]}));
Droppables.add(_31,{overlap:"horizontal",containment:_2e,onHover:Sortable.onHover,greedy:true});
_37.droppables.push(_31);
this.sortTable.addEvent(Element.getElementsByClassName(_31,Grid.className.headerCellSort)[0]);
this.colLength+=1;
},deleteColumn:function(_38){
if((isNaN(_38))||_38>=this.colLength){
return;
}
var _39=this.headerTable.rows[0];
if(!_39){
return;
}
var _3a=_39.cells[_38];
if(!_3a){
return;
}
var _3b=_3a.offsetWidth;
var _3c=this.cellTable.rows;
_39.deleteCell(_38);
for(var i=0;i<_3c.length;i++){
_3c[i].deleteCell(_38);
}
var _3e=parseInt(Element.getStyle(this.headerTable,"width"))-_3b;
this.headerTable.style.width=_3e>=0?_3e+"px":"0px";
this.cellTable.style.width=this.headerTable.style.width;
this.colLength-=1;
this.fixTablePosition();
},addRow:function(_3f,_40){
var _41=(!isNaN(_3f))?_3f:this.idTable.rows.length;
var _42=this.idTable.rows.length;
var _43=this.idTable.insertRow(_3f);
_43.id=this.rowIdBase+"_id_"+_42;
this.buildIdRow(_43,_42);
this.updateId();
var _44=this.cellTable.insertRow(_41);
_44.id=this.rowIdBase+_42;
this.cssUtil.addClassNames(_44,"cellRow");
var _45=this.headerTable.rows[0].cells;
for(var i=0;i<_45.length;i++){
var _47=_45[i];
var _48=_47.id.substring(_47.id.indexOf("_col_",0)+"_col_".length);
var _49=_44.insertCell(i);
var _4a=(_40&&_40[i])?_40[i]:"";
this.buildCell(_49,_48,_4a);
}
this.sortTable.defaultOrder.insert(_3f,_44);
var _4b=this.idTable.tBodies[0];
var _4c=Sortable.options(_4b);
_4c.draggables.push(new Draggable(_43,{revert:true,constraint:true,scroll:this.element,handle:Element.getElementsByClassName(_43,Grid.className.idCellVal)[0]}));
Droppables.add(_43,{overlap:"vertical",containment:_4b,onHover:Sortable.onHover,greedy:true});
_4c.droppables.push(_43);
this.rowLength+=1;
return _44;
},deleteRow:function(_4d){
if(isNaN(_4d)||_4d>=this.rowLength){
return;
}
var _4e=null;
if(this.cellTable.rows[_4d]){
_4e=this.cellTable.rows[_4d].id;
}else{
return;
}
this.sortTable.defaultOrder.reverse();
var _4f=new Array();
for(var i=0;this.sortTable.defaultOrder.length>0;i++){
var row=this.sortTable.defaultOrder.pop();
if(row.id==_4e){
continue;
}
_4f.push(row);
}
this.sortTable.defaultOrder=_4f;
this.idTable.deleteRow(_4d);
this.cellTable.deleteRow(_4d);
this.fixTablePosition();
this.rowLength-=1;
this.updateId();
},getHeaderCell:function(_52){
return this.headerTable.rows[0].cells[_52.cellIndex];
},getCells:function(_53){
var _54=this.cellTable.rows;
var _55=new Array();
for(var i=0;i<_54.length;i++){
_55.push(_54[i].cells[_53]);
}
return _55;
},getRow:function(_57){
return this.cellTable.rows[_57];
},getIdRow:function(_58){
var id=_58.id;
var _5a=id.substring(this.rowIdBase.length);
var _5b=$(this.rowIdBase+"_id_"+_5a);
return _5b;
},getColTitle:function(_5c){
var _5d=this.headerTable.rows[0].cells[_5c];
var _5e=Element.collectTextNodes(_5d);
return _5e;
},finishSort:function(){
for(var i=0;i<this.cellTable.rows.length;i++){
this.idTable.tBodies[0].appendChild(this.getIdRow(this.cellTable.rows[i]));
}
this.updateId();
},setSelectedCell:function(_60){
this.removeSelectedClasses();
var src=Event.element(_60);
if(src.tagName.toUpperCase()!="TH"&&src.tagName.toUpperCase()!="TD"){
src=Element.getParentByTagName(["TH,TD"],src);
}
this.targetCell=src;
if(this.targetCell){
this.targetColIndex=this.targetCell.cellIndex;
this.targetRowIndex=this.targetCell.parentNode.rowIndex;
this.targetColumn=this.getCells(this.targetColIndex);
this.targetRow=this.getRow(this.targetRowIndex);
}
this.cssUtil.addClassNames(this.targetCell,"cellSelected");
var _62=Element.getTagNodes(this.targetCell,true);
for(var i=0;i<_62.length;i++){
this.cssUtil.addClassNames(_62[i],"cellSelected");
}
},setSelectedColumn:function(_64){
this.removeSelectedClasses();
this.targetCell=null;
this.targetRowIndex=null;
this.targetRow=null;
this.targetIdRow=null;
var src=Event.element(_64);
if(src&&(src.tagName.toUpperCase()=="TH"||src.tagName.toUpperCase()=="TD")){
this.targetHeaderCell=src;
}else{
this.targetHeaderCell=Element.getParentByTagName(["TH,TD"],src);
}
if(this.targetHeaderCell){
this.targetColIndex=this.targetHeaderCell.cellIndex;
this.targetColumn=this.getCells(this.targetColIndex);
this.cssUtil.addClassNames(this.targetHeaderCell,"cellSelected");
var _66=Element.getTagNodes(this.targetHeaderCell,true);
for(var i=0;i<_66.length;i++){
this.cssUtil.addClassNames(_66[i],"cellSelected");
}
for(var i=0;i<this.targetColumn.length;i++){
this.cssUtil.addClassNames(this.targetColumn[i],"cellSelected");
var _68=Element.getTagNodes(this.targetColumn[i],true);
for(var j=0;j<_68.length;j++){
this.cssUtil.addClassNames(_68.length[j],"cellSelected");
}
}
}
},setSelectedRow:function(_6a){
this.removeSelectedClasses();
var src=Event.element(_6a);
if(src&&src.tagName.toUpperCase()=="TR"){
this.targetIdRow=src;
}else{
this.targetIdRow=Element.getParentByTagName(["TR"],src);
}
if(this.targetIdRow){
this.targetRowIndex=this.targetIdRow.rowIndex;
this.targetRow=this.getRow(this.targetRowIndex);
this.cssUtil.addClassNames(this.targetRow,"cellSelected");
var _6c=Element.getTagNodes(this.targetRow,true);
for(var i=0;i<_6c.length;i++){
this.cssUtil.addClassNames(_6c[i],"cellSelected");
}
this.cssUtil.addClassNames(this.targetIdRow,"cellSelected");
_6c=Element.getTagNodes(this.targetIdRow,true);
for(var i=0;i<_6c.length;i++){
this.cssUtil.addClassNames(_6c[i],"cellSelected");
}
}
},removeSelectedClasses:function(){
if(this.targetHeaderCell){
this.cssUtil.removeClassNames(this.targetHeaderCell,"cellSelected");
var _6e=Element.getTagNodes(this.targetHeaderCell,true);
for(var i=0;i<_6e.length;i++){
this.cssUtil.removeClassNames(_6e[i],"cellSelected");
}
for(var i=0;i<this.targetColumn.length;i++){
this.cssUtil.removeClassNames(this.targetColumn[i],"cellSelected");
var _70=Element.getTagNodes(this.targetColumn[i],true);
for(var j=0;j<_70.length;j++){
this.cssUtil.removeClassNames(_70.length[j],"cellSelected");
}
}
}
if(this.targetCell){
this.cssUtil.removeClassNames(this.targetCell,"cellSelected");
var _6e=Element.getTagNodes(this.targetCell,true);
for(var i=0;i<_6e.length;i++){
this.cssUtil.removeClassNames(_6e[i],"cellSelected");
}
}
if(this.targetRow){
this.cssUtil.removeClassNames(this.targetRow,"cellSelected");
var _6e=Element.getTagNodes(this.targetRow,true);
for(var i=0;i<_6e.length;i++){
this.cssUtil.removeClassNames(_6e[i],"cellSelected");
}
this.cssUtil.removeClassNames(this.targetIdRow,"cellSelected");
_6e=Element.getTagNodes(this.targetIdRow,true);
for(var i=0;i<_6e.length;i++){
this.cssUtil.removeClassNames(_6e[i],"cellSelected");
}
}
this.targetHeaderCell=null;
this.targetColumn=null;
this.targetColIndex=null;
this.targetCell=null;
this.targetRowIndex=null;
this.targetRow=null;
this.targetIdRow=null;
},updateId:function(){
var _72=this.idTable.rows;
for(var i=0;i<_72.length;i++){
var _74=document.getElementsByClassName(this.custumCss.idCellVal,_72[i])[0];
_74.innerHTML=i+1;
}
},updateRowLine:function(_75){
var _76=this.cellTable.rows[this.targetRowIndex];
var _77=this.targetIdRow.rowIndex;
var _78=_76.parentNode;
var _79=_78.rows;
if(this.options.hierarchy){
var _7a=_78.rows[this.targetIdRow.rowIndex];
if(this.isParentRow(_7a,_76)){
var _7b=this.idTable.tBodies[0];
_7b.insertBefore(this.targetIdRow,_7b.rows[this.targetRowIndex]);
return;
}
}
if(_77==_79.length-1){
_78.appendChild(_76);
}else{
if(this.targetRowIndex<_77){
_78.insertBefore(_76,_79[_77+1]);
}else{
_78.insertBefore(_76,_79[_77]);
}
}
if(this.options.hierarchy){
this.updateOutline(_76);
}
this.targetRowIndex=_77;
this.updateId();
},updateCellLine:function(_7c){
var _7d=this.getCells(this.targetColIndex);
var _7e=this.targetHeaderCell.cellIndex;
var _7f=this.cellTable.rows;
for(var i=0;i<_7f.length;i++){
var _81=_7f[i].cells;
var _82=_7d[i];
if(_7e==_81.length-1){
_7f[i].appendChild(_82);
}else{
if(this.targetColIndex<_7e){
_7f[i].insertBefore(_82,_81[_7e+1]);
}else{
_7f[i].insertBefore(_82,_81[_7e]);
}
}
}
this.targetColIndex=_7e;
},updateCellWidth:function(_83,_84){
if(_83.width>this.options.cellMinWidth){
var _85=Element.getElementsByClassName(_84,Grid.className.headerCellDrag)[0];
var _86=Element.getElementsByClassName(_84,Grid.className.headerCellSort)[0];
var val=_83.width-(parseInt(Element.getStyle(_84,"width")));
val=parseInt(val);
this.headerTable.style.width=(parseInt(Element.getStyle(this.headerTable,"width"))+val)+"px";
_85.style.width=_83.width-(this.marginSize*2)+"px";
_86.style.width=parseInt(Element.getStyle(_85,"width"))-(this.marginSize*2)+"px";
var _88=_84.cellIndex;
var _89=this.cellTable.rows;
this.cellTable.style.width=this.headerTable.style.width;
for(var i=0;i<_89.length;i++){
var _8b=_89[i].cells[_88];
var _8c=Element.getElementsByClassName(_8b,Grid.className.cellVal)[0];
_8c.style.width=_83.width+"px";
_8b.style.width=_83.width+"px";
}
}
},updateCellHeight:function(_8d,_8e){
if(_8d.height>this.options.cellMinHeight){
var row=_8e.parentNode;
var _90=row.rowIndex;
var _91=Element.getElementsByClassName(_8e,Grid.className.idCellVal)[0];
_91.style.height=_8d.height-(this.marginSize*2)+"px";
var _92=parseInt(_91.style.paddingTop);
var _93=this.cellTable.rows[_90];
var _94=_93.cells;
for(var i=0;i<_94.length;i++){
_94[i].style.height=_8d.height+"px";
var _96=Element.getElementsByClassName(_94[i],Grid.className.cellVal)[0];
_96.style.height=_8d.height+"px";
}
}
},setTextAreaSize:function(_97){
var _98=Event.element(_97);
var _99=parseInt(Element.getStyle(_98,"height"));
var _9a=parseInt(Element.getStyle(_98,"width"));
_98.ajax.options.rows=Math.round(_99/20);
_98.ajax.options.cols=Math.round(_9a/20);
},fixTablePosition:function(_9b){
Grid.scrollTop=this.element.scrollTop;
Grid.scrollLeft=this.element.scrollLeft;
this.baseTable.style.top=Grid.scrollTop+"px";
this.baseTable.style.left=Grid.scrollLeft+"px";
this.headerTable.style.top=Grid.scrollTop+"px";
this.idTable.style.left=Grid.scrollLeft+"px";
},createInplaceEditorParams:function(_9c,_9d){
var _9e=_9c.id.indexOf("_col_",0);
var _9f=_9c.id.substring(this.rowIdBase.length,_9e);
var _a0=_9e+"_col_".length;
var _a1=_9c.id.indexOf("_form",0);
var _a2=_9c.id.substring(_a0,_a1);
var _a3=this.rowDataToJsonObj(_9f);
var _a4=this.getColTitle(_a2);
_a3.each(function(j){
if(j.column==_a4){
j.value=_9d;
throw $break;
}
});
var _a6=JSON.stringify(_a3);
var _a7={rowData:_a6,column:_a4,value:_9d};
return $H(_a7).toQueryString();
},gridDataToJsonObj:function(){
var _a8=this.cellTable.rows;
var _a9=[];
for(var i=0;i<this.rowLength;i++){
var _ab=this.rowDataToJsonObj(i);
_a9.push(_ab);
}
return _a9;
},rowDataToJsonObj:function(_ac){
var _ad=[];
var row=$A(this.cellTable.rows).detect(function(r){
return r.id.getSuffix()==_ac;
});
for(var i=0;i<this.colLength;i++){
var _b1={};
_b1["column"]=this.getColTitle(i);
_b1["value"]=Element.collectTextNodes(row.cells[i]);
_ad.push(_b1);
}
return _ad;
},updateGridData:function(){
var _b2=JSON.stringify(this.gridDataToJsonObj());
var _b3="id="+encodeURIComponent(this.element.id)+"&data="+encodeURIComponent(_b2);
new Ajax.Updater(this.options.updateGridReceiver,this.options.updateGridUrl,{parameters:_b3,evalScripts:true,asynchronous:true});
},setHierarchyRow:function(row,_b5,_b6){
row.outlineLevel=_b5||1;
row.list=row.list||new Array();
if(row.outlineLevel==1){
this.topLevelList.push(row);
row.outlineNum=_b6||this.topLevelList.length;
}else{
var _b7=this.getParentRow(row);
_b7.list.push(row);
var num=_b7.length;
row.outlineNum=_b6||_b7.outlineNum+"."+_b7.list.length;
}
this.buildStateDiv(row.cells[this.hierarchyColIndex]);
this.setOutlineIndent(row);
this.setFontWeight(row);
},buildStateDiv:function(_b9){
var _ba=Builder.node("div");
var _bb=Element.getElementsByClassName(_b9,Grid.className.cellVal)[0];
this.cssUtil.addClassNames(_ba,"state");
_b9.insertBefore(_ba,_bb);
if(document.all){
_ba.style.position="absolute";
}else{
_ba.style.position="relative";
_ba.style.cssFloat="left";
}
this.addStateClass(_b9,_ba);
Event.observe(_ba,"click",this.toggleState.bindAsEventListener(this));
},addStateClass:function(_bc,_bd){
var row=_bc.parentNode;
if(row.list.length==0){
this.cssUtil.addClassNames(_bd,"stateEmpty");
}else{
if(this.options.open){
this.cssUtil.addClassNames(_bd,"stateOpen");
}else{
this.cssUtil.addClassNames(_bd,"stateClose");
this.closeRow(row);
}
}
},toggleState:function(_bf){
var src=Event.element(_bf);
var row=src.parentNode.parentNode;
if(!Element.hasClassName(src,Grid.className.stateEmpty)){
if(Element.hasClassName(src,Grid.className.stateOpen)){
this.closeRow(row.list);
this.cssUtil.removeClassNames(src,"stateOpen");
this.cssUtil.addClassNames(src,"stateClose");
}else{
this.openRow(row.list);
this.cssUtil.removeClassNames(src,"stateClose");
this.cssUtil.addClassNames(src,"stateOpen");
}
}
},openRow:function(_c2){
for(var i=0;i<_c2.length;i++){
var row=_c2[i];
Element.show(row);
Element.show(this.getIdRow(row));
var _c5=Element.getElementsByClassName(row.cells[this.hierarchyColIndex],Grid.className.state)[0];
if(Element.hasClassName(_c5,Grid.className.stateOpen)){
this.openRow(row.list);
}
}
},closeRow:function(_c6){
for(var i=0;i<_c6.length;i++){
Element.hide(_c6[i]);
Element.hide(this.getIdRow(_c6[i]));
this.closeRow(_c6[i].list);
}
},showStateDiv:function(_c8,_c9){
var row=Element.getParentByTagName(["TR"],_c9);
var _cb=Element.getElementsByClassName(row,Grid.className.state)[0];
Element.show(_cb);
},hideStateDiv:function(_cc){
var src=Event.element(_cc);
var row=Element.getParentByTagName(["TR"],src);
var _cf=Element.getElementsByClassName(row,Grid.className.state)[0];
Element.hide(_cf);
},addHierarchyRow:function(_d0,_d1){
if(this.colLength==0){
return;
}
var _d2=this.addRow(_d0,_d1);
_d2.list=new Array();
var _d3=_d2.previousSibling;
var _d4=null;
var _d5=null;
var _d6=0;
if(!_d3){
_d2.outlineLevel=1;
_d5=this.topLevelList;
}else{
if(_d3.list.length>0){
_d2.outlineLevel=_d3.outlineLevel+1;
_d4=_d3;
_d5=_d4.list;
}else{
_d2.outlineLevel=_d3.outlineLevel;
_d4=this.getParentRow(_d3);
_d5=_d4?_d4.list:this.topLevelList;
_d6=_d5.indexOf(_d3)+1;
}
}
_d5.insert(_d6,_d2);
this.buildStateDiv(_d2.cells[this.hierarchyColIndex]);
for(var i=_d6;i<_d5.length;i++){
if(_d5[i].outlineLevel!=1){
_d5[i].outlineNum=_d4.outlineNum+"."+(i+1);
}else{
_d5[i].outlineNum=i+1;
}
this.setOutline(_d5[i]);
}
},deleteHierarchyRow:function(_d8){
if(isNaN(_d8)||_d8>=this.rowLength){
return;
}
var row=this.getRow(_d8);
if(!row){
return;
}
var _da=this.getParentRow(row);
var _db=_da?_da.list:this.topLevelList;
var _dc=_db.indexOf(row);
var _dd=row.list;
for(var i=0;i<_dd.length;i++){
this.deleteChildRow(_dd[i]);
}
_db.remove(_dc);
this.deleteRow(_d8);
for(var i=_dc;i<_db.length;i++){
var _df=_db[i];
if(_df.outlineLevel==1){
_df.outlineNum=i+1;
}else{
_df.outlineNum=this.getParentRow(_df).outlineNum+"."+(i+1);
}
this.setOutline(_db[i]);
}
this.setFontWeight(_da);
},deleteChildRow:function(_e0){
var _e1=_e0.list;
for(var i=0;i<_e1.length;i++){
this.deleteChildRow(_e1[i]);
}
this.deleteRow(_e0.rowIndex);
},levelUp:function(row){
if(!row){
return;
}
var _e4=row.previousSibling;
if(row.outlineLevel==1||!_e4){
return;
}
var _e5=this.getParentRow(row);
var _e6=_e5.list;
var _e7=this.getParentRow(_e5);
var _e8=_e7?_e7.list:this.topLevelList;
var _e9=_e6.indexOf(row);
var _ea=_e8.indexOf(_e5)+1;
row.outlineLevel-=1;
_e8.insert(_ea,row);
_e6.remove(_e9);
while(_e6[_e9]){
var _eb=_e6[_e9];
row.list.push(_eb);
_e6.remove(_e9);
}
if(row.outlineLevel!=1){
row.outlineNum=_e7.outlineNum+"."+(_ea+1);
}else{
row.outlineNum=_ea+1;
}
this.setOutline(row);
for(var i=_ea+1;i<_e8.length;i++){
if(_e8[i].outlineLevel!=1){
_e8[i].outlineNum=_e7.outlineNum+"."+(i+1);
}else{
_e8[i].outlineNum=i+1;
}
this.setOutline(_e8[i]);
}
this.setFontWeight(row);
this.setFontWeight(_e5);
this.setFontWeight(_e7);
this.setStateClass(row);
this.setStateClass(_e4);
},levelDown:function(row){
if(!row){
return;
}
var _ee=row.previousSibling;
var _ef=this.getParentRow(row);
if(!_ee||_ef==_ee){
return;
}
var _f0=(row.outlineLevel==1)?this.topLevelList:_ef.list;
var _f1=_f0.indexOf(row);
row.outlineLevel+=1;
var _f2=this.getParentRow(row);
var _f3=_f2.list;
var _f4=_f3.length;
_f0.remove(_f1);
_f3.push(row);
row.outlineNum=_f2.outlineNum+"."+(_f4+1);
this.setOutline(row);
for(var i=_f1;i<_f0.length;i++){
if(_f0[i].outlineLevel!=1){
_f0[i].outlineNum=_ef.outlineNum+"."+(i+1);
}else{
_f0[i].outlineNum=i+1;
}
this.setOutline(_f0[i]);
}
for(var i=_f4+1;i<_f3.length;i++){
_f3[i].outlineNum=_f2.outlineNum+"."+(i+1);
this.setOutline(_f3[i]);
}
this.setFontWeight(row);
this.setFontWeight(_ef);
this.setFontWeight(_f2);
this.setStateClass(row);
this.setStateClass(_ee);
},setStateClass:function(row){
if(!row.list){
return;
}
var _f7=Element.getElementsByClassName(row.cells[this.hierarchyColIndex],Grid.className.state)[0];
if(Element.hasClassName(_f7,Grid.className.stateEmpty)&&row.list.length>0){
this.cssUtil.removeClassNames(_f7,"stateEmpty");
this.cssUtil.addClassNames(_f7,"stateOpen");
}else{
if(!Element.hasClassName(_f7,Grid.className.stateEmpty)&&row.list.length==0){
this.cssUtil.removeClassNames(_f7,"stateOpen");
this.cssUtil.addClassNames(_f7,"stateEmpty");
}
}
},setOutline:function(row){
var _f9=row.list;
if(!_f9){
return;
}
for(var i=0;i<_f9.length;i++){
var _fb=_f9[i];
_fb.outlineLevel=row.outlineLevel+1;
_fb.outlineNum=row.outlineNum+"."+(i+1);
this.setOutline(_fb);
}
this.setOutlineIndent(row);
},setOutlineIndent:function(row){
var _fd=row.cells[this.hierarchyColIndex];
if(!_fd){
return;
}
var _fe=Element.getElementsByClassName(_fd,Grid.className.cellVal)[0];
var _ff=Element.getElementsByClassName(_fd,Grid.className.state)[0];
if(!_ff){
return;
}
var _100=_ff.offsetWidth||this.stateDivWidth;
var left=this.options.hierarchyIndent*(row.outlineLevel-1);
var _102=document.all?left+_100:left;
_fe.style.left=_102+"px";
_ff.style.left=left+"px";
},setFontWeight:function(row){
if(row){
if(!row.list){
return;
}
if(row.list.length>0){
row.style.fontWeight="bold";
}else{
row.style.fontWeight="normal";
}
}
},updateOutline:function(row){
var _105=row.previousSibling;
var _106=null;
var _107=0;
var _108=null;
if(!_105.list){
return;
}
if(!_105){
_108=this.topLevelList;
}else{
if(_105.list.length>0){
_106=_105;
_108=_106.list;
}else{
_106=this.getParentRow(_105);
_108=_106?_106.list:this.topLevelList;
_107=_108.indexOf(_105)+1;
}
}
var _109=this.getParentRowByIndex(row,this.targetRowIndex);
var _10a=null;
var _10b="";
if(_109){
_10a=_109.list;
_10b=_109.outlineNum+".";
}else{
_10a=this.topLevelList;
if(!_10a){
return;
}
}
var _10c=_10a.indexOf(row);
if(_10a==_108&&_10c<_107){
_107-=1;
_10a.remove(_10c);
_10a.insert(_107,row);
for(var i=_10c;i<_10a.length;i++){
try{
_10a[i].outlineNum=_10b+(i+1);
this.setOutline(_10a[i]);
}
catch(e){
}
}
}else{
_10a.remove(_10c);
_108.insert(_107,row);
for(var i=_10c;i<_10a.length;i++){
_10a[i].outlineNum=_10b+(i+1);
this.setOutline(_10a[i]);
}
var _10e=_106?_106.outlineNum+".":"";
var _10f=_106?_106.outlineLevel:0;
row.outlineNum=_10e+(_107+1);
row.outlineLevel=_10f+1;
for(var i=_107;i<_108.length;i++){
_108[i].outlineNum=_10e+(i+1);
this.setOutline(_108[i]);
}
this.setOutline(row);
}
this.setFontWeight(row);
this.setStateClass(row);
if(_109){
this.setFontWeight(_109);
this.setStateClass(_109);
}
if(_106){
this.setFontWeight(_106);
this.setStateClass(_106);
}
this.updateHierarchyRowLine(row);
},updateHierarchyRowLine:function(row,_111){
if(!row.list){
return;
}
var _112=row.rowIndex;
var _113=this.cellTable.tBodies[0];
var _114=this.idTable.tBodies[0];
if(!_111){
_111=_113.rows[_112+1];
}
for(var i=0;i<row.list.length;i++){
if(!_111){
_113.appendChild(row.list[i]);
_114.appendChild(this.getIdRow(row.list[i]));
}else{
_113.insertBefore(row.list[i],_111);
_114.insertBefore(this.getIdRow(row.list[i]),this.getIdRow(_111));
}
this.updateHierarchyRowLine(row.list[i],_111);
}
},getParentRow:function(row){
if(row.outlineLevel==1){
return null;
}
var _117=row.previousSibling;
if(!_117){
return null;
}
try{
while(_117.outlineLevel!=(row.outlineLevel-1)){
_117=_117.previousSibling;
}
return _117;
}
catch(e){
}
},getParentRowByIndex:function(row,_119){
if(row.outlineLevel==1){
return null;
}
if(this.targetRowIndex){
for(var i=0;i<this.targetRowIndex+1;i++){
if(!this.cellTable.rows[i].list){
return;
}
if(this.cellTable.rows[i].list.indexOf(row)!=-1){
return this.cellTable.rows[i];
}
}
}
return null;
},getPreviousRootRow:function(row){
var _11c=row.previousSibling;
if(!_11c){
return;
}
while(_11c.outlineLevel!=1){
_11c=_11c.previousSibling;
}
return _11c;
},isParentRow:function(row,_11e){
var temp=this.getParentRow(row);
if(!temp){
return false;
}else{
if(temp==_11e){
return true;
}else{
return this.isParentRow(temp,_11e);
}
}
}};

