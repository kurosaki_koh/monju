var TimePicker=Class.create();
TimePicker.className={container:"timepicker_container",header:"timepicker_header",preYears:"timepicker_preYears",nextYears:"timepicker_nextYears",years:"timepicker_years",mark:"timepicker_mark",ym:"timepicker_ym",table:"timepicker_table",thRight:"right",tdRight:"right",tdBottom:"bottom",date:"timepicker_date",holiday:"timepicker_holiday",regularHoliday:"timepicker_regularHoliday",schedule:"timepicker_schedule",highlightDay:"timepicker_highlightDay",scheduleListContainer:"timepicker_scheduleListContainer",scheduleItem:"timepicker_scheduleItem",scheduleTimeArea:"timepicker_scheduleItemTimeArea",scheduleHandler:"timepicker_scheduleHandler",holidayName:"timepicker_holidayName",dateContainer:"timepicker_dateContainer",tableHeader:"timepicker_tableHeader",rowContent:"timepicker_rowContent",selected:"timepicker_selected",displayToggle:"timepicker_displayToggle",nextYearMark:"timepicker_nextYearMark",nextMonthMark:"timepicker_nextMonthMark",nextWeekMark:"timepicker_nextWeekMark",preYearMark:"timepicker_preYearMark",preMonthMark:"timepicker_preMonthMark",preWeekMark:"timepicker_preWeekMark",weekTable:"timepicker_weekContainerTable",weekMainTable:"timepicker_weekMainTable",timeLine:"timepicker_timeline",timeLineTimeTop:"timepicker_timelineTimeTop",timeLineTime:"timepicker_timelineTime",timeLineTimeIe:"timepicker_timelineTime_ie",timeLineTimeIeTop:"timepicker_timelineTime_ieTop",headerColumn:"timepicker_headerColumn",columnTopDate:"timepicker_columnTopDate",columnDate:"timepicker_columnDate",columnDateOdd:"timepicker_columnOddDate",scheduleItemSamll:"timepicker_scheduleItemSmall",scheduleItemLarge:"timepicker_scheduleItemLarge",scheduleItemSelect:"timepicker_scheduleItemSelect",deleteImg:"timepicker_deleteImage",privateImg:"timepicker_privateImage",scheduleContainer:"timepicker_weekScheduleContainer",selector:"timepicker_selector",cover:"timepicker_cover"};
Object.extend(TimePicker.prototype,Calendar.prototype);
Object.extend(TimePicker.prototype,{initialize:function(_1){
this.options=Object.extend({initDate:new Date(),cssPrefix:"custom_",holidays:[],schedules:[],size:Calendar.size.large,regularHoliday:[0,6],displayIndexes:[0,1,2,3,4,5,6],displayTime:[{hour:0,min:0},{hour:24,min:0}],weekIndex:0,dblclickListener:null,afterSelect:Prototype.emptyFunction,beforeRefresh:Prototype.emptyFunction,changeSchedule:Prototype.emptyFunction,changeCalendar:Prototype.emptyFunction,displayType:"month",highlightDay:true,beforeRemoveSchedule:function(){
return true;
},dblclickSchedule:null,updateTirm:Prototype.emptyFunction,displayTimeLine:true,clickDateText:null,getMonthHeaderText:Prototype.emptyFunction,getMonthSubHeaderText:Prototype.emptyFunction,getWeekHeaderText:Prototype.emptyFunction,getWeekSubHeaderText:Prototype.emptyFunction,getDayHeaderText:Prototype.emptyFunction,setPosition:true,headerTitle:"",standardTime:false,oneDayLabel:"24H",standardTimeLabel:"standard"},arguments[1]||{});
if(this.options.standardTime){
this.options.displayTime=this.options.standardTime;
this.options.oneDay=[{hour:0,min:0},{hour:24,min:0}];
}
this.element=$(_1);
this.date=new Date();
var _2=CssUtil.appendPrefix(this.options.cssPrefix,TimePicker.className);
this.classNames=new CssUtil([TimePicker.className,_2]);
this.builder=new TimePickerBuilder(this);
this.builder.beforeBuild();
this.calendar=this.builder.build();
this.builder.afterBuild();
this.element.appendChild(this.calendar);
Element.hide(_1);
Element.setStyle(this.element,{position:"absolute"});
Event.observe(document,"mouseup",this.onMouseUp.bindAsEventListener(this));
},refresh:function(){
this.options.beforeRefresh(this);
this.destroy();
this.selectedBase=null;
Element.remove(this.calendar);
this.builder=new TimePickerBuilder(this);
this.builder.beforeBuild();
this.calendar=this.builder.build();
this.element.appendChild(this.calendar);
this.builder.afterBuild();
},show:function(_3,_4){
Event.stop(_3);
var _5=[Event.pointerX(_3),Event.pointerY(_3)];
if(this.options.setPosition.constructor==Function){
this.options.setPosition(this.element,_5);
}else{
if(this.options.setPosition){
var _6=Position.cumulativeOffset(this.element.parentNode);
Element.setStyle(this.element,{left:_5[0]-_6[0]+"px",top:_5[1]-_6[1]+"px"});
}
}
Element.setStyle(this.element,{zIndex:ZindexManager.getIndex()});
Element.show(this.element);
this.builder.setColumnWidth();
this.builder.setCover();
},hide:function(){
Element.hide(this.element);
this.clearSelected();
},setTrigger:function(_7,_8){
_7=$(_7);
Event.observe(_7,"click",this.show.bindAsEventListener(this));
},setTargets:function(_9){
this.targets=_9;
},onMouseUp:function(_a){
var _b=this;
var _c=Element.getDimensions(this.element);
var _d=Position.cumulativeOffset(this.element);
var x=Event.pointerX(_a);
var y=Event.pointerY(_a);
if((x<_d[0])||((_d[0]+_c.width)<x)||(y<_d[1])||((_d[1]+_c.height)<y)){
this.hide();
}
if(_b.mouseDown){
setTimeout(function(){
if(_b.mouseDown){
_b.mouseDown=false;
_b.hide();
}
},10);
}
var _10=this.builder.getSelectedTerm();
if(_10){
var _11=_10.first();
var _12=_10.last();
this.setTime(this.targets.start.hour,_11.getHours());
this.setTime(this.targets.start.min,_11.getMinutes());
this.setTime(this.targets.finish.hour,_12.getHours());
this.setTime(this.targets.finish.min,_12.getMinutes());
this.hide();
}
},setTime:function(_13,_14){
$A($(_13).options).each(function(_15){
if(_15.value==_14){
_15.selected=true;
}else{
_15.selected=false;
}
});
}});
var TimePickerBuilder=Class.create();
Object.extend(TimePickerBuilder.prototype,CalendarDay.prototype);
Object.extend(TimePickerBuilder.prototype,{initialize:function(_16){
var day=_16.date.getDay();
this.calendar=_16;
this.setDisplayTime();
this.calendar.options.displayIndexesOld=this.calendar.options.displayIndexes;
this.calendar.options.displayIndexes=[day];
this.calendar.options.weekIndexOld=this.calendar.options.weekIndex;
this.calendar.options.weekIndex=day;
this.week=this.getWeek();
},buildHeader:function(){
var _18=Builder.node("TR");
_18.appendChild(this.buildHeaderCenter());
className=this.calendar.classNames.joinClassNames("header");
var _19=Builder.node("TBODY",[_18]);
return Builder.node("TABLE",{className:className},[_19]);
},buildHeaderCenter:function(){
var _1a=[];
var _1b=Builder.node("SPAN",[this.calendar.options.headerTitle]);
_1a.push(_1b);
var _1c=Builder.node("TD",_1a);
return _1c;
},buildTimeLine:function(){
var _1d=new Date();
var _1e=0,_1f=24;
_1d.setHours(_1e);
_1d.setMinutes(0);
var _20=[];
var _21="timeLineTime";
var _22="timeLineTime";
if(UserAgent.isIE()){
_21="timeLineTimeIe";
_22="timeLineTimeIeTop";
}
var _23=Builder.node("DIV");
this.calendar.classNames.addClassNames(_23,"timeLineTimeTop");
_20.push(_23);
while(_1e<_1f){
if(this.includeDisplayTime(_1e)){
_23=Builder.node("DIV",[this.formatTime(_1d)]);
if(_20.length==0){
this.calendar.classNames.addClassNames(_23,_22);
}else{
this.calendar.classNames.addClassNames(_23,_21);
}
_20.push(_23);
}
_1e++;
_1d.setHours(_1e);
}
var td=Builder.node("TD",_20);
this.calendar.classNames.addClassNames(td,"timeLine");
return td;
},buildCalendarHeader:function(){
var _25=null;
if(this.calendar.options.displayTime==this.calendar.options.standardTime){
_25=Builder.node("DIV",[this.calendar.options.oneDayLabel]);
}else{
_25=Builder.node("DIV",[this.calendar.options.standardTimeLabel]);
}
Event.observe(_25,"click",this.toggleDisplayTime.bindAsEventListener(this,_25));
this.calendar.classNames.addClassNames(_25,"headerColumn");
return Builder.node("TR",[Builder.node("TD",{align:"center"},[_25])]);
},abstractSelect:function(_26,_27){
var _28=this.findClickedElement(_26);
if(_28){
if(Element.hasClassName(_28,TimePicker.className.columnDate)||Element.hasClassName(_28,TimePicker.className.columnDateOdd)||Element.hasClassName(_28,TimePicker.className.columnTopDate)){
var _29=this.getDate(_28);
_27(_29,_28);
}
}
},toggleDisplayTime:function(_2a,_2b){
Event.stop(_2a);
if(this.calendar.options.displayTime==this.calendar.options.oneDay){
this.calendar.options.displayTime=this.calendar.options.standardTime;
}else{
this.calendar.options.displayTime=this.calendar.options.oneDay;
}
this.calendar.refresh();
},findClickedElement:function(_2c){
var _2d=$(this.getScheduleContainerId());
var _2e=Position.cumulativeOffset(_2d);
var x=Event.pointerX(_2c)-_2e[0];
var y=Event.pointerY(_2c)-_2e[1];
var _31=this.calendarTable.rows[0].cells[0].getElementsByTagName("div");
var _32=parseInt(Element.getHeight(_2d),10)/_31.length;
var _33=Math.floor(y/_32);
return _31[_33];
},beforeBuild:function(){
this.column={};
var _34=CssUtil.getCssRuleBySelectorText("."+TimePicker.className.columnDate);
this.column.height=parseInt(_34.style["height"],10)+1;
},afterBuild:function(){
this.setColumnWidth();
this.setCover();
}});

