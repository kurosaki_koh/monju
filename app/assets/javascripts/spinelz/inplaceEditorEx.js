InPlaceEditorEx=Class.create();
Object.extend(Object.extend(InPlaceEditorEx.prototype,Ajax.InPlaceEditor.prototype),{initialize:function(_1,_2){
this.element=$(_1);
this.options=Object.extend({okButton:true,okText:"ok",cancelLink:true,cancelText:"cancel",savingText:"Saving...",clickToEditText:"Click to edit",okText:"ok",rows:1,onComplete:function(_3,_4){
new Effect.Highlight(_4,{startcolor:this.options.highlightcolor});
},onFailure:function(_5){
alert("Error communicating with the server: "+_5.responseText.stripTags());
},callback:function(_6){
return Form.serialize(_6);
},handleLineBreaks:true,loadingText:"Loading...",savingClassName:"inplaceeditor-saving",loadingClassName:"inplaceeditor-loading",formClassName:"inplaceeditor-form",highlightcolor:Ajax.InPlaceEditor.defaultHighlightColor,highlightendcolor:"#FFFFFF",externalControl:null,submitOnBlur:false,ajaxOptions:{},evalScripts:false},_2||{});
if(!this.options.formId&&this.element.id){
this.options.formId=this.element.id+"-inplaceeditor";
if($(this.options.formId)){
this.options.formId=null;
}
}
if(this.options.externalControl){
this.options.externalControl=$(this.options.externalControl);
}
this.originalBackground=Element.getStyle(this.element,"background-color");
if(!this.originalBackground){
this.originalBackground="transparent";
}
this.element.title=this.options.clickToEditText;
this.ondblclickListener=this.enterEditMode.bindAsEventListener(this);
this.mouseoverListener=this.enterHover.bindAsEventListener(this);
this.mouseoutListener=this.leaveHover.bindAsEventListener(this);
Event.observe(this.element,"dblclick",this.ondblclickListener);
Event.observe(this.element,"mouseover",this.mouseoverListener);
Event.observe(this.element,"mouseout",this.mouseoutListener);
if(this.options.externalControl){
Event.observe(this.options.externalControl,"dblclick",this.ondblclickListener);
Event.observe(this.options.externalControl,"mouseover",this.mouseoverListener);
Event.observe(this.options.externalControl,"mouseout",this.mouseoutListener);
}
},getText:function(){
return this.element.innerHTML.unescapeHTML();
},onSubmit:function(){
var _7=this.editField.value.escapeHTML();
if(_7==""){
this.onclickCancel();
return false;
}else{
this.onLoading();
this.element.innerHTML=_7;
new InPlaceEditorEx(this.element.id,this.options);
return true;
}
},removeForm:function(){
if(this.form){
if(this.form.parentNode){
var _8=this.form.parentNode.childNodes;
for(var i=0;i<_8.length;i++){
_8[i].data="";
}
Element.remove(this.form);
}
this.form=null;
}
}});

