var AjaxHistory={_callback:null,_currentIframeHash:"",_currentLocationHash:"",_prefix:"ajax_history_",add:function(_1){
AjaxHistoryPageManager.setHash(this._prefix+_1);
},checkIframeHash:function(){
var _2=AjaxHistoryIframeManager.getHash().substr(this._prefix.length);
if(this._currentIframeHash!=_2){
this._currentIframeHash=_2;
this._currentLocationHash=_2;
AjaxHistoryPageManager.setHash((_2)?this._prefix+_2:"");
this.doEvent(_2);
}else{
this.checkLocationHash();
}
},checkHash:function(){
if(UserAgent.isIE()){
this.checkIframeHash();
}else{
this.checkLocationHash();
}
},checkLocationHash:function(){
var _3=AjaxHistoryPageManager.getHash().substr(this._prefix.length);
if(this._currentLocationHash!=_3){
this._currentLocationHash=_3;
if(UserAgent.isIE()){
AjaxHistoryIframeManager.setHash(this._prefix+_3);
}else{
this.doEvent(_3);
}
}
},doEvent:function(_4){
if(this._callback){
this._callback.call(null,_4);
}
},init:function(_5){
this._callback=_5;
if(UserAgent.isIE()){
AjaxHistoryIframeManager.create();
}
var _6=this;
var _7=function(){
_6.checkHash();
};
setInterval(_7,100);
}};
var AjaxHistoryIframeManager={_id:"ajax_history_frame",_element:null,_src:IECover.src,create:function(){
document.write("<iframe id=\""+this._id+"\" src=\""+this._src+"\" style=\"display: none;\"></iframe>");
this._element=$(this._id);
},getHash:function(){
var _8=this._element.contentWindow.document;
return _8.location.hash.replace(/^#/,"");
},setHash:function(_9){
var _a=this._element.contentWindow.document;
_a.open();
_a.close();
_a.location.hash=_9;
}};
var AjaxHistoryPageManager={_delimiter:"#",_location:"window.location.href",_query:"",getLocation:function(){
return eval(this._location);
},getHash:function(){
var _b=this.getLocation().split(this._delimiter);
return (_b.length>1)?_b[_b.length-1]:this._query;
},getUrl:function(){
var _c=this.getLocation().split(this._delimiter);
return _c[0];
},setHash:function(_d){
window.location.hash=_d;
}};

