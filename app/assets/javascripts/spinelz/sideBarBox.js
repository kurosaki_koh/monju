SideBarBox=Class.create();
SideBarBox.className={panelContainer:"sideBarBox_panelContainer",tabContainer:"sideBarBox_tabContainer",title:"sideBarBox_tabTitle",tab:"sideBarBox_tab",tabTopInactive:"sideBarBox_tabTopInactive",tabTopActive:"sideBarBox_tabTopActive",tabMiddleInactive:"sideBarBox_tabMiddleInactive",tabMiddleActive:"sideBarBox_tabMiddleActive",tabBottomInactive:"sideBarBox_tabBottomInactive",tabBottomActive:"sideBarBox_tabBottomActive"};
SideBarBox.prototype={initialize:function(_1){
var _2=Object.extend({selected:1,beforeSelect:function(){
return true;
},afterSelect:Prototype.emptyFunction,visible:false,close:true,cssPrefix:"custom_"},arguments[1]||{});
this.options=_2;
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
var _3=CssUtil.appendPrefix(this.options.cssPrefix,SideBarBox.className);
this.classNames=new CssUtil([SideBarBox.className,_3]);
this.start();
Element.setStyle(this.element,{visibility:"visible"});
},start:function(){
this.tabs=[];
this.panelContents=[];
this.tabSets=[];
this.visible=this.options.visible;
this.selected=(this.options.selected>0)?this.options.selected-1:0;
this.selected=(this.visible)?this.selected:-1;
this.tabId=this.element.id+"_tab";
this.tabTopId=this.tabId+"_top";
this.tabMiddleId=this.tabId+"_middle";
this.tabBottomId=this.tabId+"_bottom";
this.tabContainerId=this.element.id+"_tabContainer";
this.panelId=this.element.id+"_panel";
this.panelContainerId=this.element.id+"_panelContainer";
this.tabContainer=null;
this.panelContainer=null;
this.buildTabBox();
},buildTabBox:function(){
this.buildContainers();
Element.cleanWhitespace(this.element);
this.tabSets=this.element.childNodes;
if(this.visible&&this.selected>=this.tabSets.length){
this.selected=0;
}
var i=0;
while(this.tabSets.length>0){
var _5=this.tabSets[0];
var _6=$A(_5.childNodes).detect(function(c){
return (c.nodeType==1)&&(c.tagName.toLowerCase()=="div");
});
this.buildPanel(_6,i);
this.buildTab(_5,i);
i++;
}
this.addContainers();
},buildContainers:function(){
this.tabContainer=Builder.node("div",{id:this.tabContainerId});
this.classNames.addClassNames(this.tabContainer,"tabContainer");
this.panelContainer=Builder.node("div",{id:this.panelContainerId});
this.classNames.addClassNames(this.panelContainer,"panelContainer");
if(!this.visible){
Element.hide(this.panelContainer);
}
},addContainers:function(){
this.element.appendChild(this.panelContainer);
this.element.appendChild(this.tabContainer);
this.element.appendChild(Builder.node("div",{style:"clear: left"}));
},buildTab:function(_8,i){
var _a=_8.childNodes;
_8.id=this.tabId+i;
this.classNames.addClassNames(_8,"tab");
var _b=Builder.node("div",{id:this.tabTopId+i});
var _c=Builder.node("div",{id:this.tabMiddleId+i},$A(_a));
var _d=Builder.node("div",{id:this.tabBottomId+i});
_8.appendChild(_b);
_8.appendChild(_c);
_8.appendChild(_d);
Event.observe(_8,"click",this.selectTab.bindAsEventListener(this));
this.tabs[i]=_8;
this.tabContainer.appendChild(_8);
if(i!=this.selected){
this.setTabInactive(_8);
}else{
this.setTabActive(_8);
}
},buildPanel:function(_e,i){
var _10=Builder.node("div",{id:this.panelId+i});
_10.appendChild(_e);
this.panelContents[i]=_10;
if(i!=this.selected){
Element.hide(_10);
}
this.panelContainer.appendChild(_10);
},selectTab:function(e){
if(!this.options.beforeSelect()){
return;
}
if(!e){
this.setTabActive(this.tabs[this.selected]);
Element.show(this.panelList[this.selected]);
return;
}
var _12=this.panelContents[this.selected];
var _13=this.tabs[this.selected];
var _14=null;
if(e.nodeType){
_14=e;
}else{
_14=Event.element(e);
}
var _15=this.getTargetIndex(_14);
var _16=this.panelContents[_15];
var _17=this.tabs[_15];
if(this.visible){
if(_17.id==_13.id){
if(this.options.close){
Effect.SlideRightOutOfView(this.panelContainer);
this.visible=false;
this.selected=-1;
this.setTabInactive(_13);
Element.toggle(_16);
}
}else{
this.setTabActive(_17);
this.setTabInactive(_13);
Element.toggle(_12);
Element.toggle(_16);
this.selected=_15;
}
}else{
this.setTabActive(_17);
Element.toggle(_16);
Effect.SlideRightIntoView(this.panelContainer);
this.visible=true;
this.selected=_15;
}
this.options.afterSelect(_16,_12);
},setTabActive:function(tab){
var _19=tab.childNodes;
this.classNames.refreshClassNames(_19[0],"tabTopActive");
this.classNames.refreshClassNames(_19[1],"tabMiddleActive");
this.classNames.refreshClassNames(_19[2],"tabBottomActive");
},setTabInactive:function(tab){
var _1b=tab.childNodes;
this.classNames.refreshClassNames(_1b[0],"tabTopInactive");
this.classNames.refreshClassNames(_1b[1],"tabMiddleInactive");
this.classNames.refreshClassNames(_1b[2],"tabBottomInactive");
},getTargetIndex:function(_1c){
while(_1c){
if(_1c.id&&_1c.id.indexOf(this.tabId,0)>=0){
var _1d=_1c.id.substring(this.tabId.length);
if(!isNaN(_1d)){
return _1d;
}
}
_1c=_1c.parentNode;
}
},hasNextTab:function(){
return this.getNextTab()?true:false;
},hasPreviousTab:function(){
return this.getPreviousTab()?true:false;
},getNextTab:function(){
return Element.next(this.getCurrentTab());
},getPreviousTab:function(){
return Element.previous(this.getCurrentTab());
},selectNextTab:function(){
this.selectTab(this.getNextTab());
},selectPreviousTab:function(){
this.selectTab(this.getPreviousTab());
},tabCount:function(){
return this.tabs.inject(0,function(i,t){
return t?++i:i;
});
},getCurrentPanel:function(){
return this.panelContents[this.selected];
},getCurrentTab:function(){
return this.tabs[this.selected];
}};

