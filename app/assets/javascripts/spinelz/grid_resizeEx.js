ResizeableGridEx=Class.create();
Object.extend(Object.extend(ResizeableGridEx.prototype,Resizeable.prototype),{draw:function(_1){
var _2=[Event.pointerX(_1),Event.pointerY(_1)];
var _3=this.element.style;
var _4=0;
var _5=0;
var _6=0;
var _7=0;
if(this.currentDirection.indexOf("n")!=-1){
var _8=this.startY-_2[1];
var _9=Element.getStyle(this.element,"margin-top")||"0";
_4=this.startHeight+_8;
_6=(this.startTop-_8-parseInt(_9))+"px";
}
if(this.currentDirection.indexOf("w")!=-1){
var _8=this.startX-_2[0];
var _9=Element.getStyle(this.element,"margin-left")||"0";
_5=this.startWidth+_8;
_7=(this.startLeft-_8-parseInt(_9))+"px";
}
if(this.currentDirection.indexOf("s")!=-1){
_4=this.startHeight+_2[1]-this.startY;
}
if(this.currentDirection.indexOf("e")!=-1){
_5=this.startWidth+_2[0]-this.startX;
}
var _a={height:_4,width:_5,top:_6,left:_7};
if(this.options.draw){
this.options.draw(_a,this.element);
}
if(_4&&_4>this.options.minHeight){
_3.top=_a.top;
_3.height=_a.height+"px";
}
if(_5&&_5>this.options.minWidth){
_3.left=_a.left;
_3.width=_a.width+"px";
}
if(_3.visibility=="hidden"){
_3.visibility="";
}
},directions:function(_b){
var _c=[Event.pointerX(_b)+Grid.scrollLeft,Event.pointerY(_b)+Grid.scrollTop];
var _d=Position.cumulativeOffset(this.element);
var _e="";
if(this.between(_c[1]-_d[1],0,this.options.top)){
_e+="n";
}
if(this.between((_d[1]+this.element.offsetHeight)-_c[1],0,this.options.bottom)){
_e+="s";
}
if(this.between(_c[0]-_d[0],0,this.options.left)){
_e+="w";
}
if(this.between((_d[0]+this.element.offsetWidth)-_c[0],0,this.options.right)){
_e+="e";
}
return _e;
}});

