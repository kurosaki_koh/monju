TabBox=Class.create();
TabBox.className={tabBox:"tabBox_tabBox",panelContainer:"tabBox_panelContainer",tabContainer:"tabBox_tabContainer",tab:"tabBox_tab",tabLeftInactive:"tabBox_tabLeftInactive",tabLeftActive:"tabBox_tabLeftActive",tabMiddleInactive:"tabBox_tabMiddleInactive",tabMiddleActive:"tabBox_tabMiddleActive",tabRightInactive:"tabBox_tabRightInactive",tabRightActive:"tabBox_tabRightActive",tabTitle:"tabBox_tabTitle",closeButton:"tabBox_closeButton"};
TabBox.prototype={initialize:function(_1){
var _2=Object.extend({selected:1,cssPrefix:"custom_",beforeSelect:function(){
return true;
},afterSelect:Prototype.emptyFunction,onRemove:function(){
return true;
},sortable:false,closeButton:false,afterSort:Prototype.emptyFunction,onSort:Prototype.emptyFunction,lazyLoadUrl:[],onLazyLoad:Prototype.emptyFunction,afterLazyLoad:Prototype.emptyFunction,lazyLoadFailure:Prototype.emptyFunction,failureLimitOver:Prototype.emptyFunction,failureLimit:5,tabRow:null,titleLength:null},arguments[1]||{});
this.options=_2;
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.selected=(this.options.selected>0)?this.options.selected-1:0;
var _3=CssUtil.appendPrefix(this.options.cssPrefix,TabBox.className);
this.classNames=new CssUtil([TabBox.className,_3]);
this.classNames.addClassNames(this.element,"tabBox");
this.start();
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
if(this.options.lazyLoadUrl.length>0){
this.lazyLoad(0);
}
},start:function(){
this.tabs=[];
this.panelList=[];
this.tabId=this.element.id+"_tab";
this.tabLeftId=this.tabId+"_left";
this.tabMiddleId=this.tabId+"_middle";
this.tabRightId=this.tabId+"_right";
this.tabContainerId=this.element.id+"_tabContainer";
this.panelId=this.element.id+"_panel";
this.panelContainerId=this.element.id+"_panelContainer";
this.tabContainer=null;
this.panelContainer=null;
this.build();
if(this.options.sortable){
this.setDrag();
}
},setDrag:function(){
Sortable.create(this.tabContainerId,{tag:"div",overlap:"horizontal",constraint:"horizontal",onChange:this.options.onSort,onUpdate:this.options.afterSort,starteffect:Prototype.emptyFunction,endeffect:Prototype.emptyFunction});
},build:function(){
this.buildContainers();
Element.cleanWhitespace(this.element);
var _4=this.element.childNodes;
if(_4.length<=this.selected){
this.selected=0;
}
var i=0;
while(_4.length>0){
this.buildTabSet(_4[0],i);
i++;
}
this.addContainers();
this.selectTab();
},buildTabSet:function(_6,i){
if(_6.nodeType!=1){
Element.remove(_6);
return;
}
Element.cleanWhitespace(_6);
var _8=_6.childNodes[1];
this.buildPanel(_8,i);
this.buildTab(_6,i);
},buildContainers:function(){
this.tabContainer=Builder.node("div",{id:this.tabContainerId});
this.classNames.addClassNames(this.tabContainer,"tabContainer");
this.panelContainer=Builder.node("div",{id:this.panelContainerId});
this.classNames.addClassNames(this.panelContainer,"panelContainer");
},addContainers:function(){
this.element.appendChild(this.tabContainer);
this.element.appendChild(this.panelContainer);
},buildTab:function(_9,i){
_9.id=this.tabId+i;
this.classNames.addClassNames(_9,"tab");
var _b=Builder.node("div",[$A(_9.childNodes)]);
this.classNames.addClassNames(_b,"tabTitle");
var _c=Builder.node("div",{id:this.tabLeftId+i});
var _d=Builder.node("div",{id:this.tabMiddleId+i},[_b]);
var _e=Builder.node("div",{id:this.tabRightId+i});
_9.appendChild(_c);
_9.appendChild(_d);
_9.appendChild(_e);
Event.observe(_9,"click",this.selectTab.bindAsEventListener(this));
Event.observe(_9,"mouseover",this.onMouseOver.bindAsEventListener(this));
Event.observe(_9,"mouseout",this.onMouseOut.bindAsEventListener(this));
if(this.options.closeButton){
var _f=Builder.node("div",{id:this.element.id.appendSuffix("closeButton_"+i)});
this.classNames.addClassNames(_f,"closeButton");
_d.appendChild(_f);
Event.observe(_f,"click",this.onRemove.bindAsEventListener(this));
}
if(this.options.tabRow&&!isNaN(this.options.tabRow)&&(i%this.options.tabRow==0)){
Element.setStyle(_9,{clear:"left",styleFloat:"none"});
}
this.setTitle(_d);
this.tabs[i]=_9;
this.setTabInactive(_9);
this.tabContainer.appendChild(_9);
},setTitle:function(_10){
var _11=Element.getTextNodes(_10,true)[0];
title=_11.nodeValue.replace(/^(\s)*/,"");
title=title.replace(/(\s)*$/,"");
var _12=title;
if(this.options.titleLength&&!isNaN(this.options.titleLength)){
_12=title.substring(0,this.options.titleLength);
}
_11.nodeValue=_12;
_10.parentNode.title=title;
},buildPanel:function(_13,i){
var _15=Builder.node("div",{id:this.panelId+i});
_15.appendChild(_13);
Element.hide(_15);
this.panelList[i]=_15;
this.panelContainer.appendChild(_15);
},selectTab:function(e){
if(!this.options.beforeSelect()){
return;
}
if(!e){
this.setTabActive(this.tabs[this.selected]);
Element.show(this.panelList[this.selected]);
return;
}
var _17=this.getCurrentPanel();
var _18=this.getCurrentTab();
var _19=null;
if(e.nodeType){
_19=e;
}else{
_19=Event.element(e);
}
var _1a=this.getTargetIndex(_19);
if(_1a==this.selected){
return;
}
var _1b=this.panelList[_1a];
var _1c=this.tabs[_1a];
if(_18){
this.setTabInactive(_18);
}
this.setTabActive(_1c);
if(_17){
Element.toggle(_17);
}
Element.toggle(_1b);
this.selected=_1a;
this.options.afterSelect(_1b,_17);
},setTabActive:function(tab){
var _1e=tab.childNodes;
this.classNames.refreshClassNames(_1e[0],"tabLeftActive");
this.classNames.refreshClassNames(_1e[1],"tabMiddleActive");
this.classNames.refreshClassNames(_1e[2],"tabRightActive");
},setTabInactive:function(tab){
var _20=tab.childNodes;
this.classNames.refreshClassNames(_20[0],"tabLeftInactive");
this.classNames.refreshClassNames(_20[1],"tabMiddleInactive");
this.classNames.refreshClassNames(_20[2],"tabRightInactive");
},getTargetIndex:function(_21){
while(_21){
if(_21.id&&_21.id.indexOf(this.tabId,0)>=0){
var _22=_21.id.substring(this.tabId.length);
if(!isNaN(_22)){
return _22;
}
}
_21=_21.parentNode;
}
},onRemove:function(_23){
Event.stop(_23);
var _24=Event.element(_23);
var _25=this.getTargetIndex(_24);
var tab=this.tabs[_25];
if(this.options.onRemove(tab)){
this.remove(tab);
}
},remove:function(tab){
if(tab){
var _28=this.getTargetIndex(tab);
var _29=this.getNextTab();
if(!_29){
_29=this.getPreviousTab();
}
Element.remove(tab);
Element.remove(this.panelList[_28]);
this.tabs[_28]=null;
this.panelList[_28]=null;
if(_28==this.selected){
if(_29){
this.selectTab(_29);
}
}
}
},addByElement:function(_2a){
this.buildTabSet($(_2a),this.tabs.length);
if(this.options.sortable){
this.setDrag();
}
},add:function(_2b,_2c){
var _2d=[];
var _2e=Builder.node("div");
_2e.innerHTML=_2b;
_2d.push(_2e);
_2e=Builder.node("div");
_2e.innerHTML=_2c;
_2d.push(_2e);
this.addByElement(Builder.node("div",_2d));
},lazyLoad:function(_2f){
this.errorCount=0;
this.loadedList=[];
this.load(_2f);
},load:function(_30){
var _31=this.panelList[_30];
var url=this.options.lazyLoadUrl[_30];
var _33=this;
if(_31&&url){
new Ajax.Updater({success:_31},url,{onSuccess:function(){
_33.setLoaded(_30);
_33.options.onLazyLoad(_31,_33);
_33.load(++_30);
if(_33.isFinishLazyLoad()){
_33.options.afterLazyLoad(_33);
}
},onFailure:function(){
_33.errorCount++;
_33.options.lazyLoadFailure(_31,_33);
if(_33.errorCount<=_33.options.failureLimit){
_33.load(_30);
}else{
_33.options.failureLimitOver(_33);
}
},asynchronous:true,evalScripts:true});
}
},isFinishLazyLoad:function(){
return this.loadedList.length==this.panelList.length;
},setLoaded:function(i){
this.loadedList.push(i);
},onMouseOver:function(_35){
var _36=Event.element(_35);
var _37=this.getTargetIndex(_36);
if(_37!=this.selected){
var _38=this.tabs[_37];
this.setTabActive(_38);
}
},onMouseOut:function(_39){
var _3a=Event.element(_39);
var _3b=this.getTargetIndex(_3a);
if(_3b!=this.selected){
var _3c=this.tabs[_3b];
this.setTabInactive(_3c);
}
},hasNextTab:function(){
return this.getNextTab()?true:false;
},hasPreviousTab:function(){
return this.getPreviousTab()?true:false;
},getNextTab:function(){
return Element.next(this.getCurrentTab());
},getPreviousTab:function(){
return Element.previous(this.getCurrentTab());
},selectNextTab:function(){
this.selectTab(this.getNextTab());
},selectPreviousTab:function(){
this.selectTab(this.getPreviousTab());
},tabCount:function(){
return this.tabs.inject(0,function(i,t){
return t?++i:i;
});
},getCurrentPanel:function(){
return this.panelList[this.selected];
},getCurrentTab:function(){
return this.tabs[this.selected];
}};

