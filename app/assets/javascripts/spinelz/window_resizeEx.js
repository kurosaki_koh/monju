ResizeableWindowEx=Class.create();
Object.extend(Object.extend(ResizeableWindowEx.prototype,Resizeable.prototype),{startResize:function(_1){
if(Event.isLeftClick(_1)){
var _2=Event.element(_1);
if(_2.tagName&&(_2.tagName=="INPUT"||_2.tagName=="SELECT"||_2.tagName=="BUTTON"||_2.tagName=="TEXTAREA")){
return;
}
var _3=this.directions(_1);
if(_3.length>0){
this.active=true;
var _4=Position.cumulativeOffset(this.element);
this.startTop=_4[1];
this.startLeft=_4[0];
this.startWidth=parseInt(Element.getStyle(this.element,"width"));
this.startHeight=parseInt(Element.getStyle(this.element,"height"));
this.startX=_1.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
this.startY=_1.clientY+document.body.scrollTop+document.documentElement.scrollTop;
this.currentDirection=_3;
Event.stop(_1);
}
if(this.options.restriction){
var _5=this.element.parentNode;
var _6=Element.getDimensions(_5);
this.parentOffset=Position.cumulativeOffset(_5);
this.parentWidth=this.parentOffset[0]+_6.width;
this.parentHeight=this.parentOffset[1]+_6.height;
}
}
},draw:function(_7){
var _8=[Event.pointerX(_7),Event.pointerY(_7)];
if(this.options.restriction&&((this.parentWidth<=_8[0])||(this.parentHeight<=_8[1])||(this.parentOffset[0]>=_8[0])||(this.parentOffset[1]>=_8[1]))){
return;
}
var _9=this.element.style;
var _a=_9.height;
var _b=_9.width;
var _c=_9.top;
var _d=_9.left;
if(this.currentDirection.indexOf("n")!=-1){
var _e=this.startY-_8[1];
var _f=Element.getStyle(this.element,"margin-top")||"0";
_a=this.startHeight+_e;
_c=(this.startTop-_e-parseInt(_f))+"px";
}
if(this.currentDirection.indexOf("w")!=-1){
var _e=this.startX-_8[0];
var _f=Element.getStyle(this.element,"margin-left")||"0";
_b=this.startWidth+_e;
_d=this.startLeft-_e-parseInt(_f);
if(this.options.restriction){
_d-=this.parentOffset[0];
}
_d+="px";
}
if(this.currentDirection.indexOf("s")!=-1){
_a=this.startHeight+_8[1]-this.startY;
}
if(this.currentDirection.indexOf("e")!=-1){
_b=this.startWidth+_8[0]-this.startX;
}
var _10={height:_a,width:_b,top:_c,left:_d};
if(this.options.draw){
this.options.draw(_10,this.element);
}
if(_a&&_a>this.options.minHeight){
_9.top=_10.top;
_9.height=_10.height+"px";
}
if(_b&&_b>this.options.minWidth){
_9.left=_10.left;
_9.width=_10.width+"px";
}
if(_9.visibility=="hidden"){
_9.visibility="";
}
}});

