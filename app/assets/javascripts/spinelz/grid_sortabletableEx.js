SortableTableGridEx=Class.create();
Object.extend(Object.extend(SortableTableGridEx.prototype,SortableTable.prototype),{initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
var _2=Object.extend({ascImg:false,descImg:false,handle:false,callBack:false},arguments[1]||{});
this.options=_2;
this.handle=_2.handle?$(_2.handle):this.element.rows[0];
this.ascImg=_2.ascImg?_2.ascImg:"images/spinelz/sortableTable_down.gif";
this.descImg=_2.descImg?_2.descImg:"images/spinelz/sortableTable_up.gif";
this.sortType=_2.sortType;
this.currentOrder="default";
this.defaultOrder=new Array();
for(var i=0;i<this.element.rows.length;i++){
this.defaultOrder[i]=this.element.rows[i];
}
this.addEvents();
Element.setStyle(this.element,{visibility:"visible"});
},addEvents:function(){
var _4=this.handle.rows[0];
if(!_4){
return;
}
for(var i=0;i<_4.cells.length;i++){
Element.cleanWhitespace(_4.cells[i]);
Element.cleanWhitespace(_4.cells[i].firstChild);
this.addEvent(_4.cells[i].firstChild.firstChild);
}
},addEvent:function(_6){
if(_6){
_6.style.cursor="pointer";
Event.observe(_6,"click",this.sortTable.bindAsEventListener(this));
}
},sortTable:function(_7){
var _8=Event.element(_7);
if(_8.tagName.toUpperCase()=="IMG"){
_8=_8.parentNode;
}
var _9=_8;
if(_9.tagName.toUpperCase()!="TD"&&_9.tagName.toUpperCase()!="TH"){
_9=Element.getParentByTagName(["TD","TH"],_9);
}
var _a=_9.cellIndex;
if(this.targetColumn!=_a){
this.currentOrder="default";
}
this.targetColumn=_a;
var _b=new Array();
for(var i=0;i<this.element.rows.length;i++){
_b[i]=this.element.rows[i];
}
if(_b.length<1){
return;
}
if(this.currentOrder=="default"){
_b.sort(this.getSortFunc());
this.currentOrder="asc";
}else{
if(this.currentOrder=="asc"){
_b=_b.reverse();
this.currentOrder="desc";
}else{
if(this.currentOrder=="desc"){
_b=this.defaultOrder;
this.currentOrder="default";
}
}
}
for(var i=0;i<_b.length;i++){
this.element.tBodies[0].appendChild(_b[i]);
}
this.mark(_8);
if(this.options.callBack){
this.options.callBack(this.element);
}
},mark:function(_d){
var _e=this.options.handle?this.options.handle.rows[0]:this.element.rows[0];
var _f=_e.getElementsByTagName("IMG");
for(var i=0;i<_f.length;i++){
var _11=_f[i].parentNode;
_11.removeChild(_f[i]);
}
var _12;
if(this.currentOrder=="asc"){
_12=this.ascImg;
}else{
if(this.currentOrder=="desc"){
_12=this.descImg;
}
}
if(_12){
_d.appendChild(Builder.node("IMG",{src:_12,alt:"sortImg"}));
}
}});

