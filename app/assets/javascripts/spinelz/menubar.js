var MenuBar=Class.create();
MenuBar.cssNames={container:"menubar",menu:"menubar_menu",menuBody:"menubar_menuBody",menuBodyHover:"menubar_menuBodyHover",subMenu:"menubar_subMenu",subMenuBody:"menubar_subMenuBody",subMenuBodyHover:"menubar_subMenuBodyHover",subMenuContainer:"menubar_menuContainer",dirMark:"menubar_dirMark"};
MenuBar.mark={dir:">>"};
MenuBar.prototype={initialize:function(_1){
this.options=Object.extend({hideOnClickSubmenu:true},arguments[1]);
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
var _2=Object.extend({cssPrefix:"custom_"},arguments[1]||{});
var _3=CssUtil.appendPrefix(_2.cssPrefix,MenuBar.cssNames);
this.classNames=new CssUtil([MenuBar.cssNames,_3]);
this.clicked=[];
var _4=[];
var _5=this.element.childNodes;
for(var i=0;i<_5.length;i++){
if(_5[i].nodeType==1){
this.build(_5[i],"menu");
_4.push(_5[i]);
}
}
this.menubar=Builder.node("DIV",_4);
this.classNames.addClassNames(this.menubar,"container");
this.element.appendChild(this.menubar);
Event.observe(document,"click",this.hideAllTrigger(this.menubar).bindAsEventListener(this));
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
},build:function(_7,_8){
this.classNames.addClassNames(_7,_8);
var _9=new Array();
var _a=new Array();
var _b=_7.childNodes;
for(var i=0;i<_b.length;i++){
if(_b[i].nodeType==1&&_b[i].tagName=="DIV"){
this.build(_b[i],"subMenu");
_a.push(_b[i]);
}else{
_9.push(_b[i]);
}
}
var _d=_8+"Body";
var _e=Builder.node("DIV",_9);
this.classNames.addClassNames(_e,_d);
new Hover(_e);
_7.appendChild(_e);
if(_a.length>0){
if(_8=="subMenu"){
var _f=Builder.node("DIV",[MenuBar.mark.dir]);
this.classNames.addClassNames(_f,"dirMark");
_e.appendChild(_f);
}
var _10=Builder.node("DIV",_a);
this.classNames.addClassNames(_10,"subMenuContainer");
_7.appendChild(_10);
this.hide(_10);
}
Event.observe(_7,"click",this.onClick.bindAsEventListener(this,_e));
},onClick:function(_11,_12){
var _13=_12.parentNode;
var _14=this.getParentContainer(_13);
var _15=this.getContainer(_13);
var _16=MenuBar.cssNames.menu;
if(Element.hasClassName(_13,_16)){
if(this.clicked.length>0){
this.hideAll(this.menubar);
this.clicked=[];
}
if(_15){
this.showAtBottom(_15,_12);
}
}else{
if(_15){
var _17=this.clicked.pop();
var _18=_17.parentNode;
var _19=this.getContainer(_18);
var _1a=this.getParentContainer(_18);
if(_18==_13){
this.hide(_15);
}else{
if(Element.hasClassName(_19,MenuBar.cssNames.container)){
this.clicked.push(last);
}else{
if(_1a==_14){
this.hide(_19);
}else{
this.clicked.push(_17);
}
}
}
this.showAtLeft(_15,_13);
}else{
if(this.options.hideOnClickSubmenu){
this.hideAll(this.menubar);
}
}
}
if(_15){
this.clicked.push(_12);
}
Event.stop(_11);
},showAtBottom:function(_1b,_1c){
var _1d=Position.positionedOffset(_1c);
var _1e=0;
if(_1c.style.height){
_1e=Element.getHeight(_1c);
}else{
_1e=_1c.clientHeight;
}
_1e+=_1d[1];
_1e+=(document.all)?4:3;
_1b.style.top=_1e+"px";
_1b.style.left=_1d[0]+"px";
this.show(_1b);
},showAtLeft:function(_1f,_20){
var _21=Position.positionedOffset(_20);
_1f.style.top=(_21[1]-1)+"px";
_1f.style.left=(_21[0]+_20.offsetWidth+2)+"px";
this.show(_1f);
},hideAllTrigger:function(_22){
return function(_23){
if(!this.isMenuElement(Event.element(_23))){
this.hideAll(_22);
}
};
},hideAll:function(_24){
var _25=_24.childNodes;
for(var i=0;i<_25.length;i++){
if(_25[i].nodeType==1){
if(Element.hasClassName(_25[i],MenuBar.cssNames.subMenuContainer)){
this.hide(_25[i]);
}
this.hideAll(_25[i]);
}
}
},show:function(_27){
_27.style.visibility="visible";
},hide:function(_28){
_28.style.visibility="hidden";
},getContainer:function(_29){
if(!_29){
return;
}
return document.getElementsByClassName(MenuBar.cssNames.subMenuContainer,_29)[0];
},getParentContainer:function(_2a){
var _2b=Element.getParentByClassName(MenuBar.cssNames.subMenuContainer,_2a);
if(!_2b){
_2b=Element.getParentByClassName(MenuBar.cssNames.container,_2a);
}
return _2b;
},isMenuElement:function(_2c){
return Element.hasClassName(_2c,MenuBar.cssNames.menuBodyHover)||Element.hasClassName(_2c,MenuBar.cssNames.subMenuBodyHover)||Element.hasClassName(_2c,MenuBar.cssNames.dirMark);
}};

