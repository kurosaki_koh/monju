var Balloon=Class.create();
Balloon.classNames={tooltip:"balloon_tooltip",top:"balloon_top",topLeft:"balloon_top_left",topMiddle:"balloon_top_middle",topRight:"balloon_top_right",middle:"balloon_middle",middleLeft:"balloon_middle_left",middleRight:"balloon_middle_right",middleLeftRowT:"balloon_middle_left_row",middleLeftRowB:"balloon_middle_left_row",middleRightRowT:"balloon_middle_right_row",middleRightRowB:"balloon_middle_right_row",leftArrow:"balloon_left_arrow",rightArrow:"balloon_right_arrow",leftUpArrow:"balloon_left_up_arrow",leftDownArrow:"balloon_left_down_arrow",rightUpArrow:"balloon_right_up_arrow",rightDownArrow:"balloon_right_down_arrow",body:"balloon_body",bottom:"balloon_bottom",bottomLeft:"balloon_bottom_left",bottomMiddle:"balloon_bottom_middle",bottomRight:"balloon_bottom_right"};
Balloon.allBalloons=[];
Balloon.closeAll=function(){
Balloon.allBalloons.each(function(b){
b.close();
});
};
Balloon.eventSetting=false;
Balloon.prototype={initialize:function(_2,_3){
this.target=$(_2);
this.options=Object.extend({cssPrefix:"custom_",trigger:this.target,tipId:this.target.id+"_balloon",events:["click"],width:300,height:200},arguments[2]||{});
var _4=CssUtil.appendPrefix(this.options.cssPrefix,Balloon.classNames);
this.classNames=new CssUtil([Balloon.classNames,_4]);
this.tipNode=this._buildTipNode(_3);
Element.hide(this.tipNode);
this._setMessage(_3);
document.body.appendChild(this.tipNode);
this._setEvent();
Balloon.allBalloons.push(this);
this._setSize();
},_setEvent:function(){
var _5=this;
this.options.events.each(function(e){
Event.observe(_5.options.trigger,e,_5.open.bindAsEventListener(_5));
});
Event.observe(this.tipNode,"click",this.close.bind(this),true);
if(!Balloon.eventSetting){
Event.observe(document,"click",Balloon.closeAll,true);
Balloon.eventSetting=true;
}
},_buildTipNode:function(){
var _7=Builder.node("div",{id:this.options.tipId});
this.classNames.addClassNames(_7,"tooltip");
_7.appendChild(this._buildTop());
_7.appendChild(this._buildMiddle());
_7.appendChild(this._buildBottom());
return _7;
},_setMessage:function(_8){
var _9=_8.constructor;
if(_9==String){
this.body.innerHTML=_8;
}else{
if(_9==Object){
this.body.appendChild(_8);
}
}
},_buildTop:function(){
return this._buildMulti("top",["topLeft","topMiddle","topRight"],true);
},_buildBottom:function(){
return this._buildMulti("bottom",["bottomLeft","bottomMiddle","bottomRight"],true);
},_buildMiddle:function(){
this.middle=Builder.node("div");
this.classNames.addClassNames(this.middle,"middle");
this.middle.appendChild(this._buildMulti("middleLeft",["middleLeftRowT","leftArrow","middleLeftRowB"],true));
this.middle.appendChild(this._buildMulti("body",[],true));
this.middle.appendChild(this._buildMulti("middleRight",["middleRightRowT","rightArrow","middleRightRowB"],true));
return this.middle;
},_buildMulti:function(_a,_b,_c){
var _d=Builder.node("div");
this.classNames.addClassNames(_d,_a);
if(_c){
this[_a]=_d;
}
var _e=this;
var _f=null;
_b.each(function(s){
_f=Builder.node("div");
_e.classNames.addClassNames(_f,s);
_d.appendChild(_f);
if(_c){
_e[s]=_f;
}
});
return _d;
},_setPosition:function(){
var _11=Position.realOffset(this.tipNode);
var _12=document.documentElement.clientWidth;
var _13=document.documentElement.clientHeight;
var _14=Position.cumulativeOffset(this.target);
var _15=Element.getDimensions(this.target);
var _16=Math.round(_14[0]+_15.width);
var _17=Element.getDimensions(this.tipNode);
var _18=Math.round(_14[1]-_17.height/2);
var _19="left";
var _1a="right";
if((tmpY=_18-_11[1])<0){
_18-=tmpY;
}
if((_16+_17.width)>(_12+_11[0])){
_16=Math.round(_14[0]-_17.width);
_19="right";
_1a="left";
}
var y=_14[1]-_18;
this._setArrow(_19,y);
this._unsetArrow(_1a);
Element.setStyle(this.tipNode,{top:_18+"px",left:_16+"px",zIndex:ZindexManager.getIndex()});
},_setArrow:function(lr,y){
var _1e=(this.options.height-this.middleH)/2;
var _1f,_20,h,ud;
var _23=10;
if(lr=="left"){
h=this.middleH-this.leftArrowH;
}else{
h=this.middleH-this.rightArrowH;
}
if(_1e>y){
_1f=_23;
_20=h-_1f;
ud="up";
}else{
if((this.middleH+_1e)<y){
_20=_23;
_1f=h-_20;
ud="down";
}else{
_1f=y-_1e;
_1f=(_1f<_23)?_23:_1f;
_20=h-_1f;
ud="up";
}
}
if(lr=="left"){
if(ud=="up"){
this.classNames.refreshClassNames(this.leftArrow,"leftUpArrow");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:_1f+"px"});
Element.setStyle(this.middleLeftRowB,{height:_20+"px"});
}else{
this.classNames.refreshClassNames(this.leftArrow,"leftDownArrow");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:_1f+"px"});
Element.setStyle(this.middleLeftRowB,{height:_20+"px"});
}
}else{
if(ud=="up"){
this.classNames.refreshClassNames(this.rightArrow,"rightUpArrow");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:_1f+"px"});
Element.setStyle(this.middleRightRowB,{height:_20+"px"});
}else{
this.classNames.refreshClassNames(this.rightArrow,"rightDownArrow");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:_1f+"px"});
Element.setStyle(this.middleRightRowB,{height:_20+"px"});
}
}
},_unsetArrow:function(_24){
if(_24=="left"){
var h=(this.middleH-this.leftArrowH)/2;
this.classNames.refreshClassNames(this.leftArrow,"middleLeftRowB");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:h+"px"});
Element.setStyle(this.middleLeftRowB,{height:h+"px"});
}else{
var h=(this.middleH-this.rightArrowH)/2;
this.classNames.refreshClassNames(this.rightArrow,"middleRightRowB");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:h+"px"});
Element.setStyle(this.middleRightRowB,{height:h+"px"});
}
},_setSize:function(){
var _26=this.options.width;
var _27=this.options.height;
Element.setStyle(this.tipNode,{width:_26+"px",height:_27+"px"});
var _28=parseInt(Element.getStyle(this.top,"height"));
var _29=parseInt(Element.getStyle(this.bottom,"height"));
var _2a=this.options.height-_28-_29;
var _2b={height:_2a+"px"};
Element.setStyle(this.middle,_2b);
Element.setStyle(this.middleLeft,_2b);
Element.setStyle(this.middleRight,_2b);
Element.setStyle(this.body,_2b);
this.leftArrowH=parseInt(Element.getStyle(this.leftArrow,"height"));
this.rightArrowH=parseInt(Element.getStyle(this.rightArrow,"height"));
this.middleH=_2a;
},open:function(){
if(!Element.visible(this.tipNode)){
this._setPosition();
Effect.Appear(this.tipNode);
}
},close:function(){
if(Element.visible(this.tipNode)){
this._setPosition();
Effect.Fade(this.tipNode);
}
}};

