var AjaxHistory={_callback:null,_currentIframeHash:"",_currentLocationHash:"",_prefix:"ajax_history_",add:function(_1){
AjaxHistoryPageManager.setHash(this._prefix+_1);
},checkIframeHash:function(){
var _2=AjaxHistoryIframeManager.getHash().substr(this._prefix.length);
if(this._currentIframeHash!=_2){
this._currentIframeHash=_2;
this._currentLocationHash=_2;
AjaxHistoryPageManager.setHash((_2)?this._prefix+_2:"");
this.doEvent(_2);
}else{
this.checkLocationHash();
}
},checkHash:function(){
if(UserAgent.isIE()){
this.checkIframeHash();
}else{
this.checkLocationHash();
}
},checkLocationHash:function(){
var _3=AjaxHistoryPageManager.getHash().substr(this._prefix.length);
if(this._currentLocationHash!=_3){
this._currentLocationHash=_3;
if(UserAgent.isIE()){
AjaxHistoryIframeManager.setHash(this._prefix+_3);
}else{
this.doEvent(_3);
}
}
},doEvent:function(_4){
if(this._callback){
this._callback.call(null,_4);
}
},init:function(_5){
this._callback=_5;
if(UserAgent.isIE()){
AjaxHistoryIframeManager.create();
}
var _6=this;
var _7=function(){
_6.checkHash();
};
setInterval(_7,100);
}};
var AjaxHistoryIframeManager={_id:"ajax_history_frame",_element:null,_src:IECover.src,create:function(){
document.write("<iframe id=\""+this._id+"\" src=\""+this._src+"\" style=\"display: none;\"></iframe>");
this._element=$(this._id);
},getHash:function(){
var _8=this._element.contentWindow.document;
return _8.location.hash.replace(/^#/,"");
},setHash:function(_9){
var _a=this._element.contentWindow.document;
_a.open();
_a.close();
_a.location.hash=_9;
}};
var AjaxHistoryPageManager={_delimiter:"#",_location:"window.location.href",_query:"",getLocation:function(){
return eval(this._location);
},getHash:function(){
var _b=this.getLocation().split(this._delimiter);
return (_b.length>1)?_b[_b.length-1]:this._query;
},getUrl:function(){
var _c=this.getLocation().split(this._delimiter);
return _c[0];
},setHash:function(_d){
window.location.hash=_d;
}};

var Balloon=Class.create();
Balloon.classNames={tooltip:"balloon_tooltip",top:"balloon_top",topLeft:"balloon_top_left",topMiddle:"balloon_top_middle",topRight:"balloon_top_right",middle:"balloon_middle",middleLeft:"balloon_middle_left",middleRight:"balloon_middle_right",middleLeftRowT:"balloon_middle_left_row",middleLeftRowB:"balloon_middle_left_row",middleRightRowT:"balloon_middle_right_row",middleRightRowB:"balloon_middle_right_row",leftArrow:"balloon_left_arrow",rightArrow:"balloon_right_arrow",leftUpArrow:"balloon_left_up_arrow",leftDownArrow:"balloon_left_down_arrow",rightUpArrow:"balloon_right_up_arrow",rightDownArrow:"balloon_right_down_arrow",body:"balloon_body",bottom:"balloon_bottom",bottomLeft:"balloon_bottom_left",bottomMiddle:"balloon_bottom_middle",bottomRight:"balloon_bottom_right"};
Balloon.allBalloons=[];
Balloon.closeAll=function(){
Balloon.allBalloons.each(function(b){
b.close();
});
};
Balloon.eventSetting=false;
Balloon.prototype={initialize:function(_2,_3){
this.target=$(_2);
this.options=Object.extend({cssPrefix:"custom_",trigger:this.target,tipId:this.target.id+"_balloon",events:["click"],width:300,height:200},arguments[2]||{});
var _4=CssUtil.appendPrefix(this.options.cssPrefix,Balloon.classNames);
this.classNames=new CssUtil([Balloon.classNames,_4]);
this.tipNode=this._buildTipNode(_3);
Element.hide(this.tipNode);
this._setMessage(_3);
document.body.appendChild(this.tipNode);
this._setEvent();
Balloon.allBalloons.push(this);
this._setSize();
},_setEvent:function(){
var _5=this;
this.options.events.each(function(e){
Event.observe(_5.options.trigger,e,_5.open.bindAsEventListener(_5));
});
Event.observe(this.tipNode,"click",this.close.bind(this),true);
if(!Balloon.eventSetting){
Event.observe(document,"click",Balloon.closeAll,true);
Balloon.eventSetting=true;
}
},_buildTipNode:function(){
var _7=Builder.node("div",{id:this.options.tipId});
this.classNames.addClassNames(_7,"tooltip");
_7.appendChild(this._buildTop());
_7.appendChild(this._buildMiddle());
_7.appendChild(this._buildBottom());
return _7;
},_setMessage:function(_8){
var _9=_8.constructor;
if(_9==String){
this.body.innerHTML=_8;
}else{
if(_9==Object){
this.body.appendChild(_8);
}
}
},_buildTop:function(){
return this._buildMulti("top",["topLeft","topMiddle","topRight"],true);
},_buildBottom:function(){
return this._buildMulti("bottom",["bottomLeft","bottomMiddle","bottomRight"],true);
},_buildMiddle:function(){
this.middle=Builder.node("div");
this.classNames.addClassNames(this.middle,"middle");
this.middle.appendChild(this._buildMulti("middleLeft",["middleLeftRowT","leftArrow","middleLeftRowB"],true));
this.middle.appendChild(this._buildMulti("body",[],true));
this.middle.appendChild(this._buildMulti("middleRight",["middleRightRowT","rightArrow","middleRightRowB"],true));
return this.middle;
},_buildMulti:function(_a,_b,_c){
var _d=Builder.node("div");
this.classNames.addClassNames(_d,_a);
if(_c){
this[_a]=_d;
}
var _e=this;
var _f=null;
_b.each(function(s){
_f=Builder.node("div");
_e.classNames.addClassNames(_f,s);
_d.appendChild(_f);
if(_c){
_e[s]=_f;
}
});
return _d;
},_setPosition:function(){
var _11=Position.realOffset(this.tipNode);
var _12=document.documentElement.clientWidth;
var _13=document.documentElement.clientHeight;
var _14=Position.cumulativeOffset(this.target);
var _15=Element.getDimensions(this.target);
var _16=Math.round(_14[0]+_15.width);
var _17=Element.getDimensions(this.tipNode);
var _18=Math.round(_14[1]-_17.height/2);
var _19="left";
var _1a="right";
if((tmpY=_18-_11[1])<0){
_18-=tmpY;
}
if((_16+_17.width)>(_12+_11[0])){
_16=Math.round(_14[0]-_17.width);
_19="right";
_1a="left";
}
var y=_14[1]-_18;
this._setArrow(_19,y);
this._unsetArrow(_1a);
Element.setStyle(this.tipNode,{top:_18+"px",left:_16+"px",zIndex:ZindexManager.getIndex()});
},_setArrow:function(lr,y){
var _1e=(this.options.height-this.middleH)/2;
var _1f,_20,h,ud;
var _23=10;
if(lr=="left"){
h=this.middleH-this.leftArrowH;
}else{
h=this.middleH-this.rightArrowH;
}
if(_1e>y){
_1f=_23;
_20=h-_1f;
ud="up";
}else{
if((this.middleH+_1e)<y){
_20=_23;
_1f=h-_20;
ud="down";
}else{
_1f=y-_1e;
_1f=(_1f<_23)?_23:_1f;
_20=h-_1f;
ud="up";
}
}
if(lr=="left"){
if(ud=="up"){
this.classNames.refreshClassNames(this.leftArrow,"leftUpArrow");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:_1f+"px"});
Element.setStyle(this.middleLeftRowB,{height:_20+"px"});
}else{
this.classNames.refreshClassNames(this.leftArrow,"leftDownArrow");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:_1f+"px"});
Element.setStyle(this.middleLeftRowB,{height:_20+"px"});
}
}else{
if(ud=="up"){
this.classNames.refreshClassNames(this.rightArrow,"rightUpArrow");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:_1f+"px"});
Element.setStyle(this.middleRightRowB,{height:_20+"px"});
}else{
this.classNames.refreshClassNames(this.rightArrow,"rightDownArrow");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:_1f+"px"});
Element.setStyle(this.middleRightRowB,{height:_20+"px"});
}
}
},_unsetArrow:function(_24){
if(_24=="left"){
var h=(this.middleH-this.leftArrowH)/2;
this.classNames.refreshClassNames(this.leftArrow,"middleLeftRowB");
Element.setStyle(this.leftArrow,{height:this.leftArrowH+"px"});
Element.setStyle(this.middleLeftRowT,{height:h+"px"});
Element.setStyle(this.middleLeftRowB,{height:h+"px"});
}else{
var h=(this.middleH-this.rightArrowH)/2;
this.classNames.refreshClassNames(this.rightArrow,"middleRightRowB");
Element.setStyle(this.rightArrow,{height:this.rightArrowH+"px"});
Element.setStyle(this.middleRightRowT,{height:h+"px"});
Element.setStyle(this.middleRightRowB,{height:h+"px"});
}
},_setSize:function(){
var _26=this.options.width;
var _27=this.options.height;
Element.setStyle(this.tipNode,{width:_26+"px",height:_27+"px"});
var _28=parseInt(Element.getStyle(this.top,"height"));
var _29=parseInt(Element.getStyle(this.bottom,"height"));
var _2a=this.options.height-_28-_29;
var _2b={height:_2a+"px"};
Element.setStyle(this.middle,_2b);
Element.setStyle(this.middleLeft,_2b);
Element.setStyle(this.middleRight,_2b);
Element.setStyle(this.body,_2b);
this.leftArrowH=parseInt(Element.getStyle(this.leftArrow,"height"));
this.rightArrowH=parseInt(Element.getStyle(this.rightArrow,"height"));
this.middleH=_2a;
},open:function(){
if(!Element.visible(this.tipNode)){
this._setPosition();
Effect.Appear(this.tipNode);
}
},close:function(){
if(Element.visible(this.tipNode)){
this._setPosition();
Effect.Fade(this.tipNode);
}
}};

var SelectableTable=Class.create();
SelectableTable.classNames={table:"selectableTable_table",tr:"selectableTable_tr",trHover:"selectableTable_trHover",trSelected:"selectableTable_trSelected"};
SelectableTable.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
var _2={arrayDefaultData:[],flagAllowUnselect:true,flagInitialAllowMultiple:false,flagKeypressAvailable:false,flagKeypressDeleteAvailable:false,flagKeypressInsertAvailable:false,functionPostAdd:Prototype.emptyFunction,functionPostDelete:Prototype.emptyFunction,functionPostPressLeft:Prototype.emptyFunction,functionPostPressRight:Prototype.emptyFunction,functionPostSelect:Prototype.emptyFunction,functionPostUnselect:Prototype.emptyFunction,functionPreAdd:function(){
return true;
},functionPreDelete:function(){
return true;
},functionSubmit:Prototype.emptyFunction,initialSelected:null,prefixTrId:"selectable_table_",prefixCSS:"custom_"};
this.options=Object.extend(_2,arguments[1]||{});
this.classNames=new CssUtil([SelectableTable.classNames,CssUtil.appendPrefix(this.options.prefixCSS,SelectableTable.classNames)]);
this.documentListener=this.eventKeypress.bindAsEventListener(this);
this.flagAllowMultiple=this.options.flagInitialAllowMultiple;
this.flagAvailable=true;
this.focused=null;
this.lastSelected=null;
this.newNumber=1;
this.selected=new Object();
this.build();
if(arguments[2]){
this.selectEffect(this.buildTrId(arguments[2]));
}
Element.setStyle(this.element,{visibility:"visible"});
},add:function(){
if(!this.flagAvailable){
return;
}
if(!this.options.functionPreAdd(this)){
return;
}
if(arguments[0]==null){
arguments[0]=this.options.arrayDefaultData;
}
if(typeof (arguments[0])!="string"){
arguments=arguments[0];
}
if(arguments[0]==null){
return;
}
var _3,_4;
_3=document.createElement("tr");
_3.id="new_"+this.newNumber;
this.buildTr(_3);
for(var i=0;i<arguments.length;i++){
_4=document.createElement("td");
_4.innerHTML=arguments[i];
_3.appendChild(_4);
}
this.element.tBodies[0].appendChild(_3);
this.newNumber++;
this.options.functionPostAdd(this);
},build:function(){
var _6=this.element.tBodies[0].rows;
this.classNames.addClassNames(this.element,"table");
Event.observe(document,"keypress",this.documentListener);
for(var i=0;i<_6.length;i++){
this.buildTr(_6[i]);
}
var _8=this.options.initialSelected;
if(_8){
this.selectEffect(this.buildTrId(_8));
}
},buildTr:function(_9){
_9.id=this.buildTrId(_9.id);
this.classNames.addClassNames(_9,"tr");
Event.observe(_9,"click",this.eventClick.bindAsEventListener(this));
Event.observe(_9,"dblclick",this.eventDoubleClick.bindAsEventListener(this));
Event.observe(_9,"mouseout",this.eventFocusOut.bindAsEventListener(this));
Event.observe(_9,"mouseover",this.eventFocusOver.bindAsEventListener(this));
},buildTrId:function(_a){
return this.options.prefixTrId+_a;
},deleteAll:function(){
if(!this.flagAvailable){
return;
}
if(!this.options.functionPreDelete(this)){
return;
}
for(var _b in this.selected){
this.element.tBodies[0].removeChild($(_b));
delete this.selected[_b];
}
this.focused=null;
this.options.functionPostDelete(this);
},eventClick:function(_c){
if(!this.flagAvailable){
return;
}
if(_c.shiftKey){
this.selectOrUnselectRange(Event.findElement(_c,"tr").id);
}else{
this.selectOrUnselect(Event.findElement(_c,"tr").id,_c.ctrlKey);
}
},eventDoubleClick:function(_d){
if(!this.flagAvailable){
return;
}
if(this.flagAllowMultiple){
this.select(Event.findElement(_d,"tr").id,false);
this.submit();
}
},eventFocusOut:function(_e){
if(!this.flagAvailable){
return;
}
this.focusOff();
},eventFocusOver:function(_f){
if(!this.flagAvailable){
return;
}
this.focusOn(Event.findElement(_f,"tr").id);
Event.findElement(_f,"tr").focus();
},eventKeypress:function(_10){
if(!this.flagAvailable){
return;
}
if(!this.options.flagKeypressAvailable){
return;
}
switch(_10.keyCode){
case 13:
if(_10.shiftKey){
this.selectOrUnselectRange(this.focused);
}else{
this.selectOrUnselect(this.focused,_10.ctrlKey);
}
break;
case 37:
this.options.functionPostPressLeft(this);
break;
case 38:
this.focusMove("up");
break;
case 39:
this.options.functionPostPressRight(this);
break;
case 40:
this.focusMove("down");
break;
case 45:
if(this.options.flagKeypressInsertAvailable){
this.add();
}
break;
case 46:
if(this.options.flagKeypressDeleteAvailable){
this.deleteAll();
}
break;
}
},focusMove:function(_11){
if(!this.flagAvailable){
return;
}
if(this.focused==null){
this.focusOn(this.element.tBodies[0].rows[0].id);
}else{
var _12=$(this.focused).rowIndex;
var _13,_14;
switch(_11){
case "down":
_13=1;
_14=this.isBottom(_12);
break;
case "up":
_13=-1;
_14=this.isTop(_12);
break;
}
if(!_14){
this.focusOn(this.element.rows[_12+_13].id);
}
}
},focusOff:function(){
if(!this.flagAvailable){
return;
}
if(this.focused!=null){
var _15=$(this.focused);
this.classNames.removeClassNames(_15,"trHover");
this.focused=null;
}
},focusOn:function(_16){
if(!this.flagAvailable){
return;
}
if($(_16)!=null){
this.focusOff();
this.classNames.addClassNames($(_16),"trHover");
this.focused=_16;
}
},getSelected:function(){
var _17=new Array();
for(var _18 in this.selected){
_17.push(_18.replace(this.options.prefixTrId,""));
}
return _17;
},getSelectedElement:function(id){
var _1a=this.options.prefixTrId+id;
return $(_1a);
},isBottom:function(_1b){
return (_1b==this.element.rows.length-1)?true:false;
},isTop:function(_1c){
return (_1c==this.element.tBodies[0].rows[0].rowIndex)?true:false;
},makeAvailable:function(){
this.flagAvailable=true;
},makeMultiple:function(){
this.flagAllowMultiple=true;
},makeSingular:function(){
this.flagAllowMultiple=false;
this.unselectAll();
},makeUnavailable:function(){
this.flagAvailable=false;
},removeEventFromDocument:function(){
Event.stopObserving(document,"keypress",this.documentListener);
},select:function(_1d,_1e){
if(!this.flagAvailable){
return;
}
this.selectEffect(_1d,_1e);
this.lastSelected=_1d;
this.options.functionPostSelect(this);
if(!this.flagAllowMultiple){
this.submit();
}
},selectAll:function(){
if(!this.flagAvailable){
return;
}
if(!this.flagAllowMultiple){
return;
}
this.selected=new Object();
var _1f=this.element.tBodies[0].rows;
for(var i=0;i<_1f.length;i++){
this.select(_1f[i].id,true);
}
},selectEffect:function(_21,_22){
if($(_21)){
if(!this.flagAllowMultiple||!_22){
this.unselectAll();
}
this.classNames.addClassNames($(_21),"trSelected");
this.selected[_21]=true;
}
},selectOrUnselect:function(_23,_24){
if(!this.flagAvailable){
return;
}
if(_23==null){
return;
}
if(_24&&this.selected[_23]){
if(!this.flagAllowMultiple&&!this.options.flagAllowUnselect){
return;
}
this.unselect(_23);
}else{
this.select(_23,_24);
}
},selectOrUnselectRange:function(_25){
if(!this.flagAvailable){
return;
}
if(_25==null){
return;
}
if(this.lastSelected==null||this.lastSelected==_25){
this.selectOrUnselect(_25);
return;
}
var _26=false;
var _27=this.element.tBodies[0].rows;
var _28=this.lastSelected;
for(var i=0;i<_27.length;i++){
if(_27[i].id==_25||_27[i].id==_28){
_26=(_26)?false:true;
}else{
if(!_26){
continue;
}
}
if(this.selected[_28]){
this.select(_27[i].id,true);
}else{
this.unselect(_27[i].id);
}
}
},submit:function(_2a){
if(!this.flagAvailable){
return;
}
var _2b=this.getSelected();
this.options.functionSubmit(_2b[0]);
},unselect:function(_2c){
if(!this.flagAvailable){
return;
}
this.classNames.removeClassNames($(_2c),"trSelected");
delete this.selected[_2c];
this.lastSelected=_2c;
this.options.functionPostUnselect(this);
},unselectAll:function(){
if(!this.flagAvailable){
return;
}
var _2d=this.element.tBodies[0].rows;
for(var i=0;i<_2d.length;i++){
this.unselect(_2d[i].id);
}
}};
var SelectableTableManager=Class.create();
SelectableTableManager.prototype={initialize:function(){
this.active=null,this.list={};
},activate:function(key){
this.stop();
if(this.list[key]){
this.list[key].makeAvailable();
this.active=this.list[key];
}else{
this.active=null;
}
},push:function(key,_31){
this.list[key]=_31;
},start:function(){
if(this.active){
this.active.makeAvailable();
}
},stop:function(){
$H(this.list).each(function(el){
if(el[1]){
el[1].makeUnavailable();
}
});
}};

var Switcher=Class.create();
Switcher.classNames={open:"switcher_state_open",close:"switcher_state_close"};
Switcher.prototype={initialize:function(sw,_2){
this.options=Object.extend({open:false,duration:0.4,beforeOpen:Prototype.emptyFunction,afterOpen:Prototype.emptyFunction,beforeClose:Prototype.emptyFunction,afterClose:Prototype.emptyFunction,effect:false,cssPrefix:"custom_"},arguments[2]||{});
this.sw=$(sw);
this.content=$(_2);
var _3=CssUtil.appendPrefix(this.options.cssPrefix,Switcher.classNames);
this.classNames=new CssUtil([Switcher.classNames,_3]);
if(this.options.open){
Element.show(this.content);
this.classNames.addClassNames(this.sw,"open");
}else{
Element.hide(this.content);
this.classNames.addClassNames(this.sw,"close");
}
Event.observe(this.sw,"click",this.toggle.bind(this));
},toggle:function(){
if(Element.hasClassName(this.sw,Switcher.classNames.close)){
this.open();
}else{
this.close();
}
},open:function(){
this.options.beforeOpen(this.content);
this.classNames.refreshClassNames(this.sw,"open");
if(this.options.effect){
new Effect.BlindDown(this.content,{duration:this.options.duration});
}else{
Element.show(this.content);
}
this.options.afterOpen(this.content);
},close:function(){
this.options.beforeClose(this.content);
this.classNames.refreshClassNames(this.sw,"close");
if(this.options.effect){
new Effect.BlindUp(this.content,{duration:this.options.duration});
}else{
Element.hide(this.content);
}
this.options.afterClose(this.content);
}};

TabBox=Class.create();
TabBox.className={tabBox:"tabBox_tabBox",panelContainer:"tabBox_panelContainer",tabContainer:"tabBox_tabContainer",tab:"tabBox_tab",tabLeftInactive:"tabBox_tabLeftInactive",tabLeftActive:"tabBox_tabLeftActive",tabMiddleInactive:"tabBox_tabMiddleInactive",tabMiddleActive:"tabBox_tabMiddleActive",tabRightInactive:"tabBox_tabRightInactive",tabRightActive:"tabBox_tabRightActive",tabTitle:"tabBox_tabTitle",closeButton:"tabBox_closeButton"};
TabBox.prototype={initialize:function(_1){
var _2=Object.extend({selected:1,cssPrefix:"custom_",beforeSelect:function(){
return true;
},afterSelect:Prototype.emptyFunction,onRemove:function(){
return true;
},sortable:false,closeButton:false,afterSort:Prototype.emptyFunction,onSort:Prototype.emptyFunction,lazyLoadUrl:[],onLazyLoad:Prototype.emptyFunction,afterLazyLoad:Prototype.emptyFunction,lazyLoadFailure:Prototype.emptyFunction,failureLimitOver:Prototype.emptyFunction,failureLimit:5,tabRow:null,titleLength:null},arguments[1]||{});
this.options=_2;
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.selected=(this.options.selected>0)?this.options.selected-1:0;
var _3=CssUtil.appendPrefix(this.options.cssPrefix,TabBox.className);
this.classNames=new CssUtil([TabBox.className,_3]);
this.classNames.addClassNames(this.element,"tabBox");
this.start();
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
if(this.options.lazyLoadUrl.length>0){
this.lazyLoad(0);
}
},start:function(){
this.tabs=[];
this.panelList=[];
this.tabId=this.element.id+"_tab";
this.tabLeftId=this.tabId+"_left";
this.tabMiddleId=this.tabId+"_middle";
this.tabRightId=this.tabId+"_right";
this.tabContainerId=this.element.id+"_tabContainer";
this.panelId=this.element.id+"_panel";
this.panelContainerId=this.element.id+"_panelContainer";
this.tabContainer=null;
this.panelContainer=null;
this.build();
if(this.options.sortable){
this.setDrag();
}
},setDrag:function(){
Sortable.create(this.tabContainerId,{tag:"div",overlap:"horizontal",constraint:"horizontal",onChange:this.options.onSort,onUpdate:this.options.afterSort,starteffect:Prototype.emptyFunction,endeffect:Prototype.emptyFunction});
},build:function(){
this.buildContainers();
Element.cleanWhitespace(this.element);
var _4=this.element.childNodes;
if(_4.length<=this.selected){
this.selected=0;
}
var i=0;
while(_4.length>0){
this.buildTabSet(_4[0],i);
i++;
}
this.addContainers();
this.selectTab();
},buildTabSet:function(_6,i){
if(_6.nodeType!=1){
Element.remove(_6);
return;
}
Element.cleanWhitespace(_6);
var _8=_6.childNodes[1];
this.buildPanel(_8,i);
this.buildTab(_6,i);
},buildContainers:function(){
this.tabContainer=Builder.node("div",{id:this.tabContainerId});
this.classNames.addClassNames(this.tabContainer,"tabContainer");
this.panelContainer=Builder.node("div",{id:this.panelContainerId});
this.classNames.addClassNames(this.panelContainer,"panelContainer");
},addContainers:function(){
this.element.appendChild(this.tabContainer);
this.element.appendChild(this.panelContainer);
},buildTab:function(_9,i){
_9.id=this.tabId+i;
this.classNames.addClassNames(_9,"tab");
var _b=Builder.node("div",[$A(_9.childNodes)]);
this.classNames.addClassNames(_b,"tabTitle");
var _c=Builder.node("div",{id:this.tabLeftId+i});
var _d=Builder.node("div",{id:this.tabMiddleId+i},[_b]);
var _e=Builder.node("div",{id:this.tabRightId+i});
_9.appendChild(_c);
_9.appendChild(_d);
_9.appendChild(_e);
Event.observe(_9,"click",this.selectTab.bindAsEventListener(this));
Event.observe(_9,"mouseover",this.onMouseOver.bindAsEventListener(this));
Event.observe(_9,"mouseout",this.onMouseOut.bindAsEventListener(this));
if(this.options.closeButton){
var _f=Builder.node("div",{id:this.element.id.appendSuffix("closeButton_"+i)});
this.classNames.addClassNames(_f,"closeButton");
_d.appendChild(_f);
Event.observe(_f,"click",this.onRemove.bindAsEventListener(this));
}
if(this.options.tabRow&&!isNaN(this.options.tabRow)&&(i%this.options.tabRow==0)){
Element.setStyle(_9,{clear:"left",styleFloat:"none"});
}
this.setTitle(_d);
this.tabs[i]=_9;
this.setTabInactive(_9);
this.tabContainer.appendChild(_9);
},setTitle:function(_10){
var _11=Element.getTextNodes(_10,true)[0];
title=_11.nodeValue.replace(/^(\s)*/,"");
title=title.replace(/(\s)*$/,"");
var _12=title;
if(this.options.titleLength&&!isNaN(this.options.titleLength)){
_12=title.substring(0,this.options.titleLength);
}
_11.nodeValue=_12;
_10.parentNode.title=title;
},buildPanel:function(_13,i){
var _15=Builder.node("div",{id:this.panelId+i});
_15.appendChild(_13);
Element.hide(_15);
this.panelList[i]=_15;
this.panelContainer.appendChild(_15);
},selectTab:function(e){
if(!this.options.beforeSelect()){
return;
}
if(!e){
this.setTabActive(this.tabs[this.selected]);
Element.show(this.panelList[this.selected]);
return;
}
var _17=this.getCurrentPanel();
var _18=this.getCurrentTab();
var _19=null;
if(e.nodeType){
_19=e;
}else{
_19=Event.element(e);
}
var _1a=this.getTargetIndex(_19);
if(_1a==this.selected){
return;
}
var _1b=this.panelList[_1a];
var _1c=this.tabs[_1a];
if(_18){
this.setTabInactive(_18);
}
this.setTabActive(_1c);
if(_17){
Element.toggle(_17);
}
Element.toggle(_1b);
this.selected=_1a;
this.options.afterSelect(_1b,_17);
},setTabActive:function(tab){
var _1e=tab.childNodes;
this.classNames.refreshClassNames(_1e[0],"tabLeftActive");
this.classNames.refreshClassNames(_1e[1],"tabMiddleActive");
this.classNames.refreshClassNames(_1e[2],"tabRightActive");
},setTabInactive:function(tab){
var _20=tab.childNodes;
this.classNames.refreshClassNames(_20[0],"tabLeftInactive");
this.classNames.refreshClassNames(_20[1],"tabMiddleInactive");
this.classNames.refreshClassNames(_20[2],"tabRightInactive");
},getTargetIndex:function(_21){
while(_21){
if(_21.id&&_21.id.indexOf(this.tabId,0)>=0){
var _22=_21.id.substring(this.tabId.length);
if(!isNaN(_22)){
return _22;
}
}
_21=_21.parentNode;
}
},onRemove:function(_23){
Event.stop(_23);
var _24=Event.element(_23);
var _25=this.getTargetIndex(_24);
var tab=this.tabs[_25];
if(this.options.onRemove(tab)){
this.remove(tab);
}
},remove:function(tab){
if(tab){
var _28=this.getTargetIndex(tab);
var _29=this.getNextTab();
if(!_29){
_29=this.getPreviousTab();
}
Element.remove(tab);
Element.remove(this.panelList[_28]);
this.tabs[_28]=null;
this.panelList[_28]=null;
if(_28==this.selected){
if(_29){
this.selectTab(_29);
}
}
}
},addByElement:function(_2a){
this.buildTabSet($(_2a),this.tabs.length);
if(this.options.sortable){
this.setDrag();
}
},add:function(_2b,_2c){
var _2d=[];
var _2e=Builder.node("div");
_2e.innerHTML=_2b;
_2d.push(_2e);
_2e=Builder.node("div");
_2e.innerHTML=_2c;
_2d.push(_2e);
this.addByElement(Builder.node("div",_2d));
},lazyLoad:function(_2f){
this.errorCount=0;
this.loadedList=[];
this.load(_2f);
},load:function(_30){
var _31=this.panelList[_30];
var url=this.options.lazyLoadUrl[_30];
var _33=this;
if(_31&&url){
new Ajax.Updater({success:_31},url,{onSuccess:function(){
_33.setLoaded(_30);
_33.options.onLazyLoad(_31,_33);
_33.load(++_30);
if(_33.isFinishLazyLoad()){
_33.options.afterLazyLoad(_33);
}
},onFailure:function(){
_33.errorCount++;
_33.options.lazyLoadFailure(_31,_33);
if(_33.errorCount<=_33.options.failureLimit){
_33.load(_30);
}else{
_33.options.failureLimitOver(_33);
}
},asynchronous:true,evalScripts:true});
}
},isFinishLazyLoad:function(){
return this.loadedList.length==this.panelList.length;
},setLoaded:function(i){
this.loadedList.push(i);
},onMouseOver:function(_35){
var _36=Event.element(_35);
var _37=this.getTargetIndex(_36);
if(_37!=this.selected){
var _38=this.tabs[_37];
this.setTabActive(_38);
}
},onMouseOut:function(_39){
var _3a=Event.element(_39);
var _3b=this.getTargetIndex(_3a);
if(_3b!=this.selected){
var _3c=this.tabs[_3b];
this.setTabInactive(_3c);
}
},hasNextTab:function(){
return this.getNextTab()?true:false;
},hasPreviousTab:function(){
return this.getPreviousTab()?true:false;
},getNextTab:function(){
return Element.next(this.getCurrentTab());
},getPreviousTab:function(){
return Element.previous(this.getCurrentTab());
},selectNextTab:function(){
this.selectTab(this.getNextTab());
},selectPreviousTab:function(){
this.selectTab(this.getPreviousTab());
},tabCount:function(){
return this.tabs.inject(0,function(i,t){
return t?++i:i;
});
},getCurrentPanel:function(){
return this.panelList[this.selected];
},getCurrentTab:function(){
return this.tabs[this.selected];
}};

var TreeView=Class.create();
TreeView.className={top:"treeview",dir:"treeview_dir",dirBody:"treeview_dirBody",dirBodyText:"treeview_dirBodyText",dirBodyTextActive:"treeview_dirBodyTextActive",dirContainer:"treeview_dirContainer",dirContainerHover:"treeview_dirContainerHover",file:"treeview_file",fileBody:"treeview_fileBody",fileBodyText:"treeview_fileBodyText",fileBodyTextActive:"treeview_fileBodyTextActive",state_open:"treeview_stateOpen",state_close:"treeview_stateClose",state_empty:"treeview_stateEmpty",dirIcon:"treeview_dirIcon",fileIcon:"treeview_fileIcon",handle:"treeview_handle"};
TreeView.iconId="treeview_icon";
TreeView.prototype={initialize:function(_1){
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.options=Object.extend({dirSymbol:"dir",fileSymbol:"file",cssPrefix:"custom_",open:true,callBackFunctions:false,dirSelect:true,fileSelect:true,noSelectedInsert:true,iconIdPrefix:TreeView.iconId,move:false,unselected:Prototype.emptyFunction,enableUnselected:true,sortOptions:{},openDir:Prototype.emptyFunction,closeDir:Prototype.emptyFunction,emptyImg:false,initialSelected:null},arguments[1]||{});
this.customCss=CssUtil.appendPrefix(this.options.cssPrefix,TreeView.className);
this.classNames=new CssUtil([TreeView.className,this.customCss]);
this.changeClassNameDirAndFile(this.element);
var _2=this.element.childNodes;
for(var i=0;i<_2.length;i++){
this.build(_2[i]);
}
this.classNames.addClassNames(this.element,"top");
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
if(this.options.initialSelected){
this.selectEffect(this.options.initialSelected);
}
if(this.options.move){
this.setSortable();
}
},addChildById:function(_4,_5,_6){
_4=$(_4);
_5=$(_5);
var _7=null;
if(!_4||!_5){
return;
}else{
if(Element.hasClassName(_5,TreeView.className.dir)){
_7=this.getChildDirContainer(_5);
}else{
if(Element.hasClassName(_5,TreeView.className.top)){
_7=_5;
}else{
return;
}
}
}
this.build(_4);
if(isNaN(_6)){
_7.appendChild(_4);
}else{
var _8=this.getDirectoryContents(_7);
if(_8[_6]){
_7.insertBefore(_4,_8[_6]);
}else{
_7.appendChild(_4);
}
}
this.refreshStateImg(_5);
if(this.options.dragAdrop){
this.setSortable();
}
},addChildByPath:function(_9,_a){
_9=$(_9);
if(_9){
this.build(_9);
}else{
return;
}
var _b=_a.split("/").findAll(function(_c){
return (_c!="");
});
var _d=_b.pop();
var _e=this.search(_b.join("/"));
var _f=this.getDirectoryContents(_e);
if(_f[_d]){
_e.insertBefore(_9,_f[_d]);
}else{
_e.appendChild(_9);
}
this.refreshStateImg(_e.parentNode);
if(this.options.dragAdrop){
this.setSortable();
}
},addChildBySelected:function(_10,_11){
if(!this.selected&&!this.options.noSelectedInsert){
return;
}
if(this.selected){
this.addChildById(_10,this.selected,_11);
}else{
this.addChildById(_10,this.element,_11);
}
},addSelectItemCallback:function(_12){
if(!this.options.callBackFunctions){
this.options.callBackFunctions=new Array();
}
this.options.callBackFunctions.push(_12);
},build:function(_13){
if(_13.nodeType!=1){
return;
}
Element.cleanWhitespace(_13);
this.changeClassNameDirAndFile(_13);
if(Element.hasClassName(_13,TreeView.className.dir)){
var _14=this.createDirectoryContainer(_13);
var _15;
if(this.hasContents(_14)){
_15=this.createDirectoryBody(_13,false);
}else{
_15=this.createDirectoryBody(_13,true);
}
_13.appendChild(_15);
_13.appendChild(_14);
var _16=_14.childNodes;
for(var i=0;i<_16.length;i++){
this.build(_16[i]);
}
}else{
if(Element.hasClassName(_13,TreeView.className.file)){
var _18=this.createFileBody(_13);
_13.appendChild(_18);
}
}
},changeClassName:function(_19,to,_1b){
var _1c=document.getElementsByClassName(_1b,_19);
var _1d=this.classNames.joinClassNames(to);
_1c.each(function(n){
n.className=n.className.replace(new RegExp(_1b),_1d);
});
if(Element.hasClassName(_19,_1b)){
_19.className=_19.className.replace(new RegExp(_1b),_1d);
}
},changeClassNameDirAndFile:function(_1f){
this.changeClassName(_1f,"dir",this.options.dirSymbol);
this.changeClassName(_1f,"file",this.options.fileSymbol);
},convertJSON:function(){
return JSON.stringify(this.parse());
},createDirectoryBody:function(_20,_21){
var _22=null;
var _23=this.classNames.joinClassNames("dir");
if(_20.className!=_23){
_22=_20.className.replace(new RegExp(_23+" "),"");
_20.className=_23;
}
var _24=new Array();
var _25;
if(_21&&!this.options.emptyImg){
_25="state_empty";
}else{
if(this.options.open){
_25="state_open";
}else{
_25="state_close";
}
}
var id=this.options.iconIdPrefix.appendSuffix(_20.id);
var _27=Builder.node("DIV",{id:id.appendSuffix("stateImg")});
this.classNames.addClassNames(_27,_25);
Event.observe(_27,"click",this.toggle.bindAsEventListener(this));
var _28=Builder.node("DIV",{id:id});
this.classNames.addClassNames(_28,"dirIcon");
if(_22){
Element.addClassName(_28,_22);
}
this.classNames.addClassNames(_28,"handle");
var _29=Builder.node("SPAN",this.getDirectoryText(_20));
this.classNames.addClassNames(_29,"dirBodyText");
_24.push(_27);
_24.push(_28);
_24.push(_29);
var _2a=Builder.node("DIV",_24);
this.classNames.addClassNames(_2a,"dirBody");
if(this.options.dirSelect){
Event.observe(_28,"click",this.selectDirItem.bindAsEventListener(this));
Event.observe(_29,"click",this.selectDirItem.bindAsEventListener(this));
}
return _2a;
},createDirectoryContainer:function(_2b){
var _2c=_2b.getElementsByTagName("ul")[0];
if(!_2c){
_2c=Builder.node("UL");
}
this.classNames.addClassNames(_2c,"dirContainer");
if(!this.options.open){
Element.hide(_2c);
}
return _2c;
},createFileBody:function(_2d){
var _2e=null;
var _2f=this.classNames.joinClassNames("file");
if(_2d.className!=_2f){
_2e=_2d.className.replace(new RegExp(_2f+" "),"");
_2d.className=_2f;
}
var id=this.options.iconIdPrefix.appendSuffix(_2d.id);
var _31=Builder.node("DIV",{id:id});
this.classNames.addClassNames(_31,"fileIcon");
if(_2e){
Element.addClassName(_31,_2e);
}
this.classNames.addClassNames(_31,"handle");
var _32=Builder.node("SPAN",$A(_2d.childNodes));
this.classNames.addClassNames(_32,"fileBodyText");
var _33=new Array();
_33.push(_31);
_33.push(_32);
var _34=Builder.node("DIV",_33);
this.classNames.addClassNames(_34,"fileBody");
if(this.options.fileSelect){
Event.observe(_31,"click",this.selectFileItem.bindAsEventListener(this));
Event.observe(_32,"click",this.selectFileItem.bindAsEventListener(this));
}
return _34;
},getChildBody:function(_35){
var _36=[TreeView.className.fileBody,TreeView.className.dirBody];
return Element.getFirstElementByClassNames(_35,_36);
},getChildBodyText:function(_37){
var _38=[TreeView.className.fileBodyText,TreeView.className.fileBodyTextActive,TreeView.className.dirBodyText,TreeView.className.dirBodyTextActive];
return Element.getFirstElementByClassNames(_37,_38);
},getChildBodyTextNode:function(_39){
var _3a=this.getChildBody(_39);
var _3b=this.getChildBodyText(_3a);
return this.searchTextNode(_3b);
},getChildDir:function(_3c){
return document.getElementsByClassName(TreeView.className.dir,_3c);
},getChildDirBody:function(_3d){
return document.getElementsByClassName(TreeView.className.dirBody,_3d)[0];
},getChildDirContainer:function(_3e){
return document.getElementsByClassName(TreeView.className.dirContainer,_3e)[0];
},getChildStateImg:function(_3f){
var _40=this.getChildDirBody(_3f);
var _41=[TreeView.className.state_close,TreeView.className.state_open,TreeView.className.state_empty];
return Element.getFirstElementByClassNames(_40,_41);
},getChildren:function(_42,_43,_44){
var _45;
var _46=new Array();
if(_42){
_45=$(_42).getElementsByTagName("ul")[0];
}else{
_45=this.element;
}
$A(Element.getTagNodes(_45)).each(function(_47){
if(!_43&&Element.hasClassName(_47,TreeView.className.dir)){
_46.push(_47);
}
if(!_44&&Element.hasClassName(_47,TreeView.className.file)){
_46.push(_47);
}
});
return _46;
},getDirectoryContents:function(_48){
return $A(_48.childNodes).findAll(function(_49){
if((_49.nodeType!=1)){
return false;
}
if(_49.tagName.toLowerCase()=="li"){
return true;
}
return false;
});
},getDirectoryText:function(_4a){
return $A(_4a.childNodes).findAll(function(_4b){
if((_4b.nodeType!=1)){
return true;
}else{
if(_4b.tagName.toLowerCase()!="ul"){
return true;
}
}
return false;
});
},getHierarchyNumber:function(){
if(!this.selected){
return;
}
var _4c=this.selected;
var i=0;
while(true){
if(this.element==_4c){
return i;
}else{
_4c=this.getParentDir(_4c,true);
if(!_4c){
return;
}
i++;
}
}
},getParentDir:function(_4e,top){
var _50=Element.getParentByClassName(TreeView.className.dir,_4e);
if(!_50&&top){
_50=Element.getParentByClassName(TreeView.className.top,_4e);
}
return _50;
},hasContents:function(_51){
if(_51){
if(!Element.hasClassName(_51,TreeView.className.dirContainer)&&!Element.hasClassName(_51,TreeView.className.top)){
return false;
}
var _52=_51.childNodes;
for(var i=0;i<_52.length;i++){
if(_52[i].nodeType==1){
if(Element.hasClassName(_52[i],TreeView.className.dir)||Element.hasClassName(_52[i],TreeView.className.file)){
return true;
}
}
}
}
return false;
},parse:function(_54){
if(!_54){
_54=this.element;
}
var _55=[];
var _56=this.getDirectoryContents(_54);
for(var i=0;i<_56.length;i++){
var _58=_56[i];
var _59=this.getChildBody(_58);
var _5a=this.getChildBodyText(_59);
var _5b={};
_5b.id=_58.id;
_5b.name=Element.collectTextNodes(_5a).replace(/\n/,"");
if(Element.hasClassName(_58,TreeView.className.dir)){
_5b.type=this.options.dirSymbol;
_5b.contents=this.parse(this.getChildDirContainer(_58));
}else{
_5b.type=this.options.fileSymbol;
}
_55.push(_5b);
}
return _55;
},refreshStateImg:function(_5c){
if(!Element.hasClassName(_5c,TreeView.className.dir)){
return;
}
var _5d=this.getChildDirContainer(_5c);
var img=this.getChildStateImg(_5c);
if(!this.hasContents(_5d)&&!this.options.emptyImg){
this.classNames.refreshClassNames(img,"state_empty");
}else{
if(Element.visible(_5d)){
this.classNames.refreshClassNames(img,"state_open");
}else{
this.classNames.refreshClassNames(img,"state_close");
}
}
},removeById:function(_5f){
_5f=$(_5f);
if(_5f){
var _60=_5f.parentNode.parentNode;
Element.remove(_5f);
this.refreshStateImg(_60);
}
},removeByPath:function(_61){
var _62=_61.split("/").findAll(function(elm){
return (elm!="");
});
var _64=_62.pop();
var _65=this.search(_62.join("/"));
var _66=this.getDirectoryContents(_65)[_64];
if(_66){
this.removeById(_66);
}
},removeBySelected:function(){
if(!this.selected){
return;
}
this.removeById(this.selected);
this.selected=false;
},renameById:function(_67,_68){
_68=$(_68);
if(!Element.hasClassName(_68,TreeView.className.dir)&&!Element.hasClassName(_68,TreeView.className.file)){
return;
}
var _69=this.getChildBodyTextNode(_68);
_69.nodeValue=_67;
},renameByPath:function(_6a,_6b){
var _6c=_6b.split("/").findAll(function(elm){
return (elm!="");
});
var _6e=_6c.pop();
var _6f=this.search(_6c.join("/"));
var _70=this.getDirectoryContents(_6f)[_6e];
if(_70){
this.renameById(_6a,_70);
}
},renameBySelected:function(_71){
if(!this.selected){
return;
}
this.renameById(_71,this.selected);
},search:function(_72){
var _73=_72.split("/").findAll(function(elm){
return (elm!="");
});
var _75=this.element;
for(var i=0;i<_73.length;i++){
var num=_73[i];
var _78=this.getDirectoryContents(_75);
if(_78[num]&&Element.hasClassName(_78[num],TreeView.className.dir)){
_75=this.getChildDirContainer(_78[num]);
}else{
return false;
}
}
return _75;
},searchTextNode:function(_79){
var _7a=null;
var _7b=_79.childNodes;
for(var i=0;i<_7b.length;i++){
if(_7b[i].nodeType==3){
_7a=_7b[i];
break;
}else{
if(_7b[i].nodeType==1){
var tmp=this.searchTextNode(_7b[i]);
if(tmp){
_7a=tmp;
break;
}
}
}
}
return _7a;
},selectDirItem:function(_7e){
var _7f=Element.getParentByClassName(TreeView.className.dirBody,Event.element(_7e));
this.selectItem(_7f);
},selectEffect:function(_80){
_80=$(_80);
if(_80){
var _81=_80.firstChild;
if(this.selectItemUnselect(_81,false)){
return;
}
this.selectItemSelect(_81,false);
}
},selectFileItem:function(_82){
var _83=Element.getParentByClassName(TreeView.className.fileBody,Event.element(_82));
this.selectItem(_83);
},selectItem:function(_84){
if(this.selectItemUnselect(_84,true)){
return;
}
this.selectItemSelect(_84,true);
},selectItemSelect:function(_85,_86){
this.selected=_85.parentNode;
var _87=this.getChildBodyText(_85);
if(Element.hasClassName(_87,TreeView.className.dirBodyText)){
this.classNames.refreshClassNames(_87,"dirBodyTextActive");
this.defaultCss="dirBodyText";
}else{
if(Element.hasClassName(_87,TreeView.className.fileBodyText)){
this.classNames.refreshClassNames(_87,"fileBodyTextActive");
this.defaultCss="fileBodyText";
}
}
if(_86){
if(this.options.callBackFunctions){
for(var i=0;i<this.options.callBackFunctions.length;i++){
this.options.callBackFunctions[i](_85.parentNode);
}
}
}
},selectItemUnselect:function(_89,_8a){
if(this.selected){
var _8b=this.getChildBody(this.selected);
var _8c=this.getChildBodyText(_8b);
this.classNames.refreshClassNames(_8c,this.defaultCss);
if(this.selected==_89.parentNode&&this.options.enableUnselected){
this.selected=false;
this.defaultCss=false;
if(_8a){
this.options.unselected();
}
return true;
}
}
return false;
},setSortable:function(){
var _8d=Object.extend({dropOnEmpty:true,tree:true,hoverclass:"treeview_dirContainerHover",scroll:window,ghosting:true},this.options.sortOptions);
Sortable.create(this.element,_8d);
},toggle:function(_8e){
Event.stop(_8e);
var src=Event.element(_8e);
var _90=this.getParentDir(src);
var _91=this.getChildDirContainer(_90);
if(!this.hasContents(_91)&&!this.options.emptyImg){
return;
}
Element.toggle(_91);
this.refreshStateImg(_90);
if(!this.hasContents(_91)&&!this.options.emptyImg){
this.options.openDir(_90,_91);
}else{
if(Element.visible(_91)){
this.options.openDir(_90,_91);
}else{
this.options.closeDir(_90,_91);
}
}
}};

ResizeableWindowEx=Class.create();
Object.extend(Object.extend(ResizeableWindowEx.prototype,Resizeable.prototype),{startResize:function(_1){
if(Event.isLeftClick(_1)){
var _2=Event.element(_1);
if(_2.tagName&&(_2.tagName=="INPUT"||_2.tagName=="SELECT"||_2.tagName=="BUTTON"||_2.tagName=="TEXTAREA")){
return;
}
var _3=this.directions(_1);
if(_3.length>0){
this.active=true;
var _4=Position.cumulativeOffset(this.element);
this.startTop=_4[1];
this.startLeft=_4[0];
this.startWidth=parseInt(Element.getStyle(this.element,"width"));
this.startHeight=parseInt(Element.getStyle(this.element,"height"));
this.startX=_1.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
this.startY=_1.clientY+document.body.scrollTop+document.documentElement.scrollTop;
this.currentDirection=_3;
Event.stop(_1);
}
if(this.options.restriction){
var _5=this.element.parentNode;
var _6=Element.getDimensions(_5);
this.parentOffset=Position.cumulativeOffset(_5);
this.parentWidth=this.parentOffset[0]+_6.width;
this.parentHeight=this.parentOffset[1]+_6.height;
}
}
},draw:function(_7){
var _8=[Event.pointerX(_7),Event.pointerY(_7)];
if(this.options.restriction&&((this.parentWidth<=_8[0])||(this.parentHeight<=_8[1])||(this.parentOffset[0]>=_8[0])||(this.parentOffset[1]>=_8[1]))){
return;
}
var _9=this.element.style;
var _a=_9.height;
var _b=_9.width;
var _c=_9.top;
var _d=_9.left;
if(this.currentDirection.indexOf("n")!=-1){
var _e=this.startY-_8[1];
var _f=Element.getStyle(this.element,"margin-top")||"0";
_a=this.startHeight+_e;
_c=(this.startTop-_e-parseInt(_f))+"px";
}
if(this.currentDirection.indexOf("w")!=-1){
var _e=this.startX-_8[0];
var _f=Element.getStyle(this.element,"margin-left")||"0";
_b=this.startWidth+_e;
_d=this.startLeft-_e-parseInt(_f);
if(this.options.restriction){
_d-=this.parentOffset[0];
}
_d+="px";
}
if(this.currentDirection.indexOf("s")!=-1){
_a=this.startHeight+_8[1]-this.startY;
}
if(this.currentDirection.indexOf("e")!=-1){
_b=this.startWidth+_8[0]-this.startX;
}
var _10={height:_a,width:_b,top:_c,left:_d};
if(this.options.draw){
this.options.draw(_10,this.element);
}
if(_a&&_a>this.options.minHeight){
_9.top=_10.top;
_9.height=_10.height+"px";
}
if(_b&&_b>this.options.minWidth){
_9.left=_10.left;
_9.width=_10.width+"px";
}
if(_9.visibility=="hidden"){
_9.visibility="";
}
}});

var Window=Class.create();
Window.className={window:"window",header:"window_header",headerLeft:"window_headerLeft",headerMiddle:"window_headerMiddle",headerRight:"window_headerRight",buttonHolder:"window_buttonHolder",closeButton:"window_closeButton",maxButton:"window_maxButton",minButton:"window_minButton",body:"window_body",bodyLeft:"window_bodyLeft",bodyMiddle:"window_bodyMiddle",bodyRight:"window_bodyRight",bottom:"window_bottom",bottomLeft:"window_bottomLeft",bottomMiddle:"window_bottomMiddle",bottomRight:"window_bottomRight"};
Window.prototype={initialize:function(_1){
var _2=Object.extend({className:Window.className.window,width:300,height:300,minWidth:200,minHeight:40,drag:true,resize:true,resizeX:true,resizeY:true,modal:false,closeButton:true,maxButton:true,minButton:true,cssPrefix:"custom_",restriction:false,endDrag:Prototype.emptyFunction,endResize:Prototype.emptyFunction,addButton:Prototype.emptyFunction,preMaximize:function(){
return true;
},preMinimize:function(){
return true;
},preRevertMaximize:function(){
return true;
},preRevertMinimize:function(){
return true;
},preClose:function(){
return true;
},endMaximize:Prototype.emptyFunction,endMinimize:Prototype.emptyFunction,endRevertMaximize:Prototype.emptyFunction,endRevertMinimize:Prototype.emptyFunction,endClose:Prototype.emptyFunction,dragOptions:{},appendToBody:false},arguments[1]||{});
var _3=CssUtil.appendPrefix(_2.cssPrefix,Window.className);
this.classNames=new CssUtil([Window.className,_3]);
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
this.options=_2;
this.element.className=this.options.className;
this.header=null;
this.windowBody=null;
this.bottom=null;
this.elementId=this.element.id;
this.dragHandleId=this.elementId+"_dragHandle";
this.maxZindex=-1;
this.minFlag=false;
this.maxFlag=false;
this.currentPos=[0,0];
this.currentSize=[0,0];
this.buildWindow();
this.cover=new IECover(this.element,{padding:10});
Element.makePositioned(_1);
Element.hide(this.element);
Element.setStyle(this.element,{visibility:"visible"});
if(this.options.appendToBody){
this.appendToBody.callAfterLoading(this);
}
},buildWindow:function(){
Element.cleanWhitespace(this.element);
with(this.element.style){
width=this.options.width+"px";
height=this.options.height+"px";
}
var _4=this.element.childNodes[0];
var _5=this.element.childNodes[1];
this.buildHeader(_4);
this.buildBody(_5);
this.buildBottom();
var _6={height:this.options.height};
this.setBodyHeight(_6);
if(this.options.drag){
this.createDraggble();
}
if(this.options.resize){
this.enableResizing();
}
},buildHeader:function(_7){
var _8=Builder.node("div");
this.classNames.addClassNames(_8,"headerLeft");
var _9=Builder.node("div",{id:this.dragHandleId});
this.classNames.addClassNames(_9,"headerMiddle");
var _a=Builder.node("div");
this.classNames.addClassNames(_a,"headerRight");
var _b=Builder.node("div");
this.classNames.addClassNames(_b,"buttonHolder");
_9.appendChild(_7);
var _c=[_8,_9,_b,_a];
this.header=Builder.node("div",_c);
this.classNames.addClassNames(this.header,"header");
this.element.appendChild(this.header);
if(this.options.closeButton){
var _d=Builder.node("div",{id:this.element.id.appendSuffix("closeButton")});
this.classNames.addClassNames(_d,"closeButton");
_b.appendChild(_d);
Event.observe(_d,"click",this.close.bindAsEventListener(this));
}
if(this.options.maxButton){
var _e=Builder.node("div",{id:this.element.id.appendSuffix("maxButton")});
this.classNames.addClassNames(_e,"maxButton");
_b.appendChild(_e);
Event.observe(_e,"click",this.maximize.bindAsEventListener(this));
}
if(this.options.minButton){
var _f=Builder.node("div",{id:this.element.id.appendSuffix("minButton")});
this.classNames.addClassNames(_f,"minButton");
_b.appendChild(_f);
Event.observe(_f,"click",this.minimize.bindAsEventListener(this));
}
if(this.options.addButton){
var _10=this.options.addButton;
if(_10.constructor==Function){
_10(_b);
}else{
if(_10.constructor==Array){
var _11=this;
var _12=_b.firstChild;
_10.each(function(b){
var _14=Builder.node("div",{id:b.id,className:b.className});
Event.observe(_14,"click",b.onclick.bindAsEventListener(_11));
if(b.first&&_12){
_b.insertBefore(_14,_12);
}else{
_b.appendChild(_14);
}
});
}
}
}
},buildBody:function(_15){
var _16=Builder.node("div",{className:Window.className.bodyLeft});
this.classNames.addClassNames(_16,"bodyLeft");
var _17=Builder.node("div");
this.classNames.addClassNames(_17,"bodyMiddle");
_17.appendChild(_15);
var _18=Builder.node("div");
this.classNames.addClassNames(_18,"bodyRight");
var _19=[_18,_16,_17];
this.windowBody=Builder.node("div",_19);
this.classNames.addClassNames(this.windowBody,"body");
this.element.appendChild(this.windowBody);
},buildBottom:function(){
var _1a=Builder.node("div");
this.classNames.addClassNames(_1a,"bottomLeft");
var _1b=Builder.node("div");
this.classNames.addClassNames(_1b,"bottomMiddle");
var _1c=Builder.node("div");
this.classNames.addClassNames(_1c,"bottomRight");
var _1d=[_1a,_1b,_1c];
this.bottom=Builder.node("div",_1d);
this.classNames.addClassNames(this.bottom,"bottom");
this.element.appendChild(this.bottom);
},createDraggble:function(){
var _1e=this;
var _1f=Object.extend({handle:this.dragHandleId,starteffect:Prototype.emptyFunction,endeffect:Prototype.emptyFunction,endDrag:this.options.endDrag,scroll:window},this.options.dragOptions);
if(this.options.restriction){
_1f.snap=function(x,y){
function constrain(n,_23,_24){
if(n>_24){
return _24;
}else{
if(n<_23){
return _23;
}else{
return n;
}
}
}
var _25=Element.getDimensions(_1e.element);
var _26=Element.getDimensions(_1e.element.parentNode);
if(Element.getStyle(_1e.element.parentNode,"position")=="static"){
var _27=Position.positionedOffset(_1e.element.parentNode);
var _28=_27[0];
var _29=_27[1];
return [constrain(x,_28,_28+_26.width-_25.width),constrain(y,_29,_29+_26.height-_25.height)];
}else{
return [constrain(x,0,_26.width-_25.width),constrain(y,0,_26.height-_25.height)];
}
};
}else{
var p=Position.cumulativeOffset(Position.offsetParent(this.element));
_1f.snap=function(x,y){
return [((x+p[0])>=0)?x:0-p[0],((y+p[1])>=0)?y:0-p[1]];
};
}
new DraggableWindowEx(this.element,_1f);
},setWindowZindex:function(_2d){
_2d=this.getZindex(_2d);
this.element.style.zIndex=_2d;
},getZindex:function(_2e){
return ZindexManager.getIndex(_2e);
},open:function(_2f){
this.opening=true;
if(this.options.modal){
Modal.mask(this.element,{zIndex:_2f});
}else{
this.setWindowZindex(_2f);
}
Element.show(this.element);
this.cover.resetSize();
this.opening=false;
if(this.shouldClose){
this.close();
this.shouldClose=false;
}
},close:function(){
if(this.opening){
this.shouldClose=true;
}
if(!this.options.preClose(this)){
return;
}
this.element.style.zIndex=-1;
this.maxZindex=-1;
try{
Element.hide(this.element);
}
catch(e){
}
if(this.options.modal){
Modal.unmask();
}
this.options.endClose(this);
if(this.opening){
this.shouldClose=true;
}
},minimize:function(_30){
if(this.minFlag){
if(!this.options.preRevertMinimize(this)){
return;
}
Element.toggle(this.windowBody);
if(this.maxFlag){
this.minFlag=false;
this.setMax();
}else{
var _31={height:this.currentSize[1]};
this.setBodyHeight(_31);
this.element.style.width=this.currentSize[0];
this.element.style.height=this.currentSize[1];
this.element.style.left=this.currentPos[0];
this.element.style.top=this.currentPos[1];
this.maxFlag=false;
this.minFlag=false;
this.options.endRevertMinimize(this);
}
}else{
if(!this.options.preMinimize(this)){
return;
}
Element.toggle(this.windowBody);
if(!this.maxFlag){
this.currentPos=[Element.getStyle(this.element,"left"),Element.getStyle(this.element,"top")];
this.currentSize=[Element.getStyle(this.element,"width"),Element.getStyle(this.element,"height")];
}
this.setMin();
this.minFlag=true;
this.options.endMinimize(this);
}
this.cover.resetSize();
},maximize:function(_32){
if(this.maxFlag){
if(this.minFlag){
Element.toggle(this.windowBody);
this.minFlag=false;
this.setMax();
}else{
if(!this.options.preRevertMaximize(this)){
return;
}
var _33={height:parseInt(this.currentSize[1])};
this.setBodyHeight(_33);
this.element.style.width=this.currentSize[0];
this.element.style.height=this.currentSize[1];
this.element.style.left=this.currentPos[0];
this.element.style.top=this.currentPos[1];
this.maxFlag=false;
this.minFlag=false;
document.body.style.overflow="";
this.element.style.position=this.position;
if(this.parent){
if(this.nextElement){
this.parent.insertBefore(this.element,this.nextElement);
}else{
this.parent.appendChild(this.element);
}
}
this.options.endRevertMaximize(this);
}
}else{
if(!this.options.preMaximize(this)){
return;
}
if(!this.minFlag){
this.currentPos=[Element.getStyle(this.element,"left"),Element.getStyle(this.element,"top")];
this.currentSize=[Element.getStyle(this.element,"width"),Element.getStyle(this.element,"height")];
}else{
Element.toggle(this.windowBody);
this.minFlag=false;
}
this.parent=this.element.parentNode;
this.nextElement=Element.next(this.element,0);
this.position=Element.getStyle(this.element,"position");
document.body.style.overflow="hidden";
document.body.appendChild(this.element);
this.element.style.position="absolute";
this.setMax();
this.maxFlag=true;
this.options.endMaximize(this);
}
this.cover.resetSize();
},setMin:function(){
var _34=this.header.offsetHeight+this.bottom.offsetHeight;
var _35=this.options.minWidth;
this.element.style.height=_34+"px";
this.element.style.width=_35+"px";
},setMax:function(_36){
var _37=Element.getWindowWidth();
var _38=Element.getWindowHeight();
var _39={height:_38};
with(this.element.style){
width=_37+"px";
height=_38+"px";
left="0px";
top="0px";
}
this.setBodyHeight(_39);
this.setWindowZindex(_36);
},_getParentWidth:function(_3a){
if(_3a&&_3a.style){
var _3b=_3a.style.width;
var _3c=0;
if(_3b){
if((_3c=_3b.indexOf("px",0))>0){
return parseInt(_3b);
}else{
if((_3c=_3b.indexOf("%",0))>0){
var pw=this._getParentWidth(_3a.parentNode);
var par=parseInt(_3b);
return pw*par/100;
}else{
if(!_3b.isNaN){
return parseInt(_3b);
}
}
}
}else{
if(_3a==document.body){
return Element.getWindowWidth();
}
}
}
},setHeight:function(_3f){
_3f={height:_3f};
Element.setStyle(this.element,_3f);
this.setBodyHeight(_3f);
},setBodyHeight:function(_40){
var _41=parseInt(_40.height);
if(_41>this.options.minHeight){
var _42=(_41-this.header.offsetHeight-this.bottom.offsetHeight)+"px";
this.windowBody.childNodes[0].style.height=_42;
this.windowBody.childNodes[1].style.height=_42;
this.windowBody.childNodes[2].style.height=_42;
this.windowBody.style.height=_42;
}
if(this.cover){
this.cover.resetSize();
}
},center:function(){
var w=parseInt(Element.getStyle(this.element,"width"));
var h=parseInt(Element.getStyle(this.element,"height"));
var _45=Position.cumulativeOffset(Position.offsetParent(this.element));
var _46=(Element.getWindowWidth()-w)/2;
var top=(Element.getWindowHeight()-h)/2;
var _48=(document.documentElement.scrollTop||document.body.scrollTop);
var _49=(document.documentElement.scrollLeft||document.body.scrollLeft);
top+=_48-_45[1];
_46+=_49-_45[0];
top=((top+_45[1])>=0)?top:0-_45[1];
_46=((_46+_45[0])>=0)?_46:0-_45[0];
Element.setStyle(this.element,{left:_46+"px",top:top+"px"});
},enableResizing:function(){
var _4a=this.options.resizeY?6:0;
var _4b=this.options.resizeY?6:0;
var _4c=this.options.resizeX?6:0;
var _4d=this.options.resizeX?6:0;
this.resizeable=new ResizeableWindowEx(this.element,{top:_4a,bottom:_4b,left:_4c,right:_4d,minWidth:this.options.minWidth,minHeight:this.options.minHeight,draw:this.setBodyHeight.bind(this),resize:this.options.endResize,restriction:this.options.restriction,zindex:2000});
},disableResizing:function(){
this.resizeable.destroy();
},appendToBody:function(){
this.removeFromBody(this.element.id);
document.body.appendChild(this.element);
},removeFromBody:function(_4e){
$A(document.body.childNodes).each(function(_4f){
if(_4f.id==_4e){
_4f.remove();
}
});
}};
var DraggableWindowEx=Class.create();
Object.extend(DraggableWindowEx.prototype,Draggable.prototype);
Object.extend(DraggableWindowEx.prototype,{initDrag:function(_50){
if(Event.isLeftClick(_50)){
var src=Event.element(_50);
if(src.tagName&&(src.tagName=="INPUT"||src.tagName=="SELECT"||src.tagName=="OPTION"||src.tagName=="BUTTON"||src.tagName=="TEXTAREA")){
return;
}
if(this.element._revert){
this.element._revert.cancel();
this.element._revert=null;
}
var _52=[Event.pointerX(_50),Event.pointerY(_50)];
var pos=Position.cumulativeOffset(this.element);
this.offset=[0,1].map(function(i){
return (_52[i]-pos[i]);
});
var _55=ZindexManager.getIndex();
this.originalZ=_55;
this.options.zindex=_55;
Element.setStyle(this.element,{zIndex:_55});
Draggables.activate(this);
Event.stop(_50);
}
},endDrag:function(_56){
if(!this.dragging){
return;
}
this.stopScrolling();
this.finishDrag(_56,true);
this.options.endDrag();
Event.stop(_56);
}});

