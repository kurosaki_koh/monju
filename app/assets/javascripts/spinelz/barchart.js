var AbstractBarChart=Class.create();
AbstractBarChart.prototype={initialize:function(_1,_2){
this.options=Object.extend({title:"",graduation:true,graduationMin:0,graduationMax:30,graduationInterval:10,graduationRange:50,titleFont:"normal bold 20px serif"},arguments[2]||{});
this.fontSize="normal normal 12px serif";
this.graduationLine="2px solid gray";
this.element=$(_1);
Element.setStyle(this.element,{visibility:"hidden"});
Element.hide(this.element);
this.params=_2;
this.init();
this.hide();
this.refresh();
Element.setStyle(this.element,{visibility:"visible"});
Element.show(this.element);
},build:function(){
return Builder.node("DIV",[this.buildTitle(),this.buildGraduationLine(),this.buildContent()]);
},buildTitle:function(){
return Builder.node("DIV",{style:"font: "+this.options.titleFont+"; margin-bottom: 5px; text-align: center;"},[this.options.title]);
},show:function(){
Element.show(this.element);
},hide:function(){
Element.hide(this.element);
},refresh:function(_3){
if(this.chart){
this.remove();
}
this.chart=this.build();
this.element.appendChild(this.chart);
},getParam:function(_4){
return this.params.detect(function(_5){
return (_5.getName()==_4);
});
},remove:function(){
var _6=this.chart;
var _7=this.element;
$A(this.element.childNodes).any(function(_8){
if(_8==_6){
_7.removeChild(_8);
return true;
}
return false;
});
this.chart=null;
},getChartSize:function(){
return this.getPlusChartSize()+this.getMinusChartSize();
},getPlusChartSize:function(){
if(this.options.graduationMax<=0){
return 0;
}
return this.options.graduationMax/this.options.graduationInterval*this.options.graduationRange;
},getMinusChartSize:function(){
if(this.options.graduationMin>=0){
return 0;
}
return Math.abs(this.options.graduationMin)/this.options.graduationInterval*this.options.graduationRange;
},getBarSize:function(_9){
var _a=_9.getValue()/this.options.graduationInterval*this.options.graduationRange;
return Math.abs(_a);
},getFontSize:function(){
var _b=this.fontSize.split(" ");
if(_b.length==0){
return 0;
}
var _c=_b.detect(function(_d){
return _d.match(/px$/);
});
if(!_c){
return 0;
}
var _e=_c.indexOf("px");
if(_e<1){
return 0;
}
_c=_c.substring(0,_e);
if(isNaN(_c)){
return 0;
}
return parseInt(_c);
}};
var HorizontalBarChart=Class.create();
Object.extend(Object.extend(HorizontalBarChart.prototype,AbstractBarChart.prototype),{init:function(){
this.nameWidth=125;
this.valueWidth=50;
this.itemInterval=35;
this.adjustWidth=(document.all)?5:0;
},buildGraduationLine:function(){
if(!this.options.graduation){
return Builder.node("DIV",{style:"margin-bottom: 5px; text-align: center;"});
}
var _f=new Array();
var i=this.options.graduationMin;
var _11=this.options.graduationRange;
var _12=new StyleManager();
_12.cache("border-left",this.graduationLine);
_12.cache("height","5px");
_12.cache("font-size","5px");
while(i<=this.options.graduationMax){
var j=i+this.options.graduationInterval;
if(j<=this.options.graduationMax){
_12.add("border-top",this.graduationLine);
}
var elm=Builder.node("DIV",{style:"float: left; font: "+this.fontSize+"; width: "+_11+"px;"},[Builder.node("DIV",[i]),Builder.node("DIV",{style:_12.output()})]);
_f.push(elm);
i=j;
_12.clear();
}
return Builder.node("DIV",{style:"margin-left: "+(this.nameWidth+this.valueWidth)+"px; padding-bottom: "+this.itemInterval+"px;"},_f);
},buildContent:function(){
var _15=new Array();
var _16=new StyleManager();
_16.cache("font",this.fontSize);
_16.cache("text-align","right");
for(var i=0;i<this.params.length;i++){
var _18=this.params[i];
var _19;
if(_18.getValue()>0){
_19=this.getPlusChartSize();
}else{
_19=this.getMinusChartSize();
}
var _1a=this.getBarSize(_18);
_1a=(_1a>_19)?_19:_1a;
var _1b;
if(_18.options.image){
if(_18.getValue()>0){
_1b=Builder.node("DIV",[Builder.node("IMG",{src:_18.options.image,alt:"bar",width:_1a+"px",height:_18.options.imageHeight+"px",style:"float: left; margin-left: "+(this.getMinusChartSize()+this.valueWidth)+"px;"}),Builder.node("DIV",{style:"font: "+this.fontSize+";"},[_18.getValue()])]);
}else{
var _1c=this.getMinusChartSize()-_1a;
var _1d;
if(document.all&&_1c>125){
_1d=_1c+this.valueWidth;
_1c=0;
}else{
_1d=this.valueWidth;
}
_16.add("margin-left",_1c+"px");
_16.add("width",_1d+"px");
_16.add("float","left");
_1b=Builder.node("DIV",[Builder.node("DIV",{style:_16.output()},[_18.getValue()]),Builder.node("IMG",{src:_18.options.image,alt:"bar",width:_1a+"px",height:_18.options.imageHeight+"px"})]);
}
}else{
if(_18.getValue()>0){
_1b=Builder.node("DIV",{style:"font: "+this.fontSize+"; margin-left: "+(this.getMinusChartSize()+this.nameWidth+this.valueWidth+this.adjustWidth)+"px; border-left: "+_1a+"px solid "+_18.options.color+";"},[_18.getValue()]);
}else{
var _1d=(document.all)?(this.valueWidth+_1a):this.valueWidth;
_16.add("margin-left",(this.nameWidth+this.getMinusChartSize()-_1a)+"px");
_16.add("width",_1d+"px");
_16.add("border-right",_1a+"px solid "+_18.options.color);
_1b=Builder.node("DIV",[Builder.node("DIV",{style:_16.output()},[_18.getValue()])]);
}
}
var elm=Builder.node("DIV",{style:"margin-bottom: 10px;"},[Builder.node("DIV",{style:"text-align: right; font: "+this.fontSize+";float: left; width: "+this.nameWidth+"px;"},[_18.getName()]),_1b]);
_15.push(elm);
_16.clear();
}
return _15;
}});
var VerticalBarChart=Class.create();
Object.extend(Object.extend(VerticalBarChart.prototype,AbstractBarChart.prototype),{init:function(){
},buildGraduationLine:function(){
if(!this.options.graduation){
return Builder.node("DIV");
}
var _1f=new Array();
var _20=new Array();
var i=this.options.graduationMax;
var _22;
var _23=(document.all)?this.options.graduationRange:this.options.graduationRange-2;
var _24=_23-this.getFontSize();
var _25=new StyleManager();
_25.cache("font",this.fontSize);
var _26=new StyleManager();
_26.cache("font",this.fontSize);
while(i>=this.options.graduationMin){
_22=i-this.options.graduationInterval;
_23=(document.all)?_23-0.3:_23-0.1;
if(i==this.options.graduationMax){
_26.add("margin-top","0px");
}else{
if(_22<this.options.graduationMin){
_25.add("margin-top",_24+"px");
_26.add("border-left",this.graduationLine);
_26.add("border-top",this.graduationLine);
_26.add("border-bottom",this.graduationLine);
_26.add("height",_23+"px");
_26.add("width","100%");
}else{
_25.add("margin-top",_24+"px");
_26.add("border-left",this.graduationLine);
_26.add("border-top",this.graduationLine);
_26.add("height",_23+"px");
_26.add("width","100%");
}
}
_1f.push(Builder.node("DIV",{style:_25.output()},[i]));
if(i==this.options.graduationMax){
_20.push(Builder.node("DIV",{style:_26.output()},[Builder.node("BR")]));
}else{
_20.push(Builder.node("DIV",{style:_26.output()}));
}
_25.clear();
_26.clear();
_24-=(document.all)?0.05:0.1;
i=_22;
}
return [Builder.node("DIV",{style:"float: left; margin-left: 10px; width: 25px; text-align: right;"},_1f),Builder.node("DIV",{style:"float: left; margin-right: 10px; margin-left: 10px; width: 5px;"},_20)];
},buildContent:function(){
var _27=new Array();
for(var i=0;i<this.params.length;i++){
var _29=this.params[i];
var _2a;
if(_29.getValue()>0){
_2a=this.getPlusChartSize();
}else{
_2a=this.getMinusChartSize();
}
var _2b=this.getBarSize(_29);
_2b=(_2b>_2a)?_2a:_2b;
var bar;
if(_29.options.image){
bar=Builder.node("IMG",{src:_29.getImage(),alt:"bar",width:"10px",height:_2b+"px"});
}else{
bar=Builder.node("DIV",{style:"border-top: "+_2b+"px solid "+_29.options.color+"; width: 10px; font-size: 0;"});
}
var elm;
if(_29.getValue()>0){
elm=Builder.node("DIV",{style:"float: left; margin-right: 10px; width: 25px;"},[Builder.node("DIV",{style:"margin-top: "+(_2a-_2b)+"px; font: "+this.fontSize+";"},[_29.getValue()]),bar,Builder.node("DIV",{style:"margin-top: "+(this.getMinusChartSize()+15)+"px; font: "+this.fontSize+"; padding-top: 5px; width: 10px;"},[_29.getName()])]);
}else{
elm=Builder.node("DIV",{style:"float: left; margin-right: 10px; width: 25px;"},[Builder.node("DIV",{style:"margin-top: "+(this.getPlusChartSize()+this.getFontSize())+"px"}),bar,Builder.node("DIV",{style:"font: "+this.fontSize+";"},[_29.getValue()]),Builder.node("DIV",{style:"margin-top: "+(_2a+15-(this.getFontSize()+_2b))+"px; font: "+this.fontSize+"; padding-top: 5px; width: 10px;"},[_29.getName()])]);
}
_27.push(elm);
}
_27.push(Builder.node("DIV",{style:"clear: left;"}));
return _27;
}});
var ChartParameter=Class.create();
ChartParameter.prototype={initialize:function(_2e,_2f){
if(isNaN(_2f)){
throw "[ChartParameter] value property must be number!("+_2f+")";
}
this.name=_2e;
this.value=_2f;
this.options=Object.extend({color:"#FF0000",barHeight:"15",image:false,imageHeight:"15",action:false},arguments[2]||{});
},getName:function(){
return this.name;
},getValue:function(){
return this.value;
},setValue:function(_30){
if(!_30||isNaN(_30)){
this.value=0;
}else{
this.value=_30;
}
},getImage:function(){
return this.options.image;
}};
var StyleManager=Class.create();
StyleManager.prototype={initialize:function(){
this.cacheItems=new Array();
this.items=new Array();
},cache:function(_31){
if(typeof (_31)=="object"){
this.cacheItems.push(_31);
}else{
if(arguments[1]){
this.cacheItems.push(new StyleItem(_31,arguments[1]));
}
}
},add:function(_32){
if(typeof (_32)=="object"){
this.items.push(_32);
}else{
if(arguments[1]){
this.items.push(new StyleItem(_32,arguments[1]));
}
}
},get:function(_33){
return this.items.detect(function(_34){
return _34.getName()==_33;
});
},remove:function(_35){
this.items=this.items.findAll(function(_36){
return _36.getName()!=_35;
});
},clear:function(_37){
this.items=new Array();
},modify:function(_38,_39){
var _3a=this.get(_38);
if(_3a){
_3a.setValue(_39);
}else{
this.add(_38,_39);
}
},output:function(){
var _3b="";
this.cacheItems.each(function(_3c){
_3b+=_3c.toString();
});
this.items.each(function(_3d){
_3b+=_3d.toString();
});
return _3b;
}};
var StyleItem=Class.create();
StyleItem.prototype={initialize:function(_3e,_3f){
this.name=_3e;
this.value=_3f;
},setName:function(_40){
this.name=_40;
},setValue:function(_41){
this.value=_41;
},getName:function(){
return this.name;
},getValue:function(){
return this.value;
},toString:function(){
return this.getName()+":"+this.getValue()+"; ";
}};

