dndFlag = false

isAncestor =  (node , sourceNode)->
  while(node.getParent() )
    if node.getParent() == sourceNode
      return false
    node = node.getParent()
    return true


@fixTree = ->
  $('#ytree').dynatree()

@editTree = ->
  $('#ytree').dynatree
    onDblClick: (node)->
      window.open(node.data.href , '_top') if node.data.href
    dnd:
      onDragStart: (node) ->
        return dndFlag
      onDragEnter: isAncestor
      onDrop: (node, sourceNode, hitMode, ui, draggable) ->
        sourceNode.move(node, hitMode)
        node.expand()
        console.log " node #{sourceNode.data.title} dropped to  #{node.data.title } with #{hitMode}"  if dndFlag
        $('#loading').show();
        $('ul.dynatree-container').css('background-color' , '#eeffee')
        $.post '/yggdrasills/'+sourceNode.data.key+'/move' ,
          destination: node.data.key
          hit_mode: hitMode
          (data) ->
            $('#loading').hide();
            $('ul.dynatree-container').css('background-color' , '#ffffee')


@disableTree = ->
  dndFlag = false
  $('ul.dynatree-container').css('background-color' , 'white')

@enableTree = ->
  dndFlag = true
  $('ul.dynatree-container').css('background-color' , '#ffffee')

$ ->
  editTree()

