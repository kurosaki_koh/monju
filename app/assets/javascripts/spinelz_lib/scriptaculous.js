var Scriptaculous={Version:"1.6.4",require:function(_1){
document.write("<script type=\"text/javascript\" src=\""+_1+"\"></script>");
},load:function(){
if((typeof Prototype=="undefined")||(typeof Element=="undefined")||(typeof Element.Methods=="undefined")||parseFloat(Prototype.Version.split(".")[0]+"."+Prototype.Version.split(".")[1])<1.5){
throw ("script.aculo.us requires the Prototype JavaScript framework >= 1.5.0");
}
$A(document.getElementsByTagName("script")).findAll(function(s){
return (s.src&&s.src.match(/scriptaculous\.js(\?.*)?$/));
}).each(function(s){
var _4=s.src.replace(/scriptaculous\.js(\?.*)?$/,"");
var _5=s.src.match(/\?.*load=([a-z,]*)/);
(_5?_5[1]:"builder,effects,dragdrop,controls,slider").split(",").each(function(_6){
Scriptaculous.require(_4+_6+".js");
});
});
}};
Scriptaculous.load();

