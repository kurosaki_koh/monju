var Resizeable=Class.create();
Resizeable.prototype={initialize:function(_1){
var _2=Object.extend({top:6,bottom:6,left:6,right:6,minHeight:0,minWidth:0,zindex:1000,resize:null},arguments[1]||{});
this.element=$(_1);
this.handle=this.element;
Element.makePositioned(this.element);
this.options=_2;
this.active=false;
this.resizing=false;
this.currentDirection="";
this.eventMouseDown=this.startResize.bindAsEventListener(this);
this.eventMouseUp=this.endResize.bindAsEventListener(this);
this.eventMouseMove=this.update.bindAsEventListener(this);
this.eventCursorCheck=this.cursor.bindAsEventListener(this);
this.eventKeypress=this.keyPress.bindAsEventListener(this);
this.registerEvents();
},destroy:function(){
Event.stopObserving(this.handle,"mousedown",this.eventMouseDown);
this.unregisterEvents();
},registerEvents:function(){
Event.observe(document,"mouseup",this.eventMouseUp);
Event.observe(document,"mousemove",this.eventMouseMove);
Event.observe(document,"keypress",this.eventKeypress);
Event.observe(this.handle,"mousedown",this.eventMouseDown);
Event.observe(this.element,"mousemove",this.eventCursorCheck);
},unregisterEvents:function(){
},startResize:function(_3){
if(Event.isLeftClick(_3)){
var _4=Event.element(_3);
if(_4.tagName&&(_4.tagName=="INPUT"||_4.tagName=="SELECT"||_4.tagName=="BUTTON"||_4.tagName=="TEXTAREA")){
return;
}
var _5=this.directions(_3);
if(_5.length>0){
this.active=true;
var _6=Position.cumulativeOffset(this.element);
this.startTop=_6[1];
this.startLeft=_6[0];
this.startWidth=parseInt(Element.getStyle(this.element,"width"));
this.startHeight=parseInt(Element.getStyle(this.element,"height"));
this.startX=_3.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
this.startY=_3.clientY+document.body.scrollTop+document.documentElement.scrollTop;
this.currentDirection=_5;
Event.stop(_3);
}
}
},finishResize:function(_7,_8){
this.active=false;
this.resizing=false;
if(this.options.zindex){
this.element.style.zIndex=this.originalZ;
}
if(this.options.resize){
this.options.resize(this.element);
}
},keyPress:function(_9){
if(this.active){
if(_9.keyCode==Event.KEY_ESC){
this.finishResize(_9,false);
Event.stop(_9);
}
}
},endResize:function(_a){
if(this.active&&this.resizing){
this.finishResize(_a,true);
Event.stop(_a);
}
this.active=false;
this.resizing=false;
},draw:function(_b){
var _c=[Event.pointerX(_b),Event.pointerY(_b)];
var _d=this.element.style;
if(this.currentDirection.indexOf("n")!=-1){
var _e=this.startY-_c[1];
var _f=Element.getStyle(this.element,"margin-top")||"0";
var _10=this.startHeight+_e;
if(_10>this.options.minHeight){
_d.height=_10+"px";
_d.top=(this.startTop-_e-parseInt(_f))+"px";
}
}
if(this.currentDirection.indexOf("w")!=-1){
var _e=this.startX-_c[0];
var _f=Element.getStyle(this.element,"margin-left")||"0";
var _11=this.startWidth+_e;
if(_11>this.options.minWidth){
_d.left=(this.startLeft-_e-parseInt(_f))+"px";
_d.width=_11+"px";
}
}
if(this.currentDirection.indexOf("s")!=-1){
var _10=this.startHeight+_c[1]-this.startY;
if(_10>this.options.minHeight){
_d.height=_10+"px";
}
}
if(this.currentDirection.indexOf("e")!=-1){
var _11=this.startWidth+_c[0]-this.startX;
if(_11>this.options.minWidth){
_d.width=_11+"px";
}
}
if(_d.visibility=="hidden"){
_d.visibility="";
}
},between:function(val,low,_14){
return (val>=low&&val<_14);
},directions:function(_15){
var _16=[Event.pointerX(_15),Event.pointerY(_15)];
var _17=Position.cumulativeOffset(this.element);
var _18="";
if(this.between(_16[1]-_17[1],0,this.options.top)){
_18+="n";
}
if(this.between((_17[1]+this.element.offsetHeight)-_16[1],0,this.options.bottom)){
_18+="s";
}
if(this.between(_16[0]-_17[0],0,this.options.left)){
_18+="w";
}
if(this.between((_17[0]+this.element.offsetWidth)-_16[0],0,this.options.right)){
_18+="e";
}
return _18;
},cursor:function(_19){
var _1a=this.directions(_19);
if(_1a.length>0){
_1a+="-resize";
}else{
_1a="";
}
this.element.style.cursor=_1a;
},update:function(_1b){
if(this.active){
if(!this.resizing){
var _1c=this.element.style;
this.resizing=true;
if(Element.getStyle(this.element,"position")==""){
_1c.position="relative";
}
if(this.options.zindex){
this.originalZ=parseInt(Element.getStyle(this.element,"z-index")||0);
_1c.zIndex=this.options.zindex;
}
}
this.draw(_1b);
if(navigator.appVersion.indexOf("AppleWebKit")>0){
window.scrollBy(0,0);
}
Event.stop(_1b);
return false;
}
}};

