Event.simulateMouse=function(_1,_2){
var _3=Object.extend({pointerX:0,pointerY:0,buttons:0},arguments[2]||{});
var _4=document.createEvent("MouseEvents");
_4.initMouseEvent(_2,true,true,document.defaultView,_3.buttons,_3.pointerX,_3.pointerY,_3.pointerX,_3.pointerY,false,false,false,false,0,$(_1));
if(this.mark){
Element.remove(this.mark);
}
this.mark=document.createElement("div");
this.mark.appendChild(document.createTextNode(" "));
document.body.appendChild(this.mark);
this.mark.style.position="absolute";
this.mark.style.top=_3.pointerY+"px";
this.mark.style.left=_3.pointerX+"px";
this.mark.style.width="5px";
this.mark.style.height="5px;";
this.mark.style.borderTop="1px solid red;";
this.mark.style.borderLeft="1px solid red;";
if(this.step){
alert("["+new Date().getTime().toString()+"] "+_2+"/"+Test.Unit.inspect(_3));
}
$(_1).dispatchEvent(_4);
};
Event.simulateKey=function(_5,_6){
var _7=Object.extend({ctrlKey:false,altKey:false,shiftKey:false,metaKey:false,keyCode:0,charCode:0},arguments[2]||{});
var _8=document.createEvent("KeyEvents");
_8.initKeyEvent(_6,true,true,window,_7.ctrlKey,_7.altKey,_7.shiftKey,_7.metaKey,_7.keyCode,_7.charCode);
$(_5).dispatchEvent(_8);
};
Event.simulateKeys=function(_9,_a){
for(var i=0;i<_a.length;i++){
Event.simulateKey(_9,"keypress",{charCode:_a.charCodeAt(i)});
}
};
var Test={};
Test.Unit={};
Test.Unit.inspect=Object.inspect;
Test.Unit.Logger=Class.create();
Test.Unit.Logger.prototype={initialize:function(_c){
this.log=$(_c);
if(this.log){
this._createLogTable();
}
},start:function(_d){
if(!this.log){
return;
}
this.testName=_d;
this.lastLogLine=document.createElement("tr");
this.statusCell=document.createElement("td");
this.nameCell=document.createElement("td");
this.nameCell.appendChild(document.createTextNode(_d));
this.messageCell=document.createElement("td");
this.lastLogLine.appendChild(this.statusCell);
this.lastLogLine.appendChild(this.nameCell);
this.lastLogLine.appendChild(this.messageCell);
this.loglines.appendChild(this.lastLogLine);
},finish:function(_e,_f){
if(!this.log){
return;
}
this.lastLogLine.className=_e;
this.statusCell.innerHTML=_e;
this.messageCell.innerHTML=this._toHTML(_f);
},message:function(_10){
if(!this.log){
return;
}
this.messageCell.innerHTML=this._toHTML(_10);
},summary:function(_11){
if(!this.log){
return;
}
this.logsummary.innerHTML=this._toHTML(_11);
},_createLogTable:function(){
this.log.innerHTML="<div id=\"logsummary\"></div>"+"<table id=\"logtable\">"+"<thead><tr><th>Status</th><th>Test</th><th>Message</th></tr></thead>"+"<tbody id=\"loglines\"></tbody>"+"</table>";
this.logsummary=$("logsummary");
this.loglines=$("loglines");
},_toHTML:function(txt){
return txt.escapeHTML().replace(/\n/g,"<br/>");
}};
Test.Unit.Runner=Class.create();
Test.Unit.Runner.prototype={initialize:function(_13){
this.options=Object.extend({testLog:"testlog"},arguments[1]||{});
this.options.resultsURL=this.parseResultsURLQueryParameter();
if(this.options.testLog){
this.options.testLog=$(this.options.testLog)||null;
}
if(this.options.tests){
this.tests=[];
for(var i=0;i<this.options.tests.length;i++){
if(/^test/.test(this.options.tests[i])){
this.tests.push(new Test.Unit.Testcase(this.options.tests[i],_13[this.options.tests[i]],_13["setup"],_13["teardown"]));
}
}
}else{
if(this.options.test){
this.tests=[new Test.Unit.Testcase(this.options.test,_13[this.options.test],_13["setup"],_13["teardown"])];
}else{
this.tests=[];
for(var _15 in _13){
if(/^test/.test(_15)){
this.tests.push(new Test.Unit.Testcase(this.options.context?" -> "+this.options.titles[_15]:_15,_13[_15],_13["setup"],_13["teardown"]));
}
}
}
}
this.currentTest=0;
this.logger=new Test.Unit.Logger(this.options.testLog);
setTimeout(this.runTests.bind(this),1000);
},parseResultsURLQueryParameter:function(){
return window.location.search.parseQuery()["resultsURL"];
},getResult:function(){
var _16=false;
for(var i=0;i<this.tests.length;i++){
if(this.tests[i].errors>0){
return "ERROR";
}
if(this.tests[i].failures>0){
_16=true;
}
}
if(_16){
return "FAILURE";
}else{
return "SUCCESS";
}
},postResults:function(){
if(this.options.resultsURL){
new Ajax.Request(this.options.resultsURL,{method:"get",parameters:"result="+this.getResult(),asynchronous:false});
}
},runTests:function(){
var _18=this.tests[this.currentTest];
if(!_18){
this.postResults();
this.logger.summary(this.summary());
return;
}
if(!_18.isWaiting){
this.logger.start(_18.name);
}
_18.run();
if(_18.isWaiting){
this.logger.message("Waiting for "+_18.timeToWait+"ms");
setTimeout(this.runTests.bind(this),_18.timeToWait||1000);
}else{
this.logger.finish(_18.status(),_18.summary());
this.currentTest++;
this.runTests();
}
},summary:function(){
var _19=0;
var _1a=0;
var _1b=0;
var _1c=[];
for(var i=0;i<this.tests.length;i++){
_19+=this.tests[i].assertions;
_1a+=this.tests[i].failures;
_1b+=this.tests[i].errors;
}
return ((this.options.context?this.options.context+": ":"")+this.tests.length+" tests, "+_19+" assertions, "+_1a+" failures, "+_1b+" errors");
}};
Test.Unit.Assertions=Class.create();
Test.Unit.Assertions.prototype={initialize:function(){
this.assertions=0;
this.failures=0;
this.errors=0;
this.messages=[];
},summary:function(){
return (this.assertions+" assertions, "+this.failures+" failures, "+this.errors+" errors"+"\n"+this.messages.join("\n"));
},pass:function(){
this.assertions++;
},fail:function(_1e){
this.failures++;
this.messages.push("Failure: "+_1e);
},info:function(_1f){
this.messages.push("Info: "+_1f);
},error:function(_20){
this.errors++;
this.messages.push(_20.name+": "+_20.message+"("+Test.Unit.inspect(_20)+")");
},status:function(){
if(this.failures>0){
return "failed";
}
if(this.errors>0){
return "error";
}
return "passed";
},assert:function(_21){
var _22=arguments[1]||"assert: got \""+Test.Unit.inspect(_21)+"\"";
try{
_21?this.pass():this.fail(_22);
}
catch(e){
this.error(e);
}
},assertEqual:function(_23,_24){
var _25=arguments[2]||"assertEqual";
try{
(_23==_24)?this.pass():this.fail(_25+": expected \""+Test.Unit.inspect(_23)+"\", actual \""+Test.Unit.inspect(_24)+"\"");
}
catch(e){
this.error(e);
}
},assertEnumEqual:function(_26,_27){
var _28=arguments[2]||"assertEnumEqual";
try{
$A(_26).length==$A(_27).length&&_26.zip(_27).all(function(_29){
return _29[0]==_29[1];
})?this.pass():this.fail(_28+": expected "+Test.Unit.inspect(_26)+", actual "+Test.Unit.inspect(_27));
}
catch(e){
this.error(e);
}
},assertNotEqual:function(_2a,_2b){
var _2c=arguments[2]||"assertNotEqual";
try{
(_2a!=_2b)?this.pass():this.fail(_2c+": got \""+Test.Unit.inspect(_2b)+"\"");
}
catch(e){
this.error(e);
}
},assertIdentical:function(_2d,_2e){
var _2f=arguments[2]||"assertIdentical";
try{
(_2d===_2e)?this.pass():this.fail(_2f+": expected \""+Test.Unit.inspect(_2d)+"\", actual \""+Test.Unit.inspect(_2e)+"\"");
}
catch(e){
this.error(e);
}
},assertNotIdentical:function(_30,_31){
var _32=arguments[2]||"assertNotIdentical";
try{
!(_30===_31)?this.pass():this.fail(_32+": expected \""+Test.Unit.inspect(_30)+"\", actual \""+Test.Unit.inspect(_31)+"\"");
}
catch(e){
this.error(e);
}
},assertNull:function(obj){
var _34=arguments[1]||"assertNull";
try{
(obj==null)?this.pass():this.fail(_34+": got \""+Test.Unit.inspect(obj)+"\"");
}
catch(e){
this.error(e);
}
},assertMatch:function(_35,_36){
var _37=arguments[2]||"assertMatch";
var _38=new RegExp(_35);
try{
(_38.exec(_36))?this.pass():this.fail(_37+" : regex: \""+Test.Unit.inspect(_35)+" did not match: "+Test.Unit.inspect(_36)+"\"");
}
catch(e){
this.error(e);
}
},assertHidden:function(_39){
var _3a=arguments[1]||"assertHidden";
this.assertEqual("none",_39.style.display,_3a);
},assertNotNull:function(_3b){
var _3c=arguments[1]||"assertNotNull";
this.assert(_3b!=null,_3c);
},assertType:function(_3d,_3e){
var _3f=arguments[2]||"assertType";
try{
(_3e.constructor==_3d)?this.pass():this.fail(_3f+": expected \""+Test.Unit.inspect(_3d)+"\", actual \""+(_3e.constructor)+"\"");
}
catch(e){
this.error(e);
}
},assertNotOfType:function(_40,_41){
var _42=arguments[2]||"assertNotOfType";
try{
(_41.constructor!=_40)?this.pass():this.fail(_42+": expected \""+Test.Unit.inspect(_40)+"\", actual \""+(_41.constructor)+"\"");
}
catch(e){
this.error(e);
}
},assertInstanceOf:function(_43,_44){
var _45=arguments[2]||"assertInstanceOf";
try{
(_44 instanceof _43)?this.pass():this.fail(_45+": object was not an instance of the expected type");
}
catch(e){
this.error(e);
}
},assertNotInstanceOf:function(_46,_47){
var _48=arguments[2]||"assertNotInstanceOf";
try{
!(_47 instanceof _46)?this.pass():this.fail(_48+": object was an instance of the not expected type");
}
catch(e){
this.error(e);
}
},assertRespondsTo:function(_49,obj){
var _4b=arguments[2]||"assertRespondsTo";
try{
(obj[_49]&&typeof obj[_49]=="function")?this.pass():this.fail(_4b+": object doesn't respond to ["+_49+"]");
}
catch(e){
this.error(e);
}
},assertReturnsTrue:function(_4c,obj){
var _4e=arguments[2]||"assertReturnsTrue";
try{
var m=obj[_4c];
if(!m){
m=obj["is"+_4c.charAt(0).toUpperCase()+_4c.slice(1)];
}
m()?this.pass():this.fail(_4e+": method returned false");
}
catch(e){
this.error(e);
}
},assertReturnsFalse:function(_50,obj){
var _52=arguments[2]||"assertReturnsFalse";
try{
var m=obj[_50];
if(!m){
m=obj["is"+_50.charAt(0).toUpperCase()+_50.slice(1)];
}
!m()?this.pass():this.fail(_52+": method returned true");
}
catch(e){
this.error(e);
}
},assertRaise:function(_54,_55){
var _56=arguments[2]||"assertRaise";
try{
_55();
this.fail(_56+": exception expected but none was raised");
}
catch(e){
(e.name==_54)?this.pass():this.error(e);
}
},assertElementsMatch:function(){
var _57=$A(arguments),_58=$A(_57.shift());
if(_58.length!=_57.length){
this.fail("assertElementsMatch: size mismatch: "+_58.length+" elements, "+_57.length+" expressions");
return false;
}
_58.zip(_57).all(function(_59,_5a){
var _5b=$(_59.first()),_5c=_59.last();
if(_5b.match(_5c)){
return true;
}
this.fail("assertElementsMatch: (in index "+_5a+") expected "+_5c.inspect()+" but got "+_5b.inspect());
}.bind(this))&&this.pass();
},assertElementMatches:function(_5d,_5e){
this.assertElementsMatch([_5d],_5e);
},benchmark:function(_5f,_60){
var _61=new Date();
(_60||1).times(_5f);
var _62=((new Date())-_61);
this.info((arguments[2]||"Operation")+" finished "+_60+" iterations in "+(_62/1000)+"s");
return _62;
},_isVisible:function(_63){
_63=$(_63);
if(!_63.parentNode){
return true;
}
this.assertNotNull(_63);
if(_63.style&&Element.getStyle(_63,"display")=="none"){
return false;
}
return this._isVisible(_63.parentNode);
},assertNotVisible:function(_64){
this.assert(!this._isVisible(_64),Test.Unit.inspect(_64)+" was not hidden and didn't have a hidden parent either. "+(""||arguments[1]));
},assertVisible:function(_65){
this.assert(this._isVisible(_65),Test.Unit.inspect(_65)+" was not visible. "+(""||arguments[1]));
},benchmark:function(_66,_67){
var _68=new Date();
(_67||1).times(_66);
var _69=((new Date())-_68);
this.info((arguments[2]||"Operation")+" finished "+_67+" iterations in "+(_69/1000)+"s");
return _69;
}};
Test.Unit.Testcase=Class.create();
Object.extend(Object.extend(Test.Unit.Testcase.prototype,Test.Unit.Assertions.prototype),{initialize:function(_6a,_6b,_6c,_6d){
Test.Unit.Assertions.prototype.initialize.bind(this)();
this.name=_6a;
if(typeof _6b=="string"){
_6b=_6b.gsub(/(\.should[^\(]+\()/,"#{0}this,");
_6b=_6b.gsub(/(\.should[^\(]+)\(this,\)/,"#{1}(this)");
this.test=function(){
eval("with(this){"+_6b+"}");
};
}else{
this.test=_6b||function(){
};
}
this.setup=_6c||function(){
};
this.teardown=_6d||function(){
};
this.isWaiting=false;
this.timeToWait=1000;
},wait:function(_6e,_6f){
this.isWaiting=true;
this.test=_6f;
this.timeToWait=_6e;
},run:function(){
try{
try{
if(!this.isWaiting){
this.setup.bind(this)();
}
this.isWaiting=false;
this.test.bind(this)();
}
finally{
if(!this.isWaiting){
this.teardown.bind(this)();
}
}
}
catch(e){
this.error(e);
}
}});
Test.setupBDDExtensionMethods=function(){
var _70={shouldEqual:"assertEqual",shouldNotEqual:"assertNotEqual",shouldEqualEnum:"assertEnumEqual",shouldBeA:"assertType",shouldNotBeA:"assertNotOfType",shouldBeAn:"assertType",shouldNotBeAn:"assertNotOfType",shouldBeNull:"assertNull",shouldNotBeNull:"assertNotNull",shouldBe:"assertReturnsTrue",shouldNotBe:"assertReturnsFalse",shouldRespondTo:"assertRespondsTo"};
Test.BDDMethods={};
for(m in _70){
Test.BDDMethods[m]=eval("function(){"+"var args = $A(arguments);"+"var scope = args.shift();"+"scope."+_70[m]+".apply(scope,(args || []).concat([this])); }");
}
[Array.prototype,String.prototype,Number.prototype].each(function(p){
Object.extend(p,Test.BDDMethods);
});
};
Test.context=function(_72,_73,log){
Test.setupBDDExtensionMethods();
var _75={};
var _76={};
for(specName in _73){
switch(specName){
case "setup":
case "teardown":
_75[specName]=_73[specName];
break;
default:
var _77="test"+specName.gsub(/\s+/,"-").camelize();
var _78=_73[specName].toString().split("\n").slice(1);
if(/^\{/.test(_78[0])){
_78=_78.slice(1);
}
_78.pop();
_78=_78.map(function(_79){
return _79.strip();
});
_75[_77]=_78.join("\n");
_76[_77]=specName;
}
}
new Test.Unit.Runner(_75,{titles:_76,testLog:log||"testlog",context:_72});
};

