command_resources = {}
included_members = []
included_resources = []
main_member = null
members = []
query_cache = []

this.included_resources = included_resources #for debug

format_cr = (prop) ->
  result = (if prop.owner then ("(" + prop.owner + ")") else "")
  result += prop.name + ":" + prop.power.toString() + ":" + prop.tag
  result

append_delete_icon = (item, handler)->
  item.append(
    $('<span>')
    .addClass('middle')
    .html('&nbsp')
    .append(
      $('<img>')
      .attr('src', '/assets/icons/cross.png')
      .attr('alt', '[削除]')
      .attr('title', '削除')
      .data('model', item.data('model'))
      .click handler
    )
  )
  return item

class Unit
  @command_resources = {}
  @unit_cache = {}

  constructor: (@character_no, @name) ->
    this.command_resources = []
    this.member = false

  toString: ->
    return @character_no + ':' + @name

  @get_command_resources: (unit, func) ->
    unless Unit.unit_cache[unit]?
      $.getJSON "/command_resources.js", unit, (data) ->
        Unit.unit_cache[unit] = unit
        owner = unit.character_no + ':' + unit.name;
        unit.member = true
        unit.command_resources = []

        unit.command_resources.push new CommandResource(resource, unit) for resource in data

        Unit.command_resources[unit] = unit.command_resources

        console.log('received data:')
        console.log(data)
        func()
        return
      return
    else
      func()
    return

  @init_unit_cache: ->
    def_member = window.Definition?.members || []
    for u in window.MainUnits.concat def_member
      unit = new Unit(u.character_no, u.name)
      unit.command_resources = []
      unit.command_resources.push new CommandResource(resource,
        unit) for resource in window.CommandResources[unit]

      Unit.unit_cache[unit] = unit
      unit.member = true
      Unit.command_resources[unit] = unit.command_resources

  @create_by_label: (label) ->
    character_id_re = /(\d{2}-.{5}-.{2}):(.+)/
    array = null
    if array = character_id_re.exec(label)
      new Unit(array[1], array[2])
    else
      null

  toHash: ->
    return {character_no: @character_no , name: @name}

  @find: (character_no , name) ->
    return Unit.unit_cache[character_no + ':' + name]

class IdressObject
  @objects_cache = []

  constructor: (@name , @definition_name)->
    this.member = false


  toString: ->
    return "#{@name}：#{@definition_name}"


class CommandResource
  @attributes = ['name', 'tag', 'power' , 'id','source_id' , 'source_type']
  @dictionary = []
  constructor: (definition, @owner) ->
    this[i] = definition[i] for i in CommandResource.attributes
    this.used = false
    this.owner = owner
    CommandResource.dictionary[this.id] = this

  owner_name: ->
    console.log this
    return this.owner.toString()

  list_item: ->
    item = $('<li>')
    item.attr(i, this[i]) for i in CommandResource.attributes
    item.data('model', this)
    item.addClass('used') if this.used
    return item

  toString: ->
    return @name + ':' + @power + ':' + @tag

  format: ->
    return '(' + @owner_name() + ')' + @toString()

class IncludedCommandResource
  constructor: (definition, @children) ->
    @children = children
    this[i] = definition[i] for i in CommandResource.attributes
    c.used = true for c in this.children
    
  remove: ->
    c.used = false for c in @children

  toHash: ->
    return {
      command_resource: {name: @name , power: @power , tag: @tag},
      sources: (c.id for c in @children)
    }



class IncludedCommandResourceController
  @models = []

  @create_include: ->
    name = $('#include_name').val()
    children = $('.ui-selected').map -> $(this).data 'model'

    cr.used = true for cr in children
    included_resource = new IncludedCommandResource({
      name: name
      power: (cr.power for cr in children).reduce (a, b) -> a + b
      tag: $('#tag_selection option:selected').val()
      children: children
    }, children )

    return included_resource

  @add:  (include)->
    include = @create_include() unless include?
    $('#include_name').val('')
    @models.push include
    @add_view include

    CommandResourceSelectionController.update()
    return

  @add_view: (include) ->
    resource_list_items = (cr.list_item().html(cr.format()) for cr in include.children )
    resources_root = $('<ol>')
    resources_root.append item for item in resource_list_items

    included_resource_item = $("<li/>").attr('id', 'include_' + include.name)
    included_resource_item.attr(i, include[i]) for i in CommandResource.attributes
    included_resource_item.append('【' + format_cr(include) + '】');
    included_resource_item.data 'model' , include

    append_delete_icon(included_resource_item , @remove_handler)

    resources_root.appendTo included_resource_item
    $('#included_command_resources').append included_resource_item

  @update: ->
    $('#included_command_resources > li').remove()
    @add_view include  for include in @models
    @update_add_button()
    return


  @remove_handler: ->
    IncludedCommandResourceController.remove($(this).data 'model')

  @remove: (include)->
    @models.splice( @models.indexOf(include) , 1)
    include.remove()
    @update()
    CommandResourceSelectionController.update()

  @remove_all: ->
    @remove include for include in @models

  @is_legal: ->
    include_name = $("#include_name").val()
    ui_selected = $(".ui-selected").length

    duplicated = include_name in (model.name for model in @models)
    return (include_name.length > 0 ) and (not duplicated) and (ui_selected is 9)

  @update_add_button: ->
    $("#add_include_button").attr "disabled", not @is_legal()

class CommandResourceSelectionController
  @update: (selected_tag) ->
    console.log selected_tag
    selected_tag ?= $('#tag_selection option:selected').val()
    $('#command_resources > li').remove()
    @update_spec()
    targets = (if main_member then [main_member] else []).concat(members)
    selected_resources = []
    for owner in targets
      for resource in Unit.command_resources[owner]
        if selected_tag is `undefined` or selected_tag is resource.tag or selected_tag is "全て"
          selected_resources.push resource

    not_used = []
    used = []
    for resource in selected_resources
      console.log resource
      if resource.used
        used.push resource
      else
        not_used.push resource

    for resource in not_used.concat used
      cr = " (" + resource.owner + ") " + resource
      $("#command_resources").append (resource.list_item().html("<input type='checkbox' #{if resource.used then 'disabled' else ''}/> " + cr) )

    $("#command_resources > li > input[type='checkbox']").change ->
      if this.checked
        $(this).parent('li').addClass('ui-selected')
      else
        $(this).parent('li').removeClass('ui-selecting').removeClass('ui-selected')

      CommandResourceSelectionController.update_spec()
      return

    $('#command_resources > li').tooltip
      show: false
      position:
        at: 'right bottom'
        collision: 'fit'
      content: ->
        model = $(this).data('model')
        unless model.source_type?
          '単体コマンドリソース'
        else if model.source_type == 'Yggdrasill'
          'now loading...'
        else if model.source_type == 'ConvertSource'
          'コンバート'
      open: ->
        model = $(this).data('model')
        if model.source_type == 'Yggdrasill'
          Yggdrasill.get_definition($(this))
      items:
        '[name]'


  @update_spec : ->
    sum_power = 0
    $('.ui-selected').each ->
      sum_power += parseInt(this.getAttribute('power'))
      return

    $('#sum_power').text sum_power.toString()
    $('#num_selected').text $('.ui-selected').length.toString()
    return


class Yggdrasill
  @models = []

  @get_definition: (ui) ->
    model = ui.data('model')
    id = model.source_id
    unless Yggdrasill.models[id]?
      $.getJSON "/yggdrasills/"+id+".js",(data) ->
        Yggdrasill.models[id] = data

        console.log('received data:')
        console.log(data)
        model.source = data

        ui.tooltip('close')
        ui.tooltip('option','content' , '<pre>' + data.definition + '</pre>')
        return
      return Yggdrasill.models[id]
    else
      ui.tooltip('option','content' , '<pre>' + Yggdrasill.models[id].definition + '</pre>')


$ ->
  $.fn.input = (fn) ->
    $this = this
    return $this.trigger("keydown.input")  unless fn
    $this.bind
      "input.input": (event) ->
        $this.unbind "keydown.input"
        fn.call this, event
        return

      "keydown.input": (event) ->
        fn.call this, event
        return

  toggle_form = ->
    flag = $("#unit_registry_type").val() is "IncludeRegistry"
    console.log flag
    $("#form1").toggle not flag
    $("#form2").toggle flag
    return

  init_definition = ->
    return unless window.Definition
    main_member_uid = window.Definition.main_member.character_no + ':' + window.Definition.main_member.name
    $("#main_unit_selection option[value='"+main_member_uid+"']" ).prop('selected',true)
    $("#main_unit_selection").change()
    included_members = (Unit.find(m.character_no , m.name) for m in window.Definition.members)
    update_members()

    for inc in window.Definition.includes
      children = (CommandResource.dictionary[id] for id in inc.sources)
      IncludedCommandResourceController.add new IncludedCommandResource({
          name: inc.command_resource.name
          power: inc.command_resource.power
          tag: inc.command_resource.tag
          children: children
        }, children)


  $("#unit_registry_type").change ->
    toggle_form()


  match_unit_registry_id = (str) ->
    character_id_re = /(\d{2}-.{5}-.{2})：(.+)/
    array = null
    if array = character_id_re.exec(str)
      new Unit(array[1], array[2])
    else
      null


  match_members = (members) ->
    results = []

    for line in members
      result = match_unit_registry_id(line.trim())
      if result
        console.log result
        results.push result

    return results


  update_tag_selection = ->
    _tags = {'全て': []}
    all_crs = []
    targets = [main_member].concat(members)
    for unit_id  in targets
      resources = Unit.command_resources[unit_id]
      console.log resources
      for idx , resource of resources
        all_crs.push resource
        _tags[resource.tag] ||= []
        _tags[resource.tag].push resource

      $('#tag_selection option').remove()
      _tags['全て'] = all_crs
      for tag , resources of _tags
        option = $('<option>').html(tag + '(' + resources.length + ')').val(tag)
        option.attr('style', 'background: #DDFFDD') if resources.length >= 9 and tag != '全て'
        $('#tag_selection').append(option)


  update_cr_div = ->
    update_tag_selection()
    CommandResourceSelectionController.update()
    update_unit_selection()

    $('#loading_message').hide()
    return

  get_command_resources = (unit) ->
    Unit.get_command_resources unit, update_cr_div
    return

  update_add_button_state = ->
    $("#add_unit_button").attr("disabled", $("#unit_selection option:selected").length == 0)
    return

  update_add_include_button_state = ->
    include_name = $("#include_name").val()
    ui_selected = $(".ui-selected").length
    $("#add_include_button").attr "disabled", not (include_name.length > 0 and ui_selected is 9)
    return

  update_unit_selection = (data = query_cache) ->
    $("#unit_selection option").remove();
    $.each data, (idx, option) ->
      unit = Unit.unit_cache[option.label] || Unit.create_by_label(option.label)
      console.log(unit)
      console.log(option)
      console.log(Unit.unit_cache[option.label])

      opt = $('<option>')
      .html option.label
      .val option.value
      .data 'model', unit

      opt.prop('disabled', true) if unit.member

      $("#unit_selection").append(opt)
    update_add_button_state()


  delete_member = ->
    target = $(this).data('model')
    target.member = false
    included_members.some (v, i) ->
      #console.log(v.toString() == $(this).data('model').toString())
      included_members.splice(i, 1) if v.toString() == target.toString()

    update_members()
    update_unit_selection()
    update_cr_div()
    return


  create_member_item = (member)->
    $('<li>')
    .text(member.toString())
    .data('model', member)


  update_members = ->
    members.length = 0
    members = included_members.concat()

    window.members = members
    window.main_member = main_member
    $('#included_members').empty()

    $('#included_members').append(create_member_item main_member) if main_member

    for member in members
      item = create_member_item(member)
      append_delete_icon(item, delete_member)
      $('#included_members').append item

    IncludedCommandResourceController.remove_all()

  update_include_resource = ->
    sum_power = 0
    $('.ui-selected').each ->
      sum_power += parseInt(this.getAttribute('power'))
      return

    $('#sum_power').text sum_power.toString()
    $('#num_selected').text $('.ui-selected').length.toString()
    return


  _selectRange = false
  _deselectQueue = []

  $('#loading_message').hide()


  _keyDownCode = 0

  query_unit_registries = ->
    $.ajax
#        url: "/owner_accounts.js"
      url: "/unit_registries.js"
      data:
        q: $('#character_no_selection').val()

      dataType: "json"
      success: (data) ->
        console.log data
        query_cache = data
        update_unit_selection data
        return
    update_add_button_state()


  $('#character_no_selection').input (e)->
    _keyDownCode = e.which
    query_unit_registries() if $(this).val().length >= 2
    update_add_button_state()
  #e.preventDefault()

  $('#main_unit_selection').change ->
    uid = $('#main_unit_selection option:selected').val()
    main_member = Unit.unit_cache[uid]

    update_members()

    get_command_resources main_member

  $('#unit_selection').change ->
    update_add_button_state()

  $('#include_name').keyup ->
    $('#new_name').text $('#include_name').val()
    IncludedCommandResourceController.update_add_button()

  $('#add_include_button').click ->
    IncludedCommandResourceController.add()

  $('#add_object_button').click ->
    IncludeCommandResourceController.add_object()

  $('#add_unit_button').click ->
    unit = $('#unit_selection option:selected').data('model')
    included_members.push unit
    unit.member = true
    update_members()

    get_command_resources unit

  $('#command_resources').selectable
    filter: ':not(.used)'
    selecting: (event, ui) ->
      if event.detail == 0
        _selectRange = true
        return true

      if $(ui.selecting).hasClass('ui-selected')
        _deselectQueue.push(ui.selecting)

    unselecting: (event, ui) ->
      $(ui.unselecting).addClass('ui-selected')

    stop: ->
      unless _selectRange
        $.each _deselectQueue, (ix, de) ->
          $(de).removeClass('ui-selecting').removeClass('ui-selected')

      sum_power = 0
      _selectRange = false
      _deselectQueue = []

      $('.ui-selected input', this).each ->
        this.checked = true
        return

      $('li.ui-selectee:not(.ui-selected) input', this).each ->
        this.checked = false
        return

      CommandResourceSelectionController.update_spec()
      IncludedCommandResourceController.update_add_button()
#      update_add_include_button_state()


  $('#tag_selection').change ->
    selected_tag = $('#tag_selection option:selected').val()
    console.log selected_tag
    CommandResourceSelectionController.update selected_tag

    selected_tag = '' if selected_tag == '全て'
    $('#num_selected').text '0'
    $('#new_tag').text selected_tag

    return

  $('form').submit ->
    if $('#unit_registry_type').val() == 'IncludeRegistry'
      definition = {
        main_member: main_member.toHash(),
        members: (member.toHash() for member in members) ,
        includes: (model.toHash() for model in IncludedCommandResourceController.models)
      }
      $("#include_def").val JSON.stringify definition

    return true

  toggle_form()

  if $('#main_unit_selection option').size() == 1
    $('#main_unit_selection option:first').prop('selected' , true)
    $('#main_unit_selection').change()

  registration = $('#registration')

  $(window).scroll ->
    if $(window).scrollTop() > 80
      registration.addClass 'fixed'
    else
      registration.removeClass 'fixed'
    return

  Unit.init_unit_cache()
  init_definition()

  return



window.Unit = Unit
window.IncludedCommandResourceController = IncludedCommandResourceController
window.members = members
window.main_member = main_member
window.CommandResource = CommandResource







