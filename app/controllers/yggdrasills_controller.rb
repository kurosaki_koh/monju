# encoding: utf-8

require 'jiken/parser.rb'

class YggdrasillsController < ApplicationController
  before_filter :login_required
  requires_role :admin, :only => [:create, :update, :destroy]
  set_login_filter

  requires_role :monju_maintainer ,
                :only => [:new , :edit , :create , :update , :destroy  , :move]


  def index
    list
    render :action => 'list'
  end

  def list
    @root = if params[:owner_account_id]
              @owner_account = OwnerAccount.find(params[:owner_account_id])
              @owner_account.yggdrasills.root
            else
              Yggdrasills.root
            end
  end

  def show
    @owner_account = OwnerAccount.find(params[:owner_account_id]) if params[:owner_account_id]
    @yggdrasill = Yggdrasill.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.js { render json: @yggdrasill }
    end
  end

  def new
    @owner_account = OwnerAccount.find(params[:owner_account_id]) if params[:owner_account_id]
    @parent_node = params[:parent_id] ? Yggdrasill.find(params[:parent_id]) : nil
    @yggdrasill = Yggdrasill.new
    @yggdrasill.owner_account = @owner_account
  end

  def create
    @owner_account = OwnerAccount.find(params[:yggdrasill][:owner_account_id]) if params[:yggdrasill][:owner_account_id]
    @yggdrasill = Yggdrasill.new(params[:yggdrasill])
    begin
      Yggdrasill.transaction do
        parent = if params[:parent_id]
                   Yggdrasill.find(params[:parent_id])
                 else
                   @owner_account.yggdrasills.root
                 end
        if parent.nil?
          parent = Yggdrasill.create!(name: 'root', idress_name: 'root', owner_account_id: @owner_account.id)
          parent.reload
        end
        @yggdrasill.save
        @yggdrasill.move_to_child_of(parent)
      end
    rescue => e
      raise e
      render owner_account_yggdrasills_path(@owner_account)
    end
    flash[:notice] = 'イグドラシルの新規登録に成功しました。'
    redirect_to owner_account_yggdrasills_path(@owner_account)
  end

  def edit
    @owner_account = OwnerAccount.find(params[:owner_account_id]) if params[:owner_account_id]
    @yggdrasill = Yggdrasill.find(params[:id])
  end

  def update
    @owner_account = OwnerAccount.find(params[:owner_account_id]) if params[:owner_account_id]
    @yggdrasill = Yggdrasill.find(params[:id])
    if @yggdrasill.update_attributes(params[:yggdrasill])
      flash[:notice] = 'Yggdrasill was successfully updated.'
      redirect_to owner_account_yggdrasills_path(@owner_account)
    else
      render :action => 'edit'
    end
  end

  def destroy
    @owner_account = OwnerAccount.find(params[:owner_account_id]) if params[:owner_account_id]

    @node = Yggdrasill.find(params[:id])
    Yggdrasill.transaction do
      @children = @node.children
      @parent = @node.parent
      @children.each{|child| child.move_to_child_of @parent}
      @node.delete
    end

    redirect_to owner_account_yggdrasills_path(@owner_account)
  end

  def move
    node = Yggdrasill.find(params[:id])
    destination = Yggdrasill.find(params[:destination])

    case params[:hit_mode]
      when 'over'
        node.move_to_child_of(destination)
      when 'before'
        node.move_to_left_of(destination)
      when 'after'
        node.move_to_right_of(destination)
    end
    node.reload
    render :json =>node
  end
end

