require 'jiken/parser.rb'

class JobsController < ApplicationController
  before_filter :login_required 
  requires_role :admin , :only => [:create , :update  , :destroy  ]
  set_login_filter
  def index
    list
    render :action => 'list'
  end

  def list
     @jobs = Job.paginate(
         :page => params[:page], :order => 'id' , :per_page => 20)
  end

  def show
    @job = Job.find(params[:id])
  end

  def new
    @job = Job.new
  end

  def create
    @job = Job.new(params[:job])
    if @job.save
      flash[:notice] = 'Job was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @job = Job.find(params[:id])
  end

  def update
    @job = Job.find(params[:id])
    if @job.update_attributes(params[:job])
      flash[:notice] = 'Job was successfully updated.'
      redirect_to :action => 'show', :id => @job
    else
      render :action => 'edit'
    end
  end

  def parse_idef
    parser = ILanguageLibraryParser.new
    root = parser.parse(params[:i_definition])
    node = root.node if root && root.node
    name = node.name
    evals = node.evaluation

    result = if node
      ''
    else
      parser.failure_reason
    end
    render :update do |page|
      page[:job_name].value = name
      for col in Job::Abilities
        page['job_'+ col.to_s].value = evals[Job::AbilityKanji[col]]
      end
      page[:job_note].value = result
    end
  end

  def destroy
    Job.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end

