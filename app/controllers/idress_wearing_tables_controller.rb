# -*- encoding: utf-8 -*-
require_dependency 'i_language/parser/lib/jcode_util.rb'
class IdressWearingTablesController < ApplicationController
  include JcodeUtil
  # GET /idress_wearing_tables
  # GET /idress_wearing_tables.xml
  def index
    result = IdressWearingTable.find_all_by_nation_id(params[:nation_id])
    @idress_wearing_tables = result.sort_by{|rec|rec.created_at}.reverse
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @idress_wearing_tables }
    end
  end

  # GET /idress_wearing_tables/1
  # GET /idress_wearing_tables/1.xml
  def show
    @idress_wearing_table = IdressWearingTable.find(params[:id])

    @originator_check_results = {}
    for wearing in @idress_wearing_table.idress_wearings
      @originator_check_results[wearing] = originator_check(wearing)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @idress_wearing_table }
    end
  end

  def vcc_format
    @idress_wearing_table = IdressWearingTable.find(params[:id])
    @idress_wearing_table.idress_wearings.delete_if{|row|['-','なし'].any?{|i|i == row[:job_idress]}}

#    @originator_check_results = {}
#    for wearing in @idress_wearing_table.idress_wearings
#      @originator_check_results[wearing] = originator_check(wearing)
#    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @idress_wearing_table }
    end
  end

  # GET /idress_wearing_tables/new
  # GET /idress_wearing_tables/new.xml
  def new
    @idress_wearing_table = IdressWearingTable.new
    @idress_wearing_table.nation_id = params[:nation_id]
    @idress_wearing_table.turn = Property[:current_turn].to_s
    @idress_wearing_table.title = "ターン#{Property[:current_turn].to_s}着用アイドレス表"
    @idress_wearing_table.init_idress_wearing_table()
    @idress_wearing_table.is_current = true
    prepare_job_idresses
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @idress_wearing_table }
    end
  end

  #GET /idress_wearing_tables/1/copy
  def copy
    source = IdressWearingTable.find(params[:id])

    @idress_wearing_table = IdressWearingTable.new
    @idress_wearing_table.nation_id = params[:nation_id]
    @idress_wearing_table.turn = Property[:current_turn].to_s
    @idress_wearing_table.title = source.title + '(コピー)'
    @idress_wearing_table.idress_wearings = source.idress_wearings

    prepare_job_idresses
    respond_to do |format|
      format.html {render :action => :new}# new.html.erb
      format.xml  { render :xml => @idress_wearing_table }
    end
  end
  # GET /idress_wearing_tables/1/edit
  def edit
    if params[:task]=='削除'
      destroy
      return
    end
    @idress_wearing_table = IdressWearingTable.find(params[:id])
    if !@idress_wearing_table.authenticated?(params[:password])
      flash[:notice] = 'パスワードが一致しません。'
      redirect_to :back
    end
    prepare_job_idresses
  end

  # POST /idress_wearing_tables
  # POST /idress_wearing_tables.xml
  def create
    @idress_wearing_table = IdressWearingTable.new(params[:idress_wearing_table])
    @idress_wearing_table.nation_id = params[:nation_id]
    @idress_wearing_table.idress_wearings = parse_wearings
    @idress_wearing_table.password = params[:password]
    @idress_wearing_table.deltas = []
    respond_to do |format|
      if @idress_wearing_table.save
        flash[:notice] = '着用アイドレス表を作成しました。'
        format.html { redirect_to(nation_idress_wearing_table_path(params[:nation_id],@idress_wearing_table)) }
        format.xml  { render :xml => @idress_wearing_table, :status => :created, :location => @idress_wearing_table }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @idress_wearing_table.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /idress_wearing_tables/1
  # PUT /idress_wearing_tables/1.xml
  def update
    @idress_wearing_table = IdressWearingTable.find(params[:id])
    if !@idress_wearing_table.authenticated?(params[:password])
      flash[:notice] = 'パスワードが一致しません。'
      redirect_to :back
      return
    end
    if !params[:update_list].blank?
      @idress_wearing_table.reset_character_wearings
      @idress_wearing_table.save
      respond_to do |format|
        flash[:notice] = '表の記載を現在の国民登録で更新しました。'
        format.html { redirect_to :action => :show }
      end
      return
    end
    @idress_wearing_table.add_deltas(params[:idress_wearing_table],parse_wearings())
    update_model

    respond_to do |format|
      if @idress_wearing_table.save
        flash[:notice] = '着用アイドレス表を更新しました。'
        format.html { redirect_to([@idress_wearing_table.nation,@idress_wearing_table]) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @idress_wearing_table.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /idress_wearing_tables/1
  # DELETE /idress_wearing_tables/1.xml
  def destroy
    @idress_wearing_table = IdressWearingTable.find(params[:id])
    if !@idress_wearing_table.authenticated?(params[:password])
      flash[:notice] = 'パスワードが一致しません。'
      redirect_to :back
      return
    end
    @idress_wearing_table.destroy

    respond_to do |format|
      format.html { redirect_to(nation_idress_wearing_tables_url(params[:nation_id])) }
      format.xml  { head :ok }
    end
  end

  def restriction_check
    @idress_wearing_table = IdressWearingTable.find(params[:id])

    @originator_check_results = {}
    for wearing in @idress_wearing_table.idress_wearings
      @originator_check_results[wearing] = originator_check(wearing)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @idress_wearing_table }
    end
  end

  private
  def parse_wearings
    wearings = params[:idress_wearing_table][:idress_wearings]
    wearings.keys.sort.collect{|i|IdressWearingTable::IdressWearing.new(wearings[i])}
  end

  def prepare_job_idresses
    nation = Nation.find(params[:nation_id])
    @nation_job_idresses = nation.owner_account.job_idresses.select{|j|
      j.idress_type == 'JobIdress' && j.regular? && j.regular_no
    }.sort_by(&JobIdress::sort_by_race_lambda(nation.belong)).collect{|j|
      {:name => j.inspect_idresses , :race_type => j.race_type}
    }
    @nation_job_idresses.unshift({:name => 'なし' , :race_type => ''})
    @nation_job_idresses.push({:name => '-' , :race_type => ''}) 
    characters = nation.characters
    @person_job_idresses = characters.inject({}){|hash,c|
      hash[c.character_no]=c.owner_account.job_idresses.collect{|j|
        if j.idress_type == 'JobIdress'
          j.inspect_idresses
        else
          nil
        end
      }.compact
      hash
    }
    @character_races = characters.inject({}){|hash , c|
      hash[c.character_no] = c.race_type
      hash
    }
    @person_job4_idresses = characters.inject({}){|hash,c|
      hash[c.character_no]=c.acquired_idresses.collect{|j|j.inspect_idresses}
      hash
    }
  end

  def update_model
    @idress_wearing_table.nation_id = params[:nation_id]
    @idress_wearing_table.title = params[:idress_wearing_table][:title]
    @idress_wearing_table.turn = params[:idress_wearing_table][:turn]
    @idress_wearing_table.note = params[:idress_wearing_table][:note]
    @idress_wearing_table.is_current = params[:idress_wearing_table][:is_current] == '1'
    wearings = parse_wearings
    @idress_wearing_table.idress_wearings = wearings
  end

  def originator_check(wearing)
    @notice_hash ||= {}
    @idef_hash ||= {}
    wearing[:number].strip =~ /\d{2}-(.{5}\-.{2})/

    jobs = wearing[:job_idress].gsub('+','＋').split('＋')
    jobs <<  wearing[:personal_idress] unless wearing[:personal_idress].blank?
    for name in jobs
      next if @notice_hash.has_key? name
      idef = @idef_hash[name] ||= IDefinition.find(:first , :conditions => ["name = ? and revision is NULL" , name])
      if idef.nil?
        @notice_hash[name]="文殊に職業アイドレス定義'#{name}'の登録がありません。（管理人にご連絡下さい）"
        next
      end
      if idef.compiled_hash.nil? || idef.compiled_hash['エラー']
        @notice_hash[name]="文殊の職業アイドレス定義'#{name}'の登録がなんだかおかしいです。（管理人にご連絡下さい……）"
        next
      end
      originator_limit = idef.compiled_hash['特殊定義'].find{|sp|
        sp['特殊種別']=='使用制限' &&  sp['使用制限'] =~ /根源力/
      }
      next unless originator_limit

      originator_limit_value = parse_num(originator_limit['使用制限'])
      next unless originator_limit_value

      wearing[:number].strip =~ /\d{2}-(.{5}\-.{2})/
      character_no = $1
      character = Character.find(:first , :conditions => ['character_no like ?' , '%'+character_no])
      return nil if character.nil?
      
      orig = character.sum_originator
      orig += 20000 if ['吏族','法官','護民官','星見司','参謀','法の司','星見司２'].any?{|item| item == wearing[:personal_idress]}
      if orig < originator_limit_value
        return {'根源力'=> orig , '定義' => originator_limit['定義']}
      end
    end
    return nil
  end
end
