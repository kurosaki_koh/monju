# -*- encoding: utf-8 -*-
class BondsController < ApplicationController
  requires_role :item_officer , 
    :only => [:create , :destroy , :edit , :update 
    ]
  set_login_filter

# GET /bonds
  # GET /bonds.xml
  def index
    @bonds = Bond.find_all_by_character_id(params[:character_id])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @bonds }
    end
  end

  # GET /bonds/1
  # GET /bonds/1.xml
  def show
    @bond = Bond.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @bond }
    end
  end

  # GET /bonds/new
  # GET /bonds/new.xml
  def new
    @bond = Bond.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @bond }
    end
  end

  # GET /bonds/1/edit
  def edit
    @bond = Bond.find(params[:id])
  end

  # POST /bonds
  # POST /bonds.xml
  def create
    @bond = Bond.new(params[:bond])
    @bond[:character_id] = params[:character_id]
    respond_to do |format|
      if @bond.save
        flash[:notice] = 'ACE関係情報を登録しました。'
        format.html { redirect_to(character_bonds_path(@bond.character)) }
        format.xml  { render :xml => @bond, :status => :created, :location => @bond }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @bond.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /bonds/1
  # PUT /bonds/1.xml
  def update
    @bond = Bond.find(params[:id])

    respond_to do |format|
      if @bond.update_attributes(params[:bond])
        flash[:notice] = 'ACE関係情報を更新しました。.'
        format.html { redirect_to(character_bonds_path(@bond.character)) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @bond.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /bonds/1
  # DELETE /bonds/1.xml
  def destroy
    @bond = Bond.find(params[:id])
    character = @bond.character
    @bond.destroy

    respond_to do |format|
      format.html { redirect_to(character_bonds_path(character)) }
      format.xml  { head :ok }
    end
  end
end
