# -*- encoding: utf-8 -*-

#$KCODE='u'
require 'zlib'
require 'stringio'


class TaigenController < ApplicationController
  include TaigenUtility

  MAJOR_VERSION = 1
  MINOR_VERSION = 4
  REVISION = 0
  VERSION = "#{MAJOR_VERSION}.#{MINOR_VERSION}.#{REVISION}"


  before_filter :preprocess_source , :only => [:update , :compile]

  class ILanguage::Calculator::Unit
    
    def registered_character
      unless @character
#        number = /(\d{2}-\d{5}-\d{2})/.match(self.name).to_a[0]
        number = self.character_no
        return nil unless number
        @character = Character.find_by_character_no(number)
      end
      @character
    end
    def is_registered_character?
      !self.registered_character.nil?
    end
  end

  after_filter :compress

  def compress
    self.response.headers['Content-Encoding'] = 'gzip'
  end

  def compile
    context = Context.new(@rev)
    begin
      @troop = context.parse_troop(@source)
    rescue => e
      error = e.to_s
      if Rails.env == 'development'
        error += $@.join("\n")
      end
      render :json => {'error' => error}  , :status => :bad_request
      return
    end

    respond_to do |format|
      format.js { render :json => context.hash_for_api , :callback => params[:callback] }
      format.text { render :text => @troop.result_str}
    end

#    render :json => @troop.hash_for_diff , :callback => params[:callback]
  end

  def update
    @hints = []
    context = Context.new(@rev)

    begin
      @troop = context.parse_troop(@source)
      if context.definitions.not_founds.size > 0
        @hints << "次のアイドレス名が見つかりません：#{ERB::Util.h(context.definitions.not_founds.join(' , '))}"
      end
      @hints += context.warnings
      @source_digest = Digest::MD5.hexdigest(@source)
      @result_digest = Digest::MD5.hexdigest(@troop.all_result_str)

    rescue => e
      @exceptions = [e.to_s]
      @exceptions << $@.join("\n") if Rails.env == 'development'
    end

#    raise params[:source].encoding.inspect
    if params[:download]
      output = render_to_string(:action => :edit)
      output.gsub!(/script src="\/javascripts/,"script src=\"\http://maki.wanwan-empire.net/javascripts")
      output.gsub!(/href="\/stylesheets/,"href=\"\http://maki.wanwan-empire.net/stylesheets")
      now = Time.now
      send_data(output,:filename => "taigen_#{now.strftime('%y%m%d_%H%M%S')}.html")
    else
      render :action => :edit
    end
  end

  def edit
    @hints = []
    @rev = params[:id]
  end

  private

  def preprocess_source
    @source = trim_source(params[:source])
    @rev = params[:revision] || params[:id]
    if @source.blank?
      redirect_to :action => :edit , :id => @rev
      return
    end
  end

  def make_json_response(troop)
    result = {'divisions' => [] , 'not_found' => [] , 'warnings' => []}
  end

  class Troop
    def make_json_response
      result = {'name' => self.name , 'evaluations' => [] , 'members' => []}

    end
  end


end
