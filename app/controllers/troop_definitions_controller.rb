# -*- encoding: utf-8 -*-
class TroopDefinitionsController < ApplicationController
  requires_role :monju_maintainer , :only => [:create ,:update  , :destroy ]
  set_login_filter

  # GET /troop_definitions
  # GET /troop_definitions.xml
  def index
    turn = params[:turn]
    @troop_definitions = TroopDefinition.find(:all,:order => 'nation_id , name', :conditions => turn && ['turn = ?' , turn])
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @troop_definitions }
    end
  end

  # GET /troop_definitions/1
  # GET /troop_definitions/1.xml
  def show
    @troop_definition = TroopDefinition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @troop_definition }
    end
  end

  # GET /troop_definitions/new
  # GET /troop_definitions/new.xml
  def new
    @troop_definition = TroopDefinition.new
    @troop_definition.turn = Property[:current_turn]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @troop_definition }
    end
  end

  # GET /troop_definitions/1/edit
  def edit
    @troop_definition = TroopDefinition.find(params[:id])
  end

  # POST /troop_definitions
  # POST /troop_definitions.xml
  def create
    @troop_definition = TroopDefinition.new(params[:troop_definition])

    respond_to do |format|
      if @troop_definition.save
        flash[:notice] = '部隊表の登録を行いました。'
        format.html { redirect_to(@troop_definition) }
        format.xml  { render :xml => @troop_definition, :status => :created, :location => @troop_definition }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @troop_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  ValueCheckTemplate1 = <<EOT
EOT

  def value_check_list
    @check_list = <<EOT
| 1. 編成表に記載された分隊が、太元書式に全て含まれている。 | 【Y/N】|
| 2. 本隊・分隊・状況それぞれにおいて、編成表の記載と太元書式に齟齬がない| --- |
EOT
    check_page{|troop|
      for div in troop.all_divisions
        name = div.name == '本隊' ? '【本隊】' : div.name
        @check_list << "| #{name}・通常 | 【一致/サブセット/独自記載あり/齟齬あり】|\n"
        for situation in div.all_situation_names[1..-1]
          @check_list << "| #{name}・#{situation} | 【一致/サブセット/独自記載あり/齟齬あり】|\n"
        end
      end

    }
    @check_list << "| 3. 編成表の部隊コストと太元出力を照合する。 | 【一致/不一致かつ妥当/齟齬あり】|\n"
  end
  
  def check_page
    @troop_definition = TroopDefinition.find(params[:id])
    @v_results = {}
    error = !build_troop

    unless error
      begin
        yield @troop if block_given?
      rescue => e
        flash[:notice] = '編成チェック処理中にエラーが発生しました。バグかもしれません。黒埼に連絡して下さい。' + e.to_s
        error = true
      end
    end

    if error
      respond_to do |format|
        format.html { redirect_to(@troop_definition) }
        format.xml  { head :ok }
      end
    end
  end

  private :check_page

  #get /troop_definitions/[id]/validation
  def validation
    check_page{|troop|
        @t_results = check(troop ,ILanguage::Calculator::TaigenValidations )
        @r_results = check(troop ,ILanguage::Calculator::RuleValidations )
    }
  end
  
  def diff_form
    @troop_definition = TroopDefinition.find(params[:id])
  end
  
  def diff_update
    @troop_definition = TroopDefinition.find(params[:id])
    @source = params[:source]
    @source.gsub!("\r\n","\n")
    @source.gsub!(/ +\n/,"\n")
    @source.strip!

    @errors = []
    context = Context.new(@troop_definition.turn.to_i)

    begin
      @troop1 = @troop_definition.parse_troop 
      @troop2 = context.parse_troop(@source)
      
      (@troop2 , @troop1 = @troop1 , @troop2) if params[:modify] == 'before'
    rescue => e
      @errors << e.to_s
    end
    
    
    
    render :action => :diff_form
  end

  def build_troop
    begin
      @troop = @troop_definition.parse_troop
    rescue => e
      flash[:notice] = '編成データの解析中にエラーが発生しました。編成データの内容を確認して下さい。' + e.to_s
      return false
    end
  end

  def parse
    @directives = {}
    check_page{|troop|
      for directive in troop.node.directives
        @directives[directive.class.node_class.intern] = true
      end
    }
  end

  # PUT /troop_definitions/1
  # PUT /troop_definitions/1.xml
  def update
    @troop_definition = TroopDefinition.find(params[:id])

    respond_to do |format|
      if @troop_definition.update_attributes(params[:troop_definition])
        flash[:notice] = '編成表登録は正常に更新されました。'
        format.html { redirect_to(@troop_definition) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @troop_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /troop_definitions/1
  # DELETE /troop_definitions/1.xml
  def destroy
    @troop_definition = TroopDefinition.find(params[:id])
    @troop_definition.destroy

    respond_to do |format|
      format.html { redirect_to(troop_definitions_url) }
      format.xml  { head :ok }
    end
  end
  
  private
  def check(troop , validations)
    result = {}
    for validator in validations.sorted_list
      validation = {}
      validation[:validator] = validator
      validation[:result] = validator.do_check(troop)
      result[validator.no] = validation
    end
    result
  end
end
