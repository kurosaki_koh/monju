#encoding : utf-8

class CommandResourcesController < ApplicationController
  requires_role :monju_maintainer , :only => [:create , :update  , :destroy ]
  set_login_filter

  # GET /command_resources
  # GET /command_resources.json
  def index

    #引数からOwnerAccountを特定
    number = params[:character_no] || params[:nation_id] || params[:number]
    @owner_account = if params[:owner_account_id]
                       OwnerAccount.find(params[:owner_account_id])
                     elsif number
                       OwnerAccount.find_by_number(number)
                     end
    if @owner_account.nil?
      message = "番号:#{ params[:owner_account_id] || number}に基づくキャラクターが存在しません。"
      if params[:format] == 'js'
        render text:message , status:404
        return
      end
      redirect_to(top_nations_path , notice: message )
      return
    end

    #OwnerAccount共通のコマンドリソース
    @command_resources = @owner_account.available_command_resources

    #コンバート登録のコマンドリソース
    @converted_resources = @owner_account.converted_resources(params[:unit_name])

    if @converted_resources.nil?
      message = '指定されたユニット定義は存在しないか、もしくは無効化されています。'
      if params[:format]  == 'js'
        render text: message , status: 404
      else
        redirect_to owner_account_command_resources_path(@owner_account ), notice: message
      end
      return
    end

    #藩国・PCから波及するコマンドリソース
    @inherited_resources = @owner_account.extended_command_resources

    respond_to do |format|
      format.html # index.html.erb
      format.js {
        render :json => (@command_resources + @converted_resources + @inherited_resources),
                         methods: [:name , :power , :tag , :id ,:source_id , :source_type],
                         only: [:from , :from_id] ,
                         callback: params[:callback]
      }

    end
  end

  # GET /command_resources/1
  # GET /command_resources/1.json
  def show
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js { render json: @command_resource , methods: [:name , :power , :tag]}
    end
  end

  # GET /command_resources/new
  # GET /command_resources/new.json
  def new
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.new
    @command_resource.owner_account = @owner_account

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @command_resource }
    end
  end

  # GET /command_resources/1/edit
  def edit
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.find(params[:id])
  end

  # POST /command_resources
  # POST /command_resources.json
  def create
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.new(params[:command_resource])
    @command_resource.owner_account = @owner_account
    respond_to do |format|
      if @command_resource.save
        format.html { redirect_to owner_account_command_resource_path(@owner_account , @command_resource), notice: '正常に登録されました。' }
        format.json { render json: @command_resource, status: :created, location: @command_resource }
      else
        format.html { render action: "new" }
        format.json { render json: @command_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /command_resources/1
  # PUT /command_resources/1.json
  def update
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.find(params[:id])

    respond_to do |format|
      if @command_resource.update_attributes(params[:command_resource])
        format.html { redirect_to owner_account_command_resource_path(@owner_account , @command_resource), notice: '正常に更新されました。' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @command_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /command_resources/1
  # DELETE /command_resources/1.json
  def destroy
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @command_resource = CommandResource.find(params[:id])
    @command_resource.destroy

    respond_to do |format|
      format.html { redirect_to owner_account_command_resources_url(@owner_account) }
      format.json { head :no_content }
    end
  end
end
