# -*- encoding: utf-8 -*-
require 'kconv'
require 'hpricot'

class OwnerAccountsController < ApplicationController
  cache_sweeper :modify_sweeper, :only => [:process_edit]
  layout 'owner_accounts', :except => [:update_owner_account_list]

  requires_role :accountant, :only => [:facade, :create, :update, :destroy, :process_edit]
  set_login_filter
  # GET /owner_accounts
  # GET /owner_accounts.xml
  def index
    @owner_accounts = if params[:nation_id]
                        @nation = Nation.find(params[:nation_id])
                        [@nation.owner_account]
                      elsif params[:q]
                        name = OwnerAccount.arel_table[:name]
                        number = OwnerAccount.arel_table[:number]
                        keyword = '%'+params[:q]+'%'
                        OwnerAccount.where(number.matches(keyword).or(name.matches(keyword))).map{|i|{label:"#{i.number}：#{i.name}" , value:i.number}}
                      else
                        [] #OwnerAccount.find(:all)
                      end
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @owner_accounts }
      format.js { render :json => @owner_accounts, :callback => params[:callback] }
    end
  end

  # GET /owner_accounts/1
  # GET /owner_accounts/1.xml
  def show
    begin
      if params[:character_no]
        character = if params[:character_no] =~ /^\d{2}-\d{5}-\d{2}$/
                      Character.find_by_character_no(params[:character_no])
                    elsif params[:character_no] =~ /\d{5}-\d{2}$/
                      Character.find(:first, :conditions => ['character_no like ?', '__-'+params[:character_no]])
                    else
                      raise ActiveResource::BadRequest.new('Malformed character_no', 'Malformed character_no')
                    end
        raise ActiveRecord::RecordNotFound if character.nil?
        @owner_account = character.owner_account
      elsif params[:nation_id]
        nation = Nation.find(params[:nation_id])
        @owner_account = nation.owner_account if nation
      else
        @owner_account = OwnerAccount.find(params[:id])
      end
    rescue ActiveRecord::RecordNotFound
      head :not_found
      return
    rescue ActiveResource::BadRequest
      render :text => $!.response, :status => :bad_request
      return
    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @owner_account.to_xml(:include => :mile_changes) }
    end
  end

  # GET /owner_accounts/new
  # GET /owner_accounts/new.xml
  def new
    @owner_account = OwnerAccount.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml { render :xml => @owner_account }
    end
  end

  # GET /owner_accounts/1/edit
  def edit
    @owner_account = OwnerAccount.find(params[:id])
  end

  # POST /owner_accounts
  # POST /owner_accounts.xml
  def create
    @owner_account = OwnerAccount.new(params[:owner_account])
    respond_to do |format|
      if @owner_account.save
        flash[:notice] = 'OwnerAccount was successfully created.'
        format.html { redirect_to(@owner_account) }
        format.xml { render :xml => @owner_account, :status => :created, :location => @owner_account }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @owner_account.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /owner_accounts/1
  # PUT /owner_accounts/1.xml
  def update
    @owner_account = OwnerAccount.find(params[:id])

    respond_to do |format|
      if @owner_account.update_attributes(params[:owner_account])
        flash[:notice] = 'OwnerAccount was successfully updated.'
        format.html { redirect_to(@owner_account) }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @owner_account.errors, :status => :unprocessable_entity }
      end
    end

    rebder :action => :edit
  end

  # DELETE /owner_accounts/1
  # DELETE /owner_accounts/1.xml
  def destroy
    @owner_account = OwnerAccount.find(params[:id])
    @owner_account.destroy

    respond_to do |format|
      format.html { redirect_to(owner_accounts_url) }
      format.xml { head :ok }
    end
  end

  def update_form_parts
    @selected = OwnerAccount.find(params[:target_id])
    cookies[:owner_account_id]={:value => @selected.id.to_s, :expires => 2.weeks.from_now}
    @flag = @selected.owner_type == 'Character'
  end

  def update_owner_account_list
    cookies[:org_select_id]={:value => params[:owner_account_id], :expires => 2.weeks.from_now}
    render
  end

  def process_edit
    @owner_account = OwnerAccount.find(params[:id])
    weapons = params[:weapons]
    case params[:edit_params][:target]
      when 'weapons'
        if !(Property['do_post_to_yell'].to_s.empty?)
          result = post_to_yell_weapons_db
          if result
            flash[:notice] = "Yell!からのエラーメッセージ：#{result}"
            respond_to do |format|
              format.html { redirect_to(facade_owner_accounts_path) }
            end
            return
          end
        end
        begin
          case weapons[:app_type]
            when 'move'
              @owner_account.move_weapons(weapons)
            when 'alter'
              @owner_account.alter_weapons(weapons)
          end
        rescue
          flash[:notice] = $!.to_s
          respond_to do |format|
            format.html { redirect_to(facade_owner_accounts_path) }
          end
          return
        end
      when 'caches'
        @cash_record = @owner_account.cash_records.create(params[:caches])
        if @cash_record
          flash[:notice] = '財務表の項目を追加しました。'
        else
          flash[:notice] = '財務表の項目追加に失敗しました。管理者に連絡してください。多分バグ。'
        end
      when 'miles'
        @mile = @owner_account.mile_changes.create(params[:miles])
        @mile.turn = Property[:current_turn].to_i
        if @mile.save
          flash[:notice] = 'マイル口座履歴を追加しました。'
        else
          flash[:notice] = 'マイル口座履歴の追加に失敗しました。<br/>' + @mile.errors.full_messages.join('<br/>')
        end
    end
    respond_to do |format|
      format.html { redirect_to(facade_owner_accounts_path) }
    end
  end

  WeaponPostHost = 'idress-yell.sakura.ne.jp'
  WeaponPostPath = '/weapon/weapontouroku.php'

  def yell_weapon_list
    Specification.all(:order => 'position').map { |spec| {value: spec.id, label: spec.name} }
  end

  helper_method :yell_weapon_list

  def post_to_yell(query)
    result = ''
    Net::HTTP.start(WeaponPostHost) { |http|
      http.post(WeaponPostPath, query.keys.map { |key| "#{key}=#{query[key]}" }.join('&').tosjis, nil, result)
    }
    return result
  end

  def clip_error_message(str)
    docs = Hpricot(str.toutf8)
    (docs/:body).inner_text
  end

  def post_to_yell_weapons_db
    weapons = params[:weapons]
    query = {}
    query[:turn]=Property['current_turn'].to_s
    query[:check]='1'
    now = Time.now
    query[:year]=now.year
    query[:month]=now.month
    query[:day]=now.day
    query[:reason]=weapons[:note]
    query[:url]=weapons[:reference_url]
    query[:pass]='testpassword'
    query[:submit]='登録'
    spec = Specification.find(weapons[:weapon_id])
    query[:weapon]=spec.type_no

    case weapons[:app_type]
      when 'move'
        query[:country]=@owner_account.owner.id
        query[:number]=weapons[:number].to_i * -1
        result = post_to_yell(query)

        return clip_error_message(result) if result =~ /caution.jpg/

        query[:country] = OwnerAccount.find(weapons[:move_dest]).owner.id
        query[:number]=weapons[:number]
        result = post_to_yell(query)
        return clip_error_message(result) if result =~ /caution.jpg/
      when 'alter'
        query[:country]=@owner_account.owner.id
        query[:number]=weapons[:number]
        result = post_to_yell(query)
        return clip_error_message(result) if result =~ /caution.jpg/
    end
    return nil
    #    end
    #    query.keys.map{|key|"#{key}=#{query[key]}"}.join('&')
  end

  def confirm
    @owner_account = OwnerAccount.find_all_by_id(params[:owner_account][:id].to_i)[0]
    @results = {}
    case params[:edit_params][:target]
      when 'weapons'
        @results[:source_number]= @owner_account.weapon_registries.by_spec(params[:weapons][:weapon_id]).inject(0) { |sum, regs|
          sum += regs.number.to_i
          sum
        }
        @results[:edited_source_number] = @results[:source_number] + params[:weapons][:number].to_i
        if params[:weapons][:app_type] == 'move'
          dest_account = OwnerAccount.find(params[:weapons][:move_dest])
          @results[:dest_number]= dest_account.weapon_registries.by_spec(params[:weapons][:weapon_id]).inject(0) { |sum, regs|
            sum += regs.number.to_i
            sum
          }
          @results[:edited_source_number] = @results[:source_number] - params[:weapons][:number].to_i.abs
          @results[:edited_dest_number] = @results[:dest_number] + params[:weapons][:number].to_i.abs
        end

      when 'caches'
        @results[:cash_record] = {}
        @results[:edited_cash_record] = {}
        cash_records = @owner_account.cash_records.to_a
        for column in [:fund, :food, :resource, :amusement, :fuel, :hqfuel, :num_npc]
          @results[:cash_record][column] = cash_records.sum { |record| record[column].to_i }
          @results[:edited_cash_record][column]=@results[:cash_record][column] + params[:caches][column].to_i
        end
      when 'miles'
        @results[:edited_miles]=@owner_account.miles + params[:miles][:mile].to_i
    end
  end
=begin
process_edit
  :edit_type (マイル・財務・兵器）
  :edit_params (コマンドへのパラメータ）


兵器編集の場合
  :owner_account_id 対象の口座
　:app_type {'alter' || 'move' } 変動 or 移送
  :weapons_name 対象の兵器名
  :weapons_name 兵器の変動数
  :weapons_move_dest 兵器の移送先（optional)

=end
end
