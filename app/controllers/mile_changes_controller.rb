# -*- encoding: utf-8 -*-
class MileChangesController < ApplicationController
  cache_sweeper :modify_sweeper , :only => [:create , :destroy , :update]
  requires_role :accountant , :only => [:create , :update  , :destroy ]
  set_login_filter

  # GET /mile_changes
  # GET /mile_changes.xml
  def index
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @limit = case
    when  params[:limit]== 'all'
      nil
    when params[:limit].nil?
      100
    else
      params[:limit].to_i
    end
    if params[:turn]
      @target_turn = params[:turn]
      @count = nil
      if @target_turn == 'last'
        turn = Property['current_turn'].to_i + 1
        if MileChange.count(:conditions => ["owner_account_id = ?" , params[:owner_account_id] ]) == 0
          turn = Property['current_turn'].to_i
        else
          while @count.nil? || @count == 0
            turn -= 1
            @count = MileChange.count(:conditions => ["owner_account_id = ? and turn = ?" , params[:owner_account_id] , turn])
          end
        end
        @target_turn = turn
      else
        @count = MileChange.count(:conditions => ["owner_account_id = ? and turn = ?" , params[:owner_account_id] , @target_turn] )
      end

      opts = if @count && @limit && @count > @limit
        @offset = params[:offset] || (@count - @limit)
        @offset = @offset.to_i
        {:limit => (@offset.to_i + @limit.to_i)}
      else
        @offset = nil
        {}
      end
      result = MileChange.find_all_by_owner_account_id_and_turn(params[:owner_account_id] , @target_turn ,opts.merge(:order => :id) )
      @mile_changes = if @offset
        result[@offset , @limit]
      else
        result
      end


      @zandaka = MileChange.find_by_sql(['select sum(mile) as miles  from mile_changes where owner_account_id = ? and turn < ? ' ,
        params[:owner_account_id] ,@target_turn ])[0]

      if @offset && @zandaka
        @zandaka2 =  result[0...@offset].inject(0){|result,item| result += item.mile}

        @zandaka.miles = @zandaka.miles.to_i + @zandaka2
      end
    elsif params[:date]
      y , m , d = params[:date].split('-').map{|i|i.to_i}
      day = Date.new(y, m , d)
      @mile_changes = MileChange.find_all_by_owner_account_id(params[:owner_account_id] , :conditions => ['date(created_at) = ?' , day])
      @zandaka = MileChange.find_by_sql(['select sum(mile) as miles from mile_changes where owner_account_id = ? and created_at < ?' ,
        params[:owner_account_id] , day])[0]
    else
      @mile_changes = @owner_account.mile_changes
    end
    @mile_changes = @mile_changes.sort_by(&:created_at)
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mile_changes }
    end
  end

  # GET /mile_changes/1
  # GET /mile_changes/1.xml
  def show
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @mile_change = MileChange.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mile_change }
    end
  end

  # GET /mile_changes/new
  # GET /mile_changes/new.xml
  def new
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @mile_change = MileChange.new
    @mile_change.turn = Property[:current_turn].to_i
    @mile_change.owner_account = @owner_account
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mile_change }
    end
  end

  # GET /mile_changes/1/edit
  def edit
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @mile_change = MileChange.find(params[:id])
  end

  # POST /mile_changes
  # POST /mile_changes.xml
  def create
    @owner_account = OwnerAccount.find(params[:mile_change][:owner_account_id])
    @mile_change = MileChange.new(params[:mile_change])
    respond_to do |format|
      if @mile_change.save
        flash[:notice] = 'マイル口座に項目を追加しました。'
        format.html {
          if @mile_change.turn.to_i > 0
            redirect_to(owner_account_mile_changes_path(@owner_account,:turn => @mile_change.turn))
          else
            redirect_to(owner_account_mile_changes_path(@owner_account))
          end
        }
        format.xml  { render :xml => @mile_change, :status => :created, :location => @mile_change }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @mile_change.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mile_changes/1
  # PUT /mile_changes/1.xml
  def update
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @mile_change = MileChange.find(params[:id])

    respond_to do |format|
      if @mile_change.update_attributes(params[:mile_change])
        flash[:notice] = 'マイル口座の項目内容を変更しました。'
        format.html {
          if @mile_change.turn.to_i > 0
            redirect_to(owner_account_mile_changes_path(@owner_account,:turn => @mile_change.turn))
          else
            redirect_to(owner_account_mile_changes_path(@owner_account))
          end
        }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mile_change.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mile_changes/1
  # DELETE /mile_changes/1.xml
  def destroy
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @mile_change = MileChange.find(params[:id])
    @mile_change.destroy

    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end

end
