# -*- encoding: utf-8 -*-
class IDefinitionsController < ApplicationController

  requires_role :admin , :only => [:create , :update  , :destroy ,:new , :edit]
  set_login_filter


  # GET /i_definitions
  # GET /i_definitions.xml
  def index
    if request.format.to_s == 'text/html'
      @i_definitions = IDefinition.paginate(:page => params[:page], :order => 'id' , :per_page => 20)
    else
      @i_definitions = IDefinition.find(:all).sort_by{|s|s.id}
    end

    respond_to do |format|
      format.html # index.html.erb

      index = @i_definitions.collect{|idef| {:name => idef.name,:object_type => idef.object_type}}
      format.xml  {  
        render :xml => index.to_xml(:root => 'i_definitions')
      }
      format.js {
        builder = JsonBuilder.new
        render :json => builder.build(index) , :callback => params[:callback] }
    end
  end

  # GET /i_definitions/1
  # GET /i_definitions/1.xml
  def show
    if params[:name]
      @i_definition = IDefinition.find_by_name(params[:name])
    else
      @i_definition = IDefinition.find(params[:id])
    end

    respond_to do |format|
      format.html # show.html.erb
      if @i_definition.nil?
        head :not_found
        return
      end
      format.xml  { render :xml => @i_definition }
      format.js {
        builder = JsonBuilder.new
        render :json => builder.build(@i_definition.compiled_hash) , :callback => params[:callback] }
    end
  end

  # GET /i_definitions/new
  # GET /i_definitions/new.xml
  def new
    @i_definition = IDefinition.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @i_definition }
    end
  end

  # GET /i_definitions/1/edit
  def edit
    @i_definition = IDefinition.find(params[:id])
  end

  # POST /i_definitions
  # POST /i_definitions.xml
  def create
    @i_definition = IDefinition.new(params[:i_definition])

    respond_to do |format|
      if @i_definition.save
        flash[:notice] = 'i言語定義を登録しました。'
        format.html { redirect_to(@i_definition) }
        format.xml  { render :xml => @i_definition, :status => :created, :location => @i_definition }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @i_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /i_definitions/1
  # PUT /i_definitions/1.xml
  def update
    @i_definition= IDefinition.find(params[:id])
    @i_definition.definition = params[:i_definition][:definition].strip.gsub("\r\n","\n")
#    raise @i_definition.definition_changed?.to_s
    respond_to do |format|
      if @i_definition.save
        flash[:notice] = '正常に変更されました。'
        format.html { redirect_to(@i_definition) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @i_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /i_definitions/1
  # DELETE /i_definitions/1.xml
  def destroy
    @i_definition = IDefinition.find(params[:id])
    @i_definition.destroy

    respond_to do |format|
      format.html { redirect_to(i_definitions_url) }
      format.xml  { head :ok }
    end
  end
end
