class UtenController < ApplicationController
  before_filter :login_required , :only => [:show , :new , :edit , :create , :update , :destroy]

  layout 'owner_accounts'
  def index
    list
    render :action => 'list'
  end

  def list

    recs = LatestCashRecord.order(:id).joins(:nation)
    @republic_records = recs.select{|r|r.nation.belong == :republic}
    @empire_records = recs.select{|r|r.nation.belong == :empire}
#    raise @republic_recoreds.inspect
  end

  def show
    @latest_cash_record = LatestCashRecord.find(params[:id])
  end

  def new
    @latest_cash_record = LatestCashRecord.new
  end

  def create
    @latest_cash_record = LatestCashRecord.new(params[:latest_cash_record])
    if @latest_cash_record.save
      flash[:notice] = 'LatestCashRecord was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @latest_cash_record = LatestCashRecord.find(params[:id])
  end

  def update
    @latest_cash_record = LatestCashRecord.find(params[:id])
    if @latest_cash_record.update_attributes(params[:latest_cash_record])
      flash[:notice] = 'LatestCashRecord was successfully updated.'
      redirect_to :action => 'show', :id => @latest_cash_record
    else
      render :action => 'edit'
    end
  end

  def destroy
    LatestCashRecord.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
