# -*- encoding: utf-8 -*-

module TaigenUtility
  require_dependency 'i_language/calculator'

  include ILanguage::EVCC
  include ILanguage::Calculator
  def trim_source(source)

    return if source.blank?


    r = request.body().read
    source.encode('utf-8') if source

    source = source.gsub("\r\n", "\n")
    source.gsub!(/^"/, "")
    source.gsub!(/"$/, "")
    source.gsub!(/\n−/, "\n－")
    source.gsub!(/ +\n/, "\n")
    source.strip!
    source
  end
end