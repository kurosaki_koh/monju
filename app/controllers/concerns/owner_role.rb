module OwnerRole

  def do_authorize(user , controller)
    owner_account = OwnerAccount.find(controller.params[:owner_account_id])
    return true if user.owner_accounts.find{ |oc| oc == owner_account} #本人
    target_nation = owner_account.number[0..1].to_i
    return true if user.characters.to_a.find{|c|c.is_noble? && c.nation_id == target_nation}
    return user.has_role?(:admin)
  end

  module_function :do_authorize

end