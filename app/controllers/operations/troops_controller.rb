# -*- encoding: utf-8 -*-
class Operations::TroopsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  def list
    @troop_pages, @troops = paginate :operation_nodes, :per_page => 10 , :conditions => "type = 'Troop'"
  end

  def register
    @nations = Nation.find(:all,:order => :id).collect{|n| [n.name,n.id ] }
    @nation= @nations[0] if @nations
    @operation_id = params[:id]
    @soldiers = SoldierNode.find(:all,:conditions => ["operation_id = ? and nation_id = ?",@operation_id,@nation.id])
  end

  def show
    @troop = TroopNode.find(params[:id])
  end

  def new
    @troop = TroopNode.new(:operation_id => params[:id])
  end

  def create
    @troop = TroopNode.new(params[:troop])
    if @troop.save
      flash[:notice] = '部隊を追加しました。'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @troop = OperationNode.find(params[:id])
  end

  def update
    @troop = OperationNode.find(params[:id])
    if @troop.update_attributes(params[:troop])
      flash[:notice] = 'Troop was successfully updated.'
      redirect_to :action => 'show', :id => @troop
    else
      render :action => 'edit'
    end
  end

  def destroy
    TroopNode.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
  
  def update_players_table
    render :update do |page|
      page.replace_html  "players_table", :partial=>'players_table'
    end
  end
end
