# -*- encoding: utf-8 -*-
class JobIdressesController < ApplicationController
  cache_sweeper :modify_sweeper ,
     :only => [:create , :destroy , :update ]

  
  requires_role :herald , :only => [:create , :update  , :destroy ]
  requires_role :monju_maintainer , :only => [:create , :update  , :destroy ]

  set_login_filter
  
  def index
    list
    render :action => 'list'
  end
  
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  #verify :method => :post, :only => [ :create  ],
  #  :redirect_to => { :action => :list }


  def select_expired(job_idresses)
    job_idresses.select{|ji| !ji.regular && ji.idress_type == 'JobIdress'}
  end
  private :select_expired
  
  def list
    @nation = nation()
    if params[:nation_id] 
      @job_idresses = JobIdress.find(:all , :conditions => ['nation_id = ?',params[:nation_id] ] , :order => 'nation_id , id')
    elsif params[:owner_account_id]
      @owner_account = OwnerAccount.find(params[:owner_account_id],:include => :job_idresses)
      @job_idresses = @owner_account.job_idresses.to_a
      if @owner_account.owner.class == Nation && !@job_idresses.nil?
        @expired_idresses ,@job_idresses = @job_idresses.partition{|ji|!ji.regular && ji.idress_type == 'JobIdress'}
        @expired_idresses = @expired_idresses.sort_by{|j|j.inspect_idresses}
      end
    else
      @job_idresses = JobIdress.find(:all, :order => 'nation_id , id') #).sort_by{|j|j.id}
    end
    @job_idresses = JobIdress::sort_by_race(@job_idresses , @nation && @nation.belong )
  end
  
  def show
    @job_idress = JobIdress.find(params[:id])
    @owner_account = @job_idress.owner_account
  end
  
  def new
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @job_idress = JobIdress.new
    @job_idress.food_cost = 1
    @job_idress.idress_type = 'JobIdress'
    @job_idress.owner_account_id = @owner_account
    @job_idress.hq = {}
  end
  
  def update_abilities
    for col in [:phy , :dur , :str , :chr , :agi , :dex , :sen , :edu , :luc]
      val = @job_idress.race[col] + @job_idress.job1[col] + @job_idress.job2[col]
      val += @job_idress.job3[col] if @job_idress.job3
      @job_idress.hq = {} if @job_idress.hq.nil?
      #       p [ji.hq , col]
      val += @job_idress.hq[col] if @job_idress.hq[col]
      @job_idress[col]= val
    end
  end
  private :update_abilities

  def create
    job_idress = JobIdress.new
    update_columns(job_idress)
    if job_idress.nation == nil
      if job_idress.owner_account && job_idress.owner_account.owner_type == 'Nation'
        job_idress.nation = job_idress.owner_account.owner
      end
    end
    if job_idress.save #&& @skill.save
      flash[:notice] = '職業アイドレスを作成しました。'
      if params[:owner_account_id]
        redirect_to owner_account_job_idresses_path(params[:owner_account_id])
      else
        redirect_to :action => 'list'
      end
    else
      render :action => 'new'
    end
  end
  
  def edit
    @job_idress = JobIdress.find(params[:id])
    @owner_account = @job_idress.owner_account
  end
  
  def update
    @job_idress = JobIdress.find(params[:id])
    update_columns(@job_idress)
    if @job_idress.save # && @skill.update_attributes(params[:skill])
      flash[:notice] = '職業アイドレスの構成を変更しました。.'
      redirect_to owner_account_job_idresses_path(@job_idress.owner_account)
    else
      render :action => 'edit'
    end
  end
  
  def update_nation
    render :update do |page|
    end
  end
  
  def destroy
    ji = JobIdress.find(params[:id])
    owner = ji.owner_account
    ji.destroy
    redirect_to owner_account_job_idresses_path(owner)
  end
  
#  def authorized?(nid = nil)
#    is_admin? || current_user.has_role?(:herald,:monju_maintainer)
#  end
  
  def make_hq_hash(hq_param,job_idress)
    job_idress.hq = {}
    hq_param.keys.each{|key| job_idress.hq[key.to_sym]=hq_param[key].to_i if hq_param[key].to_i > 0}
  end
  
  def JobIdressesController::format_hq(ji)
    [:hq0 , :hq1 , :hq2 , :hq3,:etc].inject([]){|result , lv|
      if ji.bonuses[lv][:quality] != '0'
        if ji.bonuses[lv][:hq_target] == ''
          result << ji.bonuses[lv][:option].to_s+ '+' + ji.bonuses[lv][:quality].to_s
        else
          result << 
             (Job::AbilityKanji[ji.bonuses[lv][:hq_target].to_sym] + '+' + ji.bonuses[lv][:quality])
        end
      end
      result
    }.join(',')
  end
  
  private
  def update_columns(job_idress)
    job_idress.owner_account_id = params[:owner_account_id]
    [:note , :idress_type , :bonuses , :regular_no , :race_type ,
        :job1_id , :job2_id , :job3_id , :job4_id , :race_id ].each{|col|
      job_idress[col] = params[:job_idress][col]
    }
    ['hq0','hq1','hq2','hq3','etc'].each{|lv|
      if job_idress.bonuses[lv][:quality] == '0' ||  job_idress.bonuses[lv][:hq_target] == ''
        job_idress.bonuses[lv.to_s][:option] == '' 
      end
    }

    job_idress.acquired_by_mile = (params[:job_idress_type] == 'acquired_by_mile')
    job_idress.regular = (params[:job_idress_type] == 'regular')
    return job_idress
  end
  
  def nation
    case
    when params[:nation_id]
      Nation.find(params[:nation_id])
    when params[:owner_account_id]
      OwnerAccount.find(params[:owner_account_id]).nation
    else
      nil
    end
  end
end
