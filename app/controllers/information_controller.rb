class InformationController < ApplicationController
#  before_filter :login_required , :except => :last_information
  requires_role :herald , 
                :only => [:index , :list , :show , :new , :create , :edit , 
                          :update , :destroy]
                            
  requires_role :accountant , 
                :only => [:index , :list , :show , :new , :create , :edit , 
                          :update , :destroy]
                            
  set_login_filter
  
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
#  verify :method => :post, :only => [ :destroy, :create, :update ],
#         :redirect_to => { :action => :list }

  def list
    @information_pages, @information = paginate :information, :per_page => 10
  end

  def show
    @information = Information.find(params[:id])
  end

  def new
    @information = Information.new
  end

  def create
    @information = Information.new(params[:information])
    if @information.save
      flash[:notice] = 'Information was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @information = Information.find(params[:id])
  end

  def update
    @information = Information.find(params[:id])
    if @information.update_attributes(params[:information])
      flash[:notice] = 'Information was successfully updated.'
      redirect_to :action => 'show', :id => @information
    else
      render :action => 'edit'
    end
  end
  
  def last_information
    @latest_info = Information.find(:first ,:conditions => ["expired = ? " ,false] ,  :order => 'created_at DESC', :limit=>1)
    render(:layout => false)
  end

  def destroy
    Information.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
