# -*- encoding: utf-8 -*-
class OrganizationsController < ApplicationController
  requires_role :accountant , :only => [:create , :update  , :destroy ]
  requires_role :admin , :only => [ :destroy ]
  set_login_filter

  #  layout 'organizations' 
  # GET /organizations
  # GET /organizations.xml
  requires_role :herald , :only => [:create , :update  , :destroy , :import_confirm ]
  set_login_filter
  
  def index
    @organizations = Organization.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @organizations }
    end
  end

  # GET /organizations/1
  # GET /organizations/1.xml
  def show
    @organization = Organization.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @organization }
    end
  end

  # GET /organizations/new
  # GET /organizations/new.xml
  def new
    @organization = Organization.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @organization }
    end
  end

  # GET /organizations/1/edit
  def edit
    @organization = Organization.find(params[:id])
  end

  # POST /organizations
  # POST /organizations.xml
  def create
    Organization
    @organization = params[:organization][:type].constantize.new(params[:organization])

    respond_to do |format|
      if @organization.save
        flash[:notice] = '組織を登録しました。'
        format.html { 
          if has_role?(:accountant)
            redirect_to( facade_owner_accounts_path)
          else
            redirect_to( organizations_path) 
          end
        }
        format.xml  { render :xml => @organization, :status => :created, :location => @organization }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @organization.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.xml
  def update
    @organization = Organization.find(params[:id])
    @organization.type = params[:organization][:type] if @organization.type != params[:organization][:type]

    respond_to do |format|
      if @organization.update_attributes(params[:organization])
        flash[:notice] = 'Organization was successfully updated.'
        format.html { redirect_to(organizations_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @organization.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.xml
  def destroy
    @organization = Organization.find(params[:id])
    @organization.destroy

    respond_to do |format|
      format.html { redirect_to(organizations_url) }
      format.xml  { head :ok }
    end
  end
end
