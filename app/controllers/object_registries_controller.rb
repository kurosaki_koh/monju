# -*- encoding: utf-8 -*-
class ObjectRegistriesController < ApplicationController
  cache_sweeper :modify_sweeper , :only => [:create , :destroy , :update]

  requires_role :item_officer , :only => [:create , :update  , :destroy ,:new , :edit]
  set_login_filter

  include RowMover
  movable_row :child => :object_registry , :parent => :owner_account
  # GET /object_registries
  # GET /object_registries.xml
  def index
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @object_registries = @owner_account.object_registries.sort_by{|obr| 
        [(obr.object_definition ? obr.object_definition.hurigana.to_s : '' ),
          obr.note =~ /^アイドレス開始/o ? '00' : obr.note 
        ]
      }
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @object_registries }
    end
  end

  # GET /object_registries/1
  # GET /object_registries/1.xml
  def show
    @object_registry = ObjectRegistry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @object_registry }
    end
  end

  # GET /object_registries/new
  # GET /object_registries/new.xml
  def new
    @object_registry = ObjectRegistry.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @object_registry }
    end
  end

  # GET /object_registries/1/edit
  def edit
    @object_registry = ObjectRegistry.find(params[:id])
  end

  # POST /object_registries
  # POST /object_registries.xml
  def create
    @object_registry = ObjectRegistry.new(params[:object_registry])
    @object_registry.owner_account_id = params[:owner_account_id]
    respond_to do |format|
      if @object_registry.save
        flash[:notice] = '登録しました。'
        flash[:warning] = 'アイテム変動数が空欄です。間違っていませんか？' if @object_registry.number.nil?
        format.html { redirect_to(owner_account_object_registries_path(params[:owner_account_id])) }
        format.xml  { render :xml => @object_registry, :status => :created, :location => @object_registry }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @object_registry.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /object_registries/1
  # PUT /object_registries/1.xml
  def update
    @object_registry = ObjectRegistry.find(params[:id])

    respond_to do |format|
      if @object_registry.update_attributes(params[:object_registry])
        flash[:notice] = '編集しました。'
        flash[:warning] = 'アイテム変動数が空欄です。間違っていませんか？' if @object_registry.number.nil?
        format.html { redirect_to(owner_account_object_registries_path(params[:owner_account_id])) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @object_registry.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /object_registries/1
  # DELETE /object_registries/1.xml
  def destroy
    @object_registry = ObjectRegistry.find(params[:id])
    @object_registry.destroy

    respond_to do |format|
      format.html { redirect_to(owner_account_object_registries_path(params[:owner_account_id])) }
      format.xml  { head :ok }
    end
  end
  
  def wiki_output
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @name = URI.unescape(params[:name])
    @registries = @owner_account.object_registries.select{|r| r[:name] == @name}
  end
end
