class AdminController < ApplicationController
  before_filter :login_required 

#  def index
#  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  #verify :method => :post, :only => [ :destroy, :create, :update ],
  #       :redirect_to => { :action => :list }

  def list
    @yggdrasill_pages, @yggdrasills = paginate :yggdrasills, :per_page => 10
  end

  def show
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  def restriction_list
    @nations = Nation.find(:all).sort_by{|n| n[:id]}
  end
  
  def update_restriction
    for key in params[:nation].keys
      nation = Nation.find(key.to_i)
      nation[:restriction] = params[:nation][key]['restriction'] == "1"
      nation.save      
    end
    redirect_to :action => 'restriction_list'
  end
  
  def select_results
    @results  = Result.event_names('admin')
    if params[:keyword] != ""
      @results = @results.find_all{|r| r.event_name =~ /#{params[:keyword]}/ }
    end

    @none_related = []
    for r in @results
#      rbe = Result.find( :all, :conditions =>  [ "results.event_name = ?", r.event_name ] )
      @none_related << r  if r.event_id.nil?
    end
    
    @results_list = (params[:type] == "none") ? @none_related : @results

  end

  def select_relation_to_result
      @selected_results = Result.find(params[:result_item].keys)
      @entry_list = Event.find(:all , :conditions => "turn = 0")
  end

  def update_entry_list
    turn = request.raw_post || request.raw_post_data
    @entry_list = Event.find(:all , :conditions => ["turn = ?", turn.to_i])
#    render :update do |page|
#      page.replace 'entry_list' , :render=>:entry_list , :collection => @entry_list
#    end
    render :layout => false
  end

  def authorized?
    (current_user != :false) && (current_user.role == "admin" || current_user.has_role?(:admin) )
  end
  
  def add_relation_to_results
    event_id = params[:entry_list]
    results = Result.find(params[:results].keys)
    Result.transaction do 
      for r in results
        for r2 in Result.find( :all, :conditions =>  [ "results.event_name = ?", r.event_name ] )
          r2.event_id = event_id
          r2.save
        end
      end
    end
    redirect_to :action => :select_results , :type => :none
  end

end