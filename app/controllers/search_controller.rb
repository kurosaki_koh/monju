# -*- encoding: utf-8 -*-
require 'kconv'
class SearchController < ApplicationController

  def form
  end

  def result
    keyword = params[:keyword].chomp
    @results = {}
    case params[:target]
      when 'characters'
        case keyword
          when /\d{2}-(\d{5}|xxxxx)-(\d|a|x)./
            character = Character.where(character_no: keyword).first || Ace.where(character_no: keyword).first
            redirect_to character if character
          when /\d{5}-\d{2}/
            characters = Character.where('character_no like ?', '%' + keyword + '%')
            redirect_to characters[0] if characters[0]
            @results[:characters] = characters
          when /^根源力(：|\:)/
            @results[:characters] = search_by_originator(keyword)
          else
            keyword = '%'+keyword + '%'
            @results[:characters] = Character.where("character_no like ? or name like ?", keyword, keyword).sort_by { |c| c.character_no }
        end
      when 'cash'
        results = search_cash_records(keyword)
        @results[:cash_records] = results if results.size > 0
      when 'mile'
        results = search_mile_changes(keyword)
        @results[:mile_changes] = results if results.size > 0
      when 'results'
        results = ::Result.includes(:character).where("concat(event_name , '%' , magic_item , '%' , note) like ?", '%' + keyword + '%')
        results = results.select { |r| !r.character.nil? }
        @results[:results] = results.sort { |a, b|
          if a.event_name == b.event_name
            a.character.character_no <=> b.character.character_no
          else
            a.event_name <=> b.event_name
          end
        } if results.size > 0
      when 'idress'
        @results[:derived_job_idresses] = Job.where('name like ?', '%' + keyword + '%')
        job_idresses = JobIdress.all
        @results[:job_idresses] = job_idresses.select { |j| j.inspect_idresses =~ /#{keyword}/ }
        #(:all , :include => :acquired_idress).select{|i|i.acquired_idress.inspect_idresses}
        job4s = @results[:job_idresses].select { |j| j.idress_type == 'PlayerIdress' }
        acquirements = []
        for j4 in job4s
          acquirements += IdressAcquirement.where('job_idress_id = ?', j4)
        end
        @results[:acquirements] = acquirements if acquirements.size > 0
      when 'items'
#        raise params.to_yaml
        key = '%' + keyword + '%'
        @results[:items] = ObjectRegistry.
            group([:owner_account_id , :name]).
            where(["name like ? or note like ?" , key , key]).
            having('sum(number) > 0')

      when 'weapons'
        results = search_weapons(keyword)
        @results[:weapons] = results
      when 'command_resources'
        @results[:command_resources] = search_command_resources(keyword)
    end

  end

  def search_by_originator(keyword)
    keyword = sub_nums(keyword)
    result = if keyword =~ /根源力(：|\:)(\d+)(以上|以下)/
               threshould = $2.to_i
               characters = if $3 == '以上'
                              Character.all(:include => [:results, :used_results]).select { |ch|
                                (ch.originator = ch.sum_originator) >= threshould
                              }
                            else
                              Character.all.select { |ch|
                                (ch.originator = ch.sum_originator) <= threshould
                              }
                            end
               characters.sort_by { |ch| ch.character_no }
             else
               []
             end
    result
  end


  def send_for_download(format)
    output = render_to_string file: format, use_full_path: true, layout: false
    case params[:charset]
      when 'sjis'
        output = output.tosjis
    end
    send_data(output,
              :type => 'text/csv',
              :filename => "result.csv")
  end

  def result_by_csv
    @results = search_cash_records(params[:keyword].chomp)
    send_for_download('search/result_by_csv.csv.erb')
  end

  def mile_by_csv
    @results = search_mile_changes(params[:keyword].chomp)
    send_for_download('search/mile_by_csv.csv.erb')
  end

  def command_resources
    @command_resources = search_command_resources(params[:q])

    respond_to do |format|
      format.xml { render :xml => @command_resources }
      format.js { render :json => @command_resources,
                         only: [:name, :power, :tag, :command_resources],
                         include:
                             {owner_accounts:
                                  {
                                      only: [:number, :name]
                                  }
                             },
                         callback: params[:callback]
      }
    end
  end

  private

  def build_command_resource_hash(cr)
    result = {owner: {}}

    cr_hash = {}

    cr_hash = %w{name power tag}.inject({}) { |h, k|
      h[k.to_sym] = cr[k.to_sym];
      h
    }
    result[:command_resource] = cr_hash
    result[:owner][:name] = cr.owner_account.owner.name
    if cr.owner_account.owner_type == 'Character'
      result[:owner][:character_no] = cr.owner_account.owner.character_no
    end
    result
  end

  def search_cash_records(keyword)
    if keyword =~ /^\d+$/
      cash_results = CashRecord.includes(:owner_account).where("event_no like ? or turn = ?", '%' + keyword + '%', keyword)
    else
      keyword = '%'+keyword+'%'
      cash_results = CashRecord.includes(:owner_account).where("concat(event_no  ,'%', event_name ,'%' , note) like ?", keyword)
    end
    results =
        cash_results.select { |c| c.owner_account.owner_type == 'Nation' }.sort_by { |c|
          [c.owner_account.id, c.position]
        } + cash_results.select { |c| c.owner_account.owner_type != 'Nation' }.sort_by { |c|
          [c.owner_account.owner.type.to_s, c.owner_account.id, c.position]
        }
    results.uniq!
    return results
  end

  def search_mile_changes(keyword)
    miles = []
    case keyword
      when /^\-\d+$/
        miles += MileChange.find_all_by_mile(keyword, :include => :owner_account)
      when /^\d+$/
        miles += MileChange.includes(:owner_account).where('mile = ? or turn = ?', keyword, keyword)
    end
    wildcard =  '%'+keyword+'%'
    miles += MileChange.includes(:owner_account).where("concat(reason , '%' ,  note) like ? ", wildcard )
    miles.uniq!
    miles = miles.sort_by { |row| row.created_at }
    return miles
  end

  def search_weapons(keyword)
    @weapon_registries = WeaponRegistry.
        find_by_sql([<<-EOS, '%' + keyword + '%'])
select w.owner_account_id, sum(w.number) as number , w.name
 from weapon_registries w
 group by owner_account_id , name
 having w.name like ?
    EOS
  end

  def search_command_resources(keyword)
      with_wild_cards = '%' + keyword.html_safe + '%'
      CommandResourceDefinition.
          where('tag like ? or name like ?', with_wild_cards, with_wild_cards).
          includes(command_resources: [:owner_account]).uniq
  end

  def build_crd_hash(crds)
#    crds.
  end

end

