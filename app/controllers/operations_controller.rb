# -*- encoding: utf-8 -*-
require 'yggdrasill.rb'
require 'job_idress.rb'
require 'character.rb'
require 'operation_node.rb'

class OperationsController < ApplicationController
  layout 'owner_accounts' 
  # GET /organizations
  # GET /organizations.xml
  requires_role :herald , :only => [:create , :update  , :destroy , :import_confirm ]
  set_login_filter

  def index
    list
    render :action => 'list'
  end

  def list
    @operation_pages, @operations = paginate :operations, :per_page => 10
  end

  def show
    @operation = Operation.find(params[:id])
  end

  def new
    @operation = Operation.new
  end

  def create
    @operation = Operation.new(params[:operation])
    Operation.transaction do
      @operation.init_content
      troop = TroopNode.new(:name => '無所属')
      for nation in Nation.find(:all, :order => :id)
        if params[nation.id.to_s] == "true" then
          for character in nation.characters
            soldier = SoldierNode.new
            soldier.character = character
            soldier.job_idress = character.job_idress
            soldier.enable = true
            for skill in character.job_idress.skills << character.job_idress.skill
                
              modification = soldier.modifications.build #Modification.new
              modification.set_values(skill)
            end
                            
            troop.children.push soldier
            soldier.save
          end        
        end
      end
      @operation.troops.push troop
      troop.save
    end # END OF TRANSACTION
    @operation.content = @soldiers

    if @operation.save
      flash[:notice] = 'Operation was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @operation = Operation.find(params[:id])
  end

  def plan
    @operation = Operation.find(params[:id])
  end

  def update
    @operation = Operation.find(params[:id])
    if @operation.update_attributes(params[:operation])
      flash[:notice] = 'Operation was successfully updated.'
      redirect_to :action => 'show', :id => @operation
    else
      render :action => 'edit'
    end
  end

  def create_troop
    @troop = TroopNode.new()
    @troop.operation = params[:operation_id]
    @troop.name = params[:name]
    @troop.save

    render :update  do |p|
    
    end
  end

  def submit_plan
#    @operation = Operation.find(params[:id])
    @parameter = params
    children = @parameter[:children]
    for cid in children.keys.sort
      mo_params = children[cid][:modifications]
      next if mo_params.nil?
      for mid in mo_params.keys.sort
        update_modification(mid , mo_params)
      end
    end
    @parameter = params

    redirect_to :action => 'plan',:id=>params[:id]
  end
  
  def update_soldier
    children = params[:children]
    soldier_id = children.keys[0]
    mo_params = children[soldier_id][:modifications]
    mid = mo_params.keys[0]
    update_modification(mid,mo_params)
    @soldier = SoldierNode.find(soldier_id)
    @troop = TroopNode.find(@soldier.parent_id)
    
    render :update do |page|
      page.replace_html  "troop_#{@soldier.id}", :partial=>'soldier'
      page.replace_html  "troop_rd_sum_#{@troop.id}", :partial=>'troop_rd_sum'
      page.replace_html  "troop_sum_#{@troop.id}", :partial=>'troop_sum'
      page.replace_html  "troop_#{@troop.id}_fuel", "燃料消費：#{@troop.fuel}" 
    end
  end
  
  def destroy
    Operation.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
  
  hide_action :update_modification
  
  def update_modification(mid,mo_params)
    mo = Modification.find(mid)
    mod_val = mo_params[mid][:mode].nil? ? nil : mo_params[mid][:mode].to_i

    if mod_val && mo.mode != mod_val 
      mo.mode = mod_val
      for col in Job::AllAbilities
        mo[col]=mo.modes[mod_val][col]
      end          
    end
    
    enable = mo_params[mid][:enable]
    mo.enable =  enable == "1" unless enable.nil?
    
    mo.save
  end

  
end
