class WeaponsController < ApplicationController
  IObject # for require 'iobject.rb'
  layout 'owner_accounts'
  def index
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @weapons = @owner_account.weapon_registries
  end
end
