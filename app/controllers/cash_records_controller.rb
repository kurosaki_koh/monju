# -*- encoding: utf-8 -*-
require 'open-uri'
require 'hpricot'

class CashRecordsController < ApplicationController
  
  cache_sweeper :modify_sweeper , :only => [:create , :destroy , :update , :import_confirm]
  
  requires_role :accountant , :only => [:create , :update  , :destroy , :import_confirm]
  set_login_filter

  include RowMover
  movable_row :child => :cash_record , :parent => :owner_account  , :child_class => CashRecord

#  verify :method => :post, :only => [ :import_select_form ]
  # GET /cash_records
  # GET /cash_records.xml
  def index
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    if params[:turn]
      @target_turn = params[:turn]
      if params[:turn] == 'last'
        record = CashRecord.find(:first , :order => 'turn desc', :conditions => ['owner_account_id = ?' , @owner_account])
        @target_turn = record.turn if record && record.turn
      end
    end
    if @target_turn
      cr = CashRecord.find_all_by_owner_account_id_and_turn(params[:owner_account_id] , @target_turn)
    else
      cr= @owner_account.cash_records
    end

    @cash_records = cr.sort_by{|c|[c.turn , c.position]}
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cash_records }
    end
  end
  
  # GET /cash_records/1
  # GET /cash_records/1.xml
  def show
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = CashRecord.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @cash_record }
    end
  end
  
  # GET /cash_records/new
  # GET /cash_records/new.xml
  def new
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = CashRecord.new
    @cash_record.turn = Property[:current_turn].to_i
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cash_record }
    end
  end
  
  # GET /cash_records/1/edit
  def edit
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = CashRecord.find(params[:id])
  end
  
  # POST /cash_records
  # POST /cash_records.xml
  def create
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = @owner_account.cash_records.create(params[:cash_record])

    respond_to do |format|
      if @cash_record
        flash[:notice] = '財務表の項目を追加しました。'
        format.html { redirect_to(owner_account_cash_record_path(@owner_account , @cash_record) ) }
        format.xml  { render :xml => @cash_record, :status => :created, :location => @cash_record }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @cash_record.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # PUT /cash_records/1
  # PUT /cash_records/1.xml
  def update
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = CashRecord.find(params[:id])
    
    respond_to do |format|
      if @cash_record.update_attributes(params[:cash_record])
        flash[:notice] = '財務表の項目を更新しました。'
        format.html {
          if @cash_record.turn.to_i > 0
            redirect_to(owner_account_cash_records_path(@owner_account,:turn => @cash_record.turn))
          else
            redirect_to(owner_account_cash_records_path(@owner_account))
          end
        }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @cash_record.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # DELETE /cash_records/1
  # DELETE /cash_records/1.xml
  def destroy
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @cash_record = CashRecord.find(params[:id])
    @cash_record.destroy
    
    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end
  
  def import_select_table
    cash_url = params[:cash_url]
    doc = Hpricot(open(cash_url).read.toutf8)
    @tables = doc/:table
  end
  
  DEFAULT_COLUMN_ORDER = [:event_no , :event_name , nil,:entry_url , :result_url , :fund , :resource , :food , :fuel ,:amusement, :num_npc , 
  :num_id , :note , :turn]
  
  def import_confirm
    doc = Hpricot(open(params[:cash_url]).read.toutf8)
    @table = doc/params[:xpath]
    @table[0].set_attribute("border","1")
    
    
    rows = []
    for table_row in @table/:tr
      table_cells = table_row/:td
      next if table_cells.inner_text =~ /・ゃ潟・/
      cells = []
      cells << table_cells[0].inner_text
      cells << table_cells[1].inner_text
      cells << nil
      entry_link = (table_cells[3]/"a").first
      
      cells << (entry_link ? entry_link['href'] : nil)
      
      result_link = (table_cells[4]/"a").first
      cells << (result_link ? result_link['href'] : nil)
      cells << table_cells[5].inner_text.to_i
      cells << table_cells[6].inner_text.to_i
      cells << table_cells[7].inner_text.to_i
      cells << table_cells[8].inner_text.to_i
      cells << table_cells[9].inner_text.to_i
      cells << table_cells[10].inner_text.to_i
      cells << table_cells[11].inner_text.to_i
      cells << table_cells[12].inner_text
      rows  << cells      
    end
    
    update_from_rows(rows, {:owner_account_id => params[:owner_account_id]})
    redirect_to owner_account_cash_records_path(params[:owner_account_id])
  end
  def update_from_rows(rows,option = {},columns = DEFAULT_COLUMN_ORDER)
    attributes = {}
    for row in rows
      for col in columns
        val = row.shift
        attributes[col] = val if col
      end
      owner_account = OwnerAccount.find(option[:owner_account_id])
      cr = owner_account.cash_records.build
      cr.update_attributes(attributes)
      cr.save
    end
  end
  
  
end
