class SkillsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  def list
    @skill_pages, @skills = paginate :skills, :per_page => 10
  end

  def show
    @skill = Skill.find(params[:id])
  end

  def new
    @skill = Skill.new
  end

  def create
    @skill = Skill.new(params[:skill])
    if @skill.save
      flash[:notice] = 'Skill was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @skill = Skill.find(params[:id])
  end

  def update
    @skill = Skill.find(params[:id])
    if @skill.update_attributes(params[:skill])
      flash[:notice] = 'Skill was successfully updated.'
      redirect_to :action => 'show', :id => @skill
    else
      render :action => 'edit'
    end
  end

  def destroy
    Skill.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
