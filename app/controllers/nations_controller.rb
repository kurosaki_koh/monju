# -*- encoding: utf-8 -*-
require 'open-uri'
require 'hpricot'


class NationsController < ApplicationController
  cache_sweeper :modify_sweeper ,     :only => [:register_result ,    :delete_results_by_event,
                                                  :change_result_state , :update_results_by_event ]
#  before_filter :login_required , :only => [:create , :destroy, :edit , :update , 
#    :delete_results_by_event , :confirm_register_results]
  requires_role :herald , :only => [:create , :destroy , :edit , :update , :change_result_state ,
                                   :delete_results_by_event , :confirm_register_results ]
  set_login_filter
  helper InformationHelper
  #  helper_method :latest_information

  before_filter :find_nation , :except => [:new , :top,:index , :associated_nations_list]


  def find_nation
    @nation = Nation.find(params[:id])
  end
  def index
    top
    #    render :action => 'top'
  end
  
  ## GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  #verify :method => :post, :only => [ :create, :delete_results_by_event,
  #  :import_by_manual, :import_results  ,:import_text      ],
  #  :redirect_to => { :action => :list }
  #
  #verify :method => :put , :only =>[:update] , :redirect_to => { :action => :list }
  #
  def list
    @nation_pages, @nations = paginate :nations, :per_page => 20
  end
  
  def show
  end
  
  def new
    @nation = Nation.new
  end
  
  def create
    @nation[:belong] = @nation[:belong].to_sym
    if @nation.save
      flash[:notice] = '藩国を追加しました。'
      redirect_to :action => 'top'
    else
      render :action => 'new'
    end
  end
  
  def edit
#    @nation = Nation.find(params[:id])
  end
  
  def update
    params[:nation][:belong] = params[:nation][:belong].to_sym
    if @nation.update_attributes(params[:nation]) 
      flash[:notice] = '藩国詳細を更新しました。'
      redirect_to @nation
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    Nation.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
  
  def format

    @characters = characters()
  end
  
  def format_wiki
    @characters = characters()
    render(:layout => "wiki_output")
  end
  
  def war_potential
    @characters = characters()
  end
  
  def war_potential_pukiwiki
    @characters = characters()
    render(:layout => "wiki_output")
  end
  
  def republic_format
    @characters = characters()
    render(:layout => "wiki_output")
  end
  
  def weared_idress
    @characters = characters()
  end

  def update_bonus_form
    @nation = Nation.find(params[:id])
  end

  def weared_idress_pukiwiki
    @characters = characters()
    render(:layout => "wiki_output")
  end
  
  def edit_weared_idress
    @characters = characters()
  end
  
  def update_weared_idress
    @characters = params[:character]
    Character.transaction do
      for key in @characters.keys.sort
        character_data = Character.find(key)
        character_data.update_attributes(@characters[key])
        character_data.save
      end
    end
    flash[:notice] = '着用アイドレスを更新しました。'
    redirect_to :action => "edit_weared_idress" , :id => params[:id]
  end
  
  def top

    #    @nations = Nation.find(:all).sort_by do |n| n.id end
    nations = Nation.all.sort_by{|n| n . id }
    @republic_nations = nations.select{|n|n.belong == :republic}
    @empire_nations =  nations.select{|n|n.belong == :empire}
    respond_to do |format|
      format.html {render :action => :top } # list.html.erb
      format.xml  {render :xml => @republic_nations + @empire_nations }
      format.js { render :json => (@republic_nations + @empire_nations) , :callback => params[:callback] } 
    end
  end
  
  def associated_nations_list
    #    @nations = Nation.find(:all).sort_by do |n| n.id end
    @nations = Nation.where("belong like '%associated%'")
  end
  
  def menu_link
    @nation = Nation.find(params)
  end
  
  def results_by_event
    @results , @result = NationsController::find_results_by_event(params[:result_id] , params[:id])
  end
  
  def update_results_by_event
    @results , @result = NationsController::find_results_by_event(params[:result_id] , params[:id])
    
    ::Result.transaction do 
      for r in @results
        next if r.character.nil?
        r[params[:column]]=params[:value]
        r.save
      end
    end
    flash[:notice] = '変更しました。'
    redirect_to :action => :results_by_event , :result_id => params[:result_id] , :id => params[:id]
  end
  
  def self.find_results_by_event(result_id , nation_id )
    result = ::Result.find(result_id)
    if nation_id =='admin'
      nid = 'admin'
    else
      nid = result.character.nation_id
    end
    
    results = ::Result.list_by_event_name(result_id , nid)
    return results,result
  end
  
  class SubmitFormParseResult
    attr :text , true
    attr :matched , true
    attr :character_no , true
    attr :left_str , true
    attr :name , true
    attr :db_name, true
    attr :originator , true
    attr :note , true
    attr :url , true
    attr :left_str ,true
    alias :matched?  matched
    
    def parse_and_set(str)
      str.strip!
      
      case str
      when /^(h)?ttp:\/\//
        self.url = str
      when /^(\-?\d+)$/
        self.originator = str.to_i
      else
        self.note = str
      end
    end
    
    def parse(line)
      self.text = line.strip
      #      if (line =~ /h?ttp:\/\// )||(line =~ /同上/)
      #        @matched = false
      #        return 
      #      end
      
      if line =~ /^(\d{2}-\d{5}-\d{2})[：:](.+?)[：:](.+)/
        @character_no = $1
        @name = $2
        
        left_str = $3
        cols = left_str.split('：')
        for col in cols
          parse_and_set(col)
        end
        @matched = true
      else
        @matched = false
      end
    end
  end


  def register_result_form
    @nation = Nation.find(params[:id])
  end
  def parse_register_result(submited , admin_flag = false)
    parse_results = []
    db_not_found = []
    register_data = {}
    submited.each_line do |line| 
      parse_result = SubmitFormParseResult.new
      parse_result.parse(line)
      parse_results << parse_result
      next unless parse_result.matched?
      
      c = Character.find_by_character_no(parse_result.character_no)
      if c && (admin_flag || c.nation_id == auth_context_id.to_i)
        parse_result.db_name = c.name
        register_data[parse_result.character_no]=parse_result
      else
        db_not_found << parse_result
        parse_result.matched = false
      end           
    end
    return parse_results , register_data , db_not_found
  end
  
  
  def confirm_register_result
    @nation = Nation.find(params[:id])

    @event_name = params[:event_name]
    @magic_item = params[:magic_item]
    @not_yet = params[:not_yet] ? "1" :"0"
    @reference_url = params[:reference_url]
    
    @parse_results , @register_data , @db_not_found = parse_register_result(params[:submit_format] , params[:id] == 'admin')
    
    @parse_results.find_all{|r| r.matched?}.each{|r|
      r.originator = params[:default_originator] unless r.originator
      r.url = params[:reference_url] #  unless parse_result.url
    }
  end
  
  
  def register_result
    @nation = Nation.find(params[:id])

    results = params[:results]
    
    ::Result.transaction do 
      @registered_data = []
      for key in results.keys.sort
        result = results[key]
        @registered_data << [result[:character_no],result[:originator],result[:note],result[:url]].join(',')
        ch = Character.find_by_character_no(result[:character_no])
        if ch 
          new_res = ch.results.create
          new_res.originator = result[:originator]
          new_res.note = result[:note]
          new_res.event_name = params[:event_name]
          new_res.magic_item = params[:magic_item]
          new_res.url = result[:url]
          new_res.not_yet = params[:not_yet] == "1"
          new_res.save
        end
      end
    end
  end
  
  def delete_results_by_event
    @results = ::Result.list_by_event_name(params[:result_id].to_i , params[:id])
    nation_id = params[:id].to_i if params[:id] && params[:id] != "admin"
    @destroyed = []
    ::Result.transaction do
      for r in @results
        if params[:id] == "admin"
          r.destroy if has_role?(:herald)
        elsif params[:id]
          r.destroy if has_role?(:herald)
        else
          ::Result.rollback_db_transaction
          access_denied
        end
      end
    end
    if params[:id] != 'admin'
      redirect_to :action=> :show , :id =>  params[:id]
    else
      redirect_to :action=> :top
    end    
  end
  
  def change_result_state
    access_denied if params[:id] == 'admin' && (not is_admin?)
    @results = ::Result.list_by_event_name(params[:result_id] , params[:id])
    flag = params[:state] == "1"
    nation_id = params[:id]
    ::Result.transaction do
      for r in @results
        if  nation_id == 'admin' || authorized?(r.character.nation_id)
          r.not_yet = flag
          r.save 
        else
          ::Result.rollback_db_transaction
          access_denied
        end
      end
    end
    redirect_to :action=> :results_by_event , :id=>nation_id , :result_id => params[:result_id]
  end
  
  def obtain_results_table
    @target_url = params[:url]
    @target_urls = split_urls(params[:url] )
    @tables = {}
    @characters = {}
    @tables_url = {}
    @elements = {}
    @docs = []
    @done = []
    characters = Character.find(:all,:conditions => ["nation_id = ?",params[:id]])
    for url in @target_urls
      @docs << @doc = Hpricot( open(url.strip).read.toutf8 )
      @lookup = {}
      for character in characters
        cno = character.character_no[0..1]+"-?"+character.character_no[2..6]
        searched = search_last_element_by_text(@doc,/#{cno}([（\(].*?\d+?[）\)])?[：:]/)
        @lookup[character.character_no] = searched if searched
      end
      
      table1 = table2 = nil
      for key in @lookup.keys
        ch = Character.new
        ch.character_no = key
        if @lookup[key]
          table1 = search_next_tag(@doc , @lookup[key],"table")
          if table1 
            parse_table(ch,table1,false,false)
            table1.set_attribute("border","1")
            table2 = search_next_tag(@doc , table1.next_node,"table")
            @tables_url[table1] = url
            if table2
              table2.set_attribute("border","1") 
              parse_table(ch,table2,false,false)
              @tables_url[table2] = url
            end
          else
            table2 = nil        
          end
          @tables[key] = [table1,table2]
          @characters[key]=ch
        end
      end
      @elements.update(@lookup)
    end
  end
  
  def import_results
    #    target_urls = params[:target_url].split("\n").collect{|u|u.strip}
    
    check = params[:check]
    tables = params[:tables]
    
    doc_cache = Hash.new{|hash,url| hash[url]=Hpricot( open(url).read.toutf8 ) }
    #    for url in target_urls
    @characters = []
    for key in tables.keys.sort
      ch = Character.find_by_character_no(key)
      for idx in ["0","1"]
        if tables[key][idx] && tables[key][idx][:xpath] && check[key][idx] && check[key][idx][:ignore] && check[key][idx][:ignore]=="0"
          doc = doc_cache[tables[key][idx][:url] ]
          path = tables[key][idx][:xpath]
          clean_flag = false
          clean_flag = ! (check[key][idx] && check[key][idx][:append] && check[key][idx][:append] == "1")
          parse_table(ch,(doc/path),true,clean_flag)
        end
      end
      @characters << ch
    end
    #    end
  end
  
  def split_urls(urls)
    target_urls = urls.strip.gsub("\r","\n")
    array = urls.split("\n").collect{|u|u.strip}
    array.delete_if{|u| u.nil? || u == ""  }
    array
  end
  
  def import_by_manual_form
    @urls = split_urls(params[:target_urls] )
    @tables = Hpricot::Elements.new
    @table_url = {}
    
    for url in @urls
      url.strip!
      doc = Hpricot( open( url.strip ).read.toutf8 )
      now_tables = (doc/:table)
      now_tables.each{|t| @table_url[t]=url }
      @tables += now_tables
    end
  end
  
  def import_by_manual
    charas = Hash.new{|hash,cno| hash[cno]=Character.find_by_character_no(cno)}
    charas["NULL"]=nil
    docs = Hash.new{|hash,u| hash[u]=Hpricot( open(u).read.toutf8 ) }
    
    tables = params[:tables]
    
    for idx in tables.keys.sort
      cno = tables[idx][:character]
      ch = charas[cno ]
      next unless ch
      doc = docs[tables[idx][:url]]
      table = doc/(tables[idx][:xpath])
      parse_table(ch,table,true,tables[idx][:append].nil?)
    end
    @characters = []
    for key in charas.keys.sort
      @characters << charas[key] unless key == "NULL"
    end
    render(:action => :import_results)
  end
  
  def parse_table(character,table ,save_flag = false,clear_flag = true)
    if auto_detect(table) == :used_results
      UsedResult.transaction do
        character.used_results.clear if clear_flag
        rows = (table/:tr)
        row = rows[0]
        unless rows.inner_text =~ /初期根源力/ || table.inner_text =~ /(ＰＣ|PC)名/
          rec = set_used_result_cols(character,row)
          rec.save if save_flag
        end
        
        for row in rows[1..-2]
          rec = set_used_result_cols(character,row)
          rec.save if save_flag
        end
        row = rows[-1]
        cells = row/:td
        if (cells[0] && (cells[0].inner_text =~ /計/ ))|| (cells[1] && (cells[1].inner_text =~ /計/))
          next
        else
          rec = set_used_result_cols(character,row)
          rec.save if save_flag
        end
      end
      return 
    end
    
    
    if auto_detect(table) == :results
      ::Result.transaction do
        character.results.clear if clear_flag
        rows = (table/:tr)
        row = rows[0]
        unless rows.inner_text =~ /アイテム/ 
          rec = set_result_cols(character,row)
          rec.save if save_flag
        end
        for row in rows[1..-3]
          rec = set_result_cols(character,row)
          rec.save if save_flag
        end
        for row in rows[-2..-1]
          cells = row/:td
          if cells[0].inner_text =~ /計/ 
            next
          else
            rec = set_result_cols(character,row)
            rec.save if save_flag
          end
        end
      end
      return 
    end
    
  end
  
  def confirm_results_text
    parse_results_text
  end
  
  
  def confirm_bonus_text
    @nation =  Nation.find(params[:id])
    @parse_results , @register_data , @db_not_found = 
      parse_register_result(params[:character_list] , params[:id] == 'admin')
  end
  
  def update_bonus
    characters = params[:characters]
    
    @bonus_updated = []
    col = params[:column]
    targets = Job::Abilities.dup.collect{|c|c.to_s} + ["ar"]
    unless targets.include?(col)
      redirect_to :action=> :top 
      return
    end
    bonus = params[:bonus].to_i 
    
    Character.transaction do 
      for key in characters.keys.sort
        ch_data = characters[key]
        ch = Character.find_by_character_no(ch_data[:character_no])
        next unless ch
        next if params[:id] != 'admin' && params[:id].to_i != ch.nation_id
        if authorized?(params[:id]) 
          ch[col] = ch[col].to_i + bonus
          ch.save
          @bonus_updated << ch
        end
      end
    end
    if params[:id] == 'admin' then
      redirect_to :controller => :admin , :action => :index , :id => params[:id]
    else
      redirect_to :action => :show , :id => params[:id]
    end
  end
  
  def import_text
    parse_results_text
    
    for cells in @rows
      case @result_type
      when "used"
        set_used_result_cols_from_text(@ch , cells).save
      when "result"
        set_result_cols_from_text(@ch , cells).save
      else
      end
    end
  end
  
  def auto_detect(table)
    row =  (table/:tr)
    
    if (row[0] && row[0].inner_text =~ /初期根源力/) || (row[0] && row[0].inner_text =~ /(ＰＣ|PC)名/)
      return :used_results
    elsif (row[0] && row[0].inner_html =~ /アイテム/ )
      return :results
    end
    cells = row/:td
    if cells.size == 5 
      return :used_results
    elsif cells.size==4
      return :results
    end
    return nil
    
  end
  
  def vcc_format
    @nation = Nation.find(params[:id])
    @characters = characters.sort_by{|ch| ch.job_idress ? ch.job_idress[:id]: 0}
  end
  
  def items_by_nation
    query_items_by_nation
    render :action => "items_by_nation", :layout => "items"
  end
  
  def items_by_nation_for_wiki
    query_items_by_nation
  end
  
  def miles
    @nation = Nation.find(params[:id])
    @characters = characters
  end

private
def characters
  set = Character.find(:all, :conditions => ['nation_id= ?' , params[:id] ] )
  set.sort_by do |c| c.character_no end
end
  

def query_items_by_nation
    @nation = Nation.find(params[:id])
    @character_registries = @nation.characters.inject([]){|list , c|
      list += c.owner_account.object_registries
    }.sort_by{|item| [item.created_at , item.name]}
    @characters_props = @nation.characters.inject([]){|array , c| 
      array += c.owner_account.object_registries.owned_objects
      array
    }
    @bonds = Bond.find_by_sql([<<-EOS,
      select * 
      from bonds b inner join characters c
      on b.character_id = c.id
      where c.nation_id = ?
      EOS
      params[:id]])
end

def search_last_element_by_text(doc,re)
  last = nil
  count = 0
  latest = nil
  doc.traverse_element{|e|
    latest = e if e.elem?
    last = latest if e.inner_text =~ re  # && e.xpath !~ /tr/
    count += 1
  }
  last
end
  
def search_next_tag(doc,elem,cond)
    
  reached = false
  count = 0
  doc.traverse_element{|e|
    if (not reached)
      if e == elem
        reached = true
      else
        next
      end
    else
      count += 1
      if e.text? 
        if count > 2 && e.inner_text =~ /(\d{7}|\d{2}\-\d{5})([（\(]\d+[）\)])?[：:]/
          return nil 
        end
      end
        
      return e if e && (not e.text?) && (not e.comment?) && e.name == cond
    end
  }
    
  return nil
end
  
def set_result_cols(character , row)
  cells = row/:td
  res = character.results.build
  res.event_name = cells[0].inner_text.strip
  if cells[1].inner_html =~ /(strike|<s>)/i
    res.originator = 0
  else
    str = 0
    if cells[1].inner_text =~ /([－-]?\d+)/
      str = $1.gsub('－','-')
    end
    res.originator = str.to_i
  end       
  res.magic_item  = cells[2].inner_html.strip if cells[2]
  res.note = cells[3].inner_html.strip if cells[3]
  return res    
end
  
def set_used_result_cols(character , row)
  cells = row/:td
  ur = character.used_results.build
  ur.pc_name = cells[0] && cells[0].inner_text.strip
  ur.event_name = cells[1].inner_html.strip if cells[1]
  ur.initial =  cells[2] && (cells[2].inner_html =~ /(strike|<s>)/i ? 0 : cells[2].inner_text.to_i )
  ur.result =  cells[3] &&( cells[3].inner_html =~ /(strike|<s>)/i ? 0 : cells[3].inner_text.to_i )
  ur.note =   cells[4] && (cells[4].inner_html.strip)
  ur
end
  
def parse_results_text
  @cno = params[:character]
  @ch = Character.find_by_character_no(@cno)
    
  @result_type = params[:result_type]
  @separator = @separator_org = params[:separator]
  @separator = "\t" if @separator_org == "tab"
  @text = params[:text]
    
  @rows = []
  for line in @text
    @rows << line.split(@separator).collect{|c| c.strip}
  end  
end
  
def set_result_cols_from_text(character,cells)
  res = character.results.build
  res.event_name = cells[0].strip
  str = 0
  if cells[1] =~ /([－-]?\d+)/
    str = $1.gsub('－','-')
  end
  res.originator = str.to_i
  res.magic_item  = cells[2].strip if cells[2].strip
  res.note = cells[3].strip if cells[3].strip
  return res    
end
  
def set_used_result_cols_from_text(character , cells)
  ur = character.used_results.build
  ur.pc_name = cells[0].strip
  ur.event_name = cells[1].strip
  ur.initial = cells[2].to_i
  ur.result =  cells[3].to_i
  ur.note =   cells[4].strip
  return ur
end
  
def search_last_element_by_text(doc,re)
  last = nil
  count = 0
  latest = nil
  doc.traverse_element{|e|
    latest = e if e.elem?
    last = latest if e.inner_text =~ re  # && e.xpath !~ /tr/
    count += 1
  }
  last
end

def search_next_tag(doc,elem,cond)

  reached = false
  count = 0
  doc.traverse_element{|e|
    if (not reached)
      if e == elem
        reached = true
      else
        next
      end
    else
      count += 1
      if e.text? 
        if count > 2 && e.inner_text =~ /(\d{7}|\d{2}\-\d{5})([（\(]\d+[）\)])?[：:]/
          return nil 
        end
      end

      return e if e && (not e.text?) && (not e.comment?) && e.name == cond
    end
  }
    
  return nil
end
  
def set_result_cols(character , row)
  cells = row/:td
  res = character.results.build
  res.event_name = cells[0].inner_text
  if cells[1].inner_html =~ /(strike|<s>)/i
    res.originator = 0
  else
    str = 0
    if cells[1].inner_text =~ /([－-]?\d+)/
      str = $1.gsub('－','-')
    end
    res.originator = str.to_i
  end       
  res.magic_item  = cells[2].inner_html if cells[2]
  res.note = cells[3].inner_html if cells[3]
  return res    
end

def set_used_result_cols(character , row)
  cells = row/:td
  ur = character.used_results.build
  ur.pc_name = cells[0] && cells[0].inner_text
  ur.event_name = cells[1].inner_html if cells[1]
  ur.initial =  cells[2] && (cells[2].inner_html =~ /(strike|<s>)/i ? 0 : cells[2].inner_text.to_i )
  ur.result =  cells[3] &&( cells[3].inner_html =~ /(strike|<s>)/i ? 0 : cells[3].inner_text.to_i )
  ur.note =   cells[4] && (cells[4].inner_html)
  ur
end

def parse_results_text
  @cno = params[:character]
  @ch = Character.find_by_character_no(@cno)
    
  @result_type = params[:result_type]
  @separator = @separator_org = params[:separator]
  @separator = "\t" if @separator_org == "tab"
  @text = params[:text]
    
  @rows = []
  for line in @text
    @rows << line.split(@separator).collect{|c| c.strip}
  end  
end

def set_result_cols_from_text(character,cells)
  res = character.results.build
  res.event_name = cells[0]
  str = 0
  if cells[1] =~ /([－-]?\d+)/
    str = $1.gsub('－','-')
  end
  res.originator = str.to_i
  res.magic_item  = cells[2] if cells[2]
  res.note = cells[3] if cells[3]
  return res    
end
  
def set_used_result_cols_from_text(character , cells)
  ur = character.used_results.build
  ur.pc_name = cells[0]
  ur.event_name = cells[1]
  ur.initial = cells[2].to_i
  ur.result =  cells[3].to_i
  ur.note =   cells[4]
  return ur
end

end

