# -*- encoding: utf-8 -*-
class CharactersController < ApplicationController
  cache_sweeper :modify_sweeper , 
     :only => [:create , :destroy , :update, 
               :new_result , :update_result, :destroy_result ,
               :new_used_result , :update_used_result , :destroy_used_result 
              ]

  #  before_filter :login_required , 
#  :only => [:create,:destroy, :insert_by_text, :edit , :update , 
#  :edit_result, :new_result , :update_result , :destroy_result,
#  :update_used_result , :new_used_result, :destroy_used_result]

  requires_role :herald , 
  :only => [:create,:destroy, :insert_by_text, :edit , :update , 
  :edit_result, :new_result , :update_result , :destroy_result,
  :update_used_result , :new_used_result, :destroy_used_result ,
  :move_position ,
  :acquired_idresses , :edit_note_for_acquirement , :update_note_for_acquirement ,
  :append_acquired_idress , :remove_acquired_idress 
  ]

  requires_role :monju_maintainer ,
    :only => [:acquired_idresses , :edit_note_for_acquirement ,
              :update_note_for_acquirement ,
              :append_acquired_idress , :remove_acquired_idress ]

  set_login_filter
  
  def index
    list
    #   render :action => 'list'
  end
  
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
#  verify :method => :post, :only => [ :create, :destroy_used_result , :destroy_result ,
#   ],
#  :redirect_to => { :action => :list }
  
  def list
    if params[:character_no]
      @characters = [Character.find_by_character_no(params[:character_no])]
    else
      @characters = Character.find(:all , 
                                   :conditions => ["nation_id = ?", params[:nation_id]]).sort_by{|c|c.character_no}
    end

    @characters.each{|ch| ch.originator = ch.sum_originator}
    respond_to do |format|
      format.html # list.html.erb
      format.xml  { render :xml => @characters }
      format.js { render :json => @characters , :callback => params[:callback] } 
    end
  end
  
  def find

    if params[:nation_id] 
      @characters = Character.find(:all , 
                                   :conditions => ["nation_id = ?", params[:nation_id]]).sort_by{|c|c.character_no}
    elsif params[:character_no]
      @characters = Character.find_by_character_no(params[:character_no])
    end
    
    respond_to do |format|
      format.html {render :template => 'list' }
      format.xml  { render :xml => @characters }
      format.js { render :json => @characters , :callback => params[:callback] } 
    end
  end
  
  def show
    if params[:character_no]
      @character = Character.find_by_character_no(params[:character_no])
    else
      @character = Character.find(params[:id])
    end
    if @character.nil? then
      
    end
    @character.originator = @character.sum_originator
    respond_to do |format|
      format.html # list.html.erb
      format.xml  { render :xml => @character }
      format.js { render :json => @characters , :callback => params[:callback] } 
    end
  end
  
  def new
    @character = Character.new
    @character.owner_account = OwnerAccount.new
    if params[:nation_id] then
      @character.nation = Nation.find(params[:nation_id].to_i)
    end
    @character
  end
  
  def create
    @character = Character.new(params[:character])
    if @character.save
      flash[:notice] = 'キャラクターを追加しました。'
      redirect_to nation_characters_path(@character.nation)
    else
      render :action => 'new'
    end
  end
  
  def edit
    @character = Character.find(params[:id])
  end
  
  def edit_result
    @character = Character.find(params[:id])
  end
  
  def update
    @character = Character.find(params[:id])
    if @character.update_attributes(params[:character])
      flash[:notice] = 'キャラクター詳細を更新しました。'
      redirect_to nation_characters_path(@character.nation)
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    ch = Character.find(params[:id])
    nation_id = ch.nation_id
    ch.destroy
    redirect_to nation_characters_path(nation_id)
 end
  
  
  def update_job_idress_list
    @nation = Nation.find(params[:nation_id])
    @owner_account = @nation.owner_account
    render(:layout => false)
  end
  
  def parse_text(text)
    rows = []
    records = []    
    for line in list
      record = {}
      line.strip!
      row = line.split(/\s+/).join(',')
      next if row.size == 0
      rows << row
      record = {}
      data = row.split(',')
      for col in [:character_no,:name, :originator ]
        record[col]=data.shift
      end
      record[:jobs] = data.shift.split(/[＋+]/)
      records << record
    end
    records
  end
  
  def find_job_idress(record)
    nation_id = record[:character_no][0..1]
    prepare_job_idresses(nation_id)
    
    for ji in @jis[nation_id]
      return ji if ji.is_this?(record[:jobs])
    end
    return nil
  end
  
  def prepare_job_idresses(nation_id)
    @jis = {} unless @jis
    @jis[nation_id] = JobIdress.find(:all,:conditions => "nation_id = #{nation_id}" ) unless @jis[nation_id]
  end
  
  def insert_form
    
  end
  
  def new_result
    @character = Character.find(params[:id])
    result = @character.results.create(params[:new_result])
    render :action=> 'edit_result' , :id=>params[:id]
  end
  
  def destroy_result
    result = ::Result.find(params[:id])
    character = result.character
    result.destroy
    
    redirect_to :action => 'edit_result' , :id=>character
  end
  
  def destroy_used_result
    UsedResult.find(params[:id]).destroy
    
    redirect_to :action => 'edit_result' , :id=>params[:character_id]
  end
  
  def update_result
    results = params[:result]
    flag = true
    
    ::Result.transaction do
      for r_key in results.keys
        result_record = ::Result.find(r_key)
        if result_record != results[r_key]
          flag = flag && result_record.update_attributes(results[r_key])
        end
        raise ActiveRecord::Rollback ,"A Result update failed." unless flag
      end
      if flag
        flash[:notice] = '根源力は更新されました'
        redirect_to :action => 'edit_result', :id => params[:id]
      else
        render :action => 'edit'
      end
    end
  end
  
  def update_used_result
    used_results = params[:used_result]
    flag = true
    UsedResult.transaction do
      for r_key in used_results.keys
        result_record = UsedResult.find(r_key)
        if result_record != used_results[r_key]
          flag = flag && result_record.update_attributes(used_results[r_key])
        end
        raise ActiveRecord::Rollback ,"A UsedResult update failed." unless flag
      end
      if flag
        flash[:notice] = '転用根源力は更新されました'
        redirect_to :action => 'edit_result', :id => params[:id]
      else
        render :action => 'edit'
      end
    end
  end
  
  def new_used_result
    @used_result = UsedResult.new
    if params[:id] then
      @used_result.character = Character.find(params[:id].to_i)
    end
    @used_result.update_attributes(params[:new_used_result])
    @used_result.save
    redirect_to :action => 'edit_result' , :id => params[:id]
  end

  include RowMover
  movable_row :child => :result , :parent => :character , :child_class => ::Result
  
  def auth_context_id
    return params[:nation_id] if params[:nation_id] 
    return Character.find(params[:character_id]).nation_id.to_s if params[:character_id]
    return Character.find(params[:id]).nation_id.to_s if params[:id]
    return params[:character][:nation_id] if params[:character]
    return false
  end
  
  def bonus_list
    if params[:nation_id]
      c_list = Character.find(:all , 
                              :conditions => ["nation_id = ?", params[:nation_id]])
    else
      c_list = Character.find(:all)
    end
    @characters = c_list.sort_by{|c|c.character_no}
  end
  def acquired_idresses
    @character = Character.find(params[:id] , :include => :acquired_idresses )
    @acquired_idresses = @character.acquired_idresses
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @acquired_idresses }
      format.js {render :json => @acquired_idresses , :callback => params[:callback] }
    end
  end
  
  def append_acquired_idress
    acquired_idress = JobIdress.find(params[:acquired_idress_id])
    @character = Character.find(params[:id])
    @character.acquired_idresses << acquired_idress
    redirect_to acquired_idresses_character_path(@character) , :character_id => params[:character_id]
  end
  
  def remove_acquired_idress
    character = Character.find(params[:id])
    acquired_idress = JobIdress.find(params[:acquired_idress_id])
    character.acquired_idresses.delete(acquired_idress)
    redirect_to acquired_idresses_character_path(params[:id])
  end
  
  def edit_note_for_acquirement
    @character = Character.find(params[:id])
    @idress_acquirement = IdressAcquirement.find(params[:idress_acquirement_id])
  end
  
  def update_note_for_acquirement
    @character = Character.find(params[:id])
    @idress_acquirement = IdressAcquirement.find(params[:idress_acquirement_id])
    @idress_acquirement.note = params[:idress_acquirement][:note]
    @idress_acquirement.save
    redirect_to acquired_idresses_character_path(params[:id])
  end
  private
  def insert_by_text
    @records = parse_text(params[:character_list])
    @ji_list = []
    Character.transaction do
      for rec in @records
        next unless rec
        ji = find_job_idress(rec)
        @ji_list << [rec,ji]
        character = Character.find(:first , :conditions => "character_no = #{rec[:character_no]}")
        character = Character.new unless character
        character.name = rec[:name]
        character.character_no = rec[:character_no]
        character.originator = rec[:originator]
        character.nation = Nation.find(rec[:character_no][0..1].to_i)
        character.job_idress = ji
        character.save
      end
    end
  end
  
  
end
