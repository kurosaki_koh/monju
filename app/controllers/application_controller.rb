# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  include AuthenticatedSystem 
  include Acl
#  initialize_acl
  # Pick a unique cookie name to distinguish our session data from others'
  before_filter :setup_session_key
  before_filter :set_gettext_locale
  protected

  def setup_session_key
    # Pick a unique cookie name to distinguish our session data from others'
    request.session_options[:key] = 'monju_session'
  end
  
#  init_gettext "monju"

#  after_filter :set_charset

#  def set_charset
#    headers["Content-Type"] ||= "text/html; charset=utf-8" 
#  end
  def create_soldier(character)
    soldier = SoldierNode.new
    soldier.character = character
    soldier.job_idress = character.job_idress
    soldier.enable = true
    soldier.name = character.name
    soldier.nation = character.nation
    for skill in character.job_idress.skills << character.job_idress.skill
      modification = soldier.modifications.build 
      modification.set_values(skill)
    end
    soldier
  end

  def rescue_action_in_public(exception)
    render :template => 'shared/server_error' , :layout => false , :status => 500
  end

end
