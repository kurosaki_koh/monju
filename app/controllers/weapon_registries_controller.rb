# -*- encoding: utf-8 -*-
require 'owner_account.rb'
class WeaponRegistriesController < ApplicationController
  cache_sweeper :modify_sweeper , :only => [:create , :destroy , :update]
  layout 'owner_accounts'
  
  requires_role :accountant , :only => [:create , :update  , :destroy ]
  set_login_filter
  
  include RowMover
  movable_row :child => :weapon_registry , :parent => :owner_account
  # GET /weapon_registries
  # GET /weapon_registries.xml
  def index
    if params[:turn]
      @weapon_registries = WeaponRegistry.find_all_by_owner_account_id_and_turn(params[:owner_account_id] , params[:turn])
    elsif params[:name]
      @weapon_registries = WeaponRegistry.find_all_by_owner_account_id_and_name(params[:owner_account_id] , params[:name])
    else  
      @weapon_registries = WeaponRegistry.find_all_by_owner_account_id(params[:owner_account_id])
    end
    @weapon_registries = @weapon_registries.sort_by{|wr|wr.position}

    @owner_account = OwnerAccount.find(params[:owner_account_id])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @weapon_registries }
    end
  end

  # GET /weapon_registries/1
  # GET /weapon_registries/1.xml
  def show
    @weapon_registry = WeaponRegistry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @weapon_registry }
    end
  end

  # GET /weapon_registries/new
  # GET /weapon_registries/new.xml
  def new
    @weapon_registry = WeaponRegistry.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @weapon_registry }
    end
  end

  # GET /weapon_registries/1/edit
  def edit
    @weapon_registry = WeaponRegistry.find(params[:id])
  end

  # POST /weapon_registries
  # POST /weapon_registries.xml
  def create
    @weapon_registry = WeaponRegistry.new(params[:weapon_registry])

    respond_to do |format|
      if @weapon_registry.save
        flash[:notice] = 'WeaponRegistry was successfully created.'
        format.html { redirect_to(owner_account_weapon_registries_path(params[:owner_account_id])) }
        format.xml  { render :xml => @weapon_registry, :status => :created, :location => @weapon_registry }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @weapon_registry.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /weapon_registries/1
  # PUT /weapon_registries/1.xml
  def update
    @weapon_registry = WeaponRegistry.find(params[:id])

    respond_to do |format|
      if @weapon_registry.update_attributes(params[:weapon_registry])
        flash[:notice] = 'WeaponRegistry was successfully updated.'
        format.html { redirect_to(owner_account_weapon_registries_path(params[:owner_account_id])) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @weapon_registry.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /weapon_registries/1
  # DELETE /weapon_registries/1.xml
  def destroy
    @weapon_registry = WeaponRegistry.find(params[:id])
    @weapon_registry.destroy

    respond_to do |format|
      format.html { redirect_to(owner_account_weapon_registries_path(params[:owner_account_id])) }
      format.xml  { head :ok }
    end
  end
end
