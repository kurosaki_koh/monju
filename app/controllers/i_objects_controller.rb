class IObjectsController < ApplicationController
  # GET /i_objects
  # GET /i_objects.xml
  def index
    @i_objects = IObject.find_all_by_owner_account_id(params[:owner_account_id])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @i_objects }
    end
  end

  # GET /i_objects/1
  # GET /i_objects/1.xml
  def show
    @i_objects = IObject.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @i_objects }
    end
  end

  # GET /i_objects/new
  # GET /i_objects/new.xml
  def new
    @i_objects = IObject.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @i_objects }
    end
  end

  # GET /i_objects/1/edit
  def edit
    @i_objects = IObject.find(params[:id])
  end

  # POST /i_objects
  # POST /i_objects.xml
  def create
    @i_objects = IObject.new(params[:i_objects])

    respond_to do |format|
      if @i_objects.save
        flash[:notice] = 'IObjects was successfully created.'
        format.html { redirect_to(@i_objects) }
        format.xml  { render :xml => @i_objects, :status => :created, :location => @i_objects }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @i_objects.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /i_objects/1
  # PUT /i_objects/1.xml
  def update
    @i_objects = IObject.find(params[:id])

    respond_to do |format|
      if @i_objects.update_attributes(params[:i_objects])
        flash[:notice] = 'IObjects was successfully updated.'
        format.html { redirect_to(@i_objects) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @i_objects.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /i_objects/1
  # DELETE /i_objects/1.xml
  def destroy
    @i_objects = IObject.find(params[:id])
    @i_objects.destroy

    respond_to do |format|
      format.html { redirect_to(i_objects_url) }
      format.xml  { head :ok }
    end
  end
end
