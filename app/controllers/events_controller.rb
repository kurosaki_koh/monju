class EventsController < ApplicationController
#  before_filter :login_required , :except => :last_information
  requires_role :herald , 
                :only => [:index , :list , :show , :new , :create , :edit , 
                          :update , :destroy]
                            
  requires_role :accountant , 
                :only => [:index , :list , :show , :new , :create , :edit , 
                          :update , :destroy]
                            
  set_login_filter
  
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
#  verify :method => :post, :only => [ :destroy, :create, :update ],
#         :redirect_to => { :action => :list }

  def list
    @turn = params[:turn] ? params[:turn] : Property['current_turn'].to_i
    @events = Event.find(:all , :conditions => ["turn = ?" , @turn]).sort_by{|e|e.order_no}
#    @event_pages, @events = paginate :events, :per_page => 10
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(params[:event])
    if @event.save
      flash[:notice] = 'Event was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])
    if @event.update_attributes(params[:event])
      flash[:notice] = 'Event was successfully updated.'
      redirect_to :action => 'show', :id => @event
    else
      render :action => 'edit'
    end
  end

  def destroy
    Event.find(params[:id]).destroy
    redirect_to :action => 'list'
  end

  def authorized?
    current_user.role == "admin"
  end
end
