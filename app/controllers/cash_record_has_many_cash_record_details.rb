module CashRecordHasManyCashRecordDetails

  def self.append_features(base)
    super
    base.before_filter :prepare_cash_record_cash_record_details_variables
  end

protected
  def prepare_cash_record_cash_record_details_variables
    case @action_name
    when /\Acash_record_cash_record_details_/
      @ajax_prefix        = params[:ajax] ? "ajax_" : nil
      @render_method      = params[:ajax] ? :partial : :action
      @action_prefix      = $&
      @original_action_name = @action_name
      @real_action_name   = $'
      @ajax_action_name   = "#{ @ajax_prefix }#{ @real_action_name }"
      @reflection         = CashRecord.reflections[:cash_record_details] or
          raise RuntimeError, "Association Error: '%s' should contain '%s' reflection." % [CashRecord, :cash_record_details]

      case @real_action_name
      when 'ajax_position'
        @belongs_to = CashRecord.find(params[:id])

      when 'list', 'new', 'sort'
        @belongs_to = CashRecord.find(params[:id])
        @cash_record_details = @belongs_to.cash_record_details

      when 'confirm_create', 'create'
        @belongs_to = CashRecord.find(params[:id])
        @cash_record_detail = @belongs_to.cash_record_details.build(params[:cash_record_detail])

      when 'show', 'edit', 'confirm_destroy', 'confirm_destroy', 'destroy'
        @cash_record_detail = CashRecordDetail.find(params[:id])
        @belongs_to = @cash_record_detail.cash_record

      when 'confirm_update', 'update'
        @cash_record_detail = CashRecordDetail.find(params[:id])
        @belongs_to = @cash_record_detail.cash_record
        @cash_record_detail.attributes = params[:cash_record_detail]
        @cash_record_detail.cash_record_id = @belongs_to.id

      else
      end
    end
  end

public
  def cash_record_cash_record_details_ajax_position
    CashRecordDetail.transaction do
      params[:position].each_with_index do |arg, i|
        id  = arg.to_s.sub(/\A.*?(\d+)\Z/){$1}.to_i
        sql = "UPDATE cash_record_details SET position = %d WHERE id = %d" % [i+1, id]
        CashRecordDetail.connection.execute(sql)
      end
    end
    @cash_record_details = @belongs_to.cash_record_details
    render :action=>"ajax_position"
  end

  def cash_record_cash_record_details_sort
    render @render_method=>@ajax_action_name
  end

  def cash_record_cash_record_details_list
    render @render_method=>@ajax_action_name
  end

  def cash_record_cash_record_details_new
    @cash_record_detail = @belongs_to.cash_record_details.build
    render @render_method=>@ajax_action_name
  end

  def cash_record_cash_record_details_show
    render @render_method=>@ajax_action_name
  end

  def cash_record_cash_record_details_edit
    render @render_method=>@ajax_action_name
  end

  def cash_record_cash_record_details_confirm_create
    if @cash_record_detail.valid?
      render @render_method=>@ajax_action_name
    else
      render @render_method=>"#{@ajax_prefix}new"
    end
  end

  def cash_record_cash_record_details_create
    return render(@render_method=>"#{@ajax_prefix}new") if params[:btn_cancel]

    if @cash_record_detail.save
      if @ajax_prefix
        @cash_record_details = @belongs_to.cash_record_details
        render :action=>@ajax_action_name
      else
        flash[:notice] = localized_class_name(:cash_record_detail) + localize(:command, :successfully_created)
        redirect_to :action=>associated_action(:list), :id=>@belongs_to
      end
    else
      if @ajax_prefix
        render :update do |page|
          message = localized_error_messages_for(@cash_record_detail) || localize(:message, :ajax_error)
          page.replace_html "notice", message
        end
      else
        render @render_method=>"#{@ajax_prefix}new"
      end
    end
  end

  def cash_record_cash_record_details_confirm_update
    if @cash_record_detail.valid?
      render @render_method=>@ajax_action_name
    else
      render @render_method=>"#{@ajax_prefix}edit"
    end
  end

  def cash_record_cash_record_details_update
    if params[:btn_cancel]
      render @render_method=>"#{@ajax_prefix}edit"
    elsif @cash_record_detail.save
      flash[:notice] = localized_class_name(:cash_record_detail) + localize(:command, :successfully_updated)
      redirect_to :action=>associated_action(:list), :id=>@belongs_to
    else
      render @render_method=>"#{@ajax_prefix}edit"
    end
  end

  def cash_record_cash_record_details_confirm_destroy
    render @render_method=>"#{@ajax_prefix}confirm_destroy"
  end

  def cash_record_cash_record_details_destroy
    @belongs_to.cash_record_details.destroy(params[:id]) unless params[:btn_cancel]
    if @ajax_prefix
      render :action=>@ajax_action_name
    else
      redirect_to :action=>associated_action(:list), :id=>@belongs_to
    end
  end
end
