# -*- encoding: utf-8 -*-

class UnitRegistriesController < ApplicationController
  include TaigenUtility

  requires_role :monju_maintainer, :only => [:create, :update, :destroy, :rebuild, :disable]
#  requires_role OwnerRole , :only => [:create , :update  , :destroy  , :rebuild , :disable]
  set_login_filter


# GET /unit_registries
# GET /unit_registries.js
  def index
    @unit_registries = if params[:q]
                         params[:q]
                         name = OwnerAccount.arel_table[:name]
                         number = OwnerAccount.arel_table[:number]
                         type = UnitRegistry.arel_table[:type]

                         keyword = '%'+params[:q]+'%'
                         UnitRegistry.joins(:owner_account).where(number.matches(keyword).or(name.matches(keyword))).where(type.not_eq('IncludeRegistry')).
                             map { |i| {label: "#{i.owner_account.number}:#{i.name}", value: i.owner_account.number} }

                         #

                       else
                         @owner_account = if params[:number]
                                            OwnerAccount.find_by_number(params[:number])
                                          elsif params[:q]

                                          else
                                            OwnerAccount.find(params[:owner_account_id])
                                          end
                         raise ActiveRecord::RecordNotFound if @owner_account.nil?

                         @owner_account.unit_registries
                       end

    respond_to do |format|
      format.html # index.html.erb
      format.js { render json: @unit_registries, callback: params[:callback] }
    end
  end

# GET /unit_registries/1
# GET /unit_registries/1.json
  def show
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @unit_registry = UnitRegistry.find(params[:id])

    @command_resources = @unit_registry.object_definition.parsed_command_resources

    respond_to do |format|
      format.html # show.html.erb
      format.js { render json: @unit_registry }
    end
  end

# GET /unit_registries/new
# GET /unit_registries/new.json
  def new
    @unit_registry = UnitRegistry.new
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @unit_registry.owner_account = @owner_account
    @main_units = @owner_account.unit_registries.single.to_a
    @members = []
    @unit_and_command_resources = unit_and_command_resources(@main_units)
    @main_unit = nil
    @includes = []
    @definition = nil
    @source = nil
    respond_to do |format|
      format.html # new.html.erb
      format.js { render json: @unit_registry }
    end
  end

# GET /unit_registries/1/edit
  def edit
    @owner_account = OwnerAccount.find(params[:owner_account_id])
    @unit_registry = UnitRegistry.find(params[:id])

    if @unit_registry.instance_of? IncludeRegistry
      @main_units = @owner_account.unit_registries.single.to_a
      @definition = @unit_registry.definition
      @source = @unit_registry.source
      @main_unit = @owner_account.unit_registries.find_by_name(@definition[:main_member][:name])
      @members = @definition[:members].map{|m|
        OwnerAccount.find_by_number(m[:character_no]).unit_registries.find_by_name(m[:name])
      }
      @includes = @definition[:includes]
      @unit_and_command_resources = unit_and_command_resources(@main_units + @members)
    end
  end

# POST /unit_registries
# POST /unit_registries.json
  RegistryClasses = [ConvertSource, AceUnitRegistry, IncludeRegistry].inject({}) { |hash, klass| hash.update({klass.name => klass}) }

  def create
    klass = RegistryClasses[params[:unit_registry][:type]]


    if klass
      @unit_registry = klass.new(params[:unit_registry])
      @unit_registry.source = if klass == IncludeRegistry
                                JSON.parse(params[:include_def])
                              else
                                trim_source(params[:source])
                              end
    end
    respond_to do |format|
      if @unit_registry && @unit_registry.save
        puts @unit_registry.inspect
        format.html { redirect_to owner_account_unit_registries_path(@unit_registry.owner_account), notice: 'ユニット定義を登録しました。' }
        format.js { render json: @unit_registry, status: :created, location: @unit_registry }
      else
        format.html { render action: "new" }
        format.js { render json: @unit_registry.errors, status: :unprocessable_entity }
      end
    end
  end

# PUT /unit_registries/1
# PUT /unit_registries/1.json
  def update

    @unit_registry = UnitRegistry.find(params[:id])
    klass = RegistryClasses[params[:unit_registry][:type]]
    bef = @unit_registry.source
    if klass
      @unit_registry.source = if klass == IncludeRegistry
                                JSON.parse(params[:include_def])
                              else
                                trim_source(params[:source])
                              end
    end
    @unit_registry.name = params[:unit_registry][:name]

    respond_to do |format|
      if @unit_registry.update_attributes(params[:unit_registry])
        format.html { redirect_to owner_account_unit_registry_path(@unit_registry.owner_account, @unit_registry), notice: 'ユニット定義を更新しました。' }
        format.js { head :no_content }
      else
        format.html { render action: "edit" }
        format.js { render json: @unit_registry.errors, status: :unprocessable_entity }
      end
    end
  end

# DELETE /unit_registries/1
# DELETE /unit_registries/1.json
  def destroy
    @unit_registry = UnitRegistry.find(params[:id])
    @unit_registry.destroy

    respond_to do |format|
      format.html { redirect_to owner_account_unit_registries_url(params[:owner_account_id]) }
      format.js { head :no_content }
    end
  end

  def rebuild
    @unit_registry = UnitRegistry.find(params[:id])
    @owner_account = OwnerAccount.find(params[:owner_account_id])

    @unit_registry.rebuild_command_resources

    redirect_to owner_account_unit_registries_path(@unit_registry.owner_account), notice: "ユニット定義「#{@unit_registry.name}」を基に、コマンドリソースを再構築しました。"
  end

  def disable
    @unit_registry = UnitRegistry.find(params[:id])
    @owner_account = OwnerAccount.find(params[:owner_account_id])

    @unit_registry.disable_command_resources

    redirect_to owner_account_unit_registries_path(@unit_registry.owner_account), notice: "ユニット定義「#{@unit_registry.name}」のコマンドリソースを無効化しました。"
  end

  private
  def unit_and_command_resources(units)

    result = {}
    for u in units
      result[u.to_format] = u.available_command_resources.try(:map){|cr|cr.to_hash}  || []
    end
    result
  end

end
