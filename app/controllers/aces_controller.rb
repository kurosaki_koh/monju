# -*- encoding: utf-8 -*-
class AcesController < ApplicationController
  requires_role :item_officer ,
    :only => [:create , :destroy , :edit , :update , :promote , :edit_promotion ,
              :items_for_wiki
    ]
  set_login_filter
  # GET /aces
  # GET /aces.xml
  def index
    @aces = Ace.find(:all).sort_by{|ace|ace.hurigana.to_s}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @aces }
    end
  end

  # GET /aces/1
  # GET /aces/1.xml
  def show
    @ace = Ace.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ace }
    end
  end

  # GET /aces/new
  # GET /aces/new.xml
  def new
    @ace = Ace.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ace }
    end
  end

  # GET /aces/1/edit
  def edit
    @ace = Ace.find(params[:id])
  end

  # POST /aces
  # POST /aces.xml
  def create
    @ace = Ace.new(params[:ace])
    flag = nil
    Ace.transaction do
      defs = params[:definition]
      if !defs.nil? && !defs.empty?
        odef = ObjectDefinition.new(:name => @ace.name ,:hurigana =>  @ace.hurigana , :definition => defs)
        odef.zukan_definition_url = params[:zukan_definition_url]
        odef.object_type = 'ＡＣＥ'
        odef.save
        @ace.object_definition = odef
      end
      flag = @ace.save
    end
    respond_to do |format|
      if flag
        flash[:notice] = 'ACE 用口座を開設しました。'
        format.html { redirect_to(@ace) }
        format.xml  { render :xml => @ace, :status => :created, :location => @ace }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ace.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /aces/1
  # PUT /aces/1.xml
  def update
    @ace = Ace.find(params[:id])

    defs = params[:definition]
    if !defs.nil? && !defs.empty?
      
    end
    respond_to do |format|
      if @ace.update_attributes(params[:ace])
        flash[:notice] = 'ACE 登録を更新しました。'
        format.html { redirect_to(@ace) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ace.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /aces/1
  # DELETE /aces/1.xml
  def destroy
    @ace = Ace.find(params[:id])
    @ace.destroy

    respond_to do |format|
      format.html { redirect_to(aces_url) }
      format.xml  { head :ok }
    end
  end
  
  def edit_promotion
    @ace = Ace.find(params[:id])
    @object_definition = @ace.object_definition
  end
  
  def promote
    @ace = Ace.find(params[:id])
#"    flag = @ace.update_attributes(params[:ace])
    odef_param = params[:object_definition]
    defs = params[:object_definition][:definition]
    flag = true
    Ace.transaction do
      odef = ObjectDefinition.find_by_name(odef_param[:name])
      odef = ObjectDefinition.new(odef_param) unless odef
      ObjectDefinition.transaction do
        odef.zukan_definition_url = params[:zukan_definition_url] unless params[:zukan_definition_url].blank?
        odef.object_type = 'ＡＣＥ'
        flag = flag && odef.save
      end
      @ace.object_definition = odef
      flag = flag && @ace.save
    end
    respond_to do |format|
      if flag
        flash[:notice] = 'プロモーションを行いました。'
        format.html { redirect_to(@ace) }
        format.xml  { render :xml => @ace, :status => :created, :location => @ace }
      else
        flash[:notice] = 'プロモート処理が失敗しました。'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ace.errors, :status => :unprocessable_entity }
      end
    end
  end    

  def items
    @aces = Ace.find(:all)
    @registries = @aces.inject([]){|list , c|
      list += c.owner_account.object_registries
    }.sort_by{|item| [item.created_at , item.name]}
  end

  def items_for_wiki
    items()
  end
end
