# -*- encoding: utf-8 -*-
class ObjectDefinitionsController < ApplicationController
  requires_role :item_officer , :only => [:create , :update  , :destroy ]
  set_login_filter

  # GET /object_definitions
  # GET /object_definitions.xml
  def index
    @object_definitions = CommonDefinition.order(:hurigana)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @object_definitions }
    end
  end

  # GET /object_definitions/1
  # GET /object_definitions/1.xml
  def show
    @object_definition = if params[:name]
      ObjectDefinition.find_by_name(params[:name])
    else
      ObjectDefinition.find(params[:id])
    end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @object_definition }
      format.js   {
        json_data = @object_definition.attributes
        json_data[:command_resources] = @object_definition.command_resources if @object_definition.is_idress3?
        render :json => json_data , :callback => params[:callback]
      }
    end
  end

  # GET /object_definitions/new
  # GET /object_definitions/new.xml
  def new
    @object_definition = ObjectDefinition.new
    @object_definition.name = params[:name] if params[:name] && !params[:name].empty?
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @object_definition }
    end
  end

  # GET /object_definitions/1/edit
  def edit
    @object_definition = ObjectDefinition.find(params[:id])
  end

  # POST /object_definitions
  # POST /object_definitions.xml
  def create
    @object_definition = CommonDefinition.new(params[:object_definition])

    respond_to do |format|
      if @object_definition.save
        flash[:notice] = 'オブジェクト定義を登録しました。'
        format.html { redirect_to(object_definition_path(@object_definition)) }
        format.xml  { render :xml => @object_definition, :status => :created, :location => @object_definition }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @object_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /object_definitions/1
  # PUT /object_definitions/1.xml
  def update
    @object_definition = ObjectDefinition.find(params[:id])

    respond_to do |format|
      if @object_definition.update_attributes(params[:object_definition] || params[:common_definition])
        flash[:notice] = 'オブジェクト定義を更新しました。'
        format.html { redirect_to(@object_definition) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @object_definition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /object_definitions/1
  # DELETE /object_definitions/1.xml
  def destroy
    @object_definition = ObjectDefinition.find(params[:id])
    @object_definition.destroy

    respond_to do |format|
      format.html { redirect_to(object_definitions_url) }
      format.xml  { head :ok }
    end
  end
  
  def owners
    @object_definition = ObjectDefinition.find(params[:id])
  end
  
  def owners_for_wiki
    @object_definition = ObjectDefinition.find(params[:id])
  end
  
  def owners_by_name
    @object_definition = ObjectDefinition.find_by_name(params[:name],:include => :object_registries)
    render :action => :owners
  end
end
