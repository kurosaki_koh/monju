# -*- encoding: utf-8 -*-
class PackagedRegisterController < ApplicationController
  layout 'owner_accounts'
  cache_sweeper :modify_sweeper, :only => [:register_result]
  requires_role :accountant, :only => [:register_result]
  requires_role :herald, :only => [:register_result]
  set_login_filter

  include IdressSearcher
  include CommandResourceParser

  class SubmitFormParseResult
    attr :text, true
    attr :matched, true
    attr :character_no, true
    attr :left_str, true
    attr :name, true
    attr :db_name, true
    attr :originator, true
    attr :mile, true
    attr :note, true
    attr :url, true
    attr :turn, true
    attr :event_name, true

    attr_accessor :idress, :ace, :idress_not_found

    alias :matched? matched

    def initialize(format = 'originator')
      @format = format
    end

    def parse_and_set(str)
      str.strip!

      case str
        when /^(h)?ttp:\/\//
          str = 'h' + str unless $1
          self.url = str
        when /^\-?(\d+)$/
          case @format
            when 'originator'
              self.originator = str.to_i
            when 'mile', 'mile_with_event_name'
              self.mile = str.to_i
            when 'originator_and_mile'
              unless @originator_consumed
                self.originator = str.to_i
                @originator_consumed = true
              else
                self.mile = str.to_i
              end
          end
        else
          self.note = str
      end
    end

    def parse_after_note(cols)
      case @format
        when 'mile_with_event_name'
          @event_name = cols.shift
        when 'idress'
#          @idress = cols.shift
      end
      for col in cols
        parse_and_set(col)
      end
    end

    def parse(line)
      line.strip!
      self.text = line.strip

      if line =~ /^(\d{2}-\d{5}-\d{2}|\d{1,2}|\-|－)[：:]([^：:]+)([：:](.+))?/
        @character_no = $1
        @name = $2

        if $4
          left_str = $4
          cols = left_str.split('：')
          parse_after_note(cols)
        end
        @matched = true
      else
        cols = line.split('：')
        if cols.size >= 2
          @matched = true
          @character_no = cols[0]
          @name = cols[1]
          cols = cols[2,-1]
          parse_after_note(cols) if cols
        else
          @matched = false
        end
      end
    end
  end

  def parse_register_result(submited, admin_flag = false)
    parse_results = []
    db_not_found = []
    register_data = []
    submited.each_line do |line|
      parse_result = SubmitFormParseResult.new(params[:format_type])
      parse_result.parse(line)
      parse_results << parse_result
      next unless parse_result.matched?

      c = find_target(parse_result.character_no, parse_result.name)

      if c # && (admin_flag || c.nation_id == auth_context_id.to_i)
        parse_result.db_name = c.name
        register_data << parse_result
      else
        db_not_found << parse_result
        parse_result.matched = false
      end
    end
    return parse_results, register_data, db_not_found
  end


  def confirm_register_result
    @event_name = params[:event_name]
    @magic_item = params[:magic_item]
    @not_yet = params[:not_yet] ? "1" : "0"
    @reference_url = params[:reference_url]

    @parse_results, @register_data, @db_not_found = parse_register_result(params[:submit_format], params[:id] == 'admin')

    @parse_results.find_all { |r| r.matched? }.each { |r|
      r.originator = params[:default_originator] unless r.originator
      r.mile = params[:default_mile] unless r.mile
      r.url = params[:reference_url] if params[:reference_url] && !params[:reference_url].empty?
      r.turn = params[:turn].to_i > 0 ? params[:turn].to_i : Property[:current_turn].to_i
      r.idress = params[:magic_item] #unless params[:magic_item].blank?
      r.idress_not_found = !search_idress(r.idress)
      if params[:format_type] == 'idress'
        r.note = @event_name if r.note.blank? && !@event_name.blank?
      end
    }

    #null check
    @null_check = true
    for reg in @register_data.sort_by { |data| data.character_no }
      if params[:format_type] =~ /mile/
        @null_check = @null_check && !(reg.mile.nil? || (reg.mile.class != Fixnum && reg.mile.empty?))
      elsif params[:format_type] =~ /originator/
        @null_check = @null_check && !(reg.originator.nil? || (reg.originator.class != Fixnum && reg.originator.empty?))
      end
    end
  end


  def find_target(key, name)
    case key
      when /^\d{2}-\d{5}-\d{2}$/
        Character.find_by_character_no(key)
      when /^\d{1,2}$/
        Nation.find(key)
      when /^(\-|－)$/
        Organization.find_by_name(name)
      else
        Ace.find_by_name(name)
    end
  end

  def register_originator_result(ch, result)
    ::Result.transaction do
      new_res = ch.results.create
      new_res.originator = result[:originator]
      new_res.note = result[:note]
      new_res.event_name = params[:event_name]
      new_res.magic_item = params[:magic_item]
      new_res.url = result[:url]
      new_res.not_yet = params[:not_yet] == "1"
      new_res.save
    end
  end

  def register_mile_result(ch, result)
    MileChange.transaction do
      new_mile = ch.owner_account.mile_changes.create
      new_mile.mile = result[:mile]
      new_mile.note = result[:note]
      new_mile.reason = (result[:event_name] && !result[:event_name].empty?) ? result[:event_name] : params[:event_name]
      new_mile.ground_url = result[:url]
      new_mile.turn = params[:turn]
      new_mile.save
    end
  end


  def register_idress_result(ch, result)
    odef = search_idress(result[:idress])
    return unless odef

    ch.owner_account.add_object_definition(odef , result)
  end

  def register_result
    results = params[:results]

    ::Result.transaction do
      @registered_data = []
      for key in results.keys.sort
        result = results[key]
        row = [result[:character_no]]
        row << result[:originator] if result[:originator]
        row << result[:mile] if result[:mile]
        row << result[:idress] if result[:idress]
        row += [result[:note], result[:url]]
        @registered_data << row.join(',')
        ch = find_target(result[:character_no], result[:name])

        if ch && result[:originator]
          register_originator_result(ch, result)  if has_role?(:herald)
        elsif ch && result[:mile]
          register_mile_result(ch, result)  if has_role?(:herald) ||  has_role?(:accountant)
        elsif ch && result[:idress]
          register_idress_result(ch, result) if has_role?(:herald)
        end
      end
      redirect_to :action => :register_form
    end
  end
end
