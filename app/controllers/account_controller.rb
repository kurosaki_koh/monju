# -*- encoding: utf-8 -*-
class AccountController < ApplicationController
  # Be sure to include AuthenticationSystem in Application Controller instead
  # If you want "remember me" functionality, add this before_filter to Application Controller
  before_filter :login_from_cookie
  before_filter :login_required , :only => ['signup']

  requires_role :admin , :only => [:edit , :edit_role,:add_role, :remove_role,:delete , :update_user]
  set_login_filter
  
  layout 'admin'
  # say something nice, you goof!  something sweet.
  def index
    redirect_to(:action => 'signup') unless logged_in? || User.count > 0
  end

  def login
    unless request.post?
      unless using_open_id?
        store_location(params[:refferer]) if params[:refferer]
        return 
      end
    end
    if using_open_id?
      open_id_authentication
      return 
    end

    self.current_user = User.authenticate(params[:login], params[:password])
    if logged_in?
      if params[:remember_me] == "1"
        self.current_user.remember_me
        cookies[:auth_token] = { :value => self.current_user.remember_token , :expires => self.current_user.remember_token_expires_at }
      end
      flash[:notice] = "ログインしました。ようこそ#{self.current_user.login}さん。"
      redirect_back_or_default(:controller => 'nations', :action => 'top') # , :id=> self.current_user.role == 'admin')
    else
      flash[:notice] = "ユーザIDもしくはパスワードが違います。"
    end
  end

  def signup
    @user = User.new(params[:user])
    return unless request.post?
    @user.save!
    self.current_user = @user
    redirect_back_or_default(:controller => :nations, :action => :top)
    flash[:notice] = "アカウント登録を行いました。権限が設定されるまで、暫くお待ち下さい。"
  rescue ActiveRecord::RecordInvalid
    render :action => 'signup'
  end
  
  def logout
    self.current_user.forget_me if logged_in?
    cookies.delete :auth_token
    reset_session
    flash[:notice] = "ログアウトしました。"
    redirect_back_or_default(:controller => :nations, :action => :top )
  end

  def authorized?
    if action_name == 'edit'
      return params[:id].to_i == current_user.id || is_admin?
    end
    return true if current_user.role == 'admin' || is_admin?
  end
  
  def users
    @users = User.all(:order => 'id')
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def edit_roles
    @user = User.find(params[:id])
  end
  def update_user
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = 'ユーザ設定を更新しました。'
        format.html { redirect_to(:back) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    
    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end

  def remove_role
    @user = User.find(params[:id])
    @role = @user.roles.find(params[:role_id])
    @user.roles.delete(@role)
    redirect_to :action => :edit_roles , :id => @user
  end
  
  def add_role
    @user = User.find(params[:id])
    @role = Role.find(params[:role_id])
    @user.roles << @role
    redirect_to :action => :edit_roles , :id => @user
  end
  
  def authorized?(nid=nil)
    if action_name == 'edit' || action_name == 'update_user'  then
      return do_authorize(nid) || (current_user != :false && curret_user == User.find(params[:id]))
    end
    do_authorize(nid)
  end
  
  def open_id_authentication
    authenticate_with_open_id do |result, identity_url|
      if result.successful?
        if self.current_user = User.find_by_identity_url(identity_url)
          flash[:notice] = "ログインしました。ようこそ#{current_user.name || current_user.login}さん。"
          cookies[:openid_url]={:value => identity_url,:expires => 2.weeks.from_now}
          redirect_back_or_default(:controller => 'nations', :action => 'top') 
        else
          failed_login "Sorry, no user by that identity URL exists (#{identity_url})"
        end
      else
        failed_login result.message
      end
    end
  end
    
  private

  def failed_login(message)
    flash[:error] = message
    redirect_to(:action => :login)
  end
end

