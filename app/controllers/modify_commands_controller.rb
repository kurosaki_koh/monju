class ModifyCommandsController < ApplicationController
  layout 'owner_accounts'
  # GET /modify_commands
  # GET /modify_commands.xml
  def index
    query = ModifyCommand
    if params[:owner_account_id]
      query = ModifyCommand.where(owner_account_id: params[:owner_account_id] )
      @owner_account = OwnerAccount.find(params[:owner_account_id])
    end

    query = query.where(:target_type => params[:target_type] ) if params[:target_type]
    @modify_commands = query.order('created_at desc').paginate(:page => params[:page] )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @modify_commands }
    end
  end
  
  # GET /modify_commands/1
  # GET /modify_commands/1.xml
  def show
    @modify_command = ModifyCommand.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @modify_command }
    end
  end
  
  # GET /modify_commands/new
  # GET /modify_commands/new.xml
  def new
    @modify_command = ModifyCommand.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @modify_command }
    end
  end
  
  # GET /modify_commands/1/edit
  def edit
    @modify_command = ModifyCommand.find(params[:id])
  end
  
  # POST /modify_commands
  # POST /modify_commands.xml
  def create
    @modify_command = ModifyCommand.new(params[:modify_command])
    
    respond_to do |format|
      if @modify_command.save
        flash[:notice] = 'ModifyCommand was successfully created.'
        format.html { redirect_to(@modify_command) }
        format.xml  { render :xml => @modify_command, :status => :created, :location => @modify_command }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @modify_command.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # PUT /modify_commands/1
  # PUT /modify_commands/1.xml
  def update
    @modify_command = ModifyCommand.find(params[:id])
    
    respond_to do |format|
      if @modify_command.update_attributes(params[:modify_command])
        flash[:notice] = 'ModifyCommand was successfully updated.'
        format.html { redirect_to(@modify_command) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @modify_command.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # DELETE /modify_commands/1
  # DELETE /modify_commands/1.xml
  def destroy
    @modify_command = ModifyCommand.find(params[:id])
    @modify_command.destroy
    
    respond_to do |format|
      format.html { redirect_to(modify_commands_url) }
      format.xml  { head :ok }
    end
  end
end
