# -*- encoding: utf-8 -*-
class NpcNameListController < ApplicationController
  def show

  end

  def edit
    @nation = Nation.find(params[:id])
    @npc_type = case @nation.belong
    when :republic
      '猫・犬士'
    else :empire
      '犬・猫士'
    end
  end

  def update
    @nation = Nation.find(params[:id])
    @nation.npc_names = params[:nation][:npc_names]
    @nation.save

    redirect_to @nation
  end
end
