class SpecialGenerator < ScaffoldGenerator
  attr_accessor :model_class, :primary_key, :model_table_name, :charset,
                :column_position, :column_enabled, :order_field, :acts_as_tree,
                :target_reflections, :view_path, :enabled_tasks

  default_options :recursively=>false

  def initialize(runtime_args, runtime_options = {})
    runtime_args << runtime_args.last if strip_options!(runtime_args.dup).size == 1
    super

    @model_class        = create_model_class
    @model_table_name   = @model_class.table_name
    @primary_key        = @model_class.primary_key
    @column_position  = @model_class.columns_hash["position"].name rescue nil
    @column_enabled     = @model_class.columns_hash["enabled"].name rescue nil
    @view_path          = File.join('app/views', controller_class_path, controller_file_name)
    @order_field        = "#{model_table_name}.#{@primary_key}"
    @order_field        = "#{model_table_name}.position, #{order_field}" if column_position
    @enabled_tasks      = calculate_enabled_tasks
    @acts_as_tree       = guess_acts_as_tree
    @charset            = guess_charset
    @target_reflections =
      model_class.reflect_on_all_associations(:has_many) +
      model_class.reflect_on_all_associations(:has_one)

    @target_reflections.sort!{|a,b| a.name.to_s <=> b.name.to_s}
  end

  def manifest
    record do |m|
      check_class_collisions(m)
      enabled_tasks.each do |task|
        send("prepare_#{task}", m)
      end
    end
  end

  def check_class_collisions (m)
    m.class_collisions "#{controller_class_name}Controller", "#{controller_class_name}ControllerTest", "#{controller_class_name}Helper"
  end

  def prepare_localize (m)
    # Localized message file (depends on $KCODE)
    m.template File.join('localized_messages', @charset),
               File.join('config', 'localize.yml'),
               :collision => :skip

    m.directory 'db/localized'
    m.template 'localized_model.rhtml', File.join('db/localized', "#{model_table_name}.yml"),
                :collision => :skip
  end

  def prepare_controller (m)
    m.directory File.join('app/controllers', controller_class_path)

    # Controller class
    m.template 'controller.rb',
                File.join('app/controllers', controller_class_path, "#{controller_file_name}_controller.rb"),
                :assigns => { :base_controller => options[:base_controller] || 'ApplicationController' }
  end

  def prepare_views (m)
    m.directory view_path

    # Ajax views
    %w(
        _ajax_list.rhtml         _ajax_list_header.rhtml  _ajaxloading.rhtml  ajax_list.rhtml
        _ajax_list_footer.rhtml  _ajax_new.rhtml
        _ajax_list_main.rhtml    _ajax_list_sort.rhtml    _ajax_list_record.rhtml
        ajax_create.rjs          ajax_destroy.rjs         ajax_enabled.rjs         ajax_position.rjs
        ajax_acts_as_tree_select.rjs
    ).each do |view|
      m.template File.join("ajax/views", view), File.join(view_path, view),
                 {:assigns => {:column_position=>column_position, :model_class=>@model_class,
                               :controller_class_path=>controller_class_path,
                               :controller_file_name=>controller_file_name}}
    end

    # Normal views
    %w(
        _list.rhtml  _list_footer.rhtml  _list_header.rhtml  list.rhtml _title.rhtml sort.rhtml
        _associations.rhtml
    ).each do |view|
      m.template File.join("views", view), File.join(view_path, view)
    end

    # Scaffolded views.
    %w(list show new edit confirm_create confirm_update confirm_destroy).each do |action|
      m.template "views/#{action}.rhtml",
                 File.join('app/views', controller_class_path, controller_file_name, "#{action}.rhtml"),
                 :assigns => { :action => action }
    end
  end

  def prepare_test (m)
    m.directory File.join('test/functional', controller_class_path)
    m.template 'ajax/functional_test.rb',
                File.join('test/functional', controller_class_path, "#{controller_file_name}_controller_test.rb")
  end

  def prepare_layout (m)
    m.directory 'app/views/layouts'
    m.template 'layout.rhtml',  "app/views/layouts/#{controller_file_name}.rhtml"
  end

  def prepare_stylesheets (m)
    m.directory "public/stylesheets"
    m.template 'style.css',     "public/stylesheets/scaffold.css"
  end

  def prepare_helpers (m)
    m.directory File.join('app/helpers', controller_class_path)
    m.directory File.join('app/helpers/localized')

    # Helpers
    m.template 'helpers/helper.rb',
               File.join('app/helpers', controller_class_path, "#{controller_file_name}_helper.rb")

    %w( localize class_method field_order ) .each do |helper|
      filename = "#{helper}_helper.rb"
      m.template File.join("helpers", filename), File.join('app/helpers', filename)
    end

    # Localized settings
    %w( message model view_property ) .each do |helper|
      filename = "helpers/localized/#{helper}.rb"
      m.file  filename, File.join('app', filename)
    end

    # IPE Helpers
    %w( ipe ipe_for ) .each do |helper|
      filename = "#{helper}_helper.rb"
      m.file  File.join("ajax", filename), File.join('app/helpers', filename)
    end

    # Associated Helpers
    %w( associated associated_action ) .each do |helper|
      filename = "#{helper}_helper.rb"
      m.file  File.join("associated", filename), File.join('app/helpers', filename)
    end
  end

  def prepare_images (m)
    m.directory File.join('public/images/icons')

    %w( close.png ).each do |filename|
      m.file  File.join("ajax/images/icons", filename), File.join('public/images/icons', filename)
    end
  end

  def prepare_associations (m)
    target_reflections.each do |reflection|
      file_name   = "#{controller_file_name}_#{reflection.macro}_#{reflection.name}.rb"
      module_name = "#{controller_class_name}#{reflection.macro.to_s.camelize}#{reflection.name.to_s.camelize}"
      belongs_to  = reflection.klass.reflect_on_all_associations(:belongs_to).find{|ref| ref.class_name == @model_class.to_s} or
        raise "Association Error: %s should belong to %s" % [reflection.class_name, @model_class]
      m.template "associated/#{reflection.macro}_helper.rb",
      File.join('app/controllers', controller_class_path, file_name),
                 {:assigns => {
                   :module_name       => module_name,
                   :reflection        => reflection,
                   :belongs_to        => belongs_to,
                   :child_name        => belongs_to.active_record.to_s.gsub(/::/, '_').underscore,
                   :child_plural_name => belongs_to.active_record.to_s.gsub(/::/, '_').underscore.pluralize,
                 }}
    end
  end

  def prepare_model_views (m)
    view_model_path = "app/views/models/#{model_table_name}"
    m.directory view_model_path

    # Scaffolded Views for Model
    %w( _show1 _show2 _edit1 _edit2 _hidden ).each do |filename|
      filename = "#{filename}.rhtml"
      m.template File.join("views/models", filename), File.join(view_model_path, filename)
    end
  end


  def prepare_javascripts (m)
    %w( ajaxautospinner ).each do |filename|
      filename = filename + ".js"
      m.file  File.join('ajax', filename),  File.join('public/javascripts', filename)
    end
  end

  def prepare_common_views (m)
    # for flash message
    m.directory 'app/views/common'
    m.file "plain/common/_notice.rhtml", 'app/views/common/_notice.rhtml', :collision => :skip
    m.file "plain/common/_error.rhtml" , 'app/views/common/_error.rhtml' , :collision => :skip
  end

  def prepare_recursively (m)
    target_reflections.each do |ref|
      model_name      = ref.klass.to_s
      controller_name = "#{controller_class_nesting}::#{model_name}".sub(/\A::/, '')
      m.dependency "special", [model_name, controller_name], :recursive=>false
    end
  end

  protected
    # Override with your own usage banner.
    def banner
      "Usage: #{$0} special [options] ModelName [ControllerName]"
    end

    def add_options!(opt)
      opt.on('-r', "Generate associations also") { |value| options[:recursively] = true }
      opt.on('--[no-]belongs-to', 'Hide belongs_to field or not')  { |value| options[:belongs_to] = value }
      opt.on('--base-controller <name>', 'Use <name> as base class') { |value| options[:base_controller] = value }

      default_tasks.each do |task|
        opt.on("--no-#{task}", "Skip generator for #{task}") { |value| options[task]  = value }
      end
      default_tasks.each do |task|
        opt.on("--only-#{task}", "Generate only #{task}")    { |value| options[:only] = task.to_s.intern }
      end
    end

    def model_instance
      @model_class.new rescue super
    end

    def guess_acts_as_tree
      @model_class.columns_hash['parent_id'] and @model_class.reflections[:children].macro == :has_many
    rescue
      false
    end

    def guess_charset
      case $KCODE.to_s.downcase[0]
      when ?e; 'EUC-JP'
      when ?s; 'Shift_JIS'
      when ?j; 'ISO-2022-JP'
      when ?u; 'UTF-8'
      else   ; 'ISO-8859-1'
      end
    end

    def strip_options!(args)
      parse!(args, {})
      return args
    end

    def default_tasks
      [:localize,
       :controller,
       :views,
       :test,
       :layout,
       :stylesheets,
       :javascripts,
       :images,
       :helpers,
       :associations,
       :model_views,
       :common_views,
       :recursively
      ]
    end

    def calculate_enabled_tasks
      tasks = options[:only] ? [options[:only]] : default_tasks.dup
      tasks.select{|task| options[task] != false}
    end

    def create_model_class
      @model_class = Inflector.classify(name).constantize
    rescue
      raise RuntimeError, <<-ERROR
Couldn't find '#{ name }' model.
This generator doesn't support model creation, so please create it manually.
% ruby script/generate model #{ name }

#{ banner }
      ERROR
    end

end
