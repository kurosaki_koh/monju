class Localized::Message
  include Reloadable
  include Singleton
  class EntryNotFound < RuntimeError; end

  def initialize
    @yaml_path          = "config/localize.yml"
    @localized_messages = YAML::load_file(absolute_yaml_path)
    ActiveRecord::Base.logger.debug("Loaded localized message from '#{@yaml_path}'")
  rescue => err
    ActiveRecord::Base.logger.error("Localize Error: cannot load '#{@yaml_path}'.\n#{err}")
    raise
  end

  def group (group_name)
    @localized_messages[group_name.to_s]
  end

  def [] (group_name, item_name)
    @localized_messages     or raise EntryNotFound
    @localized_messages[group_name.to_s]  or raise EntryNotFound
    @localized_messages[group_name.to_s][item_name.to_s]
  rescue
    raise EntryNotFound, "Missing localized entry for '#{group_name}'=>'#{item_name}'! Check #{@yaml_path}"
  end

  protected
    def absolute_yaml_path
      File.join(RAILS_ROOT, @yaml_path)
    end
end
