class Localized::Model
  include Reloadable

  attr_reader :yaml_path, :active_record

  class ConfigurationError < RuntimeError; end
  class << self
    def [] (table_name)
      model = normalize_model(table_name)
      @localized_models ||= {}
      @localized_models[model.table_name] ||= new(model)
    end

    def normalize_model (name)
      case name
      when Class                  ; name
      when ActiveRecord::Base     ; normalize_model(name.class)
      else                        ; normalize_model(name.to_s.classify.constantize)
      end
    end
  end

  def initialize (active_record)
    @active_record   = active_record
    @table_name      = @active_record.table_name
    @yaml_path       = "db/localized/#{@table_name}.yml"
    @view_properties = {}
    @yaml = YAML::load_file(absolute_yaml_path)
    ActiveRecord::Base.logger.debug("Loaded localized setting from '#{@yaml_path}'")
  rescue Errno::ENOENT
    raise ConfigurationError, "Cannot read YAML data from #{@yaml_path}.\nRun 'ruby script/generate special --only-localize %s'" % @active_record
  rescue ArgumentError => err
    ActiveRecord::Base.logger.debug("localize error: YAML #{err} in #{@yaml_path}")
    @load_error = true
  end

  def [] (group_name, attr_name = nil)
    return nil if load_error?
    localized_name = @yaml[group_name.to_s]
    localized_name = localized_name[attr_name] if attr_name
    return localized_name
  rescue => err
    ActiveRecord::Base.logger.debug("Localize Error: for (%s, %s,%s). %s" % [table_name, group_name, attr_name, err])
    return nil
  end

  def instance_name
    self[:names][:instance]
  end

  def masters
    pkey    = active_record.primary_key
    options = active_record.columns_hash[instance_name.to_s] && {:select=>"#{pkey},#{instance_name}"} || {}
    active_record.find(:all, options.merge(:order=>pkey)).collect{|r| [r[pkey], localize_instance(r)]}
  end

  def localize_instance (record)
    name = instance_name
    case name
    when NilClass ; return nil
    when Symbol ; return record.send(name)
    when String ; return name.gsub('%d', record.id.to_s)
    else
      raise TypeError, "got %s, expected Symbol or String. Check '%s'" % [name.class, @yaml_path]
    end
  end

  def load_error?
    @load_error
  end

  def view_property (column_name)
    @view_properties[column_name] ||= Localized::ViewProperty.new(self, self["property_#{column_name}"])
  end

  protected
    def absolute_yaml_path
      File.join(RAILS_ROOT, @yaml_path)
    end
end
