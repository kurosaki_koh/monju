module <%= controller_class_name %>Helper
  include LocalizeHelper
  include IpeHelper
  include AssociatedHelper

  def human_attribute_name (record, column_name)
    super
  end

  def human_attribute_value (record, column_name)
    super
  end

  def human_attribute_edit (singular_name, column_name)
    super
  end
end
