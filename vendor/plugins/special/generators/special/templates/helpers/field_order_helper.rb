module FieldOrderHelper
  def field_order (*names)
    if names.empty?
      @field_order || model_class.content_columns.collect{|column| column.name}
    else
      unknown_column_names = names.collect(&:to_s) - model_class.column_names
      unless unknown_column_names.empty?
        raise "Unknown column name(s): %s for %s" % [unknown_column_names.inspect, model_class]
      end
      @field_order = names.collect{|name| name.to_s}
    end
  end

  def visible_columns (action_name)
    field_order.select{|name|
      not 'enabled' == name and
      not hidden_field?(action_name, name)
    }.collect{|name| model_class.columns_hash[name]}
  end
end
