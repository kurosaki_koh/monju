Ajax.InPlaceCollectionEditor = Class.create();
Object.extend(Ajax.InPlaceCollectionEditor.prototype, Ajax.InPlaceEditor.prototype);
Object.extend(Ajax.InPlaceCollectionEditor.prototype, {
  createEditField: function() {
    if (! this.options.cached_selectTag) {
      var selectTag = document.createElement("select");
      var collection = this.options.collection || [];
      var optionTag;
      for (var i = 0; i < collection.length; i++) {
	optionTag = document.createElement("option");
	optionTag.value = collection[i][0];
	if (this.options.model == optionTag.value)
	  optionTag.selected = true;
	optionTag.appendChild(document.createTextNode(collection[i][1]));
	selectTag.appendChild(optionTag);
      }
      this.options.cached_selectTag = selectTag;
    }

    this.editField = this.options.cached_selectTag;
    if(this.options.loadTextURL) {
      this.loadExternalText();
    }
    this.form.appendChild(this.editField);
    this.options.callback = function(form, value) {
      return "value=" + encodeURIComponent(value);
    }
  }
});

