class <%= controller_class_name %>Controller < <%= base_controller %>
  extend  ClassMethodHelper
  include <%= controller_class_name %>Helper

  model_class   <%= model_class %>
  ipe_for       <%= model_class %>
<%- already_defined_for = {} -%>
<%- if acts_as_tree -%>
  has_many      :children
  belongs_to    :parent
<%- already_defined_for[model_class.to_s] = true -%>
<%- end -%>
<%- model_class.reflections.values.sort{|a,b| a.name.to_s<=>b.name.to_s}.each do |reflection| -%>
  <%- prefix = '# ' if already_defined_for[reflection.class_name] -%>
  <%- already_defined_for[reflection.class_name] = true -%>
  <%= "%s%-14s" % [prefix, reflection.macro] %>:<%= reflection.name %>
<%- end -%>
<%- show_fields = (options[:belongs_to] ? model_class.columns : model_class.content_columns).map{ |i|i.name} -%>
<%- show_fields -= [model_class.primary_key, column_position, column_enabled].compact -%>
<%- hide_fields = model_class.column_names - show_fields -%>
  field_order   <%= model_class.column_names.map{|i|":#{i}"}.join(', ') %>
  hide_field    <%= hide_fields.map{|i|":#{i}"}.join(', ') %> # <%= show_fields.map{|i|":#{i}"}.join(', ') %>

<%- if column_enabled -%>
  def ajax_enabled
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
    @<%= singular_name %>.toggle!(:enabled)
  end
<%- end -%>

<%- if column_position -%>
  def ajax_position
    <%= model_name %>.transaction do
      params[:position].each_with_index do |arg, i|
        id  = arg.to_s.sub(/\A.*?(\d+)\Z/){$1}.to_i
        sql = "UPDATE <%= model_class.table_name %> SET position = %d WHERE <%= model_class.primary_key %> = %d" % [i+1, id]
        <%= model_name %>.connection.execute(sql)
      end
    end
    list
  end

  def sort
    list
  end
<%- end -%>

  def index
    options = {
      :include    => self.class.reflections.collect{|ref| ref.name},
      :order      => '<%= order_field %>',
      :per_page   => 10,
<%- if acts_as_tree -%>
      :conditions => "<%= model_class.table_name %>.parent_id IS NULL",
<%- end -%>
    }
    options[:conditions] = ["<%= primary_key %> = ?", params[:id]] if params[:id]
    @<%= singular_name %>_pages, @<%= plural_name %> = paginate(model_class, options)
    render :action=>"ajax_list"
  end

  def list
    options = {
      :include    => self.class.reflections.collect{|ref| ref.name},
      :order      => '<%= order_field %>',
      :per_page   => 10,
<%- if acts_as_tree -%>
      :conditions => "<%= model_class.table_name %>.parent_id IS NULL",
<%- end -%>
    }
    @<%= singular_name %>_pages, @<%= plural_name %> = paginate :<%= plural_name %>, options
  end

  def show
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def new
    @<%= singular_name %> = <%= model_name %>.new
  end

  def confirm_create
    @<%= singular_name %> = <%= model_name %>.new(params[:<%= singular_name %>])
    render :action => 'new' unless @<%= singular_name %>.valid?
  end

  def create
    @<%= singular_name %> = <%= model_name %>.new(params[:<%= singular_name %>])
    return render(:action => 'new') if params[:btn_cancel]

<%- if column_enabled -%>
    @<%= singular_name %>.enabled    ||= true
<%- end -%>
<%- if column_position -%>
    @<%= singular_name %>.position ||= <%= model_name %>.find(:first, :select=>"MAX(position) AS val").val.to_i+1 rescue 0
<%- end -%>
    if @<%= singular_name %>.save
      if params[:ajax]
        list
        render :action=>"ajax_create"
      else
        flash[:notice] = localized_class_name(:<%= singular_name %>) + localize(:command, :successfully_created)
        redirect_to :action => 'list'
      end
    else
      if params[:ajax]
        render :update do |page|
          message = localized_error_messages_for(@<%= singular_name %>) || localize(:message, :ajax_error)
          page.replace_html "notice", message
        end
      else
        render :action => 'new'
      end
    end
  end


  def edit
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def confirm_update
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
    @<%= singular_name %>.attributes = params[:<%= singular_name %>]
    render :action => 'edit' unless @<%= singular_name %>.valid?
  end

  def update
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
    if params[:btn_cancel]
      @<%= singular_name %>.attributes = params[:<%= singular_name %>]
      render :action => 'edit'
    elsif @<%= singular_name %>.update_attributes(params[:<%= singular_name %>])
      flash[:notice] = localized_class_name(:<%= singular_name %>) + localize(:command, :successfully_updated)
      redirect_to :action => 'show', :id => @<%= singular_name %>
    else
      render :action => 'edit'
    end
  end

  def confirm_destroy
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def destroy
    if params[:btn_cancel]
      redirect_to :action => 'list'
    else
      <%= model_name %>.find(params[:id]).destroy
      if params[:ajax]
        render :action=>"ajax_destroy"
      else
        redirect_to :action => 'list'
      end
    end
  end
end
