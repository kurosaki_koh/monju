<%- action_prefix = "#{singular_name}_#{reflection.name}_" -%>
module <%= module_name %>

  def self.append_features(base)
    super
    base.before_filter :prepare_<%= action_prefix %>variables
  end

protected
  def prepare_<%= action_prefix %>variables
    case @action_name
    when /\A<%= action_prefix %>/
      @ajax_prefix        = params[:ajax] ? "ajax_" : nil
      @render_method      = params[:ajax] ? :partial : :action
      @action_prefix      = $&
      @original_action_name = @action_name
      @real_action_name   = $'
      @ajax_action_name   = "#{ @ajax_prefix }#{ @real_action_name }"
      @reflection         = <%= reflection.active_record %>.reflections[:<%= reflection.name %>] or
          raise RuntimeError, "Association Error: '%s' should contain '%s' reflection." % [<%= reflection.active_record %>, :<%= reflection.name %>]

      case @real_action_name
      when 'ajax_position'
        @belongs_to = <%= reflection.active_record %>.find(params[:id])

      when 'list', 'new', 'sort'
        @belongs_to = <%= reflection.active_record %>.find(params[:id])
        @<%= child_plural_name %> = @belongs_to.<%= reflection.name %>

      when 'confirm_create', 'create'
        @belongs_to = <%= reflection.active_record %>.find(params[:id])
        @<%= child_name %> = @belongs_to.<%= reflection.name %>.build(params[:<%= child_name %>])

      when 'show', 'edit', 'confirm_destroy', 'confirm_destroy', 'destroy'
        @<%= child_name %> = <%= belongs_to.active_record %>.find(params[:id])
        @belongs_to = @<%= child_name %>.<%= belongs_to.name %>

      when 'confirm_update', 'update'
        @<%= child_name %> = <%= belongs_to.active_record %>.find(params[:id])
        @belongs_to = @<%= child_name %>.<%= belongs_to.name %>
        @<%= child_name %>.attributes = params[:<%= child_name %>]
        @<%= child_name %>.<%= belongs_to.primary_key_name %> = @belongs_to.id

      else
      end
    end
  end

public
  def <%= action_prefix %>ajax_position
    <%= belongs_to.active_record %>.transaction do
      params[:position].each_with_index do |arg, i|
        id  = arg.to_s.sub(/\A.*?(\d+)\Z/){$1}.to_i
        sql = "UPDATE <%= belongs_to.active_record.table_name %> SET position = %d WHERE <%= belongs_to.active_record.primary_key %> = %d" % [i+1, id]
        <%= belongs_to.active_record %>.connection.execute(sql)
      end
    end
    @<%= child_plural_name %> = @belongs_to.<%= reflection.name %>
    render :action=>"ajax_position"
  end

  def <%= action_prefix %>sort
    render @render_method=>@ajax_action_name
  end

  def <%= action_prefix %>list
    render @render_method=>@ajax_action_name
  end

  def <%= action_prefix %>new
    @<%= child_name %> = @belongs_to.<%= reflection.name %>.build
    render @render_method=>@ajax_action_name
  end

  def <%= action_prefix %>show
    render @render_method=>@ajax_action_name
  end

  def <%= action_prefix %>edit
    render @render_method=>@ajax_action_name
  end

  def <%= action_prefix %>confirm_create
    if @<%= child_name %>.valid?
      render @render_method=>@ajax_action_name
    else
      render @render_method=>"#{@ajax_prefix}new"
    end
  end

  def <%= action_prefix %>create
    return render(@render_method=>"#{@ajax_prefix}new") if params[:btn_cancel]

<%- if reflection.klass.columns_hash['enabled'] -%>
    @<%= child_name %>.enabled    ||= true
<%- end -%>
<%- if reflection.klass.columns_hash['position'] -%>
    @<%= child_name %>.position ||= <%= reflection.class_name %>.find(:first, :select=>"MAX(position) AS val", :conditions=>["<%= reflection.primary_key_name %> = ?", @belongs_to.id]).val.to_i+1 rescue 0
<%- end -%>
    if @<%= child_name %>.save
      if @ajax_prefix
        @<%= child_plural_name %> = @belongs_to.<%= reflection.name %>
        render :action=>@ajax_action_name
      else
        flash[:notice] = localized_class_name(:<%= child_name %>) + localize(:command, :successfully_created)
        redirect_to :action=>associated_action(:list), :id=>@belongs_to
      end
    else
      if @ajax_prefix
        render :update do |page|
          message = localized_error_messages_for(@<%= child_name %>) || localize(:message, :ajax_error)
          page.replace_html "notice", message
        end
      else
        render @render_method=>"#{@ajax_prefix}new"
      end
    end
  end

  def <%= action_prefix %>confirm_update
    if @<%= child_name %>.valid?
      render @render_method=>@ajax_action_name
    else
      render @render_method=>"#{@ajax_prefix}edit"
    end
  end

  def <%= action_prefix %>update
    if params[:btn_cancel]
      render @render_method=>"#{@ajax_prefix}edit"
    elsif @<%= child_name %>.save
      flash[:notice] = localized_class_name(:<%= child_name %>) + localize(:command, :successfully_updated)
      redirect_to :action=>associated_action(:list), :id=>@belongs_to
    else
      render @render_method=>"#{@ajax_prefix}edit"
    end
  end

  def <%= action_prefix %>confirm_destroy
    render @render_method=>"#{@ajax_prefix}confirm_destroy"
  end

  def <%= action_prefix %>destroy
    @belongs_to.<%= reflection.name %>.destroy(params[:id]) unless params[:btn_cancel]
    if @ajax_prefix
      render :action=>@ajax_action_name
    else
      redirect_to :action=>associated_action(:list), :id=>@belongs_to
    end
  end
end
