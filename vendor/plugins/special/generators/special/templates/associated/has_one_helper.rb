<%- action_prefix = "#{singular_name}_#{reflection.name}_" -%>
module <%= module_name %>

  def self.append_features(base)
    super
    base.before_filter :prepare_<%= action_prefix %>variables
  end

protected
  def prepare_<%= action_prefix %>variables
    case @action_name
    when /\A<%= action_prefix %>/
      @ajax_prefix        = params[:ajax] ? "ajax_" : nil
      @action_prefix      = $&
      @real_action_name   = $'
      @ajax_action_name   = "#{ @ajax_prefix }#{ @real_action_name }"
      @reflection         = <%= reflection.active_record %>.reflections[:<%= reflection.name %>] or
          raise RuntimeError, "Association Error: '%s' should contain '%s' reflection." % [<%= reflection.active_record %>, :<%= reflection.name %>]

      @belongs_to = <%= reflection.active_record %>.find(params[:id])
      @action_id  = @belongs_to.id

      case @real_action_name
      when 'create', 'confirm_create', 'new'
        @<%= child_name %> = @belongs_to.build_<%= reflection.name %>(params[:<%= child_name %>])
      when 'list', 'sort', 'show', 'edit', 'confirm_destroy', 'confirm_destroy', 'destroy'
        @<%= child_name %> = @belongs_to.<%= reflection.name %> || @belongs_to.build_<%= reflection.name %>
      when 'confirm_update', 'update'
        @<%= child_name %> = @belongs_to.build_<%= reflection.name %>(params[:<%= child_name %>], true)
      else
        raise RuntimeError, "unsupported action: #{@real_action_name}"
      end
    end
  end

public
  def <%= action_prefix %>new
    render :action=>@ajax_action_name
  end

  def <%= action_prefix %>show
    render :action=>@ajax_action_name
  end

  def <%= action_prefix %>edit
    render :action=>@ajax_action_name
  end

  def <%= action_prefix %>confirm_create
    if @<%= child_name %>.valid?
      render :action=>@ajax_action_name
    else
      render :action=>"#{@ajax_prefix}new"
    end
  end

  def <%= action_prefix %>create
    if params[:btn_cancel]
      render :action=>"#{@ajax_prefix}new"
    elsif @<%= child_name %>.save
      if @ajax_prefix
        @<%= reflection.name %> = @belongs_to.<%= reflection.name %>
        render :action=>"ajax_create"
      else
        flash[:notice] = localized_class_name(:<%= child_name %>) + localize(:command, :successfully_created)
        redirect_to :action => "#{@ajax_prefix}#{@action_prefix}show", :id=>@belongs_to
      end
    else
      if @ajax_prefix
        render :update do |page|
          message = localized_error_messages_for(@<%= child_name %>) || localize(:message, :ajax_error)
          page.replace_html "notice", message
        end
      else
        render :action=>"#{@ajax_prefix}new"
      end
    end
  end

  def <%= action_prefix %>confirm_update
    if @<%= child_name %>.valid?
      render :action=>@ajax_action_name
    else
      render :action=>"#{@ajax_prefix}edit"
    end
  end

  def <%= action_prefix %>update
    if params[:btn_cancel]
      render :action=>"#{@ajax_prefix}edit"
    elsif @<%= child_name %>.save
      flash[:notice] = localized_class_name(:<%= child_name %>) + localize(:command, :successfully_updated)
      redirect_to :action => "#{@ajax_prefix}#{@action_prefix}show", :id=>@belongs_to
    else
      render :action => "#{@ajax_prefix}edit"
    end
  end

  def <%= action_prefix %>confirm_destroy
    render :action=>"#{@ajax_prefix}confirm_destroy"
  end

  def <%= action_prefix %>destroy
    @belongs_to.<%= reflection.name %>.destroy(params[:id]) unless params[:btn_cancel]
    if @ajax_prefix
      render :action=>@ajax_action_name
    else
      redirect_to :action=>"#{@ajax_prefix}list", :id=>@belongs_to
    end
  end
end
