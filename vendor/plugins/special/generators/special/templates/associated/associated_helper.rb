module AssociatedHelper
protected
  def model_class
    self.class.model_class
  end

  def belongs_to_name (record = @belongs_to)
    localized_instance_name(record)
  end

  def association_name (reflection)
    localized_data(reflection.active_record, :associations, reflection.name) ||
      localized_class_name(reflection.class_name)
  end

  def association_label (reflection, record)
    case reflection.macro
    when :has_many  ; localize(:label, :has_many) % record.send(reflection.name).size rescue "has_many"
    when :has_one   ; localize(:label, :has_one)
    else ; raise RuntimeError, "Association Error: not supported macro '%s'" % reflection.macro
    end
  end

  def belongs_to_link (record = @belongs_to, format = "%s &gt;&gt;")
    owner = belongs_to_name(record)
    if owner
      link = link_to(owner, :controller=>record.class.to_s.underscore, :action=>:show, :id=>record)
      format % link
    else
      nil
    end
  end

  def ajax_link_to_association (reflection, record, label = nil)
    case reflection.macro
    when :has_many
      ajax_link_to_list(reflection, record, label)
    when :has_one
      if record.send(reflection.name)
        ajax_link_to_show(reflection, record, label)
      else
        ajax_link_to_new
      end
    else
      raise RuntimeError, "Association Error: not supported macro '%s'" % reflection.macro
    end
  end

  def associated_controller_name (object)
    case object
    when ActiveRecord::Reflection::AssociationReflection
      object.class_name.to_s.gsub(/::/, '_').underscore
    when ActiveRecord::Base
      object.class.to_s.gsub(/::/, '_').underscore
    else
      raise RuntimeError, "Association Error: cannot create controller name from this type '%s'" % object.class
    end
  end

  def link_to_association (reflection, record, label = nil)
    label ||= association_label(reflection, record)
    case reflection.macro
    when :has_many
      associated_link_to(:list, reflection, record, record, label)
    when :has_one
      if record.send(reflection.name)
        associated_link_to(:show, reflection, record, record, label)
      else
        associated_link_to(:edit, reflection, record, record, label)
      end
    else ; raise RuntimeError, "Association Error: not supported macro '%s'" % reflection.macro
    end
  end

  def associated_link_to (action_name, reflection, record, id = nil, label = nil)
    link_to label || association_label(reflection, record),
      @controller.class.associated_url(reflection, record, "%s_#{action_name}", :id=>id)
  end

  def ajax_link_to_sort (label = nil)
    link_to_function label || localize(:label, :sort),
      "Element.toggle('#{associated_element_id(:list)}');Element.toggle('#{associated_element_id(:sort)}')"
  end

  def ajax_link_to_sortable_element (element_id, tag = :li)
    options = {
      :tag      => tag.to_s,
      :url      => {
        :action  => associated_action(:ajax_position), :id=>@belongs_to, :ajax=>true,
        :sort_id => associated_element_id(:sort),
      },
      :with     => "Sortable.serialize('#{ element_id }', {name:'position'})",
    }
    if params[:ajax]
      options[:complete] = visual_effect(:highlight, associated_element_id(:main))
      options[:url][:list_id] = associated_element_id(:list)
    end
    sortable_element element_id, options
  end

  def ajax_link_to_list (reflection, record, label = nil)
    link_to_remote label || association_label(reflection, record),
      :update=>associated_element_id(reflection),
      :url=>@controller.class.associated_url(reflection, record, "%s_list", :ajax=>true,
               :record_id => associated_element_id(:record, record.id))
  end

  def ajax_link_to_show (reflection, record, label = nil)
    link_to_remote label || association_label(reflection, record),
      :url=>{:action=>associated_action(:show), :id=>@belongs_to, :ajax=>true}
  end

  def ajax_link_to_create (label = nil)
    link_to_remote label || localize(:label, :create),
      :submit=>associated_element_id(:new),
      :url=>{:action=>associated_action(:create), :id=>@belongs_to, :ajax=>true,
        :new_id=>associated_element_id(:new),
        :all_id=>associated_element_id(:all),
        :belongs_to_id=>params[:record_id],
      }
  end

  def ajax_link_to_destroy (record, label = nil)
    link_to_remote label || localize(:label, :destroy),
      :confirm => localize(:label, :confirm_destroy),
      :url=>{:action=>associated_action(:destroy), :id=>record, :ajax=>true,
        :record_id => associated_element_id(:record, record.id)}
  end

  def ajax_link_to_enabled (record, label = nil)
    element_id = associated_element_id(:record_enabled, record.id)
    checkbox   = check_box_tag('check', record.id, record.enabled, :id=>element_id)
    observer   = observe_field element_id,
      :url=>{:action=>:ajax_enabled, :id=>record, :ajax=>true,
             :record_id  => associated_element_id(:record, record.id),
             :element_id => element_id}
    return "#{ checkbox }#{ observer }"
  end

  def ajax_link_to_close (element_id, effect_element_id = nil, label = nil)
    link_to_function label || image_tag("icons/close.png"),
      "Element.remove('#{element_id}')",
      :onmouseover => visual_effect(:highlight, effect_element_id || element_id, :duration => 0.6)
  end

  def ajax_link_to_new (label = nil)
    link_to_function label || localize(:label, :new),
      "Element.toggle('#{associated_element_id(:new)}')"
  end

  def associated_action (action_name)
    "%s%s" % [@action_prefix, action_name]
  end

  def associated_element_id_prefix
    @element_id_prefix ||=
      @reflection ? ("%s-%d-%s-" % [@reflection.class_name, @belongs_to.id, @reflection.name]) : "associated-"
  end

  def associated_element_id (name_or_reflection, rest = nil)
    case name_or_reflection
    when ActiveRecord::Reflection::AssociationReflection
      associated_element_id(name_or_reflection.name)
    else
      "%s%s%s" % [associated_element_id_prefix, name_or_reflection, rest]
    end
  end

  def action_name_without_association
    @action_name.to_s.sub(/\A.*?((confirm_)?[^_]+)\Z/){$1.to_s}
  end

  def ajax_acts_as_tree_select (klass)
    selected     = params[:id] && klass.find(params[:id].to_i) rescue nil
    candidates   = selected ? selected.children : klass.roots
    url_options  = {:action=>@action_name, :model=>params[:model], :method=>params[:method]}
    html         = candidates.collect{|obj|
      "<li>%s</li>\n" % link_to_remote(obj.name, :url=>url_options.merge(:id=>obj.id))
    }.join
    html = "<div><ul>%s</ul></div>" % html

    if selected
      parent_link = ([selected] + selected.ancestors).flatten.reverse.collect{|obj|
        link_to_remote obj.name, :url=>url_options.merge(:id=>obj.parent_id)
      }.join("&nbsp;&gt;&gt;&nbsp;")
      html = "%s%s" % [parent_link, html]
    end

    acts_as_tree_field(params[:model], params[:method], klass, selected, html)
  end

public
  def acts_as_tree_field (model, method, klass, selected, html = nil, select_html = nil)
    select_html ||= localize(:label, :select) || "[SELECT]"
    object_id = selected ? selected.id : nil
    controller_class = klass.to_s.underscore.sub(%r|\A.*/|, '')
    url_options = {:controller=>controller_class, :action=>"ajax_acts_as_tree_select", :id=>object_id,
                   :model=>model, :method=>method}
    opener = link_to_remote(select_html, :url=>url_options)
    opener = '' if html && selected
    value  = CGI::escapeHTML(localized_instance_name(selected)) rescue object_id
    <<-HTML
    <div id="#{model}_#{method}">
      <input type=hidden name="#{model}[#{method}]" value="#{object_id}">
      #{value}
      #{opener}
      #{html}
    </div>
    HTML
  end
end
