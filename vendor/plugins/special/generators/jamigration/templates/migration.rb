class <%= class_name %> < ActiveRecord::Migration
  class << self
    def tablename
      '<%= table_name %>'
    end

<% case ActiveRecord::Base.configurations[::RAILS_ENV || "development"]["adapter"] -%>
<% when "mysql" -%>
    def options
      {
        :force       => true,
        :options     => "ENGINE=InnoDB DEFAULT CHARSET=utf8",
#        :options     => "ENGINE=MyISAM DEFAULT CHARSET=ujis",
#        :id          => false,
#        :primary_key => '<%= prefix_name %>_code',
      }
    end
<% when "postgresql" -%>
    def options
      {
        :force       => true,
#        :id          => false,
#        :primary_key => '<%= prefix_name %>_code',
      }
    end
<% else -%>
    def options
      {
        :force       => true,
      }
    end
<% end -%>

    def up
      create_table(tablename, options) {|table|
#	table.column	:<%= prefix_name %>_name		,:string	,:limit=>32
#	table.column	:<%= prefix_name %>_code		,:integer
#	table.column	:<%= prefix_name %>_flag		,:boolean	,:default=>0
	table.column	:created_on	  	,:datetime
      }
    end

    def down
      drop_table(tablename) rescue :nop
    end
  end

end
