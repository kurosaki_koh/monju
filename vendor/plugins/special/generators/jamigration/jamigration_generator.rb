class JamigrationGenerator < Rails::Generator::NamedBase
  def prepare_variables
    class_name = Inflector.classify(name)          
    model_class = class_name.constantize rescue nil
    model_table_name = model_class.table_name rescue
      ActiveRecord::Base.pluralize_table_names ? name.tableize : name.underscore
    @table_name  = model_table_name.gsub('/', '_')
    @prefix_name = name.underscore.gsub('/', '_')
  end

  def manifest
    prepare_variables
    record do |m|
      m.migration_template 'migration.rb', 'db/migrate',
        :assigns=>{:table_name => @table_name, :prefix_name => @prefix_name}
    end
  end
end
