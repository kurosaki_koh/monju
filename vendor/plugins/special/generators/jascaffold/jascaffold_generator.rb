class ScaffoldingSandbox
  attr_accessor :model_name
end

module SandboxFeatureShowOnPlain
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
        <p>
           <label class="confirm"><%= human_attribute_name(@#{singular_name}, '#{column.name}') %></label><br/>
           <%= human_attribute_value(@#{singular_name}, '#{column.name}') %>
        </p>
      <%- end -%>|
    }
  end
end

module SandboxFeatureShowOnTable
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
      <tr>
        <td class="label"><%= human_attribute_name(@#{singular_name}, '#{column.name}') %></td>
        <td><%= human_attribute_value(@#{singular_name}, '#{column.name}') %></td>
      </tr>
      <%- end -%>|
    }
  end
end

module SandboxFeatureEditOnPlain
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
      <p>
        <label for="#{record}_#{column.name}" class="confirm"><%= human_attribute_name(@#{singular_name}, '#{column.name}') %></label><br/>
        <%= human_attribute_edit('#{singular_name}', '#{column.name}') %>
      </p>
      <%- end -%>|
    }
  end
end

module SandboxFeatureEditOnTable
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
      <tr>
        <td class="<%= localized_label_class_on(@#{singular_name}, '#{column.name}') %>">
          <%= human_attribute_name(@#{singular_name}, '#{column.name}') %>
        </td>
        <td><%= human_attribute_edit('#{singular_name}', '#{column.name}') %></td>
      </tr>
      <%- end -%>|
    }
  end
end

module SandboxFeatureListOnTable
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
      <tr>
        <td class="label"><%= human_attribute_name(@#{singular_name}, '#{column.name}') %></td>
        <td><%= human_attribute_value(@#{singular_name}, '#{column.name}') %></td>
      </tr>
      <%- end -%>|
    }
  end
end

module SandboxFeatureHidden
  def default_input_block
    Proc.new { |record, column| %|
      <%- unless @controller.class.hidden_field?(@action_name, '#{column.name}') -%>
        <%= hidden_field "#{singular_name}", "#{column.name}" %>
      <%- end -%>|
    }
  end
end

class JascaffoldGenerator < ScaffoldGenerator
  attr_reader :model_table_name, :charset
  
  def initialize (*)
    super

    class_name = Inflector.classify(name)          
    @model_class = class_name.constantize rescue nil
    @model_table_name = @model_class.table_name rescue
      ActiveRecord::Base.pluralize_table_names ? name.tableize : name.underscore
    @model_table_name.gsub!('/', '_')

    if Object.const_defined?(class_name)
      Object.instance_eval("remove_const(class_name)")
    end

    @charset =
      case $KCODE.to_s.downcase[0]
      when ?e; 'EUC-JP'
      when ?s; 'Shift_JIS'
      when ?j; 'ISO-2022-JP'
      when ?u; 'UTF-8'
      else   ; 'ISO-8859-1'
      end

    @primary_key = @model_class.primary_key rescue "%s.primary_key" % class_name
  end

  def manifest
    m = parent_manifest = super
    m.template 'localize_helper.rb',
               File.join('app/helpers',
              "localize_helper.rb")

    # Localized message file (depends on $KCODE)
    m.template File.join('localized_messages', @charset),
               File.join('config', 'localize.yml'),
               :collision => :skip

    m.directory 'db/localized'
    m.template "localized_columns.rhtml", File.join('db/localized', "#{model_table_name}.yml"),
               :collision => :skip

    model_path = "app/views/models/#{model_table_name}"
    m.directory model_path

    # Scaffolded show-view (plain version)
    m.complex_template "form.rhtml",
               File.join(model_path, "_show1.rhtml"),
               :insert  => 'form_scaffolding1.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureShowOnPlain }

    # Scaffolded show-view (table version)
    m.complex_template "form.rhtml",
               File.join(model_path, "_show2.rhtml"),
               :insert  => 'form_scaffolding2.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureShowOnTable }

    # Scaffolded edit-view (plain version)
    m.complex_template "form.rhtml",
               File.join(model_path, "_edit1.rhtml"),
               :insert  => 'form_scaffolding1.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureEditOnPlain }

    # Scaffolded edit-view (table version)
    m.complex_template "form.rhtml",
               File.join(model_path, "_edit2.rhtml"),
               :insert  => 'form_scaffolding2.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureEditOnTable }

    # Scaffolded list-view
    m.complex_template "form.rhtml",
               File.join(model_path, "_list.rhtml"),
               :insert  => 'form_scaffolding2.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureListOnTable }

    # Scaffolded hidden-view
    m.complex_template "form.rhtml",
               File.join(model_path, "_hidden.rhtml"),
               :insert  => 'form_scaffolding1.rhtml',
               :sandbox => lambda { create_sandbox.extend SandboxFeatureHidden }

    # for flash message
    m.directory 'app/views/common'
    m.file "plain/common/_notice.rhtml", 'app/views/common/_notice.rhtml', :collision => :skip
    m.file "plain/common/_error.rhtml" , 'app/views/common/_error.rhtml' , :collision => :skip

    return parent_manifest
  end

  protected
    # Override with your own usage banner.
    def banner
      "Usage: #{$0} jascaffold ModelName [ControllerName] [action, ...]"
    end

    def scaffold_views
      %w(list show new edit confirm_create confirm_update confirm_destroy)
    end

    def model_instance
      @model_class.new rescue super
    end
end


