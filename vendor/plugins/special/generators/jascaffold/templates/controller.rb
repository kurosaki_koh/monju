class <%= controller_class_name %>Controller < ApplicationController
  include <%= controller_class_name %>Helper

<% unless suffix -%>
  def index
    list
    render :action => 'list'
  end
<% end -%>

<% for action in unscaffolded_actions -%>
  def <%= action %><%= suffix %>
  end

<% end -%>
  def list<%= suffix %>
    @<%= singular_name %>_pages, @<%= plural_name %> = paginate :<%= plural_name %>, :per_page => 10, :order=><%= model_name %>.primary_key
  end

  def show<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def new<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.new
  end

  def confirm_create<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.new(params[:<%= singular_name %>])
    render :action => 'new<%= suffix %>' unless @<%= singular_name %>.valid?
  end

  def create<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.new(params[:<%= singular_name %>])
    if params[:btn_cancel]
      render :action => 'new<%= suffix %>'
    elsif @<%= singular_name %>.save
      flash[:notice] = localize(:model, '<%= singular_name %>') + localize(:command, :successfully_created)
      redirect_to :action => 'list<%= suffix %>'
    else
      render :action => 'new<%= suffix %>'
    end
  end

  def edit<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def confirm_update<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.new(params[:<%= singular_name %>])
    render :action => 'edit<%= suffix %>' unless @<%= singular_name %>.valid?
  end

  def update
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
    if params[:btn_cancel]
      @<%= singular_name %>.attributes = params[:<%= singular_name %>]
      render :action => 'edit<%= suffix %>'
    elsif @<%= singular_name %>.update_attributes(params[:<%= singular_name %>])
      flash[:notice] = localize(:model, '<%= singular_name %>') + localize(:command, :successfully_updated)
      redirect_to :action => 'show<%= suffix %>', :id => @<%= singular_name %>
    else
      render :action => 'edit<%= suffix %>'
    end
  end

  def confirm_destroy<%= suffix %>
    @<%= singular_name %> = <%= model_name %>.find(params[:id])
  end

  def destroy<%= suffix %>
    if params[:btn_cancel]
      redirect_to :action => 'list<%= suffix %>'
    else
      <%= model_name %>.find(params[:id]).destroy
      redirect_to :action => 'list<%= suffix %>'
    end
  end
end
