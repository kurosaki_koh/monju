module LocalizeHelper
  class ViewProperty
    def initialize (hash)
      @yaml_data    = hash
      @options      = hash[:options] || {}
      @master_array = []
      @master_hash  = {}

      parse_master(hash[:masters] || [])
      @include_blank = @master_array.first.first.to_s.empty? rescue false
    end

    attr_reader :options

  protected
    def parse_master (array)
      array .each do |hash|
        key, val = hash.to_a.first
        @master_array << [key, val]
        @master_hash[key] = val
      end
    end

  public
    def [] (key)
      @yaml_data[key]
    end

    def masters
      @master_array
    end

    def master (value)
      @master_hash[value]
    end

    def has_master?
      not masters.empty?
    end

    def include_blank?
      @include_blank
    end

    def has_time_format?
      self[:time_format]
    end

    def has_format? (postfix = nil)
      self["format#{postfix}".intern] || self[:format]
    end

    def has_column_type?
      self[:column_type].is_a?(Symbol)
    end

    def human_value (value, controller)
      controller = controller.is_a?(ActionController::Base) ? controller : controller.controller
      if has_master?
        master(value)
      elsif has_time_format?
        value.strftime(self[:time_format])
      elsif format = has_format?("_" + controller.action_name)
        format % value
      else
        value
      end
    end

    def human_edit (singular_name, column_name, view)
      if has_master?
        html = view.collection_select(singular_name, column_name, masters, :first, :last, options)
      elsif has_column_type?
        tag = ActionView::Helpers::InstanceTag.new(singular_name, column_name, self)
        tag.instance_eval("def column_type; :%s; end" % self[:column_type])
        html = tag.to_tag(options)
      else
        html = view.input(singular_name, column_name, options)
      end

      if format = has_format?("_" + view.controller.action_name)
        html = format % html
      end

      return html
    end
  end

  class LocalizedMessage
    include Singleton

    class EntryNotFound < RuntimeError; end

    def initialize
      @localized_messages = YAML::load_file("#{RAILS_ROOT}/config/localize.yml")
    rescue => err
      ActiveRecord::Base.logger.error("localize error: cannot load 'config/localize.yml'.\n#{err}")
      raise
    end

    def group (group_name)
      @localized_messages[group_name.to_s]
    end

    def [] (group_name, item_name)
      @localized_messages			or raise EntryNotFound
      @localized_messages[group_name.to_s]	or raise EntryNotFound
      @localized_messages[group_name.to_s][item_name.to_s]
    end
  end

  def localize (group_name, action_name, debug = nil)
    localized = LocalizedMessage.instance[group_name, action_name]
  rescue LocalizedMessage::EntryNotFound
    ActiveRecord::Base.logger.debug("Missing localized entry for '#{group_name}'=>'#{action_name}'!! Check config/localized.yml")
  rescue => err
    ActiveRecord::Base.logger.debug("localize error: #{err} in ('#{group_name}'=>'#{action_name}'")
  ensure
    return localized || Inflector::humanize(action_name)
  end

  def localized_columns (singular_name_or_klass)
    table_name  = singular_name2table_name(singular_name_or_klass)
    @@localized_columns ||= {}
    @@localized_columns[table_name] ||= YAML::load_file("#{RAILS_ROOT}/db/localized/#{table_name}.yml")
  rescue ArgumentError => err
    ActiveRecord::Base.logger.debug("localize error: YAML #{err} in db/localized/#{table_name}.yml")
    @@localized_columns[table_name] = :load_error
  end

  def localized_column_name (klass, column_name)
    # return klass.human_attribute_name(column_name.to_s)
    localized_name = localized_columns(klass)['column_names'][column_name.to_s]
  rescue NoMethodError
  rescue => err
    ActiveRecord::Base.logger.debug("localize error(column_name): #{err}")
  ensure
    return localized_name || Inflector::humanize(column_name)
  end

  def localized_error_messages_for(object, options = {})
    options = options.symbolize_keys
    object  = instance_variable_get("@#{object}") unless object.is_a?(ActiveRecord::Base)
    if object.nil? or object.errors.empty?
      return nil
    else
      header_tag = options[:header_tag]    || localize(:error_messages_for, :options_header_tag)
      summary    = object.errors.count.to_s + localize(:error_messages_for, :errors_occurred, :debug)
      content_tag("div",
                  content_tag(header_tag, summary) +
                  content_tag("p", localize(:error_messages_for, :there_were_problems)) +
                  content_tag("ul", object.errors.collect { |attr, msg| content_tag("li", "%s%s" %
                    [attr == 'base' ? '' : localized_column_name(object.class, attr), localize(:error_messages, msg)])}),
                  "id" => options[:id] || "errorExplanation", "class" => options[:class] || "errorExplanation"
                  )
    end
  end

  def singular_name2table_name (name)
    case name
    when Class                  ; name.table_name
    when ActiveRecord::Base     ; singular_name2table_name(name.class)
    else                        ; singular_name2table_name(name.to_s.classify.constantize)
    end
  end

  def view_property (singular_name_or_klass, column_name)
    table_name  = singular_name2table_name(singular_name_or_klass)
    column_name = column_name.to_s

    @@view_properties ||= {}
    @@view_properties[table_name] ||= {}
    @@view_properties[table_name][column_name] ||=
      (ViewProperty.new(localized_columns(singular_name_or_klass)["property_#{column_name}"]) rescue :property_not_found)

    case (property = @@view_properties[table_name][column_name])
    when ViewProperty        ; return property
    else                     ; return nil
    end
  rescue NameError
    raise
  rescue
    @@view_properties[table_name][column_name] ||= :property_not_found
    ActiveRecord::Base.logger.debug("localize error (invalid master): (#{klass}, #{column_name})")
    return nil
  end

  def human_attribute_name (record, column_name)
    klass = record.is_a?(Class) ? record : record.class
    localized_column_name(klass, column_name)
  end

  def human_attribute_value (record, column_name)
    value    = record.send(column_name)
    property = view_property(record.class, column_name)
    property.human_value(value, self)  # self is a Controller or a View
  rescue
    value
  end

  def human_attribute_edit (singular_name, column_name)
    singular_name = singular_name.to_s
    property      = view_property(singular_name, column_name)
    case property
    when LocalizeHelper::ViewProperty
      property.human_edit(singular_name, column_name, self) # self should be a View
    else
      input(singular_name.to_s, column_name)
    end
  end

  def localized_label_class_on (record, column_name)
    if record.errors.on(column_name)
      LocalizedMessage.instance[:error_messages_for, :label_error_class] || 'labelWithErrors'
    else
      LocalizedMessage.instance[:error_messages_for, :label_class] || 'label'
    end
  end

  module_function :localize
  module_function :localized_columns
  module_function :localized_column_name
  module_function :view_property
  module_function :singular_name2table_name # for inner function
end
