module HabtmMigration
  module ClassMethods
    def active_records (*values)
      case values.size
      when 0 # getter
        @active_records || super()
      when 2 # setter
        klasses = values.collect{|value| value.is_a?(Class) ? value : value.to_s.classify.constantize}
        klasses.each do |klass|
          unless klass.ancestors.include?(ActiveRecord::Base)
            raise "active_records should be two ActiveRecord classes. (#{values.inspect})"
          end
        end
        klasses.sort!{|a,b| a.table_name<=>b.table_name}
        @active_records = klasses
      else
        raise "active_records should be two ActiveRecord classes. (#{values.inspect})"
      end
    end

    def table_name (value = nil)
      case value
      when NilClass
        @table_name || super()
      when Array
        @table_name = value
      else
        raise "Table name should be a string or a symbol. (#{value.inspect})"
      end
    end

    def options (value = nil)
      case value
      when NilClass
        @options || super()
      when Hash
        @options = value
      else
        raise "options should be an hash. (#{value.inspect})"
      end
    end
  end

  class Base < ActiveRecord::Migration
#    class_inheritable_reader :ignore_nesting

    class << self
      def active_records
        raise NotImplementedError, "subclass responsibility"
      end

      def table_name
        active_records.collect{|klass| klass.table_name}.join('_')
      end

      def options
        {}
      end

      def up
        create_table(table_name, options) do |table|
          active_records.each do |klass|
            name = "%s_id" % klass.table_name.singularize
            table.column(name, :integer)
          end
        end
      end

      def down
        drop_table(table_name) rescue :nop
      end
    end
  end

  class MySQL < Base
    extend ClassMethods
    options(:options => "ENGINE=InnoDB DEFAULT CHARSET=utf8")
  end

  class PostgreSQL < Base
    extend ClassMethods
  end

  class SQLite < Base
    extend ClassMethods
  end
end
