module HideField
  def self.append_features(base)
    super

    # Extend base with class methods to declare helpers.
    base.extend(ClassMethods)
  end

  module ClassMethods
    def hidden_fields
      read_inheritable_attribute(:hidden_fields) || []
    end

    # specify field names of a model those you want to hide
    # :only and :except takes action name(s)
    def hide_field (*args)
      options = Hash === args.last ? args.pop : {}

      options[:disabled] = true if args.empty?
      options[:fields]   = args.map { |o| o.to_s }
      options[:only]     = [*options[:only]].map { |o| o.to_s } if options[:only]
      options[:except]   = [*options[:except]].map { |o| o.to_s } if options[:except]
      if options[:only] && options[:except]
        raise ArgumentError, "only one of either :only or :except are allowed"
      end

      write_inheritable_array(:hidden_fields, [options])
      ActiveRecord::Base.logger.debug("hidden_field: adding new rule: %s" % options.inspect)
    end

    # an accessor method to tell whether the specified field should be hidden
    def hidden_field? (action_name, field_name)
      hidden_fields .each do |options|
        if options[:disabled]
          return false
        end

        if options[:only]
          if options[:only].include?(action_name)
            if options[:fields].include?(field_name)
              return true
            end
          else
            next
          end
        end

        if options[:except]
          if options[:except].include?(action_name)
            next
          else
            if options[:fields].include?(field_name)
              return true
            end
          end
        end

        if options[:fields].include?(field_name)
          return true
        end
      end

      return false
    end
  end
end
