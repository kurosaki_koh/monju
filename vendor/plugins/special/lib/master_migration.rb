module MasterMigration
  module ClassMethods
    def active_record (value = nil)
      case value
      when NilClass
        @active_record || super()
      when String, Symbol
        @active_record = value.to_s.classify.constantize
      when Class
        @active_record = value
      else
        raise "Masters should be an ActiveRecord class. (#{value.inspect})"
      end
    end

    def masters (value = nil)
      case value
      when NilClass
        @masters || super()
      when Array
        @masters = value
      else
        raise "Masters should be an array. (#{value.inspect})"
      end
    end

    def options (value = nil)
      case value
      when NilClass
        @options || super()
      when Hash
        @options = value
      else
        raise "Masters should be an hash. (#{value.inspect})"
      end
    end
  end

  class Base < ActiveRecord::Migration
    class << self
      def active_record
        raise NotImplementedError, "subclass responsibility"
      end

      def masters
        raise NotImplementedError, "subclass responsibility"
      end

      def options
        {}
      end

      def tablename
        active_record.table_name
      end

      def up
        create_table(tablename, options) do |table|
          table.column	:name	,:string	,:limit=>16
        end
        import_data
      rescue
        down
        raise
      end

      def down
        drop_table(tablename) rescue :nop
      end

      def import_data
        masters.each_with_index do |name, i|
          expected_id = i + 1
          master = active_record.create(:name=>name)
          unless master.id == expected_id
            raise "Import Error: id mismatch. expected=%d, result=%d, name=%s" % [expected_id, master.id, name]
          end
        end
      end
    end
  end

  class MySQL < Base
    extend ClassMethods
    options(:options => "ENGINE=InnoDB DEFAULT CHARSET=utf8")
  end

  class PostgreSQL < Base
    extend ClassMethods
  end

  class SQLite < Base
    extend ClassMethods
  end
end
