require_dependency 'hide_field'

ActionController::Base.class_eval do
  unless self.respond_to?(:hide_field)
    include HideField
  end
end

begin
  ActiveRecord::Errors.class_eval do
    LocalizeHelper::LocalizedMessage.instance.group(:error_messages) .each_pair do |attr, message|
      default_error_messages[attr.intern] = message
    end
  end
rescue NameError, Errno::ENOENT
  # jascaffold may be not installed yet.
end
