include IdressSearcher
require Rails.root.join('app/models/i_definition')

not_equal = {}
parser = OptionParser.new do |opts|
  opts.banner = <<-BANNER.gsub(/^          /, '')
    リザルトのインポート（個別アイドレス指定）
    Usage: #{File.basename($0)} [options]
    Options are:
  BANNER
  opts.separator ""
  opts.on('--import') {
    options[:import] = true
  }

  opts.on('--environment VALUE') {} #do nothing for "ruby script/runner"
  opts.parse!(ARGV)

end
puts ARGV
open(ARGV.shift) do |f|

  while !f.eof?
    line = f.readline.strip
    puts line

    cno, name, idress, note, url = line.split(/[：\t]/)

    owner_account = OwnerAccount.find_by_number(cno)

    if owner_account.name != name
      puts "name #{name} is not equal : #{owner_account.name}"
      next
    end

    odef = search_idress(idress)

    if odef.nil?
      puts "idress:#{idress} definition not found."
      next
    end
#    puts [cno, name, note, url, odef ? odef.name : "#not found"].join("\t")
    owner_account.add_object_definition(odef, url: url, note: note) if options[:import]
  end
end
