ARGV.shift
ARGV.shift

characters = ARGV.collect{|cno|
  p cno
  Character.find_by_character_no(cno)}


for ch in characters
  print ch.name.tosjis,"\n"
  count = 1
  for r in ch.results
    r.position = count
    count += 1
    r.save
  end
end