require 'yaml'
require 'kconv'

class String
  def is_binary_data?
    false
  end

  def decode
    gsub(/\\x(\w{2})/){[Regexp.last_match.captures.first.to_i(16)].pack("C")}
  end
end

ObjectSpace.each_object(Class){|klass|
  klass.class_eval{
    if method_defined?(:to_yaml) && !method_defined?(:to_yaml_with_decode)
      def to_yaml_with_decode(*args)
        result = to_yaml_without_decode(*args)
        if result.kind_of? String
          result.decode
        else
          result
        end
      end
      alias_method :to_yaml_without_decode, :to_yaml
      alias_method :to_yaml, :to_yaml_with_decode
    end
  }
}



filename = ARGV.shift

opt = ARGV.shift
data = YAML.load(open(filename))

ObjectRegistry.delete_all() if opt == '--clear'

not_found = []
for row in data
  item_name , note , url , number , table_name, owner_name = row
  target = row[4].constantize.find_by_name(owner_name)
  if target
    p target
    target.owner_account.object_registries.create(
      {:name => item_name,
      :number => number ,
      :note => ((note.nil? ? "" : note) + "\n" + url)
      }
    )
  else
    not_found << row
  end
end

open('rejected.yaml','w'){|f|
  f.print not_found.to_yaml
}