# -*- encoding: utf-8 -*-
require 'rubygems'
require 'open-uri'
require 'hpricot'
require 'kconv'
require 'yaml'
require 'csv'

class String
  def is_binary_data?
    false
  end

  def decode
    gsub(/\\x(\w{2})/){[Regexp.last_match.captures.first.to_i(16)].pack("C")}
  end
end

ObjectSpace.each_object(Class){|klass|
  klass.class_eval{
    if method_defined?(:to_yaml) && !method_defined?(:to_yaml_with_decode)
      def to_yaml_with_decode(*args)
        result = to_yaml_without_decode(*args)
        if result.kind_of? String
          result.decode
        else
          result
        end
      end
      alias_method :to_yaml_without_decode, :to_yaml
      alias_method :to_yaml, :to_yaml_with_decode
    end
  }
}



  
#/html/body/div[11]/div/div[2]/div/div/div/table

def parse_got(cell,item_name)
  links =  cell/:a
  target_link = links.find{|l| l.inner_text =~ /(入手)|(取得)|(購入)/}
  unless target_link
    target_link = links[-1]
  end
  return [item_name , cell.inner_text , target_link[:href],1]
end

NON_CONSUMED_KEYWORDS=['非消費','未使用','効果永続','着用型','装備','乗り物','施設','家']

def parse_used(cell,item_name)
  cell_text = cell.inner_text
  if NON_CONSUMED_KEYWORDS.find{ |keyword| cell_text =~ Regexp.new(keyword) }
    return []
  end
  links =  cell/:a
  return links.inject([]){|list,item| list << [item_name , item.inner_text , item[:href],-1]}
end

def parse_extra_note(cell)
  cell_text = cell.inner_text
  if NON_CONSUMED_KEYWORDS.find{|keyword| cell_text =~ Regexp.new(keyword)} && (cell/:a).size > 0
    return cell.inner_html
  else
    nil
  end
end

def parse_table(table , cols)
  rows = table/:tr
  rows.shift
  
  result = []
  row_spans = []
  cols.times{row_spans << 0}
  prev_cells = []
  for row in rows
    reading_cells = row/:td
    next if reading_cells.to_s =~ /(colspan|現在所持数)/
    #puts row.to_s.tosjis
    cells = []
    cols.times{ cells << nil }

    for i in 0...cols
      if row_spans[i] > 0
        #       puts "## cell:#{i}"
        cells[i]= prev_cells[i]
        row_spans[i] -= 1
      else
        cell = cells[i] = reading_cells.shift
        #        puts "## cell:#{i}, #{cell.to_s.tosjis} , #{reading_cells.to_s.tosjis}"
        row_spans[i] = cell['rowspan'].to_i - 1
      end
    end
    prev_cells = cells
    result << cells
  end
  return result
end

def parse_nations_own(table,name)
  rows = parse_table(table,4)
  result = []
  for cells in rows
    item_name = cells[0].inner_text
    note = parse_got(cells[1],item_name)
#    note = cells[1].inner_text

    used_tags = parse_used(cells[2],item_name)
#    used_notes = used_tags.inject([]){|list,item| list << [item.inner_text,item[:href]]}
#    note.each{|n| 
      note << 'Nation'
      note << name
      extra_note = parse_extra_note(cells[2])
      note[1] += extra_note  if extra_note
#    } 
    
    result << note
    if used_tags.size > 0
      used_tags.each{|ut|
        ut << 'Nation'
        ut << name}
      result += used_tags
    end
  end
  return result
end

def parse_characters_own(table)
  rows =   parse_table(table,5)
  result = []
  for cells in rows
    item_name = cells[0].inner_text
    note = parse_got(cells[1],item_name)
#puts cells[1].to_s.tosjis
    #    note = cells[1].inner_text
    owner_name_str = cells[2].inner_text.gsub("\n",'')
    owner_name = owner_name_str.split(/(：)|(:)|(→)|(⇒)/)[0]
    used_tags = parse_used(cells[3],item_name)
#    used_notes = used_tags.inject([]){|list,item| list << [item.inner_text,item[:href]]}
#    note.each{|n| 
      note << 'Character'
      note << owner_name
      extra_note = parse_extra_note(cells[3])
      note[1] += extra_note if extra_note
#    }
    
    result << note 
    if used_tags.size > 0
      used_tags.each{|ut|
        ut << 'Character'
        ut << owner_name
      }
      result += used_tags
    end
  end
  return result
end

def parse_characters_house(table)
  rows =   parse_table(table,5)
  result = []
  for cells in rows
    item_name = cells[0].inner_text
    note = parse_got(cells[1],item_name)
#puts cells[1].to_s.tosjis
    #    note = cells[1].inner_text
    owner_name_str = cells[2].inner_text.gsub("\n",'')
    owner_name = owner_name_str.split(/(：)|(:)|(→)|(⇒)/)[0]
    used_tags = parse_used(cells[3],item_name)
#    used_notes = used_tags.inject([]){|list,item| list << [item.inner_text,item[:href]]}
#    note.each{|n| 
      note << 'Character'
      note << owner_name
#    }
    result << note
  end
  return result
end

opt = ARGV.shift
if opt
  links = [opt]
  nations = [ARGV.shift.toutf8 ]
  urls = {nations[0] => opt}
else
  index_url = 'http://www35.atwiki.jp/marsdaybreaker/pages/151.html'
  doc = Hpricot(open(index_url).read.toutf8)
  links = doc / 'div/h3/a'
  doc = Hpricot(open('http://www35.atwiki.jp/marsdaybreaker/pages/146.html').read.toutf8)
  links += doc / 'div/h3/a'
  nations = links.map{|l|l.inner_html}
  urls = links.inject({}){|hash , link | 
    hash[link.inner_html]=link[:href]
    hash
  }
end

#p urls
result = []
#wout = open('result.yaml','w')
for key in nations
#  print ('●●' + key).tosjis , ":",urls[key],"\n"
  nation_page = Hpricot(open(urls[key]).read.toutf8)
  
  tables = nation_page / 'div#wikibody/table'
  
  headers =  (nation_page/'h3').map{|h|h.inner_text}
  #puts headers.to_s.tosjis
  #  puts parse_nations_own(tables[0]).to_yaml.tosjis
  hankoku = headers.find{|h| h =~ /国/} ? tables.shift : nil

  skills = headers.find{|h| h =~ /技術/ } ? tables.shift : nil
  smiles = headers.find{|h| h =~ /勲章/ } ? tables.shift : nil
  personal = headers.find{|h| h =~ /^個人所有$/} ? tables.shift : nil
  personal_furnitures = headers.find{|h| h =~ /施設/} ? tables.shift : nil
  
#  puts '国有'.tosjis , parse_nations_own(hankoku,key).to_yaml.tosjis if hankoku
#  puts '・個人'.tosjis, parse_characters_own(personal).to_yaml.tosjis if personal
#  puts '・個人施設'.tosjis, parse_characters_house(personal_furnitures).to_yaml.tosjis if personal_furnitures

  result += parse_nations_own(hankoku,key) if hankoku
  result += parse_characters_own(personal) if personal
  result += parse_characters_house(personal_furnitures) if personal_furnitures
#  YAML.dump(parse_nations_own(hankoku,key),wout) if hankoku
#  YAML.dump(parse_characters_own(personal),wout) if personal
#  YAML.dump(parse_characters_house(personal_furnitures),wout) if personal_furnitures

end
#CSV.open('result.csv','w'){|f|
#  for row in result
#    f << row
#  end
#}

print result.to_yaml