# -*- encoding: utf-8 -*-
require 'rubygems'
require 'open-uri'
require 'hpricot'
require 'kconv'

index_url = 'http://www33.atwiki.jp/t-eyes/pages/12.html'

doc = Hpricot(open(index_url).read.toutf8)

targets =<<EOT
天領
るしにゃん王国
ａｋｉｈａｒｕ国
フィールド・エレメンツ・グローリー
海法よけ藩国
鍋の国
レンジャー連邦
ながみ藩国
ジェントルラット藩国
世界忍者国
玄霧藩国
土場藩国
よんた藩国
後ほねっこ男爵領
ナニワアームズ商藩国
フィーブル藩国
Flores valerosas bonitas
詩歌藩国
人狼領地
ビギナーズ王国
キノウツン藩国
紅葉国
羅幻王国
たけきの藩国
ヲチ藩国
になし藩国
芥辺境藩国
越前藩国
無名騎士藩国
リワマヒ国
ゴロネコ藩国
神聖巫連盟
暁の円卓藩国
アウトウェイ
都築藩国
悪童同盟
星鋼京
愛鳴之藩国
EOT

#/html/body/div[12]/div/div/div[2]/div/div/div/h3[9]/a
links = doc / 'div/h3/a'

nations = links.map{|l|l.inner_html}
urls = links.inject({}){|hash , link | 
#  puts link.inner_html
  hash[link.inner_html]=link[:href]
  hash
}


def parse_mile_list(url)
  doc = Hpricot(open(url))
  rows = doc / 'div#mainbody2/table[1]/tr'
  rows.shift
  rows.inject([]){|list , row|
    cells = row/'td'
    list <<  [cells[0].inner_html , cells[1].inner_text,  cells[3].inner_html.to_i]
    list
  }
end

character_no_different = []
not_found = []

for key in targets.map{|line| line.chomp}
  list = parse_mile_list(urls[key])
  
  for row in list
    owner_account = nil
    if row[0] =~ /\d{2}-\d{5}(-\d{2})?/
      character_no = row[0]
      character_no += '-01' if character_no =~ /^\d{2}-\d{5}$/

      c = Character.find_by_character_no(character_no)
      if c
        db_name = c.name
      else
        
        c = Character.find_by_name(row[1])
        if c
          db_name = c.name
          character_no_different << [c,key,urls[key]]
        else
          db_name = 'not found'
          row << urls[key]
          not_found << row
          c = nil
        end
      end
      owner_account = c.owner_account if c
      name = row[1]
      puts [character_no , name , db_name , row[2]].join(',')
      
    elsif row[1] =~ /口座/
      row[0] = key
      nation = Nation.find_by_name(key)
      row[0] = nation.id.to_s + key if nation 
      puts row.join(',')
      owner_account = nation.owner_account
    else
      row << urls[key]
      not_found << row
    end
    next if owner_account.nil?
    
    owner_account.mile_changes.clear
    mc = MileChange.new
    mc.reason = 'キノウツン旅行社より繰り越し'
    mc.note = Time.now.strftime("%y%m%d")
    mc.mile = row[2].to_i
    mc.owner_account = owner_account
    mc.save
  end
end
puts "\ncharadcer_no not found."
for r in character_no_different
  character , nation_name , url = r
  puts [character.character_no , character.name,nation_name , url].join(',')
end
puts "\nnot found."
for r in not_found
  puts r.join(',')
end
