# -*- encoding: utf-8 -*-

puts RUBY_VERSION , RUBY_PATCHLEVEL

require 'nokogiri'
require 'open-uri'
require 'yaml'
require 'uri'

TREASURE_LOG_URL_FORMAT = "http://kaiho.main.jp/takarachat/%s_%d.html"


def parse_treasure_log(character_no , url )
  puts character_no
  result = {}
  #for i in 1..6
#    url = format(TREASURE_LOG_URL_FORMAT , character_no , i) unless url
    begin
      enc = url =~ /kaiho/ ? 'utf-8' : 'Windows-31J'
      html = open(url,"r:#{enc}").read
      html.encode!('utf-8') if url =~ /cwtg/
#      puts
      treasures = if url =~ /kaiho/
        html.scan(/(?:表.より、)?(.+)をゲット！/)
                  else
                    html.scan(/－獲得賞品\<br\>.・(.+?)\</)
                  end

      result[:url] = url
      result[:result] = treasures.flatten
    rescue
      puts $! , $@
      puts url
    end
#  end
  result
end

lists = ['http://gamechaki.kotonet.com/claire/EXD_tacara.html' ,
         'http://gamechaki.kotonet.com/claire/EXD_tacara2.html' ]


$log_url = Hash.new{|h,k| h[k] = []  }

def parse_tacara_table(url)
  html = open(url , 'r:sjis').read

  doc = Nokogiri::HTML.parse(html , nil ,'sjis')

  rows= doc.search('tr')
  for row in rows
    cno , name=  row.search('td:nth-of-type(1)').map{|t|t.inner_text}.first.split('：')
    logs = row.search('object').map{|tag|tag['data']}
    $log_url[cno] += logs
  end
end


parse_tacara_table(lists[0])
parse_tacara_table(lists[1])

#蝶子ヤガミのURL修正
$log_url['06-00147-x1'] = (1..6).map{|i|'http://kaiho.main.jp/takarachat/06-00147-01_%E9%9C%B0%E7%9F%A2%E6%83%A3%E4%B8%80%E9%83%8E' + "#{i}.html"}

#優しいカールのURL修正
$log_url['06-00735-x1'][0] = "http://kaiho.main.jp/takarachat/06-00735-011_%E5%84%AA%E3%81%97%E3%81%84%E3%82%AB%E3%83%BC%E3%83%AB1.html"
#puts $log_url.to_yaml

hash = {}

count = 0
$log_url.each do |cno , urls|
  for url in urls
    hash[cno] ||= []
    url = URI.escape(url) unless url.include?('%')
    result =  parse_treasure_log(cno , url)
    puts result.to_yaml

#    hash[cno] = result[:result] unless result[:result].nil?
    hash[cno] << result
    sleep(0.1)
  end
#  next if cno =~ /[a-z]/
  count += 1
#  break if count == 10

end

puts hash.to_yaml