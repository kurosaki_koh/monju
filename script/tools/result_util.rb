# -*- encoding: utf-8 -*-

ENV["RAILS_ENV"] ||= 'development'
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'config', 'environment'))

require 'csv'
require 'yaml'

DATA_PATH = File.join(File.dirname(__FILE__), 'data')
SOURCE_PATH = File.join(DATA_PATH, 'dice_log2.yaml')
CLARE_PATH =  File.join(DATA_PATH, 'dungeon_dice_result2.csv')

$source = YAML.load_file(File.join(DATA_PATH, 'trade_results.yaml'))
$clare = {}
CSV.foreach(CLARE_PATH) do |row|
  $clare[row[0]] = row
end

NOTE_STR = {
    '1' => '迷宮競技会 B10リザルト',
    '2' => '迷宮競技会 B20リザルト',
    '3' => '迷宮競技会 B30リザルト',
    '4' => '迷宮競技会 B40リザルト',
    '5' => '迷宮競技会 B50リザルト',
    '6' => '迷宮競技会 B59リザルト'
}

def create_note(url)
  if url =~ /cwtg/
    return '迷宮競技会 B10リザルト'
  end
  if url =~ /(\d)\.html/
    return NOTE_STR[$1]
  end
  return '迷宮競技会リザルト'
end


class AcePcTable

  CHARACTER_NO_FORM = /^\d{2}-(\d{5})-\d{2}$/

  def initialize()
    #龍樹さんは迷宮にエントリーしてないので、予めハッシュに入れておく。
    @ace_pc_table = {
        '00762' => '12-00762-01'
    }
    $source.keys.each { |cno|
      if cno =~ CHARACTER_NO_FORM
        @ace_pc_table[$1] = cno
      end
    }
  end

  def pc_from_ace(cno)
    result = @ace_pc_table[cno[3..7]]

    result ? result : cno[0, 2]
  end

  def self.ace_no?(cno)
    cno !~ CHARACTER_NO_FORM
  end
end

$table = AcePcTable.new
