# -*- encoding: utf-8 -*-
require 'jiken/util/stdout_hook'

YAML_FILE = './idefs.yml'

yaml_defs = YAML.load(open(YAML_FILE))
options = {:import => false}
mandatory_options = %w(  )
parser = OptionParser.new do |opts|
  opts.banner = <<-BANNER.gsub(/^          /,'')
    コンパイル済み定義のインポート
    Usage: #{File.basename($0)} [options]
    Options are:
  BANNER
  opts.separator ""
  opts.on('--import'){
    options[:import] = true
  }
  
  opts.on('--environment VALUE'){} #do nothing for "ruby script/runner"
  opts.parse!(ARGV)
  if mandatory_options && mandatory_options.find { |option| options[option.to_sym].nil? }
    stdout.puts opts; exit
  end
end

class IDefinition
  def before_save
    #do nothing
  end
end

for key in yaml_defs.keys
  ydef = yaml_defs[key]
  idef = IDefinition.find(:first , :conditions => ["name = ? and revision is NULL" , key])
  hash = ydef
  flag = false
  if idef
    if idef.compiled_hash != hash
      print "Updateded:#{key}" 
      flag = true
    end
  else
    print "Not found:#{key}"
    flag = true
    idef = IDefinition.new if options[:import]
  end
  if options[:import] && idef.compiled_hash != hash
    idef.name = key
    idef.object_type = ydef['種別']
    idef.definition = ydef['原文']
    idef.compiled_hash = hash
    idef.save
    print ": saved."
  end
  puts if flag
end

for idef in IDefinition.find(:all , :conditions => "revision is null")
  klass = case idef.object_type
  when /職業/
    Job
  when /(種族|人)$/
    Race
  else
    next
  end
  rec = klass.find_by_name(idef.name)
  if rec.nil? || Job::AbilityKanji.keys.any?{|item|
      rec[key] != idef.evaluation[Job::AbilityKanji[key]]
    }

    print idef.name
    if options[:import]
      idef.to_record.save
      print " converted to #{klass.name}."
    end
    puts
  end
end
if options[:import]
  prop = Property.find_by_name('i_definition_updated_at')
  prop.value = Time.new.strftime('%Y/%m/%d %H:%M:%S')
  prop.save
end
