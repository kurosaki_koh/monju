ObjectDefinition.all.each{|od|
  if od.is_idress3_format?
    if od.command_resource_definitions.size != od.parsed_command_resources.size
      od.register_command_resource_definitions
      puts od.name
    end
  end
}

CommandResource.all.each{|cr|
  if cr.command_resource_definition.nil?
    cr.command_resource_definition = CommandResourceDefinition.where(name: cr[:name]).first
    cr.save
  end
}