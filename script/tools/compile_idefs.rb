require 'i_language/parser/compiler.rb'
require 'uconv'
$KCODE='u'

compiler = ILanguage::Compiler.new(ARGV)
max = compiler.source.key_list.size
i = 1
compiler.compile_all{|name|
  puts Uconv.u8tosjis(sprintf("%s (%d/%d)",name , i  , max))
  i += 1
}
