# -*- encoding: utf-8 -*-8
require 'google_drive'
require 'optparse'
params = ARGV.getopts("" ,* %w"password: environment: import verbose")

puts params.inspect
session = GoogleDrive.login("admin@echizen.wanwan-empire.net", params["password"])

ws = session.spreadsheet_by_key("0An1Ed-nCKHhPdFRxU1MtOGFYZFI0aVpFY0poSFFOMlE").worksheets[0]

dungeon_fix = Struct.new('DungeonFix' , :character_no , :name , :source , :url)

entries = []
Title = '迷宮突破後エントリー（施設補正抜き）'

for row in 2..ws.num_rows
  puts [ws[row , 2] , ws[row , 3]].join('：') if params['verbose'] || params['import']
  entry =  dungeon_fix.new(ws[row,2] , ws[row,3] , ws[row,10] , ws[row , 8])
  # puts entry.source
  #
  owner_account = OwnerAccount.find_by_number(entry.character_no)

  if owner_account.nil?
    puts "not found:#{entry.character_no} , #{entry.name}"
    next
  end

  if owner_account.convert_sources.where(name: entry.name).first
    puts "duplicated, skip.: #{entry.character_no} , #{entry.name}"
    next
  end

  if params['import'] && owner_account
    cs = owner_account.convert_sources.build(name:  entry.name, authority_url: entry.url   , note: Title + '：'+entry.name)
    cs.source = entry.source
    cs.save
    cs.reload
    puts cs.inspect
    cs.rebuild_command_resources
  end
end

