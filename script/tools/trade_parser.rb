# -*- encoding: utf-8 -*-
require 'nokogiri'
require 'csv'
require 'yaml'
require 'open-uri'


DATA_PATH = File.join(File.dirname(__FILE__) , 'data')
TRADE_PATH = File.join(DATA_PATH, 'trade_auth.txt')

source = open(TRADE_PATH , 'r:utf-8').read
$trades = []
count = 0

class TradeData
  attr_accessor  :trade_id , :side ,:content , :traders  , :url


  def initialize(line)
    @trade_id , @url = line.split('：')
    @side = []
    @traders = {}

    parse
  end

  def parse
    html = open(@url,'r:Windows-31J').read
    content_html =  Nokogiri::HTML.parse(html , nil ,'Windows-31J').search('body div p font:nth-of-type(1)').first
    content_html.search('br').each{|node| node.replace("\n") }
    @content =  content_html.inner_text

    trader_lines = @content.scan(/\d{4}(?:\:|：)(.+?)(?:\:|：)(.+?)(?:\:|：)(.+?)(?:\:|：)(.+?)$/)

    names = {}
    for line in trader_lines
      character_no , name , item , url = line
      names[character_no] = name
      @traders[character_no] ||= []
      @traders[character_no] << {item: item , url: url}
    end

    @traders.each do |character_no , items|
      @side << TradeSide.new(character_no ,  names[character_no] , @traders[character_no])

    end
  end


  def to_hash
    {trade_id: @trade_id ,
     url: @url,
     side: @side.map{|s|s.to_hash}
    }
  end

  def form
    self.to_hash.to_yaml
  end
end

class TradeSide
  attr_accessor :character_no , :name , :items

  def initialize(character_no , name , items)
    @character_no = character_no
    @name = name
    @items = items.map{|i|TradeItem.new(i[:item] , i[:url])}
  end

  def to_hash
    {character_no: @character_no,
     name: @name ,
     items: @items.map{|i|i.to_hash}}
  end
end

class TradeItem
  attr_accessor :item_name , :auth_url

  def initialize(name , url)
    @item_name = name
    @auth_url = url
  end

  def to_hash
    {name: @item_name , url:@auth_url}
  end

end

count = 0
$trades = {}

for line in source.each_line
  trade = TradeData.new(line.strip)
  $trades[trade.trade_id] = trade.to_hash
  puts trade.content
#  puts trade.traders.to_yaml
  puts trade.form
  count += 1
  puts "############" * 10
#  break if count = 10
end

puts $trades.to_yaml