#require 'i_language'
require 'yaml'
include ILanguage

class String
  def is_binary_data?
    false
  end

  def decode
    gsub(/\\x(\w{2})/){[Regexp.last_match.captures.first.to_i(16)].pack("C")}
  end
end

ObjectSpace.each_object(Class){|klass|
  klass.class_eval{
    if method_defined?(:to_yaml) && !method_defined?(:to_yaml_with_decode)
      def to_yaml_with_decode(*args)
        result = to_yaml_without_decode(*args)
        if result.kind_of? String
          result.decode
        else
          result
        end
      end
      alias_method :to_yaml_without_decode, :to_yaml
      alias_method :to_yaml, :to_yaml_with_decode
    end
  }
}

#source = Uconv.sjistou8(open(DEF_JOBS_PATH).read)
dir = File.dirname(File.expand_path(__FILE__))
filename = File.join(dir,'tools/i_def.txt')
p filename
source = Uconv.sjistou8(open(filename).read)
parser = ILanguageLibraryParser.new

result =  parser.parse(source) 
#result =  parser.parse(open('i_def2.txt').read.toutf8) 


if result.nil?
  puts parser.terminal_failures.join("\n").to_s
  puts parser.failure_line
  puts parser.failure_reason
  exit
end

libraries = result.to_a
for lib in libraries
  case lib.node_type
  when :comment
    puts lib.to_s
  else
#    puts lib.name + "#"
#    puts lib.text_value
#    puts lib.content + "#"
#    puts special_text(lib.content)
#    specials = special_text(lib.content).map{|line| parse_special(lib.name , line)}
    begin
      parsed = Library.new(lib.text_value)
      specials = parsed.specials 
    rescue ArgumentError => e
      puts e.message.tosjis
      exit
    end

    puts parsed.name.tosjis , parsed.object_type.tosjis
    for value in specials
      next if value.nil?
#      eff = value[:effects]
#      puts eff.to_yaml
#      puts value.to_yaml.tosjis
    end
  end
#      effect_text = "type:#{eff[:effect_type]} "
#      case eff[:effect_type]
#      when :event_cost
#        effect_text += "event_cost:#{eff[:cost]}"
#      when :restriction
#        effect_text += "restriction:#{eff[:condition]}"
#      when :availables
#        effect_text += "availables:#{eff[:availables]}"
#      when :action
#        effect_text += "cond:#{eff[:condition]} judge:#{eff[:judge_type]} "
#        if eff[:success]
#          effect_text += "success:true"
#        else
#          effect_text += "bonus:#{eff[:bonus]} fuel:#{eff[:fuel_cost]} food:#{eff[:food_cost]}"
#        end
#      else
##        p value
#        effect_text += value[:text].to_s
#      end
#      puts value[:source].strip
#      puts "name:#{value[:special_name]} action:#{value[:action_name]} soldier_type:#{value[:soldier_type]} cond:#{value[:cond_type]}\n effect:{ #{effect_text} }"
#      print "\n"
#    end
#  end
end  