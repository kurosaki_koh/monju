# -*- encoding: utf-8 -*-

#リザルト一括登録用の書式を生成する。

require_relative('result_util')

$mile_total = 0
$errors = []
$miles = []
$originators = []

$idress = []

class Ace
  def nation
    self.related_character.nation
  end
end

def create_result_form(cno , oc , result , note , log)
  [cno , oc.try(:name) ,result , note  , log[:url] ].join('：')
end

$source.each do |cno , logs|
  ace_name = nil
  if AcePcTable.ace_no?(cno)
    ace_name =  $clare[cno][1] if $clare[cno]
    cno = $table.pc_from_ace(cno)
  end
  oc = OwnerAccount.find_by_number(cno)
  for log in logs
#    puts log.to_yaml
    next unless log[:result]
    note =  create_note(log[:url])
    note += " 個人ＡＣＥ（#{ace_name}）より" if ace_name

    for r in log[:result]
      case r
        when /マイル$/
          m = r.tr('０-９','0-9').to_i
          $mile_total += m
          puts log.to_yaml if cno.nil?
          $miles << [cno , oc.try(:name) ,note  , m , '' , log[:url] ].join('：')
        when /根源力/
          originator = r.tr('０-９','0-9').gsub('根源力' , '').to_i * 10000
          $originators << [cno , oc.try(:name) ,originator , note  , log[:url] ].join('：')
        when /民間用/
          idress_name  = oc.owner.nation.belong == :republic ?  '民間用ペルシャ' : '民間用ケント'
          $idress << create_result_form(cno , oc , idress_name , note , log)
        when /犬士／猫士/
          idress_name  = oc.owner.nation.belong == :republic ?  '個人用猫士' : '個人用犬士'
          $idress << create_result_form(cno , oc , idress_name , note , log)
        when 'プロモチケット'
          $idress << create_result_form(cno , oc , '１／１プロモチケット' , note , log)
        else
           $idress << create_result_form(cno , oc , r , note , log)
#          log[:error] = r
#          $errors << log.dup
      end
    end
  end
end

$miles.sort!
$miles << ['00' , '天領' , '迷宮競技会 マイル報酬支払い' , -$mile_total , ''].join('：')
puts $miles
puts

#秋春さん特例の追加
$originators. << "02-00027-01：涼原秋春：200000：迷宮競技会 B50リザルト 個人ＡＣＥ（岩崎仲俊）より。関連質疑（ http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=16586 ） ：http://kaiho.main.jp/takarachat/02-00028-01_%E5%B2%A9%E5%B4%8E%E4%BB%B2%E4%BF%8A5.html"

puts $originators.sort
puts

puts $idress.sort

#puts $errors