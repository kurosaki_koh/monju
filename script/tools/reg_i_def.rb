require 'i_language'
include ILanguage

source = Uconv.sjistou8(open(DEF_JOBS_PATH).read)
parser = ILanguageLibraryParser.new
result =  parser.parse(source) 

if result.nil?
  puts parser.terminal_failures.join("\n").to_s
  puts parser.failure_line
  puts parser.failure_reason
  exit
end

libraries = result.to_a
for lib in libraries
  case lib.node_type
  when :comment
    #do nothing 
    #puts lib.to_s
  else
    ygg = Job.find_by_name(lib.name)
    if ygg
      ygg.definition = lib.text_value
      ygg.save
    else
      puts "#{lib.name} not found."
    end
  end
end