# -*- encoding: utf-8 -*-
characters = Character.find(:all , :order => "nation_id , character_no")

puts '#国民番号,キャラクター名,種族,性別,特記事項'
for ch in characters
  gender = case ch.gender
  when 'male'
    '男'
  when 'female'
    '女'
  when 'undefined'
    ''
  else
  end
  puts [ch.character_no , ch.name , ch.race_type , gender , ch.note].join(',')
end