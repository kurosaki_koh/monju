# -*- encoding: utf-8 -*-
require 'rubygems'
require 'open-uri'
require 'hpricot'
require 'kconv'

index_url = 'http://www28.atwiki.jp/i-dress_zaimu/pages/178.html'

doc = Hpricot(open(index_url).read.toutf8)

rows = doc / '//div[@id="mainbody2"]/table/tr'
#/html/body/div[12]/div/div/div[2]/div/div/div/table

def acquired?(cell)
  cell[:style]=='color:red;'
end

def acquire(character,job_name)
  ji = JobIdress.find_by_name(job_name)
  if ji.nil?
    puts "#{job_name} not found for #{character.character_no}:#{character.name}."
    return
  end
  if character.acquired_idresses.find_by_name(job_name)
    puts "#{character.character_no}:#{character.name} already acquires #{ji.name}."
  else
    character.acquired_idresses << ji 
    puts "#{character.character_no}:#{character.name} acquires #{ji.name}"
  end
end

def acquire_job4(character_no , name , rizoku , hoshimi ,houkan, gomin , sanbou ,etc1 , etc2)
#  puts [character_no,name,rizoku,hoshimi,houkan,gomin,sanbou,etc1,etc2].join(',')
  pairs = ['吏族','星見司','法官','護民官','参謀'].zip([rizoku,hoshimi,houkan,gomin,sanbou])
  character = Character.find_by_character_no(character_no)
  if character == nil then
    puts "#{character_no}(#{name}) not exist."
    return
  end
  for pair in pairs
    next unless pair[1]
    acquire(character , pair[0])
  end
  for etc_name in [etc1,etc2]
    next if etc_name.nil?
    next if etc_name.strip.empty?
    acquire(character , etc_name)
  end
end

for row in rows
  cells = row / 'td'
  next unless cells[0].inner_text =~ /\d\d-\d{5}-\d\d/
  character_no = cells[0].inner_text.strip
  name = cells[1].inner_text.strip
  rizoku , hoshimi,houkan,gomin,sanbou = cells[3..7].map{|cell| acquired?(cell)}
  etc1 = etc2 = nil
  #  rizoku = cells[3][:style]
#  hoshimi = cells[4].inner_text.strip
#  houkan = cells[5].inner_text.strip
#  gomin = cells[6].inner_text.strip
#  sanbou = cells[7].inner_text.strip
  etc1 = cells[8].inner_text.strip if cells[8]
  etc2 = cells[9].inner_text.strip if cells[9]
  acquire_job4(character_no,name,rizoku,hoshimi,houkan,gomin,sanbou,etc1,etc2)
end