# -*- encoding: utf-8 -*-
require 'kconv'
data = [
  ['吏族',0],
  ['星見司',0],
  ['法官',0],
  ['護民官',0],
  ['参謀',0],
  ['秘書官',0],
  ['シオネ・アラダの守り手',0],
  ['ヤガミの恋人',5],
  ['宇宙艦長',17],
  ['神官',18],
  ['大神官',18],
  ['ぽちの騎士',29],
  ['ゲーマー',30],
  ['補給士官',34]
]

for d in data
  puts d.join(',').tosjis
  name = d[0]
  no = d[1]
  job4_ygg = Job.find_by_name(name)
  if j = JobIdress.find_by_job4_id(job4_ygg.id)
    puts "#{j.name} exists.".tosjis
    next
  else
    ji = JobIdress.new
    ji.job4 = job4_ygg
    ji.owner_account = Nation.find(no).owner_account
    ji.idress_type = 'PlayerIdress'
    ji[:name] = job4_ygg.name
    ji.save
    puts "#{ji.name} registered for #{ji.owner_account.owner.name}.".tosjis
  end
end
