# -*- encoding: utf-8 -*-

#Ace の国民番号付与を行う。

ENV["RAILS_ENV"] ||= 'development'
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'config', 'environment'))

characters = {}

Ace.all.each{|ace|
  ace.bonds.each{|b|
    characters[b.character] = b
  }
}

characters.keys.sort_by{|ch| ch.character_no}.each do |ch|
  character_no_base  = ch.character_no[0,9]
  index = 1
  puts ch.character_no + "\t" + ch.name
  ch.bonds.sort_by{|b|b.id}.each do |b|
#    if b.ace.object_definition
      puts character_no_base + 'a' + index.to_s  + "\t" + b.ace.name
      index += 1
#    end
  end
  puts
end