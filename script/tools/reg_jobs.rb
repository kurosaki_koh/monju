# -*- encoding: utf-8 -*-
require 'kconv'
not_registereds = IDefinition.find_by_sql <<SQL
SELECT i_definitions.name
FROM i_definitions LEFT OUTER JOIN yggdrasills ON i_definitions.name = yggdrasills.name
WHERE yggdrasills.name is NULL and i_definitions.object_type like '%職業%'
SQL

for rec in not_registereds
  print rec.name
  idef = IDefinition.find_by_name(rec.name)
  print( (!idef.nil?).to_s + ":")
  if idef.nil?
    puts
    next
  end
  print( idef.name + ':')
  record = idef.to_record
  print( (!record.nil?).to_s + ':')
  puts record.save
end

def evals_equal?(ygg,idef)
  Job::AbilityKanji.keys.all?{|key|
    ygg[key] == idef.evaluation[Job::AbilityKanji[key]]
  }
end

yggs = Job.find(:all)

for ygg in yggs
  idef = IDefinition.find_by_name(ygg.name)
  unless idef
    puts "Definition of #{ygg.name.tosjis} not exist."
    next
  end

  unless evals_equal?(ygg,idef)
    puts "#{ygg.name} evaluation is not equal."
    puts Job::Abilities.collect{|c| "#{c}:#{ygg[c]}" }.join(' ')
    puts Job::Abilities.collect{|c| "#{c}:#{idef.evaluation[Job::AbilityKanji[c]]}"}.join(' ')
    Job::AbilityKanji.keys.each{|key|
      ygg[key] = idef.evaluation[Job::AbilityKanji[key]]
    }
    ygg.save
  end
end