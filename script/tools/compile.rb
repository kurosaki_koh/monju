require Rails.root + '/config/boot'
require Rails.root + '/lib/i_language/parser/compiler.rb'

options = { :environment => (ENV['RAILS_ENV'] || "development").dup }

compiler = ILanguage::Compiler.new
op = compiler.optparser
op.on('-e'){ |v| options[:environment] = v }

op.parse(ARGV)
ENV["RAILS_ENV"] = options[:environment]
RAILS_ENV.replace(options[:environment]) if defined?(RAILS_ENV)

require RAILS_ROOT + '/config/environment'

compiler.parse_option(ARGV)
max = compiler.source.key_list.size
i = 1
compiler.compile_all{|name|
  puts sprintf("%s (%d/%d)",name , i  , max)
  i += 1
}