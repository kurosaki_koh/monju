# -*- encoding: utf-8 -*-

#クレールさんのリザルトまとめと、交換会のデータを掛けあわせて、交換後のデータを作成する。

require 'csv'
require 'yaml'
require 'uri'

DATA_PATH = File.join(File.dirname(__FILE__) , 'data')
SOURCE_PATH = File.join(DATA_PATH, 'dice_log2.yaml')

Source = YAML.load_file(File.join(DATA_PATH, 'dice_log2.yaml') )
trades = YAML.load_file(File.join(DATA_PATH, 'trade_data.yaml') )


Source.each do |cno , ch|
  ch.each do |r|
    next if r[:result].nil?
    r[:result].each{|item|
      item.gsub!(/（(施設|アイテム|技術|職業?４|イベント|再録|職業)）$/ , '')
      item.gsub!(/まあ待て落ち付け/,'まあまて落ち着け')
      item.gsub!(/発行信号/,'発光信号')
      item.gsub!(/真珠の指輪$/,'真珠の指輪Ｄ')
    }
  end
end


def search_target(url , character_no)
  puts character_no , url
  ch_results = Source[character_no]
  result = search_target_from_results(ch_results , url)
  unless result
    p_no = character_no[3..7]
    targets = Source.select{|cno , r|cno =~ /#{p_no}/ }

    ch_results = targets.find{|tuple|
      search_target_from_results(tuple.last  , url)
    }
    return nil, nil unless ch_results
    result = search_target_from_results(ch_results.last , url)
  end
  return character_no , result
end

def search_target_from_results(ch_results , url)
  ch_results.find{| r|
    r[:url] == url
  }
end

trades.each do |trade_id , trade|
  characters = []
  got_items = []
  if trade[:side].size != 2
    puts trade.to_yaml
    exit
  end
  trade[:side].each do |side|
    ch_results = Source[side[:character_no]]
    characters << ch_results

    sold_items = []
    side[:items].each do |item|
      url = item[:url].include?('%') ?item[:url] :  URI.encode(item[:url])
#      url = item[:url]
      ch_results , target = search_target(url ,side[:character_no] )
#      target = ch_results.find{|r|r[:url] == item[:url]}
      begin
#        puts trade.to_yaml
#        puts ch_results.to_yaml
#        puts target.to_yaml
#        puts item.to_yaml

        idx =  target[:result].index{|i|
          i ==  item[:name]
        }
#        puts( {item_name: item[:name] , idx: idx}.to_yaml )
        target[:result].delete_at(idx )
        sold_items << item[:name]
      rescue
        puts ch_results.to_yaml
        puts target.to_yaml
        puts item.to_yaml
        raise $!
      end
    end
    got_items.push sold_items
  end
  characters.each do |ch_results|
    note = "迷宮リザルト交換会・交換番号#{trade_id}で入手。 #{ trade[:url] }"
#    items = got_items.pop
    trade_result = {url: trade[:url] , result: got_items.pop , note: note}
    ch_results << trade_result

  end
end

open(File.join(DATA_PATH, 'trade_results.yaml') , 'w'){|f| YAML.dump(Source , f)}

puts Source.to_yaml