# -*- encoding: utf-8 -*-
require 'jiken/util/stdout_hook'

module ILanguage
  module Calculator

    class TaigenWarning
      attr :message,true
      attr :decision,true
      @@warnings = []
      def self.add(warn)
        @@warnings << warn
      end

      def initialize(warning , &decision)
        @message = warning
        @decision = decision
        TaigenWarning.add(self)
      end

      def is_target?(idress)
        @decision.call(idress)
      end

      def self.update_warnings(options)
        IDefinition.update_all('warnings = NULL')  #if options[:import]
        for idef in IDefinition.find(:all , :conditions => "revision is null")
          for warn in @@warnings
            if warn.is_target?(idef.compiled_hash)
              puts idef.name + ':' + warn.message
              warns = idef.warnings || []
              puts warns.class
              warns << warn.message
              idef.warnings = warns
              puts idef.warnings
              puts
            end
          end
          idef.save if options[:import] && idef.changed?
        end
      end
    end
  end
end


include ILanguage::Calculator

TaigenWarning.new('補足：ＦＥＧの騎士のロケットパンチ戦闘補正を加算する場合は、「＃！部分有効条件：ロケットパンチ」を追加してください。'){|idress|
  idress['Ｌ：名'] == 'ＦＥＧの騎士'
}

TaigenWarning.new('補足：体格・耐久力以外の評価値を使う防御は、部隊全員がそれを使用可能な場合にのみ評価を算出しています。http://bb2.atbb.jp/echizenrnd/viewtopic.php?t=9'){|idress|
  idress['原文'] =~ /防御判定に(.+?)を使うことができる。/
}

TaigenWarning.new('補足：ＲＢ等のパイロット能力評価を２倍にする乗り物の計算には対応していません。パイロットの個人修正欄を利用して、評価２倍になるよう調整してください。'){|idress|
  idress['原文'] =~ /能力評価、２倍。/
}
TaigenWarning.new('注意：外交戦可能なキャラクターが艦船に搭乗している部隊・分隊では、外交戦評価に船乗り・海賊の艦上補正が加算されます。艦上補正を含まない外交戦評価が必要な場合は、艦船から降りた分隊を用意してください。'){|idress|
  idress['原文'] =~ /外交戦/
}
TaigenWarning.new('補足：猫と犬の前足が重なった腕輪は部隊内で一人でも持っていれば、同調評価提出者の外見にその補正を足す事ができます。'){|idress|
  idress['Ｌ：名'] == '猫と犬の前足が重なった腕輪'
}

TaigenWarning.new('注意：ＴＬＯは用法・用量を守って正しくお使いください。'){|idress|
  idress['原文'] =~ /の位置づけ　＝　.*ＴＬＯ/ ||  idress['Ｌ：名'] == "るしにゃんＴＬＯ"
}

TaigenWarning.new('注意：評価のいずれかが-17以下（＝RD0.1未満）のアイドレスについて、本来のルールと違い、太元ではあえて小数点１桁での切り捨て処理を行いません。http://bb2.atbb.jp/echizenrnd/viewtopic.php?p=37#37'){|idress|
  evals = idress['評価']
  evals && evals.keys.any?{|key|evals[key] <= -17}
}


TaigenWarning.new('注意：＜愛情評価＞ないし＜友情評価＞ による補正には対応していません。定義パッチを利用し、任意の数字に置き換えて下さい。http://bb2.atbb.jp/echizenrnd/viewtopic.php?p=44#44'){|idress|
  idress['原文'] =~ /評価＞。/
}
TaigenWarning.new('補足：乗り物が含まれる部隊において、魅力及び歌唱行為は判定できません。http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=2027'){|idress|
  idress['原文'] =~ /歌唱行為/
}

TaigenWarning.new('注意：情報収集能力の評価値計算には対応していません。'){|idress|
  idress['原文'] =~ /情報収集、/
}

TaigenWarning.new('注意：射撃に対する防御自動成功を有効にした場合、本来の裁定に反し、太元は部隊単位で防御自動成功と扱います。'){|idress|
  idress['Ｌ：名'] == 'ムラマサ２'
}

TaigenWarning.new('注意：栄光の野戦炊飯具１号は必ず編成参加者全員分を用意し、＜栄光の野戦炊飯具１号の効果＞は本隊に付与してください。<br/>栄光の野戦炊飯具１号が不足して個人単位でこのアイドレス付与の有無に差が出る場合、太元はこれを正しく扱えません。（猫士・犬士の食料消費半減の切り上げタイミングが曖昧になるためです。）'){|idress|
  idress['Ｌ：名'] =~ /栄光の野戦炊飯具１号(の効果)?$/
}

TaigenWarning.new('補足：盾用Ｉ＝Ｄを編成する場合、"（バンド）スカールド01：スカールド；"等と「（バンド）」タグを付ける事で、７５％制限の数え上げ除外及び行為判定評価からのRD加算除外を行う事ができます。 http://bb2.atbb.jp/echizenrnd/viewtopic.php?t=122'){|idress|
  idress['Ｌ：名'] == 'スカールド'
}

TaigenWarning.new('補足：電子妖精（レンジャー版）の情報防壁は、”（随行）Firewall:電子妖精（レンジャー版）の情報防壁；”等と、擬似的なユニットとして記述してください。 http://bb2.atbb.jp/echizenrnd/viewtopic.php?p=295#295'){|idress|
  idress['Ｌ：名'] =~ /電子妖精（レンジャー版）/o
}

TaigenWarning.new('補足：調伏補正が必要な場合は、定義パッチを利用して下さい。'){|idress|
 idress['原文'] =~ /調伏行為/o
}
options = {:import => false}
mandatory_options = %w(  )

parser = OptionParser.new do |opts|
  opts.banner = <<-BANNER.gsub(/^          /,'')
    太元用警告メッセージの登録
    Usage: #{File.basename($0)} [options]
    Options are:
  BANNER
  opts.separator ""
  opts.on('--import'){
    options[:import] = true
  }

  opts.on('--environment VALUE'){} #do nothing for "ruby script/runner"
  opts.parse!(ARGV)
  if mandatory_options && mandatory_options.find { |option| options[option.to_sym].nil? }
    stdout.puts opts; exit
  end
end


TaigenWarning.update_warnings(options)