# -*- encoding: utf-8 -*-
require 'rubygems'
require 'open-uri'
require 'hpricot'
require 'kconv'
require 'yaml'
require 'csv'

class String
  def is_binary_data?
    false
  end

  def decode
    gsub(/\\x(\w{2})/){[Regexp.last_match.captures.first.to_i(16)].pack("C")}
  end
end

ObjectSpace.each_object(Class){|klass|
  klass.class_eval{
    if method_defined?(:to_yaml) && !method_defined?(:to_yaml_with_decode)
      def to_yaml_with_decode(*args)
        result = to_yaml_without_decode(*args)
        if result.kind_of? String
          result.decode
        else
          result
        end
      end
      alias_method :to_yaml_without_decode, :to_yaml
      alias_method :to_yaml, :to_yaml_with_decode
    end
  }
}

arg = ARGV.shift

index_url = arg ? arg : 'http://www35.atwiki.jp/marsdaybreaker/pages/52.html'

doc = Hpricot(open(index_url).read.toutf8)

links = doc/'blockquote/div/a'

links = links.select{|l| ! l[:title].nil?} #.collect{|l| l[:href]}

result = {}
for link in links
  url = link[:href]
  doc = Hpricot(open(url).read.toutf8)
  item_name = link.inner_text
  title = doc / 'div/div/h2/span'
  title_str = title.inner_text.strip
  if title_str =~ /）（/
    title_str =~ /(.+）)（(.+)）/
    name = $1
    kana = $2
  elsif title_str =~ /\)\(/
    title_str =~ /(.+\))\((.+)\)/
    name = $1
    kana = $2
  else
    title_str =~ /(.+?)(（|\()(.+)(）|\))/
    name = $1
    kana = $3
  end
#  print (item_name +' :' ).tosjis
#  puts title_str
#  puts  name + ',' + kana if name && kana
  result[name]=kana
end
puts result.to_yaml