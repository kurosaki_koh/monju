# -*- encoding: utf-8 -*-

#リザルト一括登録用の書式を生成する。

require_relative('result_util')

$mile_total = 0
$errors = []
$miles = []
$originators = []

$idress = []

def create_result_form(cno , oc , result , note , url )
  [cno , oc.try(:name) ,result , note ,url ].join('：')
end

FIXED_RESULTS_PATH =  File.join(DATA_PATH, 'dungeon_results_fixed.tsv')

File.open(FIXED_RESULTS_PATH,'r:utf-8' ){|f|
  f.readline

  while !f.eof
    ace_name = nil
    cno , name , r , note , url = f.readline.split("\t")
    if AcePcTable.ace_no?(cno)
      ace_name =  name
      cno = $table.pc_from_ace(cno)
    end
    oc = OwnerAccount.find_by_number(cno)
    note2 = note.dup
    note2 += " #{oc.owner.class == Character ? '個人' : '' }ＡＣＥ（#{ace_name}）より" if ace_name
    case r
      when /マイル$/
        next if r =~ /[０-９]/
        m = r.tr('０-９','0-9').to_i
        $mile_total += m
#        puts log.to_yaml if cno.nil?
        note =~ /(.+リザルト) (.*)$/
        reason = $1
        note = $2
        reason += " #{oc.owner.class == Character ? '個人' : '' }ＡＣＥ（#{ace_name}）より" if ace_name

        $miles << [cno , oc.try(:name) ,reason ,m , note  , url ].join('：')
      when /根源力/
        note += " 個人ＡＣＥ(#{ace_name}）より" if ace_name
        originator = r.tr('０-９','0-9').gsub('根源力' , '').to_i * 10000
        $originators << [cno , oc.try(:name) ,originator , note2 , url].join('：')
      when /民間用/
        idress_name  = oc.owner.nation.belong == :republic ?  '民間用ペルシャ' : '民間用ケント'
        $idress << create_result_form(cno , oc , idress_name , note2 , url)
      when /(犬|猫)士/
        idress_name  = "個人用#{$1}士"
        $idress << create_result_form(cno , oc , idress_name , note2   , url)
      when 'プロモチケット'
        $idress << create_result_form(cno , oc , '１／１プロモチケット' , note2  , url)
      else
        $idress << create_result_form(cno , oc , r , note2  , url)
#          log[:error] = r
#          $errors << log.dup
    end

  end

}

$miles.sort!
$miles << ['00' , '天領' , '迷宮競技会 マイル報酬支払い' , -$mile_total , ''].join('：')
puts $miles
puts

puts $originators.sort
puts

puts $idress.sort

#puts $errors