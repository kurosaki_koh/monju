require 'open-uri'
require 'rss'
require 'kconv'

rss = RSS::Parser.parse('http://blog.tendice.jp/rss/index.rdf')

for i in rss.items.reverse
  if Event.find_by_name(i.title.toutf8).nil?
    printf "%s,%s\n",i.title.tosjis , i.link
    e = Event.new
    e.name = i.title.toutf8
    e.results_url = i.link
    e.turn = Property['current_turn'].to_i
    e.save
    e.order_no = e[:id]
    e.save
  else
    next
  end
end


#urls = [
#  'http://blog.tendice.jp/200705/index.html' ,
#]
#doc = read_document()
#
#links = doc/:a
#
#entries = links.find_all{|link| link['id']}
#
#for e in entries
#  print e.to_s.tosjis,"\n"
#end