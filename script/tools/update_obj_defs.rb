# -*- encoding: utf-8 -*-
require 'hpricot'
require 'open-uri'

def zukan_url(name)
  'http://www35.atwiki.jp/marsdaybreaker/?page=' + URI.escape(name)
end

def parse_zukan_definition(name)
  doc = Hpricot(open(zukan_url(name)))
  if (block = doc/:blockquote).size > 0
    result = block.inner_html.gsub('<br/>','')
  elsif (block = doc/:pre).size > 0
    result = block.inner_text
  else
    result = ''
  end
  result.gsub!('<br/>','')
  result.gsub!(/^$/,'')
  return result
end

#registries = ObjectRegistry.find(:all,:select => 'name , count(id)' , :group => :name)
#for reg in registries
#  obj_def = ObjectDefinition.find_by_name(reg.name)
#  if obj_def.nil?
#    new_def = ObjectDefinition.new(:name => reg.name)
#    new_def.definition = parse_zukan_definition(reg.name)
#    new_def.save
#  end
#end

def parse_object_type(text)
  text.gsub!('<br />',"\n")
  type_line = text.to_a.find{|line| line =~ /名称/}
  if type_line
    type_line =~ /((\(|（)(.+)(\)|）))?(\(|（)(.+)(\)|）)/
    return $6
  end
end

for obj_def in ObjectDefinition.find(:all)
  if obj_def.definition.nil? || obj_def.definition.empty?
    obj_def.definition = parse_zukan_definition(obj_def.name)
  end
  if obj_def.object_type.nil? || obj_def.object_type.empty?
    begin
      obj_def.object_type = parse_object_type(obj_def.definition)
    rescue => e
      puts obj_def.name.tosjis
      puts e.message.tosjis
    end
  end
  obj_def.save
end