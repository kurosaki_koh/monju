# -*- encoding: utf-8 -*-
require 'csv'
require 'yaml'

DATA_PATH = File.join(File.dirname(__FILE__) , 'data')

$dice_results = {}
$dice_results_by_name = {}
$dice_dic = {}
$trades_dic = {}
$dice_results_with_ace = {}


YAML::ENGINE.yamler= 'psych'


def read_dungeon_results
  CSV.foreach(File.join(DATA_PATH, 'dungeon_dice_result2.csv'), encoding: "UTF-8:UTF-8") do |row|
    character_no, name, *results = row

    results.each{|r|if r
                      r.gsub!('まあ待て落ち着け','まあまて落ち着け')
                      r.gsub!(/会話イベント$/ , '今すぐ使える個人ＡＣＥとの会話イベント１回分')
                    end
    }
    hash = {
        character_no: character_no,
        name: name,
        results: results
    }
#    puts hash.to_yaml
    $dice_results[character_no] = hash
    $dice_results_by_name[name]= hash

    $dice_results_with_ace[character_no[0..7]] ||= []
    $dice_results_with_ace[character_no[0..7]] += hash[:results]
    results.each { |r|
      key = r ? r : 'N/A'
      $dice_dic[key] = r
    }
  end
end

read_dungeon_results


trades = Hash.new{|h,k|h[k] = []}
$trade_entries = Hash.new{|h,k|h[k] = []}
TRADES = File.join(DATA_PATH , 'trade.txt')

def normalize_trade(trades)
  result = trades.map{|item| item.gsub(/（.+）$/ , '')}
  result.delete('0')
  result
end

def read_trades
  trades = Hash.new{|h,k|h[k] = []}
  open(TRADES, 'r:utf-8') { |f|
    while !f.eof
      trade_id, character_no, name, *others = f.readline.strip.split('：')
      losts =

          hash = {
              character_no: character_no,
              name: name,
              losts: normalize_trade(others[0..2]),
              gets: normalize_trade(others[3..5]),
          }
      trades[trade_id] << hash
      others.each { |r| $trades_dic[r] = r }
      $trade_entries[character_no] << hash

#      puts({trade_id => hash}.to_yaml)
    end
  }
  trades
end

trades = read_trades


def have_all?(trades)
  trades.all?{|trade|
    trade[:losts].all?{|item|
      $dice_results_with_ace[trade[:character_no][0..7]].include?(item)
    }
  }
end

def is_symmetrical(trades)
  (trades[0][:losts] == trades[1][:gets]) && (trades[1][:losts] == trades[0][:gets])
end

def trade_id_is_unique(trades)
   trades.size == 2
end


$treasure_logs = YAML.load_file(File.join(DATA_PATH , 'dice_log2.yaml'))

# puts "#ダイス結果のアイテム名:"
# puts dice_dic.keys.sort
# puts
#
# puts "#交換会結果のアイテム名:"
# puts trades_dic.keys.sort.map{|k| k.gsub(/（.+）$/ , '')}
# puts

# puts "ダイス結果のエントリー名"
# puts $dice_results.keys.sort.map{|k| "#{k}\t#{$dice_results[k][:name]}"}
# puts

# puts "#国民番号チェック"
# $trade_entries.keys.sort.each do |cno|
#   a_name = $dice_results[cno][:name]
#   for b_name in $trade_entries[cno].collect{|t|t[:name]}
#     if a_name != b_name
#       puts [cno , a_name , b_name].join("\t")
#     end
#   end
# end



#puts treasure_logs.to_yaml

#puts $dice_results.to_yaml
def check_treasure_log
  $treasure_logs.each do |cno , data|
#    treasures = data.map{|datum| datum.values}.flatten.map {|item|
     treasures = data.map{|datum|datum[:result]}.flatten.compact.map{|item|
#    treasures = data.flatten.map {|item|

      item.gsub(/（.+?）$/ , '').
        gsub('まあ待て落ち付け' , 'まあまて落ち着け').
          gsub('発行信号','発光信号').
          gsub(/真珠の指輪$/,'真珠の指輪Ｄ').
          gsub(/民間用(ケント|ペルシャ)$/ , '民間用ケント／ペルシャ')
    }
    begin
      treasures2 =  $dice_results[cno][:results].dup

      treasures2.delete('×')

      if treasures.compact !=  treasures2.compact
        puts cno
        puts treasures.join("\t")
        puts treasures2.join("\t")
      end
    rescue
      puts $!
      puts cno
    end

  end

end

def do_trade(trade_pair)
  trade_pair.each do |trade|
    cno = trade[:character_no]
    $treasure_logs[]

  end
end

def check_trade(trade_id , trade_pair)
  if trade_id_is_unique(trade_pair) && have_all?(trade_pair) && is_symmetrical(trade_pair)
    trade_pair.each do |trade|
      trade[:losts].each do |item|
        index = $dice_results_with_ace[trade[:character_no][0..7]].index(item)
        if index.nil?
          puts trade_id
          puts trade_pair.to_yaml
          puts trade_pair[0][:character_no] + $dice_results_with_ace[trade_pair[0][:character_no][0..7]].to_yaml
          puts trade_pair[1][:character_no] + $dice_results_with_ace[trade_pair[1][:character_no][0..7]].to_yaml
        end
        $dice_results_with_ace[trade[:character_no][0..7]].delete_at(index)
      end

      $dice_results_with_ace[trade[:character_no][0..7]] += trade[:gets]
    end
    true
  else
    puts "交換エラー：" + trade_id
    puts trade_pair.to_yaml
    putstrade_pair[0][:character_no]
    puts $dice_results_with_ace[trade_pair[0][:character_no][0..7]].to_yaml
    puts trade_pair[1][:character_no]
    puts $dice_results_with_ace[trade_pair[1][:character_no][0..7]].to_yaml
  end
end

def check_all_trades(trades)
  trades.keys.sort.each do |trade_id|
    check_trade(trade_id , trades[trade_id])
  end
end



check_treasure_log

#交換チェック
check_all_trades(trades)

$source = YAML.load_file(File.join(DATA_PATH, 'trade_data.yaml'))


def compare_trades(side , trade_b)
  side[:items].all?{|item|
    item_name = item[:name]
    item_name.gsub!("民間用ケント（帝國）／民間用ペルシャ（共和国）" , '民間用ケント／ペルシャ')
    item_name.gsub!('（特殊なし、ＡＬＬ２５）' , '')
    item_name.gsub!(/（.+）$/ , '')
    trade_b[:losts].include?(item_name)
  } && side[:items].size == trade_b[:losts].size
end

def print_error(side ,pair)
  puts "#error"
  puts pair.to_yaml
  puts side.to_yaml
end
$source.each do |trade_id , trade_a|
  pair = trades[trade_id]

  if trade_a[:side][0][:character_no] != pair[0][:character_no]
    tmp = pair[1]
    pair[1] = pair[0]
    pair[0] = tmp
  end

  unless compare_trades(trade_a[:side][0] , pair[0])
    print_error(trade_a[:side][0] , pair[0])
    next
  end

  print_error(trade_a[:side][1] , pair[1])  unless compare_trades(trade_a[:side][1] , pair[1])
end
