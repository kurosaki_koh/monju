# -*- encoding: utf-8 -*-
require 'kconv'
#source = open('pjobs.txt').read.toutf8

class PersonalJobRegister
  attr :source , true

  attr :source_data , false

  def initialize
    @source_data = []
  end

  def load(path)
    @source = open(path).read.toutf8
  end

  def parse(line)
    result = {}
    columns = line.dup.split(':')
    result[:character_no] , result[:name], jobs_str = columns[0..2]
    jobs =jobs_str.split('＋')
    result[:race] = jobs.shift
    jobs.each{|item| item.strip!}
    result[:jobs]=jobs
    result
  end

  def parse_all
    for line in @source
      @source_data << parse(line)
    end
  end

  def find_character(params)
    Character.find_by_character_no(params[:character_no])
  end

  def check(params)
    ch = find_character(params)
    return nil unless ch

    jobidress_names = [params[:race] ,params[:jobs]].flatten
    return !ch.owner_account.job_idresses.any?{|ji| ji.idress_type == 'JobIdress' && ji.is_this?(jobidress_names)}
  end

  def validate(params)
    return false unless Race.find_by_name(params[:race])
    return params[:jobs].all?{|i| Job.find_by_name(i)}
  end

  def register(params)
    ch = Character.find_by_character_no(params[:character_no])
    return nil unless ch

    ji = JobIdress.new
    ji.race = Race.find_by_name(params[:race])
    return nil unless ji.race
    yggs = params[:jobs].collect{|jname| Job.find_by_name(jname)}
    return nil if yggs.any?{|item| item.nil?}
    ji.job1 , ji.job2 , ji.job3 = yggs
    ji.owner_account = ch.owner_account
    ji.name = ji.inspect_idresses
    ji.save
  end


  def each
    @source_data.each{|i| yield i}
  end
  
  def register_all
    for param in @source_data
      register(param) if check(param)
    end
  end
end

if $PROGRAM_NAME == 'runner'
  require 'yaml'
  require 'jiken/util/stdout_hook'
  options = {}
  mandatory_options = %w(  )
  parser = OptionParser.new do |opts|
    opts.banner = <<-BANNER.gsub(/^          /,'')
      コンパイル済み定義のインポート
      Usage: #{File.basename($0)} [options]
      Options are:
    BANNER
    opts.separator ""
    opts.on('--import'){
      options[:import] = true
    }
    opts.on('--environment VALUE'){} #do nothing for "ruby script/runner"

    opts.parse!(ARGV)
    if mandatory_options && mandatory_options.find { |option| options[option.to_sym].nil? }
      stdout.puts opts; exit
    end
  end

  pjr = PersonalJobRegister.new

  pjr.source = open(ARGV.shift).read.toutf8
  pjr.parse_all

  flag = false
  pjr.each{|param|
    puts param.to_yaml
    ch = pjr.find_character(param)
    unless ch
      print "#{param[:character_no]}:#{param[:name]} not found\n"
      flag = true
      next
    end
    unless pjr.validate(param)
      print "Unknown idress name: "
      param[:race]
      flag = true
    end

    unless pjr.check(param)
      print "job Idress exists of #{ch.character_no}:#{ch.name}.#{param[:race]}+#{param[:jobs].join('+')}\n"
      puts ch.owner_account.job_idresses.collect{|j|j.inspect_idresses}
      puts
    end
  }
  exit if flag

  if options[:import]
    pjr.register_all
  end
end

