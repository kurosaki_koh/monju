require 'i_language'
require 'set'

puts ILanguage::Calculator::EvaluatorsArray.inject(Set.new){|conds , ev| 
  conds += ev.conditions
}.map{|item|item.inspect}
