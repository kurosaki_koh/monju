# -*- encoding: utf-8 -*-
start_date = Time.new
options = {}
mandatory_options = %w(  )

ARGV.shift if ARGV[0] =~ /environment/o
parser = OptionParser.new do |opts|
  opts.banner = <<-BANNER.gsub(/^          /,'')
    アイドレス定義の新リビジョンを設定する。
    Usage: #{File.basename($0)} [options]
    Options are:
  BANNER
  opts.separator ""
  opts.on('--revision VALUE' , Integer){|val|
    options[:revision] = val
  }

  opts.parse!(ARGV)
  if mandatory_options && mandatory_options.find { |option| options[option.to_sym].nil? }
    stdout.puts opts; exit
  end
end
revision = options[:revision]
puts revision.inspect
if revision && revision.to_i > 0
  class IDefinition
    def before_save
      #do nothing
    end
  end
  IDefinition.find(:all , :conditions => 'revision is NULL')
  IDefinition.make_revision(revision){|idef|
    puts idef.name
  }
  puts "Revision #{revision} created."
end
puts start_date
puts Time.new