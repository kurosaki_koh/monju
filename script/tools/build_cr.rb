for od in ObjectDefinition.all
  next if !od.is_idress3? && !od.is_command_resource?
  puts od.inspect
  if od.command_resource_definitions.size != od.parsed_command_resources.size
    CommandResourceDefinition.transaction do
      for cr_hash in od.parsed_command_resources
        cr_def = CommandResourceDefinition.new(cr_hash)
        cr_def.object_definition = od
        cr_def.save
        puts cr_def.inspect
      end
    end
  end
end

for cr in CommandResource.order('name , id')
  next if cr.command_resource_definition_id
  cr_def = CommandResourceDefinition.find_by_name(cr.name)
  if cr_def
    cr.command_resource_definition = cr_def
    cr.save
  else
    puts "#{cr.name} not found."
  end
end