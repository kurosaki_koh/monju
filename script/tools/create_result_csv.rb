# -*- encoding: utf-8 -*-

#trade_result.yaml の内容をCSV形式に変換する。
require_relative('result_util')

$mile_total = 0
csv = CSV.open(File.join(DATA_PATH, 'trade_result.csv') ,'w:utf-8:utf-8')

$source.each do |cno, logs|
  partner_cno = AcePcTable.ace_no?(cno) ? $table.pc_from_ace(cno) : nil
  oc = OwnerAccount.find_by_number(cno)

  next if $clare[cno].nil?
  name = $clare[cno][1]

  for log in logs
    next unless log[:result]
    note = create_note(log[:url])

    for r in log[:result]
      csv << [cno , name.strip , log[:note] || note , r , log[:url] ]
    end
  end
end

csv.close
