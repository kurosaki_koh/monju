# -*- encoding: utf-8 -*-

require_relative('result_util')

EXSTING_IDRESSES = %w!
まあまて落ち着け
アーミーナイフ
急げ馬よ
メード武術
剛力
護衛騎士
クローバー畑
多目的ナイフ
リハビリ
奇跡の治療
簡易整備ツール
発光信号
!

$trades = Hash.new { |h, k| h[k]=[] }


class ResultRecord
  attr_accessor :number, :name, :reason, :item, :url

  def initialize(*args)
    @number, @name, @reason, @item, @url = *args
  end

  def to_s
    [@number, @name, @reason, @item, @url].join(',')
  end
end

CSV.foreach(File.join(DATA_PATH, 'trade_result.csv')) do |row|

  cno = row[0]
  cols = row.to_a
  if AcePcTable.ace_no?(cno)
    cno = $table.pc_from_ace(cno) if AcePcTable.ace_no?(cno)
    ace_name = cols[1]
    partner_name = OwnerAccount.find_by_number(cno).name

    cols[2] += "・個人ＡＣＥ取得分（#{ace_name}）"
    cols[1] = partner_name
    cols[0] = cno
  end
  $trades[cno ] << ResultRecord.new(*cols)
end

ITEM_TO_MILE = {
    '残念賞メダル' => 10 ,
    'まあまて落ち着け' => 10,
    '民間用ケント（帝國）／民間用ペルシャ（共和国）' => 10,
    'アーミーナイフ' => 10,
    '急げ馬よ' => 20,
    'メード武術' => 20,
    '剛力' => 20,
    '護衛騎士' => 20,
    'クローバー畑' => 20,
    '多目的ナイフ' => 20,
    'リハビリ' => 30,
    'オーマ化阻止の秘宝（愛の思い出）' => 30,
    '野心封印猫' => 30,
    '奇跡の治療' => 30,
    '簡易整備ツール' => 10,
    '発光信号' => 10,
    'ロングソード＋１' => 10,
    'ナイトシールド＋１' => 20,
    'マジックワンド＋１' => 20,
    'エルブンチェイン＋１' => 20,
    'ヘルム＋１' => 20,
    'スピア＋１' => 20,
    'プロモチケット' => 30 ,
    'ＰＬＡＣＥ素体' => 30,

    'クローバー畑' => 10,
    'スローイングダガー＋１' => 10,
    '真珠の指輪Ｄ' => 10,
    'まず過ぎるポーション' => 10,

    '計略' => 20,
    '見事な手榴弾' => 20,
    '格好いい拳銃' => 20,
    '巨大な狙撃銃' => 20,

    '馬車馬' => 30,
    'プロモチケット／みなし職業開示チケット' => 30,
    '回避の達人' => 30,
    '竜牙の使い手' => 30,
    'Ｉ＝Ｄ用キャノン砲' => 30,

    'だきつきっ' => 10,
    '治癒芸術' => 10,
    'アートオブロングドライブ' => 10,
    'スコップアート' => 10,
    '再資源化' => 10,
    'すごい灯りの魔法' => 10,
    'サバイバル芸術' => 10,
    'マッスルアート' => 10,
    '整備芸術' => 10,
    '偵察芸術' => 10,
    'ごめんなさい' => 10,

    '古い聖銃に似せたブラスター' => 10,
    '弱体化！' => 10,
    '複合双眼鏡' => 10,
    'ポケットピケ' => 10,
    'ビームシールド' => 10,
    'ミサイルコンテナ' => 10,
    'アートオブリミッターカット' => 10,
    'おともネコリス' => 10,
    'シオネの小像' => 10,
    'ほねっこの宝飾品' => 10,
    '皆でとった思い出の写真' => 10,
    'お風呂セット' => 10,
    '海法に授けられる予定だった緑色のかつら' => 10,
    '銀一郎毛布' => 10,
}

SELECTABLE = %w!
民間用ケント（帝國）／民間用ペルシャ（共和国）
アーミーナイフ
多目的ナイフ
野心封印猫
犬士／猫士　一匹
残念賞メダル
簡易整備ツール
ロングソード＋１
ナイトシールド＋１
マジックワンド＋１
エルブンチェイン＋１
ヘルム＋１
スピア＋１
プロモチケット
スローイングダガー＋１
真珠の指輪Ｄ
まず過ぎるポーション
見事な手榴弾
格好いい拳銃
巨大な狙撃銃
馬車馬
プロモチケット／みなし職業開示チケット
Ｉ＝Ｄ用キャノン砲
古い聖銃に似せたブラスター
複合双眼鏡
ポケットピケ
ビームシールド
ミサイルコンテナ
おともネコリス
シオネの小像
ほねっこの宝飾品
皆でとった思い出の写真
お風呂セット
海法に授けられる予定だった緑色のかつら
銀一郎毛布
!

def duplicate_check(results)

  dupes = {traded: [], selectable: [] ,unselectable:[]}

  idresses = Hash.new { |h, k| h[k] = [] }
  results.each { |r| idresses[r.item] << r }

  idresses.each do |item, rows|

  #一件以下なら、重複はしていない。
    next if rows.size <= 1

    #マイルと根源力の重複は放置
    next if item =~ /(マイル|根源力)/

    cols = [rows[0].number, rows[0].name, item]
    cols << ITEM_TO_MILE[item]
    cols += rows.collect { |r| [r.reason, r.url] }.flatten
    line = cols.join("\t")
#    lines = rows.map{|r|r.to_s}
#交換会取得があるなら、重複していてもマイル化はしない。
    if rows.find { |r| r.reason =~ /交換会/ }
      dupes[:traded] << line
    else
      if SELECTABLE.include?(item)
        dupes[:selectable] << line
      else
        dupes[:unselectable] << line
      end
    end
  end
  dupes
end

EXSTING_IDRESSES.each do |name|
  yggs = Yggdrasill.where(idress_name: name).includes(:owner_account).order("owner_accounts.number")
  puts "#{name}"
  yggs.each { |y|
    $trades[y.owner_account.number].unshift ResultRecord.new(y.owner_account.number, y.owner_account.name, '以前より所有', name, '')
    puts "・" + y.owner_account.number + '：' + y.owner_account.name
  }
  puts
end

#$trades.keys.sort.each{|k| $trades[k].each{|r| puts r.to_s } }

dupe_checked = []
$trades.keys.sort.each do |k|
  dupes = duplicate_check($trades[k])
  next if [:traded ,:selectable ,:unselectable].all?{|i| dupes[i].size == 0}
  dupe_checked << dupes
end

puts "＃マイル化必須"
dupe_checked.each { |d|
  puts d[:unselectable]
}
puts
puts "＃重複可能・マイル化選択"
dupe_checked.each { |d|
  puts d[:selectable]
}
puts
puts "＃交換による重複"
dupe_checked.each { |d|
  puts d[:traded]
}





