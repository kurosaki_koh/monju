YAML_PATH = Rails.root + '/public/pub/idefs.yml'

definitions = YAML.load(open(YAML_PATH,'r'))

#for name in definitions.keys
#  idef = IDefinition.find_by_name(name)
#  dh = definitions[name]
#  unless idef
#    idef = IDefinition.new
#    puts "#{dh['Ｌ：名']} registered."
#  end
#  if idef.definition != dh['原文']
#    p idef.definition
#    p dh['原文']
#    idef.definition = dh['原文']
#    idef.save
#    idef.reload
#    puts "#{idef.name} updated."
#  end
#end

for idef in IDefinition.find(:all)
  dh = definitions[idef.name]
  unless dh
    puts "#{idef.name} not found in YAML defs."
  end
end