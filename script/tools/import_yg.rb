# encoding: utf-8
require 'json'
require 'yaml'
COUNTRY = File.join(File.dirname(__FILE__), 'country_ygtree.json')
PLAYER = File.join(File.dirname(__FILE__), 'pc_ygtree2.json')


puts ARGV.include?('n')

def parse_note(str)
  note = []
  if str =~ /^○(.+?)(（.+）)?：(.+)/
    type = '開発イベント'
    note << '開発イベント：' + $3
    event_name = $1
    idress = $3
    return {'type' => type, 'note' => note, 'name' => event_name + '：'+ idress, 'idress_name' => idress}
  end

  if str =~ /^\<del\>/
    type = '着用'
    note << '１２枠外'
    str = str.gsub(/\<\/?del\>/, '')
  end

  if str =~ /(.+?)（職業）(.+)/
    type = '職業'
    idress = $1
    note << "職業:" + $2
    return {'type' => type, 'note' => note, 'name' => $2, 'idress_name' => idress}
  end
  if str =~ /^○?(.+)(（.+）)?/
    type = nil
    idress = $1
  else
    type = nil
    idress = str
  end
  {'type' => type, 'note' => note, 'idress_name' => idress}
end

def register_yggdrasill(owner, nodes)
  dic = {}
  root = Yggdrasill.find_by_owner_account_id_and_name(owner.id, 'root')
  unless root
    root = Yggdrasill.create!(name: 'root', idress_name: nil, owner_account_id: owner.id)
  end
  dic[-1] = root
  for node in nodes
    notes = node['node'] ? node['node'].join("\n") : ''
    puts node['idress_name']

    ygg = Yggdrasill.create!(
        owner_account_id: owner.id,
        name: node['name'],
        idress_name: node['idress_name'],
        node_type: node['type'],
        note: notes
    )
    dic[node['id']] = ygg
    parent = dic[node['parent']]
    ygg.move_to_child_of(parent)
#    root.reload

  end

end

def register_to_nation(id, nodes)
  dic = {}
  owner = Nation.find(id).owner_account
  register_yggdrasill(owner, nodes)
end

def register_to_character(id, nodes)
  dic = {}
  character = character.where('character_no like ?', id).first
#  if character

  register_yggdrasill(owner, nodes)
end

def find_nation_account(id)
  owner = Nation.find(id).owner_account
end
def find_character(id)
  Character.where('character_no like ?', '%'+id).first
end

targets = nil
if ARGV.include?('n')
  open(COUNTRY) do |io|
    targets = JSON.load(io)
  end
  $nations = Nation.all(order: :id).select{|n|n.belong != :associated}.collect{|n|[n.id , n.name]}

  $nations.unshift([0 , '宰相府藩国'])
else
  targets = []
  open(PLAYER) do |io|
    while !io.eof?
      begin
        targets << JSON.parse(line = io.readline)
      rescue => e
        puts line
        puts e
      end
    end
  end
end


for target in targets

#  puts "\n＃#{country}\n"
#  tree = country['tree']
#  puts player['id']
#  puts player['text']
#  puts player['url']
#  puts player['name']


  tree = target['tree']
  nodes = []
#  puts tree.to_yaml

  for node in tree
    parsed = parse_note(node['name'])
    nodes << node.merge(parsed)
  end

  if ARGV.include?('n')
    owner_account = find_nation_account(target['id'].to_i)
    register_yggdrasill(owner_account  , nodes)
  else
    ch = find_character(target['id'])
    if ch
      puts ch.character_no + ":"+ch.name
      register_yggdrasill(ch.owner_account, nodes)
    end
  end

  puts
#  puts nodes.to_yaml

#
end