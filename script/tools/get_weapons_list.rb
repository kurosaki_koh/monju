# 
# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'kconv'
require 'jcode'
require 'open-uri'
require 'hpricot' 

YELL_WEAPON_REGISTRATION_URL = 'http://idress-yell.sakura.ne.jp/weapon/CreateWeapon.php'

doc = Hpricot(open(YELL_WEAPON_REGISTRATION_URL).read.toutf8)
table = (doc/"select")[1]
options = table/'option'

Specification.transaction do
  for opt in options
    print opt['value'],opt.inner_text , "\n"
    spec = Specification.find_all_by_name(opt.inner_text)
    if spec.size == 0
      spec = Specification.new if spec.size == 0
    else
      spec = spec[0]
    end
    print spec
    spec[:name]=opt.inner_text
    spec[:type_no]=opt['value']
    spec.save
  end
end