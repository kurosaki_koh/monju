module Acl


  def do_authorize(nid = nil)
    cu = current_user
    return false if cu == :false #restricted and not logined?
#    nid = auth_context_id unless nid
    return true if cu.role == 'admin'
    roles = self.class.required_roles(self.action_name)
    has_role?(*roles)
  end

  def authorized?(nid = nil)
    do_authorize(nid)
  end
  
  def has_role?(*roles)
    return false if current_user == :false
    roles << 'admin'
    return true if current_user.has_role?( roles )
    return roles.select{|r|r.class != String }.find{|r|r.do_authorize(current_user , self)}
  end
  
  def is_admin?
    return false if current_user == :false
    return false if current_user == false
    return true if current_user.role == 'admin'
    return current_user.has_role?(:admin)
  end
  
  def auth_context_id
    params[:id]
  end

  def self.append_features(base)
    super(base)
    base.extend(ClassMethods)
    base.class_eval do
      attr :acl , true
    end
  end
  module ClassMethods  
    def requires_role(role,options={})
      if options.has_key?(:only)
        options[:only].each{|s|
          add_requires_role_for_action(s,role)
        }
      end
    end

    def acl
      @acl ||= Hash.new{|hash,key| hash[key]=[]}
    end

    def add_requires_role_for_action(action_name , role)
      acl[action_name.to_s] << case role
                                 when Symbol
                                   role.to_s
                                 else
                                   role
                                 end
    end

    def set_login_filter
      targets = acl.keys
      before_filter :login_required , :only => targets
    end
    
    def required_roles(action_name = @action_name)
      acl[action_name.to_s]
    end
    #  before_filter :login_required , :only => [:create , :destroy, :edit , :update , 
#    :delete_results_by_event , :confirm_register_results]
  end  
end