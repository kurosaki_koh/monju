=begin
使い方
１)コントローラーの冒頭に以下の行を加える。
  include RowMover
  movable_row :child => :（自身のモデル名,小文字で） , :parent => :(親のモデル名)  , :child_class => :(子のクラス名)

２）index.html.erbの冒頭に以下を加える。
<%= render :partial => 'shared/move_position_func', 
    :locals => {:controller => "owner_accounts/#{@owner_account.id}/cash_records" ,
                :length => @owner_account.cash_records.size } %>

:controller => コントローラーへのpath
:length => 子モデルの数

３）index.html.erbのTABLEタグに以下の修正を加える。
<TR>タグ：id属性を加える。
  <tr id="row_<%=cash_record.position %>" >
先頭の<TD>タグ追加：以下のような<TD>タグを加える。
    <td> <%= radio_button_tag 'target_row' , cash_record.id ,selected , :onClick => "row_radio_clicked(#{cash_record.position});"%> </td>
<TH>タグの追加：THタグがある場合は、忘れずにこれも加える（でないとカラムがずれる）

上下移動ボタンの追加：TABLEタグの直後に以下のようなコードを加える。
<% if has_role?(:accountant)%>
<%= move_button :up %> 
<%= move_button :down %>
<% end %>

４）partialビューの追加
TABLEの1行を表現するPartial ビューファイルを追加する。内容は３）のルールに従うこと。
ファイル名は"_(子モデルの単数形)

5)partialビューをindex.html.erbでも流用する場合は、<TR>タグを以下のように記述する。
  <tr id="row_<%=specification.position %>" >
    <%= render :partial => 'specification' , :locals => {:specification => specification ,:selected => false} %>
  </tr>

６）routes.rbの対象コントローラーの記述に、以下のように:collectionとして:move_position=>:postを追加する。
  map.resources :specifications ,:collection => {:move_position => :post }

７）子モデルにacts_as_list を記述する。

=end

module RowMover
  def move_position
    row = child_class.find(params[:target_id])
    opponent = nil
    opponent_position = nil
    target_position = row.position
    if params[:direction] == "up"
      opponent = row.higher_item
      opponent_position = opponent.position
      row.move_higher
    elsif params[:direction] == "down"
      opponent = row.lower_item
      opponent_position = opponent.position
      row.move_lower
    end
    if row.has_attribute?(:turn)
      row.turn = opponent.turn if row.turn != opponent.turn
    end
    row.send(parent_name).reload
    opponent.reload
    row.save
    child_name_val = child_name
    partial_val=partial
    render :update do |page|
      instance_variable_set('@'+child_name_val.to_s , row)
      page << "if (document.getElementById('row_#{opponent_position}')){\n"
      page["row_#{opponent_position}"].replace_html :partial => partial_val , 
        :locals => {child_name_val => row,:selected => true}
      instance_variable_set('@'+child_name_val.to_s , opponent)
      page["row_#{target_position}"].replace_html :partial => partial_val , 
        :locals => {child_name_val => opponent,:selected => false}
        page["row_#{opponent_position}"].visual_effect(:pulsate, :duration => 0.5)
      page['up_button'].enable if !row.first?
      page['down_button'].enable if !row.last?

      page << "}else{\n"

      page.remove("row_#{target_position}")
      page['up_button'].enable if params[:direction] == 'down' || !row.first?
      page['down_button'].enable if params[:direction] == 'up' || !row.last?
      page << "}\n"

      page.assign 'loading' , false
    end
  end

  def self.append_features(base)
    super(base)
    base.extend(ClassMethods)
    base.instance_eval{
      cattr_accessor :child_name
      cattr_accessor :parent_name
      cattr_accessor :child_class
      cattr_accessor :partial
    }
  end

  module ClassMethods
    def movable_row(options={})
      self.child_name = options[:child].to_sym
      self.parent_name = options[:parent].to_s
      self.child_class = options[:child_class] ? options[:child_class] : self.child_name.to_s.classify.constantize
      self.partial = options[:partial] ? options[:partial] : self.child_name.to_s
    end
  end  
end