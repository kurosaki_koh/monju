namespace 'pegjs' do

  JSParserPath = File.join(Rake.original_dir , 'public/javascripts/evchk_parser.js')
  JSParserSourcePath = File.join(Rake.original_dir , 'lib/eval_checker.pegjs')

  file JSParserPath => JSParserSourcePath do
    sh "pegjs -e evchk_parser #{JSParserSourcePath} #{JSParserPath}"
  end

  desc 'Compile EvalChecker Parser'
  task :compile => JSParserPath
end