TT_SRCS = FileList["lib/i_language/parser/lib/*.treetop" , "lib/i_language/calculator/*.treetop" ,"lib/i_language/fcalc/*.treetop" ]
TT_RBS = TT_SRCS.ext('.rb')


rule '.rb' => ['.treetop'] do |t|

  require 'treetop' unless defined? Treetop
  when_writing 'tt %s' % t.source do
    puts 'tt %s' % t.source if verbose
    Treetop::Compiler::GrammarCompiler.new.compile(t.source, t.name)
  end
end

desc 'Pre-compile Treetop definitions for taigen'
task :tt => TT_RBS

#desc 'test'
#task :ctest do|t|
#  puts TT_RBS.inspect
#end
