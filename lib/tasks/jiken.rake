# coding: utf-8

namespace :jiken do
  require 'rake/clean'

  ParserDir = File.join(Rake.original_dir , 'lib/i_language/parser')
  PublicDir = File.join(Rake.original_dir,'public/pub')
  YamlDef = File.join(PublicDir,'idefs.yml')
  JSDef = File.join(PublicDir,'idefs.js')

  DEFS = [YamlDef , JSDef]
  PACKS = DEFS.collect{|f|f + '.gz'}
  
  CLOBBER.include DEFS
  CLOBBER.include PACKS
  
  desc 'コンパイル結果からYAMLファイルとJSONファイルを作る。'
  task :make => DEFS

  file YamlDef  do
    ruby "script/runner script/tools/yaml_out.rb > \"#{YamlDef}\""
  end

  file JSDef  do
    ruby "script/runner script/tools/yaml_out.rb --json > \"#{JSDef}\""
  end

  PACKS.each{|pack|
    task pack => pack.pathmap('%d/%n') do |t|
      sh %! gzip "#{t.prerequisites}"!
    end
  }

  task :pack => PACKS

#  SQLiteDB = ParserDir + '/idef.sqlite3'
  desc 'ｉ言語定義の文法チェックを行う。'
  task :check => ['errors.txt' , :decompile_check ]

  desc 'ｉ言語定義からSQLiteへコンパイルを行う。'
  task :compile do |t|
    ruby "script/runner 'script/tools/compile.rb'"
  end

  desc 'ｉ言語定義からSQLiteへコンパイルを行う。'
  task :compile_opt, :options do |t,args|
    ruby "script/runner 'script/tools/compile.rb' #{args.options}"
  end


  task :decompile_check do
    Dir.chdir Rake.original_dir
    Dir.chdir 'i_language'
    sh 'spec spec/idef_decompile_check_spec.rb'
  end

  file 'errors.txt' do
    Dir.chdir ParserDir
    ruby  'error_check.rb > ../errors.txt'
  end

  desc 'Update IDefiniton table from YAML file.'
  task :update_from_yaml do
    ruby "script/runner script/tools/update_idef_from_yaml.rb"
  end

end