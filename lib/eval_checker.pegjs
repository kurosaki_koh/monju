
start
  = (left:stmt right:start) {return left.concat(right);}
  / left:stmt


stmt
  = null_line / troop_evals / eval_group / action_evals / etc

colon_or_tab = "\t" / "：" / ':'

troop_evals
  = "体格" colon_or_tab "筋力" colon_or_tab "耐久力" colon_or_tab "外見" colon_or_tab "敏捷" colon_or_tab "器用" colon_or_tab "感覚" colon_or_tab "知識" colon_or_tab "幸運" line_term
    phy:integer colon_or_tab str:integer  colon_or_tab dur:integer colon_or_tab chr:integer colon_or_tab agi:integer colon_or_tab dex:integer colon_or_tab sen:integer colon_or_tab edu:integer colon_or_tab luc:integer line_term {
    proc = function(name ,value){
      return {name: name , value: value};
    }

    result = []
    result.push(proc('体格',phy));
    result.push(proc('筋力',str));
    result.push(proc('耐久力',dur));
    result.push(proc('外見',chr));
    result.push(proc('敏捷',agi));
    result.push(proc('器用',dex));
    result.push(proc('感覚',sen));
    result.push(proc('知識',edu));
    result.push(proc('幸運',luc));

    return result;
  }

action_evals
 = (left:label_and_value space* '/' space*  right:action_evals ) {return left.concat(right)}
  / action_eval

label_and_value
  = act_name:action_name '：' val:value {
     return [{name: act_name, value: val}];
  }

auto
  = str:( '自動成功' / '-') { return str}


value = integer / auto

action_eval
  = label:label_and_value line_term{

    return label;
  }

action_name
  = (act_name_1:[^:：\n]+ act_name_2:(&('：' !auto [^0-9] ) '：' p_name:[^:：]+ )?) {
    result = act_name_1.join('');
    if (act_name_2 != ""){
      result = result + '：' + act_name_2[2].join("");
    }

    return result;
  }

eval_group
  = '○' g_name:((!space !nl .)+) line_term members:eval_group_member+ {
    var gr_name = '';
    for(idx = 0 ;idx < g_name.length; idx++ ){
      gr_name = gr_name + g_name[idx][2];
    }
    result_array = []
    for( idx = 0 ; idx < members.length; idx++ ){
　　　for( idx2 = 0 ; idx2 < members[idx].length; idx2++ ){
//        members[idx][idx2]["name"] = gr_name + '＠' + members[idx][idx2]["name"];
        result_array.push({name : (gr_name + '＠' + members[idx][idx2]["name"]) , value: members[idx][idx2]["value"]});
      }
    }
    return result_array;

  }

eval_group_member
  =  !'○' space* '・'? member:action_evals　{return member;}

space
  = ( " " / "　" / "\t")

nl = "\r\n" / "\n"

line_term
 = space* comment? (nl / !.)

comment
  = ("＃" / "#" / "※") (!nl .)+

null_line
  =   space* comment? nl {return []}


etc
  = label:[^\n]+ line_term {return [{ name :label.join('') , value : null}]}

integer "integer"
  = digits:[0-9]+ { return parseInt(digits.join(""), 10); }