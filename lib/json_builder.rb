# = Simple JSON builder
#
# This class converts an Ruby object to a JSON string. you can convert
# *ruby_obj* like below.
#
#   json_str = JsonBuilder.new.build(ruby_obj)
#
# *ruby_obj* must satisfy these conditions.
# - It must support to_s method, otherwise must be an array, a hash or nil.
# - All keys of a hash must support to_s method.
# - All values of an array or a hash must satisfy all conditions mentioned above.
#
# If the *ruby_obj* is not an array or a hash, it will be converted to an array
# with a single element.
class JsonBuilder

  #:stopdoc:
  RUBY19             = RUBY_VERSION >= '1.9.0'
  Name               = 'JsonBuilder'
  ERR_NestIsTooDeep  = "[#{Name}] Array / Hash nested too deep."
  ERR_NaN            = "[#{Name}] NaN and Infinite are not permitted in JSON."
  #:startdoc:

  # Create a new instance of JsonBuilder. *options* can contain these values.
  # [:max_nest]
  #     If Array / Hash is nested more than this value, an exception would be thrown.
  #     64 by default.
  # [:nan]
  #     NaN is replaced with this value. If nil or false, an exception will be thrown.
  #     nil by default.
  def initialize(options = {})
    @default_max_nest = options.has_key?(:max_nest) ? options[:max_nest] : 64
    @default_nan      = options.has_key?(:nan)      ? options[:nan]      : nil
  end

  # Convert *obj* to a JSON form string.
  # [obj]
  #     A ruby object. this object must satisfy all conditions mentioned above.
  # [options]
  #     Same as new.
  def build(obj, options = {})
    @max_nest = options.has_key?(:max_nest) ? options[:max_nest] : @default_max_nest
    @nan      = options.has_key?(:nan)      ? options[:nan]      : @default_nan
    case obj
    when Array then build_array(obj, 0)
    when Hash  then build_object(obj, 0)
    else            build_array([obj], 0)
    end
  end

  private #---------------------------------------------------------

  if RUBY19
    ESCAPE_CONVERSION = { '\x' => '\u00', '\a' => '\u0007', '\v' => '\u000B', '\e' => '\u001B' }
    def escape(str)
      str = str.to_s.encode('UTF-8').inspect
      str.gsub!(/\\[xave]/u){|s| ESCAPE_CONVERSION[s] }
      str
    end
  else
    ESCAPE_CONVERSION = ['\"', '\\\\', '\/', '\b', '\f', '\n', '\r', '\t']
    def escape(str)
      str = str.gsub(/[^\x20-\x21\x23-\x5b\x5d-\xff]/n) do |chr|
        if index = "\"\\/\b\f\n\r\t".index(chr[0])
          ESCAPE_CONVERSION[index]
        else
          sprintf("\\u%04X", chr[0])
        end
      end
      "\"#{str}\""
    end
  end

  def build_value(obj, level)
    case obj
    when Integer, TrueClass, FalseClass then obj.to_s
    when Float    then raise ERR_NaN unless obj.finite? || (obj = @nan) ; obj.to_s
    when NilClass then 'null'
    when Array    then build_array(obj, level + 1)
    when Hash     then build_object(obj, level + 1)
    else               escape(obj)
    end
  end

  def build_array(obj, level)
    raise ERR_NestIsTooDeep if level >= @max_nest
    '[' + obj.map { |item| build_value(item, level) }.join(',') + ']'
  end

  def build_object(obj, level)
    raise ERR_NestIsTooDeep if level >= @max_nest
    '{' + obj.map do |item|
      "#{build_value(item[0].to_s,level)}:#{build_value(item[1],level)}"
    end.join(',') + '}'
  end

end
