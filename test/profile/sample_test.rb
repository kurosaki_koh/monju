# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../profile_test_helper'

class TaigenControllerTest < Test::Unit::TestCase
  include RubyProf::Test

#  fixtures :my_fixture
  include TaigenHelper
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::JavaScriptHelper
  include ActionView::Helpers::CaptureHelper
  def setup
    @controller = TaigenController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

#  def test_edit
#    get(:edit)
#  end

  def test_update
    source = <<EOT
＃！部隊補正：教導パイロットの指導
＃！有効条件：｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して、戦闘する場合での

_define_I=D:{Antares1：Ａｎｔａｒｅｓ＋ＨＱ感覚＋ＨＱ体格＋ＨＱ体格＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷:耐久力+32*幸運+12*全評価+9：防御（通常）+3*防御（白兵距離）+3*防御（近距離）+3*防御（中距離）+3,
－Ａｎｔａｒｅｓの軽装鎧
－増加燃料装備
（P）42-00537-01_吾妻　勲：北国人＋撃墜王＋トップガン＋教導パイロット＋参謀＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*知識+1*幸運+1;
（CP）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
}；

_define_I=D:{Antares2：Ａｎｔａｒｅｓ＋ＨＱ感覚＋ＨＱ体格＋ＨＱ体格＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷:耐久力+32*幸運+12*全評価+3：防御（通常）+3*防御（白兵距離）+3*防御（近距離）+3*防御（中距離）+3,
－Ａｎｔａｒｅｓの軽装鎧
－増加燃料装備
（P）42-00606-01_ＧＴ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*知識+1*幸運+1;
（CP）42-00574-01_コール・ポー：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵+ゲーマー＋ＨＱ感覚：知識+1*幸運+2;
（CP）42-00593-01_うのり：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵＋ＨＱ感覚：敏捷+1*知識+1*幸運+1;
}；

_define_I=D:{Antares3：Ａｎｔａｒｅｓ＋ＨＱ感覚＋ＨＱ体格＋ＨＱ体格＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷:耐久力+32*幸運+12*全評価+6：防御（通常）+3*防御（白兵距離）+3*防御（近距離）+3*防御（中距離）+3,
－Ａｎｔａｒｅｓの軽装鎧
－増加燃料装備
（P）42-00608-01_ＬＣ：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵＋ＨＱ感覚：筋力+1*外見+1*敏捷+1*幸運+1;
（CP）42-00610-01_センハ：北国人＋トップガン＋教導パイロット＋帝國軍歩兵＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*幸運+1;
（CP）42-00352-01_葉崎京夜：北国人＋トップガン＋教導パイロット＋帝國軍歩兵＋補給士官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ知識＋ＨＱ知識：幸運+1;
}；

_define_I=D:{Antares4：Ａｎｔａｒｅｓ＋ＨＱ感覚＋ＨＱ体格＋ＨＱ体格＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷＋ＨＱ敏捷:耐久力+32*幸運+12：防御（通常）+3*防御（白兵距離）+3*防御（近距離）+3*防御（中距離）+3,
－Ａｎｔａｒｅｓの軽装鎧
－増加燃料装備
（P）42-00590-01_セタ・ロスティフンケ・フシミ：北国人＋教導パイロット＋テストパイロット＋外戚＋ＨＱ感覚：敏捷+1;
（CP）42-00764-01_瀬咲：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵＋ＨＱ感覚;
（CP）42-00602-01_ビブー：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵＋ＨＱ感覚：幸運+1;
}；

＃！状況：ＡＲ７以下
＃！有効条件：ＡＲ７以下の場合

＃！状況：同じ行為を行う
＃！有効条件：そのゲーム内の同じ行為を行う場合

＃！状況：ＡＲ７以下と同じ行為を行う
＃！有効条件：そのゲーム内の同じ行為を行う場合
＃！有効条件：ＡＲ７以下の場合

_define_division:{
ミーティア・カイル：教導パイロットの指導,
Antares1;
Antares2;
Antares3;
};

_define_division:{
ミーティア・リーダー：教導パイロットの指導,
Antares1;
};

_define_division:{
ミーティア２：教導パイロットの指導,
Antares2;
};

_define_division:{
ミーティア３：教導パイロットの指導,
Antares3;
};

_define_division:{
ミーティア４：教導パイロットの指導,
Antares4;
};
EOT

#    @evals_str = ''
#    @troop = context.parse_troop(source)
#    @evals_str = @troop.evals_str

    context = Context.new
    troop = context.parse_troop(source)
    result = division_tab(troop,'本隊')
    puts result
  end
end