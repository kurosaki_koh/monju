require File.dirname(__FILE__) + '/../test_helper'

class CashRecordsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:cash_records)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_cash_record
    assert_difference('CashRecord.count') do
      post :create, :cash_record => { }
    end

    assert_redirected_to cash_record_path(assigns(:cash_record))
  end

  def test_should_show_cash_record
    get :show, :id => cash_records(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => cash_records(:one).id
    assert_response :success
  end

  def test_should_update_cash_record
    put :update, :id => cash_records(:one).id, :cash_record => { }
    assert_redirected_to cash_record_path(assigns(:cash_record))
  end

  def test_should_destroy_cash_record
    assert_difference('CashRecord.count', -1) do
      delete :destroy, :id => cash_records(:one).id
    end

    assert_redirected_to cash_records_path
  end
end
