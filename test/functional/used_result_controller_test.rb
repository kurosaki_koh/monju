require File.dirname(__FILE__) + '/../test_helper'
require 'used_result_controller'

# Re-raise errors caught by the controller.
class UsedResultController; def rescue_action(e) raise e end; end

class UsedResultControllerTest < Test::Unit::TestCase
  fixtures :used_results

  def setup
    @controller = UsedResultController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:used_results)
  end

  def test_show
    get :show, :id => 1

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:used_result)
    assert assigns(:used_result).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:used_result)
  end

  def test_create
    num_used_results = UsedResult.count

    post :create, :used_result => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_used_results + 1, UsedResult.count
  end

  def test_edit
    get :edit, :id => 1

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:used_result)
    assert assigns(:used_result).valid?
  end

  def test_update
    post :update, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_destroy
    assert_not_nil UsedResult.find(1)

    post :destroy, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      UsedResult.find(1)
    }
  end
end
