require File.dirname(__FILE__) + '/../test_helper'

class WeaponRegistriesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:weapon_registries)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_weapon_registry
    assert_difference('WeaponRegistry.count') do
      post :create, :weapon_registry => { }
    end

    assert_redirected_to weapon_registry_path(assigns(:weapon_registry))
  end

  def test_should_show_weapon_registry
    get :show, :id => weapon_registries(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => weapon_registries(:one).id
    assert_response :success
  end

  def test_should_update_weapon_registry
    put :update, :id => weapon_registries(:one).id, :weapon_registry => { }
    assert_redirected_to weapon_registry_path(assigns(:weapon_registry))
  end

  def test_should_destroy_weapon_registry
    assert_difference('WeaponRegistry.count', -1) do
      delete :destroy, :id => weapon_registries(:one).id
    end

    assert_redirected_to weapon_registries_path
  end
end
