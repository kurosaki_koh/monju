# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../test_helper'
require 'characters_controller'

# Re-raise errors caught by the controller.
class CharactersController; def rescue_action(e) raise e end; end

class CharactersControllerTest < Test::Unit::TestCase
  include AuthenticatedTestHelper
  fixtures :races , :yggdrasills , :nations , :job_idresses , :characters , :users

  def setup
    @controller = CharactersController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = characters(:first).id
  end

  def test_index
    get :index,:nation_id=>32
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list , :nation_id => 32

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:characters)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:character)
    assert assigns(:character).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:character)
  end

  def test_create
    login_as(:echizen)
    num_characters = Character.count

    post :create, :character => {:name => "create_test" , :character_no => "32010",:nation_id => "32"}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_characters + 1, Character.count
  end

  def test_edit
    login_as(:echizen)
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:character)
    assert assigns(:character).valid?
  end

  def test_update
    login_as(:echizen)
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list', :nation_id => characters(:first).nation_id
  end

  def test_destroy
    login_as(:echizen)
    assert_nothing_raised {
      Character.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Character.find(@first_id)
    }
  end

  def test_parse_text
    test_line = "32001  	セントラル越前  	15300  	東国人＋サイボーグ＋剣士"
    records = @controller.parse_text(test_line)
    rec = records[0]
    assert_equal "32001",rec[:character_no]
    assert_equal "セントラル越前",rec[:name]
    assert_equal "15300",rec[:originator]
    assert_equal ["東国人","サイボーグ","剣士"] ,rec[:jobs]
  end
TEST_RECORDS = <<EOT
32001  	セントラル越前  	15300  	東国人＋サイボーグ＋剣士
32002 	test1	5200 	東国人＋犬士＋ドラッガー
32003 	test2 	5200 	東国人＋犬士＋ドラッカー
32004  	test3  	15300  	東国人＋剣士＋サイボーグ
EOT
  
  def test_find_job_idress
    test_line = TEST_RECORDS

    records = @controller.parse_text(test_line)
    
    assert_equal job_idresses(:mighty_swordman) , @controller.find_job_idress(records[0])
    assert_equal job_idresses(:doggie_solder) , @controller.find_job_idress(records[1])
    assert_equal nil ,   @controller.find_job_idress(records[2])
    assert_equal job_idresses(:mighty_swordman) , @controller.find_job_idress(records[3])
  end
  
  def test_insert_by_text
    login_as(:admin)
    post :insert_by_text , :character_list => TEST_RECORDS
    
    chars = Character.find(:all)
    assert Character.find(:first,:conditions => "character_no=32001")
    assert Character.find(:first,:conditions => "character_no=32002")
    assert Character.find(:first,:conditions => "character_no=32003")
    assert Character.find(:first,:conditions => "character_no=32004")
    assert_equal nil ,  Character.find(:first,:conditions => "character_no=32005")
  end
  
  def test_result_new
    login_as(:echizen)
    post :new_result , :id => @first_id , 
         :new_result => { :event_name => 'test' ,
                          :originator => 2000 , 
                          :magic_item => nil , 
                          :url => nil , 
                          :note => nil }
    ch = Character.find(@first_id)
    
    assert_equal ch.results[0].event_name , "test"
    assert_equal ch.results[0].originator , 2000
    assert_nil ch.results[0].magic_item 
    assert_nil ch.results[0].url
    assert_nil ch.results[0].note
  end
  
  def test_new_used_result
    login_as(:echizen)
    post :new_used_result , :id => @first_id , 
         :new_used_result => {
            :pc_name => "pc_name" , 
            :event_name => 'test' ,
            :initial => 2000 , 
            :result => 3000 , 
            :url => nil , 
            :note => "note" }

    ch = Character.find(@first_id)
    
    assert_equal ch.used_results[0].pc_name , "pc_name"
    assert_equal ch.used_results[0].event_name , "test"
    assert_equal ch.used_results[0].initial , 2000
    assert_equal ch.used_results[0].result , 3000
    assert_nil ch.used_results[0].url 
    assert_equal ch.used_results[0].note , "note"
  end
  

  def test_restricted_actions_without_auth_failed
    actions = [:update,:create,:destroy, :insert_by_text,:edit,
               :edit_result, :new_result , :update_result , :destroy_result,
               :new_used_result ,:update_used_result , :destroy_used_result]
    for u in [nil , :quentin]
      login_as(u)
      for action in actions
        post action , :nation_id => 32 ,:id => 1 # => @first_id
        assert_response :redirect
        assert_redirected_to :action => 'login'
      end
    end
  end
  
end
