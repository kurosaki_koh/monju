require 'test_helper'

class BondsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:bonds)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_bond
    assert_difference('Bond.count') do
      post :create, :bond => { }
    end

    assert_redirected_to bond_path(assigns(:bond))
  end

  def test_should_show_bond
    get :show, :id => bonds(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => bonds(:one).id
    assert_response :success
  end

  def test_should_update_bond
    put :update, :id => bonds(:one).id, :bond => { }
    assert_redirected_to bond_path(assigns(:bond))
  end

  def test_should_destroy_bond
    assert_difference('Bond.count', -1) do
      delete :destroy, :id => bonds(:one).id
    end

    assert_redirected_to bonds_path
  end
end
