require File.dirname(__FILE__) + '/../test_helper'

class MileChangesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:mile_changes)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_mile_change
    assert_difference('MileChange.count') do
      post :create, :mile_change => { }
    end

    assert_redirected_to mile_change_path(assigns(:mile_change))
  end

  def test_should_show_mile_change
    get :show, :id => mile_changes(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => mile_changes(:one).id
    assert_response :success
  end

  def test_should_update_mile_change
    put :update, :id => mile_changes(:one).id, :mile_change => { }
    assert_redirected_to mile_change_path(assigns(:mile_change))
  end

  def test_should_destroy_mile_change
    assert_difference('MileChange.count', -1) do
      delete :destroy, :id => mile_changes(:one).id
    end

    assert_redirected_to mile_changes_path
  end
end
