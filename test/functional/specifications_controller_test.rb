require File.dirname(__FILE__) + '/../test_helper'

class SpecificationsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:specifications)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_specification
    assert_difference('Specification.count') do
      post :create, :specification => { }
    end

    assert_redirected_to specification_path(assigns(:specification))
  end

  def test_should_show_specification
    get :show, :id => specifications(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => specifications(:one).id
    assert_response :success
  end

  def test_should_update_specification
    put :update, :id => specifications(:one).id, :specification => { }
    assert_redirected_to specification_path(assigns(:specification))
  end

  def test_should_destroy_specification
    assert_difference('Specification.count', -1) do
      delete :destroy, :id => specifications(:one).id
    end

    assert_redirected_to specifications_path
  end
end
