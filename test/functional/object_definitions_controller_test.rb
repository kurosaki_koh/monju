require File.dirname(__FILE__) + '/../test_helper'

class ObjectDefinitionsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:object_definitions)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_object_definition
    assert_difference('ObjectDefinition.count') do
      post :create, :object_definition => { }
    end

    assert_redirected_to object_definition_path(assigns(:object_definition))
  end

  def test_should_show_object_definition
    get :show, :id => object_definitions(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => object_definitions(:one).id
    assert_response :success
  end

  def test_should_update_object_definition
    put :update, :id => object_definitions(:one).id, :object_definition => { }
    assert_redirected_to object_definition_path(assigns(:object_definition))
  end

  def test_should_destroy_object_definition
    assert_difference('ObjectDefinition.count', -1) do
      delete :destroy, :id => object_definitions(:one).id
    end

    assert_redirected_to object_definitions_path
  end
end
