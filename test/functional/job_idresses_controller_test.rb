require File.dirname(__FILE__) + '/../test_helper'
require 'job_idresses_controller'

# Re-raise errors caught by the controller.
class JobIdressesController; def rescue_action(e) raise e end; end

class JobIdressesControllerTest < Test::Unit::TestCase
  fixtures :job_idresses

  def setup
    @controller = JobIdressesController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = job_idresses(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:job_idresses)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:job_idress)
    assert assigns(:job_idress).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:job_idress)
  end

  def test_create
    num_job_idresses = JobIdress.count

    post :create, :job_idress => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_job_idresses + 1, JobIdress.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:job_idress)
    assert assigns(:job_idress).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      JobIdress.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      JobIdress.find(@first_id)
    }
  end
end
