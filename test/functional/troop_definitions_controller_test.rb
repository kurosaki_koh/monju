require 'test_helper'

class TroopDefinitionsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:troop_definitions)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_troop_definition
    assert_difference('TroopDefinition.count') do
      post :create, :troop_definition => { }
    end

    assert_redirected_to troop_definition_path(assigns(:troop_definition))
  end

  def test_should_show_troop_definition
    get :show, :id => troop_definitions(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => troop_definitions(:one).id
    assert_response :success
  end

  def test_should_update_troop_definition
    put :update, :id => troop_definitions(:one).id, :troop_definition => { }
    assert_redirected_to troop_definition_path(assigns(:troop_definition))
  end

  def test_should_destroy_troop_definition
    assert_difference('TroopDefinition.count', -1) do
      delete :destroy, :id => troop_definitions(:one).id
    end

    assert_redirected_to troop_definitions_path
  end
end
