require 'test_helper'

class IdressWearingTablesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:idress_wearing_tables)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_idress_wearing_table
    assert_difference('IdressWearingTable.count') do
      post :create, :idress_wearing_table => { }
    end

    assert_redirected_to idress_wearing_table_path(assigns(:idress_wearing_table))
  end

  def test_should_show_idress_wearing_table
    get :show, :id => idress_wearing_tables(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => idress_wearing_tables(:one).id
    assert_response :success
  end

  def test_should_update_idress_wearing_table
    put :update, :id => idress_wearing_tables(:one).id, :idress_wearing_table => { }
    assert_redirected_to idress_wearing_table_path(assigns(:idress_wearing_table))
  end

  def test_should_destroy_idress_wearing_table
    assert_difference('IdressWearingTable.count', -1) do
      delete :destroy, :id => idress_wearing_tables(:one).id
    end

    assert_redirected_to idress_wearing_tables_path
  end
end
