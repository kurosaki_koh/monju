require File.dirname(__FILE__) + '/../test_helper'

class ObjectRegistriesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:object_registries)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_object_registry
    assert_difference('ObjectRegistry.count') do
      post :create, :object_registry => { }
    end

    assert_redirected_to object_registry_path(assigns(:object_registry))
  end

  def test_should_show_object_registry
    get :show, :id => object_registries(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => object_registries(:one).id
    assert_response :success
  end

  def test_should_update_object_registry
    put :update, :id => object_registries(:one).id, :object_registry => { }
    assert_redirected_to object_registry_path(assigns(:object_registry))
  end

  def test_should_destroy_object_registry
    assert_difference('ObjectRegistry.count', -1) do
      delete :destroy, :id => object_registries(:one).id
    end

    assert_redirected_to object_registries_path
  end
end
