require 'test_helper'

class ConvertSourcesControllerTest < ActionController::TestCase
  setup do
    @unit_registry = convert_sources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unit_registries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create convert_source" do
    assert_difference('ConvertSource.count') do
      post :create, unit_registry: { authority_url: @unit_registry.authority_url, convert_definition_id: @unit_registry.convert_definition_id, name: @unit_registry.name, note: @unit_registry.note, owner_account_id: @unit_registry.owner_account_id }
    end

    assert_redirected_to convert_source_path(assigns(:unit_registry))
  end

  test "should show convert_source" do
    get :show, id: @unit_registry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unit_registry
    assert_response :success
  end

  test "should update convert_source" do
    put :update, id: @unit_registry, unit_registry: { authority_url: @unit_registry.authority_url, convert_definition_id: @unit_registry.convert_definition_id, name: @unit_registry.name, note: @unit_registry.note, owner_account_id: @unit_registry.owner_account_id }
    assert_redirected_to convert_source_path(assigns(:unit_registry))
  end

  test "should destroy convert_source" do
    assert_difference('ConvertSource.count', -1) do
      delete :destroy, id: @unit_registry
    end

    assert_redirected_to convert_sources_path
  end
end
