require File.dirname(__FILE__) + '/../test_helper'

class OwnerAccountsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:owner_accounts)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_owner_account
    assert_difference('OwnerAccount.count') do
      post :create, :owner_account => { }
    end

    assert_redirected_to owner_account_path(assigns(:owner_account))
  end

  def test_should_show_owner_account
    get :show, :id => owner_accounts(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => owner_accounts(:one).id
    assert_response :success
  end

  def test_should_update_owner_account
    put :update, :id => owner_accounts(:one).id, :owner_account => { }
    assert_redirected_to owner_account_path(assigns(:owner_account))
  end

  def test_should_destroy_owner_account
    assert_difference('OwnerAccount.count', -1) do
      delete :destroy, :id => owner_accounts(:one).id
    end

    assert_redirected_to owner_accounts_path
  end
end
