require File.dirname(__FILE__) + '/../test_helper'

class AcesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:aces)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ace
    assert_difference('Ace.count') do
      post :create, :ace => { }
    end

    assert_redirected_to ace_path(assigns(:ace))
  end

  def test_should_show_ace
    get :show, :id => aces(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => aces(:one).id
    assert_response :success
  end

  def test_should_update_ace
    put :update, :id => aces(:one).id, :ace => { }
    assert_redirected_to ace_path(assigns(:ace))
  end

  def test_should_destroy_ace
    assert_difference('Ace.count', -1) do
      delete :destroy, :id => aces(:one).id
    end

    assert_redirected_to aces_path
  end
end
