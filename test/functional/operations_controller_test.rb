require File.dirname(__FILE__) + '/../test_helper'
require 'operations_controller'

# Re-raise errors caught by the controller.
class OperationsController; def rescue_action(e) raise e end; end

class OperationsControllerTest < Test::Unit::TestCase
  fixtures :operations

  def setup
    @controller = OperationsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = operations(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:operations)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:operation)
    assert assigns(:operation).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:operation)
  end

  def test_create
    num_operations = Operation.count

    post :create, :operation => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_operations + 1, Operation.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:operation)
    assert assigns(:operation).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      Operation.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Operation.find(@first_id)
    }
  end
end
