require File.dirname(__FILE__) + '/../test_helper'

class ModifyCommandsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:modify_commands)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_modify_command
    assert_difference('ModifyCommand.count') do
      post :create, :modify_command => { }
    end

    assert_redirected_to modify_command_path(assigns(:modify_command))
  end

  def test_should_show_modify_command
    get :show, :id => modify_commands(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => modify_commands(:one).id
    assert_response :success
  end

  def test_should_update_modify_command
    put :update, :id => modify_commands(:one).id, :modify_command => { }
    assert_redirected_to modify_command_path(assigns(:modify_command))
  end

  def test_should_destroy_modify_command
    assert_difference('ModifyCommand.count', -1) do
      delete :destroy, :id => modify_commands(:one).id
    end

    assert_redirected_to modify_commands_path
  end
end
