# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../test_helper'
require 'nations_controller'

# Re-raise errors caught by the controller.
class NationsController; def rescue_action(e) raise e end; end

class NationsControllerTest < Test::Unit::TestCase
  include AuthenticatedTestHelper
  fixtures :nations , :users , :characters , :results

  def setup
    @controller = NationsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = nations(:dva).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:nations)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:nation)
    assert assigns(:nation).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:nation)
  end

  def test_create
    login_as(:admin)
    num_nations = Nation.count

    post :create, :nation => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_nations + 1, Nation.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:nation)
    assert assigns(:nation).valid?
  end

  def test_update
    login_as(:admin)
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
#    assert_redirected_to :controller => 'account' , :action => 'login'
  end

  def test_update_auth_failed
    post :update, :id => @first_id
    assert_response :redirect
#    assert_redirected_to :action => 'show', :id => @first_id
    assert_redirected_to :controller => 'account' , :action => 'login'
  end

  def test_destroy_auth_failed
    assert_nothing_raised {
      Nation.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'login'

#    assert_raise(ActiveRecord::RecordNotFound) {
#      Nation.find(@first_id)
#    }
  end
  
  def test_restricted_actions_without_auth_failed
    actions = [:create , :update_weared_idress,:update,:destroy ]
    
    for action in actions
      post action , :id => @first_id
      assert_response :redirect
      assert_redirected_to :action => 'login'
    end
  end


  RESULT_TEXT = <<-EOT
3200004：character4：20000：戦R参
1200001：dva1：20000：戦R参
3200004：character4：戦R参

  EOT
  
  #特定国のリザルト入力は、その国のキャラ以外は対象外とする
  def test_parse_result_text_1
    login_as(:admin)

    parameters = {}
    parameters[:submit_format] = RESULT_TEXT
    parameters[:id]=32
    parameters[:reference_url] = "http://test.net/"
    parameters[:default_originator] = 1234
    post :confirm_register_result , parameters
    
    results = assigns(:parse_results)
#    p results[0]
#    p results[2]
    assert_equal '3200004' , results[0].character_no
    assert_equal '1200001' , assigns(:db_not_found)[0].character_no
    assert_equal '1234' , results[2].originator
  end

  #管理者モードでのリザルト入力は、どの国も受け付ける
  def test_parse_result_text_2
    login_as(:admin)

    parameters = {}
    parameters[:submit_format] = RESULT_TEXT
    parameters[:id]="admin"
    
    post :confirm_register_result , parameters
    
    results = assigns(:parse_results)
    assert_nil  assigns(:db_not_found)[0]
  end

  def test_delete_results_by_event_1
    login_as(:admin)
    
    parameters = {}
    parameters[:id] = "32"
    parameters[:result_id] = "1"
    
    post :delete_results_by_event , parameters
    assert_raise(ActiveRecord::RecordNotFound){Result.find(1)}
  end

  def test_delete_results_by_event_2
    login_as(:echizen)
    
    parameters = {}
    parameters[:id] = "32"
    parameters[:result_id] = "1"
    
    post :delete_results_by_event , parameters
    assert_raise(ActiveRecord::RecordNotFound){Result.find(1)}
  end

  def test_delete_results_by_event_3
    login_as(:quentin)
    
    parameters = {}
    parameters[:id] = "32"
    parameters[:result_id] = "1"
    
    post :delete_results_by_event , parameters
    assert_not_nil Result.find(1)
  end

  def test_delete_results_by_event_4
    login_as(:admin)
    
    parameters = {}
    parameters[:id] = "admin"
    parameters[:result_id] = "1"
    
    post :delete_results_by_event , parameters
    p assigns(:destroyed)
#    p assigns(:nation_id)
    p assigns(:results)
    assert_raise(ActiveRecord::RecordNotFound){Result.find(1)}
  end



end
