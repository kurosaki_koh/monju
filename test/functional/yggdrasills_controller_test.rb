require File.dirname(__FILE__) + '/../test_helper'
require 'yggdrasills_controller'

# Re-raise errors caught by the controller.
class YggdrasillsController; def rescue_action(e) raise e end; end

class YggdrasillsControllerTest < Test::Unit::TestCase
  fixtures :yggdrasills

  def setup
    @controller = YggdrasillsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = yggdrasills(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:yggdrasills)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:yggdrasill)
    assert assigns(:yggdrasill).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:yggdrasill)
  end

  def test_create
    num_yggdrasills = Yggdrasill.count

    post :create, :yggdrasill => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_yggdrasills + 1, Yggdrasill.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:yggdrasill)
    assert assigns(:yggdrasill).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      Yggdrasill.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Yggdrasill.find(@first_id)
    }
  end
end
