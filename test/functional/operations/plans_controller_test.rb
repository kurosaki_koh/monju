require File.dirname(__FILE__) + '/../../test_helper'
require 'operations/plans_controller'

# Re-raise errors caught by the controller.
class Operations::PlansController; def rescue_action(e) raise e end; end

class Operations::PlansControllerTest < Test::Unit::TestCase
  fixtures :operation_nodes

  def setup
    @controller = Operations::PlansController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = operation_nodes(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:operation_nodes)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:operation_node)
    assert assigns(:operation_node).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:operation_node)
  end

  def test_create
    num_operation_nodes = OperationNode.count

    post :create, :operation_node => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_operation_nodes + 1, OperationNode.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:operation_node)
    assert assigns(:operation_node).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      OperationNode.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      OperationNode.find(@first_id)
    }
  end
end
