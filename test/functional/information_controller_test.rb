require File.dirname(__FILE__) + '/../test_helper'
require 'information_controller'

# Re-raise errors caught by the controller.
class InformationController; def rescue_action(e) raise e end; end

class InformationControllerTest < Test::Unit::TestCase
  fixtures :information

  def setup
    @controller = InformationController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = information(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:information)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:information)
    assert assigns(:information).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:information)
  end

  def test_create
    num_information = Information.count

    post :create, :information => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_information + 1, Information.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:information)
    assert assigns(:information).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      Information.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      Information.find(@first_id)
    }
  end
end
