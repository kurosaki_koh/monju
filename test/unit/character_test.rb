require File.dirname(__FILE__) + '/../test_helper'

class CharacterTest < Test::Unit::TestCase
  fixtures :races , :yggdrasills , :nations , :job_idresses , :characters
 
  # Replace this with your real tests.
  def test_truth
    assert true
  end
  
  def test_sum_originator
    echizen = characters(:first)
    test2 = characters(:two)
    test3 = characters(:test3)    
    assert_equal 0,echizen.sum_originator 
    assert_equal 20000 , test2.sum_originator 
    assert_equal 0 , test3.sum_originator 
  end
end
