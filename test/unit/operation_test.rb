require File.dirname(__FILE__) + '/../test_helper'

class OperationTest < Test::Unit::TestCase
  fixtures :operations,:nations #, :job_idresses , :yggdrasills

  def setup
    @echizen = Nation.find(32)
    @op = Operation.find(1)
  end

  def test_init_content
    @op.init_content
p @op.content
    assert_equal 1,@op.content[32][:fund]
    assert_equal 2,@op.content[32][:resource]
    assert_equal 3,@op.content[32][:food]
    assert_equal 4,@op.content[32][:fuel]
    assert_equal 5,@op.content[32][:num_npc]
  end
end
