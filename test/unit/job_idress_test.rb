# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../test_helper'
require 'pp'

class JobIdressTest < Test::Unit::TestCase
  fixtures :races , :yggdrasills , :nations , :job_idresses

  def setup
    @forcer = JobIdress.find(1001)
    @mighty_swordman = JobIdress.find(1002)
    @dragger = JobIdress.find(1003)
    @test1 = JobIdress.find(2001)
  end

  # Replace this with your real tests.
  def test_create
    assert_kind_of JobIdress , @forcer
    assert_equal 1001 , @forcer.id
    assert_equal "理族" , @forcer.name
    assert_equal 4 , @forcer.race_id
    assert_equal 101 , @forcer.job1_id
    assert_equal 106 , @forcer.job2_id
    assert_nil @job3_id
    assert_nil @forcer.job4_id
    assert_equal 32 , @forcer.nation_id
  end
  
  def test_is_this
    assert @forcer.is_this?(["東国人","吏族","理力使い"])
    assert_equal false, @forcer.is_this?(["西国人","吏族","理力使い"])
    assert_equal false, @forcer.is_this?(["東国人","剣士","理力使い"])
  end
  
  def test_abilities
    assert_equal 1 , @test1[:phy]
    assert_equal 2 , @test1[:str]
    assert_equal 3 , @test1[:dur]
    assert_equal 4 , @test1[:chr]
    assert_equal 5 , @test1[:agi]
    assert_equal 6 , @test1[:dex]
    assert_equal 7 , @test1[:sen]
    assert_equal 8 , @test1[:edu]
    assert_equal 9 , @test1[:luc]
    
    assert_equal 1.5 , @test1[:zero_attack]
    assert_equal 1.5 , @test1[:to_5m]
    assert_equal 7.5 , @test1[:to_50m]
    assert_equal 7.5 , @test1[:to_300m]
    assert_equal 6 , @test1[:to_500m]
    assert_equal 2 , @test1[:defense]
    assert_equal nil , @test1[:error]
  end

  def test_rd
    assert_equal 1.5 , @test1.rd(:phy)
    assert_equal 2.25 , @test1.rd(:str)
    assert_equal 3.375 , @test1.rd(:dur)
    assert_equal nil , @test1.rd(:error)
  end
  
  def test_skills
    skills =  @mighty_swordman.skills
    assert_equal "宇宙戦行為",skills[0].name
    assert_equal "宇宙戦行為",skills[0].modes[0][:name]
    assert_equal "ナショナルネット接続行為",skills[1].modes[0][:name]
    
    skills = @dragger.skills
    assert_equal "薬物による強化行為",skills[0].name
    assert_equal "体格+1",skills[0].modes[0][:name]
    assert_equal "筋力+1",skills[0].modes[1][:name]
  end
  
end
