require File.dirname(__FILE__) + '/../test_helper'

class OperationNodeTest < Test::Unit::TestCase
  fixtures :operation_nodes , :nations , :races , :yggdrasills , :job_idresses,  :operations

  def setup
    @dog1 = OperationNode.find(2)
    @dog2 = OperationNode.find(3)
    @dog3 = OperationNode.find(4)
  end
  
  def test_cost
    assert_equal 0.5 , @dog1.cost(:food)
    assert_equal 0.5 , @dog2.cost(:food)
    assert_equal 0   , @dog2.cost(:fund)
    assert_equal 0.5 , @dog3.cost(:food)
    assert_equal 1   , @dog3.cost(:fund)
  end
end
