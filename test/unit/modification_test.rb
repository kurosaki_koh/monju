# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../test_helper'

class ModificationTest < Test::Unit::TestCase
  fixtures :modifications , :yggdrasills , :races , :nations , :job_idresses , :skills 

  # Replace this with your real tests.
  def setup
    @dragger = JobIdress.find(1003)
    @drag = Skill.find(1)
  end

  def test_set_values
    mo = Modification.new
    mo.set_values(@drag)
    assert_equal 1,mo.sen
    assert_equal 0,mo.str
    assert_equal 0,mo.mode
    assert_equal false,mo.enable
    assert_equal "薬物吸引:感覚+1", mo.name
  end
end
